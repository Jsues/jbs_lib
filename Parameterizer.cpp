#include "Parameterizer.h"
#include "operators/getVertexFan.h"

#include <map>
#include <fstream>

extern "C" {
#include <taucs.h>
}

#ifndef NO_UMFPACK
#include "SparseMatrix.h"
#endif

void computeWeightsCotangent(SimpleMesh* sm) {
	// Compute cotangent weights of urshape:
	for (UInt i1 = 0; i1 < sm->eList.size(); i1++) {
		sm->eList[i1]->computeCotangentWeights();
	}
}

void computeWeightsChordal(SimpleMesh* sm) {
	// Compute cordal weights of urshape:
	for (UInt i1 = 0; i1 < sm->eList.size(); i1++) {
		sm->eList[i1]->cotangent_weight = sm->vList[sm->eList[i1]->v0].c.dist(sm->vList[sm->eList[i1]->v1].c);
		if (sm->eList[i1]->cotangent_weight > 0)
			sm->eList[i1]->cotangent_weight = 1.0/sm->eList[i1]->cotangent_weight;
	}
}

void computeWeightsUniform(SimpleMesh* sm) {
	// Compute cotangent weights of urshape:
	for (UInt i1 = 0; i1 < sm->eList.size(); i1++) {
		sm->eList[i1]->cotangent_weight = 1;
	}
}

void computeWeightsShapePres(SimpleMesh* sm) {
	// Compute cotangent weights of urshape:
	for (UInt i1 = 0; i1 < sm->eList.size(); i1++) {
		sm->eList[i1]->cotangent_weight = 1;
	}
}

//******************************************************************************************

//! Computes the barycentric coordinates of the point (0,0) w.r.t the triangle (v0,v1,v2).
vec3d getBarycentrics(vec2d v0, vec2d v1, vec2d v2) {

	vec2d p(0,0);

	double A_tri = v1[0]*v2[1] + v2[0]*v0[1] + v0[0]*v1[1] - v0[0]*v2[1] - v2[0]*v1[1] - v1[0]*v0[1];
	double A1 =  p[0]*v2[1] + v2[0]*v0[1] + v0[0]*p[1] - v0[0]*v2[1] - v2[0]*p[1] - p[0]*v0[1];
	double A2 = v1[0]*p[1] + p[0]*v0[1] + v0[0]*v1[1] - v0[0]*p[1] - p[0]*v1[1] - v1[0]*v0[1];
	double A0 = v1[0]*v2[1] + v2[0]*p[1] + p[0]*v1[1] - p[0]*v2[1] - v2[0]*v1[1] - v1[0]*p[1];

	double u = A0/A_tri;
	double v = A1/A_tri;
	double w = A2/A_tri;

	return vec3d(u, v, w);
}

//******************************************************************************************

Parameterizer::Parameterizer(SimpleMesh* mesh_, std::vector<int> boundary_) {

	mesh = mesh_;
	boundary = boundary_;
}

//******************************************************************************************

Parameterizer::~Parameterizer(void) {}

//******************************************************************************************

#ifndef NO_TAUCS
std::vector<vec2d> Parameterizer::doParametrization(enum METHOD met, enum BOUNDARY bound) {


	// Reserve memory for all points
	//std::cerr << "Parameterizing " << (int)mesh->vList.size() << " Points" << std::endl;
	parametrization.resize(mesh->vList.size());


	fixBoundaryVertices(bound);


	int num_of_points = (int)mesh->vList.size();


	if (met == Parameterizer::COTANGENT) {
		computeWeightsCotangent(mesh);
	} else if (met == Parameterizer::CHORD) {
		computeWeightsChordal(mesh);
	} else if (met == Parameterizer::SHAPE_PRES) {
		computeWeightsShapePres(mesh);
	} else {
		computeWeightsUniform(mesh);
	}


	/************************************/
	// Parameterize inner points
	/************************************/
	std::vector<double> entries;
	std::vector<int> row_index;				// row indices, 0-based
	std::vector<int> col_ptr;				// pointers to where columns begin in rowind and values 0-based, length is (n+1)
	col_ptr.push_back(0);


	// b component of Ax = b, umfpack specific arrays of doubles
	std::vector<double> b(2*num_of_points);

	// for all points
	for (int i1 = 0; i1 < num_of_points; i1++) {
		Vertex* v = &mesh->vList[i1];
		
		b[i1] = b[i1+num_of_points] = 0;

		if (v->is_boundary_point) {
			/***************************************************
			/ v ist ein Randpunkt, f�ge in die entsprechende	/
			/ Matrixzeile an pos. i1 eine 1 ein,				/
			/ in den Vektor bx/by an pos i1 die Koordinaten		/
			/ des Punktes auf dem Einheitskreis					/
			 ***************************************************/

			entries.push_back(1);
			row_index.push_back(i1);

			b[i1] = parametrization[i1].x;
			b[num_of_points+i1] = parametrization[i1].y;

		} else {

			std::map<int, double > row;

			int numN = v->getNumN();
			double summedWeights = 0;
			for (int i2 = 0; i2 < numN; i2++) {
				UInt neighbor = v->getNeighbor(i2, i1);
				double weight = v->eList[i2]->cotangent_weight;
				summedWeights += weight;
				row.insert(std::make_pair(neighbor, weight));
			}
			row.insert(std::make_pair(i1, -summedWeights));

			for (std::map<int, double>::const_iterator it = row.begin(); it != row.end(); it++) {
				int idx = it->first;
				double weight = it->second;

				if (mesh->vList[idx].is_boundary_point) {
					b[i1]				+= weight * parametrization[idx].x;
					b[num_of_points+i1]	+= weight * parametrization[idx].y;
				} else {
					if (idx <= i1) {
						entries.push_back(-weight);
						row_index.push_back(idx);
					}
				}

			}
		}

		col_ptr.push_back((UInt)entries.size());
	}


	// create TAUCS matrix from vector objects an, jn and ia
	taucs_ccs_matrix  A; // a matrix to solve Ax=b in CCS format
	A.n = num_of_points;
	A.m = num_of_points;
	A.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
	A.colptr = &col_ptr[0];
	A.rowind = &row_index[0];
	A.values.d = &entries[0];
	std::vector<double> xv_x(num_of_points);
	std::vector<double> xv_y(num_of_points);

	// solve the linear system
	void* F = NULL;
	char* options[] = {"taucs.factor.LLT=true", NULL};
	void* opt_arg[] = { NULL };


	std::vector<double> x(2*num_of_points);
	taucs_double* x_p = &x[0];
	taucs_double* b_p = &b[0];



	int i = taucs_linsolve(&A, &F, 2, x_p, b_p, options, opt_arg);
	i = taucs_linsolve(NULL,&F,0,NULL,NULL,NULL,NULL);



	for (int i1 = 0; i1 < num_of_points; i1++) {
		//vec2d pt(xv_x[i1], xv_y[i1]);
		vec2d pt(x[i1], x[i1+num_of_points]);
		parametrization[i1] = pt;
		mesh->vList[i1].param = pt;
	}


	return parametrization;

}
#endif

//******************************************************************************************

#ifndef NO_UMFPACK
std::vector<vec2d> Parameterizer::doParametrization(enum METHOD met, enum BOUNDARY bound) {


	// Reserve memory for all points
	std::cerr << "Parameterizing " << (int)mesh->vList.size() << " Points" << std::endl;
	parametrization.resize(mesh->vList.size());

	fixBoundaryVertices(bound);

	int num_of_points = (int)mesh->vList.size();
	/************************************/
	// Parameterize inner points
	/************************************/
	SparseMatrixCol<double> M(num_of_points);


	// b component of Ax = b, umfpack specific arrays of doubles
	double* b_x = new double[num_of_points];
	double* b_y = new double[num_of_points];

	// for all points
	for (int i1 = 0; i1 < num_of_points; i1++) {
		Vertex* v = &mesh->vList[i1];
		

		if (v->is_boundary_point) {
			/***************************************************
			/ v ist ein Randpunkt, f�ge in die entsprechende	/
			/ Matrixzeile an pos. i1 eine 1 ein,				/
			/ in den Vektor bx/by an pos i1 die Koordinaten		/
			/ des Punktes auf dem Einheitskreis					/
			 ***************************************************/

			M.insert(i1, i1, 1);

			b_x[i1] = parametrization[i1][0];
			b_y[i1] = parametrization[i1][1];

		} else if (met == Parameterizer::CHORD) {
			/***************************************************
			/ v ist ein innerer Punkt, berechne die Gewichte	/
			/ zu seinen Nachbarn								/
			***************************************************/
			
			M.insert(i1, i1, 1);

			// Sammle alle Punkte die Nachbarn von v sind oder von denen v Nachbar ist:
			std::map<int, double> neighbors;

			// nur die Nachbarn des betrachteten Punktes
			double summed_up_weight = 0.0;
			for (unsigned int i2 = 0; i2 < v->eList.size(); i2++) {
				int n = (v->eList[i2]->v0 == i1) ? v->eList[i2]->v1 : v->eList[i2]->v0;
				neighbors[n] = 1/(mesh->vList[i1].c - mesh->vList[n].c).length();
				summed_up_weight += neighbors[n];
			}

			int num_neighbors = (int)neighbors.size();
			double right_hand_x = 0.0;
			double right_hand_y = 0.0;

			// Normiere die Gewichte und f�ge sie in die Matrix ein:
			std::map<int, double>::iterator it = neighbors.begin();
			for (int i2 = 0; i2 < num_neighbors; i2++) {
				
				Vertex* neighbor_v = &mesh->vList[it->first];
				double weight = 0 - (it->second/summed_up_weight);

				if (!neighbor_v->is_boundary_point) {
					// F�ge Gewicht in die Matrix ein:
					M.insert(it->first, i1, weight); 
				} else {
					// Der Nachbar ist ein Randvertex
					right_hand_x -= weight * parametrization[it->first][0];
					right_hand_y -= weight * parametrization[it->first][1];
				}
				++it;
			}			

			b_x[i1] = right_hand_x;
			b_y[i1] = right_hand_y;

		} else if (met == Parameterizer::SHAPE_PRES) {
			/***************************************************
			/ v ist ein innerer Punkt, berechne die Gewichte	/
			/ zu seinen Nachbarn								/
			***************************************************/
			// F�ge den Punkt selbst ein:
			M.insert(i1, i1, 1);

			double right_hand_x = 0.0;
			double right_hand_y = 0.0;

			int num_neighbors = (int)v->eList.size();

			// Sammle alle Punkte die Nachbarn von v sind oder von denen v Nachbar ist:
			std::map<int, double> neighbors;

			std::vector<int> fan;
			getVertexFan(mesh, i1, fan);

			// Flat'e 1-Neighborhood:
			std::vector< std::pair<double, double> > polarcoords;
			double angle_sum = 0;
			for (int i2 = 0; i2 < num_neighbors; i2++) {
				int n1 = fan[i2];
				int n2 = fan[i2+1];
				// Calculate angle:
				vec3d side1 = mesh->vList[i1].c - mesh->vList[n1].c;
				double length = side1.length();
				side1.normalize();
				vec3d side2 = mesh->vList[i1].c - mesh->vList[n2].c;
				side2.normalize();
				double angle = acos(side1|side2);
				polarcoords.push_back(std::pair<double, double>(angle, length));
				angle_sum += angle;
			}
			if (v->is_boundary_point) angle_sum /= M_PI;
			else angle_sum /= M_PI2;

			// Speichere geflatteten Fan:
			std::vector<vec2d> neighbors_flat;
			double angle_used = 0;
			for (int i2 = 0; i2 < num_neighbors; i2++) {
				vec2d pos(cos(angle_used)*polarcoords[i2].second, sin(angle_used)*polarcoords[i2].second);
				//pos.print();
				neighbors_flat.push_back(pos);
				angle_used += polarcoords[i2].first/angle_sum;
			}
			// For each q_i find a triangle (q_i, q_{i+1}, p) which contains (0,0):
			double* barys = new double[num_neighbors];
			for (int i2 = 0; i2 < num_neighbors; i2++)
				barys[i2] = 0;

			int num = 0;
			for (int i2 = 0; i2 < num_neighbors; i2++) {
				vec2d q_i = neighbors_flat[i2];
				vec2d q_i1 = neighbors_flat[(i2+1)%num_neighbors];

				// Find the p:
				vec2d p;
				for (int i3 = 0; i3 < num_neighbors; i3++) {
					p = neighbors_flat[i3];
					vec3d bary = getBarycentrics(q_i, q_i1, p);
					if (bary.x >= 0 && bary.y >= 0 && bary.z >= 0) {
						// Point found:
						barys[i2] += bary.x;
						barys[(i2+1)%neighbors_flat.size()] += bary.y;
						barys[i3] += bary.z;

						num++;
						break;
					}
				}
			}
			for (int i2 = 0; i2 < num_neighbors; i2++) {
				barys[i2] /= num;
			}

			// The barys are now our weights
			for (int i2 = 0; i2 < num_neighbors; i2++) {

				int n = (v->eList[i2]->v0 == i1) ? v->eList[i2]->v1 : v->eList[i2]->v0;
				Vertex* neighbor_v = &mesh->vList[n];

				double weight = 0 - barys[i2];

//				std::cerr << "Weight = " << weight << std::endl;
				
				if (!neighbor_v->is_boundary_point) {
					// F�ge Gewicht in die Matrix ein:
					M.insert(n, i1, weight); 
				} else {
					// Der Nachbar ist ein Randvertex
					right_hand_x -= weight * parametrization[n][0];
					right_hand_y -= weight * parametrization[n][1];
				}
			}

			b_x[i1] = right_hand_x;
			b_y[i1] = right_hand_y;

		} else if (met == Parameterizer::MEAN_VALUE) {
			/***************************************************
			/ v ist ein innerer Punkt, berechne die Gewichte	/
			/ zu seinen Nachbarn								/
			***************************************************/
			// F�ge den Punkt selbst ein:
			M.insert(i1, i1, 1);

			double right_hand_x = 0.0;
			double right_hand_y = 0.0;

			UInt num_neighbors = (UInt)v->getNumN();

			std::vector<int> fan;
			getVertexFan(mesh, i1, fan);

			std::vector<double> weights;
			double sum_of_weights = 0;

			for (UInt i2 = 0; i2 < num_neighbors; i2++) {
				int prev = fan[(i2-1+num_neighbors)%num_neighbors];
				int neighbor = fan[i2];
				int next = fan[i2+1];

				vec3d prev_v = v->c - mesh->vList[prev].c;
				vec3d neighbor_v = v->c - mesh->vList[neighbor].c;
				vec3d next_v = v->c - mesh->vList[next].c;

				double length = neighbor_v.length();
				prev_v.normalize();
				neighbor_v.normalize();
				next_v.normalize();

				double sin_a_m1 = (prev_v^neighbor_v).length();
				double cos_a_m1 = (prev_v|neighbor_v);

				double sin_a = (neighbor_v^next_v).length();
				double cos_a = (neighbor_v|next_v);

				double w = ((sin_a_m1/(1+cos_a_m1)) + (sin_a/(1+cos_a))) / length;
				weights.push_back(w);
				sum_of_weights += w;
			}

			for (UInt i2 = 0; i2 < num_neighbors; i2++) {
				int n = fan[i2];
				double w = -weights[i2]/sum_of_weights;
				if (!mesh->vList[n].is_boundary_point) {
					// F�ge Gewicht in die Matrix ein:
					M.insert(n, i1, w); 
				} else {
					// Der Nachbar ist ein Randvertex
					right_hand_x -= w * parametrization[n][0];
					right_hand_y -= w * parametrization[n][1];
				}
			}

			b_x[i1] = right_hand_x;
			b_y[i1] = right_hand_y;
		}
	}


	std::cerr << "Matrix aufgebaut" << std::endl;


	double* x_x = new double[num_of_points];
	double* x_y = new double[num_of_points];

	// Copy compressed row matrix to compressed column matrix:
	CompColSparseMatrix ccsm; 
	M.getCompColSparseMatrix(ccsm);

	ccsm.computeLU_Umfpack();
	ccsm.backSolve_Umfpack(b_x, x_x);
	ccsm.backSolve_Umfpack(b_y, x_y);

	//std::cerr << "Image dimensions: " << ccsm.dim << "x" << ccsm.dim << std::endl;
	//ccsm.printToImage("matrix.raw");

	for (int i1 = 0; i1 < num_of_points; i1++) {
		parametrization[i1] = vec2d(x_x[i1], x_y[i1]);
		mesh->vList[i1].param = vec2d(x_x[i1], x_y[i1]);
	}

	delete[] x_x;
	delete[] x_y;

	delete[] b_x;
	delete[] b_y;

	return parametrization;

}
#endif

//******************************************************************************************


void Parameterizer::fixBoundaryVertices(enum BOUNDARY bound) {

	// Calculate length of boundary:
	double length = 0.0;
	for (unsigned int i1 = 0; i1 < boundary.size()-1; i1++) {
		length += (mesh->getVertex(boundary[i1]) - mesh->getVertex(boundary[i1+1])).length();
		// Mark the point as boundary
		mesh->vList[boundary[i1]].is_boundary_point = true;
	}

	/************************************/
	// Parameterize boundary points
	/************************************/
	if (bound == Parameterizer::CIRCLE) {

		// First point of boundary is mapped to (1,0);
		parametrization[boundary[0]] = vec2d(1,0);

		double used_segsize = 0.0;
		for (unsigned int i1 = 0; i1 < boundary.size()-2; i1++) {
			// Berechne Prozentsatz der L�nge:
			used_segsize += (mesh->getVertex(boundary[i1]) - mesh->getVertex(boundary[i1+1])).length();
			double phi = (used_segsize / length) * (M_PI2);

			parametrization[boundary[i1+1]] = vec2d(cos(phi), sin(phi));
		}

	} else if (bound == Parameterizer::RECT) {
		// ------------------
		// First, compute the Tangentplane of the boundary vertices:
		// ------------------

		// Compute centroid:
		vec3d c(0,0,0);
		for (unsigned int i1 = 0; i1 < boundary.size(); i1++) {
			c += mesh->getVertex(i1);
		}
		c /= (double)boundary.size();
		// Compute Covariance Matrix:
		Matrix3D<double> cv;
		for (unsigned int i1 = 0; i1 < boundary.size(); i1++) {
			vec3d pt = mesh->getVertex(i1);
			vec3d diff = pt-c;

			double a_ = diff[0]*diff[0];
			double b_ = diff[0]*diff[1];
			double c_ = diff[0]*diff[2];
			double d_ = b_;
			double e_ = diff[1]*diff[1];
			double f_ = diff[1]*diff[2];
			double g_ = c_;
			double h_ = f_;
			double i_ = diff[2]*diff[2];

			Matrix3D<double> temp_m(a_, b_, c_, d_, e_, f_, g_, h_, i_);
			cv.add(temp_m);
		}
		cv.divide((double)boundary.size());

		// First point of boundary is mapped to (0,0);
		parametrization[boundary[0]] = vec2d(0,0);
		double used_segsize = 0.0;
		for (unsigned int i1 = 0; i1 < boundary.size()-2; i1++) {
			// Berechne Prozentsatz der L�nge:
			used_segsize += (mesh->getVertex(boundary[i1]) - mesh->getVertex(boundary[i1+1])).length();

			float used_segsize_percentage = (float)(used_segsize/length);

			if (used_segsize_percentage < 0.25f)
				parametrization[boundary[i1+1]] = vec2d(0, used_segsize_percentage * 4);
			else if ((used_segsize/length) < 0.5f)
				parametrization[boundary[i1+1]] = vec2d((used_segsize_percentage-0.25) * 4, 1);
			else if ((used_segsize/length) < 0.75f)
				parametrization[boundary[i1+1]] = vec2d(1, 1-(used_segsize_percentage-0.5f) * 4);
			else 
				parametrization[boundary[i1+1]] = vec2d(1-(used_segsize_percentage-0.75f) * 4, 0);
		}
	} else if (bound == Parameterizer::USEMESHVERTICES) {
		for (UInt i1 = 0; i1 < boundary.size(); i1++) {
			const vec3d& pos = mesh->vList[boundary[i1]].c;
			parametrization[boundary[i1]] = vec2d(pos.x, pos.y);
		}
	}

}

//******************************************************************************************

void Parameterizer::createIVFile(const char *filename) {


	std::ofstream file(filename);


	// Header schreiben.
	file << "#Inventor V2.1 ascii\n\n";
	file << "Separator {\n";
	file << "	Coordinate3 {\n";
	file << "		point [\n";
	// Punkte hier
	for(int i1 = 0; i1 < (int)mesh->vList.size(); i1++)
			file << parametrization[i1].x << " " << parametrization[i1].y << " " << 0.0 << "\n";

	file << "		]\n";
	file << "   DrawStyle { pointSize 3 }\n";
	file << "PointSet { }\n";
	file << "}\n";
	file.close();
}

//******************************************************************************************

void Parameterizer::createOFFFile(const char *filename) {


	std::ofstream file(filename);


	// Header schreiben.
	file << "OFF" << std::endl;
	file << mesh->getNumV() << " " << mesh->getNumT() << " 0" << std::endl;
	// Punkte hier
	for(int i1 = 0; i1 < (int)mesh->vList.size(); i1++)
			file << parametrization[i1].x << " " << parametrization[i1].y << " " << 0.0 << "\n";
	// Dreiecke schreiben
	for (int i1 = 0; i1 < mesh->getNumT(); i1++) {
		Triangle* t = mesh->tList[i1];
		file << "3 " << t->v0() << " " << t->v1() << " " << t->v2() << std::endl;
	}
	file.close();
}

//******************************************************************************************
