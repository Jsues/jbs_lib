#include "iso4d.h"

void stream_bin(short n) {
//	std::cerr << (n&8) ? "1" : "0" << (n&4) ? "1" : "0" << (n&2) ? "1" : "0" << (n&1) ? "1" : "0" << " " ;
	std::cerr << ((n&8) ? "1" : "0");
	std::cerr << ((n&4) ? "1" : "0");
	std::cerr << ((n&2) ? "1" : "0");
	std::cerr << ((n&1) ? "1" : "0");
	std::cerr << " ";
}