/*
#include "include/operators/UmbrellaMinimizeSelectionTNT.h"

#include <map>

#include "tnt.h"
#include "jama_lu.h"


void SmoothMeshTNT_Ver2(SimpleMesh* sm) {

	// Set the vertex marker 'is_boundary_point' true for all vertices that are to be moved:
	for (unsigned int i1 = 0; i1 < sm->tList.size(); i1++) {
		Triangle* t = sm->tList[i1];
		if (t->marker) {
			sm->vList[t->v0()].is_boundary_point = true;
			sm->vList[t->v1()].is_boundary_point = true;
			sm->vList[t->v2()].is_boundary_point = true;
		}
	}

	int numVertices = (int)sm->vList.size();
	// Compute map
	std::map<int, int> indexLookup;
	int numMoovables = 0;
	for (int i1 = 0; i1 < numVertices; i1++) {
		if (sm->vList[i1].is_boundary_point) {
			indexLookup.insert(std::pair<int, int>(i1, numMoovables));
			numMoovables++;
		}
	}


	//******************************************************************************************
	// Use LU decomposition to solve:

	TNT::Array2D<double> A(numMoovables, numMoovables);
	for (int i1 = 0; i1 < numMoovables; i1++) for (int i2 = 0; i2 < numMoovables; i2++) A[i1][i2] = 0.0;

	TNT::Array1D<vec3d> b(numMoovables);

	std::cerr << numMoovables << " of " << numVertices << " points moveable" << std::endl;

	// Build Matrix:
	for (int i1 = 0; i1 < numVertices; i1++) {
		
		if (sm->vList[i1].is_boundary_point) { // Point should be moved
			int lookup = indexLookup[i1];
			A[lookup][lookup] += 2.0;
		}

		for (unsigned int i2 = 0; i2 < sm->vList[i1].eList.size(); i2++) {

		}
	}

}


void SmoothMeshTNT(SimpleMesh* sm) {

	// Set the vertex marker 'is_boundary_point' true for all vertices that are to be moved:
	for (unsigned int i1 = 0; i1 < sm->tList.size(); i1++) {
		Triangle* t = sm->tList[i1];
		if (t->marker) {
			sm->vList[t->v0()].is_boundary_point = true;
			sm->vList[t->v1()].is_boundary_point = true;
			sm->vList[t->v2()].is_boundary_point = true;
		}
	}

	int numVertices = (int)sm->vList.size();
	// Compute map
	std::map<int, int> indexLookup;
	int numMoovables = 0;
	for (int i1 = 0; i1 < numVertices; i1++) {
		if (sm->vList[i1].is_boundary_point) {
			indexLookup.insert(std::pair<int, int>(i1, numMoovables));
			numMoovables++;
		}
	}


	//******************************************************************************************
	// Use LU decomposition to solve:

	TNT::Array2D<double> A(numMoovables, numMoovables);
	TNT::Array1D<vec3d> b(numMoovables);

	std::cerr << numMoovables << " of " << numVertices << " points moveable" << std::endl;

	for (int i1 = 0; i1 < numMoovables; i1++) { // F�r jede Zeile

		// �u�ere Summe (�ber alle Einzelfehler, d.h. �ber alle Punkte im Netz:)
		for (int i2 = 0; i2 < numVertices; i2++) {
			if (sm->vList[i2].is_boundary_point && indexLookup[i2] == i1) {
				// Ableitung nach dem Punkt, dessen Fehler wir momentan Berechnen:
				// f' = 2*P[i1] - 2/k*(N[0]...N[k-1])
				// - wobei k die Anzahl der Nachbarn ist.
				int k = (int)sm->vList[i2].eList.size();
				double k_d = (double)k;
				A[i1][i1] += 2.0;
				for (int i3 = 0; i3 < k; i3++) {
					int neighborIndex = (sm->vList[i2].eList[i3]->v0 == i2) ? sm->vList[i2].eList[i3]->v1 : sm->vList[i2].eList[i3]->v0;
					// Teste, ob der Nachbar beweglich ist.
					// - Wenn ja kommt er auf die linke Seite, sonst auf die rechte!
					if (sm->vList[neighborIndex].is_boundary_point) {
						int lookup = indexLookup[neighborIndex];
						A[i1][lookup] -= (2.0/k_d);
					} else {
						b[i1] += sm->vList[neighborIndex].c * (2.0/k_d);
					}
				}
			} else {  // Der Punkt, nachdem abgeleitet wird, kommt nur in den Nachbarn vor.
				// f' = 2/k�*(N[0]...N[k-1]) - 2/k*P[i1]
				// - wobei k die Anzahl der Nachbarn ist.
				bool currentPointIsAffected = false;
				int k = (int)sm->vList[i2].eList.size();
				double k_d = (double)k;
				double k_d_square = k*k;
				for (int i3 = 0; i3 < k; i3++) {
					int neighborIndex = (sm->vList[i2].eList[i3]->v0 == i2) ? sm->vList[i2].eList[i3]->v1 : sm->vList[i2].eList[i3]->v0;
					if (sm->vList[neighborIndex].is_boundary_point)
						if (indexLookup[neighborIndex] == i1)
							currentPointIsAffected = true;
				}
				if (currentPointIsAffected) { // Der Punkt 'i1' der gerade betrachtet wird ist ein Nachbar von 'i2':
					if (sm->vList[i2].is_boundary_point) { // 'i1' ist Nachbar des ebenfalls beweglichen Punktes 'i2':
						int index_i2 = indexLookup[i2];
						A[i1][index_i2] -= (2.0/k_d);
					} else {
						b[i1] += sm->vList[i2].c * (2.0/k_d);
					}
					// F�ge auch die restlichen Nachbargewichte ein:
					for (int i3 = 0; i3 < k; i3++) {
						int neighborIndex = (sm->vList[i2].eList[i3]->v0 == i2) ? sm->vList[i2].eList[i3]->v1 : sm->vList[i2].eList[i3]->v0;
						if (sm->vList[neighborIndex].is_boundary_point) {
							int index_neighborIndex = indexLookup[neighborIndex];
							A[i1][index_neighborIndex] += (2.0/k_d_square);
						} else {
							b[i1] -= sm->vList[neighborIndex].c * (2.0/k_d_square);
						}
					}
				}
			}
		}
	}

	JAMA::LU<double> lu(A);

	TNT::Array1D<double> current_b(numMoovables);
	TNT::Array1D<double> current_x(numMoovables);

	vec3d* best_pos = new vec3d[numMoovables];

	// solve for x:
	for (int i1 = 0; i1 < numMoovables; i1++) current_b[i1] = b[i1].x;
	current_x = lu.solve(current_b);
	for (int i1 = 0; i1 < numMoovables; i1++) best_pos[i1].x = current_x[i1];
	// solve for y:
	for (int i1 = 0; i1 < numMoovables; i1++) current_b[i1] = b[i1].y;
	current_x = lu.solve(current_b);
	for (int i1 = 0; i1 < numMoovables; i1++) best_pos[i1].y = current_x[i1];
	// solve for z:
	for (int i1 = 0; i1 < numMoovables; i1++) current_b[i1] = b[i1].z;
	current_x = lu.solve(current_b);
	for (int i1 = 0; i1 < numMoovables; i1++) best_pos[i1].z = current_x[i1];

	//******************************************************************************************



	for (int i1 = 0; i1 < numVertices; i1++) {
		if (sm->vList[i1].is_boundary_point) {
			int index = indexLookup[i1];
			sm->vList[i1].c = best_pos[index];
			sm->vList[i1].c.print();
		}
	}

	delete[] best_pos;

}
*/

