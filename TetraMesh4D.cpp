#include "TetraMesh4D.h"

// ****************************************************************************************

void TetraMesh4D<float>::readMeshFromBinary(char* filename) {
	std::ifstream fin(filename, std::ifstream::binary);

	if (!fin.good()) {
		std::cerr << "Error reading file in 'TetraMesh4D<T>::readMeshFromBinary(char* filename)'" << std::endl;
		return;
	}

	char type;
	UInt numTetra;
	// read header
	fin.read((char*)&type, 1);
	fin.read((char*)&numTetra, sizeof(UInt));

	std::cerr << "Reading " << numTetra << " Tetras" << std::endl;

	tets.resize(numTetra);

	UInt size1Tetrahedron = sizeof(Tetrahedron);
	fin.read((char*)&tets[0], numTetra*size1Tetrahedron);

	std::cerr << "Read " << (UInt)tets.size() << " Tetras" << std::endl;

	fin.close();

	// Set min, max:
	for (UInt i1 = 0; i1 < numTetra; i1++) {
		t_min = m_min(t_min, tets[i1].v0.w);
		t_min = m_min(t_min, tets[i1].v1.w);
		t_min = m_min(t_min, tets[i1].v2.w);
		t_min = m_min(t_min, tets[i1].v3.w);
		t_max = m_max(t_max, tets[i1].v0.w);
		t_max = m_max(t_max, tets[i1].v1.w);
		t_max = m_max(t_max, tets[i1].v2.w);
		t_max = m_max(t_max, tets[i1].v3.w);
	}

}

// ****************************************************************************************

void TetraMesh4D<float>::writeMeshToBinary(char* filename) {
	std::ofstream fout(filename, std::ofstream::binary);

	if (!fout.good()) {
		std::cerr << "Error writing file in 'TetraMesh4D<T>::writeMeshToBinary(char* filename)'" << std::endl;
		return;
	}

	UInt numTetra = (UInt)tets.size();

	char type = 1;
	// write header
	fout.write((char*)&type, 1);
	fout.write((char*)&numTetra, sizeof(UInt));

	if (fout.bad()) {
		std::cerr << "Error writing file in 'TetraMesh4D<T>::writeMeshToBinary(char* filename)'" << std::endl;
	}

	// write tetrahedra
	UInt size1Tetrahedron = sizeof(Tetrahedron);
	fout.write((char*)&tets[0], numTetra*size1Tetrahedron);
	fout.close();

	std::cerr << "Wrote 4D Tetrahedra Mesh to file '" << filename << "' (" << numTetra << " Tetras)" << std::endl;
}

// ****************************************************************************************

void TetraMesh4D<double>::writeMeshToBinary(char* filename) {
	std::ofstream fout(filename, std::ofstream::binary);

	UInt numTetra = (UInt)tets.size();

	char type = 2;
	// write header
	fout.write((char*)&type, 1);
	fout.write((char*)&numTetra, sizeof(UInt));

	// write tetrahedra
	UInt size1Tetrahedron = sizeof(Tetrahedron);
	fout.write((char*)&tets[0], numTetra*size1Tetrahedron);

	fout.close();

	std::cerr << "Wrote 4D Tetrahedra Mesh to file '" << filename << "' (" << numTetra << " Tetras)" << std::endl;
}

// ****************************************************************************************
