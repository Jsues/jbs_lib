#include <algorithm>
#include <list>

#include "MultiResMesh.h"
#include "operators/computeVertexNormals.h"
#include "operators/getVertexFan.h"
#include "operators/testColinear.h"
#include "FileIO/MeshWriter.h"
#include "timer.h"


//******************************************************************************************

bool findNonCoLinear2(const SimpleMesh* sm, int valence, const std::vector<int>& fan, vec3i& bv, double eps) {
	// Find 3 non-colinear vertices in the fan:
	for (bv.x = 0; (bv.x < valence); bv.x++) {
		for (bv.y = bv.x+1; (bv.y < valence); bv.y++) {
			for (bv.z = bv.y+1; (bv.z < valence); bv.z++) {
				if (!areColinear(sm->vList[fan[bv.x]].c, sm->vList[fan[bv.y]].c, sm->vList[fan[bv.z]].c, eps)) {
					//std::cerr << bv << " are not colinear" << std::endl;
					return true;
				}
			}
		}
	}

	return false;
}

//******************************************************************************************


MultiResMesh::MultiResMesh(SimpleMesh* sm) : m_sm(sm), m_max_depth(0) {
}


//******************************************************************************************


MultiResMesh::~MultiResMesh() {
	for (UInt i1 = 0; i1 < vertexLookUpTables.size(); i1++) {
		delete[] vertexLookUpTables[i1];
	}
	for (UInt i1 = 0; i1 < meshStack.size(); i1++) {
		delete meshStack[i1];
	}
}


//******************************************************************************************


void MultiResMesh::buildMeshHierarchy(UInt max_depth, double lambda, SimpleMesh* secondMesh) {

	m_max_depth = max_depth;

	SimpleMesh* current = m_sm;

	for (UInt level = 0; level < m_max_depth; level++) {

		JBS_Timer t;

		t.log("Computing set of removable vertices");

		UInt numV = (UInt)current->vList.size();
		vec3d* normals = computeVertexNormals(current);

		double* areas = new double[numV];
		double* curvatures = new double[numV];
		double max_area = 0; double max_curvature = 0;

		for (UInt i1 = 0; i1 < numV; i1++) {
			double area = getAreaAtFan(current, i1);
			double curvature = getCurvatureAtFan(current, normals[i1], i1);
			current->vList[i1].bool_flag = false;
			areas[i1] = area;
			curvatures[i1] = curvature;
			if (area > max_area) max_area = area;
			if (curvature > max_curvature) max_curvature = curvature;
		}

		std::vector<p_v_i> pri_queue;
		pri_queue.reserve(numV);
		for (UInt i1 = 0; i1 < numV; i1++) {
			double weight = (lambda)*areas[i1]/max_area + (1-lambda)*curvatures[i1]/max_curvature;
			// Multiply weight by Vertex valence to remove low valence vertices first.
			weight *= current->vList[i1].getNumN();
			pri_queue.push_back(p_v_i(weight, i1));
		}
		std::sort(pri_queue.begin(), pri_queue.end());

		delete[] areas;
		delete[] curvatures;
		delete[] normals;

		std::list<UInt> markForDelete;
		UInt numVafterDelete = numV;
		for (UInt i1 = 0; i1 < numV; i1++) {
			UInt id = pri_queue[i1].index;
			if (current->vList[id].getNumN() > 12) // do not delete
				continue;
			if (current->vList[id].bool_flag == false) { // vertex can be deleted -> mark for delete
				markForDelete.push_back(id);
				// mark 1-ring as not-deleteable
				for (UInt i2 = 0; i2 < current->vList[id].eList.size(); i2++) {
					int neighbor = current->vList[id].eList[i2]->getOther(id);
					current->vList[neighbor].bool_flag = true;
				}
				numVafterDelete--;
			}
		}

		// Set triangle marker to false again:
		for (UInt i1 = 0; i1 < current->tList.size(); i1++) {
			current->tList[i1]->marker = false;
		}

		t.log("Remove fans");

		SimpleMesh* new_sm = new SimpleMesh();
		int* vertexLookUpTable = new int[numV];
		int vertexCnt = 0;
		int vertexCnt2 = 0;
		for (UInt i1 = 0; i1 < numV; i1++) {
			if (current->vList[i1].bool_flag) {
				// Keep Vertex:
				vertexLookUpTable[i1] = vertexCnt;
				new_sm->insertVertex(current->vList[i1].c);
				vertexCnt++;
			} else {
				// Delete Vertex:
				vertexLookUpTable[i1] = numVafterDelete + vertexCnt2;
				vertexCnt2++;
			}
		}

		for (UInt i1 = 0; i1 < (UInt)current->getNumT(); i1++) {
			const Triangle* t = current->tList[i1];
			const int& v0 = t->v0();
			const int& v1 = t->v1();
			const int& v2 = t->v2();
			if (!current->vList[v0].bool_flag) continue;
			if (!current->vList[v1].bool_flag) continue;
			if (!current->vList[v2].bool_flag) continue;
			new_sm->insertTriangle(vertexLookUpTable[v0], vertexLookUpTable[v1], vertexLookUpTable[v2]);
		}



		std::cerr << "Level << " << (level+1) << " kept " << new_sm->getNumV() << " of " << current->getNumV() << " vertices" << std::endl;

		reduceInfo rInfo;
		rInfo.lastOldTriangle = new_sm->getNumT();

		// Retriangulate holes:
		for (UInt i1 = 0; i1 < numV; i1++) {
			if (!current->vList[i1].bool_flag) {
				reduceStep rStep;

				if (current->vList[i1].getNumN() == 0) {
					std::cerr << "Skip isolated point" << std::endl;
					continue;
				}

				// Vertex has been removed.
				std::vector<int> fan;
				bool boundary_v = getVertexFan(current, i1, fan);
				fan.pop_back();
				int valence = (int)fan.size();

				if (valence >= 3) { 

					// of all possible triangulations, use the one with the absolute smallest maximum dihidral angle:
					double smallest = std::numeric_limits<double>::max();
					int sm_id = -1;
					for (int idx = 0; idx < valence; idx++) {
						int corner = fan[idx];
						double error = 0;
						for (int i2 = idx; i2 < idx+valence-3; i2++) {
							int t_v1 = fan[(i2+1)%valence];
							int t_v2 = fan[(i2+2)%valence];
							int t_v3 = fan[(i2+3)%valence];
							vec3d n0 = (current->vList[corner].c-current->vList[t_v1].c)^(current->vList[corner].c-current->vList[t_v2].c);
							if (n0.squaredLength() > 0) n0.normalize();
							vec3d n1 = (current->vList[corner].c-current->vList[t_v2].c)^(current->vList[corner].c-current->vList[t_v3].c);
							if (n1.squaredLength() > 0) n1.normalize();
							error += (1-(n0|n1));
						}
						if (error < smallest) {
							smallest = error;
							sm_id = idx;
						}
					}

					if (boundary_v) { // check for sqrt-3 configs and overfolding boundary vertices
						for (int i2 = sm_id; i2 < sm_id+valence-2; i2++) {
							int t_v0 = fan[sm_id];
							int t_v1 = fan[(i2+1)%valence];
							int t_v2 = fan[(i2+2)%valence];
							vec3d normal_of_new_tri = (current->vList[t_v0].c-current->vList[t_v1].c)^(current->vList[t_v0].c-current->vList[t_v2].c);
							normal_of_new_tri.normalize();

							Edge* e = current->vList[t_v0].eList.getEdge(t_v0, t_v1);
							if (e != NULL) {
								Triangle* t = e->tList[0];
								vec3d n;
								if (t != NULL) n = t->getNormal();
								if ((n|normal_of_new_tri) < -0.3) {
									//std::cerr << "Skip insertion of overfolded boundary triangle" << std::endl;
									continue;
								}
							}
							e = current->vList[t_v1].eList.getEdge(t_v1, t_v2);
							if (e != NULL) {
								Triangle* t = e->tList[0];
								vec3d n;
								if (t != NULL) n = t->getNormal();
								if ((n|normal_of_new_tri) < -0.3) {
									//std::cerr << "Skip insertion of overfolded boundary triangle" << std::endl;
									continue;
								}
							}
							e = current->vList[t_v2].eList.getEdge(t_v2, t_v0);
							if (e != NULL) {
								Triangle* t = e->tList[0];
								vec3d n;
								if (t != NULL) n = t->getNormal();
								if ((n|normal_of_new_tri) < -0.3) {
									//std::cerr << "Skip insertion of overfolded boundary triangle" << std::endl;
									continue;
								}
							}

							if (new_sm->getTriangle(vertexLookUpTable[t_v0], vertexLookUpTable[t_v1], vertexLookUpTable[t_v2]) == NULL) // Avoid inserting tris in sqrt-3 configs.
								new_sm->insertTriangle(vertexLookUpTable[t_v0], vertexLookUpTable[t_v1], vertexLookUpTable[t_v2]);
						}
					} else {
						for (int i2 = sm_id; i2 < sm_id+valence-2; i2++) {
							int t_v1 = fan[(i2+1)%valence];
							int t_v2 = fan[(i2+2)%valence];
							new_sm->insertTriangle(vertexLookUpTable[fan[sm_id]], vertexLookUpTable[t_v1], vertexLookUpTable[t_v2]);
						}
					}
				}

				rStep.isBoundaryVertex = boundary_v;
				for (UInt i2 = 0; i2 < (UInt)fan.size(); i2++) {
					rStep.fan.push_back(vertexLookUpTable[fan[i2]]);
				}

				double fan_circ = 0;
				for (int i2 = 0; i2 < valence; i2++) {
					fan_circ += current->vList[fan[i2]].c.dist(current->vList[fan[(i2+1)%valence]].c);
				}
				double eps = (fan_circ/valence) / 1e9;


				vec3i bv;
				bool foundBasis = findNonCoLinear2(new_sm, valence, rStep.fan, bv, eps);

				if (foundBasis) { // We found a basis
					rStep.basis.x = rStep.fan[bv.x];
					rStep.basis.y = rStep.fan[bv.y];
					rStep.basis.z = rStep.fan[bv.z];
				} else {
					// Expand neighborhood:
					std::set<int> fanLevel2;
					for (UInt i2 = 0; i2 < (UInt)fan.size(); i2++) {
						int v = fan[i2];
						fanLevel2.insert(v);
						std::vector<int> fan2;
						getVertexFan(current, v, fan2);
						for (UInt i3 = 0; i3 < (UInt)fan2.size(); i3++) {
							if (current->vList[fan2[i3]].bool_flag)
								fanLevel2.insert(fan2[i3]);
						}
					}
					std::vector<int> fanLevel2_v;
					for (std::set<int>::const_iterator it = fanLevel2.begin(); it != fanLevel2.end(); ++it) {
						fanLevel2_v.push_back(*it);
					}
					foundBasis = findNonCoLinear2(current, (int)fanLevel2_v.size(), fanLevel2_v, bv, eps);

					if (!foundBasis) {
						std::cerr << "No basis found in level 2" << std::endl;
						continue;
					}

					rStep.basis.x = vertexLookUpTable[fanLevel2_v[bv.x]];
					rStep.basis.y = vertexLookUpTable[fanLevel2_v[bv.y]];
					rStep.basis.z = vertexLookUpTable[fanLevel2_v[bv.z]];

				}


				vec3d e0 = new_sm->vList[rStep.basis.x].c-new_sm->vList[rStep.basis.y].c;
				vec3d e1 = new_sm->vList[rStep.basis.x].c-new_sm->vList[rStep.basis.z].c;
				vec3d e2 = e0^e1;

				Matrix3D<double> bas(e0, e1, e2);
				// Express 'v0' in the basis
				vec3d diff = current->vList[i1].c - new_sm->vList[rStep.basis.x].c;
				rStep.local_coords = bas.vecTrans(diff);

				rInfo.rSteps.push_back(rStep);
			}
		}


		if (secondMesh != NULL) {
			SimpleMesh* sm_xx = new SimpleMesh;
			for (UInt i1 = 0; i1 < numV; i1++) {
				if (current->vList[i1].bool_flag)
					sm_xx->insertVertex(secondMesh->vList[i1].c);
			}
			for (UInt i1 = 0; i1 < (UInt)new_sm->getNumT(); i1++) {
				Triangle* t = new_sm->tList[i1];
				sm_xx->insertTriangle(t->v0(), t->v1(), t->v2());
			}
			secondMesh = sm_xx;
			std::stringstream ss;
			ss << "second_" << level << ".off";
			MeshWriter::writeOFFFile(ss.str().c_str(), sm_xx);
		}

		//delete[] vertexLookUpTable;

		meshStack.push_back(new_sm);
		reduceInfoStack.push_back(rInfo);
		vertexLookUpTables.push_back(vertexLookUpTable);

		current = new_sm;
	}


}


//******************************************************************************************


SimpleMesh* MultiResMesh::refineMesh(const SimpleMesh* sm, int level) {

	SimpleMesh* new_sm = new SimpleMesh;
	// Insert points
	for (int i1 = 0; i1 < sm->getNumV(); i1++) {
		new_sm->insertVertex(sm->vList[i1].c);
	}
	// Insert triangles
	for (int i1 = 0; i1 < reduceInfoStack[level].lastOldTriangle; i1++) {
		const Triangle* t = sm->tList[i1];
		new_sm->insertTriangle(t->v0(), t->v1(), t->v2());
	}

	for (UInt i1 = 0; i1 < reduceInfoStack[level].rSteps.size(); i1++) {
		const reduceStep& rStep = reduceInfoStack[level].rSteps[i1];

		// Recover vertex positions
		vec3d basvec0 = (sm->vList[rStep.basis[0]].c-sm->vList[rStep.basis[1]].c);
		vec3d basvec1 = (sm->vList[rStep.basis[0]].c-sm->vList[rStep.basis[2]].c);
		vec3d basvec2 = basvec0^basvec1;
		Matrix3D<double> bas(basvec0, basvec1, basvec2);
		Matrix3D<double> bas_inv = bas;
		bas_inv.invert();

		vec3d newdisp = bas_inv.vecTrans(rStep.local_coords);
		vec3d newpos = sm->vList[rStep.basis[0]].c + newdisp;
		new_sm->insertVertex(newpos);

		// Re-Triangulate the fan.
		int newVert = new_sm->getNumV()-1;

		UInt valence = (UInt)rStep.fan.size();
		for (UInt i2 = 0; i2 < valence-1; i2++) {
			new_sm->insertTriangle(newVert, rStep.fan[i2], rStep.fan[i2+1]);
		}
		if (!rStep.isBoundaryVertex) {
			new_sm->insertTriangle(newVert, rStep.fan[valence-1], rStep.fan[0]);
		}
	}
	return new_sm;

	//UInt numUndoSteps = (UInt)colInfoStack[level].size();

	//for (UInt i1 = 0; i1 < numUndoSteps; i1++) {
	//	colInfoStack[level][i1].undoCollaps(sm);
	//}
}


//******************************************************************************************


SimpleMesh* MultiResMesh::resortRefinedMesh(const SimpleMesh* sm_refined, int level) {

	SimpleMesh* sm_refined_resorted = new SimpleMesh();
	int numV = sm_refined->getNumV();

	std::vector<int> inverseVertexLookUpTable(numV);
	std::vector<vec3d> old_pos(numV);
	for (int i1 = 0; i1 < numV; i1++) {
		inverseVertexLookUpTable[vertexLookUpTables[level][i1]] = i1;
		old_pos[i1] = sm_refined->vList[i1].c;
	}

	for (int i1 = 0; i1 < numV; i1++) {
		int oldIndex = vertexLookUpTables[level][i1];
		sm_refined_resorted->insertVertex(old_pos[oldIndex]);
	}

	SimpleMesh* sm_level_n_m_1 = getLevel(level);
	// Copy topology from previous mesh
	for (int i1 = 0; i1 < sm_level_n_m_1->getNumT(); i1++) {
		Triangle* t = sm_level_n_m_1->tList[i1];
		sm_refined_resorted->insertTriangle(t->v0(), t->v1(), t->v2());
	}

	return sm_refined_resorted;
}


//******************************************************************************************
