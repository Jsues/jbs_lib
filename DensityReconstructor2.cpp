#include "DensityReconstructor2.h"


#ifndef M_PI
#define M_PI
#endif

//******************************************************************************************

DensityReconstructor2::DensityReconstructor2(float sigma, float search_radius_) {
	kdTree = NULL;
	dataPts = NULL;
	queryPt = annAllocPt(3);
	size_kDtree = -1;

	search_radius = search_radius_;
	search_radius_square = search_radius*search_radius;
	scale = sigma;
	scale_square = scale*scale;
	sigma_square = scale / 2.0f;
	std::cerr << "Search-Radius: " << search_radius_ << " Scale: " << scale << std::endl;
	
	CVs = NULL;
	dists = NULL;
	nnIdx = NULL;
}

//******************************************************************************************

DensityReconstructor2::~DensityReconstructor2(void) {

	std::cerr << "destroying DensityReconstructor2...";
	annDeallocPt(queryPt);

	if (nnIdx != NULL)
		delete[] nnIdx;

	if (dists != NULL)
		delete[] dists;

	if (dataPts != NULL)
		annDeallocPts(dataPts);

	if (kdTree != NULL)
		delete kdTree;

	if (CVs != NULL)
		delete[] CVs;

	std::cerr << "done" << std::endl;
}

//******************************************************************************************

void DensityReconstructor2::setPointCloud(PointCloud<vec3f>* pc_) {

	size_kDtree = pc_->getNumPts();

	const vec3f* pts = pc_->getPoints();

	dataPts = annAllocPts(size_kDtree, 3);
	for (int i1 = 0; i1 < size_kDtree; i1++) {
		dataPts[i1][0] = pts[i1][0];
		dataPts[i1][1] = pts[i1][1];
		dataPts[i1][2] = pts[i1][2];
	}

	std::cerr << "Building kd-tree...";
	kdTree = new ANNkd_tree(dataPts, size_kDtree, 3);
	std::cerr << "done" << std::endl;

	// Allocate array for NN-Search:
	nnIdx = new ANNidx[size_kDtree];						// allocate near neigh indices
	dists = new ANNdist[size_kDtree];						// allocate near neighbor dists

}

//******************************************************************************************

void DensityReconstructor2::computeNeighborTensorData(int numN) {

	if (size_kDtree < 0) {
		std::cerr << "Error! Add pointcloud before computing neighbors!, EXIT" << std::endl;
		exit(EXIT_FAILURE);
	}

	CVs = new Matrix3D<float>[size_kDtree];

	if (CVs == NULL) {
		std::cerr << "Not enough memory for computeNeighborTensorData" << std::endl;
		exit(EXIT_FAILURE);
	}

	float avg_spacing = 0;

	std::cerr << "Building neighborhood info...";
	for (int i1 = 0; i1 < size_kDtree; i1++) {

		queryPt[0] = dataPts[i1][0]; queryPt[1] = dataPts[i1][1]; queryPt[2] = dataPts[i1][2];
		kdTree->annkSearch(queryPt, numN, nnIdx, dists);

		float a = 0, b = 0, c = 0, e = 0, f = 0, i = 0;

		vec3f center(0,0,0);
		for (int i2 = 1; i2 < numN; i2++) center += vec3f((float)dataPts[nnIdx[i2]][0], (float)dataPts[nnIdx[i2]][1], (float)dataPts[nnIdx[i2]][2]);
		center /= (float)(numN-1);

		for (int i2 = 1; i2 < numN; i2++) {
			vec3f diff = center - vec3f((float)dataPts[nnIdx[i2]][0], (float)dataPts[nnIdx[i2]][1], (float)dataPts[nnIdx[i2]][2]);

			if (i2 <= 6) avg_spacing += diff.length();

			a += diff.x*diff.x;
			b += diff.x*diff.y;
			c += diff.x*diff.z;
			e += diff.y*diff.y;
			f += diff.y*diff.z;
			i += diff.z*diff.z;
		}
		Matrix3D<float> tmp(a, b, c, b, e, f, c, f, i);
		//tmp.divide((float)(numN-1));

		if (false) {
			if (! tmp.calcEValuesAndVectors() ) {
				exit(EXIT_FAILURE);
			}

			vec3f ev0 = tmp.getEV(0);
			ev0.normalize();
			vec3f ev1 = tmp.getEV(1);
			ev1.normalize();
			vec3f ev2 = tmp.getEV(2);
			ev2.normalize();

			CVs[i1] = Matrix3D<float>(ev0.x, ev1.x, ev2.x,
								  	  ev0.y, ev1.y, ev2.y,
									  ev0.z, ev1.z, ev2.z);

			CVs[i1].invert();

			// Compute extends:
			vec3f min_e = vec3f(10e10f);
			vec3f max_e = vec3f(-10e10f);
			for (int i2 = 0; i2 < numN; i2++) {
				vec3f diff((float)(queryPt[0] - dataPts[nnIdx[i2]][0]), (float)(queryPt[1] - dataPts[nnIdx[i2]][1]), (float)(queryPt[2] - dataPts[nnIdx[i2]][2]));
				vec3f diff_trans = CVs[i1].vecTrans(diff);
				if (diff_trans.x > max_e.x) max_e.x = diff_trans.x;
				if (diff_trans.y > max_e.y) max_e.y = diff_trans.y;
				if (diff_trans.z > max_e.z) max_e.z = diff_trans.z;
				if (diff_trans.x < min_e.x) min_e.x = diff_trans.x;
				if (diff_trans.y < min_e.y) min_e.y = diff_trans.y;
				if (diff_trans.z < min_e.z) min_e.z = diff_trans.z;
			}
			float facx = (max_e-min_e).x;
			float facy = (max_e-min_e).y;
			float facz = (max_e-min_e).z;

			// Normalize factors:
            facy = facy/facx;
			facz = facz/facx;
			facx = 1.0f;
			facy = 1;
			facz = 1;

			// Assure they are not too flat:
			if (facy < 0.3f) facy = 0.3f;
			if (facz < 0.3f) facz = 0.3f;
			ev0 /= facx;
			ev1 /= facy;
			ev2 /= facz;

			CVs[i1] = Matrix3D<float>(ev0.x, ev0.y, ev0.z,
									  ev1.x, ev1.y, ev1.z,
									  ev2.x, ev2.y, ev2.z);

			//CVs[i1].transpose();
			//CVs[i1].setID();
			CVs[i1].invert();

			//CVs[i1].a[0] /= facx;
			//CVs[i1].a[3] /= facx;
			//CVs[i1].a[6] /= facx;
			//CVs[i1].a[1] /= facy;
			//CVs[i1].a[4] /= facy;
			//CVs[i1].a[7] /= facy;
			//CVs[i1].a[2] /= facz;
			//CVs[i1].a[5] /= facz;
			//CVs[i1].a[8] /= facz;

			//CVs[i1].a[0] = 1;
			//CVs[i1].a[1] = 0;
			//CVs[i1].a[2] = 0;
			//CVs[i1].a[3] = 0;
			//CVs[i1].a[4] = 1;
			//CVs[i1].a[5] = 0;
			//CVs[i1].a[6] = 0;
			//CVs[i1].a[7] = 0;
			//CVs[i1].a[8] = 1;

			//(CVs[i1].vecTrans(ev0)).print();
			//(CVs[i1].vecTrans(ev1)).print();
			//(CVs[i1].vecTrans(ev2)).print();
			//std::cerr << CVs[i1].determinant() << std::endl;
			//if (i1 > 5)	exit(1);

		} else {

			if (! tmp.calcEValuesAndVectors() ) {
				exit(EXIT_FAILURE);
			}

			vec3f ev0 = tmp.getEV(0);
			vec3f ev1 = tmp.getEV(1);
			vec3f ev2 = tmp.getEV(2);
			float eval0 = tmp.getEValue(0);
			float eval1 = tmp.getEValue(1);
			float eval2 = tmp.getEValue(2);

			eval1 = 1;
			eval2 /= eval0;
			eval0 = 1;

			const float croptoval = 0.2f;


			if (eval1 < croptoval) eval1 = croptoval;
			if (eval2 < croptoval) eval2 = croptoval;

			Matrix3D<float>  Q(ev0.x, ev0.y, ev0.z,
							   ev1.x, ev1.y, ev1.z,
							   ev2.x, ev2.y, ev2.z);
			Matrix3D<float> Q2 = Q;
			Q.transpose();

			Matrix3D<float> EV(eval0, 0, 0,
							   0, eval1, 0,
							   0, 0, eval2);

			CVs[i1] = Q*(EV*Q2);
			CVs[i1].invert();
			//CVs[i1].setID();

			//tmp.print();
			//CVs[i1].print();
			//std::cerr << "-------------" << std::endl;

		}
		//CVs[i1] = Matrix3D<float>(1, 0, 0,   0, 1, 0,   0, 0, 1);
	}

	avg_spacing /= (size_kDtree*6);
	std::cerr << "Average spacing is " << avg_spacing << std::endl;

}

//******************************************************************************************

returnHelper DensityReconstructor2::getTargetValueEllipticalAt(float x, float y, float z) const {

	returnHelper ret;

	// Find all points in the radius 'search_radius'
	queryPt[0] = x; queryPt[1] = y; queryPt[2] = z; 
	
	// Get all points in the radius 'search_radius'
	int num_pts = kdTree->annkFRSearch(queryPt, search_radius_square, 0);		// Get number of points within radius
	// No points in the vicinity, return
	if (num_pts == 0) {
		return ret;
	}
	kdTree->annkFRSearch(queryPt, search_radius_square, num_pts, nnIdx, dists);

	vec3f grad(0,0,0);
	Matrix3D<float> hessian(0,0,0,0,0,0,0,0,0);
	float F = 0;

	// Sum up densities of influence points:
	for (int i1 = 0; i1 < num_pts; i1++) {
		const int& index = nnIdx[i1];

		const vec3f& closest = vec3f((float)dataPts[index][0], (float)dataPts[index][1], (float)dataPts[index][2]);
		const vec3f& dist_vec = vec3f(x,y,z)-closest;

		if (CVs == NULL) {

			const float& the_exp = getEXP((float)-dists[i1]/scale);
			//const float& the_exp = getEXPapprox((float)sqrt(dists[i1]));
			F += the_exp;

			grad += dist_vec*the_exp;

			Matrix3D<float> dist_vec_cross = Matrix3D<float>(dist_vec, dist_vec);
			//dist_vec_cross.mult(expDIVscale);

			dist_vec_cross.a[0] -= sigma_square;
			dist_vec_cross.a[4] -= sigma_square;
			dist_vec_cross.a[8] -= sigma_square;
			dist_vec_cross.mult(the_exp);
			hessian.add(dist_vec_cross);
		} else {
			Matrix3D<float> A = CVs[index];
			//Matrix3D<float> A(1,0,0,  0,1,0,  0,0,1);
			Matrix3D<float> A_t = A; A_t.transpose();
			Matrix3D<float> B = A+A_t;


			const float& the_exp = getEXP((float)-(A.vecTrans(dist_vec)|dist_vec)/scale);
			F += the_exp;

			vec3f dir = B.vecTrans(dist_vec);
			grad += dir*the_exp;

			Matrix3D<float> dist_vec_cross = Matrix3D<float>(dir, dir);

			for (int i = 0; i < 9; i++) {
				dist_vec_cross.a[i] -= 2*B.a[i]*sigma_square;
			}
			//dist_vec_cross.a[0] -= sigma_square;
			//dist_vec_cross.a[4] -= sigma_square;
			//dist_vec_cross.a[8] -= sigma_square;
			dist_vec_cross.mult(the_exp);
			hessian.add(dist_vec_cross);
		}
	}

	if (!hessian.calcEValuesAndVectors()) {
		hessian.print();
		exit(EXIT_FAILURE);
	}

	ret.density = F;
	ret.gradient = grad;
	ret.curvatures = vec3f(hessian.getEValue(0), hessian.getEValue(1), hessian.getEValue(2));
	ret.e0 = hessian.getEV(0);
//#define TESTSECONDBIGGEST // doesn't work.
#ifdef TESTSECONDBIGGEST
	if (fabs(ret.curvatures[0])*0.5 < fabs(ret.curvatures[1])) {
		if (ret.curvatures[0] > 0 && ret.curvatures[1] < 0) {
			ret.e0 = hessian.getEV(1);
			float a = ret.curvatures[1];
			ret.curvatures[1] = ret.curvatures[0];
			ret.curvatures[0] = a;
		}
	}
#endif
	ret.value = ret.e0 | grad;

    return ret;
}

//******************************************************************************************

//******************************************************************************************

returnHelper2 DensityReconstructor2::getTargetValueEllipticalAtver2(float x, float y, float z) {

	returnHelper2 ret;

	// Find all points in the radius 'search_radius'
	queryPt[0] = x; queryPt[1] = y; queryPt[2] = z; 
	
	// Get all points in the radius 'search_radius'
	int num_pts = kdTree->annkFRSearch(queryPt, search_radius_square, 0);		// Get number of points within radius

	kdTree->annkFRSearch(queryPt, search_radius_square, num_pts, nnIdx, dists);

	// No points in the vicinity, return
	if (num_pts == 0) {
		return ret;
	}

	int index;
	vec3f grad(0,0,0);
	Matrix3D<float> hessian(0,0,0,0,0,0,0,0,0);
	float F = 0;
	float the_exp;
	vec3f closest;

	// Sum up densities of influence points:
	for (int i1 = 0; i1 < num_pts; i1++) {
		index = nnIdx[i1];

		closest = vec3f((float)dataPts[index][0], (float)dataPts[index][1], (float)dataPts[index][2]);

		vec3f dist_vec = vec3f(x,y,z)-closest;

		the_exp = (float)exp(-dists[i1]/scale);
		F += the_exp;
		grad += dist_vec * (-2.0f*the_exp/scale);
		Matrix3D<float> dist_vec_cross = Matrix3D<float>(dist_vec, dist_vec);
		dist_vec_cross.mult(4.0f*the_exp/scale_square);
		float id_entries = -2.0f*the_exp/scale;
		Matrix3D<float> id = Matrix3D<float>(id_entries, 0, 0,
												0, id_entries, 0,
												0, 0, id_entries);
		hessian.add(id);
		hessian.add(dist_vec_cross);

	}

	if (!hessian.calcEValuesAndVectors()) {
		hessian.print();
		exit(EXIT_FAILURE);
	}

	ret.density = F;
	ret.gradient = grad;
	ret.curvatures = vec3f(hessian.getEValue(0), hessian.getEValue(1), hessian.getEValue(2));
	ret.e0 = hessian.getEV(0);
	ret.value = ret.e0 | grad;

	float p = ((hessian.getEValue(0)-hessian.getEValue(1))*(hessian.getEValue(0)-hessian.getEValue(1))) / (hessian.getEValue(0)*hessian.getEValue(0)+hessian.getEValue(1)*hessian.getEValue(1));

    return ret;
}

//******************************************************************************************

