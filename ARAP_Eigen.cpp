#include "ARAP_Eigen.h"

void ARAP_computeMatrixEigen(SimpleMesh* sm, SimpleMesh* urshape, PrecomputedMatrixEigen& pm) {

	UInt numOfPoints = (UInt)sm->getNumV();

	pm.vertexLookup.clear();
	pm.vertexLookup.resize(numOfPoints);
	pm.unconstrained  = 0;
	for (UInt i1 = 0; i1 < numOfPoints; i1++) {
		pm.vertexLookup[i1] = pm.unconstrained;
		if (!sm->vList[i1].bool_flag) {
			pm.unconstrained++;
		}
	}

	std::cerr << "unconstrained points: " << pm.unconstrained << std::endl;
	pm.A.resize(pm.unconstrained, pm.unconstrained);
	std::vector<Eigen::Triplet<double>> entries;

	int cnt = 0;
	for (UInt i1 = 0; i1 < numOfPoints; i1++) {
		if (!sm->vList[i1].bool_flag) { // point is unconstrained

			entries.push_back(Eigen::Triplet<double>(cnt, cnt, urshape->vList[i1].param.x));

			for (UInt i2 = 0; i2 < sm->vList[i1].eList.size(); i2++) {
				Edge* e = sm->vList[i1].eList[i2];
				int other = e->getOther(i1);
				if (!sm->vList[other].bool_flag) { // other one is unconstrained, instert into system matrix
					entries.push_back(Eigen::Triplet<double>(cnt, pm.vertexLookup[other], -urshape->vList[i1].eList[i2]->cotangent_weight));
				}
			}
			cnt++;
		}
	}

	pm.A.setFromTriplets(entries.begin(), entries.end());
	pm.solverA.compute(pm.A);

}

void ARAP_computeMatrixEigenSimpleUniform(SimpleMesh* sm, PrecomputedMatrixEigen& pm) {
    
    UInt numOfPoints = (UInt)sm->getNumV();
    
    pm.vertexLookup.clear();
    pm.vertexLookup.resize(numOfPoints);
    pm.unconstrained  = 0;
    for (UInt i1 = 0; i1 < numOfPoints; i1++) {
        pm.vertexLookup[i1] = pm.unconstrained;
        if (!sm->vList[i1].bool_flag) {
            pm.unconstrained++;
        }
    }
    
    std::cerr << "unconstrained points: " << pm.unconstrained << std::endl;
    pm.A.resize(pm.unconstrained, pm.unconstrained);
    std::vector<Eigen::Triplet<double>> entries;
    
    int cnt = 0;
    for (UInt i1 = 0; i1 < numOfPoints; i1++) {
        if (!sm->vList[i1].bool_flag) { // point is unconstrained
            
            entries.push_back(Eigen::Triplet<double>(cnt, cnt, sm->vList[i1].getNumN()));
            
            for (UInt i2 = 0; i2 < sm->vList[i1].eList.size(); i2++) {
                Edge* e = sm->vList[i1].eList[i2];
                int other = e->getOther(i1);
                if (!sm->vList[other].bool_flag) { // other one is unconstrained, instert into system matrix
                    entries.push_back(Eigen::Triplet<double>(cnt, pm.vertexLookup[other], -1.0f));
                }
            }
            cnt++;
        }
    }
    
    pm.A.setFromTriplets(entries.begin(), entries.end());
    pm.solverA.compute(pm.A);
    
}


//******************************************************************************************

void ARAP_Deform_with_given_factorizationEigen(SimpleMesh* sm, SimpleMesh* urshape, PrecomputedMatrixEigen& pm, UInt numInnerIter) {

	UInt numOfPoints = (UInt)sm->getNumV();

	//JBS_Timer t;


	//t.log("alloc memory");
	std::vector< SquareMatrixND<vec3d> > rots_vec(numOfPoints);
	SquareMatrixND<vec3d>* rots = &rots_vec[0];
	std::vector< double > rhs_vec(3*numOfPoints);

	Eigen::VectorXd rhs_eigen_x;
    rhs_eigen_x.resize(pm.unconstrained);
	Eigen::VectorXd rhs_eigen_y;
    rhs_eigen_y.resize(pm.unconstrained);
	Eigen::VectorXd rhs_eigen_z;
    rhs_eigen_z.resize(pm.unconstrained);


	for (UInt ARAP_iter = 0; ARAP_iter < numInnerIter; ARAP_iter++) {
		//**********************************************************
		// Find ideal rotations
		//**********************************************************
		//std::cerr << "estimating rotations..." << "                                \r";
		//t.log("estimating rotations");
		FindOptimalRotations(sm, urshape, rots_vec);

		//**********************************************************
		// Find vertex positions which match rotations best
		//**********************************************************
		//t.log("build rhs");
		int cnt = 0;
//#pragma omp parallel for
		for (int i1 = 0; i1 < (int)numOfPoints; i1++) {
			if (!sm->vList[i1].bool_flag) { // point is unconstrained:
				vec3d rhs(0,0,0);
				//vec3d p_i = sm->vList[i1].c;

				const vec3d& p_0 = urshape->vList[i1].c;

				for (UInt i2 = 0; i2 < sm->vList[i1].eList.size(); i2++) {
					Edge* e = sm->vList[i1].eList[i2];
					const int other = e->getOther(i1);

					const double& w_ij = urshape->vList[i1].eList[i2]->cotangent_weight;

					if (sm->vList[other].bool_flag) { // other one is constrained, put to right side
						vec3d tmp = sm->vList[other].c;
						tmp *= w_ij;
						rhs += tmp;
					}
					//! Right-hand side
					SquareMatrixND<vec3d> rot = rots[i1]+rots[other];
					const vec3d& p_1 = urshape->vList[other].c;
					vec3d t = rot.vecTrans(p_0-p_1);
					t *= (w_ij / 2.0);
					t = (p_0-p_1)*w_ij; // LAPLACE ONLY
					rhs += t;
				}
				rhs_eigen_x[cnt] = rhs.x;
				rhs_eigen_y[cnt] = rhs.y;
				rhs_eigen_z[cnt] = rhs.z;
				cnt++;
			}
		}

		//t.log("solve");
		Eigen::VectorXd X_x = pm.solverA.solve(rhs_eigen_x);
		Eigen::VectorXd X_y = pm.solverA.solve(rhs_eigen_y);
		Eigen::VectorXd X_z = pm.solverA.solve(rhs_eigen_z);

		//t.log("update mesh vertices");
#pragma omp parallel for
		for (int i1 = 0; i1 < (int)numOfPoints; i1++) if (!sm->vList[i1].bool_flag) {
			const int& id = pm.vertexLookup[i1];

			sm->vList[i1].c.x = X_x[id];
			sm->vList[i1].c.y = X_y[id];
			sm->vList[i1].c.z = X_z[id];
		}
	}
	//t.print();

	return;
}


//******************************************************************************************

void Laplace_Deform_with_given_factorizationEigen(SimpleMesh* sm, SimpleMesh* urshape, PrecomputedMatrixEigen& pm) {

	UInt numOfPoints = (UInt)sm->getNumV();

	//JBS_Timer t;

	//t.log("alloc memory");
	Eigen::VectorXd rhs_eigen_x;
    rhs_eigen_x.resize(pm.unconstrained);
	Eigen::VectorXd rhs_eigen_y;
    rhs_eigen_y.resize(pm.unconstrained);
	Eigen::VectorXd rhs_eigen_z;
    rhs_eigen_z.resize(pm.unconstrained);

	int cnt = 0;
//#pragma omp parallel for
	for (int i1 = 0; i1 < (int)numOfPoints; i1++) {
		if (!sm->vList[i1].bool_flag) { // point is unconstrained:
			vec3d rhs(0,0,0);
			//vec3d p_i = sm->vList[i1].c;

			const vec3d& p_0 = urshape->vList[i1].c;

			for (UInt i2 = 0; i2 < sm->vList[i1].eList.size(); i2++) {
				Edge* e = sm->vList[i1].eList[i2];
				const int other = e->getOther(i1);

				const double& w_ij = urshape->vList[i1].eList[i2]->cotangent_weight;

				if (sm->vList[other].bool_flag) { // other one is constrained, put to right side
					vec3d tmp = sm->vList[other].c;
					tmp *= w_ij;
					rhs += tmp;
				}
				//! Right-hand side
				const vec3d& p_1 = urshape->vList[other].c;
				rhs += (p_0-p_1)*w_ij; // LAPLACE ONLY
			}
			rhs_eigen_x[cnt] = rhs.x;
			rhs_eigen_y[cnt] = rhs.y;
			rhs_eigen_z[cnt] = rhs.z;
			cnt++;
		}
	}

	//t.log("solve");
	Eigen::VectorXd X_x = pm.solverA.solve(rhs_eigen_x);
	Eigen::VectorXd X_y = pm.solverA.solve(rhs_eigen_y);
	Eigen::VectorXd X_z = pm.solverA.solve(rhs_eigen_z);

	//t.log("update mesh vertices");
#pragma omp parallel for
	for (int i1 = 0; i1 < (int)numOfPoints; i1++) if (!sm->vList[i1].bool_flag) {
		const int& id = pm.vertexLookup[i1];

		sm->vList[i1].c.x = X_x[id];
		sm->vList[i1].c.y = X_y[id];
		sm->vList[i1].c.z = X_z[id];
	}
	//t.print();

	return;
}

void Laplace_with_given_factorizationEigenUniform(SimpleMesh* sm, PrecomputedMatrixEigen& pm) {
    
    UInt numOfPoints = (UInt)sm->getNumV();
    
    //JBS_Timer t;
    
    //t.log("alloc memory");
    Eigen::VectorXd rhs_eigen_x;
    rhs_eigen_x.resize(pm.unconstrained);
    Eigen::VectorXd rhs_eigen_y;
    rhs_eigen_y.resize(pm.unconstrained);
    Eigen::VectorXd rhs_eigen_z;
    rhs_eigen_z.resize(pm.unconstrained);
    
    int cnt = 0;
    //#pragma omp parallel for
    for (int i1 = 0; i1 < (int)numOfPoints; i1++) {
        if (!sm->vList[i1].bool_flag) // point is unconstrained:
        {
            vec3d rhs(0,0,0);
            
            for (UInt i2 = 0; i2 < sm->vList[i1].eList.size(); i2++) {
                Edge* e = sm->vList[i1].eList[i2];
                const int other = e->getOther(i1);
                
                if (sm->vList[other].bool_flag) { // other one is constrained, put to right side
                    vec3d tmp = sm->vList[other].c;
                    rhs += tmp;
                }
            }
            rhs_eigen_x[cnt] = rhs.x;
            rhs_eigen_y[cnt] = rhs.y;
            rhs_eigen_z[cnt] = rhs.z;
            cnt++;
        }
    }
    
    //t.log("solve");
    Eigen::VectorXd X_x = pm.solverA.solve(rhs_eigen_x);
    Eigen::VectorXd X_y = pm.solverA.solve(rhs_eigen_y);
    Eigen::VectorXd X_z = pm.solverA.solve(rhs_eigen_z);
    
    //t.log("update mesh vertices");
#pragma omp parallel for
    for (int i1 = 0; i1 < (int)numOfPoints; i1++) if (!sm->vList[i1].bool_flag) {
        const int& id = pm.vertexLookup[i1];
        
        sm->vList[i1].c.x = X_x[id];
        sm->vList[i1].c.y = X_y[id];
        sm->vList[i1].c.z = X_z[id];
    }
    //t.print();
    
    return;
}


//******************************************************************************************

void ARAP_DeformEigen(SimpleMesh* sm, SimpleMesh* urshape, UInt numInnerIter) {

	PrecomputedMatrixEigen pm;
	ARAP_computeMatrixEigen(sm, urshape, pm);
	ARAP_Deform_with_given_factorizationEigen(sm, urshape, pm, numInnerIter);

}
