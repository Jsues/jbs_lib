CC = cl
CFLAGS = /ML /EHsc /GS /O2
INC = /I "./include/"

lib: Image.cpp
	$(CC) Image.cpp /c $(INC) $(CFLAGS)

clean:
	del *.obj

docu:
	doxygen DoxyFile.txt

