#include "ARAP_common.h"

//******************************************************************************************

void computeUrshapeWeightsUniform(SimpleMesh* urshape) {
	for (UInt i1 = 0; i1 < urshape->eList.size(); i1++) {
		urshape->eList[i1]->cotangent_weight = 1.0;
	}
	for (UInt i1 = 0; i1 < urshape->vList.size(); i1 ++) {
		urshape->vList[i1].param.x = urshape->vList[i1].getNumN();
	}
}

//******************************************************************************************

void computeUrshapeWeightsPositive(SimpleMesh* urshape) {
	// Compute cotangent weights of urshape:
	for (UInt i1 = 0; i1 < urshape->eList.size(); i1++) {
		urshape->eList[i1]->computeCotangentWeights();
		if (urshape->eList[i1]->cotangent_weight < 0) urshape->eList[i1]->cotangent_weight = 0;
	}
	for (UInt i1 = 0; i1 < urshape->vList.size(); i1 ++) {
		double sum = 0;
		for (UInt i2 = 0; i2 < urshape->vList[i1].getNumN(); i2++) sum += urshape->vList[i1].eList[i2]->cotangent_weight;
		urshape->vList[i1].param.x = sum;
	}
}

//******************************************************************************************

void computeUrshapeWeights(SimpleMesh* urshape) {
	// Compute cotangent weights of urshape:
	for (UInt i1 = 0; i1 < urshape->eList.size(); i1++) {
		urshape->eList[i1]->computeCotangentWeights();
	}
	for (UInt i1 = 0; i1 < urshape->vList.size(); i1 ++) {
		double sum = 0;
		for (UInt i2 = 0; i2 < urshape->vList[i1].getNumN(); i2++) sum += urshape->vList[i1].eList[i2]->cotangent_weight;
		urshape->vList[i1].param.x = sum;
	}
}

//******************************************************************************************

void computeUrshapeWeightsChordal(SimpleMesh* urshape) {
	// Compute cotangent weights of urshape:
	for (UInt i1 = 0; i1 < urshape->eList.size(); i1++) {
		urshape->eList[i1]->cotangent_weight = urshape->eList[i1]->getLength();
	}
	for (UInt i1 = 0; i1 < urshape->vList.size(); i1 ++) {
		double sum = 0;
		for (UInt i2 = 0; i2 < urshape->vList[i1].getNumN(); i2++) sum += urshape->vList[i1].eList[i2]->cotangent_weight;
		urshape->vList[i1].param.x = sum;
	}
}

//******************************************************************************************

void getEDefPerVertex(SimpleMesh* sm, SimpleMesh* urshape, SquareMatrixND<vec3d>* rots, std::vector<double>& errors) {

	UInt numOfPoints = (UInt)sm->getNumV();
	errors.resize(numOfPoints);

	for (UInt i1 = 0; i1 < numOfPoints; i1++) {
		double Efan = 0;
		const vec3d& p0 = sm->vList[i1].c;
		const vec3d& p_dash_0 = urshape->vList[i1].c;
		const SquareMatrixND<vec3d>& R = rots[i1];

		for (UInt i2 = 0; i2 < sm->vList[i1].getNumN(); i2++) {
			int other = sm->vList[i1].getNeighbor(i2, i1);
			const vec3d& p1 = sm->vList[other].c;
			const vec3d& p_dash_1 = urshape->vList[other].c;

			vec3d v = p0-p1;
			vec3d v_dash = p_dash_0-p_dash_1;

			vec3d trans = R.getTransposed().vecTrans(v);

			Efan += urshape->vList[i1].eList[i2]->cotangent_weight * trans.squaredDist(v_dash);
		}
		errors[i1] = Efan;
	}

}

//******************************************************************************************

double getEDef(SimpleMesh* sm, SimpleMesh* urshape, SquareMatrixND<vec3d>* rots) {
	double EDef = 0;

	std::vector<double> errors(sm->getNumV(), 0);
	getEDefPerVertex(sm, urshape, rots, errors);

	UInt numOfPoints = (UInt)sm->getNumV();
	for (UInt i1 = 0; i1 < numOfPoints; i1++) EDef += errors[i1];

	return EDef;
}

//******************************************************************************************

void FindOptimalRotations(SimpleMesh* sm, SimpleMesh* urshape, std::vector< SquareMatrixND<vec3d> >& rots_vec) {

	UInt numOfPoints = (UInt)sm->getNumV();
	rots_vec.resize(numOfPoints);
//	std::fill(rots_vec.begin(), rots_vec.end(), 0);

#pragma omp parallel for
	for (int i2 = 0; i2 < (int)numOfPoints; i2++) {
		//if (sm->vList[i2].bool_flag) continue;
		SquareMatrixND<vec3d> COV;
		UInt v0 = i2;

		//double scale1 = 0;
		//double scale2 = 0;

		for (UInt i3 = 0; i3 < (UInt)urshape->vList[v0].getNumN(); i3++) {
			UInt v1 = urshape->vList[v0].getNeighbor(i3, v0);
			SquareMatrixND<vec3d> tmp;
			const vec3d& p_i = urshape->vList[v0].c;
			const vec3d& p_j = urshape->vList[v1].c;
			const vec3d& p_dash_i = sm->vList[v0].c;
			const vec3d& p_dash_j = sm->vList[v1].c;

			tmp.addFromTensorProduct((p_i-p_j) , (p_dash_i-p_dash_j));

			//scale1 += p_i.dist(p_j);
			//scale2 += p_dash_i.dist(p_dash_j);

			const double& w_ij = urshape->vList[v0].eList[i3]->cotangent_weight;
			tmp *= w_ij;
			COV += tmp;
		}
		//COV.print();
		SquareMatrixND<vec3d> U, V;
		vec3d sigma;
		bool SVD_result = COV.SVD_decomp(U, sigma, V);
		if (SVD_result) {

			V.transpose();
			SquareMatrixND<vec3d> rot = U*V;

			//rot /= (scale1/scale2);

			double det = rot.getDeterminant();
			if (det < 0) {
				int sm_id = 0;
				double smallest = sigma[sm_id];
				for (UInt dd = 1; dd < 3; dd++) {
					if (sigma[dd] < smallest) {
						smallest = sigma[dd];
						sm_id = dd;
					}
				}
				// flip sign of entries in colums 'sm_id' in 'U'
				U.m[sm_id + 0] *= -1;
				U.m[sm_id + 3] *= -1;
				U.m[sm_id + 6] *= -1;
				rot = U*V;
			}
			rot.transpose();
			rots_vec[i2] = rot;

		} else { // SVD failed!
			std::cerr << "SVD failed" << std::endl;
			rots_vec[i2].setToIdentity();
		}

		//if (i2 == 10) exit(1);
		//std::cerr << "---" << std::endl;
		//rots[i2].print();
	}

}

//******************************************************************************************
