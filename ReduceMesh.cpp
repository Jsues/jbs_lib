#include "ReduceMesh.h"
#include <iostream>


//******************************************************************************************

//! Computes the cotangens of x.
double JBS_cot(double x) {
	return cos(x)/sin(x);
}

//******************************************************************************************
//********************************** REDUCE MESH *******************************************
//******************************************************************************************


void ReduceMesh::reduceTriangle(void) {

	// 1.) Compute reduction error for each triangle in the mesh:
	int numT = getNumT();
	for (int i1 = 0; i1 < numT; i1++) {
		float red_error = getTriangleReduceError(i1);
	}
}

//******************************************************************************************

float ReduceMesh::getTriangleReduceError(int i1) {
	float error = 0.0f;

	Triangle* t = tList[i1];

	int v0 = (*t)[0];
	int v1 = (*t)[1];
	int v2 = (*t)[2];

	float error0 = getUmbrellaError(v0);
	float error1 = getUmbrellaError(v1);
	float error2 = getUmbrellaError(v2);

	return error;
}

//******************************************************************************************

float ReduceMesh::getUmbrellaError(int v) {
	float error = 0.0f;

	// Compute centroid of fan:
	vec3d centroid(0,0,0);
	for (UInt i2 = 0; i2 < vList[v].eList.size(); i2++) {
		int neighbor;
		(vList[v].eList[i2]->v0 == v) ? neighbor = vList[v].eList[i2]->v1 : vList[v].eList[i2]->v0;
		centroid += vList[neighbor].c;
	}
	centroid /= float(vList[v].eList.size());

	error = (float) (centroid - vList[v].c).squaredLength();

	return error;
}

//******************************************************************************************

float ReduceMesh::Taubin(double kpb, double lambda, bool allatonce) {

	if (old_pos == NULL) {
		std::cerr << "Error, init first!!" << std::endl;
		init();
	}

	if (!boundary_vertices_computed)
		computeBoundaryVertices();

	UInt num_pts = (UInt)vList.size();

	double mue = 1.0f/(kpb-(1.0f/lambda));

//	std::cout << "Start taubin kpb: " << kpb << " lambda: " << lambda << " mue: " << mue << std::endl;

	//for (UInt i1 = 0; i1 < num_pts; i1++) {
	//	std::cout << i1 << std::endl;
	//	old_pos[i1] = vList[i1].c;
	//}

	// Copy current vertex positions:
	Umbrella(lambda, allatonce, false);
	Umbrella(mue, allatonce, false);

	float total_move = 0.0;
	//for (UInt i1 = 0; i1 < num_pts; i1++) {
	//	vec3d dist = vList[i1].c - old_pos[i1];
	//	total_move += (float)dist.length();
	//}

//	std::cout << "Taubin done" << std::endl;
	return total_move;
}

//******************************************************************************************

float ReduceMesh::Curvature(double lambda, bool allatonce, bool print) {

	if (!boundary_vertices_computed)
		computeBoundaryVertices();

	UInt num_pts = (UInt)vList.size();

	for (UInt i1 = 0; i1 < num_pts; i1++) {
		vec3d v_new = CurvatureSmooth(i1, lambda);
		if (allatonce)
			new_pos[i1] = v_new;
		else {
			std::cerr << allatonce << std::endl;
			vList[i1].c = v_new;
		}
	}

	float total_move = 0.0;
	if (allatonce) {
		for (UInt i1 = 0; i1 < num_pts; i1++) {
			vec3d dist = vList[i1].c - new_pos[i1];
			total_move += (float)dist.length();
			vList[i1].c = new_pos[i1];
		}
	}

	//if (print)
	//	std::cerr << "Total move Umbrella: " << total_move << std::endl;

	return total_move;

}

//******************************************************************************************

float ReduceMesh::Umbrella(double lambda, bool allatonce, bool print) {

	if (!boundary_vertices_computed)
		computeBoundaryVertices();

	UInt num_pts = (UInt)vList.size();

	for (UInt i1 = 0; i1 < num_pts; i1++) {
		vec3d v_new = UmbrellaSmoth(i1, lambda);
		if (allatonce)
			new_pos[i1] = v_new;
		else {
			std::cerr << allatonce << std::endl;
			vList[i1].c = v_new;
		}
	}

	float total_move = 0.0;
	if (allatonce) {
		for (UInt i1 = 0; i1 < num_pts; i1++) {
			vec3d dist = vList[i1].c - new_pos[i1];
			total_move += (float)dist.length();
			vList[i1].c = new_pos[i1];
		}
	}

	//if (print)
	//	std::cerr << "Total move Umbrella: " << total_move << std::endl;

	return total_move;
}

//******************************************************************************************

float ReduceMesh::ScaleDependentUmbrella(double lambda, bool allatonce, bool print) {

	if (!boundary_vertices_computed)
		computeBoundaryVertices();

	UInt num_pts = (UInt)vList.size();

	for (UInt i1 = 0; i1 < num_pts; i1++) {
		vec3d v_new = ScaleDependentUmbrellaSmoth(i1, lambda);
		if (allatonce)
			new_pos[i1] = v_new;
		else {
			std::cerr << allatonce << std::endl;
			vList[i1].c = v_new;
		}
	}

	float total_move = 0.0;
	if (allatonce) {
		for (UInt i1 = 0; i1 < num_pts; i1++) {
			vec3d dist = vList[i1].c - new_pos[i1];
			total_move += (float)dist.length();
			vList[i1].c = new_pos[i1];
		}
	}

	//if (print)
	//	std::cerr << "Total move Umbrella: " << total_move << std::endl;

	return total_move;
}

//******************************************************************************************

float ReduceMesh::InverseUmbrella(int startwith, double lambda, bool allatonce) {

	if (!boundary_vertices_computed)
		computeBoundaryVertices();

	UInt num_pts = (UInt)vList.size();

	for (UInt i1 = startwith; i1 < num_pts; i1++) {
		vec3d v_new = InverseUmbrellaSmoth(i1);
		if (allatonce)
			new_pos[i1] = v_new;
		else
			vList[i1].c = v_new;
	}


	float total_move = 0.0;
	if (allatonce) {
		for (UInt i1 = 0; i1 < num_pts; i1++) {
			vec3d dist = vList[i1].c - new_pos[i1];
			total_move += (float)dist.length();
			vList[i1].c = new_pos[i1];
		}
	}

//	std::cerr << "Total move InverseUmbrella: " << total_move << std::endl;

	return total_move;
}

//******************************************************************************************

void ReduceMesh::Balance() {
	for (UInt i1 = 0; i1 < eList.size(); i1++) {
		ValenceBalance(i1);
		//if (i1%100 == 0)
		//	std::cerr << i1 << "/" << (int)eList.size() << std::endl;
	}
}

//******************************************************************************************

void ReduceMesh::printValenceStatistics() {
	int* val_stat = new int[20];
	int overflow = 0;
	for (UInt i1 = 0; i1 < 20; i1++)
		val_stat[i1] = 0;

	for (UInt i1 = 0; i1 < vList.size(); i1++) {
		int val = getValence(i1);
		if (val < 20)
			val_stat[val]++;
		else
			overflow++;
	}
	std::cerr << "Valence statistics:" << std::endl;
	for (UInt i1 = 0; i1 < 20; i1++) {
		std::cerr << "Valence " << i1 << ": " << val_stat[i1] << std::endl;
	}
	std::cerr << "Valence overflow: " << overflow << std::endl;

	delete[] val_stat;
}

//******************************************************************************************

void ReduceMesh::subdivideTriangle(Triangle* t) {

	int v0 = t->v0();
	int v1 = t->v1();
	int v2 = t->v2();

	//std::cerr << "v0: " << v0 << ", v1: " << v1 << ", v2: " << v2 << std::endl;


	// Compute barycenter of triangle:
	vec3d bary = (vList[v0].c + vList[v1].c + vList[v2].c) / 3.0;
	insertVertex(bary[0], bary[1], bary[2]);
	int vn = getNumV()-1;

	removeTriangle(t);
	insertTriangle(v0, v1, vn);
	insertTriangle(v1, v2, vn);
	insertTriangle(v2, v0, vn);
}

//******************************************************************************************

void ReduceMesh::subdivide() {

	UInt numT = (UInt)tList.size();
	UInt numE = (UInt)eList.size();
	UInt numV = (UInt)vList.size();


	for (UInt i1 = 0; i1 < numT; i1++)
		subdivideTriangle(tList[0]);

	// Now flip all old edges:
	for (UInt i1 = 0; i1 < numE; i1++)
		FlipEdge(eList[i1]);

	// We have new boundary vertices
	boundary_vertices_computed = false;

	// Now Optimize positions of the new Vertices:
	//vec3d* new_pos;
	//new_pos = new vec3d[vList.size()-numV];
	//computeBoundaryVertices();
	//for (int a = 0; a < 10; a++) {
	//	for (UInt i1 = numV; i1 < (UInt)vList.size(); i1++) {
	//		vec3d v_new = InverseUmbrellaSmoth(i1);
	//		new_pos[i1-numV] = v_new;
	//	}
	//	for (UInt i1 = numV; i1 < (UInt)vList.size(); i1++) {
	//		vList[i1].c = new_pos[i1-numV];
	//	}
	//}
	//delete[] new_pos;
}

//******************************************************************************************

vec3d ReduceMesh::CurvatureSmooth(int v, double lambda) {
	// Don't move boundary points
	if (vList[v].is_boundary_point)
		return vList[v].c;

	// Compute curvature normal;
	vec3d curvature_normal(0,0,0);

	double area = 0.0;
	double sum = 0.0;
	for (UInt i2 = 0; i2 < vList[v].eList.size(); i2++) {
		Edge* e = vList[v].eList[i2];
		int other_v = (e->v0 == v) ? e->v1 : e->v0;

		
		if (e->tList.size() != 2) { // We have excluded boundary vertices above, so this should NEVER happen!
			std::cerr << "ReduceMesh::CurvatureSmoth(...): Edge with " << (int) e->tList.size() << " Triangles found!" << std::endl;
			exit(EXIT_FAILURE);
		}

		//area += e->tList[0]->getArea()*0.5; // Each triangle will be processed twice!
		//area += e->tList[1]->getArea()*0.5; // Each triangle will be processed twice!

		//double angle0 = e->tList[0]->getAngleAtVertex(e->tList[0]->getOther(e->v0, e->v1));
		//double angle1 = e->tList[1]->getAngleAtVertex(e->tList[1]->getOther(e->v0, e->v1));
		//double factor = JBS_cot(angle0)+JBS_cot(angle1);

		e->computeCotangentWeights();
		double factor = e->cotangent_weight;

		curvature_normal += (vList[other_v].c-vList[v].c)*factor;

		sum += factor;
	}

	curvature_normal /= sum;
	//curvature_normal /= (4*area);

	return (vList[v].c + curvature_normal);

}

//******************************************************************************************

vec3d ReduceMesh::UmbrellaSmoth(int v, double lambda) {

	// Don't move boundary points
	if (vList[v].is_boundary_point)
		return vList[v].c;

	// Compute centroid of fan:
	vec3d centroid(0,0,0);
	for (UInt i2 = 0; i2 < vList[v].eList.size(); i2++) {
		int neighbor;
		(vList[v].eList[i2]->v0 == v) ? neighbor = vList[v].eList[i2]->v1 : neighbor = vList[v].eList[i2]->v0;
		centroid += vList[neighbor].c;
	}
	if (vList[v].eList.size() > 0) {
		centroid /= float(vList[v].eList.size());
	} else {
#ifdef DEBUG
		std::cerr << "ReduceMesh::UmbrellaSmoth: Ooops, none-connected vertex found! index=" << v << std::endl;
#endif
		centroid = vList[v].c;
	}

	vec3d d = (centroid - vList[v].c)*lambda;

//	d.print();

	return (vList[v].c + d);
}

//******************************************************************************************

vec3d ReduceMesh::ScaleDependentUmbrellaSmoth(int v, double lambda) {

	// Don't move boundary points
	if (vList[v].is_boundary_point)
		return vList[v].c;

	// Compute scale dependent centroid of fan:
	vec3d centroid(0,0,0);
	double total_edge_length = 0.0;
	for (UInt i2 = 0; i2 < vList[v].eList.size(); i2++) {
		int neighbor;
		(vList[v].eList[i2]->v0 == v) ? neighbor = vList[v].eList[i2]->v1 : neighbor = vList[v].eList[i2]->v0;
		vec3d dir = vList[neighbor].c-vList[v].c;
		double edge_length = dir.length();
		total_edge_length += edge_length;
		dir.normalize();
		centroid += dir;
	}
	if (vList[v].eList.size() > 0) {
		std::cerr << "total edge length: " << total_edge_length << std::endl;
		centroid /= (2.0/total_edge_length);
	} else {
		centroid = vList[v].c;
	}

	vec3d d = (centroid - vList[v].c)*lambda;
//	vec3d d = centroid*lambda;

	d.print();

	return (vList[v].c + d);
}

//******************************************************************************************

vec3d ReduceMesh::InverseUmbrellaSmothFALSE(int v) {
	vec3d ci(0.0, 0.0, 0.0);
	for (UInt i2 = 0; i2 < vList[v].eList.size(); i2++) {
		int neighbor;
		(vList[v].eList[i2]->v0 == v) ? neighbor = vList[v].eList[i2]->v1 : neighbor = vList[v].eList[i2]->v0;
		ci += vList[neighbor].c;
	}
	if (vList[v].eList.size() > 0)
		ci /= vList[v].eList.size();

	for (UInt i2 = 0; i2 < vList[v].eList.size(); i2++) {
		int neighbor;
		(vList[v].eList[i2]->v0 == v) ? neighbor = vList[v].eList[i2]->v1 : neighbor = vList[v].eList[i2]->v0;

		vec3d pj = vList[neighbor].c;
		vec3d cj(0.0, 0.0, 0.0);
		for (UInt i3 = 0; i3 < vList[neighbor].eList.size(); i3++) {
			int neighbor2;
			(vList[neighbor].eList[i3]->v0 == neighbor) ? neighbor2 = vList[neighbor].eList[i3]->v1 : neighbor2 = vList[neighbor].eList[i3]->v0;
			cj += vList[neighbor2].c;
		}
		cj /= vList[neighbor].eList.size();
		cj -= pj;
		cj /= vList[neighbor].eList.size();

		ci -= cj;
	}

    return ci;
}

//******************************************************************************************

vec3d ReduceMesh::InverseUmbrellaSmoth(int v) {

	// Don't move boundary points
	if (vList[v].is_boundary_point)
		return vList[v].c;


	vec3d pi;
	double valenz = double(vList[v].eList.size());

	// Compute centroid:
	vec3d ci(0.0, 0.0, 0.0);
	for (UInt i2 = 0; i2 < valenz; i2++) {
		int neighbor;
		(vList[v].eList[i2]->v0 == v) ? neighbor = vList[v].eList[i2]->v1 : neighbor = vList[v].eList[i2]->v0;
		ci += vList[neighbor].c;
	}
	if (vList[v].eList.size() > 0)
		ci /= valenz;

	pi = ci;

	double divider = 1.0;

	for (UInt i2 = 0; i2 < vList[v].eList.size(); i2++) {
		int neighbor;
		(vList[v].eList[i2]->v0 == v) ? neighbor = vList[v].eList[i2]->v1 : neighbor = vList[v].eList[i2]->v0;

		vec3d pj = vList[neighbor].c;
		double valenz_j = double(vList[neighbor].eList.size());
		vec3d cj(0.0, 0.0, 0.0);
		for (UInt i3 = 0; i3 < valenz_j; i3++) {
			int neighbor2;
			(vList[neighbor].eList[i3]->v0 == neighbor) ? neighbor2 = vList[neighbor].eList[i3]->v1 : neighbor2 = vList[neighbor].eList[i3]->v0;
			if (neighbor2 == v) {
				divider += 1.0/(valenz_j*valenz);
			} else {
				cj += vList[neighbor2].c;
			}
		}
		cj /= valenz_j;
		cj -= pj;
		cj /= valenz;

		pi -= cj;
	}

	pi /= divider;

    return pi;
}

//******************************************************************************************

bool ReduceMesh::ValenceBalance(int e) {

	// Edge lies on the boundary -> don't flip!
	if (eList[e]->tList.size() != 2)
		return false;

	int v0, v1, v2, v3;

	v0 = eList[e]->v0;
	v1 = eList[e]->v1;

	Triangle t0 = *(eList[e]->tList[0]);
	Triangle t1 = *(eList[e]->tList[1]);

	if ((t0[0] == v0 && t0[1] == v1) || (t0[1] == v0 && t0[0] == v1))
		v2 = t0[2];
	else if ((t0[2] == v0 && t0[1] == v1) || (t0[1] == v0 && t0[2] == v1))
		v2 = t0[0];
	else
		v2 = t0[1];


	if ((t1[0] == v0 && t1[1] == v1) || (t1[1] == v0 && t1[0] == v1))
		v3 = t1[2];
	else if ((t1[2] == v0 && t1[1] == v1) || (t1[1] == v0 && t1[2] == v1))
		v3 = t1[0];
	else
		v3 = t1[1];

	int val_v0 = getValence(v0);
	int val_v1 = getValence(v1);
	int val_v2 = getValence(v2);
	int val_v3 = getValence(v3);

	int current_valence_deriv = (int)fabs(float(val_v0 + val_v1 - val_v2 - val_v3));

	if (current_valence_deriv < 3) {
		FlipEdge(eList[e]);
	}

	return false;
}

//******************************************************************************************

UInt ReduceMesh::getValence(int v) {
	return (UInt) vList[v].eList.size();
}

//******************************************************************************************

void ReduceMesh::FlipEdge(Edge* e) {

	if (e->tList.size() != 2)
		return;

	Triangle* t0 = e->tList[0];
	Triangle* t1 = e->tList[1];

	int v0 = e->v0;
	int v1 = e->v1;
	int v2, v3;

	v2 = t0->getOther(v0, v1);
	v3 = t1->getOther(v0, v1);

	// Check for illegal 'tetrahedron-configuration'
	if (vList[v2].eList.getEdge(v2, v3) != NULL) {
		//std::cerr << "Tetrahedron-configuration; edge skipped" << std::endl; 
		return;
	}

	Edge* e02 = vList[v0].eList.getEdge(v0, v2);
	Edge* e03 = vList[v0].eList.getEdge(v0, v3);
	Edge* e12 = vList[v1].eList.getEdge(v1, v2);
	Edge* e13 = vList[v1].eList.getEdge(v1, v3);

	if (e02 == NULL || e03 == NULL || e12 == NULL || e13 == NULL) {
		std::cerr << "ReduceMesh::FlipEdge(...): Error" << std::endl;
		return;
	}

	// Update triangles:
	if ((((*t0)[0] == v0) && ((*t0)[1] == v1)) || (((*t0)[1] == v0) && ((*t0)[2] == v1)) || (((*t0)[2] == v0) && ((*t0)[0] == v1))) {
		(*t0)[0] = v0;
		(*t0)[1] = v3;
		(*t0)[2] = v2;
		(*t1)[0] = v2;
		(*t1)[1] = v3;
		(*t1)[2] = v1;
	} else {
		(*t0)[0] = v2;
		(*t0)[1] = v3;
		(*t0)[2] = v0;
		(*t1)[0] = v1;
		(*t1)[1] = v3;
		(*t1)[2] = v2;
	}

	// Update vertices:
	vList[v0].eList.removeEdge(v0, v1);
	vList[v1].eList.removeEdge(v0, v1);
	vList[v2].eList.insertEdge(e);
	vList[v3].eList.insertEdge(e);

	t0->e0 = e02;
	t0->e1 = e;
	t0->e2 = e03;
	t1->e0 = e12;
	t1->e1 = e;
	t1->e2 = e13;

	// Update edges:
	e->v0 = v2;
	e->v1 = v3;
	if (e03->tList[0] == t1)
		e03->tList[0] = t0;
	else
		e03->tList[1] = t0;
	if (e12->tList[0] == t0)
		e12->tList[0] = t1;
	else
		e12->tList[1] = t1;

}

//******************************************************************************************

double ReduceMesh::CalculateVolume() {
	UInt num_T = (UInt)tList.size();

	double volume = 0.0;

	for (UInt i1 = 0; i1 < num_T; i1++) {
		Triangle* t = tList[i1];
		vec3d v1 = vList[t->v0()].c;
		vec3d v2 = vList[t->v1()].c;
		vec3d v3 = vList[t->v2()].c;

		volume += -v3[0]*v2[1]*v1[2] + v2[0]*v3[1]+v1[2] + v3[0]*v1[1]+v2[2] - v1[0]*v3[1]+v2[2] - v2[0]*v1[1]+v3[2] + v1[0]*v2[1]+v3[2];
	}

	volume /= 6;

	return volume;
}

//******************************************************************************************

double ReduceMesh::CalculateSurfaceArea() {

	UInt num_T = (UInt)tList.size();

	double area = 0.0;

	for (UInt i1 = 0; i1 < num_T; i1++) {
		Triangle* t = tList[i1];
		vec3d v1 = vList[t->v0()].c;
		vec3d v2 = vList[t->v1()].c;
		vec3d v3 = vList[t->v2()].c;

		area += (v1-v2 ^ v1-v3).length();
	}

	area /= 2.0;

	return area;
}

//******************************************************************************************

//******************************************************************************************

//******************************************************************************************

