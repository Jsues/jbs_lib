#include "ICP_3D.h"


#include "FileIO/PointCloudIO.h"


//******************************************************************************************

ICP_3D::ICP_3D() {
	model = NULL;
	data = NULL;
}

//******************************************************************************************

ICP_3D::~ICP_3D() {
	if (model != NULL)
		delete model;
}

//******************************************************************************************

void ICP_3D::setModelData(char* model_file) {

	PointCloudReader pcr;
	PointCloud<vec3f>* pc = pcr.readASCFile(model_file);

	int numV = pc->getNumPts();
	model = new kdPointCloud<vec3f>(numV);
	for (int i1 = 0; i1 < numV; i1++) {
		model->insertPoint(pc->getPoint(i1));
	}
	model->buildKdTree();


	// Compute model cog:
	for (int i1 = 0; i1 < numV; i1++) {
		model_cog += model->getPoint(i1);
	}
	model_cog /= (float)numV;

	delete pc;
}

//******************************************************************************************

void ICP_3D::setData(char* data_file) {
	PointCloudReader pcr;
	data = pcr.readASCFile(data_file);

	// Compute data cog:
	for (int i1 = 0; i1 < data->getNumPts(); i1++) {
		data_cog += data->getPoint(i1);
	}
	data_cog /= (float)data->getNumPts();
}

//******************************************************************************************

void ICP_3D::applyTransformToData(mat3f mat, vec3f trans) {

	if (data == NULL) {
		std::cerr << "ICP_3D::applyTransformToData(...)" << std::endl;
		std::cerr << "Error, you must specify a data point cloud first!" << std::endl;
		exit(EXIT_FAILURE);
	}

	vec3f* pts = data->getPoints();
	for (int i1 = 0; i1 < data->getNumPts(); i1++) {
		vec3f pt = pts[i1];
		pt = mat.vecTrans(pt) + trans;
		pts[i1] = pt;
	}

	data_cog = mat.vecTrans(data_cog) + trans;
}

//******************************************************************************************

void ICP_3D::writeCurrentDataPointCloud(char* filename) {
	PointCloudWriter<vec3f> pcw(data);
	pcw.writeASCFile(filename);
}

//******************************************************************************************

void ICP_3D::perform1ICPStep() {

	int N = data->getNumPts();

	mat3f CV;
	for (int i1 = 0; i1 < N; i1++) {
		vec3f pt_data = data->getPoint(i1);
		int closest_in_model = model->getNearestPoint(pt_data);
		vec3f pt_model = model->getPoint(closest_in_model);
		mat3f CV_current(pt_data-data_cog, pt_model-model_cog);
		CV.add(CV_current);
	}
	CV.divide((float)N);
    
	mat3f CV_trans = CV; CV_trans.transpose();

	float trace = CV.a[0]+CV.a[4]+CV.a[8];

	mat3f ID_trace(	-trace, 0, 0,
					0, -trace, 0,
					0, 0, -trace );

	mat3f sum = CV;
	sum.add(CV_trans);
	sum.add(ID_trace);

	// Use jacobi's method to compute the eigenvectors
	// Build 4x4 matrix NR-style:
	float** CV_4 = new float*[5];
	for(int i1 = 0; i1 < 5; i1++)
		CV_4[i1] = new float[5];
	float lambda[5];
	float** v = new float*[5];
	for(int i1 = 0; i1 < 5; i1++)
		v[i1] = new float[5];


	CV_4[1][1] = trace;
	CV_4[2][1] = CV.a[5]-CV_trans.a[5];
	CV_4[3][1] = CV.a[6]-CV_trans.a[6];
	CV_4[4][1] = CV.a[1]-CV_trans.a[1];
	CV_4[1][2] = CV_4[2][1];
	CV_4[2][2] = sum.a[0];
	CV_4[3][2] = sum.a[1]; 
	CV_4[4][2] = sum.a[2];
	CV_4[1][3] = CV_4[3][1];
	CV_4[2][3] = sum.a[3];
	CV_4[3][3] = sum.a[4]; 
	CV_4[4][3] = sum.a[5];
	CV_4[1][4] = CV_4[4][1];
	CV_4[2][4] = sum.a[6];
	CV_4[3][4] = sum.a[7]; 
	CV_4[4][4] = sum.a[8];

	int num_of_required_jabobi_rotations;

	if (!jacobi(CV_4, 4, lambda, v, &num_of_required_jabobi_rotations)) {
		std::cerr << "ICP_3D::perform1ICPStep()" << std::endl;
		std::cerr << "Error in call jacobi! EXIT" << std::endl;
		exit(EXIT_FAILURE);
	}

	int largest_index = 1;
	float largest = lambda[1];
	for (int i1 = 1; i1 <= 4; i1++) {
		if (lambda[i1] > largest) {
			largest = lambda[i1];
			largest_index = i1;
		}
	}

	Quaternion<float> quat(v[1][largest_index], v[2][largest_index], v[3][largest_index], v[4][largest_index]);
	mat3f opt_rot(quat);

	// Optimal translation:
	vec3f opt_transl = model_cog - opt_rot.vecTrans(data_cog);

	// Update data:
	applyTransformToData(opt_rot, opt_transl);

	//std::cerr << "---" << std::endl;
	//data_cog.print();
	//vec3f cog2;
	//for (unsigned int i1 = 0; i1 < data->getNumPts(); i1++)
	//	cog2 += data->getPoint(i1);
	//cog2 /= data->getNumPts();
	//cog2.print();
	//std::cerr << "---" << std::endl;
	//data_cog = cog2;


	writeCurrentDataPointCloud("first_it.asc");

}

//******************************************************************************************

