#include "CurvatureMesh.h"
#include "linInterpolate.h"
#include "operators/fitQuadricToPoints.h"


#include <set>


void CurvatureMesh::ComputeVertexCurvatures() {

	computeBoundaryVertices();

	int numV = getNumV();
	curvature = new double[numV];


	//* Compute initial normal estimates:
	vec3d* normals = new vec3d[numV];
	for (int i1 = 0; i1 < getNumT(); i1++) {
		Triangle* t = tList[i1];
		vec3d normal = t->getNormal();
		normals[t->v0()] += normal;
		normals[t->v1()] += normal;
		normals[t->v2()] += normal;
	}


	for (int i1 = 0; i1 < numV; i1++) {
		normals[i1].normalize();

		if (vList[i1].eList.size() > 0)
			curvature[i1] = ComputeCurvatureAt(i1, normals[i1]);
		else
			curvature[i1] = 0;

		if (curvature[i1] > max_curv)
			max_curv = curvature[i1];
		if (curvature[i1] < min_curv)
			min_curv = curvature[i1];
	}

	delete[] normals;

}


double CurvatureMesh::ComputeCurvatureAt(int i1, vec3d normal) {
	
	vec3d ref_point = vList[i1].c;
	std::vector<vec3d> points;

	if (vList[i1].eList.size() >= 5) {
		// Use 1-ring
		for (UInt i2 = 0; i2 < vList[i1].eList.size(); i2++) {
			int other_v = (vList[i1].eList[i2]->v0 == i1) ? vList[i1].eList[i2]->v1 : vList[i1].eList[i2]->v0;
			points.push_back(vList[other_v].c);
		}

	} else {
		// Use 2-ring
		std::vector<int> ring_1;
		std::set<int> ring_2;
		for (UInt i2 = 0; i2 < vList[i1].eList.size(); i2++) {
			int other_v = (vList[i1].eList[i2]->v0 == i1) ? vList[i1].eList[i2]->v1 : vList[i1].eList[i2]->v0;
			ring_1.push_back(other_v);
		}
		for (UInt i2 = 0; i2 < ring_1.size(); i2++) {
			int the_v = ring_1[i2];
			ring_2.insert(the_v);
			for (UInt i3 = 0; i3 < vList[the_v].eList.size(); i3++) {
				int other_v = (vList[the_v].eList[i3]->v0 == the_v) ? vList[the_v].eList[i3]->v1 : vList[the_v].eList[i3]->v0;
				ring_2.insert(other_v);
			}
		}
		for (std::set<int>::iterator it = ring_2.begin(); it != ring_2.end(); it++) {
			points.push_back(vList[*it].c);
			//std::cerr << *it << " "; (vList[*it].c).print();
		}
	}

	if (points.size() > 0) {

		std::cerr << i1 << std::endl;

		if (i1 >= 270 && i1 < 90000)
			return 0;

		Quadric<double> quad = fitQuadricToPoints(ref_point, points, normal);
		double curvature = quad.getMeanCurvature();
		return curvature;
	} else {
		return 0;
	}
}

LineSet CurvatureMesh::ComputeCurvatureIsoLines(double iso) {
	LineSet ls;

	if (curvature == NULL)
		ComputeVertexCurvatures();


	std::cerr << "Computing lineset for iso-value: " << iso << std::endl;


	for (int i1 = 0; i1 < getNumT(); i1++) {
		Triangle* t = tList[i1];
		int v0 = t->v0();
		int v1 = t->v1();
		int v2 = t->v2();

		if ((curvature[v0] > iso && curvature[v1] < iso) || (curvature[v0] < iso && curvature[v1] > iso))
			ls.push_back(LinInterpolate(vList[v0].c, vList[v1].c, curvature[v0], curvature[v1], iso));
		if ((curvature[v0] > iso && curvature[v2] < iso) || (curvature[v0] < iso && curvature[v2] > iso))
			ls.push_back(LinInterpolate(vList[v0].c, vList[v2].c, curvature[v0], curvature[v2], iso));
		if ((curvature[v2] > iso && curvature[v1] < iso) || (curvature[v2] < iso && curvature[v1] > iso))
			ls.push_back(LinInterpolate(vList[v2].c, vList[v1].c, curvature[v2], curvature[v1], iso));

	}

	return ls;
}


