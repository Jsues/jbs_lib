#include "include/operators/UmbrellaMinimizeSelection.h"

#include <map>

/*

#include "sparselib.h"

void UmbrellaMinimizeSelection(SimpleMesh* sm, std::map<int, int>& indexLookup, VECTOR_double& x_x, VECTOR_double& x_y, VECTOR_double& x_z) {

	int numVertices = (unsigned int)sm->vList.size();
	int numMoovables = (int) indexLookup.size();

	std::cerr << numMoovables << " of " << numVertices << " points moveable" << std::endl;

	// Verschiebe alle ausgew�hlten Punkte so, dass sie das Umbrella Kriterium minimieren:
	// Die Matrix ist quadratisch der Gr��e 'numMoovables'
	std::vector<double> val;					// Hier werden die "nicht null" Eintr�ge der Sparse Matrix gespeichert
	std::vector<int> col_index;					// Die "x" Koordinate des entsprechenden Eintrags in der Sparse Matrix
	std::vector<int> row_ptr;					// Gibt an, vor welchen Elementen ein Zeilen(=row)-Wechsel erfolgt.
	int matrix_entrys = 0;						// Z�hler f�r Matrix-Eintr�ge
	row_ptr.push_back(0);						// beim 0ten Element f�ngt die erste Matrixzeile an
	VECTOR_double b_x(numMoovables);			// Rechte Seite Matrix Gleichung
	VECTOR_double b_y(numMoovables);
	VECTOR_double b_z(numMoovables);

	std::vector<vec3d> b;						// UNSER L�sungsvektor
	b.reserve(numVertices);


	for (int i1 = 0; i1 < numMoovables; i1++) { // F�r jede Zeile der Matrix. Wir leiten nach v_i1 ab.

		double* row = new double[numMoovables];
		for (int i2 = 0; i2 < numMoovables; i2++)
			row[i2] = 0.0;

		for (int i2 = 0; i2 < numVertices; i2++) { // F�r jeden Vertex im Mesh:
			if (sm->vList[i2].is_boundary_point || indexLookup[i2] == i1) { // Wir leiten nach diesem Punk ab.
				row[i1] += 1.0;
                // Sortiere auch Nachbarn ein.
				int k = (int)sm->vList[i1].eList.size();
				for (int i3 = 0; i3 < k; i3++) {
					int indexInMesh = (sm->vList[i1].eList[i3]->v0 == i2) ? sm->vList[i1].eList[i3]->v1 : sm->vList[i1].eList[i3]->v0;
					// Testen, ob v_indexInMesh 'moveable ist'
					if (sm->vList[indexInMesh].is_boundary_point) {
						int indexInLookup = indexLookup[indexInMesh];
						row[indexInLookup] -= 1.0/((double)k);
					} else {
						b[i1] += (sm->vList[indexInMesh].c * 1.0/((double)k));
					}
				}
			} else {



			}
		}

		// Schreibe die aktuelle Zeile in die Matrix:
		for (int i2 = 0; i2 < numMoovables; i2++) {
			if (row[i2] != 0.0) {
				val.push_back(row[i2]);
				col_index.push_back(i2);		// Eingef�gt an der i1ten position in der aktuellen Zeile;
				matrix_entrys++;
			}
		}
		delete[] row;

		// Rechte Seite:
		b_x(i1) = b[i1].x;
		b_y(i1) = b[i1].y;
		b_z(i1) = b[i1].z;
		// Startwerte:
		x_x(i1) = sm->vList[i1].c.x;
		x_y(i1) = sm->vList[i1].c.y;
		x_z(i1) = sm->vList[i1].c.z;

		// Die i1te Zeile der Matrix ist gef�llt, f�ge 'matrix_entrys' in row_ptr ein:
		row_ptr.push_back(matrix_entrys);
	}

	CompRow_Mat_double A(numMoovables, numMoovables, (int)val.size(), &val[0], &row_ptr[0], &col_index[0]);

	DiagPreconditioner_double D(A);

	double tol = 0.0001;								// Convergence tolerance
	int maxit = 10000;									// Maximum iterations	
	int result = BiCGSTAB(A, x_x, b_x, D, maxit, tol);	// Solve system
	std::cout << "Result von BiCGSTAB: " << result << "Maxit: " << maxit << " tol: " << tol << std::endl;
	tol = 0.0001;										// Convergence tolerance
	maxit = 10000;										// Maximum iterations	
	result = BiCGSTAB(A, x_y, b_y, D, maxit, tol);		// Solve system
	std::cout << "Result von BiCGSTAB: " << result << "Maxit: " << maxit << " tol: " << tol << std::endl;
	tol = 0.0001;										// Convergence tolerance
	maxit = 10000;										// Maximum iterations	
	result = BiCGSTAB(A, x_z, b_z, D, maxit, tol);		// Solve system
	std::cout << "Result von BiCGSTAB: " << result << "Maxit: " << maxit << " tol: " << tol << std::endl;

	for (int i1 = 0; i1 < numMoovables; i1++) {
		std::cout << x_x(i1) << ", " << x_y(i1) << ", " << x_z(i1) << std::endl;
	}

}

void InverseUmbrellaMinimizeSelection(SimpleMesh* sm, std::map<int, int>& indexLookup, VECTOR_double& x_x, VECTOR_double& x_y, VECTOR_double& x_z) {

	int numVertices = (unsigned int)sm->vList.size();
	int numMoovables = (int) indexLookup.size();

	std::cerr << numMoovables << " of " << numVertices << " points moveable" << std::endl;

	// Verschiebe alle ausgew�hlten Punkte so, dass sie das Umbrella Kriterium minimieren:
	// Die Matrix ist quadratisch der Gr��e 'numMoovables'
	std::vector<double> val;					// Hier werden die "nicht null" Eintr�ge der Sparse Matrix gespeichert
	std::vector<int> col_index;					// Die "x" Koordinate des entsprechenden Eintrags in der Sparse Matrix
	std::vector<int> row_ptr;					// Gibt an, vor welchen Elementen ein Zeilen(=row)-Wechsel erfolgt.
	int matrix_entrys = 0;						// Z�hler f�r Matrix-Eintr�ge
	row_ptr.push_back(0);						// beim 0ten Element f�ngt die erste Matrixzeile an
	VECTOR_double b_x(numMoovables);			// Rechte Seite Matrix Gleichung
	VECTOR_double b_y(numMoovables);
	VECTOR_double b_z(numMoovables);

	std::vector<vec3d> b;			// L�sungsvektor
	b.reserve(numVertices);


	for (int i1 = 0; i1 < numMoovables; i1++) { // F�r jede Zeile

		double* row = new double[numMoovables];
		for (int i2 = 0; i2 < numMoovables; i2++)
			row[i2] = 0.0;

		// �u�ere Summe (�ber alle Einzelfehler, d.h. �ber alle Punkte im Netz:)
		for (int i2 = 0; i2 < numVertices; i2++) {
			if (sm->vList[i2].is_boundary_point && indexLookup[i2] == i1) {
				// Ableitung nach dem Punkt, dessen Fehler wir momentan Berechnen:
				// f' = 2*P[i1] - 2/k*(N[0]...N[k-1])
				// - wobei k die Anzahl der Nachbarn ist.
				int k = (int)sm->vList[i2].eList.size();
				row[i1] += 2.0;
				for (int i3 = 0; i3 < k; i3++) {
					int neighborIndex = (sm->vList[i2].eList[i3]->v0 == i2) ? sm->vList[i2].eList[i3]->v1 : sm->vList[i2].eList[i3]->v0;
					// Teste, ob der Nachbar beweglich ist.
					// - Wenn ja kommt er auf die linke Seite, sonst auf die rechte!
					if (sm->vList[neighborIndex].is_boundary_point) {
						int lookup = indexLookup[neighborIndex];
						row[lookup] -= (2.0/(double)k);
					} else {
						b[i1] += sm->vList[neighborIndex].c * (2.0/(double)k);
					}
				}
			} else {  // Der Punkt, nachdem abgeleitet wird, kommt nur in den Nachbarn vor.
				// f' = 2/k�*(N[0]...N[k-1]) - 2/k*P[i1]
				// - wobei k die Anzahl der Nachbarn ist.
				bool currentPointIsAffected = false;
				int k = (int)sm->vList[i2].eList.size();
				for (int i3 = 0; i3 < k; i3++) {
					int neighborIndex = (sm->vList[i2].eList[i3]->v0 == i2) ? sm->vList[i2].eList[i3]->v1 : sm->vList[i2].eList[i3]->v0;
					if (sm->vList[neighborIndex].is_boundary_point)
						if (indexLookup[neighborIndex] == i1)
							currentPointIsAffected = true;
				}
				if (currentPointIsAffected) { // Der Punkt 'i1' der gerade betrachtet wird ist ein Nachbar von 'i2':
					if (sm->vList[i2].is_boundary_point) { // 'i1' ist Nachbar des ebenfalls beweglichen Punktes 'i2':
						int index_i2 = indexLookup[i2];
						row[index_i2] -= (2.0/(double)k);
					} else {
						b[i1] += sm->vList[i2].c * (2.0/(double)k);
					}
					// F�ge auch die restlichen Nachbargewichte ein:
					for (int i3 = 0; i3 < k; i3++) {
						int neighborIndex = (sm->vList[i2].eList[i3]->v0 == i2) ? sm->vList[i2].eList[i3]->v1 : sm->vList[i2].eList[i3]->v0;
						if (sm->vList[neighborIndex].is_boundary_point) {
							int index_neighborIndex = indexLookup[neighborIndex];
							row[index_neighborIndex] += (2.0/(double)(k*k));
						} else {
							b[i1] -= sm->vList[neighborIndex].c * (2.0/(double)(k*k));
						}
					}
				}
			}
		}

		// Schreibe die aktuelle Zeile in die Matrix:
		for (int i2 = 0; i2 < numMoovables; i2++) {
			if (row[i2] != 0.0) {
				val.push_back(row[i2]);
				col_index.push_back(i2);		// Eingef�gt an der i1ten position in der aktuellen Zeile;
				matrix_entrys++;
			}
		}
		delete[] row;

		// Rechte Seite:
		b_x(i1) = b[i1].x;
		b_y(i1) = b[i1].y;
		b_z(i1) = b[i1].z;
		// Startwerte:
		x_x(i1) = sm->vList[i1].c.x;
		x_y(i1) = sm->vList[i1].c.y;
		x_z(i1) = sm->vList[i1].c.z;

		// Die i1te Zeile der Matrix ist gef�llt, f�ge 'matrix_entrys' in row_ptr ein:
		row_ptr.push_back(matrix_entrys);
	}

	CompRow_Mat_double A(numMoovables, numMoovables, (int)val.size(), &val[0], &row_ptr[0], &col_index[0]);

	DiagPreconditioner_double D(A);

	double tol = 0.0001;								// Convergence tolerance
	int maxit = 10000;									// Maximum iterations	
	int result = BiCGSTAB(A, x_x, b_x, D, maxit, tol);	// Solve system
	std::cout << "Result von BiCGSTAB: " << result << "Maxit: " << maxit << " tol: " << tol << std::endl;
	tol = 0.0001;										// Convergence tolerance
	maxit = 10000;										// Maximum iterations	
	result = BiCGSTAB(A, x_y, b_y, D, maxit, tol);		// Solve system
	std::cout << "Result von BiCGSTAB: " << result << "Maxit: " << maxit << " tol: " << tol << std::endl;
	tol = 0.0001;										// Convergence tolerance
	maxit = 10000;										// Maximum iterations	
	result = BiCGSTAB(A, x_z, b_z, D, maxit, tol);		// Solve system
	std::cout << "Result von BiCGSTAB: " << result << "Maxit: " << maxit << " tol: " << tol << std::endl;

	for (int i1 = 0; i1 < numMoovables; i1++) {
		std::cout << x_x(i1) << ", " << x_y(i1) << ", " << x_z(i1) << std::endl;
	}
}


void SmoothMesh(SimpleMesh* sm, FAIRING_METHOD method) {

	// Set the vertex marker 'is_boundary_point' true for all vertices that are to be moved:
	for (unsigned int i1 = 0; i1 < sm->tList.size(); i1++) {
		Triangle* t = sm->tList[i1];
		if (t->marker) {
			sm->vList[t->v0()].is_boundary_point = true;
			sm->vList[t->v1()].is_boundary_point = true;
			sm->vList[t->v2()].is_boundary_point = true;
		}
	}

	int numVertices = (int)sm->vList.size();
	// Compute map
	std::map<int, int> indexLookup;
	int numMoovables = 0;
	for (int i1 = 0; i1 < numVertices; i1++) {
		if (sm->vList[i1].is_boundary_point) {
			indexLookup.insert(std::pair<int, int>(i1, numMoovables));
			numMoovables++;
		}
	}

	VECTOR_double x_x(numMoovables, 0.00);			// L�sungsvektor x-Komponente
	VECTOR_double x_y(numMoovables, 0.00);			// L�sungsvektor y-Komponente
	VECTOR_double x_z(numMoovables, 0.00);			// L�sungsvektor z-Komponente

	if (method == INVERSE_UMBRELLA) {
		InverseUmbrellaMinimizeSelection(sm, indexLookup, x_x, x_y, x_z);
	} else if (method == UMBRELLA) {
		UmbrellaMinimizeSelection(sm, indexLookup, x_x, x_y, x_z);
	}

	for (int i1 = 0; i1 < numVertices; i1++) {
		if (sm->vList[i1].is_boundary_point) {
			int index = indexLookup[i1];
			sm->vList[i1].c = vec3d(x_x(index), x_y(index), x_z(index));
			sm->vList[i1].c.print();
		}
	}

}

*/