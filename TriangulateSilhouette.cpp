#include "TriangulateSilhouette.h"

#include "RandomStuff.h"

// returns whether the polyline is ordered clockwise
bool isClockwise(const PolyLine<vec2f>& pl) {
	float sum = 0;
	for (unsigned int i1 = 0; i1 < pl.points.size(); i1++) {
		vec2f p0 = pl.points[i1].c;
		vec2f p1 = pl.points[(i1+1)%pl.points.size()].c;
		sum += (p0.x*p1.y)*(p0.y*p1.x);
	}
	return (sum > 0);
}


PoissonDiscGrid::PoissonDiscGrid(float width, float height, float radius)
    : m_width(width)
    , m_height(height)
    , m_radius(radius)
    , m_cellSize(radius / sqrtf(2.0f)) // This will give us the edge length of the rectangle inscribed in the unit poisson disc.
    , m_cellWidth(ceilf(m_width / m_cellSize))
    , m_cellHeight(ceilf(m_height / m_cellSize))
    , m_grid(m_cellWidth * m_cellHeight, -1)
    {
    }
    
int PoissonDiscGrid::LinearIndex(const vec2i& index) const
{
    return index.y * m_cellWidth + index.x;
}
    
vec2i PoissonDiscGrid::Index(const vec2f& sample) const
{
    vec2i gridIndex(static_cast<int>(sample.x / m_cellSize), static_cast<int>(sample.y / m_cellSize));
    return gridIndex;
}
    
void PoissonDiscGrid::Insert(const vec2f& sample, int sampleIndex)
{
    vec2i gridIndex = Index(sample);
    int linearIndex = LinearIndex(gridIndex);
    assert(m_grid[linearIndex] == -1);
    m_grid[linearIndex] = sampleIndex;
    
}
    
bool PoissonDiscGrid::IsInside(const vec2f& sample) const
{
    return sample.x > 0.0f && sample.x < m_width && sample.y > 0.0f && sample.y < m_height;
}
    
bool PoissonDiscGrid::IsOccupied(const vec2i& gridIndex) const
{
    return GetSampleIndex(gridIndex) > -1;
}
    
int PoissonDiscGrid::GetSampleIndex(const vec2i& gridIndex) const
{
    return m_grid[LinearIndex(gridIndex)];
}
    
bool PoissonDiscGrid::IsValidIndex(const vec2i& gridIndex) const
{
    return gridIndex.x >= 0 && gridIndex.x < m_cellWidth && gridIndex.y >= 0 && gridIndex.y < m_cellHeight;
}
    
float PoissonDiscGrid::GetWidth() const
{
    return m_width;
}
    
float PoissonDiscGrid::GetHeight() const
{
    return m_height;
}
    
float PoissonDiscGrid::GetRadius() const
{
    return m_radius;
}

void PoissonDiscGrid::Print() const
{
    for (int y = 0; y < m_cellHeight; ++y) {
        for (int x = 0; x < m_cellWidth; ++x) {
            std::cout << (m_grid[LinearIndex(vec2i(x,y))] != -1 ? "X" : "O");
        }
        std::cout << std::endl;
    }
}

PoissonDiscSampler::PoissonDiscSampler(const vec2f& min, const vec2f& max, float radius, unsigned int maxNumCandidates)
: m_min(min)
, m_max(max)
, m_grid(max.x - min.x, max.y - min.y, radius)
, m_samples()
, m_activeList()
, m_maxNumCandidates(maxNumCandidates)
//, m_rd()
//, m_mt(m_rd())
, m_mt(42)
, m_random_radius(m_grid.GetRadius(), 2.0f * m_grid.GetRadius())
, m_random_angle(0.0, 2.0 * M_PI)
{
}
    
std::vector<vec2f> PoissonDiscSampler::GetSamples()
{
    m_samples.clear();
    m_activeList.clear();

    vec2f initialSample(m_grid.GetWidth() / 2.0f, m_grid.GetHeight() / 2.0f);
    m_grid.Insert(initialSample, m_samples.size());
    m_activeList.push_back(m_samples.size());
    m_samples.push_back(initialSample);
    
    float squaredRadius = m_grid.GetRadius() * m_grid.GetRadius();
    
    while (!m_activeList.empty())
    {
        std::uniform_int_distribution<int> randomIndexDitribution(0, static_cast<int>(m_activeList.size()-1));
        int activeIndex = randomIndexDitribution(m_mt);
        vec2f sample = m_samples[m_activeList[activeIndex]];
        bool isValidCandidate = false;
        for (unsigned int i = 0; i < m_maxNumCandidates; ++i)
        {
            // Get candidate in circular ring around current sample.
            vec2f candidate = GetCircleSampleBiased(sample);
            
            // Reject candidate when it is not inside the boundary.
            if (!m_grid.IsInside(candidate)) { continue; }
            
            // Reject candidate when it is too close to a neighbor.
            // We use squared radius to speed up the distance computation.
            if (IsCloseToNeighbors(candidate, squaredRadius)) { continue; }
            
            // Valid sample will be inserted into grid and active list.
            isValidCandidate = true;
            m_grid.Insert(candidate, m_samples.size());
            m_activeList.push_back(m_samples.size());
            m_samples.push_back(candidate);
            
            // We want to pick a new random sample from the active list and continue with that.
            break;
        }
        
        // We could not find a valid sample around our current sample so we do not want to try searching again.
        if (!isValidCandidate) {
            m_activeList.erase(m_activeList.begin() + activeIndex);
        }
    }
    
    // Computing samples was zero based so we translate back to proper space.
    for (std::vector<vec2f>::iterator it = m_samples.begin(); it != m_samples.end(); ++it)
    {
        vec2f& sample = *it;
        sample += m_min;
    }
    
    return m_samples;
}

vec2f PoissonDiscSampler::GetCircleSampleBiased(vec2f center)
{
    // Note that this will deliberately produce samples biased toward smaller radii.
    // This is will lead to a tighter packaging of our distribution.
    // In order to get uniformly distributed circle samples one should use the sqrt of the radius.
    float randR = m_random_radius(m_mt);
    float randT = m_random_angle(m_mt);
    
    return vec2f(randR * cosf(randT), randR * sinf(randT)) + center;
}

bool PoissonDiscSampler::IsCloseToNeighbor(const vec2f& sample, float squaredRadius, const vec2i& neighborIndex)
{
    if (!m_grid.IsValidIndex(neighborIndex) || !m_grid.IsOccupied(neighborIndex)) { return false; }
    auto neigborSampleIndex = m_grid.GetSampleIndex(neighborIndex);
    const vec2f& neighbor = m_samples[neigborSampleIndex];
    auto dist = sample.squareDist(neighbor);
    return dist < squaredRadius;
}
    
bool PoissonDiscSampler::IsCloseToNeighbors(const vec2f& sample, float squaredRadius)
{
    vec2i gridIndex = m_grid.Index(sample);
    
    // If we already have a sample in this cell there is no need to check the neighbors.
    if (m_grid.IsOccupied(gridIndex)) { return true; }
    
    //if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x - 2, gridIndex.y - 2))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x - 1, gridIndex.y - 2))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x    , gridIndex.y - 2))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x + 1, gridIndex.y - 2))) return true;
    //if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x + 2, gridIndex.y - 2))) return true;
    
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x - 2, gridIndex.y - 1))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x - 1, gridIndex.y - 1))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x    , gridIndex.y - 1))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x + 1, gridIndex.y - 1))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x + 2, gridIndex.y - 1))) return true;
    
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x - 2, gridIndex.y))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x - 1, gridIndex.y))) return true;
    //if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x    , gridIndex.y))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x + 1, gridIndex.y))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x + 2, gridIndex.y))) return true;
    
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x - 2, gridIndex.y + 1))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x - 1, gridIndex.y + 1))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x    , gridIndex.y + 1))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x + 1, gridIndex.y + 1))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x + 2, gridIndex.y + 1))) return true;
    
    //if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x - 2, gridIndex.y + 2))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x - 1, gridIndex.y + 2))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x    , gridIndex.y + 2))) return true;
    if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x + 1, gridIndex.y + 2))) return true;
    //if (IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x + 2, gridIndex.y + 2))) return true;
    
    // It is not necessary to check the corners of the 5x5 neighborhood of gridIndex
    assert(!IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x - 2, gridIndex.y - 2)));
    assert(!IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x + 2, gridIndex.y - 2)));
    assert(!IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x - 2, gridIndex.y + 2)));
    assert(!IsCloseToNeighbor(sample, squaredRadius, vec2i(gridIndex.x + 2, gridIndex.y + 2)));
    
    return false;
}

void getInnerPoints2(const PolyLine<vec2f>& pl, float targetEdgeLength, std::vector<vec2f>& innerPoints) {
    
    vec2f mmin(FLT_MAX);
    vec2f mmax(-FLT_MAX);
    
    std::vector<vec2f> points;
    for (unsigned int i1 = 0; i1 < pl.points.size(); i1++) {
        const vec2f& p = pl.points[i1].c;
        points.push_back(p);
        if (p.x < mmin.x) mmin.x = p.x;
        if (p.y < mmin.y) mmin.y = p.y;
        if (p.x > mmax.x) mmax.x = p.x;
        if (p.y > mmax.y) mmax.y = p.y;
    }
    points.push_back(points[0]);
    
    PoissonDiscSampler sampler(mmin, mmax, targetEdgeLength);
    std::vector<vec2f> candidate_pts = sampler.GetSamples();
    
    float sq_targetEdgeLength = targetEdgeLength*targetEdgeLength;
    
    std::vector<bool> flags(candidate_pts.size(), true);
    
    for (int i1 = 0; i1 < candidate_pts.size(); i1++)
    {
        const vec2f& p = candidate_pts[i1];
        
        // Check whether a boundary point is closeby
        for (unsigned int i2 = 0; i2 < points.size(); i2++) {
            const vec2f& o = points[i2];
            if (o.squareDist(p) < sq_targetEdgeLength) {
                flags[i1] = false;
            }
        }
    }
    
    int cnt = 0;
    
    for (int i1 = 0; i1 < candidate_pts.size(); i1++) {
        
        if (!flags[i1]) continue;
        
        cnt ++;
        
        const vec2f& candidatePoint = candidate_pts[i1];
        
        // check whether it is inside:
        int wn = wn_PnPoly(candidatePoint, &points[0], points.size()-1);
        if (wn != 0) innerPoints.push_back(candidatePoint);
    }
    std::cerr << "sq_targetEdgeLength: " << sq_targetEdgeLength << std::endl;
    std::cerr << cnt << " poisson samples out of " << candidate_pts.size() << std::endl;
    //	exit(1);
}



void getInnerPoints(const PolyLine<vec2f>& pl, float targetEdgeLength, std::vector<vec2f>& innerPoints) {

	vec2f mmin(FLT_MAX);
	vec2f mmax(-FLT_MAX);

	std::vector<vec2f> points;
	for (unsigned int i1 = 0; i1 < pl.points.size(); i1++) {
		const vec2f& p = pl.points[i1].c;
		points.push_back(p);
		if (p.x < mmin.x) mmin.x = p.x;
		if (p.y < mmin.y) mmin.y = p.y;
		if (p.x > mmax.x) mmax.x = p.x;
		if (p.y > mmax.y) mmax.y = p.y;
	}
	points.push_back(points[0]);

	int numSamples = (int)((mmax.x-mmin.x)*(mmax.y-mmin.y)*20);
	numSamples = (int)(numSamples/targetEdgeLength);

	RNG rng;
	rng.init(10,10,10,10);


	float sq_targetEdgeLength = targetEdgeLength*targetEdgeLength;


	std::vector<vec2f> candidate_pts;
	for (int i1 = 0; i1 < numSamples; i1++) candidate_pts.push_back(vec2f((float)rng.uniform(mmin.x, mmax.x), (float)rng.uniform(mmin.y, mmax.y)));
	std::vector<bool> flags(candidate_pts.size(), true);

	for (int i1 = 0; i1 < numSamples; i1++) {
		if (!flags[i1]) continue;

		const vec2f& p = candidate_pts[i1];

		// Check whether a boundary point is closeby
		for (unsigned int i2 = 0; i2 < points.size(); i2++) {
			const vec2f& o = points[i2];
			if (o.squareDist(p) < sq_targetEdgeLength) {
				flags[i1] = false;
			}
		}
		if (!flags[i1]) continue;
		// Mark all points in 'targetEdgeLength' invalid
		for (int i2 = 0; i2 < numSamples; i2++) {
			if (i2 == i1) continue;
            if (!flags[i2]) continue;
			const vec2f& o = candidate_pts[i2];
			if (o.squareDist(p) < sq_targetEdgeLength) {
				flags[i2] = false;
			}
		}
	}


	int cnt = 0;

	for (int i1 = 0; i1 < numSamples; i1++) {

		if (!flags[i1]) continue;

		cnt ++;

		const vec2f& candidatePoint = candidate_pts[i1];

		// check whether it is inside:
		int wn = wn_PnPoly(candidatePoint, &points[0], points.size()-1);
		if (wn != 0) innerPoints.push_back(candidatePoint);
	}
	std::cerr << "sq_targetEdgeLength: " << sq_targetEdgeLength << std::endl;
	std::cerr << cnt << " poisson samples out of " << numSamples << std::endl;
//	exit(1);
}

void triangulatePolyLine(const PolyLine<vec2f>& pl, std::vector<vec3f>& pts, std::vector<vec3i>& tris, float target_pt_dist, bool addSteinerPoints, bool useNewPoissonDiskSampling) {

	std::cerr << "pl.points.size(): " << pl.points.size() << std::endl;
    
    /*
    for (size_t i = 0; i < pl.points.size(); ++i) {
        std::cout << std::setprecision(std::numeric_limits<float>::max_digits10 + 1) << i << ":" << pl.points[i].c.x << " " << pl.points[i].c.y << std::endl;
        for (size_t j = i+1; j < pl.points.size(); ++j) {
            assert(pl.points[i].c.x != pl.points[j].c.x && pl.points[i].c.y != pl.points[j].c.y);
            if (pl.points[i].c.x == pl.points[j].c.x && pl.points[i].c.y == pl.points[j].c.y) {
                std::cout << "Duplicated points in polyline!" << std::endl;
            }
        }
    }
    */
    
    std::vector<vec2f> innerPoints;
    if (addSteinerPoints) {
        // Compute inner points
        if (useNewPoissonDiskSampling) {
            getInnerPoints2(pl, target_pt_dist, innerPoints);
        } else {
            getInnerPoints(pl, target_pt_dist, innerPoints);
        }
    }
	
    std::vector<p2t::Point> p2tPoints;
    p2tPoints.reserve(pl.points.size() + innerPoints.size());
    
	for (unsigned int i1 = 0; i1 < pl.points.size(); i1++)
    {
		const vec2f p = pl.points[i1].c;
		pts.push_back(vec3f(p.x, p.y, 0));
        p2tPoints.emplace_back(p.x, p.y);
	}
    
    for (const auto p : innerPoints) {
        pts.push_back(vec3f(p.x, p.y, 0));
        p2tPoints.emplace_back(p.x, p.y);
    }
    
    std::vector<p2t::Point*> polylinePtrs;
    for (size_t i = 0; i < pl.points.size(); ++i) {
        polylinePtrs.emplace_back(&p2tPoints[i]);
    }
	p2t::CDT cdt(polylinePtrs);
    
    for (size_t i = pl.points.size(); i < p2tPoints.size(); ++i) {
        cdt.AddPoint(&p2tPoints[i]);
    }

	cdt.Triangulate();
    
	std::vector<p2t::Triangle*> triangles = cdt.GetTriangles();

	for (unsigned int i = 0; i < triangles.size(); i++) {
        p2t::Triangle* triangle = triangles[i];
		size_t i0 = triangle->GetPoint(0) - &p2tPoints[0];
		size_t i1 = triangle->GetPoint(1) - &p2tPoints[0];
		size_t i2 = triangle->GetPoint(2) - &p2tPoints[0];
        
        //assert(i0 < p2tPoints.size());
        //assert(i1 < p2tPoints.size());
        //assert(i2 < p2tPoints.size());
        
        if (i0 >= p2tPoints.size()) {
            std::cout << "Head or tail point in triangulation" << std::endl;
            auto point = triangle->GetPoint(0);
            i0 = pts.size();
            pts.emplace_back(point->x, point->y, 0.0f);
        }
        if (i1 >= p2tPoints.size()) {
            std::cout << "Head or tail point in triangulation" << std::endl;
            auto point = triangle->GetPoint(1);
            i1 = pts.size();
            pts.emplace_back(point->x, point->y, 0.0f);
        }
        if (i2 >= p2tPoints.size()) {
            std::cout << "Head or tail point in triangulation" << std::endl;
            auto point = triangle->GetPoint(2);
            i2 = pts.size();
            pts.emplace_back(point->x, point->y, 0.0f);
        }
        
		tris.push_back(vec3i(i0, i1, i2));
	}

	//if (addSteinerPoints) {

		//for (int i1 = 0; i1 < 10; i1++) smoothDataOnMeshIgnoreFirst(polylinePtrs.size(), pts, tris);
	//}
}
