#include "include/operators/UmbrellaMinimizeSelectionUMF.h"


#include <map>

#include "SparseMatrix.h"


void SmoothMeshUMF(SimpleMesh* sm) {
	// Set the vertex marker 'is_boundary_point' true for all vertices that are to be moved:
	for (unsigned int i1 = 0; i1 < sm->tList.size(); i1++) {
		Triangle* t = sm->tList[i1];
		if (t->marker) {
			sm->vList[t->v0()].is_boundary_point = true;
			sm->vList[t->v1()].is_boundary_point = true;
			sm->vList[t->v2()].is_boundary_point = true;
		}
	}

	int numVertices = (int)sm->vList.size();
	// Compute map
	std::map<int, int> indexLookup;
	int numMoovables = 0;
	for (int i1 = 0; i1 < numVertices; i1++) {
		if (sm->vList[i1].is_boundary_point) {
			indexLookup.insert(std::pair<int, int>(i1, numMoovables));
			numMoovables++;
		}
	}

	//******************************************************************************************
	// Use UMFPACK fast sparse LU decomposition to solve:

	std::cerr << "Num moveables: " << numMoovables << std::endl;

	SparseMatrixCol<double> matrix(numMoovables);
	vec3d* b = new vec3d[numMoovables];

	double* one_row = new double[numMoovables];

	for (std::map<int, int>::iterator it = indexLookup.begin(); it != indexLookup.end(); it++) { // F�r jede Zeile, leite nach i1 ab.
		int lookup = it->second;
		int index = it->first;
		int valenz_index = (int)sm->vList[index].eList.size();

		// Zero-out 'one_row':
		for (int i1 = 0; i1 < numMoovables; i1++) one_row[i1] = 0;

		one_row[lookup] += 2.0;

		// proceed one-fan:
		for (int i1 = 0; i1 < valenz_index; i1++) {

			int other_v = (sm->vList[index].eList[i1]->v0 == index) ? sm->vList[index].eList[i1]->v1 : sm->vList[index].eList[i1]->v0;

			if (sm->vList[other_v].is_boundary_point) {
				int lookup_other_v = indexLookup[other_v];
				one_row[lookup_other_v] -= (2.0/valenz_index);

			} else {
				b[lookup] += sm->vList[other_v].c * (2.0/valenz_index);
			}

			// process fan of neighbor:
			int valenz_neighbor = (int)sm->vList[other_v].eList.size();
			for (int i2 = 0; i2 < valenz_neighbor; i2++) {
				int neighbor_neighbor = (sm->vList[other_v].eList[i2]->v0 == other_v) ? sm->vList[other_v].eList[i2]->v1 : sm->vList[other_v].eList[i2]->v0;

				if (sm->vList[other_v].is_boundary_point) {
					int lookup_other_v = indexLookup[other_v];
					one_row[lookup_other_v] -= (2.0/valenz_index);
				} else {
					b[lookup] += sm->vList[other_v].c * (2.0/valenz_index);
				}

			}

		}

		// Copy 'one_row' to matrix:
		for (int i1 = 0; i1 < numMoovables; i1++)
			if (one_row[i1] != 0)
				matrix.insert(i1, lookup, one_row[i1]);

	}
	delete[] one_row;

	CompColSparseMatrix ccsm;
	matrix.getCompColSparseMatrix(ccsm);
//	ccsm.printToImage("matrix_img.raw");
	ccsm.computeLU_Umfpack();

	// Backsolve for x,y,z components:
	double* bb = new double[numMoovables];
	double* xx = new double[numMoovables];
	for (int i1 = 0; i1 < 3; i1++) {
		for (int i2 = 0; i2 < numMoovables; i2++) {
			bb[i2] = b[i2][i1];
		}
		ccsm.backSolve_Umfpack(bb, xx);
		for (int i2 = 0; i2 < numVertices; i2++) {
			if (sm->vList[i2].is_boundary_point) {
				int index = indexLookup[i2];
				sm->vList[i2].c[i1] = bb[index];
			}
		}
	}


	delete[] xx;
	delete[] bb;
	delete[] b;
}
