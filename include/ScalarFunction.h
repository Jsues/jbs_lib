#ifndef SCALAR_FUNCTION_H
#define SCALAR_FUNCTION_H

//#include "marchingCubes.h"
//#include "SimpleMesh.h"
//#include "FileIO/VolumeIO.h"
#include "JBS_General.h" // for UInt

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class ScalarFunction {

public:

	typedef T Point;
	typedef typename T::ValueType ValueType;

	virtual ValueType eval(const Point& pt) const = 0;
	virtual ValueType evalValAndGrad(const Point& pt, Point& grad) const = 0;
	virtual UInt getID() const = 0;

	static const UInt TYPE_RBF_SUM = 1001;
	static const UInt TYPE_RBF_TREE = 1002;
	static const UInt TYPE_RBF_TREE_3 = 1003;
	static const UInt TYPE_VOID_FUNC = 1004;
	static const UInt TYPE_MARCO_MPU = 1005;
	static const UInt TYPE_JOCHEN_MPU = 1006;
	static const UInt TYPE_DENSITY_FUNC = 1007;
	static const UInt TYPE_HOPPE_IMPLICIT = 1008;

};
	
// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class ScalarFunctionWithConfidence : public ScalarFunction<T> {

public:

	typedef T Point;
	typedef typename T::ValueType ValueType;

	virtual ValueType evalConfidence(const Point& pt) const = 0;

};
	
// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


//****************************************************************************************************************

//template <class T>
//SimpleMesh* RBFToSm(ScalarFunction<T>* func, T minV, T maxV, int level, int stepsMax=100) {
//
//
//	T diag = maxV-minV;
//	minV -= diag*0.02f;
//	maxV += diag*0.02f;
//	T::ValueType brick_size = std::max(maxV[0] - minV[0], std::max(maxV[1] - minV[1], maxV[2] - minV[2])) / (T::ValueType)stepsMax;
//	//std::cerr << "brick_size: " << brick_size << std::endl;
//	const int brickX = 2 + (unsigned int)((maxV[0] - minV[0]) / brick_size);
//	const int brickY = 2 + (unsigned int)((maxV[1] - minV[1]) / brick_size);
//	const int brickZ = 2 + (unsigned int)((maxV[2] - minV[2]) / brick_size);
//	Volume<float>* vol = new Volume<float>;
//	vol->initWithDims(minV, maxV, brickX, brickY, brickZ);
//
//	double d; vec3d grad;
//	for (int i = 0; i < brickX; ++i) {
//		std::cerr << i << " / " << brickX << "            \r";
//		for (int j = 0; j < brickY; ++j) {
//			for (int k = 0; k < brickZ; ++k) {
//				vec3d pos = vol->pos(i,j,k);
//				d = func->eval(pos);
//				vol->set(i,j,k, (float)d);
//			}
//		}
//	}
//
//	//VolumeWriter<float> volW(vol);
//	//volW.writeRAWFile("vol.raw");
//
//	TrivialMesh<vec3f> tm(2000);
//	for (int x = 0; x < brickX-1; x++) {
//		for (int y = 0; y < brickY-1; y++) {
//			for (int z = 0; z < brickZ-1; z++) {
//				processVolumeCell(vol, x, y, z, 0, &tm);
//			}
//		}
//	}
//	SimpleMesh* sm = TrivialMeshToSimpleMesh(&tm);
//
//	return sm;
//}

//****************************************************************************************************************


#endif
