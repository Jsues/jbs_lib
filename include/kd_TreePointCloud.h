#ifndef JBS_KD_TREE_POINT_CLOUD_H
#define JBS_KD_TREE_POINT_CLOUD_H


#include <vector>
#include <algorithm>
#include <fstream>
#include "JBS_General.h"
#include "PointCloud.h"
#include "point3d.h"
#include "point2d.h"
#include <ANN/ANN.h>
#include "nr_templates.h"
#include "MathMatrix.h"
#include "Matrix3D.h"

//#include "approximate.h"



#define NUM_NEAREST 6



struct myPair {

	myPair(int f, float s) {
		first = f;
		second = s;
	}

	int first;
	float second;

	bool operator<(const myPair& other) {
		return (second < other.second);
	}
};



template <class T>
class PointCloud;


//! This class represents a pointcloud or unstructured pointset, i.e. it holds nothing but a vector of 2D/3D points
template<class T>
class kdTreePointCloud : public PointCloud<T> {

public:

//****************************************************************************************************

	//! Type of a Point, for example a 3D Vector of floats.
	typedef T Point;

	//! Value-Type of Templates components (i.e. float or double).
	typedef typename T::ValueType ValueType;

	//! Dimension of the pointcloud.
	static const int dim = T::dim;

//****************************************************************************************************

	//! Constructor.
	kdTreePointCloud(unsigned int numANNalloc_) {

#ifdef DEBUG
		std::cerr << "Enter Constructor" << std::endl;
#endif

		numANNalloc = numANNalloc_;
		nearestNeighbors = NULL;
		nearestNeighborsParametrization = NULL;
		normals = NULL;
		color = NULL;
		base1 = NULL;
		base2 = NULL;
		kdTree = NULL;
		dataPts = annAllocPts(numANNalloc, dim);
		locked = false;

#ifdef DEBUG
		std::cerr << "Leave Constructor" << std::endl;
#endif

	}

//****************************************************************************************************

	//! Destructor.
	~kdTreePointCloud() {

#ifdef DEBUG
		std::cerr << "Enter Destructor" << std::endl;
#endif


		if (nearestNeighbors != NULL) {
			for (int i1 = 0; i1 < numPts; i1++)
				delete[] nearestNeighbors[i1];
			delete[] nearestNeighbors;
		}

		if (nearestNeighborsParametrization != NULL) {
			for (int i1 = 0; i1 < numPts; i1++)
				delete[] nearestNeighborsParametrization[i1];
			delete[] nearestNeighborsParametrization;
		}

		if (normals != NULL) delete[] normals;
		if (base1 != NULL) delete[] base1;
		if (base2 != NULL) delete[] base2;
		curvature.clear();
		if (color != NULL) delete[] color;

#ifdef DEBUG
		std::cerr << "Leave Destructor" << std::endl;
#endif

	}

//****************************************************************************************************

	//! Returns the indices of the k-nearest neighbors of point 'n'
	std::vector<int> getNeighbors(int n, int k) {
		std::vector<int> neighbors;

		T p = PointCloud<T>::m_pts[n];
		ANNpoint queryPt;
		queryPt = annAllocPt(dim);
		queryPt[0] = p[0]; queryPt[1] = p[1]; queryPt[2] = p[2];

		ANNidxArray nnIdx = new ANNidx[k+1];							// allocate near neigh indices
		ANNdistArray dists = new ANNdist[k+1];						// allocate near neighbor dists

		kdTree->annkSearch(queryPt, k+1, nnIdx, dists, 0);

		for (int i1 = 0; i1 < k; i1++)
			neighbors.push_back(nnIdx[i1+1]);

		delete[] nnIdx;
		delete[] dists;

		return neighbors;
	}

//****************************************************************************************************

	//! Computes Normals.
	void computeNormals() {

#ifdef DEBUG
		std::cerr << "Enter computeNormals" << std::endl;
#endif

		if (nearestNeighbors == NULL)
			buildNeighborStructure(NUM_NEAREST);

		normals = new point3d<ValueType>[numPts];
		base1 = new point3d<ValueType>[numPts];
		base2 = new point3d<ValueType>[numPts];

		// Covariance matrix
		ValueType **CV = new ValueType *[4];
		for(int i1 = 0; i1 < 4; i1++)
			CV[i1] = new ValueType[4];
		// Eigenvalues
		ValueType lambda[4];
		// Eigenvectors
		ValueType **EV = new ValueType *[4];
		for(int i1 = 0; i1 < 4; i1++)
			EV[i1] = new ValueType[4];

		for (int i1 = 0; i1 < numPts; i1++) {
			// Compute cog of neighborhood:
			T cog;
			for (int i2 = 0; i2 < NUM_NEAREST; i2++)
				cog += PointCloud<T>::m_pts[nearestNeighbors[i1][i2]];
			cog /= NUM_NEAREST;

			// Compute Covariance matrix:
			for(int i2 = 1; i2 <= 3; i2++)
				for(int i3 = 1; i3 <= 3; i3++)
					CV[i2][i3] = 0.0;

			for(int i4 = 0; i4 < NUM_NEAREST; i4++) {
				T diff = PointCloud<T>::m_pts[nearestNeighbors[i1][i4]] - cog;
				for(int i2 = 1; i2 <= 3; i2++) {
					for(int i3 = i2; i3 <= 3; i3++) {
						CV[i2][i3] += diff[i2-1] * diff[i3-1];
						CV[i3][i2] = CV[i2][i3];
					}
				}
			}

			int tmp = 0;
			jacobi(CV, 3, lambda, EV, &tmp);

			if (lambda[1] <= lambda[2] && lambda[1] <= lambda[3]) {
				normals[i1][0] = EV[1][1];
				normals[i1][1] = EV[2][1];
				normals[i1][2] = EV[3][1];
				base1[i1][0] = EV[1][2];
				base1[i1][1] = EV[2][2];
				base1[i1][2] = EV[3][2];
				base2[i1][0] = EV[1][3];
				base2[i1][1] = EV[2][3];
				base2[i1][2] = EV[3][3];
			} else if (lambda[2] <= lambda[1] && lambda[2] <= lambda[3]) {
				normals[i1][0] = EV[1][2];
				normals[i1][1] = EV[2][2];
				normals[i1][2] = EV[3][2];
				base1[i1][0] = EV[1][1];
				base1[i1][1] = EV[2][1];
				base1[i1][2] = EV[3][1];
				base2[i1][0] = EV[1][3];
				base2[i1][1] = EV[2][3];
				base2[i1][2] = EV[3][3];
			} else if (lambda[3] <= lambda[1] && lambda[3] <= lambda[2]) {
				normals[i1][0] = EV[1][3];
				normals[i1][1] = EV[2][3];
				normals[i1][2] = EV[3][3];
				base1[i1][0] = EV[1][2];
				base1[i1][1] = EV[2][2];
				base1[i1][2] = EV[3][2];
				base2[i1][0] = EV[1][1];
				base2[i1][1] = EV[2][1];
				base2[i1][2] = EV[3][1];
			}

			normals[i1].normalize();
			base1[i1].normalize();
			base2[i1].normalize();
		}

		for (int i1 = 0; i1 < 4; i1++) {
			delete[] CV[i1];
			delete[] EV[i1];
		}
		delete[] CV;
		delete[] EV;


#ifdef DEBUG
		std::cerr << "Leave computeNormals" << std::endl;
#endif

	};

//****************************************************************************************************

	//! Builds the kD-Tree.
	void buildKdTree() {

#ifdef DEBUG
		std::cerr << "Enter buildKdTree" << std::endl;
#endif

		locked = true;
		numPts = (int)PointCloud<T>::m_pts.size();

		std::cerr << "Points: " << (int)PointCloud<T>::m_pts.size() << " dim: " << dim << std::endl;
		kdTree = new ANNkd_tree(dataPts, (int)PointCloud<T>::m_pts.size(), dim);

#ifdef DEBUG
		std::cerr << "Leave buildKdTree" << std::endl;
#endif

	};

//****************************************************************************************************

	//! Inserts a point into Pointcloud and kd-tree.
	void insertPoint(T pt) {

		if (locked) {
			std::cerr << "Pointcloud is locked. No more points may be added!" << std::endl;
			return;
		}

		PointCloud<T>::insertPoint(pt);

		int pos = (int)PointCloud<T>::m_pts.size()-1;

		if (pos >= numANNalloc) {
			std::cerr << "ERROR: Insufficient number of points allocated for kd-tree." << std::endl;
			exit(EXIT_FAILURE);
		}

		float x = (float)PointCloud<T>::m_pts[pos][0];
		float y = (float)PointCloud<T>::m_pts[pos][1];
		float z = (float)PointCloud<T>::m_pts[pos][2];
		dataPts[pos][0] = x; dataPts[pos][1] = y; dataPts[pos][2] = z;
	}

//****************************************************************************************************

	//! Calculates Nearest Neighbors for all vertices and stores them im int** nearestNeighbors.
	void buildNeighborStructure(int numNN = 20) {

#ifdef DEBUG
		std::cerr << "Enter buildNeighborStructure" << std::endl;
#endif

		if (kdTree == NULL)
			buildKdTree();

		nearestNeighbors = new int*[numPts];

		for (int i1 = 0; i1 < numPts; i1++)
			computeNearestNN(i1, numNN);

#ifdef DEBUG
		std::cerr << "Leave buildNeighborStructure" << std::endl;
#endif

	}

//****************************************************************************************************

	//! Calculates Parametrization of neighbor vertices for all vertices and stores them im point2d<ValueType>** nearestNeighborsParametrization.
	void computeParametrization(int numNN = 20) {

#ifdef DEBUG
		std::cerr << "Enter computeParametrization" << std::endl;
#endif

		if (normals == NULL)
			computeNormals();

		nearestNeighborsParametrization = new point2d<ValueType>*[numPts];

		for (int i1 = 0; i1 < numPts; i1++)
			computeParametrization(i1, NUM_NEAREST);

#ifdef DEBUG
		std::cerr << "Leave computeParametrization" << std::endl;
#endif

	}

//****************************************************************************************************

	//! Calculates Nearest Neighbors for all vertices and stores them im int** nearestNeighbors.
	void calculateCurvature() {
std::cerr << "Enter calculateCurvature" << std::endl;
#ifdef DEBUG
		std::cerr << "Enter calculateCurvature" << std::endl;
#endif

		if (nearestNeighborsParametrization == NULL)
			computeParametrization();

		curvature.reserve(numPts);

		for (int i1 = 0; i1 < numPts; i1++)
			computeCurvature(i1, NUM_NEAREST);

		//myPair* beg = (myPair*)curvature.begin();
		//myPair* end = (myPair*)curvature.end();
		//std::cerr << beg << "-" << end << std::endl;
		std::sort(curvature.begin(), curvature.end());

		color = new ValueType[numPts];
		for (int i1 = 0; i1 < numPts; i1++) {
			//color[curvature[i1].first] = curvature[i1].second*300; //ValueType(i1)/ValueType(numPts);
			//if (color[curvature[i1].first] > 1)
			//	color[curvature[i1].first] = 1;

			color[curvature[i1].first] = ValueType(i1)/ValueType(numPts);
//			std::cerr << curvature[i1].second << " " << curvature[i1].first << " " << ValueType(i1)/ValueType(numPts) << std::endl;
//			std::cerr << curvature[i1].second << " " << curvature[i1].first << " " << color[curvature[i1].first] << std::endl;
		}

#ifdef DEBUG
		std::cerr << "Leave calculateCurvature" << std::endl;
#endif

	}

//****************************************************************************************************

	//! Estimated Normals.
	point3d<ValueType>* normals;
	point3d<ValueType>* base1;
	point3d<ValueType>* base2;

	ValueType* color;


private:

//****************************************************************************************************

	//! computes the 'numNN' nearest neighbors of vertex 'v'.
	void computeNearestNN(int v, int numNN) {
		T p = PointCloud<T>::m_pts[v];
		ANNpoint queryPt;
		queryPt = annAllocPt(dim);
		queryPt[0] = p[0]; queryPt[1] = p[1]; queryPt[2] = p[2];

		ANNidxArray nnIdx = new ANNidx[numNN+1];							// allocate near neigh indices
		ANNdistArray dists = new ANNdist[numNN+1];						// allocate near neighbor dists

		kdTree->annkSearch(queryPt, numNN+1, nnIdx, dists, 0);

		nearestNeighbors[v] = new int[numNN];

		for (int i1 = 0; i1 < numNN; i1++)
			nearestNeighbors[v][i1] = nnIdx[i1+1];

	};

//****************************************************************************************************

	void computeParametrization(int v, int numNN) {
		nearestNeighborsParametrization[v] = new point2d<ValueType>[numNN];

		for (int i1 = 0; i1 < numNN; i1++) {
			std::cerr << "neighbor " << i1 << " = " << nearestNeighbors[v][i1] << std::endl;
			ValueType x_tilda = base1[v] | (PointCloud<T>::m_pts[v]-PointCloud<T>::m_pts[nearestNeighbors[v][i1]]);
			ValueType y_tilda = base2[v] | (PointCloud<T>::m_pts[v]-PointCloud<T>::m_pts[nearestNeighbors[v][i1]]);

			ValueType tmp = ((PointCloud<T>::m_pts[v]-PointCloud<T>::m_pts[nearestNeighbors[v][i1]]).length() / sqrt(x_tilda*x_tilda + y_tilda*y_tilda));
			ValueType x = tmp * x_tilda;
			ValueType y = tmp * y_tilda;
			nearestNeighborsParametrization[v][i1][0] = x;
			nearestNeighborsParametrization[v][i1][1] = y;
		}
	};

//****************************************************************************************************

	void computeCurvature(int v_, int numNN) {

		//// Generate arrays for calls:
		//vec3f* points = new vec3f[numNN+1];
		//vec2f* params = new vec2f[numNN+1];
		//vec3d Tf[5];

		//points[0] = vec3f(0,0,0);
		//params[0] = vec2f(0,0);
		//for (int i1 = 0; i1 < numNN; i1++) {
		//	points[i1+1] = vec3f(PointCloud<T>::m_pts[nearestNeighbors[v_][i1]] - PointCloud<T>::m_pts[v_]);
		//	params[i1+1] = vec2f(nearestNeighborsParametrization[v_][i1][0], nearestNeighborsParametrization[v_][i1][1]);
		//}
		//JBSlib::approximate(numNN+1, points, params, Tf);
		//exit(1);
		//return;

		// Compute Matrix V:
		Matrix<ValueType> V(numNN, 5);
		Matrix<ValueType> Qi(numNN, 3);

		Matrix3D<float> mat(base2[v_][0], base2[v_][1], base2[v_][2], base1[v_][0], base1[v_][1], base1[v_][2], normals[v_][0], normals[v_][1], normals[v_][2]);
		//mat.print();

		for (int i1 = 0; i1 < numNN; i1++) {
			//ValueType v = nearestNeighborsParametrization[v_][i1][0];
			//ValueType u = -nearestNeighborsParametrization[v_][i1][1];
			ValueType u = (PointCloud<T>::m_pts[nearestNeighbors[v_][i1]] - PointCloud<T>::m_pts[v_])[0];
			ValueType v = (PointCloud<T>::m_pts[nearestNeighbors[v_][i1]] - PointCloud<T>::m_pts[v_])[1];
			V(i1,0) =  u;
			V(i1,1) =  v;
			V(i1,2) = (u*u) / 2;
			V(i1,3) =  u*v;
			V(i1,4) = (v*v) / 2;

			point3d<ValueType> vec = PointCloud<T>::m_pts[nearestNeighbors[v_][i1]] - PointCloud<T>::m_pts[v_];
			//vec = mat.vecTrans(vec);

//			std::cerr << "Parametrization: " << u << " " << v << std::endl;

			Qi(i1,0) = vec[0];
			Qi(i1,1) = vec[1];
			Qi(i1,2) = vec[2];
		}

		// V.print();
		// Qi.print();

		Matrix<ValueType> Vtrans;
		V.transposed(Vtrans);
		Matrix<ValueType> Vinv;
		Matrix<ValueType> Vproduct;

		// F = (Vtransposed * V)inverse * Vtransposed * Qi
		Vproduct = Vtrans * V; 
		Vproduct.inverse(Vinv); 
		Matrix<ValueType> f = Vtrans * Qi;
		Matrix<ValueType> F = Vinv * f;


		point3d<ValueType> Fu(F[0]);
		point3d<ValueType> Fv(F[1]);
		point3d<ValueType> Fuu(F[2]);
		point3d<ValueType> Fvv(F[4]);
		point3d<ValueType> Fuv(F[3]);

		//Fu.print();
		//Fv.print();
		//Fuu.print();
		//Fvv.print();
		//Fuv.print();


		if (v_ == 0) {
			// Write gnuplot file:
			std::ofstream outf("function.dat");
			outf << "reset" << std::endl;
			outf << "set size square" << std::endl;
			outf << "set parametric" << std::endl;
			outf << "set isosamples 20" << std::endl;
			outf << "splot [-3.5:3.5][-3.5:3.5] ";
			outf << "u,v,u*" << Fu[2] << " + v*" << Fv[2] << " + (u*u)/2*" << Fuu[2] << " + u*v*" << Fuv[2] << " + (v*v)/2*" << Fvv[2] << ", \"C:\\\\Projects\\\\pc_curvature\\\\Debug\\\\points.dat\" with points, \"C:\\\\Projects\\\\pc_curvature\\\\Debug\\\\refpt.dat\" with points" << std::endl;
			outf.close();
			std::ofstream outf3("refpt.dat");
			outf3 << "0 0 0" << std::endl;
			outf3.close();
			// Transform neighbors to local coordinate System:
			std::ofstream outf2("points.dat");
			//outf2 << PointCloud<T>::m_pts[v_][0] << " " <<  PointCloud<T>::m_pts[v_][1] << " " << PointCloud<T>::m_pts[v_][2] << std::endl;

			vec3f* transNN = new vec3f[numNN];
			for (int i1 = 0; i1 < numNN; i1++) {
				transNN[i1] = PointCloud<T>::m_pts[nearestNeighbors[v_][i1]] - PointCloud<T>::m_pts[v_];
				//transNN[i1] = mat.vecTrans(transNN[i1]);
				outf2 << transNN[i1][0] << " " << transNN[i1][1] << " " << transNN[i1][2] << std::endl;
			}
			outf2.close();
		}


		//F.print();
		ValueType xx1 = Fuu|Fvv;
		ValueType xx2 = Fuv|Fuv;
		ValueType xx3 = Fu|Fu;
		ValueType xx4 = Fv|Fv;

		//curvature[v_] = myPair(v_, fabs((xx1 - xx2)/(1 + xx3 + xx4)));
		ValueType K = fabs((xx1 - xx2)/(1 + xx3 + xx4));
		curvature.push_back(myPair(v_, K));
		//std::cerr << K << std::endl;

		// Calculate and print distances:
		if (v_ == 0)
			for (int i1 = 0; i1 < numNN; i1++) {
				ValueType u = nearestNeighborsParametrization[v_][i1][0];
				ValueType v = nearestNeighborsParametrization[v_][i1][1];
				point3d<ValueType> z = Fu*u + Fv*v + Fuu*(u*u)/2 + Fuv*u*v + Fvv*(v*v)/2;
				std::cerr << "-----" << std::endl;
				z.print();
				(PointCloud<T>::m_pts[nearestNeighbors[v_][i1]] - PointCloud<T>::m_pts[v_]).print();
			}

	};
	
//****************************************************************************************************

	//! NOT IMPLEMENTED! Tries to orient the normals of the pointcloud consistent.
	void OrientConsistent() {
	}

//****************************************************************************************************

	//! set to true by 'buildNeighborStructure'. No more points may be inserted afterwards.
    bool locked;

	//! Number of points in the cloud; will be set as soon as the cloud gets locked.
	int numPts;

	int** nearestNeighbors;

	point2d<ValueType>** nearestNeighborsParametrization;

	std::vector<myPair> curvature;

	int numANNalloc;

	ANNkd_tree* kdTree;

	ANNpointArray dataPts;
};

#endif
