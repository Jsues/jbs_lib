#ifndef JBS_TIMER_H
#define JBS_TIMER_H

#include <vector>
#include <ctime>
#include <iostream>
#include <sstream>
#include <string>


//! Stopwatch, measures timespans in ms and can print them to the screen.
class JBS_Timer {

public:

	JBS_Timer(bool _verbose = true) : verbose(_verbose) {
	};

	~JBS_Timer() {
	};

	void reset() {
		time_stamps.clear();
		strings.clear();
	}

	void log(std::string s = "") {
		if (verbose) std::cerr << s << std::endl;
		clock_t current = clock();
		time_stamps.push_back(static_cast<int>(current));
		strings.push_back(s);
	}

	void printStream(std::stringstream& stream) {
		for (unsigned int i1 = 1; i1 < time_stamps.size(); i1++) {
			int span = time_stamps[i1] - time_stamps[i1-1];
			stream << "Timespan " << i1 << " (" << strings[i1-1] << "): " << float(span)/1000.0f << std::endl;
		}
		int total = time_stamps[time_stamps.size()-1] - time_stamps[0];
		stream << "Total: " << float(total)/1000.0f << std::endl;
	}

	void print() {
		for (unsigned int i1 = 1; i1 < time_stamps.size(); i1++) {
			int span = time_stamps[i1] - time_stamps[i1-1];
			std::cerr << "Timespan " << i1 << " (" << strings[i1-1] << "): " << float(span)/1000.0f << std::endl;
		}
		int total = time_stamps[time_stamps.size()-1] - time_stamps[0];
		std::cerr << "Total: " << float(total)/1000.0f << std::endl;
	}

	std::vector<int> time_stamps;
	std::vector<std::string> strings;

private:

	bool verbose;

};


#endif

