#ifndef CELL_4D_H
#define CELL_4D_H


#if defined (WIN32) && !defined (__MINGW32__)
#pragma warning( disable : 4244 )
#endif

#include "JBS_General.h"
#include "point4d.h"
#include "iso4d.h"
#include "TetraMesh4D.h"
#include "marchingCubes4d_2.h"
#include "Volume.h"
#include "IndexedTetraMesh.h"
#include "FindDuplicatesOctree.h"
#include "kd_PointCloud.h"


// ****************************************************************************************

//! A fourdimensional cell. See C:\Forschung\Projects\4dmarching\material\Hypercube.cdr for visualization.
template <class T, class U>
class Cell4D {

public:

	static const int VertexNeighbors[16][4];
	static const int VertexNeighborsBit[16];

// ****************************************************************************************

	//! Value-Type of Templates components.
	typedef T ValueType; 

	//! Value-Type of Templates components.
	typedef U GridType; 

// ****************************************************************************************

	Cell4D() {
		//for (UInt i1 = 0; i1 < 16; i1++)
		//	std::cerr << VertexNeighbors[i1][0] << " " << VertexNeighbors[i1][1] << " " << VertexNeighbors[i1][2] << " " << VertexNeighbors[i1][3] << std::endl;
	}

// ****************************************************************************************

	Cell4D(const ValueType* values) {
		for (UInt i1 = 0; i1 < 16; i1++) {
			isovalues.array[i1] = values[i1];
		}
	}

// ****************************************************************************************

	Cell4D(const ValueType* values, const GridType* grid) {
		for (UInt i1 = 0; i1 < 16; i1++) {
			isovalues.array[i1] = values[i1];
			gridpoints.array[i1] = grid[i1];
		}
	}

// ****************************************************************************************

	inline ValueType operator[] (const UInt i) const {
		return isovalues.array[i];
	}

// ****************************************************************************************

	inline GridType getCorner(const short i) const {
		return gridpoints.array[i];
	}

// ****************************************************************************************

	void print() {
		std::cerr << "Cell4D:" << std::endl;
		for (UInt i1 = 0; i1 < 16; i1++)
			std::cerr << "(" << gridpoints.array[i1] << "): " << isovalues.array[i1] << std::endl;
	}

// ****************************************************************************************

	//inline void computeIso4DCellDirect(ValueType isovalue, TetraMesh4D<ValueType>* tetMesh) const {

	//	int index = 0;
	//	if (isovalues.array[0] > isovalue)
	//		index |= 1;
	//	if (isovalues.array[1] > isovalue)
	//		index |= 2;
	//	if (isovalues.array[2] > isovalue)
	//		index |= 4;
	//	if (isovalues.array[3] > isovalue)
	//		index |= 8;
	//	if (isovalues.array[4] > isovalue)
	//		index |= 16;
	//	if (isovalues.array[5] > isovalue)
	//		index |= 32;
	//	if (isovalues.array[6] > isovalue)
	//		index |= 64;
	//	if (isovalues.array[7] > isovalue)
	//		index |= 128;
	//	if (isovalues.array[8] > isovalue)
	//		index |= 256;
	//	if (isovalues.array[9] > isovalue)
	//		index |= 512;
	//	if (isovalues.array[10] > isovalue)
	//		index |= 1024;
	//	if (isovalues.array[11] > isovalue)
	//		index |= 2048;
	//	if (isovalues.array[12] > isovalue)
	//		index |= 4096;
	//	if (isovalues.array[13] > isovalue)
	//		index |= 8192;
	//	if (isovalues.array[14] > isovalue)
	//		index |= 16384;
	//	if (isovalues.array[15] > isovalue)
	//		index |= 32768;

	//	std::cerr << index << std::endl;
	//	exit(1);

	//}

// ****************************************************************************************

	inline void computeIso4D(ValueType isovalue, TetraMesh4D<ValueType>* tetMesh) {

		std::cerr << "Deprecated function call Cell4D<T,U>::computeIso4D(...)" << std::endl;
		std::cerr << "use Cell4D<T,U>::GetIsoTetrahedra(...) instead" << std::endl;
		std::cerr << "EXIT" << std::endl;
		exit(EXIT_FAILURE);

		/*
		for (UChar i1 = 0; i1 < marching4d::numPathes; ++i1) {

			short v0 = (marching4d::pathes[i1] >> 8) & 0xF;
			short v1 = (marching4d::pathes[i1] >> 4) & 0xF;
			short v2 = (marching4d::pathes[i1]) & 0xF;

			UInt numV = process1Simplex(	v0,
											v1,
											v2,
											isovalue,
											&vertices[0]						);

			if (numV == 0) continue;

			else if (numV == 4) {		// One tetrahedron
				//if (TetraMesh4D<ValueType>::Tetrahedron::isTetrahedron(vertices[0], vertices[1], vertices[2], vertices[3]))
					tetMesh->insert(TetraMesh4D<ValueType>::Tetrahedron(vertices[0], vertices[1], vertices[2], vertices[3]));
				//else {
				//	std::cerr << "No real tetrahedron, skip" << std::endl;
				//	std::cerr << vertices[0] << " " << vertices[1] << " " << vertices[2] << " " << vertices[3] << std::endl;
				//}
			} else if (numV == 6) {		// Three tetrahedra
				tetMesh->insert(TetraMesh4D<ValueType>::Tetrahedron(vertices[0], vertices[1], vertices[2], vertices[3]));
				tetMesh->insert(TetraMesh4D<ValueType>::Tetrahedron(vertices[1], vertices[2], vertices[3], vertices[4]));
				tetMesh->insert(TetraMesh4D<ValueType>::Tetrahedron(vertices[2], vertices[3], vertices[4], vertices[5]));
			}
		}
		*/
	}

// ****************************************************************************************

	inline GridType getInterpolationFromEdge(int edgeIndex, ValueType isovalue) const {

		int corner_index_1 = 0;
		int corner_index_2 = 0;

		UInt i1;
		for (i1 = 0; i1 <= 16; i1++) {
			if (edgeIndex & 1) {
				corner_index_1 = i1;
				edgeIndex = edgeIndex >> 1;
				break;
			}
			edgeIndex = edgeIndex >> 1;
		}
		for ( i1 = i1+1; i1 <= 16; i1++) {
			if (edgeIndex & 1) {
				corner_index_2 = i1;
				edgeIndex = edgeIndex >> 1;
				break;
			}
			edgeIndex = edgeIndex >> 1;
		}

		//std::cerr << "One edge: (" << edgeIndex << ")  " << corner_index_1 << " " << corner_index_2 << std::endl;
		//gridpoints.array[corner_index_1].print();
		//gridpoints.array[corner_index_2].print();

		return marching4d::linearInterpolate(isovalue, gridpoints.array[corner_index_1], gridpoints.array[corner_index_2], isovalues.array[corner_index_1], isovalues.array[corner_index_2]);

	}

// ****************************************************************************************

	inline int computeCases(ValueType isovalue) const {
		int index = 0;
		if (isovalues.array[0] > isovalue)
			index |= 1;
		if (isovalues.array[1] > isovalue)
			index |= 2;
		if (isovalues.array[2] > isovalue)
			index |= 4;
		if (isovalues.array[3] > isovalue)
			index |= 8;
		if (isovalues.array[4] > isovalue)
			index |= 16;
		if (isovalues.array[5] > isovalue)
			index |= 32;
		if (isovalues.array[6] > isovalue)
			index |= 64;
		if (isovalues.array[7] > isovalue)
			index |= 128;
		if (isovalues.array[8] > isovalue)
			index |= 256;
		if (isovalues.array[9] > isovalue)
			index |= 512;
		if (isovalues.array[10] > isovalue)
			index |= 1024;
		if (isovalues.array[11] > isovalue)
			index |= 2048;
		if (isovalues.array[12] > isovalue)
			index |= 4096;
		if (isovalues.array[13] > isovalue)
			index |= 8192;
		if (isovalues.array[14] > isovalue)
			index |= 16384;
		if (isovalues.array[15] > isovalue)
			index |= 32768;


		//if (index >= 32768)
		//	index = 65535-index;


		//if (index >= 32768)
		//	std::cerr << index << "**''--''**" << std::endl;

		return index;
	}

// ****************************************************************************************

	inline void GetIsoTetrahedra(ValueType isovalue, std::vector<UInt>* vertices_container, JBSlib::DFOcTree< JBSlib::DFOcNode< point4d<ValueType> > >* octree) {
		// Compute case:
		int index = computeCases(isovalue);

		// Case computed, lookup which edges are intersected:
		const int* vertices = MC4D::Cell4DVertices[index];

		//for (UInt i1 = 0; i1 < 20; i1++) {
		//	std::cerr << vertices[i1] << " ";
		//}
		//std::cerr << std::endl;

		// Compute 4d vertex positions from table:
		std::vector<GridType> intersections;
		intersections.reserve(32);
        UInt numV = ComputeInterpolatedPositions(isovalue, intersections, vertices);

		for (UInt i1 = 0; i1 < numV; i1++) {
			UInt pos;
			octree->insertObject(pos, intersections[i1]);
			intersection_indices[i1] = pos;
		}

		const short numTetras = MC4D::NumCell4DTetra[index];
		for (short i1 = 0; i1 < numTetras*4; i1+=4) {
			vertices_container->push_back(intersection_indices[MC4D::Cell4DTetra[index][i1+0]]);
			vertices_container->push_back(intersection_indices[MC4D::Cell4DTetra[index][i1+1]]);
			vertices_container->push_back(intersection_indices[MC4D::Cell4DTetra[index][i1+2]]);
			vertices_container->push_back(intersection_indices[MC4D::Cell4DTetra[index][i1+3]]);
		}

	}

// ****************************************************************************************

	inline void GetIsoTetrahedra(ValueType isovalue, IndexedTetraMesh<ValueType>* mesh, JBSlib::DFOcTree< JBSlib::DFOcNode< point4d<ValueType> > >* octree) {
		// Compute case:
		int index = computeCases(isovalue);

		// Case computed, lookup which edges are intersected:
		const int* vertices = MC4D::Cell4DVertices[index];

		//for (UInt i1 = 0; i1 < 20; i1++) {
		//	std::cerr << vertices[i1] << " ";
		//}
		//std::cerr << std::endl;

		// Compute 4d vertex positions from table:
		std::vector<GridType> intersections;
		intersections.reserve(32);
        UInt numV = ComputeInterpolatedPositions(isovalue, intersections, vertices);

		for (UInt i1 = 0; i1 < numV; i1++) {
			UInt pos;
			if (octree->insertObject(pos, intersections[i1])) {
				mesh->insertVertex(intersections[i1]);
			}
			intersection_indices[i1] = pos;
		}

		const short numTetras = MC4D::NumCell4DTetra[index];
		for (short i1 = 0; i1 < numTetras*4; i1+=4) {

			mesh->insertTetrahedron(
				intersection_indices[MC4D::Cell4DTetra[index][i1+0]],
				intersection_indices[MC4D::Cell4DTetra[index][i1+1]],
				intersection_indices[MC4D::Cell4DTetra[index][i1+2]],
				intersection_indices[MC4D::Cell4DTetra[index][i1+3]]
			);
		}

	}

// ****************************************************************************************

	inline void GetIsoTetrahedra(ValueType isovalue, TetraMesh4D<ValueType>* mesh) const {
		// Compute case:
		int index = computeCases(isovalue);

		// Case computed, lookup which edges are intersected:
		const int* vertices = MC4D::Cell4DVertices[index];

		// Compute 4d vertex positions from table:
		std::vector<GridType> intersections;
		intersections.reserve(32);
        ComputeInterpolatedPositions(isovalue, intersections, vertices);

		const short numTetras = MC4D::NumCell4DTetra[index];
		for (short i1 = 0; i1 < numTetras*4; i1+=4) {
			point4d<T> p0 = intersections[MC4D::Cell4DTetra[index][i1+0]];
			point4d<T> p1 = intersections[MC4D::Cell4DTetra[index][i1+1]];
			point4d<T> p2 = intersections[MC4D::Cell4DTetra[index][i1+2]];
			point4d<T> p3 = intersections[MC4D::Cell4DTetra[index][i1+3]];
			mesh->insert(Tet4D<T>(p0, p1, p2, p3));
		}

	}

// ****************************************************************************************

	inline UInt ComputeInterpolatedPositions(ValueType isovalue, std::vector<GridType>& intersections, const int* vertices) const {

		for (UInt i1 = 0; i1 < 32; i1++) {
			if (vertices[i1] == 0) {
				return i1;
			}
			else {
				intersections.push_back(getInterpolationFromEdge(vertices[i1], isovalue));
			}
		}

        return 32;
	}

// ****************************************************************************************

	inline void GetIntersections(ValueType isovalue, std::vector<GridType>& intersections, std::vector<short>& indices) const {

		// Compute case:
		int index = computeCases(isovalue);

		// Case computed, lookup which edges are intersected:
		const int* vertices = MC4D::Cell4DVertices[index];

		for (UInt i1 = 0; i1 < 20; i1++) {
			std::cerr << vertices[i1] << " ";
		}
		std::cerr << std::endl;

		// Compute 4d vertex positions from table:
        ComputeInterpolatedPositions(isovalue, intersections, vertices);

		const short numTetras = MC4D::NumCell4DTetra[index];
		for (short i1 = 0; i1 < numTetras*4; i1++) {
			indices.push_back(MC4D::Cell4DTetra[index][i1]);
		}
		std::cerr << "Makes " << numTetras << " Tetrahedra" << std::endl;

	}

// ****************************************************************************************

	GridType vertices[6];

	UInt intersection_indices[32];

	struct {
		ValueType array[16];
	} isovalues;

	struct {
		GridType array[16];
	} gridpoints;

private:

// ****************************************************************************************

	//! Compute the 'isovolume of a 4D-Simplex'
	/*!
		\return the number of tetrahedra that have been extracted (i.e. 4 for one tetrahedron or 6 for three tetrahedra). Please read detailed description.
		\param v0			The vertices 0, v0, v1, v2 and 15 must form one simplex in 4D.
		\param v1			The vertices 0, v0, v1, v2 and 15 must form one simplex in 4D.
		\param v2			The vertices 0, v0, v1, v2 and 15 must form one simplex in 4D.
		\param isolevel		isovalue.
		\param vertices		a preinitialized array of 4D-vertices (must be size 6).
		If the return value is 4, the vertices 'vertices[0]', 'vertices[1]', 'vertices[2]' and 'vertices[3]' form the tetrahedron<br>
		If the return value is 6,
		the vertices 'vertices[0]', 'vertices[1]', 'vertices[2]' and 'vertices[3]' form the first tetrahedron,
		the vertices 'vertices[1]', 'vertices[2]', 'vertices[3]' and 'vertices[4]' form the second tetrahedron
		and the vertices 'vertices[2]', 'vertices[3]', 'vertices[4]' and 'vertices[5]' form the third tetrahedron.
	*/
	inline UInt process1Simplex(short v1, short v2, short v3, ValueType isolevel, GridType* vertices) const {

		char simplexIndex = 0;
		if (isovalues.array[0]  < isolevel) simplexIndex |= 1;
		if (isovalues.array[v1] < isolevel) simplexIndex |= 2;
		if (isovalues.array[v2] < isolevel) simplexIndex |= 4;
		if (isovalues.array[v3] < isolevel) simplexIndex |= 8;
		if (isovalues.array[15] < isolevel) simplexIndex |= 16;

		const short EdgesInvolved = marching4d::edgeTableTetrahedron4D[(int)simplexIndex];

		if (EdgesInvolved == 0)		// All isovalues are larger/smaller than 'isolevel'
			return false;

		UInt numVertices = 0;

		if (EdgesInvolved & 1)		vertices[numVertices++] = marching4d::linearInterpolate(isolevel, gridpoints.array[0], gridpoints.array[v1], isovalues.array[0], isovalues.array[v1]);
		if (EdgesInvolved & 2)		vertices[numVertices++] = marching4d::linearInterpolate(isolevel, gridpoints.array[0], gridpoints.array[v2], isovalues.array[0], isovalues.array[v2]);
		if (EdgesInvolved & 4)		vertices[numVertices++] = marching4d::linearInterpolate(isolevel, gridpoints.array[0], gridpoints.array[v3], isovalues.array[0], isovalues.array[v3]);
		if (EdgesInvolved & 8)		vertices[numVertices++] = marching4d::linearInterpolate(isolevel, gridpoints.array[0], gridpoints.array[15], isovalues.array[0], isovalues.array[15]);
		if (EdgesInvolved & 16)		vertices[numVertices++] = marching4d::linearInterpolate(isolevel, gridpoints.array[v1], gridpoints.array[v2], isovalues.array[v1], isovalues.array[v2]);
		if (EdgesInvolved & 32)		vertices[numVertices++] = marching4d::linearInterpolate(isolevel, gridpoints.array[v1], gridpoints.array[v3], isovalues.array[v1], isovalues.array[v3]);
		if (EdgesInvolved & 64)		vertices[numVertices++] = marching4d::linearInterpolate(isolevel, gridpoints.array[v1], gridpoints.array[15], isovalues.array[v1], isovalues.array[15]);
		if (EdgesInvolved & 128)	vertices[numVertices++] = marching4d::linearInterpolate(isolevel, gridpoints.array[v2], gridpoints.array[v3], isovalues.array[v2], isovalues.array[v3]);
		if (EdgesInvolved & 256)	vertices[numVertices++] = marching4d::linearInterpolate(isolevel, gridpoints.array[v2], gridpoints.array[15], isovalues.array[v2], isovalues.array[15]);
		if (EdgesInvolved & 512)	vertices[numVertices++] = marching4d::linearInterpolate(isolevel, gridpoints.array[v3], gridpoints.array[15], isovalues.array[v3], isovalues.array[15]);

		return numVertices;
	}

// ****************************************************************************************

};

template <class T, class U>
const int Cell4D<T,U>::VertexNeighbors[16][4] = {
		{1, 2, 4, 8},			// v0
		{0, 3, 5, 9},			// v1
		{0, 3, 6, 10},			// v2
		{1, 2, 7, 11},			// v3
		{0, 5, 6, 12},			// v4
		{1, 4, 7, 13},			// v5
		{2, 4, 7, 14},			// v6
		{3, 5, 6, 15},			// v7
		{0, 9, 10, 12},			// v8
		{1, 8, 11, 13},			// v9
		{2, 8, 11, 14},			// v10
		{3, 9, 10, 15},			// v11
		{4, 8, 13, 14},			// v12
		{5, 9, 12, 15},			// v13
		{6, 10, 12, 15},		// v14
		{7, 11, 13, 14}			// v15
};

// ****************************************************************************************

template <class T, class U>
const int Cell4D<T,U>::VertexNeighborsBit[16] = {
		2|4|16|256,			// v0
		1|8|32|512,			// v1
		1|8|64|1024,		// v2
		2|4|128|2048,		// v3
		1|32|64|4096,		// v4
		2|16|128|8192,		// v5
		4|16|128|16384,		// v6
		8|32|64|32768,		// v7
		1|512|1024|4096,	// v8
		2|256|2048|8192,	// v9
		4|256|2048|16384,	// v10
		8|512|1024|32768,	// v11
		16|256|8192|16384,	// v12
		32|512|4096|32768,	// v13
		64|1024|4096|32768, // v14
		128|2048|8192|16384	// v15
};

// ****************************************************************************************

template<class T>
void fillCell(Cell4D< T,point4d<T> >& cell, Volume<T>* vol1, Volume<T>* vol2, UInt x, UInt y, UInt z, T t, T t_plus_1) {
	cell.isovalues.array[0]  = vol1->get(vec3i(x,y,z));
	cell.isovalues.array[1]  = vol1->get(vec3i(x+1,y,z));
	cell.isovalues.array[2]  = vol1->get(vec3i(x,y+1,z));
	cell.isovalues.array[3]  = vol1->get(vec3i(x+1,y+1,z));
	cell.isovalues.array[4]  = vol1->get(vec3i(x,y,z+1));
	cell.isovalues.array[5]  = vol1->get(vec3i(x+1,y,z+1));
	cell.isovalues.array[6]  = vol1->get(vec3i(x,y+1,z+1));
	cell.isovalues.array[7]  = vol1->get(vec3i(x+1,y+1,z+1));

	cell.isovalues.array[8]  = vol2->get(vec3i(x,y,z));
	cell.isovalues.array[9]  = vol2->get(vec3i(x+1,y,z));
	cell.isovalues.array[10] = vol2->get(vec3i(x,y+1,z));
	cell.isovalues.array[11] = vol2->get(vec3i(x+1,y+1,z));
	cell.isovalues.array[12] = vol2->get(vec3i(x,y,z+1));
	cell.isovalues.array[13] = vol2->get(vec3i(x+1,y,z+1));
	cell.isovalues.array[14] = vol2->get(vec3i(x,y+1,z+1));
	cell.isovalues.array[15] = vol2->get(vec3i(x+1,y+1,z+1));

	cell.gridpoints.array[0] = point4d<T>(vol2->pos(x,y,z)			,t);
	cell.gridpoints.array[1] = point4d<T>(vol2->pos(x+1,y,z)		,t);
	cell.gridpoints.array[2] = point4d<T>(vol2->pos(x,y+1,z)		,t);
	cell.gridpoints.array[3] = point4d<T>(vol2->pos(x+1,y+1,z)		,t);
	cell.gridpoints.array[4] = point4d<T>(vol2->pos(x,y,z+1)		,t);
	cell.gridpoints.array[5] = point4d<T>(vol2->pos(x+1,y,z+1)		,t);
	cell.gridpoints.array[6] = point4d<T>(vol2->pos(x,y+1,z+1)		,t);
	cell.gridpoints.array[7] = point4d<T>(vol2->pos(x+1,y+1,z+1)	,t);
	cell.gridpoints.array[8] = point4d<T>(vol2->pos(x,y,z)			,t_plus_1);
	cell.gridpoints.array[9] = point4d<T>(vol2->pos(x+1,y,z)		,t_plus_1);
	cell.gridpoints.array[10] = point4d<T>(vol2->pos(x,y+1,z)		,t_plus_1);
	cell.gridpoints.array[11] = point4d<T>(vol2->pos(x+1,y+1,z)		,t_plus_1);
	cell.gridpoints.array[12] = point4d<T>(vol2->pos(x,y,z+1)		,t_plus_1);
	cell.gridpoints.array[13] = point4d<T>(vol2->pos(x+1,y,z+1)		,t_plus_1);
	cell.gridpoints.array[14] = point4d<T>(vol2->pos(x,y+1,z+1)		,t_plus_1);
	cell.gridpoints.array[15] = point4d<T>(vol2->pos(x+1,y+1,z+1)	,t_plus_1);

}

// ****************************************************************************************

template<class T>
void processTimeSlice(Volume<T>* vol1, Volume<T>* vol2, float iso, std::vector<UInt>* vertices_container, JBSlib::DFOcTree< JBSlib::DFOcNode< point4d<T> > >* octree, T t = 0, T t_increment = 1) {
	UInt dx = vol1->getDimX();
	UInt dy = vol1->getDimY();
	UInt dz = vol1->getDimZ();

	T t_plus_1 = t+t_increment;
	Cell4D< T,point4d<T> > cell;

	for (UInt x = 0; x < dx-1; x++) {
		std::cerr << x << " / " << dx << "            \r";
		for (UInt y = 0; y < dy-1; y++) {
			for (UInt z = 0; z < dz-1; z++) {
				fillCell(cell, vol1, vol2, x,y,z, t, t_plus_1);
				cell.GetIsoTetrahedra(iso, vertices_container, octree);
			}
		}
	}
}

// ****************************************************************************************

template<class T>
void processTimeSliceCheckNaN(Volume<T>* vol1, Volume<T>* vol2, float iso, std::vector<UInt>* vertices_container, JBSlib::DFOcTree< JBSlib::DFOcNode< point4d<T> > >* octree, T t = 0, T t_increment = 1) {
	UInt dx = vol1->getDimX();
	UInt dy = vol1->getDimY();
	UInt dz = vol1->getDimZ();

	T t_plus_1 = t+t_increment;

	Cell4D< T,point4d<T> > cell;

	for (UInt x = 0; x < dx-1; x++) {
		std::cerr << x << " / " << dx << "            \r";
		for (UInt y = 0; y < dy-1; y++) {
			for (UInt z = 0; z < dz-1; z++) {
				fillCell(cell, vol1, vol2, x,y,z, t, t_plus_1);
				bool isnan = false;
				for (UInt i = 0; i < 16; ++i) {
					if (cell[i] != cell[i]) {
						isnan = true;
						break;
					}
				}
				if (!isnan) {
					cell.GetIsoTetrahedra(iso, vertices_container, octree);
				}
			}
		}
	}
}

// ****************************************************************************************

template<class T>
void processTimeSlice(Volume<T>* vol1, Volume<T>* vol2, float iso, IndexedTetraMesh<T>* mesh, JBSlib::DFOcTree< JBSlib::DFOcNode< point4d<T> > >* octree, T t = 0, T t_increment = 1) {
	UInt dx = vol1->getDimX();
	UInt dy = vol1->getDimY();
	UInt dz = vol1->getDimZ();

	T t_plus_1 = t+t_increment;

	Cell4D< T,point4d<T> > cell;

	for (UInt x = 0; x < dx-1; x++) {
		std::cerr << x << " / " << dx << "            \r";
		for (UInt y = 0; y < dy-1; y++) {
			for (UInt z = 0; z < dz-1; z++) {
				fillCell(cell, vol1, vol2, x,y,z, t, t_plus_1);
				cell.GetIsoTetrahedra(iso, mesh, octree);
			}
		}
	}
}

// ****************************************************************************************

template<class T>
void processTimeSlice(Volume<T>* vol1, Volume<T>* vol2, float iso, TetraMesh4D<T>* mesh, T t = 0, T t_increment = 1) {
	UInt dx = vol1->getDimX();
	UInt dy = vol1->getDimY();
	UInt dz = vol1->getDimZ();

	dx = dy = dz = 50;

	T t_plus_1 = t+t_increment;

	Cell4D< T,point4d<T> > cell;

	for (UInt x = 0; x < dx-1; x++) {
		std::cerr << x << " / " << dx << "            \r";
		for (UInt y = 0; y < dy-1; y++) {
			for (UInt z = 0; z < dz-1; z++) {
				fillCell(cell, vol1, vol2, x,y,z, t, t_plus_1);
				cell.GetIsoTetrahedra(iso, mesh);
			}
		}
	}
}

// ****************************************************************************************

#endif
