#ifndef JBS_POINT_CLOUD_H
#define JBS_POINT_CLOUD_H

#include <vector>
#include <algorithm>
#include <limits>
#include "JBS_General.h"

#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif

template<class T>
struct sort_pred {
	bool operator () (const T& left, const T& right) {
		//return left.z < right.z;
		return left[T::dim-1] < right[T::dim-1];
	}
};



//! This class represents a pointcloud or unstructured pointset, i.e. it holds nothing but a vector of 2D/3D points
template<class T>
class PointCloud {

public:

	//! Type of a Point, for example a 3D Vector of floats.
	typedef T Point;

	//! Value-Type of Templates components (i.e. float or double).
	typedef typename T::ValueType ValueType;

	//! Dimension of the pointcloud.
	static const UInt dim = T::dim;

	PointCloud() {
		max = Point(-std::numeric_limits<ValueType>::max());
		min = Point(std::numeric_limits<ValueType>::max());
	}
	
	virtual ~PointCloud() {}

	//! Recomputes all vertices as pt_new = (pt+move)*scale;
	inline void resize(ValueType scale, Point move) {
		for (int i1 = 0; i1 < getNumPts(); i1++) {
			m_pts[i1] = (m_pts[i1]+move)*scale;
		}
	}

	void recomputeMinMax() {
		max = Point(-std::numeric_limits<ValueType>::max());
		min = Point(std::numeric_limits<ValueType>::max());

		for (int i1 = 0; i1 < getNumPts(); i1++) {
			for (UInt i = 0; i < dim; ++i) {
				min[i] = std::min(min[i], m_pts[i1][i]);
				max[i] = std::max(max[i], m_pts[i1][i]);
			}
		}
	}
	

	//! Add a point to the pointcloud
	virtual void insertPoint(const T& pt) {

/*		const ValueType& x = pt[0];
		const ValueType& y = pt[1];
		const ValueType& z = pt[2];
		// Calculate extend of the Mesh
		if(getNumPts() == 0) {
			min[0] = x;
			max[0] = x;
			min[1] = y;
			max[1] = y;
			min[2] = z;
			max[2] = z;
		} else {
			if(x < min[0])
				min[0] = x;
			if(x > max[0])
				max[0] = x;
			if(y < min[1])
				min[1] = y;
			if(y > max[1])
				max[1] = y;
			if(z < min[2])
				min[2] = z;
			if(z > max[2])
				max[2] = z;
		}*/
		
		// Calculate extend of the Mesh
		//if(getNumPts() == 0) {
		//	for (UInt i = 0; i < dim; ++i) {
		//		min[i] = max[i] = pt[i];
		//	}
		//} else {
		for (UInt i = 0; i < dim; ++i) {
			min[i] = std::min(min[i], pt[i]);
			max[i] = std::max(max[i], pt[i]);
		}
		//}

		m_pts.push_back(pt);
	};

	//! Gets the number of points in the pointcloud
	int getNumPts() const {
		return (int)m_pts.size();
	};

	//! Returns an array of the points
	const Point* getPoints() const {
		return &m_pts[0];
	};

	//! Returns an array of the points
	Point* getPoints() {
		return &m_pts[0];
	};

	//! Returns the point with index 'i'
	inline Point getPoint(int i) {
		return m_pts[i];
	};

	//! Lower left front point of the extend.
	Point getMin() {
		return min;
	};

	//! Upper right back point of the extend.
	Point getMax() {
		return max;
	};

	//! Prints all points.
	void print() {
		for (UInt i1 = 0; i1 < m_pts.size(); i1++) {
			m_pts[i1].print();
		}
	}

	//! Scales and moves points such that they all lie within [0.025,0.025,0.025]x[0.975,0.975,0.975]
	/*!
		\param scale_factor is a return value which stores the scaling of the normalization.
		\param move is the offset of the normalization.
		in order to recover the original PointCloud, you will have to evaluate:
		pt_org = (pt_normalized + move)*scale_factor;
	*/
	void normalizeConservative(ValueType& scale_factor, Point& move) {
		scale_factor = 0;
		for (UInt i1 = 0; i1 < dim; i1++) {
			scale_factor = std::max((max[i1]-min[i1]), scale_factor);
		}

		scale_factor *= (ValueType)1.05;

		Point min_tmp = min/scale_factor;
		Point max_tmp = max/scale_factor;
		move = ((max_tmp+min_tmp)/(ValueType)2)-Point((ValueType)0.5);

		for (UInt i1 = 0; i1 < m_pts.size(); i1++) {
			m_pts[i1] = (m_pts[i1]/scale_factor) - move;
			//m_pts[i1].print();
		}

		recomputeMinMax();
		//std::cerr << min << "  " << max << std::endl;
	}

	//! Scales and moves points such that they all lie within [0,0,0]x[1,1,1]
	/*!
		\param scale_factor is a return value which stores the scaling of the normalization.
		\param move is the offset of the normalization.
		in order to recover the original PointCloud, you will have to evaluate:
		pt_org = (pt_normalized + move)*scale_factor;
	*/
	void normalize(ValueType& scale_factor, Point& move) {
		scale_factor = 0;
		for (UInt i1 = 0; i1 < dim; i1++) {
			scale_factor = std::max((max[i1]-min[i1]), scale_factor);
		}

		Point min_tmp = min/scale_factor;
		Point max_tmp = max/scale_factor;
		move = ((max_tmp+min_tmp)/(ValueType)2)-Point((ValueType)0.5);

		for (UInt i1 = 0; i1 < m_pts.size(); i1++) {
			m_pts[i1] = (m_pts[i1]/scale_factor) - move;
			//m_pts[i1].print();
		}

		recomputeMinMax();
		//std::cerr << min << "  " << max << std::endl;
	}

	//! Sorts the point in z-dir.
	void sort_Z() {
		std::sort(m_pts.begin(), m_pts.end(), sort_pred<T>());
	}

	virtual void null() {};

	//! Storage for the points of the pointset.
	std::vector<T> m_pts;

	//! Extend of the pointcloud, min[0] denotes the smallest x value, max[1] the largest y value...
	T min, max;

protected:


};



//****************************************************************************************************


//! This class represents a pointcloud with normals
template<class T>
class PointCloudNormals : public PointCloud<T> {

public:

	PointCloudNormals(): PointCloud() {
	}

	//! Destruktor, frees some Memory
	PointCloudNormals(const PointCloudNormals& other) {
		min = other.min;
		max = other.max;
		m_pts = other.m_pts;
		m_normals = other.m_normals;
	}

	//! Destruktor, frees some Memory
	~PointCloudNormals() {
		m_pts.clear();
		m_normals.clear();
	}

	//! Add a point to the pointcloud
	void insertPoint(const T& pt, const T& normal) {
		PointCloud<T>::insertPoint(pt);
		m_normals.push_back(normal);
	}

	//! Returns an array of the points
	const T* getNormals() const{
		return &m_normals[0];
	};
	
	//! Returns an array of the points
	T* getNormals() {
		return &m_normals[0];
	};

	//! Storage for the normals of the pointset.
	std::vector<T> m_normals;

private:

};

#endif
