#ifndef RBF_TO_IV_H
#define RBF_TO_IV_H


#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoQuadMesh.h>
#include "rbf.h"
#include "fit_rbf.h"
#include "PointCloudToIV.h"
#include "Volume.h"
#include "Polyline.h"
//#include "RBFTree.h"
#include "iv_stuff/vecFieldToIv.h"
#include "iv_stuff/fancy_materials.h"

template <class REAL>
Volume<REAL>* sampleFunction(ScalarFunction< point2d<REAL> >& func, vec2d minV, vec2d maxV, int bricksMax=200) {
	vec2d diag = maxV-minV;
	double scale = 0.048;
	scale = 0;
	minV -= diag*scale;
	maxV += diag*scale;

	double brick_size = std::max(maxV[0] - minV[0], maxV[1] - minV[1]) / (double)bricksMax;
	//std::cerr << "brick_size: " << brick_size << std::endl;
	const int brickX = 2 + (unsigned int)((maxV[0] - minV[0]) / brick_size);
	const int brickY = 2 + (unsigned int)((maxV[1] - minV[1]) / brick_size);
	Volume<REAL>* vol = new Volume<REAL>;
	vol->initWithDims(vec3d(minV.x, minV.y, -0.01), vec3d(maxV.x, maxV.y, 0), brickX, brickY, 1);

	REAL d;
	vec2d grad;
	for (int i = 0; i < brickX; ++i) {
		for (int j = 0; j < brickY; ++j) {
			vec3d pos = vol->pos(i,j,0);
			point2d<REAL> pos2d((REAL)pos.x, (REAL)pos.y);
			d = func.eval(pos2d);
			vol->set(i,j,0, d);
		}
	}
	return vol;
}


//****************************************************************************************************

template <class T>
SoSeparator* CellToIV(T* cell, float col = 0) {

	PolyLine<vec2d> pl;
	vec2d m_min = cell->m_min;
	vec2d m_max = cell->m_max;
	pl.insertVertex(vec2d(m_min.x, m_min.y));
	pl.insertVertex(vec2d(m_max.x, m_min.y));
	pl.insertVertex(vec2d(m_max.x, m_max.y));
	pl.insertVertex(vec2d(m_min.x, m_max.y));

	pl.insertLine(0,1);
	pl.insertLine(1,2);
	pl.insertLine(2,3);
	pl.insertLine(3,0);
	return PolyLineToIV(&pl, col);
}


//****************************************************************************************************

template <class RBF_Type>
SoSeparator* RBFToHeightfield(RBF_sum<RBF_Type>* rbf, vec2d minV, vec2d maxV) {

	vec2d diag = maxV-minV;
	minV -= diag*0.1;
	maxV += diag*0.1;
    
	int stepsMax = 80;
	double brick_size = std::max(maxV[0] - minV[0], maxV[1] - minV[1]) / (double)stepsMax;
	//std::cerr << "brick_size: " << brick_size << std::endl;
	const int brickX = 2 + (unsigned int)((maxV[0] - minV[0]) / brick_size);
	const int brickY = 2 + (unsigned int)((maxV[1] - minV[1]) / brick_size);
	Volume<double>* vol = new Volume<double>;
	vol->initWithDims(vec3d(minV.x, minV.y, 0), vec3d(maxV.x, maxV.y, 0), brickX, brickY, 1);

	double d;
	vec2d grad;
	for (int i = 0; i < brickX; ++i) {
		for (int j = 0; j < brickY; ++j) {
			vec3d pos = vol->pos(i,j,0);
			vec2d pos2d(pos.x, pos.y);
			d = rbf->eval(RBF_Type::Point((RBF_Type::Point::ValueType)pos2d.x, (RBF_Type::Point::ValueType)pos2d.y));
			//d = rbf->evalNormalize(pos2d);
			vol->set(i,j,0, d);
		}
	}

	return RBFToHeightfield(vol);
}

//****************************************************************************************************

template <class T>
SoSeparator* RBFToHeightfield(Volume<T>* vol) {

	SoSeparator* iv_pc = new SoSeparator();
	UInt numX = vol->dx;
	UInt numY = vol->dy;

	//SoShapeHints* sh = new SoShapeHints();
	//sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	//sh->creaseAngle = 0.1f;
	//iv_pc->addChild(sh);


	SbVec3f* points = new SbVec3f[numX*numY];
	for (UInt i1 = 0; i1 < numX; i1++) {
		for (UInt i2 = 0; i2 < numY; i2++) {
			vec3f pos = vol->pos(i1, i2, 0);
			pos.z = (float)vol->get(i1, i2, 0)/1;
			points[i1*numY+i2] = SbVec3f(pos.array);
		}
	}

	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numX*numY, points);
	iv_pc->addChild(coord3);
	delete[] points;

	SoQuadMesh* qm = new SoQuadMesh;
	qm->verticesPerColumn = numX;
	qm->verticesPerRow = numY;
	iv_pc->addChild(qm);

	return iv_pc;

}

//****************************************************************************************************

template <class Point>
SoSeparator* FunctionSamplesWithNormalsToIV(RBFFunctionSamples<Point>* fs, int pointSize = 2, bool addZeroSamples = false) {

	SoSeparator* iv_pc = new SoSeparator();

	PointCloud<vec3f> pc_in;
	PointCloud<vec3f> pc_out;
	for (UInt i1 = 0; i1 < fs->numNonZeroSamples; i1++) {
		vec3f pt = vec3f((float)fs->positions[i1].x, (float)fs->positions[i1].y, (float)fs->values[i1]);
		float height = pt.z;
		pt.z = 0;
		if (height < 0)
			pc_in.insertPoint(pt);
		else
			pc_out.insertPoint(pt);
	}

	iv_pc->addChild(pointcloudToIV(&pc_in, vec3f(1,0,0), pointSize));
	iv_pc->addChild(pointcloudToIV(&pc_out, vec3f(0,1,0), pointSize));
	std::vector< point3d<Point::ValueType> > dirs;
	std::vector< point3d<Point::ValueType> > poss;
	std::cerr << fs->directions.size() << std::endl;
	for (UInt i1 = 0; i1 < fs->directions.size(); i1++) {
		point3d<Point::ValueType> pt(fs->directions[i1].x, fs->directions[i1].y, 0.01);
		point3d<Point::ValueType> ps(fs->positions[i1].x, fs->positions[i1].y, 0);
		dirs.push_back(pt);
		poss.push_back(ps);
	}
	iv_pc->addChild(vectorFieldToIV(poss, dirs, 0.003f, 0.01f));

	if (addZeroSamples) {
		PointCloud<Point> pc_on;
		for (UInt i1 = fs->numNonZeroSamples; i1 < fs->positions.size(); i1++) {
			pc_on.insertPoint(fs->positions[i1]);
		}
		iv_pc->addChild(pointcloudToIV(&pc_on, vec3f(1,1,1), pointSize));
	}

	return iv_pc;

}

//****************************************************************************************************

template <class Point>
SoSeparator* FunctionSamplesToIV(RBFFunctionSamplesBase<Point>* fs, int pointSize = 2, bool addZeroSamples = false, bool colorBlack = false, float z_move=0, vec3f col2 = vec3f(0,0,0)) {

	SoSeparator* iv_pc = new SoSeparator();

	PointCloud<vec3f> pc_in;
	PointCloud<vec3f> pc_out;
	PointCloud<vec3f> pc_on;

	for (UInt i1 = 0; i1 < fs->getNumSamples(); i1++) {
		vec3f pt = vec3f((float)fs->getSamplePos(i1).x, (float)fs->getSamplePos(i1).y, (float)fs->getSampleVal(i1));

		float height = pt.z;
		pt.z = 0;
		pt.z += z_move;
		if (height == 0)
			pc_on.insertPoint(pt);
		else if (height < 0)
			pc_in.insertPoint(pt);
		else
			pc_out.insertPoint(pt);
	}

	if (pc_in.getNumPts() > 0) iv_pc->addChild(pointcloudToIV(&pc_in, vec3f(1,0,0), pointSize));
	if (pc_out.getNumPts() > 0) iv_pc->addChild(pointcloudToIV(&pc_out, vec3f(0,1,0), pointSize));
	
	if (colorBlack) {
		if (addZeroSamples) iv_pc->addChild(pointcloudToIV(&pc_on, vec3f(0,0,0), pointSize));
	} else {
		if (addZeroSamples) {
			vec3f col = vec3f(1,1,1);
			if (col2.squaredLength() > 0)
				col = col2;
			iv_pc->addChild(pointcloudToIV(&pc_on, col, pointSize));
		}
	}

	return iv_pc;

}


//****************************************************************************************************


template <class RBF_Type>
SoSeparator* RBFToIVSphere(RBF_sum<RBF_Type>* rbf, SoMaterial* mat = NULL) {

	SoSeparator* iv_pc = new SoSeparator();

	if (mat == NULL)
		mat = JBSmaterials::matCoper();

	iv_pc->addChild(mat);

	SoSphere* sp = new SoSphere;
	sp->radius = (float)0.02;

	int numOfPoints = rbf->getNumCenters();

	const RBF_Type::Point* pts = rbf->getPoints();
	
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		SoSeparator* sep = new SoSeparator;
		SoTransform* trans = new SoTransform;
		trans->translation.setValue((float)pts[i1].x, (float)pts[i1].y, (float)pts[i1].z);
		sep->addChild(trans);
		sep->addChild(sp);
		iv_pc->addChild(sep);
	}

	return iv_pc;
}


//****************************************************************************************************

template <class RBF_Type>
SoSeparator* RBFToIV(RBF_sum<RBF_Type>* rbf, vec3f color, int pointSize = 2) {

	typedef RBF_Type::Point Point;

	SoSeparator* iv_pc = new SoSeparator();
	int numOfPoints = rbf->getNumCenters();

	std::cerr << "numOfPoints: " << numOfPoints << std::endl;

	const Point* pts = rbf->getPoints();
	SbVec3f* points = new SbVec3f[numOfPoints];
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		for (UInt x = 0; x < Point::dim && x < 3; x++)
			points[i1][x] = (float)pts[i1][x];
		for (UInt x = Point::dim; x < 3; x++)
			points[i1][x] = 0;
	}


	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.set1Value(0, color.x, color.y, color.z);
	iv_pc->addChild(mat);


	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numOfPoints, points);
	iv_pc->addChild(coord3);
	SoDrawStyle* ds = new SoDrawStyle;
	ds->pointSize = (float)pointSize;
	iv_pc->addChild(ds);
	SoPointSet* pointset = new SoPointSet;
	iv_pc->addChild(pointset);

	delete[] points;

	return iv_pc;
}


//****************************************************************************************************


#endif
