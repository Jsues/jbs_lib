#ifndef LINEAR_INTERPOLATE_H
#define LINEAR_INTERPOLATE_H

#include "point3d.h"

template<class T>
T LinInterpolate(T p0, T p1, double v0, double v1,  double v) {

	double dist = abs(v0-v1);

	double u = 0.5; //v0/dist;

	return p0*(1-u)+p1*(u);
}

#endif
