#ifndef ENC_REL_ROT_H
#define ENC_REL_ROT_H


#include <fstream>
#include <vector>

#include "SimpleMesh.h"
#include "MatrixnD.h"




struct RotInvEncMesh {

	static const int entriesPerFace = 15;

	RotInvEncMesh(SimpleMesh* sm_template) : m_sm_template(sm_template){
		numFaces = sm_template->getNumT();
		m_data.resize(entriesPerFace*numFaces);
	}

	~RotInvEncMesh() {
	}

	void removeAverage(const RotInvEncMesh& avg) {
		for (int i1 = 0; i1 < numFaces*15; i1++) {
			m_data[i1] -= avg.m_data[i1];
		}
	}

	void addAverage(const RotInvEncMesh& avg) {
		for (int i1 = 0; i1 < numFaces*15; i1++) {
			m_data[i1] += avg.m_data[i1];
		}
	}

	void interpolate(const RotInvEncMesh& other, double fac = 0.5) {
		for (int i1 = 0; i1 < numFaces*15; i1++) {
			const double& t = m_data[i1];
			const double& o = other.m_data[i1];
			m_data[i1] = t*(1-fac)+o*fac;
		}
	}

	void writeASCII(const char* filename) {
		std::ofstream fout(filename);

		for (int i1 = 0; i1 < (int)m_data.size(); i1++)
			fout << m_data[i1] << std::endl;

		fout.close();
	}

	void readFromASCII(const char* filename) {
		std::ifstream fin(filename);

		for (int i1 = 0; i1 < (int)m_data.size(); i1++)
			fin >> m_data[i1];

		fin.close();
	}

	void readFromFile(const char* filename) {
		std::ifstream fin(filename, std::ios::binary);

		int numF;
		fin.read((char*)&numF, sizeof(int));
		if (numF != numFaces) {
			std::cerr << "Error in RotInvEncMesh::readFromFile()" << std::endl;
			std::cerr << "Incompatible file size" << std::endl;
			std::cerr << "File has " << numF << " faces, should have " << numFaces << std::endl;
			exit(1);
		}
		fin.read((char*)&m_data[0], sizeof(double)*entriesPerFace*numFaces);

		fin.close();
	}

	void writeToFile(const char* filename) const {

		std::ofstream fout(filename, std::ios::binary);

		fout.write((char*)&numFaces, sizeof(int));
		fout.write((char*)&m_data[0], sizeof(double)*entriesPerFace*numFaces);

		fout.close();
	}

	void print() const {
		for (int i1 = 0; i1 < numFaces*15; i1++) {
			const double& t = m_data[i1];
			std::cerr << t << std::endl;
		}
	}

	void print100() const {
		for (int i1 = 0; i1 < numFaces*15 && i1 < 100; i1++) {
			const double& t = m_data[i1];
			std::cerr << t << std::endl;
		}
	}

	SimpleMesh* m_sm_template;

	int numFaces;

	std::vector<double> m_data;
};


// works
std::vector<double> getDefGrads(SimpleMesh* sm, SimpleMesh* sm_template) {

	typedef SquareMatrixND<vec3d> M3D;


	vec3d N;
	vec3d tn;

	int numFaces = sm_template->getNumT();

	std::vector<double> data;
	data.resize(numFaces*9);

	for (int i1 = 0; i1 < numFaces; i1++) {
		sm->tList[i1]->intFlag = i1;
		sm_template->tList[i1]->intFlag = i1;
		const Triangle* t = sm->tList[i1];
		const vec3d& p0 = sm->vList[t->m_v0].c;
		const vec3d& p1 = sm->vList[t->m_v1].c;
		const vec3d& p2 = sm->vList[t->m_v2].c;
		vec3d e1 = p1-p0;
		vec3d e2 = p2-p0;
		vec3d n = e1^e2;
		n.normalize();
		M3D tgt;
		tgt.setColI(e1, 0);
		tgt.setColI(e2, 1);
		tgt.setColI(n, 2);


		const Triangle* t2 = sm_template->tList[i1];
		const vec3d& p20 = sm_template->vList[t2->m_v0].c;
		const vec3d& p21 = sm_template->vList[t2->m_v1].c;
		const vec3d& p22 = sm_template->vList[t2->m_v2].c;
		vec3d e21 = p21-p20;
		vec3d e22 = p22-p20;
		vec3d n2 = e21^e22;
		n2.normalize();
		M3D src;
		src.setColI(e21, 0);
		src.setColI(e22, 1);
		src.setColI(n2, 2);

		M3D T = tgt.opSlash(src);



		M3D R = T;
		M3D U; vec3d S; M3D V;
		R.SVD_decomp(U,S,V);
		R = U*V.getTransposed();
		if (R.getDeterminant() < 0) {
			R.transpose();
			std::cerr << "R transposed" << std::endl;
		}

		vec3d rv = R.getRotVec();


		M3D SS = R.opBackSlash(T);

		data[i1*9+0] = rv.x;
		data[i1*9+1] = rv.y;
		data[i1*9+2] = rv.z;
		data[i1*9+3] = SS(0,0);
		data[i1*9+4] = SS(0,1);
		data[i1*9+5] = SS(0,2);
		data[i1*9+6] = SS(1,1);
		data[i1*9+7] = SS(1,2);
		data[i1*9+8] = SS(2,2);
	}

	return data;
}



std::vector<double> getDefGradsMatrix(SimpleMesh* sm, SimpleMesh* sm_template) {

	typedef SquareMatrixND<vec3d> M3D;


	vec3d N;
	vec3d tn;

	int numFaces = sm_template->getNumT();

	std::vector<double> data;
	data.resize(numFaces*9);

	for (int i1 = 0; i1 < numFaces; i1++) {
		sm->tList[i1]->intFlag = i1;
		sm_template->tList[i1]->intFlag = i1;
		const Triangle* t = sm->tList[i1];
		const vec3d& p0 = sm->vList[t->m_v0].c;
		const vec3d& p1 = sm->vList[t->m_v1].c;
		const vec3d& p2 = sm->vList[t->m_v2].c;
		vec3d e1 = p1-p0;
		vec3d e2 = p2-p0;
		vec3d n = e1^e2;
		n.normalize();
		M3D tgt;
		tgt.setColI(e1, 0);
		tgt.setColI(e2, 1);
		tgt.setColI(n, 2);


		const Triangle* t2 = sm_template->tList[i1];
		const vec3d& p20 = sm_template->vList[t2->m_v0].c;
		const vec3d& p21 = sm_template->vList[t2->m_v1].c;
		const vec3d& p22 = sm_template->vList[t2->m_v2].c;
		vec3d e21 = p21-p20;
		vec3d e22 = p22-p20;
		vec3d n2 = e21^e22;
		n2.normalize();
		M3D src;
		src.setColI(e21, 0);
		src.setColI(e22, 1);
		src.setColI(n2, 2);

		M3D T = tgt.opSlash(src);



		M3D R = T;
		M3D U; vec3d S; M3D V;
		R.SVD_decomp(U,S,V);
		R = U*V.getTransposed();
		if (R.getDeterminant() < 0) {
			R.transpose();
			std::cerr << "R transposed" << std::endl;
		}

		//vec3d rv = R.getRotVec();
		//M3D SS = R.opBackSlash(T);

		data[i1*9+0] = R(0,0);
		data[i1*9+1] = R(0,1);
		data[i1*9+2] = R(0,2);
		data[i1*9+3] = R(1,0);
		data[i1*9+4] = R(1,1);
		data[i1*9+5] = R(1,2);
		data[i1*9+6] = R(2,0);
		data[i1*9+7] = R(2,1);
		data[i1*9+8] = R(2,2);
	}

	return data;
}



// works
RotInvEncMesh encodeRotInvariant(SimpleMesh* sm, SimpleMesh* sm_template, const std::vector<vec3i>& neigh) {

	typedef SquareMatrixND<vec3d> M3D;

	RotInvEncMesh riem(sm_template);

	vec3d N;
	vec3d tn;

	int numFaces = sm_template->getNumT();

	std::vector<M3D> rots;
	rots.reserve(numFaces);

	for (int i1 = 0; i1 < numFaces; i1++) {
		sm->tList[i1]->intFlag = i1;
		sm_template->tList[i1]->intFlag = i1;
		const Triangle* t = sm->tList[i1];
		const vec3d& p0 = sm->vList[t->m_v0].c;
		const vec3d& p1 = sm->vList[t->m_v1].c;
		const vec3d& p2 = sm->vList[t->m_v2].c;
		vec3d e1 = p1-p0;
		vec3d e2 = p2-p0;
		vec3d n = e1^e2;
		n.normalize();
		M3D tgt;
		tgt.setColI(e1, 0);
		tgt.setColI(e2, 1);
		tgt.setColI(n, 2);


		const Triangle* t2 = sm_template->tList[i1];
		const vec3d& p20 = sm_template->vList[t2->m_v0].c;
		const vec3d& p21 = sm_template->vList[t2->m_v1].c;
		const vec3d& p22 = sm_template->vList[t2->m_v2].c;
		vec3d e21 = p21-p20;
		vec3d e22 = p22-p20;
		vec3d n2 = e21^e22;
		n2.normalize();
		M3D src;
		src.setColI(e21, 0);
		src.setColI(e22, 1);
		src.setColI(n2, 2);

		M3D T = tgt.opSlash(src);



		M3D R = T;
		M3D U; vec3d S; M3D V;
		R.SVD_decomp(U,S,V);
		R = U*V.getTransposed();
		if (R.getDeterminant() < 0) {
			R.transpose();
			std::cerr << "R transposed" << std::endl;
		}
		rots.push_back(R);

		M3D SS = R.opBackSlash(T);

		riem.m_data[i1*RotInvEncMesh::entriesPerFace+9] = SS(0,0);
		riem.m_data[i1*RotInvEncMesh::entriesPerFace+10] = SS(0,1);
		riem.m_data[i1*RotInvEncMesh::entriesPerFace+11] = SS(0,2);
		riem.m_data[i1*RotInvEncMesh::entriesPerFace+12] = SS(1,1);
		riem.m_data[i1*RotInvEncMesh::entriesPerFace+13] = SS(1,2);
		riem.m_data[i1*RotInvEncMesh::entriesPerFace+14] = SS(2,2);
	}

	for (int i1 = 0; i1 < numFaces; i1++) {
		Triangle* t = sm->tList[i1];


		//int tn0 = t->getTriangleNeighbor(0)->intFlag;
		//int tn1 = t->getTriangleNeighbor(1)->intFlag;
		//int tn2 = t->getTriangleNeighbor(2)->intFlag;
		const int& tn0 = neigh[i1][0];
		const int& tn1 = neigh[i1][1];
		const int& tn2 = neigh[i1][2];

		

		//if (i1 < 10) std::cerr << tn0 << " " << tn1 << " " << tn2 << std::endl;

		M3D d0 = rots[i1].opBackSlash(rots[tn0]);
		M3D d1 = rots[i1].opBackSlash(rots[tn1]);
		M3D d2 = rots[i1].opBackSlash(rots[tn2]);


		//if (i1 < 10) {
		//	std::cerr << "rots[i1]" << std::endl;
		//	rots[i1].print(4);
		//	std::cerr << "rots[tn0]" << std::endl;
		//	rots[tn2].print(4);
		//	std::cerr << "delta" << std::endl;
		//	d2.print(4);
		//	std::cerr << std::endl;
		//}


		vec3d rv0 = d0.getRotVec();
		vec3d rv1 = d1.getRotVec();
		vec3d rv2 = d2.getRotVec();

		riem.m_data[i1*RotInvEncMesh::entriesPerFace+0] = rv0.x;
		riem.m_data[i1*RotInvEncMesh::entriesPerFace+1] = rv0.y;
		riem.m_data[i1*RotInvEncMesh::entriesPerFace+2] = rv0.z;
		riem.m_data[i1*RotInvEncMesh::entriesPerFace+3] = rv1.x;
		riem.m_data[i1*RotInvEncMesh::entriesPerFace+4] = rv1.y;
		riem.m_data[i1*RotInvEncMesh::entriesPerFace+5] = rv1.z;
		riem.m_data[i1*RotInvEncMesh::entriesPerFace+6] = rv2.x;
		riem.m_data[i1*RotInvEncMesh::entriesPerFace+7] = rv2.y;
		riem.m_data[i1*RotInvEncMesh::entriesPerFace+8] = rv2.z;
	}

	return riem;
}

#endif
