#ifndef JBS_POINT_CLOUD_NOISER_H
#define JBS_POINT_CLOUD_NOISER_H

#include "JBS_General.h"
#include "PointCloud.h"
#include "random.h"
#include "point3d.h"
#include "rng.h"
#include <ctime>


//! This function takes a 'PointCloud*' as input and returns a Pointcloud with gaussian noise.
/*!
	\param pc The input pointcloud
	\param amplitude The width of the is a gausian distributed variable with mean 0 and variance 1 which is multiplied withh 'amplitude'.
	\return the pointcloud with noise.
*/
template <class Point>
PointCloud<Point>* addNoise(PointCloud<Point>* pc, double amplitude, int replaceEachPointsBy = 1) {


	RNG generator(0);


	PointCloud<Point>* ret = new PointCloud<Point>();
	int numOfPoints = pc->getNumPts();

	Point* pts = pc->getPoints();
	float phi, theta;

	for (int i1 = 0; i1 < numOfPoints; i1++) {

		for (int i2 = 0; i2 < replaceEachPointsBy; i2++) {

			phi = (float)generator.uniform() * float(M_PI);
			theta = (float)generator.uniform() * float(M_PI2);

			Point disp;

			if (Point::dim == 3) {
				disp[0] = sin(theta)*cos(phi);
				disp[1] = sin(theta)*sin(phi);
				disp[2] = cos(theta);
			}
			if (Point::dim == 2) {
				disp[0] = sin(theta);
				disp[1] = cos(theta);
			}

			disp *= (float)generator.normal(0, amplitude);

			Point new_pt = pts[i1] + disp;
			ret->insertPoint(new_pt);
		}
	}

	return ret;
}


//! This function takes a 'PointCloud*' as input and returns a Pointcloud with gaussian noise.
/*!
	\param pc The input pointcloud
	\param amplitude The width of the is a gausian distributed variable with mean 0 and variance 1 which is multiplied withh 'amplitude'.
	\return the pointcloud with noise.
*/
template <class Point>
PointCloudNormals<Point>* addNoise(PointCloudNormals<Point>* pc, double amplitude, int replaceEachPointsBy = 1) {


	RNG generator(0);


	PointCloudNormals<Point>* ret = new PointCloudNormals<Point>();
	int numOfPoints = pc->getNumPts();

	Point* pts = pc->getPoints();
	float phi, theta;

	for (int i1 = 0; i1 < numOfPoints; i1++) {

		for (int i2 = 0; i2 < replaceEachPointsBy; i2++) {

			phi = (float)generator.uniform() * float(M_PI);
			theta = (float)generator.uniform() * float(M_PI2);

			Point disp;

			if (Point::dim == 3) {
				disp[0] = sin(theta)*cos(phi);
				disp[1] = sin(theta)*sin(phi);
				disp[2] = cos(theta);
			}
			if (Point::dim == 2) {
				disp[0] = sin(theta);
				disp[1] = cos(theta);
			}

			disp *= (float)generator.normal(0, amplitude);

			Point new_pt = pts[i1] + disp;
			ret->insertPoint(new_pt, pc->getNormals()[i1]);
		}
	}

	return ret;
}

#endif
