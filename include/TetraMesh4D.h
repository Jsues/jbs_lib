#ifndef TETRAMESH4D_H
#define TETRAMESH4D_H

#include <vector>
//for min() and max()
#include <algorithm>
#include <iostream>

#include "point4d.h"
#include "point3d.h"
#include "iso4d.h"
#include "JBS_General.h"
#include "TrivialMesh.h"


// ****************************************************************************************


//! A triangle in 3D.
template <class T>
class Tri3D {

// ****************************************************************************************

	//! Value-Type of Templates components, for example float or double.
	typedef T ValueType;

	//! Type of a 3D Point (vec3f, vec3d).
	typedef point3d<ValueType> Point;

// ****************************************************************************************

	//! Default constructor, does <b>not</b> preinitialize the vertices, i.e. the vertex-positions are undefined!
	Tri3D() {
	}

// ****************************************************************************************

	//! Constructor, create a tetrahedron from 3 given points v0_, v1_ and v3_.
	Tri3D(Point v0_, Point v1_, Point v2_) : v0(v0_), v1(v1_), v2(v2_) {
	}

// ****************************************************************************************

	//! Copy constructor.
	Tri3D(const Tri3D<ValueType>& other) {
		v0 = other.v0;
		v1 = other.v1;
		v2 = other.v2;
	}

// ****************************************************************************************

	Point v0, v1, v2;

// ****************************************************************************************

private:

};


// ****************************************************************************************


#define COMPACTIFY_FAC 0.1

//! A tetrahedron in 4D. i.e. four connected vertices in 4D-space.
template <class T>
class Tet4D {
public:

// ****************************************************************************************

	//! Value-Type of Templates components, for example float or double.
	typedef T ValueType;

	//! Type of a 4D Point (vec4f, vec4d).
	typedef point4d<ValueType> Point4D;

	//! Type of a 3D Point (vec3f, vec3d).
	typedef point3d<ValueType> Point3D;

	//! Type of a Triangle (vec4f, vec4d).
	typedef Tri3D<ValueType> Tri;

// ****************************************************************************************

	//! Default constructor, does <b>not</b> preinitialize the vertices, i.e. the vertex-positions are undefined!
	Tet4D() {
	}

// ****************************************************************************************

	//! Constructor, create a tetrahedron from 4 given points v0_, v1_, v2_ and v3_.
	Tet4D(Point4D v0_, Point4D v1_, Point4D v2_, Point4D v3_) : v0(v0_), v1(v1_), v2(v2_), v3(v3_) {
	}

// ****************************************************************************************

	//! Copy constructor.
	Tet4D(const Tet4D<ValueType>& other) {
		v0 = other.v0;
		v1 = other.v1;
		v2 = other.v2;
		v3 = other.v3;
	}

// ****************************************************************************************

	//! Prints the vertices of the tetraeder:
	void print() const {
		std::cerr << "Tetraeder:" << std::endl;
		v0.print();
		v1.print();
		v2.print();
		v3.print();
	}

// ****************************************************************************************

	//! Returns the 3D interpolation between 'v0' and 'v1' where the time component is 'iso'
	inline static Point3D interpolateCompactify(const Point4D& v0, const Point4D& v1, ValueType iso) {

		if (fabs(v0.w-v1.w) < COMPACTIFY_FAC)
			return(Point3D(v1.x, v1.y, v1.z));

		ValueType mu = (iso - v0.w) / (v1.w - v0.w);

		if (mu < COMPACTIFY_FAC)
			return Point3D(v0.x, v0.y, v0.z);
		if ((1-mu) < COMPACTIFY_FAC)
			return Point3D(v1.x, v1.y, v1.z);

		return Point3D(v0.x+mu*(v1.x-v0.x), v0.y+mu*(v1.y-v0.y), v0.z+mu*(v1.z-v0.z));

	}

// ****************************************************************************************

	//! Returns the 3D interpolation between 'v0' and 'v1' where the time component is 'iso'
	inline static Point3D interpolate(const Point4D& v0, const Point4D& v1, ValueType iso) {
		if (fabs(iso-v0.w) < marching4d::EPSILON)
			return(Point3D(v0.x, v0.y, v0.z));
		if (fabs(iso-v1.w) < marching4d::EPSILON)
			return(Point3D(v1.x, v1.y, v1.z));
		if (fabs(v0.w-v1.w) < marching4d::EPSILON)
			return(Point3D(v1.x, v1.y, v1.z));

		ValueType mu = (iso - v0.w) / (v1.w - v0.w);

		return Point3D(v0.x+mu*(v1.x-v0.x), v0.y+mu*(v1.y-v0.y), v0.z+mu*(v1.z-v0.z));
	}


// ****************************************************************************************

	//! Intersect the 4D Tetrahedron with time 't' and return the number of polygons.
	/*!
		\return the number of vertices that have been extracted (i.e. 3 for one triangle or 4 for two triangles).
		\param t			time.
		\param vertices		a preinitialized array of vertices (must be size 4).
		If the return value is 3, the vertices 'vertices[0]', 'vertices[1]' and 'vertices[2]' form the triangle<br>
		If the return value is 4, the vertices 'vertices[0]', 'vertices[1]' and 'vertices[2]' form the first triangle
		and the vertices 'vertices[2]', 'vertices[1]' and 'vertices[3]' form the second triangle.
	*/
	inline UInt intersectWithTimeCompactify(ValueType t, Point3D* const vertices) const {

		char simplexIndex = 0;
		if (v0.w > t) simplexIndex |= 1;
		if (v1.w > t) simplexIndex |= 2;
		if (v2.w > t) simplexIndex |= 4;
		if (v3.w > t) simplexIndex |= 8;

		char edgesInvolved = marching4d::edgeTableTetrahedron[(int)simplexIndex];

		if (edgesInvolved == 0) // All isovalues are bigger or smaller than 't'.
			return 0;

		// Compute intersections:
		UChar numVertices = 0;

		if (edgesInvolved & 1) // Edge 0 (v0-v1) crosses 't'
			vertices[numVertices++] = interpolateCompactify(v0, v1, t);
		if (edgesInvolved & 2) // Edge 1 (v0-v2) crosses 't'
			vertices[numVertices++] = interpolateCompactify(v0, v2, t);
		if (edgesInvolved & 4) // Edge 2 (v0-v3) crosses 't'
			vertices[numVertices++] = interpolateCompactify(v0, v3, t);
		if (edgesInvolved & 8) // Edge 3 (v1-v2) crosses 't'
			vertices[numVertices++] = interpolateCompactify(v1, v2, t);
		if (edgesInvolved & 16) // Edge 4 (v1-v3) crosses 't'
			vertices[numVertices++] = interpolateCompactify(v1, v3, t);
		if (edgesInvolved & 32) // Edge 5 (v2-v3) crosses 't'
			vertices[numVertices++] = interpolateCompactify(v2, v3, t);

		return numVertices;
	}

// ****************************************************************************************

	//! Intersect the 4D Tetrahedron with time 't' and return the number of polygons.
	/*!
		\return the number of vertices that have been extracted (i.e. 3 for one triangle or 4 for two triangles).
		\param t			time.
		\param vertices		a preinitialized array of vertices (must be size 4).
		If the return value is 3, the vertices 'vertices[0]', 'vertices[1]' and 'vertices[2]' form the triangle<br>
		If the return value is 4, the vertices 'vertices[0]', 'vertices[1]' and 'vertices[2]' form the first triangle
		and the vertices 'vertices[2]', 'vertices[1]' and 'vertices[3]' form the second triangle.
	*/
	inline UInt intersectWithTime(ValueType t, Point3D* const vertices) const {

		char simplexIndex = 0;
		if (v0.w >= t) simplexIndex |= 1;
		if (v1.w >= t) simplexIndex |= 2;
		if (v2.w >= t) simplexIndex |= 4;
		if (v3.w >= t) simplexIndex |= 8;

		char edgesInvolved = marching4d::edgeTableTetrahedron[(int)simplexIndex];

		if (edgesInvolved == 0) // All isovalues are bigger or smaller than 't'.
			return 0;

		// Compute intersections:
		UChar numVertices = 0;

		if (edgesInvolved & 1) // Edge 0 (v0-v1) crosses 't'
			vertices[numVertices++] = interpolate(v0, v1, t);
		if (edgesInvolved & 2) // Edge 1 (v0-v2) crosses 't'
			vertices[numVertices++] = interpolate(v0, v2, t);
		if (edgesInvolved & 4) // Edge 2 (v0-v3) crosses 't'
			vertices[numVertices++] = interpolate(v0, v3, t);
		if (edgesInvolved & 8) // Edge 3 (v1-v2) crosses 't'
			vertices[numVertices++] = interpolate(v1, v2, t);
		if (edgesInvolved & 16) // Edge 4 (v1-v3) crosses 't'
			vertices[numVertices++] = interpolate(v1, v3, t);
		if (edgesInvolved & 32) // Edge 5 (v2-v3) crosses 't'
			vertices[numVertices++] = interpolate(v2, v3, t);

		return numVertices;
	}

// ****************************************************************************************

	//! If the determinant of (a1, a2, a3, a4) is zero, then the tetrahedron is collapsed
	inline static bool isTetrahedron(const Point4D& a1, const Point4D& a2, const Point4D& a3, const Point4D& a4) {

		const ValueType& a11 = a1.x;
		const ValueType& a12 = a1.y;
		const ValueType& a13 = a1.z;
		const ValueType& a14 = a1.w;
		const ValueType& a21 = a2.x;
		const ValueType& a22 = a2.y;
		const ValueType& a23 = a2.z;
		const ValueType& a24 = a2.w;
		const ValueType& a31 = a3.x;
		const ValueType& a32 = a3.y;
		const ValueType& a33 = a3.z;
		const ValueType& a34 = a3.w;
		const ValueType& a41 = a4.x;
		const ValueType& a42 = a4.y;
		const ValueType& a43 = a4.z;
		const ValueType& a44 = a4.w;

		// Formula according to Bronstein, Chapter 4.2.1.2
		ValueType det
			= a31*(a12*a23*a44 + a13*a24*a42 + a14*a22*a43 - a14*a23*a42 - a13*a22*a44 - a12*a24*a43)
			- a32*(a11*a23*a44 + a13*a24*a41 + a14*a21*a43 - a14*a23*a41 - a13*a21*a44 - a11*a24*a43)
			+ a33*(a11*a22*a44 + a12*a24*a41 + a14*a21*a42 - a14*a22*a41 - a12*a21*a44 - a11*a24*a42)
			- a34*(a11*a22*a43 + a12*a23*a41 + a13*a21*a42 - a13*a22*a41 - a12*a21*a43 - a11*a23*a42);

		return (abs(det) > (ValueType)marching4d::EPSILON);

/* Ohne aliases
		ValueType det
			= a3.x*(a1.y*a2.z*a4.w + a1.z*a2.w*a4.y + a1.w*a2.y*a4.z - a1.w*a2.z*a4.y - a1.z*a2.y*a4.w - a1.y*a2.w*a4.z)
			- a3.y*(a1.x*a2.z*a4.w + a1.z*a2.w*a4.x + a1.w*a2.x*a4.z - a1.w*a2.z*a4.x - a1.z*a2.x*a4.w - a1.x*a2.w*a4.z)
			+ a3.z*(a1.x*a2.y*a4.w + a1.y*a2.w*a4.x + a1.w*a2.x*a4.y - a1.w*a2.y*a4.x - a1.y*a2.x*a4.w - a1.x*a2.w*a4.y)
			- a3.w*(a1.x*a2.y*a4.z + a1.y*a2.z*a4.x + a1.z*a2.x*a4.y - a1.z*a2.y*a4.x - a1.y*a2.x*a4.z - a1.x*a2.z*a4.y);
*/
	}

// ****************************************************************************************

	//! The four vertices of the tetrahedron
	Point4D v0, v1, v2, v3;

// ****************************************************************************************

private:

};

typedef Tet4D<float> Tet4Df;
typedef Tet4D<double> Tet4Dd;

// ****************************************************************************************


//! A four-dimensional tetrahedral mesh
template <class T>
class TetraMesh4D {

public:

// ****************************************************************************************

	//! Value-Type of Templates components, for example float or double.
	typedef T ValueType;

	//! Type of a 4D Point (vec4f, vec4d).
	typedef point4d<T> Point4D;

	//! Type of a 3D Point (vec3f, vec3d).
	typedef point3d<T> Point3D;

	//! Tetrahedron Type.
	typedef Tet4D<T> Tetrahedron;

// ****************************************************************************************

	//! Constructor.
	TetraMesh4D(UInt preinitializeNumTetrahedrons = 10000) {
		tets.reserve(preinitializeNumTetrahedrons);

		t_min = 1000000000;
		t_max = -1000000000;
	}

// ****************************************************************************************

	//! Returns the number of tetrahedrons in the mesh.
	inline UInt getNumTets() const {
		return (UInt)tets.size();
	}

// ****************************************************************************************

	//! Returns the current capacity of the tetradedra storage.
	inline UInt getNumTetsAlloc() const {
		return (UInt)tets.capacity();
	}

// ****************************************************************************************

	//! Insert a tetrahedron into the mesh.
	inline void insert(const Tetrahedron& tet) {
		t_min = m_min(t_min, tet.v0.w);
		t_min = m_min(t_min, tet.v1.w);
		t_min = m_min(t_min, tet.v2.w);
		t_min = m_min(t_min, tet.v3.w);
		t_max = m_max(t_max, tet.v0.w);
		t_max = m_max(t_max, tet.v1.w);
		t_max = m_max(t_max, tet.v2.w);
		t_max = m_max(t_max, tet.v3.w);

		tets.push_back(tet);
	}

// ****************************************************************************************

	//! Insert a four points that form an tetrahedron into the mesh.
	inline void insert(const Point4D& v0, const Point4D& v1, const Point4D& v2, const Point4D& v3) {
		t_min = m_min(t_min, v0.w);
		t_min = m_min(t_min, v1.w);
		t_min = m_min(t_min, v2.w);
		t_min = m_min(t_min, v3.w);
		t_max = m_max(t_max, v0.w);
		t_max = m_max(t_max, v1.w);
		t_max = m_max(t_max, v2.w);
		t_max = m_max(t_max, v3.w);

		tets.push_back(Tetrahedron(v0,v1,v2,v3));
	}

// ****************************************************************************************

	//! Computes the intersection of the 4D tetraedra mesh with time 't'.
	inline void intersectWithTimeCompactify(ValueType t, TrivialMesh<Point3D>* const tm) const {

		std::cerr << "Intersect with time: " << t << std::endl;

		std::vector<Point3D> vertices;
		vertices.reserve(4);

		for (UInt i1 = 0; i1 < tets.size(); i1++) {
			UInt num = tets[i1].intersectWithTimeCompactify(t, &vertices[0]);

			// Insert triangles into TrivialMesh
			if (num >= 3) {
				if ((vertices[0] == vertices[1]) || (vertices[0] == vertices[2]) || (vertices[1] == vertices[2])) {
					// void
				} else {
					tm->insertTriangle(vertices[0], vertices[1], vertices[2]);
				}
				if (num == 4) {
					if ((vertices[2] == vertices[1]) || (vertices[2] == vertices[3]) || (vertices[1] == vertices[3])) {
						// void
					} else {
						tm->insertTriangle(vertices[2], vertices[1], vertices[3]);
					}
				}
			}

		}

		std::cerr << (UInt)tm->getNumTris() << " triangles extracted" << std::endl;
	}

// ****************************************************************************************

	//! Computes the intersection of the 4D tetraedra mesh with time 't'.
	inline void intersectWithTime(ValueType t, TrivialMesh<Point3D>* const tm) const {

		std::cerr << "Intersect with time: " << t << std::endl;

		std::vector<Point3D> vertices;
		vertices.reserve(4);

		for (UInt i1 = 0; i1 < tets.size(); i1++) {
			UInt num = tets[i1].intersectWithTime(t, &vertices[0]);

			// Insert triangles into TrivialMesh
			if (num >= 3) {
				tm->insertTriangle(vertices[0], vertices[1], vertices[2]);
				if (num == 4) {
					tm->insertTriangle(vertices[2], vertices[1], vertices[3]);
				}
			}

		}

		std::cerr << (UInt)tm->getNumTris() << " triangles extracted" << std::endl;
	}

// ****************************************************************************************

	//! Prints out some very basic statistic (number of tetrahedra contained).
	inline void printStats() const {
		std::cerr << "TetraMesh4D: Statistics" << std::endl;
		std::cerr << "- I have " << (UInt)tets.size() << " tetrahedra." << std::endl;
	}

// ****************************************************************************************

	//! returns the 'i'-th tetrahedron.
	inline Tetrahedron getTetrahedron(UInt i) {
		return tets[i];
	}

// ****************************************************************************************

	//! writes the mesh to a binary file, for file format read documentation detail!
	/*!
		File format:
		- first byte denotes the floating point type of the 4d vertices. (1 = float, 2 = double) (i.e. if the 1th byte is a 1, then each tetrahedron consists of 4 vec4f vertices, capisce?)
		- bytes 2-5 are an 'unsigned int' which denotes the number of tetrahedra in the mesh<BR>
		...
	*/
	void writeMeshToBinary(char* filename);

// ****************************************************************************************

	//! writes the mesh to a binary file, for file format read documentation detail!
	/*!
		File format:
		- first byte denotes the floating point type of the 4d vertices. (1 = float, 2 = double) (i.e. if the 1th byte is a 1, then each tetrahedron consists of 4 vec4f vertices, capisce?)
		- bytes 2-5 are an 'unsigned int' which denotes the number of tetrahedra in the mesh<BR>
		...
	*/
	void readMeshFromBinary(char* filename);

// ****************************************************************************************

	Tetrahedron* getTetrahedraPts() {
		return &tets[0];
	}

// ****************************************************************************************

	inline ValueType getT_max() const {
		return t_max;
	}

// ****************************************************************************************

	inline ValueType getT_min() const {
		return t_min;
	}

// ****************************************************************************************

private:

// ****************************************************************************************

	ValueType m_min(ValueType x, ValueType y) {
		return (x < y) ? x : y;
	}

// ****************************************************************************************

	ValueType m_max(ValueType x, ValueType y) {
		return (x > y) ? x : y;
	}

// ****************************************************************************************

	std::vector<Tetrahedron> tets;

	ValueType t_min;
	ValueType t_max;

};

// ****************************************************************************************

typedef TetraMesh4D<double> TetraMesh4Dd;
typedef TetraMesh4D<float> TetraMesh4Df;

// ****************************************************************************************

#endif
