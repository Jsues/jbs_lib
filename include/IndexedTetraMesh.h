#ifndef INDEXED_TETRA_MESH_H
#define INDEXED_TETRA_MESH_H


#include "point4d.h"
#include "JBS_General.h"
#include "SimpleMesh.h"
#include "hsv2rgb.h"
#include "TrivialMesh.h"
#include "iso4d.h"
#include "MatrixnD.h"

#include "determinant.h"

#include <vector>
#include <limits>
#include <map>

template<class T> class TetraVertex;
template<class T> class Tetrahedron;
template<class T> class TetraEdge;
template<class T> class IndexedTetraMesh;
template<class T> class StreamingIndexedTetraMesh;


// ****************************************************************************************
// ****************************************************************************************


//! A vertex of a 4D-Tetrahedron-Mesh.
template<class T>
class TetraVertex {

public:

// ****************************************************************************************

	//! Value-Type of Templates components, for example float or double.
	typedef T ValueType;

	//! Type of a 4D Point (vec4f, vec4d).
	typedef point4d<ValueType> Point4D;

	//! Allow IndexedTetraMesh to access our private members.
	friend class IndexedTetraMesh<ValueType>;

	//! Allow Tetrahedron to access our private members.
	friend class Tetrahedron<ValueType>;

	friend class StreamingIndexedTetraMesh<ValueType>;

// ****************************************************************************************

	TetraVertex(const Point4D& c_, IndexedTetraMesh<ValueType>* m_mesh_) : c(c_), m_mesh(m_mesh_), flag(-1) {}

	~TetraVertex() {}

// ****************************************************************************************

	inline int getEdge(UInt v0, UInt v1, UInt v2) const {
		for (UInt i1 = 0; i1 < edges.size(); i1++) {
			if (m_mesh->eList[edges[i1]].consistsOf(v0, v1, v2)) {
				return edges[i1];
			}
		}
		// not found
		return -1;
	}

// ****************************************************************************************

	inline void print() const {
		std::cerr << "Vertex: " << c.x << ", " << c.y << ", " << c.z << ", " << c.w << std::endl;
	}

// ****************************************************************************************

	inline UInt getMemoryRequired() const {
		UInt memory = 0;
		memory += 8*sizeof(ValueType);	// 'c' and 'normal'
		memory += sizeof(int*);			// *m_mesh;
		memory += sizeof(std::vector<UInt>)+sizeof(UInt)*(UInt)edges.capacity();
		return memory;
	}

// ****************************************************************************************

	inline UInt getEdge(UInt i) const {
		return edges[i];
	}

// ****************************************************************************************

	inline UInt getNumEdges() const {
		return (UInt)edges.size();
	}

// ****************************************************************************************

	inline Point4D getNormal() const {
		return normal;
	}

// ****************************************************************************************

	inline Point4D getPoint() const {
		return c;
	}

// ****************************************************************************************

	int flag;

	//! Coordinates of the vertex.
	Point4D c;
private:


	//! Normal of the vertex.
	Point4D normal;
	//! 'mothermesh'.
	IndexedTetraMesh<ValueType>* m_mesh;
	//! Edges = Triangles of the Tetrahedron.
	std::vector<UInt> edges;

// ****************************************************************************************

};


// ****************************************************************************************
// ****************************************************************************************


//! An edge of a tetrahedron = a triangle in 4D space
template<class T>
class TetraEdge {

public:

// ****************************************************************************************

	//! Value-Type of Templates components, for example float or double.
	typedef T ValueType;

	//! Type of a 4D Point (vec4f, vec4d).
	typedef point4d<ValueType> Point4D;

	//! Allow IndexedTetraMesh to access our private members.
	friend class IndexedTetraMesh<ValueType>;

// ****************************************************************************************

	TetraEdge(UInt v0_, UInt v1_, UInt v2_, IndexedTetraMesh<ValueType>* m_mesh_) : v0(v0_), v1(v1_), v2(v2_), m_mesh(m_mesh_) {}

// ****************************************************************************************

	~TetraEdge() {}

// ****************************************************************************************

	void Swap() {
	}

// ****************************************************************************************

	//! Returns 'true' if the vertices of this edge (triangle) are 'v0_', 'v1_' and 'v2_'(in any order).
	inline bool consistsOf(UInt v0_, UInt v1_, UInt v2_) const {
		if (v0_ == v0) {
			if (v1_ == v1) {
				if (v2_ == v2) return true;
				/* else */ return false;	
			} else if (v1_ == v2) {
				if (v2_ == v1) return true;
				/* else */ return false;	
			}
			/* else */ return false;	
		} else if (v0_ == v1) {
			if (v1_ == v0) {
				if (v2_ == v2) return true;
				/* else */ return false;	
			} else if (v1_ == v2) {
				if (v2_ == v0) return true;
				/* else */ return false;	
			}
			/* else */ return false;	
		} else if (v0_ == v2) {
			if (v1_ == v0) {
				if (v2_ == v1) return true;
				/* else */ return false;	
			} else if (v1_ == v1) {
				if (v2_ == v0) return true;
				/* else */ return false;	
			}
			/* else */ return false;	
		}
		/* else */ return false;
	}
	
// ****************************************************************************************

	inline void print() const {
		std::cerr << "Edge: " << v0 << ", " << v1 << ", " << v2 << "\t " << (UInt)tets.size() << " adjacent tets" << std::endl;
	}

// ****************************************************************************************

	//! Checks if the edge is a boundary edge (number of adjacent tets = 1).
	inline bool isBoundaryEdge() const {
		return (tets.size() == 1);
	}

// ****************************************************************************************

	inline UInt getMemoryRequired() const {
		UInt memory = 0;
		memory += 3*sizeof(UInt);		// 'v0', 'v1' and 'v2'
		memory += sizeof(int*);			// *m_mesh;
		memory += sizeof(std::vector<UInt>)+sizeof(UInt)*(UInt)tets.capacity();
		return memory;
	}

// ****************************************************************************************

	inline UInt getNumTets() const {
		return (UInt)tets.size();
	}

// ****************************************************************************************

	inline UInt getTet(UInt i) const {
		return tets[i];
	}

// ****************************************************************************************

	inline UInt getVertex(UInt i) const {
		if (i == 0) return v0;
		else if (i == 1) return v1;
		else return v2;
	}

// ****************************************************************************************

private:

// ****************************************************************************************

	//! Vertex 0 of the triangle.
	UInt v0;
	//! Vertex 1 of the triangle.
	UInt v1;
	//! Vertex 2 of the triangle.
	UInt v2;
	//! 'mothermesh'.
	IndexedTetraMesh<ValueType>* m_mesh;
	// Vector of Tetrahedrons adjacent to this Edge.
	std::vector<UInt> tets;

// ****************************************************************************************

};


// ****************************************************************************************
// ****************************************************************************************


//! A tetrahedron in an indexed tetrahedra-mesh.
template<class T>
class Tetrahedron {

public:

// ****************************************************************************************

	//! Value-Type of Templates components, for example float or double.
	typedef T ValueType;

	//! Type of a 4D Point (vec4f, vec4d).
	typedef point4d<ValueType> Point4D;

	//! Allow IndexedTetraMesh to access our private members.
	friend class IndexedTetraMesh<ValueType>;

// ****************************************************************************************

	//! Constructor.
	Tetrahedron(UInt v0_, UInt v1_, UInt v2_, UInt v3_, IndexedTetraMesh<ValueType>* m_mesh_) : v0(v0_), v1(v1_), v2(v2_), v3(v3_), m_mesh(m_mesh_) {
		normal = NULL;
	}

// ****************************************************************************************

	//! Donstructor.
	~Tetrahedron() {
		if (normal != NULL)
			delete normal;
	}

// ****************************************************************************************

	//! returns (v0+v1+v2+v3)/4;
	inline Point4D getCenter() const {
		const Point4D& v0_ = m_mesh->vList[v0].c;
		const Point4D& v1_ = m_mesh->vList[v1].c;
		const Point4D& v2_ = m_mesh->vList[v2].c;
		const Point4D& v3_ = m_mesh->vList[v3].c;

		return ((v0_+v1_+v2_+v3_)/4);
	}

// ****************************************************************************************

	//! Computes the normal and stores it in the member 'this->normal'.
	void computeNormal() {

		normal = new Point4D;

		SquareMatrixND<Point4D> cv;

		Point4D v = m_mesh->vList[v0].c - m_mesh->vList[v1].c;
		cv.addFromTensorProduct_NO_SYMMETRIZE(v);
		v = m_mesh->vList[v0].c - m_mesh->vList[v2].c;
		cv.addFromTensorProduct_NO_SYMMETRIZE(v);
		v = m_mesh->vList[v0].c - m_mesh->vList[v3].c;
		cv.addFromTensorProduct_NO_SYMMETRIZE(v);


		SquareMatrixND<Point4D> evectors;
		cv.calcEValuesAndVectors(evectors);
        
		(*normal) = evectors.getColI(Point4D::dim-1);
	}

// ****************************************************************************************

	//! Returns the normal of the subspace in which the tetrahedron lies.
	inline const Point4D& getNormal() {
		if (normal == 0) computeNormal();
		return (*normal);
	}

// ****************************************************************************************

	//! Projects the point 'pt' onto the tangent(sub-)space of the tetrahedron.
	inline Point4D projPointOnTet(const Point4D& pt) {
		Point4D pt_proj;

		const Point4D& normal = getNormal();

		pt_proj = pt-normal*(normal|(pt-m_mesh->vList[v0].c));

		return pt_proj;
	}

// ****************************************************************************************

	//! Returns the volume of the tetrahedron.
	inline ValueType getVolume() const {
		const Point4D& v0_ = m_mesh->vList[v0].c;
		const Point4D& v1_ = m_mesh->vList[v1].c;
		const Point4D& v2_ = m_mesh->vList[v2].c;
		const Point4D& v3_ = m_mesh->vList[v3].c;

		return determinant4x4(v0_, v1_, v2_, v3_);
	}

// ****************************************************************************************

	//! Projects the point 'pt' onto the tangent(sub-)space of the tetrahedron and computes the barycentric coordinated of this point.
	inline Point4D barycentric_with_proj(const Point4D& pt) {
		Point4D pt_proj = projPointOnTet(pt);

		return barycentric(pt_proj);
	}

// ****************************************************************************************

	//! Returns the (unnormalized normal) interpolated according to the current tetrahedron.
	inline Point4D getInterpolatedNormal(const Point4D& barys) const {
		Point4D interpolatedNormal = 
			m_mesh->vList[v0].normal * barys.x +
			m_mesh->vList[v1].normal * barys.y +
			m_mesh->vList[v2].normal * barys.z +
			m_mesh->vList[v3].normal * barys.w;
		return interpolatedNormal;
	}

// ****************************************************************************************

	//! Computes the barys of the point 'pt' w.r.t. this tetrahedron. Assumes that 'pt' lies in the sub-space to the tetrahedron.
	inline Point4D barycentric(const Point4D& pt) const {

		const Point4D& v0_ = m_mesh->vList[v0].c;
		const Point4D& v1_ = m_mesh->vList[v1].c;
		const Point4D& v2_ = m_mesh->vList[v2].c;
		const Point4D& v3_ = m_mesh->vList[v3].c;

		ValueType tetVolume = determinant4x4(v0_, v1_, v2_, v3_);
		ValueType vol3 = determinant4x4(v0_, v1_, v2_, pt);
		ValueType vol2 = determinant4x4(v0_, v1_, pt, v3_);
		ValueType vol1 = determinant4x4(v0_, pt, v2_, v3_);
		ValueType vol0 = determinant4x4(pt, v1_, v2_, v3_);

		Point4D barys(vol0, vol1, vol2, vol3);
		barys /= tetVolume;

		//std::cerr << "tetVolume: " << tetVolume <<std::endl;
		//std::cerr << "volume v3: " << vol3 << std::endl;
		//std::cerr << "volume v2: " << vol2 << std::endl;
		//std::cerr << "volume v1: " << vol1 << std::endl;
		//std::cerr << "volume v0: " << vol0 << std::endl;
		//std::cerr << "sum: " << (vol0+vol1+vol2+vol3) << std::endl;
		//barys.print();

		return barys;
	}

// ****************************************************************************************

	inline void print() const {
		std::cerr << "Tetrahedron: " << v0 << ", " << v1 << ", " << v2 << ", " << v3 << std::endl;
	}

// ****************************************************************************************

	inline UInt getMemoryRequired() const {
		UInt memory = 0;
		memory += 8*sizeof(UInt);		// 'v0', 'v1', 'v2' and 'v3' and edges[0...3].
		memory += sizeof(int*);			// *m_mesh;
		return memory;
	}

// ****************************************************************************************

	inline UInt intersectWithTime(ValueType t, point3d<ValueType>* const vertices,  point3d<ValueType>* const normals = NULL) const {

		const Point4D& v0_ = m_mesh->vList[v0].c;
		const Point4D& v1_ = m_mesh->vList[v1].c;
		const Point4D& v2_ = m_mesh->vList[v2].c;
		const Point4D& v3_ = m_mesh->vList[v3].c;

		//Point4D v0_ = m_mesh->vList[v0].c;
		//Point4D v1_ = m_mesh->vList[v1].c;
		//Point4D v2_ = m_mesh->vList[v2].c;
		//Point4D v3_ = m_mesh->vList[v3].c;
		//ValueType w0_tmp = v0_.w;
		//ValueType w1_tmp = v1_.w;
		//ValueType w2_tmp = v2_.w;
		//ValueType w3_tmp = v3_.w;
		//v0_.w = v0_.x;
		//v0_.x = w0_tmp;
		//v1_.w = v1_.x;
		//v1_.x = w1_tmp;
		//v2_.w = v2_.x;
		//v2_.x = w2_tmp;
		//v3_.w = v3_.x;
		//v3_.x = w3_tmp;

		char simplexIndex = 0;
		if (v0_.w >= t) simplexIndex |= 1;
		if (v1_.w >= t) simplexIndex |= 2;
		if (v2_.w >= t) simplexIndex |= 4;
		if (v3_.w >= t) simplexIndex |= 8;

		char edgesInvolved = marching4d::edgeTableTetrahedron[(int)simplexIndex];

		if (edgesInvolved == 0) // All isovalues are bigger or smaller than 't'.
			return 0;

		// Compute intersections:
		UChar numVertices = 0;

		//std::cerr << "t: " << t << " v0_.w: " << v0_.w << " v1_.w: " << v1_.w << " v2_.w: " << v2_.w << " v3_.w: " << v3_.w << std::endl;

		if (edgesInvolved & 1) { // Edge 0 (v0-v1) crosses 't'
			vertices[numVertices++] = marching4d::linearInterpolate(t, point3d<ValueType>(v0_.x, v0_.y, v0_.z), point3d<ValueType>(v1_.x, v1_.y, v1_.z), v0_.w, v1_.w);
		}
		if (edgesInvolved & 2) { // Edge 1 (v0-v2) crosses 't'
			vertices[numVertices++] = marching4d::linearInterpolate(t, point3d<ValueType>(v0_.x, v0_.y, v0_.z), point3d<ValueType>(v2_.x, v2_.y, v2_.z), v0_.w, v2_.w);
		}
		if (edgesInvolved & 4) { // Edge 2 (v0-v3) crosses 't'
			vertices[numVertices++] = marching4d::linearInterpolate(t, point3d<ValueType>(v0_.x, v0_.y, v0_.z), point3d<ValueType>(v3_.x, v3_.y, v3_.z), v0_.w, v3_.w);
		}
		if (edgesInvolved & 8) { // Edge 3 (v1-v2) crosses 't'
			vertices[numVertices++] = marching4d::linearInterpolate(t, point3d<ValueType>(v1_.x, v1_.y, v1_.z), point3d<ValueType>(v2_.x, v2_.y, v2_.z), v1_.w, v2_.w);
		}
		if (edgesInvolved & 16) { // Edge 4 (v1-v3) crosses 't'
			vertices[numVertices++] = marching4d::linearInterpolate(t, point3d<ValueType>(v1_.x, v1_.y, v1_.z), point3d<ValueType>(v3_.x, v3_.y, v3_.z), v1_.w, v3_.w);
		}
		if (edgesInvolved & 32) { // Edge 5 (v2-v3) crosses 't'
			vertices[numVertices++] = marching4d::linearInterpolate(t, point3d<ValueType>(v2_.x, v2_.y, v2_.z), point3d<ValueType>(v3_.x, v3_.y, v3_.z), v2_.w, v3_.w);
		}

		if (normals != NULL) {
			const Point4D& n0_ = m_mesh->vList[v0].normal;
			const Point4D& n1_ = m_mesh->vList[v1].normal;
			const Point4D& n2_ = m_mesh->vList[v2].normal;
			const Point4D& n3_ = m_mesh->vList[v3].normal;

			UChar numNormals = 0;

			if (edgesInvolved & 1) // Edge 0 (v0-v1) crosses 't'
				normals[numNormals++] = marching4d::linearInterpolate(t, point3d<ValueType>(n0_.x, n0_.y, n0_.z), point3d<ValueType>(n1_.x, n1_.y, n1_.z), v0_.w, v1_.w);
			if (edgesInvolved & 2) // Edge 1 (v0-v2) crosses 't'
				normals[numNormals++] = marching4d::linearInterpolate(t, point3d<ValueType>(n0_.x, n0_.y, n0_.z), point3d<ValueType>(n2_.x, n2_.y, n2_.z), v0_.w, v2_.w);
			if (edgesInvolved & 4) // Edge 2 (v0-v3) crosses 't'
				normals[numNormals++] = marching4d::linearInterpolate(t, point3d<ValueType>(n0_.x, n0_.y, n0_.z), point3d<ValueType>(n3_.x, n3_.y, n3_.z), v0_.w, v3_.w);
			if (edgesInvolved & 8) // Edge 3 (v1-v2) crosses 't'
				normals[numNormals++] = marching4d::linearInterpolate(t, point3d<ValueType>(n1_.x, n1_.y, n1_.z), point3d<ValueType>(n2_.x, n2_.y, n2_.z), v1_.w, v2_.w);
			if (edgesInvolved & 16) // Edge 4 (v1-v3) crosses 't'
				normals[numNormals++] = marching4d::linearInterpolate(t, point3d<ValueType>(n1_.x, n1_.y, n1_.z), point3d<ValueType>(n3_.x, n3_.y, n3_.z), v1_.w, v3_.w);
			if (edgesInvolved & 32) // Edge 5 (v2-v3) crosses 't'
				normals[numNormals++] = marching4d::linearInterpolate(t, point3d<ValueType>(n2_.x, n2_.y, n2_.z), point3d<ValueType>(n3_.x, n3_.y, n3_.z), v2_.w, v3_.w);
		}

		return numVertices;
	}

// ****************************************************************************************

	inline UInt getEdge(int i1) const {
		if (i1 == 0) return edge0;
		else if (i1 == 1) return edge1;
		else if (i1 == 2) return edge2;
		else return edge3;
	}

// ****************************************************************************************

	inline UInt getVertex(int i1) const {
		if (i1 == 0) return v0;
		else if (i1 == 1) return v1;
		else if (i1 == 2) return v2;
		else return v3;
	}

// ****************************************************************************************

private:

// ****************************************************************************************

	//! Vertex 0 of the tetrahedron.
	UInt v0;
	//! Vertex 1 of the tetrahedron.
	UInt v1;
	//! Vertex 2 of the tetrahedron.
	UInt v2;
	//! Vertex 3 of the tetrahedron.
	UInt v3;
	//! 'mothermesh'.
	IndexedTetraMesh<ValueType>* m_mesh;
	//! Index of the 0'th edge (Triangle containing the vertices v1, v2, v3)
	UInt edge0;
	//! Index of the 1'th edge (Triangle containing the vertices v0, v2, v3)
	UInt edge1;
	//! Index of the 2'th edge (Triangle containing the vertices v0, v1, v3)
	UInt edge2;
	//! Index of the 3'th edge (Triangle containing the vertices v0, v1, v2)
	UInt edge3;
	//! The 4D normal of the tetrahedron.
	Point4D* normal;

// ****************************************************************************************

};


// ****************************************************************************************
// ****************************************************************************************


template<class T> class IndexedTetraMesh {

public:

// ****************************************************************************************

	//! Value-Type of Templates components, for example float or double.
	typedef T ValueType;

	//! Type of a 4D Point (vec4f, vec4d).
	typedef point4d<ValueType> Point4D;

	//! Type of a 3D Point (vec3f, vec3d).
	typedef point3d<ValueType> Point3D;

	//! Type of a tetrahedron.
	typedef Tetrahedron<ValueType> Tet;

	//! Type of a tetrahedron vertex.
	typedef TetraVertex<ValueType> Vert;

	//! Type of a tetrahedron edge.
	typedef TetraEdge<ValueType> Edge;

	//! allow TetraVertex to access private members.
	friend class TetraVertex<ValueType>;
	//! allow Tetrahedron to access private members.
	friend class Tetrahedron<ValueType>;
	//! allow TetraEdge to access private members.
	friend class TetraEdge<ValueType>;

// ****************************************************************************************

	IndexedTetraMesh() {
		maxV = Point4D(-std::numeric_limits<ValueType>::max());
		minV = Point4D(std::numeric_limits<ValueType>::max());
	}

	~IndexedTetraMesh() {}

// ****************************************************************************************

	//! Inserts the vertex 'v' into the mesh.
	void insertVertex(Point4D v_) {
		Vert v(v_, this);
		vList.push_back(v);

		if (maxV.x < v_.x) maxV.x = v_.x;
		if (maxV.y < v_.y) maxV.y = v_.y;
		if (maxV.z < v_.z) maxV.z = v_.z;
		if (maxV.w < v_.w) maxV.w = v_.w;
		if (minV.x > v_.x) minV.x = v_.x;
		if (minV.y > v_.y) minV.y = v_.y;
		if (minV.z > v_.z) minV.z = v_.z;
		if (minV.w > v_.w) minV.w = v_.w;
	}

// ****************************************************************************************

	//! Inserts the tetrahedron 'v0 v1 v2 v3' into the mesh.
	void insertTetrahedron(UInt v0, UInt v1, UInt v2, UInt v3) {
		UInt index = (UInt)tList.size();
		Tet tetrahedron(v0, v1, v2, v3, this);

		int index_e0 = insertEdge(v1, v2, v3);
		eList[index_e0].tets.push_back(index);
		tetrahedron.edge0 = index_e0;

		int index_e1 = insertEdge(v0, v2, v3);
		eList[index_e1].tets.push_back(index);
		tetrahedron.edge1 = index_e1;

		int index_e2 = insertEdge(v0, v1, v3);
		eList[index_e2].tets.push_back(index);
		tetrahedron.edge2 = index_e2;

		int index_e3 = insertEdge(v0, v1, v2);
		eList[index_e3].tets.push_back(index);
		tetrahedron.edge3 = index_e3;

		tList.push_back(tetrahedron);
	}

// ****************************************************************************************

	//! Prints the mesh to std::cerr
	void print(bool detailed = true) {
		std::cerr << "--------------------------------" << std::endl;
		std::cerr << "|      IndexedTetraMesh        |" << std::endl;
		std::cerr << "--------------------------------" << std::endl;
		std::cerr << "numV: " << (UInt)vList.size() << ", numT: " << (UInt)tList.size() << ", numE: " << (UInt)eList.size() << std::endl;
		std::cerr << "--------------------------------" << std::endl;
		if (detailed) {
			std::cerr << "Vertices:" << std::endl;
			for (UInt i1 = 0; i1 < vList.size(); i1++) vList[i1].print();
			std::cerr << "--------------------------------" << std::endl;
			std::cerr << "Tetrahedra:" << std::endl;
			for (UInt i1 = 0; i1 < tList.size(); i1++) tList[i1].print();
			std::cerr << "--------------------------------" << std::endl;
			std::cerr << "Edges:" << std::endl;
			for (UInt i1 = 0; i1 < eList.size(); i1++) eList[i1].print();
			std::cerr << "--------------------------------" << std::endl;
		}
		std::cerr << "IndexedTetraMesh uses [" << getMemoryRequired() << "] Bytes in memory" << std::endl;
	}

// ****************************************************************************************

	//! Returns the memory occupied by the IndexedTetraMesh in Bytes (last updated 16.02.2008)
	inline UInt getMemoryRequired() const {
		UInt memory = 0;
		memory += 8*sizeof(ValueType);			// minV and maxV.
		memory += 3*sizeof(std::vector<int>);	// vList, eList and tList.
		for (UInt i1 = 0; i1 < vList.size(); i1++) memory += vList[i1].getMemoryRequired();
		for (UInt i1 = 0; i1 < tList.size(); i1++) memory += tList[i1].getMemoryRequired();
		for (UInt i1 = 0; i1 < eList.size(); i1++) memory += eList[i1].getMemoryRequired();
		return memory;
	}

// ****************************************************************************************

	//! Extracts the boundary to the tetrahedral mesh, projects it according to the specified 'projFunc' and adds it to a SimpleMesh.
	inline void boundaryToSimpleMesh(SimpleMesh* const sm, Point3D(*projFunc)(Point4D) ) const {

		std::map<UInt, UInt> vertexMap;

		ValueType t_min = minV.w;
		ValueType t_max = maxV.w;

		// For all edges
		for (UInt i1 = 0; i1 < eList.size(); i1++)
			if (eList[i1].isBoundaryEdge())
				addEdgeToSimpleMesh(i1, sm, &vertexMap, projFunc, t_min, t_max);

	}

// ****************************************************************************************

	//! Extracts the boundary to the tetrahedral mesh, projects it to the w=0 plane and adds it to a SimpleMesh.
	inline void boundaryToSimpleMesh(SimpleMesh* const sm) const {

		std::map<UInt, UInt> vertexMap;

		ValueType t_min = minV.w;
		ValueType t_max = maxV.w;

		// For all edges
		for (UInt i1 = 0; i1 < eList.size(); i1++)
			if (eList[i1].isBoundaryEdge())
				addEdgeToSimpleMesh(i1, sm, &vertexMap, t_min, t_max);

	}

// ****************************************************************************************

	void loadFromFile(const char* filename) {

		std::ifstream fin(filename, std::ifstream::binary);

		if (!fin.good()) {
			std::cerr << "Error reading file in 'IndexedTetraMesh<T>::loadFromFile(char* filename)'" << std::endl;
			return;
		}

		char numByteForDT;
		UInt numV;
		UInt numT;
		// read header
		fin.read((char*)&numByteForDT, 1);
		fin.read((char*)&numV, sizeof(UInt));
		fin.read((char*)&numT, sizeof(UInt));

		vList.reserve(numV);
		tList.reserve((numT > 2000000) ? 2000000 : numT);

		if (numByteForDT != sizeof(ValueType)) {
			std::cerr << "Could not read IndexedTetraMesh, because of following error" << std::endl;
			std::cerr << "The sizeof the datatype is wrong." << std::endl;
			std::cerr << "The File uses " << (UInt)numByteForDT << " Bytes per floating point" << std::endl;
			std::cerr << "The Mesh uses " << sizeof(ValueType) << " Bytes per floating point" << std::endl;
			std::cerr << "Probably a float/double issue" << std::endl;
			std::cerr << "EXIT" << std::endl;
			exit(EXIT_FAILURE);
		}

		// read vertices:
		Point4D ver;
		for (UInt i1 = 0; i1 < numV; i1++) {
			fin.read((char*)&ver, 4*sizeof(ValueType));
			insertVertex(ver);
		}

		// Read tets;
		UInt idx[4];
		for (UInt i1 = 0; i1 < numT && i1 < 2000000; i1++) {
			fin.read((char*)idx, 4*sizeof(UInt));
			insertTetrahedron(idx[0], idx[1], idx[2], idx[3]);
		}

		std::cerr << "IndexedTetraMesh read" << std::endl;
		std::cerr << "Mesh requires: " << getMemoryRequired()/(1024*1024) << " MBytes" << std::endl;

	}

// ****************************************************************************************

	void writeToFile(const char* filename) {
		std::ofstream fout(filename, std::ofstream::binary);

		if (!fout.good()) {
			std::cerr << "Error writing file in 'IndexedTetraMesh<T>::writeToFile(char* filename)'" << std::endl;
			return;
		}

		// write header:
		UInt numV = (UInt)vList.size();
		UInt numT = (UInt)tList.size();
		char numByteForDT = sizeof(ValueType);

		// write header
		fout.write((char*)&numByteForDT, 1);
		fout.write((char*)&numV, sizeof(UInt));
		fout.write((char*)&numT, sizeof(UInt));

		// write vertices:
		for (UInt i1 = 0; i1 < numV; i1++) {
			fout.write((char*)&(vList[i1].c), 4*numByteForDT);
		}

		// write tets:
		for (UInt i1 = 0; i1 < numT; i1++) {
			Tet t = tList[i1];
			fout.write((char*)&t.v0, sizeof(UInt));
			fout.write((char*)&t.v1, sizeof(UInt));
			fout.write((char*)&t.v2, sizeof(UInt));
			fout.write((char*)&t.v3, sizeof(UInt));
		}

		fout.close();

	}

// ****************************************************************************************

	inline void intersectWithTime(ValueType time, TrivialMesh< point3d<ValueType> >* const tm, point3d<ValueType>* normals = NULL) {

		std::cerr << "Intersect with time: " << time << std::endl;

		std::vector< point3d<ValueType> > vertices;
		vertices.reserve(4);

		std::vector< point3d<ValueType> > normals_tmp;
		normals_tmp.reserve(4);

		if (normals == NULL) {
			for (UInt i1 = 0; i1 < tList.size(); i1++) {
				UInt num = tList[i1].intersectWithTime(time, &vertices[0]);

				// Insert triangles into TrivialMesh
				if (num >= 3) {
					if ((vertices[0] == vertices[1]) || (vertices[0] == vertices[2]) || (vertices[1] == vertices[2])) {
						// void
					} else {
						tm->insertTriangle(vertices[0], vertices[1], vertices[2]);
					}
					if (num == 4) {
						if ((vertices[2] == vertices[1]) || (vertices[2] == vertices[3]) || (vertices[1] == vertices[3])) {
							// void
						} else {
							tm->insertTriangle(vertices[2], vertices[1], vertices[3]);
						}
					}
				}
			}
		} else {

			UInt normal_count = 0;

			for (UInt i1 = 0; i1 < tList.size(); i1++) {
				UInt num = tList[i1].intersectWithTime(time, &vertices[0], &normals_tmp[0]);

				// Insert triangles into TrivialMesh
				if (num >= 3) {
					if ((vertices[0] == vertices[1]) || (vertices[0] == vertices[2]) || (vertices[1] == vertices[2])) {
						// void
					} else {
						tm->insertTriangle(vertices[0], vertices[1], vertices[2]);
						normals[normal_count++] = normals_tmp[0];
						normals[normal_count++] = normals_tmp[1];
						normals[normal_count++] = normals_tmp[2];
					}
					if (num == 4) {
						if ((vertices[2] == vertices[1]) || (vertices[2] == vertices[3]) || (vertices[1] == vertices[3])) {
							// void
						} else {
							tm->insertTriangle(vertices[2], vertices[1], vertices[3]);
							normals[normal_count++] = normals_tmp[2];
							normals[normal_count++] = normals_tmp[1];
							normals[normal_count++] = normals_tmp[3];
						}
					}
				}
			}
		}

		std::cerr << "found " << tm->getNumTris()*3 << " vertices" << std::endl;

	}

// ****************************************************************************************

	inline ValueType getT_min() const {
		return minV.w;
	}

// ****************************************************************************************

	inline ValueType getT_max() const {
		return maxV.w;
	}

// ****************************************************************************************

	inline Tet getTet(UInt i1) const {
		return tList[i1];
	}

// ****************************************************************************************

	inline UInt getNumE() const {
		return eList.size();
	}

// ****************************************************************************************

	inline UInt getNumT() const {
		return (UInt)tList.size();
	}

// ****************************************************************************************

	inline Point4D getPoint(UInt i) const {
		return vList[i].c;
	}

// ****************************************************************************************

	inline UInt getNumV() const {
		return (UInt)vList.size();
	}

// ****************************************************************************************

protected:

// ****************************************************************************************

	inline UInt insertEdge(UInt v1, UInt v2, UInt v3) {
		int index_e0 = vList[v1].getEdge(v1, v2, v3);
		if (index_e0 == -1) { // Edge does not yet exist
			index_e0 = (int)eList.size();
			Edge e(v1, v2, v3, this);
			vList[v1].edges.push_back(index_e0);
			vList[v2].edges.push_back(index_e0);
			vList[v3].edges.push_back(index_e0); 
			eList.push_back(e);
		}
		return index_e0;
	}

// ****************************************************************************************

	inline void addEdgeToSimpleMesh(UInt edge_index, SimpleMesh* const sm, std::map<UInt, UInt>* vmap, Point3D(*projFunc)(Point4D), ValueType t_min = 0, ValueType t_max = 1) const {
		const Edge* edge = &eList[edge_index];

		Point3D p0 = projFunc(vList[edge->v0].c);
		Point3D p1 = projFunc(vList[edge->v1].c);
		Point3D p2 = projFunc(vList[edge->v2].c);

		UInt pos_v0, pos_v1, pos_v2;

		if (vmap->find(edge->v0) != vmap->end()) { // point already in SimpleMesh
			pos_v0 = (*vmap)[edge->v0];
		} else { // Add point to SimpleMesh.
			pos_v0 = sm->getNumV();
			vmap->insert(std::pair<UInt, UInt>(edge->v0, pos_v0));
			sm->insertVertex((double)p0.x, (double)p0.y, (double)p0.z);
			sm->vList[pos_v0].color = JBSlib::hsv2rgb((float)((vList[edge->v0].c.w-t_min)/t_max * (ValueType)0.8), (float)1, (float)1);
		}
		if (vmap->find(edge->v1) != vmap->end()) { // point already in SimpleMesh
			pos_v1 = (*vmap)[edge->v1];
		} else { // Add point to SimpleMesh.
			pos_v1 = sm->getNumV();
			vmap->insert(std::pair<UInt, UInt>(edge->v1, pos_v1));
			sm->insertVertex((double)p1.x, (double)p1.y, (double)p1.z);
			sm->vList[pos_v1].color = JBSlib::hsv2rgb((float)((vList[edge->v1].c.w-t_min)/t_max * (ValueType)0.8), (float)1, (float)1);
		}
		if (vmap->find(edge->v2) != vmap->end()) { // point already in SimpleMesh
			pos_v2 = (*vmap)[edge->v2];
		} else { // Add point to SimpleMesh.
			pos_v2 = sm->getNumV();
			vmap->insert(std::pair<UInt, UInt>(edge->v2, pos_v2));
			sm->insertVertex((double)p2.x, (double)p2.y, (double)p2.z);
			sm->vList[pos_v2].color = JBSlib::hsv2rgb((float)((vList[edge->v2].c.w-t_min)/t_max * (ValueType)0.8), (float)1, (float)1);
		}

		sm->insertTriangle(pos_v0, pos_v1, pos_v2);
	}

// ****************************************************************************************

	inline void addEdgeToSimpleMesh(UInt edge_index, SimpleMesh* const sm, std::map<UInt, UInt>* vmap, ValueType t_min = 0, ValueType t_max = 1) const {

		const Edge* edge = &eList[edge_index];

		Point3D p0(vList[edge->v0].c.x, vList[edge->v0].c.y, vList[edge->v0].c.z);
		Point3D p1(vList[edge->v1].c.x, vList[edge->v1].c.y, vList[edge->v1].c.z);
		Point3D p2(vList[edge->v2].c.x, vList[edge->v2].c.y, vList[edge->v2].c.z);

		UInt pos_v0, pos_v1, pos_v2;

		if (vmap->find(edge->v0) != vmap->end()) { // point already in SimpleMesh
			pos_v0 = (*vmap)[edge->v0];
		} else { // Add point to SimpleMesh.
			pos_v0 = sm->getNumV();
			vmap->insert(std::pair<UInt, UInt>(edge->v0, pos_v0));
			sm->insertVertex((double)p0.x, (double)p0.y, (double)p0.z);
			sm->vList[pos_v0].color = JBSlib::hsv2rgb((float)((vList[edge->v0].c.w-t_min)/t_max * (ValueType)0.8), (float)1, (float)1);
		}
		if (vmap->find(edge->v1) != vmap->end()) { // point already in SimpleMesh
			pos_v1 = (*vmap)[edge->v1];
		} else { // Add point to SimpleMesh.
			pos_v1 = sm->getNumV();
			vmap->insert(std::pair<UInt, UInt>(edge->v1, pos_v1));
			sm->insertVertex((double)p1.x, (double)p1.y, (double)p1.z);
			sm->vList[pos_v1].color = JBSlib::hsv2rgb((float)((vList[edge->v1].c.w-t_min)/t_max * (ValueType)0.8), (float)1, (float)1);
		}
		if (vmap->find(edge->v2) != vmap->end()) { // point already in SimpleMesh
			pos_v2 = (*vmap)[edge->v2];
		} else { // Add point to SimpleMesh.
			pos_v2 = sm->getNumV();
			vmap->insert(std::pair<UInt, UInt>(edge->v2, pos_v2));
			sm->insertVertex((double)p2.x, (double)p2.y, (double)p2.z);
			sm->vList[pos_v2].color = JBSlib::hsv2rgb((float)((vList[edge->v2].c.w-t_min)/t_max * (ValueType)0.8), (float)1, (float)1);
		}

		sm->insertTriangle(pos_v0, pos_v1, pos_v2);
	}

// ****************************************************************************************

public:

	//! Vertices of the mesh.
	std::vector<Vert> vList;

	//! Edges of the mesh.
	std::vector<Edge> eList;

	//! Tetrahedrons of the mesh.
	std::vector<Tet> tList;

	//! Max corner of AABB.
	Point4D maxV;

	//! Min corner of AABB.
	Point4D minV;

// ****************************************************************************************

};


typedef IndexedTetraMesh<double> IndexedTetraMeshD;
typedef IndexedTetraMesh<float> IndexedTetraMeshF;

// ****************************************************************************************
// ****************************************************************************************

#endif
