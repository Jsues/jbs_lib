#ifndef SORT_STRING_VECTOR
#define SORT_STRING_VECTOR

#include <vector>
#include <algorithm>
#include <sstream>

bool sortStrings(const std::string& s0, const std::string& s1) {

	if (s0.length() < s1.length()) return true;
	if (s0.length() > s1.length()) return false;
	return s0 < s1;
}


void sortStringVector(std::vector<std::string>& vec) {

	std::sort(vec.begin(), vec.end(), sortStrings);

}

#endif
