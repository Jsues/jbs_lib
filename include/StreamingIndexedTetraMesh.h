#ifndef STREAMING_INDEXED_TETRA_MESH_H
#define STREAMING_INDEXED_TETRA_MESH_H

#include "IndexedTetraMesh.h"
#include <string>
#include <fstream>
#include <iostream>
#include <set>
#include <cassert>


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

template<class T>
class timesliceInfo {

public:

	timesliceInfo(T t, UInt id, UInt first) : timeStart(t), timeSliceID(id), indexOfFirstTetrahedron(first) {
	}

	T timeStart;
	UInt timeSliceID;
	UInt indexOfFirstTetrahedron;
};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

template<class T>
class StreamingIndexedTetraMesh : public IndexedTetraMesh<T> {

public:
	
	typedef T ValueType;
	typedef point4d<ValueType> Point4D;
	
// ****************************************************************************************

	//! Constructor
	StreamingIndexedTetraMesh() : IndexedTetraMesh<T>() {
	}

// ****************************************************************************************

	//! Destructor
	~StreamingIndexedTetraMesh() {
		if (tetra_stream.is_open()) {
			tetra_stream.close();
		}
	}

// ****************************************************************************************

	//! Removes all tetrahedron and edges information, i.e. removes 'eList' and 'tList' and the 'std::vector<UInt> edges' for all vertices
	void removeAllCurrentTets() {
		IndexedTetraMesh<T>::eList.clear();
		IndexedTetraMesh<T>::tList.clear();
		for (UInt i1 = 0; i1 < IndexedTetraMesh<T>::vList.size(); i1++) {
			IndexedTetraMesh<T>::vList[i1].edges.clear();
		}
		slicesInMemory.clear();
	}

// ****************************************************************************************

	//! finds the slice containing 'time' and returns it in the reference 'sliceID'. Returns false if time is not in any timeslice.
	inline bool findSliceContainingTime(ValueType time, UInt& sliceID) const {

		sliceID = 0;

		for (UInt i1 = 0; i1 < sliceInfo.size()-1; i1++) {
			if (sliceInfo[i1].timeStart <= time && sliceInfo[i1+1].timeStart > time) {
				sliceID = i1;
				return true;
			}
		}
		return false;
	}

// ****************************************************************************************

	//! Test whether the edge with id 'eID' lies on the boundary of the current slice
	/*!
		\param nextSliceID the id of the timeSlice which contains the (second) tetrahedron adjacent to the edge with id 'eID'.
	*/
	bool edgeLiesOnSliceBoundary(UInt eID, UInt& nextSliceID) {

		UInt v0_ind = IndexedTetraMesh<T>::eList[eID].getVertex(0);
		UInt v1_ind = IndexedTetraMesh<T>::eList[eID].getVertex(1);
		UInt v2_ind = IndexedTetraMesh<T>::eList[eID].getVertex(2);

		const Point4D& v0 = IndexedTetraMesh<T>::vList[v0_ind].c;
		const Point4D& v1 = IndexedTetraMesh<T>::vList[v1_ind].c;
		const Point4D& v2 = IndexedTetraMesh<T>::vList[v2_ind].c;

		if ((v0.w != v1.w) || (v0.w != v2.w)) {
			return false;
		}

		UInt currentSliceID = getCurrentSliceID();
		point2d<ValueType> sliceTminmax = getTimeRangeOfSlice(currentSliceID);

		ValueType dist = sliceTminmax.y-sliceTminmax.x;
		ValueType dist_frac = dist/1000;

		if (fabs(sliceTminmax.x - v0.w) < dist_frac) {
			nextSliceID = currentSliceID-1;

			assert(nextSliceID >= 0);
			return true;
		} else if (fabs(sliceTminmax.y - v0.w) < dist_frac) {
			nextSliceID = currentSliceID+1;

			assert(nextSliceID < sliceInfo.size());
			return true;
		}

		return false;
	}

// ****************************************************************************************

	inline UInt getIndexOfLastTimeSlice() const {
		if (sliceInfo.size() != 0)
			return UInt (sliceInfo.size() - 2);
		return 0;
	}

// ****************************************************************************************

	//! Returns t_min and t_max of the slice 'sliceID'
	/*!
		t_min is the first component of the return value, t_max the second.
	*/
	point2d<ValueType> getTimeRangeOfSlice(UInt sliceID) {

		point2d<ValueType> ret;

		if (sliceID == 0) { // first slice.
			ret.x = IndexedTetraMesh<T>::getT_min();
			ret.y = sliceInfo[sliceID+1].timeStart;
		} else if (sliceID == (sliceInfo.size()-2)) { // last slice
			ret.x = sliceInfo[sliceID].timeStart;
			ret.y = IndexedTetraMesh<T>::getT_max();
		} else { // intermediate slice
			ret.x = sliceInfo[sliceID].timeStart;
			ret.y = sliceInfo[sliceID+1].timeStart;
		}

		return ret;
	}

// ****************************************************************************************

	//! Returns the ID of the slice currently in memory.
	UInt getCurrentSliceID() const {
		UInt sliceID = *(slicesInMemory.begin());
		return sliceID;
	}

// ****************************************************************************************

	//! if there is currently only one slice in memory, it is removed and the next slice is loaded.
	bool readNextSlice() {
		if (slicesInMemory.size() == 1) {
			UInt sliceID = *(slicesInMemory.begin());
			std::cerr << "getting slice " << (sliceID+1) << std::endl;
			removeAllCurrentTets();
			return readTimeSlice(sliceID+1);
		} else {
			std::cerr << "Can't read next slice in StreamingIndexedTetraMesh::readNextSlice()" << std::endl;
			std::cerr << "Number of slices in memory is: " << (UInt)slicesInMemory.size() << std::endl;
			return false;
		}
	}

// ****************************************************************************************

	//! Reads tetrahedra and edges for the slice with 'sliceID'.
	bool readTimeSlice(UInt sliceID, bool readOnlyEdges = false) {

		// Check if slice is allready im memory:
		if (slicesInMemory.find(sliceID) != slicesInMemory.end()) {
			// slice already loaded
			return true;
		}

		if (sliceID >= sliceInfo.size()-1) {
			std::cerr << "ERROR in StreamingIndexedTetraMesh::readTimeSlice(...)" << std::endl;
			std::cerr << "You requested timeSlice " << sliceID << " max id is " << (int)sliceInfo.size()-2 << std::endl;
			return false;
		}

		if (!tetra_stream.is_open()) {
			std::cerr << "StreamingIndexedTetraMesh::readTimeSlice(...) tetrafile (" << tetraFile << ")is not open : " << std::endl;
			exit(EXIT_FAILURE);
		}

		UInt startTetra = sliceInfo[sliceID].indexOfFirstTetrahedron;
		UInt numTetra = sliceInfo[sliceID+1].indexOfFirstTetrahedron-startTetra;
		UInt bytesPerTetra = 4*sizeof(UInt);

		std::cerr << "Reading slice: " << sliceID << " (tets " << startTetra << " to " << startTetra+numTetra << ") Jumping to pos: " << (bytesPerTetra*startTetra) << std::endl;

		//Jump to the correct position:
		tetra_stream.seekg(bytesPerTetra*startTetra);
		// Read tets;
		UInt idx[4];

		if (readOnlyEdges) {
			for (UInt i1 = 0; i1 < numTetra; i1++) {
				if ((i1 % 1000) == 1) std::cerr << (float)(i1*100)/(float)numTetra << "% read" << "                  \r";
				tetra_stream.read((char*)idx, bytesPerTetra);
				IndexedTetraMesh<T>::insertEdge(idx[1], idx[2], idx[3]);
				IndexedTetraMesh<T>::insertEdge(idx[0], idx[2], idx[3]);
				IndexedTetraMesh<T>::insertEdge(idx[0], idx[1], idx[3]);
				IndexedTetraMesh<T>::insertEdge(idx[0], idx[1], idx[2]);
			}
		} else { // Read tetrahedrons
			for (UInt i1 = 0; i1 < numTetra; i1++) {
				if ((i1 % 1000) == 1) std::cerr << (float)(i1*100)/(float)numTetra << "% read" << "                  \r";
				tetra_stream.read((char*)idx, bytesPerTetra);
				IndexedTetraMesh<T>::insertTetrahedron(idx[0], idx[1], idx[2], idx[3]);
			}
		}

		slicesInMemory.insert(sliceID);

		std::cerr << (UInt) IndexedTetraMesh<T>::tList.size() << " tetrahedra" << std::endl;
		return true;
	}

// ****************************************************************************************

	//! Reads the meta-file which contains information on time-slices
	/*
		Example Metafile:<br>
		vertexfile: C:\Forschung\Projects\data\timevaryingPointclouds\igea_move4.itmverts<br>
		tetrafile: C:\Forschung\Projects\data\timevaryingPointclouds\igea_move4.itmtets<br>
		normalfile: null<br>
		numVerts: 35626<br>
		numTets: 666644<br>
		7.2537908554077148 0<br>
		12.08965015411377 103080<br>
		16.925510406494141 204920<br>
		21.761371612548828 317660<br>
		26.597232818603516 476840<br>
	
		First lines should be clear, the list at the end of the file says:

		- first time slice starts at t=w=7.25, 0 is the index of the first tetrahedron in the first timeslice.<br>
		- second time slice starts at t=w=12.09, 103080 is the index of the first tetrahedron in the second timeslice.<br>
		- third time slice starts at t=w=16.93, 204920 is the index of the first tetrahedron in the third timeslice.

		i.e. the first timeslice contains the tetrahedra 0-103079, the second the tetrahedrons 103080-204919, ...
	*/
	void readMetaFile(const char* filename) {

		metaFile = std::string(filename);

		std::ifstream fin(filename);

		if (!fin.is_open()) {
			std::cerr << "StreamingIndexedTetraMesh::readMetaFile(...) : File not found : " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		std::string tempstring;

		fin >> tempstring >> vertexFile;
		fin >> tempstring >> tetraFile;
		fin >> tempstring >> normalFile;
		fin >> tempstring >> numV;
		UInt numT_temp;
		fin >> tempstring >> numT_temp;

		// read time_slice information
		UInt id = 0;
		while (!fin.eof()) {
			ValueType t;
			UInt start;
			fin >> t >> start;
			sliceInfo.push_back(timesliceInfo<ValueType>(t, id, start));
			id++;
		}
		fin.close();
		sliceInfo.pop_back();
		std::cerr << "We have " << (UInt)sliceInfo.size() << " slices" << std::endl;
		// add 'finisher':
		//sliceInfo.push_back(timesliceInfo<ValueType>(std::numeric_limits<ValueType>::max(), id, numT_temp));
		sliceInfo.push_back(timesliceInfo<ValueType>(sliceInfo[sliceInfo.size()-1].timeStart, id, numT_temp));

		// read vertices:
		IndexedTetraMesh<T>::loadFromFile(vertexFile.c_str());
		numT = numT_temp;

		if (normalFile.compare(std::string("null")) != 0) { // We have a normal file, read it.
			std::ifstream normal_stream(normalFile.c_str(), std::ifstream::binary);
			if (!normal_stream.is_open()) {
				std::cerr << "StreamingIndexedTetraMesh::readMetaFile(...) normalfile (" << normalFile << ")is not open : " << std::endl;
				std::cerr << "skip reading normals" << std::endl;
			} else {
				std::cerr << "reading normals...";
				Point4D normal;
				for (UInt i1 = 0; i1 < IndexedTetraMesh<T>::getNumV(); i1++) {
					normal_stream.read((char*)&normal, 4*sizeof(ValueType));
					normal.normalize();
					IndexedTetraMesh<T>::vList[i1].normal = normal;
				}
				std::cerr << "done" << std::endl;
			}
			normal_stream.close();
		}

		// open tetra file:
		tetra_stream.open(tetraFile.c_str(), std::ifstream::binary);
		if (!tetra_stream.is_open()) {
			std::cerr << "StreamingIndexedTetraMesh::readMetaFile(...) tetrafile (" << tetraFile << ")is not open : " << std::endl;
			exit(EXIT_FAILURE);
		}
		// Query size => num tets of tet file:
		tetra_stream.seekg( 0, std::ios::end );
		long fileSize = tetra_stream.tellg();
		std::cerr << "tetrafile contains " << fileSize/(4*sizeof(UInt)) << " tetrahedra" << std::endl;

		std::cerr << "vertexfile:" << vertexFile << std::endl;
		std::cerr << "tetrafile:" << tetraFile << std::endl;
		std::cerr << "normalfile:" << normalFile << std::endl;
		std::cerr << "numVerts:" << numV << std::endl;
		std::cerr << "numTets:" << numT << std::endl << std::endl;
	}

// ****************************************************************************************

	void printSliceInfo() {
		std::cerr << "Slice Info:" << std::endl;
		std::cerr << "numSlices: " << (UInt)sliceInfo.size()-1 << std::endl;
		for (UInt i1 = 0; i1 < sliceInfo.size()-1; i1++) {
			std::cerr << sliceInfo[i1].timeSliceID << ": starts at time " << sliceInfo[i1].timeStart << " with tetrahedron " <<  sliceInfo[i1].indexOfFirstTetrahedron << std::endl;
			std::cerr << "\t and ends with tetrahedron " << sliceInfo[i1+1].indexOfFirstTetrahedron-1 << std::endl;
		}
	}

// ****************************************************************************************

private:

	//! Number of vertices:
	UInt numV;


public:

	//! Number of tetrahedrons:
	UInt numT;

	//! File/Stream for tetrahedrons.
	std::ifstream tetra_stream;

	//! This std::set stores the id of all slices that are currently in memory.
	std::set<UInt> slicesInMemory;
	//! Filename for vertices-file.
	std::string vertexFile;
	//! Filename for tetrahedra-file.
	std::string tetraFile;
	//! Filename for normals-file.
	std::string normalFile;
	//! Filename for meta-file.
	std::string metaFile;

	//! The metainfo for slices.
	std::vector< timesliceInfo<ValueType> > sliceInfo;

};

#endif
