#ifndef NONLINEAR_OPTIMIZE_WITH_GRADIENT_H
#define NONLINEAR_OPTIMIZE_WITH_GRADIENT_H

#include <sstream>

#include "rbf.h"
#include "MathMatrix2.h"
#include "MatrixnD.h"

//****************************************************************************************************

//! Solves A*x=b with double precision
template <class ValueType>
bool solveCholdouble(const MathMatrix2<ValueType>& A, const ValueType* b, ValueType* x, MathMatrix2<double>& A2, MathMatrix2<double>& Chol, double* diag, double* b2, double* x2) {
	int n = A.getDim();
	for (int i1 = 0; i1 < n*n; i1++) A2.m[i1] = A.m[i1];
	for (int i1 = 0; i1 < n; i1++) {
		b2[i1] = b[i1];
		x2[i1] = x[i1];
	}


	bool cholRes = A2.CHOLESKY_decomp(Chol, diag);
	Chol.CHOLESKY_bksolve(diag, b2, x2);

	for (int i1 = 0; i1 < n; i1++) {
		x[i1] = (ValueType)x2[i1];
	}

	return cholRes;
}

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class RBF_Sum>
typename RBF_Sum::Point::ValueType computeErrorWithGrad(RBFFunctionSamplesBase<typename RBF_Sum::Point>* samples, RBF_Sum* rbf, typename RBF_Sum::ValueType theta = 0.99) {

	UInt numPts = (UInt)samples->getNumSamples();

	typename RBF_Sum::Point::ValueType error_fit = 0;
	typename RBF_Sum::Point::ValueType error_grad = 0;

	typename RBF_Sum::Point func_grad;
	typename RBF_Sum::Point::ValueType func_val;
	RBF_Sum::Point e_grad;
	RBF_Sum::Point::ValueType e_func;

	for (UInt i1 = 0; i1 < numPts; i1++) {
		//typename RBF_Sum::Point::ValueType tmp;
		//const RBF_Sum::Point& pt = samples->getSamplePos(i1);
		//const RBF_Sum::Point::ValueType& val = samples->getSampleVal(i1);
		//tmp = rbf->eval(pt)-val;
		//error_fit += tmp*tmp;
		//RBF_Sum::Point grad = rbf->evalGrad(pt)-samples->getSampleGrad(i1);
		//error_grad += grad|grad;


		func_val = rbf->evalValAndGrad(samples->getSamplePos(i1), func_grad);
		e_grad = func_grad-samples->getSampleGrad(i1);
		e_func = func_val-samples->getSampleVal(i1);
		error_fit += e_func*e_func;
		error_grad += e_grad|e_grad;
	}

	//std::cerr << "error_grad: " << error_grad*(1-theta) << "   error_fit: " << error_fit*theta << "  ->  sum: " << theta*error_fit+(1-theta)*error_grad << std::endl;

    return (theta*error_fit+(1-theta)*error_grad);
}

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class RBF_Sum>
inline void addToRBF(RBF_Sum* rbf, typename RBF_Sum::ValueType* gradient, typename RBF_Sum::ValueType u) {
	typedef RBF_Sum::ValueType ValueType;
	typedef RBF_Sum::Point Point;
	static const UInt dim = Point::dim;
	static const UInt multiplier = dim+1;

	for (UInt i1 = 0; i1 < rbf->getNumCenters(); i1++) {
		rbf->m_heights[i1] += u*gradient[i1*multiplier+0];
		for (UInt i2 = 0; i2 < dim; i2++) {
			rbf->m_centers[i1][i2] += u*gradient[i1*multiplier+i2+1];
		}
	}
}

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class RBF_Sum>
typename RBF_Sum::Point::ValueType computeErrorWithGrad2(RBFFunctionSamplesBase<typename RBF_Sum::Point>* samples, RBF_Sum* rbf, typename RBF_Sum::ValueType* gradient, typename RBF_Sum::ValueType u, typename RBF_Sum::ValueType theta = 0.99) {
	addToRBF(rbf, gradient, u);
	typename RBF_Sum::Point::ValueType error = computeErrorWithGrad(samples, rbf, theta);
	addToRBF(rbf, gradient, -u);

	return error;
}


//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class RBF_Sum>
void computeDtDwithGradV2(RBFFunctionSamplesBase<typename RBF_Sum::Point>* samples, RBF_Sum* rbf, MathMatrix2<typename RBF_Sum::Point::ValueType>& DtD, typename RBF_Sum::Point::ValueType* Dtr, const typename RBF_Sum::ValueType theta = 0.99) {

	typedef RBF_Sum::ValueType ValueType;
	typedef RBF_Sum::Point Point;
	typedef SquareMatrixND<Point> Mat;
	static const UInt dim = Point::dim;
	static const UInt multiplier = dim+1;
	static const UInt mult_sq = multiplier*multiplier;
	const ValueType theta2 = (1-theta);

	//// We have 'm' samples and 'n' unknown
	const UInt m = (UInt)samples->getNumSamples();
	const UInt n2 = rbf->getNumCenters();
	const UInt numUnknown = n2*multiplier+multiplier;
	for (UInt i1 = 0; i1 < numUnknown; i1++) Dtr[i1] = 0;
	DtD.zeroOut();

	RBF_Sum::RBF_Type* func = &(rbf->basisfunction);
	ValueType delta_sq = func->c_square;
	
	/*
		derivs_f2[0] is the vector f2_derived_for_height.
		derivs_f2[1] is the vector f2_derived_for_cx.
		derivs_f2[2] is the vector f2_derived_for_cy.
		derivs_f2[3] is the vector f2_derived_for_cz.
	*/
	Point derivs_f2_i[multiplier];
	Point derivs_f2_j[multiplier];

	Point derivs_f2_id[multiplier];
	derivs_f2_id[0] = Point((ValueType)0);
	for (UInt i1 = 0; i1 < dim; i1++) {
		derivs_f2_id[1+i1] = Point((ValueType)0);
		derivs_f2_id[1+i1][i1] = 1;
	}


	//! Run over all points
	for (UInt i1 = 0; i1 < m; i1++) {

		//! Compute residuals
		const Point& pt = samples->getSamplePos(i1);
		Point current_grad;
		ValueType current_val	= rbf->evalValAndGrad(pt, current_grad);
		ValueType res_val		= current_val - samples->getSampleVal(i1);
		Point res_grad			= current_grad - samples->getSampleGrad(i1);

		for (UInt i = 0; i < n2; i++) {
			const ValueType& weight_i = rbf->m_heights[i];
			const Point& center_i = rbf->m_centers[i];
			Point dir_i = center_i-pt;
			ValueType d_i = dir_i.length();
			ValueType grad_fac_i = func->evalGradFactor(d_i);
			Point grad_i = dir_i*grad_fac_i*weight_i;
			ValueType d_height_i = func->eval(d_i);

			Mat f2_cent_i = func->evalHessian(dir_i);
			f2_cent_i *= weight_i;
			derivs_f2_i[0] = -dir_i*grad_fac_i;
			for (UInt x = 0; x < dim; x++) derivs_f2_i[1+x] = -f2_cent_i.getRowI(x);

			//! Compute upper left part of the matrix
			for (UInt j = i; j < n2; j++) {
				const ValueType& weight_j = rbf->m_heights[j];
				const Point& center_j = rbf->m_centers[j];
				Point dir_j = center_j-pt;
				ValueType d_j = dir_j.length();
				ValueType grad_fac_j = func->evalGradFactor(d_j);
				Point grad_j = dir_j*grad_fac_j*weight_j;
				ValueType d_height_j = func->eval(d_j);

				Mat f2_cent_j = func->evalHessian(dir_j);
				f2_cent_j *= weight_j;
				derivs_f2_j[0] = -dir_j*grad_fac_j;
				for (UInt x = 0; x < dim; x++) derivs_f2_j[1+x] = -f2_cent_j.getRowI(x);

				//! Function value Fit
				DtD(i*multiplier+0, j*multiplier+0) += d_height_i*d_height_j*theta;
				for (UInt x = 0; x < dim; x++) {
					// !TODO! toggle next 2 rows
					DtD(i*multiplier+(1+x), j*multiplier+0) += d_height_j*grad_i[x]*theta;
					DtD(i*multiplier+0, j*multiplier+(1+x)) += d_height_i*grad_j[x]*theta;
					for (UInt y = 0; y < dim; y++) {
						DtD(i*multiplier+(1+x), j*multiplier+(1+y)) += grad_i[x]*grad_j[y]*theta;
					}
				}

				//! Gradient Fit
				for (UInt x = 0; x < multiplier; x++) {
					for (UInt y = 0; y < multiplier; y++) {
						DtD(i*multiplier+(x), j*multiplier+(y)) += (derivs_f2_i[x]|derivs_f2_j[y])*theta2;
					}
				}


			}

			//! Compute upper part of Dtr
			//! Function value Fit
			Dtr[i*multiplier+0] += res_val*d_height_i*theta;
			for (UInt x = 0; x < dim; x++) Dtr[i*multiplier+(1+x)] += res_val*grad_i[x]*theta;
			//! Gradient Fit
			for (UInt x = 0; x < multiplier; x++) Dtr[i*multiplier+x] += (res_grad|derivs_f2_i[x])*theta2;


			//! Compute upper right/lower left part of DtD
			DtD(i*multiplier+0, n2*multiplier+0) += d_height_i*theta;
			for (UInt x = 0; x < dim; x++) {
				DtD(i*multiplier+(1+x), n2*multiplier+0) += grad_i[x]*theta;
				DtD(i*multiplier+0, n2*multiplier+(1+x)) += d_height_i*pt[x]*theta;
				for (UInt y = 0; y < dim; y++) {
					DtD(i*multiplier+(1+x), n2*multiplier+(1+y)) += grad_i[x]*pt[y]*theta;
				}
			}
			for (UInt x = 0; x < multiplier; x++) {
				for (UInt y = 0; y < multiplier; y++) {
					DtD(i*multiplier+(x), n2*multiplier+(y)) += (derivs_f2_i[x]|derivs_f2_id[y])*theta2;
				}
			}
		}

		// Lower (dim+1)x(dim+1) part of DtD
		for (UInt x = 0; x < dim; x++) {
			DtD(n2*multiplier+0, n2*multiplier+(1+x)) += pt[x]*theta;
			for (UInt y = x; y < dim; y++) {
				DtD(n2*multiplier+(1+x), n2*multiplier+(1+y)) += pt[x]*pt[y]*theta;
			}
		}

		// Lower (dim+1) part of Dtr.
		Dtr[n2*multiplier+0] += res_val*theta;
		for (UInt x = 0; x < dim; x++) {
			Dtr[n2*multiplier+(1+x)] += pt[x]*res_val*theta + res_grad[x]*theta2;
		}

	}

	DtD(n2*multiplier+0, n2*multiplier+0) += (ValueType)m*theta;
	for (UInt x = 0; x < dim; x++) {
		DtD(n2*multiplier+(1+x), n2*multiplier+(1+x)) += (ValueType)m*theta2;
	}

}

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class RBF_Sum>
void computeDtDwithGrad(RBFFunctionSamplesBase<typename RBF_Sum::Point>* samples, RBF_Sum* rbf, MathMatrix2<typename RBF_Sum::Point::ValueType>& DtD, typename RBF_Sum::Point::ValueType* Dtr, typename RBF_Sum::ValueType theta = 0.99) {

	typedef RBF_Sum::ValueType ValueType;
	typedef RBF_Sum::Point Point;
	typedef RBF_Sum::RBF_Type RBF_Type;
	typedef SquareMatrixND<Point> Mat;
	static const UInt dim = Point::dim;
	static const UInt multiplier = dim+1;
	static const UInt mult_sq = multiplier*multiplier;

	//// We have 'm' samples and 'n' unknown
	UInt m = (UInt)samples->getNumSamples();
	UInt n2 = rbf->getNumCenters();

	RBF_Type* func = &(rbf->basisfunction);
	ValueType delta_sq = func->c_square;
	
	ValueType mat_f1[mult_sq];
	ValueType mat_f2[mult_sq];
	ValueType Dtr_f1_i[multiplier];
	ValueType Dtr_f2_i[multiplier];

	/*
		derivs_f2[0] is the vector f2_derived_for_height.
		derivs_f2[1] is the vector f2_derived_for_cx.
		derivs_f2[2] is the vector f2_derived_for_cy.
		derivs_f2[3] is the vector f2_derived_for_cz.
	*/
	Point derivs_f2_i[multiplier];
	Point derivs_f2_j[multiplier];


	ValueType* r	= new ValueType[m]; // Residuum vector.
	Point* r_g		= new Point[m]; // Residuum vector.
	for (UInt i1 = 0; i1 < m; i1++) {
		//if ((i1 % 1000) == 0) std::cerr << i1 << "                \r";
		const Point& pt = samples->getSamplePos(i1);
		r[i1]	= rbf->eval(pt) - samples->getSampleVal(i1);
		r_g[i1]	= rbf->evalGrad(pt) - samples->getSampleGrad(i1);
	}


	for (UInt i = 0; i < n2; i++) {
		//std::cerr << (i+1) << " / " << n2 << "                \r";
		const ValueType& height_i = rbf->m_heights[i];
		const Point& center_i = rbf->m_centers[i];

		for (UInt j = i; j < n2; j++) {

			for (UInt x = 0; x < mult_sq; x++) {
				mat_f1[x] = 0;
				mat_f2[x] = 0;
			}
			for (UInt x = 0; x < multiplier; x++) {
				Dtr_f1_i[x] = 0;
				Dtr_f2_i[x] = 0;
			}

			const ValueType& height_j = rbf->m_heights[j];
			const Point& center_j = rbf->m_centers[j];

			for (UInt k = 0; k < m; k++) {
				const Point& pt = samples->getSamplePos(k);

				Point dir_i = center_i-pt;
				Point dir_j = center_j-pt;
				ValueType d_i = dir_i.length();
				ValueType d_j = dir_j.length();


				ValueType grad_fac_i = func->evalGradFactor(d_i);
				ValueType grad_fac_j = func->evalGradFactor(d_j);

				Point grad_i = dir_i*grad_fac_i*height_i;
				Point grad_j = dir_j*grad_fac_j*height_j;

				ValueType d_height_i = func->eval(d_i);
				ValueType d_height_j = func->eval(d_j);
				mat_f1[0] += d_height_i*d_height_j;
				Dtr_f1_i[0] += r[k]*d_height_i;
				for (UInt x = 0; x < dim; x++) {
					mat_f1[(1+x)*multiplier+(0)] += d_height_j*grad_i[x];
					mat_f1[(0)*multiplier+(1+x)] += d_height_i*grad_j[x];
					Dtr_f1_i[x+1] += r[k]*grad_i[x];
					for (UInt y = 0; y < dim; y++) {
						mat_f1[(1+x)*multiplier+(1+y)] += grad_i[x]*grad_j[y];
					}
				}

				//****************************************************************
				// Second part, compute derivatives of gradient-fit
				//****************************************************************
				// f2 (Gradient fit) derived for heigth

				derivs_f2_i[0] = -dir_i*grad_fac_i;
				derivs_f2_j[0] = -dir_j*grad_fac_j;
				// f2 (Gradient fit) derived for center -> 3x3 Tensor.
				Mat f2_cent_i = func->evalHessian(dir_i);
				f2_cent_i *= height_i;
				Mat f2_cent_j = func->evalHessian(dir_j);
				f2_cent_j *= height_j;

				for (UInt i1 = 0; i1 < dim; i1++) {
					derivs_f2_i[1+i1] = -f2_cent_i.getRowI(i1);
					derivs_f2_j[1+i1] = -f2_cent_j.getRowI(i1);
				}
				for (UInt x = 0; x < multiplier; x++) {
					Dtr_f2_i[x] += r_g[k]|derivs_f2_i[x];
					for (UInt y = 0; y < multiplier; y++) {
						mat_f2[x*multiplier+y] += derivs_f2_i[x]|derivs_f2_j[y];
					}
				}

			}
			for (UInt x = 0; x < multiplier; x++) {
				for (UInt y = 0; y < multiplier; y++) {
					DtD(i*multiplier+x, j*multiplier+y) = theta*mat_f1[x*multiplier+y] + (1-theta)*mat_f2[x*multiplier+y];
				}
				Dtr[i*multiplier+x] = theta*Dtr_f1_i[x] + (1-theta)*Dtr_f2_i[x];
			}

		}

#ifdef USE_LINEAR_TERM
		for (UInt i1 = 0; i1 < mult_sq; i1++) { mat_f1[i1] = 0; mat_f2[i1] = 0; }

		for (UInt k = 0; k < m; k++) {
			const Point& pt = samples->getSamplePos(k);

			Point dir_i = center_i-pt;
			ValueType d_i = dir_i.length();

			ValueType grad_fac_i = func->evalGradFactor(d_i);
			Point grad_i = dir_i*grad_fac_i*height_i;

			ValueType d_height_i = func->eval(d_i);

			mat_f1[0] += d_height_i;
			for (UInt x = 0; x < dim; x++) {
				mat_f1[(1+x)*multiplier+(0)] += grad_i[x];
				mat_f1[(0)*multiplier+(1+x)] += d_height_i*pt[x];
				for (UInt y = 0; y < dim; y++) {
					mat_f1[(1+x)*multiplier+(1+y)] += grad_i[x]*pt[y];
				}
			}

			//****************************************************************
			// Second part, compute derivatives of gradient-fit
			//***************************************************************
			derivs_f2_i[0] = -dir_i*grad_fac_i;
			// f2 (Gradient fit) derived for center -> 3x3 Tensor.
			ValueType tmp_i = d_i*d_i*delta_sq+1;
			ValueType denominator_i = tmp_i*tmp_i*tmp_i;

			Mat f2_cent_i = func->evalHessian(dir_i);
			f2_cent_i *= height_i;

			for (UInt i1 = 0; i1 < dim; i1++) {
				derivs_f2_i[1+i1] = -f2_cent_i.getRowI(i1);
			}
			derivs_f2_j[0] = Point((ValueType)0);
			for (UInt i1 = 0; i1 < dim; i1++) {
				derivs_f2_j[1+i1] = Point((ValueType)0);
				derivs_f2_j[1+i1][i1] = 1;
			}

			for (UInt x = 0; x < multiplier; x++) {
				for (UInt y = 0; y < multiplier; y++) {
					mat_f2[x*multiplier+y] += derivs_f2_i[x]|derivs_f2_j[y];
				}
			}

		}

		for (UInt x = 0; x < multiplier; x++) {
			for (UInt y = 0; y < multiplier; y++) {
				DtD(i*multiplier+x, n2*multiplier+y) = theta*mat_f1[x*multiplier+y] + (1-theta)*mat_f2[x*multiplier+y];
			}
		}
#endif
	}

#ifdef USE_LINEAR_TERM
	for (UInt i1 = 0; i1 < mult_sq; i1++) { mat_f1[i1] = 0; mat_f2[i1] = 0; }
	for (UInt i1 = 0; i1 < multiplier; i1++) { Dtr_f1_i[i1] = 0; Dtr_f2_i[i1] = 0; }

	// Lower (dim+1)x(dim+1) part:
	mat_f1[0] = (ValueType)m; // m is numPoints
	Point grad_sum;
	for (UInt k = 0; k < m; k++) {
		const Point& pt = samples->getSamplePos(k);
		Dtr_f1_i[0] += r[k];
		grad_sum += r_g[k];
		for (UInt x = 0; x < dim; x++) {
			Dtr_f1_i[x+1] += r[k]*pt[x];
			mat_f1[(1+x)*multiplier+(0)] += pt[x];
			mat_f1[(0)*multiplier+(1+x)] += pt[x];
			for (UInt y = x; y < dim; y++) {
				mat_f1[(1+x)*multiplier+(1+y)] += pt[x]*pt[y];
			}
		}
	}

	//****************************************************************
	// Second part, compute derivatives of gradient-fit
	//***************************************************************
	for (UInt i1 = 0; i1 < dim; i1++) {
		mat_f2[(1+i1)*multiplier+(1+i1)] = (ValueType)m; // m is numPoints
		Dtr_f2_i[1+i1] = grad_sum[i1];
	}

	for (UInt x = 0; x < multiplier; x++) {
		for (UInt y = x; y < multiplier; y++) {
			DtD(n2*multiplier+x, n2*multiplier+y) = theta*mat_f1[x*multiplier+y] + (1-theta)*mat_f2[x*multiplier+y];
		}
		Dtr[n2*multiplier+x] = theta*Dtr_f1_i[x] + (1-theta)*Dtr_f2_i[x];
	}
#endif


	//std::cerr << "-------------------------" << std::endl;
	//DtD.print();
	//DtD.printMaple(6);
	//std::cerr << "-------------------------" << std::endl;
	//exit(1);


	delete[] r;
	delete[] r_g;
}

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class RBF_Sum>
void LineSearch(RBFFunctionSamplesBase<typename RBF_Sum::Point>* samples, typename RBF_Sum::ValueType* x, typename RBF_Sum::ValueType error_0, typename RBF_Sum::ValueType theta, int n, RBF_Sum* rbf) {

	typedef RBF_Sum::ValueType ValueType;
	typedef RBF_Sum::Point Point;
	static const UInt dim = Point::dim;
	static const UInt multiplier = dim+1;

	UInt numLinSearch = 20;

	RBF_Sum rbf2(*rbf);

	std::cerr << "start: " << error_0 << std::endl;
	for (UInt i1 = 1; i1 <= numLinSearch; i1++) {
		ValueType u = (ValueType)(i1)/(ValueType)(1000*numLinSearch);

		addToRBF(rbf, x, -u);
		ValueType error_new = computeErrorWithGrad(samples, rbf, theta);
		addToRBF(rbf, x, u);

		std::cerr << u << ": " << error_new << std::endl;

	}

}

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class RBF_Sum>
void computeGradientDirection(RBFFunctionSamplesBase<typename RBF_Sum::Point>* samples, RBF_Sum* rbf, typename RBF_Sum::ValueType theta, typename RBF_Sum::ValueType* gradient) {

	typedef RBF_Sum::ValueType ValueType;
	typedef RBF_Sum::Point Point;
	typedef RBF_Sum::RBF_Type RBF_Type;
	typedef SquareMatrixND<Point> Mat;
	static const UInt dim = Point::dim;
	static const UInt multiplier = dim+1;

	UInt m = (UInt)samples->getNumSamples();
	UInt n = (UInt)rbf->getNumCenters();

	//VisualizeState2D_rbf(rbf, n/multiplier, pc, 0, true, "", 1);

	RBF_Type* basisfunction = &(rbf->basisfunction);
	ValueType theta2 = 1-theta;

	//std::cerr << "theta: " << theta << std::endl;
	//std::cerr << "m: " << m << "  n: " << n << std::endl;


	for (UInt i = 0; i < n; i++) {
		ValueType d_height_1 = 0;
		ValueType d_height_2 = 0;
		Point d_grad_1;
		Point d_grad_2;

		ValueType height_i	= rbf->m_heights[i];
		Point center_i		= rbf->m_centers[i];

		for (UInt j = 0; j < m; j++) {
			Point point_j	= samples->getSamplePos(j);
			Point normal_j  = samples->getSampleGrad(j);
			ValueType val_j	= samples->getSampleVal(j);

			Point dir_i			= point_j-center_i;
			ValueType dist_i	= dir_i.length();
			ValueType phi_i		= basisfunction->eval(dist_i);
			Point grad_phi_i	= dir_i*basisfunction->evalGradFactor(dist_i);
			Mat Hessian_i = basisfunction->evalHessian(dir_i);

			for (UInt k = 0; k < n; k++) {
				Point dir_k			= point_j-rbf->m_centers[k];
				ValueType phi_k		= basisfunction->eval(dir_k.length());
				Point grad_phi_k	= dir_k*basisfunction->evalGradFactor(dir_k.length());
				ValueType height_k	= rbf->m_heights[k];

				d_height_1	+= phi_i*phi_k*height_k;
				d_height_2	+= (grad_phi_i|grad_phi_k)*height_k;
				d_grad_1	+= -grad_phi_i*(phi_k*height_i*height_k);
				d_grad_2	+= -Hessian_i.vecTrans(grad_phi_k)*height_i*height_k;
			}
			d_height_1	-= phi_i*val_j;
			d_height_2	-= grad_phi_i|normal_j;
			d_grad_1	+= grad_phi_i*height_i*val_j;
			d_grad_2	+= Hessian_i.vecTrans(normal_j)*height_i;
		}
		gradient[i*multiplier+0] = (theta*d_height_1+theta2*d_height_2)*2;
		for (UInt i1 = 0; i1 < dim; i1++) gradient[i*multiplier+i1+1] = (theta*d_grad_1[i1]+theta2*d_grad_2[i1])*2;
	}

}


//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class RBF_Sum>
typename RBF_Sum::ValueType goldenSectionSearch(RBFFunctionSamplesBase<typename RBF_Sum::Point>* samples, RBF_Sum* rbf, typename RBF_Sum::ValueType* gradient, UInt numIter = 5, typename RBF_Sum::ValueType theta = 0.99) {

	typedef RBF_Sum::ValueType ValueType;

	ValueType one_by_goldenRatio = 1.0/1.61803399;

	ValueType e0 = computeErrorWithGrad(samples, rbf, theta);
	ValueType u0 = 0;
	// Find bracket
	ValueType u1 = -1;
	ValueType e1 = computeErrorWithGrad2(samples, rbf, gradient, u1, theta);
	ValueType u2 = u1;
	ValueType e2 = e1;
	ValueType u3, e3;

	while (e2 > e0  || e2 > e1) {
		u1 = u2;
		e1 = e2;
		u2 = one_by_goldenRatio*u1;
		e2 = computeErrorWithGrad2(samples, rbf, gradient, u2, theta);
		//std::cerr << e0 << " \t " << e2 << " \t " << e1 << std::endl;
	}

	//std::cerr << "Bracket:" << std::endl;
	//std::cerr << u0 << "  " << e0 << std::endl;
	//std::cerr << u1 << "  " << e1 << std::endl;
	//std::cerr << u2 << "  " << e2 << std::endl;

	for (UInt i3 = 0; i3 < numIter; i3++) {
		if ((u0-u2)>(u2-u1)) {
			u3 = u0 + (u2-u0)*one_by_goldenRatio;
			e3 = computeErrorWithGrad2(samples, rbf, gradient, u3, theta);
			if (e3 > e2) {
				u0 = u3;
				e0 = e3;
			} else {
				u1 = u2;
				e1 = e2;
				u2 = u3;
				e2 = e3;
			}
		} else {
			u3 = u2 + (u1-u2)*one_by_goldenRatio;
			e3 = computeErrorWithGrad2(samples, rbf, gradient, u3, theta);
			if (e3 > e2) {
				u1 = u3;
				e1 = e3;
			} else {
				u0 = u2;
				e0 = e2;
				u2 = u3;
				e2 = e3;
			}
		}
		//std::cerr << "> " << i3 << ", " << u3 << " -> " << e2 << std::endl;
	}
	addToRBF(rbf, gradient, u2);

	return e2;
}

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class ValueType>
ValueType computeGradSquareLength(ValueType* x, UInt n) {
	ValueType squareSum = 0;
	for (UInt i1 = 0; i1 < n; i1++) {
		squareSum += x[i1]*x[i1];
	}
	return squareSum;
}

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class RBF_Sum>
void conjugateGradientGoldenSection(RBFFunctionSamplesBase<typename RBF_Sum::Point>* samples, RBF_Sum* rbf, UInt numIter = 200, typename RBF_Sum::ValueType theta = 0.99) {

	typedef RBF_Sum::ValueType ValueType;
	int n = rbf->getNumCenters()*(RBF_Sum::Point::dim+1);
	ValueType* gradient = new ValueType[n];
	ValueType* h_old = new ValueType[n];

	ValueType e = computeErrorWithGrad(samples, rbf, theta);

	computeGradientDirection(samples, rbf, theta, h_old);
	ValueType length_old = computeGradSquareLength(h_old, n);

	for (UInt i1 = 0; i1 < numIter; i1++) {
		std::cerr << i1 << " " << e << std::endl;

		computeGradientDirection(samples, rbf, theta, gradient);
		ValueType length_new = computeGradSquareLength(gradient, n);
        
		ValueType fac = length_new/length_old;

		for (int i2 = 0; i2 < n; i2++) {
			h_old[i2] = gradient[i2] + fac*h_old[i2];
		}
		length_old = length_new;

		e = goldenSectionSearch(samples, rbf, h_old, 5, theta);
	}
	delete[] gradient;
}

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class RBF_Sum>
void gradientGoldenSection(RBFFunctionSamplesBase<typename RBF_Sum::Point>* samples, RBF_Sum* rbf, UInt numIter = 200, typename RBF_Sum::ValueType theta = 0.99) {

	typedef RBF_Sum::ValueType ValueType;
	ValueType* gradient = new ValueType[rbf->getNumCenters()*(RBF_Sum::Point::dim+1)];

	for (UInt i1 = 0; i1 < numIter; i1++) {
		computeGradientDirection(samples, rbf, theta, gradient);
		ValueType e = goldenSectionSearch(samples, rbf, gradient, 5, theta);
		std::cerr << i1 << " " << e << std::endl;
	}
	delete[] gradient;
}


//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class RBF_Sum>
void improveRBF_Fit_Grad(RBFFunctionSamplesBase<typename RBF_Sum::Point>* samples, RBF_Sum* rbf, UInt numIter = 200, typename RBF_Sum::ValueType factor = (RBF_Sum::ValueType)0.5, typename RBF_Sum::ValueType theta = 0.95) {

	typedef RBF_Sum::ValueType ValueType;
	typedef RBF_Sum::Point Point;
	typedef RBF_Sum::RBF_Type RBF_Type;
	static const UInt dim = Point::dim;
	static const UInt multiplier = dim+1;
	// We have 'm' samples and 'n' unknown
	int m = (int)samples->getNumSamples();
	int n = rbf->getNumCenters()*multiplier;
    
	int numUnknown = n;

#ifdef USE_LINEAR_TERM
	numUnknown += multiplier;
#endif


	MathMatrix2<ValueType> DtD(numUnknown);
	MathMatrix2<ValueType> Chol(numUnknown);
	ValueType* Dtr = new ValueType[numUnknown];
	ValueType* diag = new ValueType[numUnknown];
	ValueType* x = new ValueType[numUnknown];
	ValueType* gradient = new ValueType[numUnknown];
	//ValueType lambda_0 = (ValueType)1.e-30;
	ValueType lambda_0 = (ValueType)0.000001;
	ValueType lambda = lambda_0;
	ValueType inc_factor_v = 10;

	for (int i1 = 0; i1 < n; i1++) {
		gradient[i1] = 0;
	}

	// Helper data for cholesky
	MathMatrix2<double> A2(numUnknown);
	MathMatrix2<double> Chol2(numUnknown);
	double* diag2 = new double[numUnknown];
	double* b2 = new double[numUnknown];
	double* x2 = new double[numUnknown];



	ValueType currentError = computeErrorWithGrad(samples, rbf, theta)/m;

#ifdef PRINT_VERBOSE
	std::cerr << "Error before opt: " << currentError << std::endl;
#endif

	ValueType startError = currentError;

	std::stringstream ss;
	ss << rbf->getNumCenters() << "_cent.txt";
	std::ofstream fileout(ss.str().c_str());
	fileout << "0 " << currentError << std::endl;

	//VisualizeState2D_V2(rbf, samples, 0, true, "imgs2/screenshot_");

	for (UInt i1 = 0; i1 < numIter; i1++) {

		//VisualizeState2D_V2(rbf, samples, i1, false, "imgs/screenshot_");

		// Compute DtD
		//computeDtDwithGrad(samples, rbf, DtD, Dtr, theta);

		//JBS_Timer tt;
		//tt.log("compute System Matrix");
		computeDtDwithGradV2(samples, rbf, DtD, Dtr, theta);
		//DtD.printMaple(3, "DtD.txt");
		//exit(1);

		//tt.log("solve");

		// Solve Normal Equation
		ValueType maxDiag = 0;
		for (int i2 = 0; i2 < numUnknown; i2++) {
			maxDiag = std::max(fabs(DtD(i2,i2)), maxDiag);
		}
		for (int i2 = 0; i2 < numUnknown; i2++) {
			DtD(i2,i2) += lambda*maxDiag;
		}


		bool chol_res = solveCholdouble(DtD, Dtr, x, A2, Chol2, diag2, b2, x2);
		int xxx = 0;
		while (!chol_res) {
			xxx++;
			if (xxx > 100) {
				break;
			}

			std::cerr << ".";
			for (int i2 = 0; i2 < numUnknown; i2++) {
				DtD(i2,i2) += lambda*maxDiag;
			}
			chol_res = solveCholdouble(DtD, Dtr, x, A2, Chol2, diag2, b2, x2);
		}
		if (xxx > 100) break;

		//tt.log("update rbf and compute new error");



		// Update rbf
		for (int i2 = 0; i2 < n; i2+=multiplier) {
			rbf->m_heights[i2/multiplier] -= x[i2];
			Point center;
			for (int i = 0; i < dim; i++) center[i] = x[i2+i+1];
			rbf->m_centers[i2/multiplier] -= center;
		}
#ifdef USE_LINEAR_TERM
		rbf->d -= x[n];
		for (UInt i = 0; i < dim; i++) rbf->linear_term[i] -= x[n+i+1];
#endif
		ValueType error_new = computeErrorWithGrad(samples, rbf, theta)/m;

		//std::cerr << "-----------" << std::endl;

		//if (error_new < currentError)
		//	lambda /= inc_factor_v; // Decrease Damping

		//tt.log("find optimal lambda");

		while (error_new > currentError) {
			// Undo last step
			for (int i2 = 0; i2 < n; i2+=multiplier) {
				rbf->m_heights[i2/multiplier] += x[i2];
				Point center;
				for (int i = 0; i < dim; i++) center[i] = x[i2+i+1];
				rbf->m_centers[i2/multiplier] += center;
			}
#ifdef USE_LINEAR_TERM
			rbf->d += x[n];
			for (UInt i = 0; i < dim; i++) rbf->linear_term[i] += x[n+i+1];
#endif
			ValueType maxDiag = 0;
			for (int i2 = 0; i2 < numUnknown; i2++) {
				maxDiag = std::max(fabs(DtD(i2,i2)), maxDiag);
			}
			// Move descent direction to gradient direction
			for (int i2 = 0; i2 < numUnknown; i2++) {
				DtD(i2,i2) += lambda*maxDiag;
			}
			// Solve Normal Equation
			solveCholdouble(DtD, Dtr, x, A2, Chol2, diag2, b2, x2);
			//bool cholRes = DtD.CHOLESKY_decomp(Chol, diag);
			//Chol.CHOLESKY_bksolve(diag, Dtr, x);

			// Update rbf
			for (int i2 = 0; i2 < n; i2+=multiplier) {
				rbf->m_heights[i2/multiplier] -= x[i2];
				Point center;
				for (int i = 0; i < dim; i++) center[i] = x[i2+i+1];
				rbf->m_centers[i2/multiplier] -= center;
			}
#ifdef USE_LINEAR_TERM
			rbf->d -= x[n];
			for (UInt i = 0; i < dim; i++) rbf->linear_term[i] -= x[n+i+1];
#endif
			error_new = computeErrorWithGrad(samples, rbf, theta)/m;
			//std::cerr << "lambda: " << lambda << " new_error: " << error_new << std::endl;

			lambda *= inc_factor_v;

			//ValueType x_sum = 0;
			//for (int i2 = 0; i2 < n; i2++) {
			//	x_sum += x[i2]*x[i2];
			//}
			//if (x_sum < 0.0000001) {
			//	i1 = numIter;
			//	break;
			//}

			//if (fabs(currentError - error_new) < 0.00000001) {
			//	std::cerr << "Found local minimum" << std::endl;
			//	i1 = numIter;
			//	break;
			//}

			if (error_new < currentError) {
				lambda = lambda / (inc_factor_v*inc_factor_v);
			}
		}

		//tt.log();
		//tt.print();

#ifdef PRINT_VERBOSE
		std::cerr << std::endl << lambda << std::endl;
#endif

		if (fabs(currentError - error_new) < 0.000000001) {
#ifdef PRINT_VERBOSE
			std::cerr << "Found minimum" << std::endl;
#endif
			i1 = numIter;
		}

		currentError = error_new;

		if (startError*factor > currentError) {
#ifdef PRINT_VERBOSE
			std::cerr << "Reached " << factor*100 << " percent after " << i1 << " iterations" << std::endl;
#endif
			i1 = numIter;
		}

		//std::cerr << "current_error: " << currentError << std::endl;
#ifdef PRINT_VERBOSE
		std::cerr << i1 << " current_error: " << currentError << "                 \r";
#endif
		//std::cerr << "------------------" << std::endl;
		//rbf->print();

		fileout << (i1+1) << " " << currentError << std::endl;

	}
	//std::cerr << "Error after optimization " << currentError << std::endl;


    delete[] x;
    delete[] diag;
    delete[] Dtr;
	delete[] gradient;
	delete[] diag2;
	delete[] b2;
	delete[] x2;

}

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************


#endif
