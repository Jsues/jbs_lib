#ifndef JBS_GENERAL_H
#define JBS_GENERAL_H


#include <limits>


typedef unsigned int UInt;
typedef unsigned char UChar;

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288 //declare our M_PI constant
#endif

#ifndef M_PI2
#define M_PI2 6.2831853 //declare our M_PI2 constant
#endif

template <class T>
bool isInfinity(T val) {
	if (val >= std::numeric_limits<T>::infinity()) return true;
	if (val <= -std::numeric_limits<T>::infinity()) return true;
	if (val != val) return true;
	return false;
}

#endif
