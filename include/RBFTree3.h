#ifndef RBF_TREE_V3_H
#define RBF_TREE_V3_H


#include <set>

#include "point2d.h"
#include "point3d.h"
#include "point4d.h"
#include "ScalarFunction.h"
#include "rbf.h"
#include "nonlinear_opt_with_grad.h"
#include "fit_rbf.h"
#include "rng.h"


#ifdef USE_CUDA_JBS
#include "cuda\ApproximateRBF_GPU.h"
#include "cuda\Array.h"
#endif


#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************
template <class Point> class RBFTree3;
// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************
#ifdef USE_BSPLINE_WEIGHT
	//! 'r' must be within [0..1]!
	template<class T>
	T getWeight(T r) {
		r *= 1.5;
		if (r < 0.5)
			return (T)0.75 - r*r;
		else
			return (T)0.5*((T)1.5-r)*((T)1.5-r);
	}
#else
	//! 'r' must be within [0..1]!
	template<class T>
	T getWeight(T r) {
		return 1-(r*r*r*r*(5-4*r));
	}
#endif
// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


// ****************************************************************************************
//! A function which is zero everywhere.
template <class Point>
class ZeroFunction : public ScalarFunction<Point> {
	virtual ValueType eval(const Point& pt) const { return 0; }
	virtual ValueType evalValAndGrad(const Point& pt, Point& grad) const { grad = Point((ValueType)0); return 0; }
	virtual UInt getID() const { return ScalarFunction::TYPE_VOID_FUNC; }
};
// ****************************************************************************************


template <class T>
class RBFTree3_Cell {

public:
	//! Point type, e.g. vec2d, vec3d, vec4f...
	typedef T Point;
	//! floating point type (double || float)
	typedef typename T::ValueType ValueType;
	//! Vertex dimension, 2 or 3 or 4...
	static const UInt dim = Point::dim;
	//! Number of children = 2_tothepowerof_dim (8 for 3D, 16 for 4D...).
	static const UInt numChildren = 1 << dim;
	//! Short cut for Cell
	typedef RBFTree3_Cell<Point> Node;
	//! Short cut for Tree
	typedef RBFTree3<Point> Tree;
	//! Short cut for RBF Basis Function
	typedef InverseQuadratic<ValueType, Point> RBF_Type;
	//typedef multiquadric<ValueType, Point> RBF_Type;
	//typedef Gaussian<ValueType, Point> RBF_Type;
	//! Short cut for an implicit function
	typedef ScalarFunction<Point> Function;
	//! Short cut for an implicit void function which is zero anywhere
	typedef ZeroFunction<Point> VoidFunction;
	//! Short cut for a RBF sum function.
	typedef RBF_sum<RBF_Type> RBFFunction;
	//! Allow tree to access our private members
	friend class Tree;
	static const UInt NUM_CENTERS_PER_NODE = NUM_CENTERS_PER_NODE_DEFINE;
	static const UInt NUM_CENTERS_PER_NODE_CUDA = 16;
	static const UInt NUM_ITER_IMPROVE_RBF = 50;
	static const UInt FIT_LEVELS_CUDA = 1;
	#define THETA 0.95
	#define IMPROVE_TO_PERCENT 0.1
	#define C_SQUARE_START_VAL 10
	#define CENTER_OFFSET 0.0

// ****************************************************************************************

	//! Constructor.
	RBFTree3_Cell(Tree* tree_, Point min_, Point max_, Node* father_ = NULL) : m_tree(tree_), m_min(min_), m_max(max_), m_father(father_) {
		if (m_father == NULL) m_nodelevel = 0;
		else m_nodelevel = m_father->getNodeLevel()+1;
		m_children = NULL;
		m_center = (m_min+m_max)/2;
		m_searchRange = max_.dist(min_)*m_tree->m_search_radius;
		m_searchRangeSquare = m_searchRange*m_searchRange;
		m_func = NULL;
		m_maxError = 0;
	}

// ****************************************************************************************
	
	//! Denstructor.
	~RBFTree3_Cell() {
		if (m_func != NULL) delete m_func;
		if (!isLeafNode()) { // we have kids, kill them!
			for (UInt i1 = 0; i1 < numChildren; i1++) {
				delete m_children[i1];
			}
			delete[] m_children;
		}
	}

// ****************************************************************************************

	//! Returns true is the 'pt' > 'm_min' && 'pt' <= 'm_max'.
	inline bool PointIsIn(const Point& pt) const {
		if (pt > m_min && pt <= m_max) return true;
		else return false;
	}

// ****************************************************************************************

	//! Returns true is within the nodes search range.
	inline bool PointIsInSearchRange(const Point& pt) const {
		if (pt.squareDist(m_center) < m_searchRangeSquare) return true;
		else return false;
	}

// ****************************************************************************************

	//! Returns the weight of the point 'pt' with respect to the current cell.
	inline ValueType getWeightForPoint(const Point& pt) const {
		ValueType distSquare = pt.squareDist(m_center);
		if (distSquare > m_searchRangeSquare) return 0;
		else {
			return getWeight(sqrt(distSquare) / m_searchRange);
		}
	}

// ****************************************************************************************

	inline Point getInterpolatedPosLower(bool *bitmask) const {
		Point v;
		for (UInt i = 0; i < dim; ++i) v[i] = (m_min[i] * (2-(int)bitmask[i]) + m_max[i] * (int)bitmask[i]) / ValueType(2.0);
		return v;
	}

// ****************************************************************************************

	inline Point getInterpolatedPosHigher(bool *bitmask) const {
		Point v;
		for (UInt i = 0; i < dim; ++i) v[i] = (m_min[i] * (1-(int)bitmask[i]) + m_max[i] * ((int)bitmask[i]+1)) / ValueType(2.0);
		return v;
	}

// ****************************************************************************************

	inline char getNodeLevel() const { return m_nodelevel; }

// ****************************************************************************************

	inline bool isLeafNode() const { return (m_children == NULL); }

// ****************************************************************************************

	inline bool hasRealFunction() const {
		if ((m_func != NULL) && (m_func->getID() == ScalarFunction<Point>::TYPE_RBF_SUM)) return true;
		return false;
	}

// ****************************************************************************************

	//! Splits the node, i.e. creates the children and stores them in 'm_children'.
	inline void splitNode() {
		m_children = new Node*[numChildren];
		for (UInt i = 0; i < numChildren; ++i) {
			bool bitmask[dim];
			for (UInt j = 0; j < dim; ++j) {
				bitmask[j] = ((i & (1 << j)) != 0);
			}
			m_children[i] = new Node(m_tree, getInterpolatedPosLower(bitmask), getInterpolatedPosHigher(bitmask), this);
		}

		// Distribute node content to children:
		for (UInt i1 = 0; i1 < PointsInNodeSR.size(); i1++) {
			for (UInt i = 0; i < numChildren; ++i) {
				m_children[i]->addPointIfInSR(PointsInNodeSR[i1]);
			}
		}
		clearPointsInNodeSR();
	}

// ****************************************************************************************

	inline void addPointIfInSR(const UInt& id) {
		const Point& pt = m_tree->m_sample_pos[id];
		if (PointIsInSearchRange(pt)) PointsInNodeSR.push_back(id);
	}

// ****************************************************************************************

	void fit() {

		if (PointsInNodeSR.size() <= dim+1) {
			m_func = new VoidFunction;
			return;
		}

		//! Create set of function values and gradients which should be approximated
		RBFFunctionSamplesRBF_TreeWrap<Point> fs((UInt)PointsInNodeSR.size(), &PointsInNodeSR[0], m_tree->m_sample_pos, m_tree->m_sample_residuals, m_tree->m_sample_residuals_grad);

#ifdef USE_CUDA_JBS
		if (getNodeLevel() < FIT_LEVELS_CUDA)
			getInitialFitCuda(&fs);
		else
			getInitialFit(&fs);
#else
		getInitialFit(&fs);
#endif

#ifdef USE_NONLINEAR_OPT
		RBFFunction* rbf_func = dynamic_cast<RBFFunction*>(m_func);
		if (getNodeLevel() < 2)
			improveRBF_Fit_Grad< RBF_sum<RBF_Type> >(&fs, rbf_func, NUM_ITER_IMPROVE_RBF, (ValueType)IMPROVE_TO_PERCENT, (ValueType)THETA);
#endif

	}

// ****************************************************************************************

	inline void fitVoidFunction() {
		m_func = new VoidFunction;
	}

// ****************************************************************************************

	inline ValueType eval(const Point& pos) const {
		return m_func->eval(pos);
	}

// ****************************************************************************************

	inline ValueType getMaxError() const { return m_maxError; }

// ****************************************************************************************

	const Node* findNodeAtLevel(const Point& pt, const UInt& level) const {
		if (PointIsIn(pt)) {
			if (getNodeLevel() == level) return this;
			/* else check children */
			for (UInt i1 = 0; i1 < numChildren; i1++) {
				const Node* n = m_children[i1]->findNodeAtLevel(pt, level);
				if (n != 0) return n;
			}
			std::cerr << "RBFTree3_Cell::findNodeAtLevel(): I should never be here" << std::endl;
			return 0;
		}
		return 0;
	}

// ****************************************************************************************

	const Node* findNode(const Point& pt) const {
		if (PointIsIn(pt)) {
			if (isLeafNode()) {
				return this;
			}
			/* else check children */
			for (UInt i1 = 0; i1 < numChildren; i1++) {
				const Node* n = m_children[i1]->findNode(pt);
				if (n != 0) return n;
			}
			std::cerr << "RBFTree3_Cell::findNode(): I should never be here" << std::endl;
			return 0;
		}
		return 0;
	}

// ****************************************************************************************

	inline Point getMin() const {
		return m_min;
	}

// ****************************************************************************************

	inline Point getMax() const {
		return m_max;
	}

// ****************************************************************************************

private:

#ifdef USE_CUDA_JBS
	bool getInitialFitCuda(RBFFunctionSamplesRBF_TreeWrap<Point>* fs) {

		// Select centers:
		std::set<UInt> selectedCenters;
		UInt numCenters = std::min((UInt)PointsInNodeSR.size(), NUM_CENTERS_PER_NODE_CUDA);

		RNG randnum(1);
		while (selectedCenters.size() < numCenters) {
			long r_int = randnum.rand_int31();
			UInt id = (UInt)abs((long)(r_int % PointsInNodeSR.size()));
			selectedCenters.insert(id);
		}
		Point* centers = new Point[numCenters];
		UInt cnt = 0;

		ValueType offset = m_searchRange*(ValueType)CENTER_OFFSET;
		for (std::set<UInt>::iterator it = selectedCenters.begin(); it != selectedCenters.end(); ++it) {
			centers[cnt++] = m_tree->m_sample_pos[PointsInNodeSR[*it]] + m_tree->m_sample_target_grad[PointsInNodeSR[*it]]*offset;
			offset *= -1;
		}

		ValueType c_square = C_SQUARE_START_VAL;
		c_square *= pow((float)4, ((float)getNodeLevel()/2.0f));

		// TODO: CUDA goes here!
		m_tree->GPUapprox->m_deltaSquared = c_square;
		m_tree->GPUapprox->setCenterFromFloat3((const float*)centers);
		m_tree->GPUapprox->setCellIndexArray(&PointsInNodeSR[0], (int)PointsInNodeSR.size(), m_center.x, m_center.y, m_center.z, m_searchRange, c_square);	
		m_tree->GPUapprox->copyConstantParametersToGPU();
		m_tree->GPUapprox->copyCellParametersToGPU();		
		m_tree->GPUapprox->copyCenterPosAndWeightsToGPU();
		//JBS_Timer t;
		//t.log("Initial Reconstruction");
		m_tree->GPUapprox->initialFit();	
		//t.log("nonlinear Fit");
		if (NUM_ITER_IMPROVE_RBF > 0) m_tree->GPUapprox->optimizeFit(NUM_ITER_IMPROVE_RBF);
		//t.log();
		//t.print();

		const float* weights = m_tree->GPUapprox->m_rbfWeights->getConstPtr();
		for(UInt i = 0; i < numCenters; i++) {
			centers[i].x = m_tree->GPUapprox->m_centerPos->get(i).x;
			centers[i].y = m_tree->GPUapprox->m_centerPos->get(i).y;
			centers[i].z = m_tree->GPUapprox->m_centerPos->get(i).z;
		}
		RBFFunction* rbf = new RBFFunction(numCenters, centers, weights, 1, c_square);

		rbf->d = m_tree->GPUapprox->m_affinePlaned;
		rbf->linear_term = vec3f(m_tree->GPUapprox->m_affinePlanex, m_tree->GPUapprox->m_affinePlaney, m_tree->GPUapprox->m_affinePlanez);
		m_func = rbf;

		delete[] centers;

		return true;
	}
#endif

// ****************************************************************************************

	bool getInitialFit(RBFFunctionSamplesRBF_TreeWrap<Point>* fs) {
		// Select centers:
		std::set<UInt> selectedCenters;
		UInt numCenters = std::min((UInt)PointsInNodeSR.size(), NUM_CENTERS_PER_NODE);

		RNG randnum(1);
		while (selectedCenters.size() < numCenters) {
			long r_int = randnum.rand_int31();
			UInt id = (UInt)abs((long)(r_int % PointsInNodeSR.size()));
			selectedCenters.insert(id);
		}
		Point* centers = new Point[numCenters];
		UInt cnt = 0;

		ValueType offset = m_searchRange*(ValueType)CENTER_OFFSET;
		//std::cerr << "m_searchRange: " << m_searchRange << " CENTER_OFFSET " << CENTER_OFFSET << std::endl;
		for (std::set<UInt>::iterator it = selectedCenters.begin(); it != selectedCenters.end(); ++it) {
			centers[cnt++] = m_tree->m_sample_pos[PointsInNodeSR[*it]] + m_tree->m_sample_target_grad[PointsInNodeSR[*it]]*offset;
			//centers[cnt++] = m_tree->m_sample_pos[PointsInNodeSR[*it]] - m_tree->m_sample_target_grad[PointsInNodeSR[*it]]*offset;
			offset *= -1;
		}

		ValueType c_square = C_SQUARE_START_VAL;
		//c_square *= pow((float)1.5, (int)(getNodeLevel()));
		c_square *= pow((float)4, ((float)getNodeLevel()/2.0f));
		//std::cerr << getNodeLevel() << " -> " << pow((float)4, (int)(getNodeLevel())) << std::endl;
		//if (getNodeLevel() == 2) exit(1);

		//ValueType theta = (ValueType)THETA;
		//if (numCenters < 10) theta = 1;
		//if (getNodeLevel() >= 4) theta = 1;

		m_func = aproximateWithGradient<RBF_Type>(centers, numCenters, fs, 1, c_square, (ValueType)THETA, (ValueType)0.01);
		delete[] centers;

		return true;
	}

// ****************************************************************************************

	void clearPointsInNodeSR() {
		std::vector<UInt>().swap(PointsInNodeSR);
	}

// ****************************************************************************************

	//! Center of the cell (('m_min'+'m_max')/2).
	Point m_center;
	//! Radius in which Points will be fitted and blended
	ValueType m_searchRange;
	ValueType m_searchRangeSquare;

	public:
	//! The implicit function in this cell.
	Function* m_func;

	//! The parent tree.
	Tree* m_tree;

	//! The maximum error in this cell.
	ValueType m_maxError;

	//! Pre-allocated data structure for nn-search.
	std::vector<UInt> PointsInNodeSR;

	//! The cell's lower left corner.
	Point m_min;
	//! The cell's upper right corner.
	Point m_max;
	//! Pointer to node's father.
	Node* m_father;
	//! Array of pointers to the nodes children (set to NULL in Constructor).
	Node** m_children;
	//! Level (=depth) in tree.
	char m_nodelevel;

};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class RBFTree3 : public ScalarFunctionWithConfidence<T> {

public:

	typedef T Point;
	static const UInt dim = Point::dim;
	static const UInt numChildren = 1 << dim;
	typedef typename Point::ValueType ValueType;
	typedef RBFTree3<Point> Tree;
	typedef RBFTree3_Cell<Point> Cell;
	typedef RBFFunctionSamples<Point> FunctionSamples;
	friend class Cell;

// ****************************************************************************************

	RBFTree3(FunctionSamples* fs, ValueType max_error = ValueType(0.005), ValueType search_radius_ = ValueType(0.55), Point min_ = Point(ValueType(0)), Point max_ = Point(ValueType(1)), ValueType diagEnlarge = (ValueType)0.05) {

		//! Enlarge domain a little
		Point diag = max_-min_;
		diag *= diagEnlarge;
		m_min = min_-diag;
		m_max = max_+diag;
		
		m_search_radius = search_radius_;
		m_max_error = max_error;
		m_treeDepth = 0;

		m_numSamples = fs->getNumSamples();
		m_sample_residuals = new ValueType[m_numSamples];
		m_sample_pos = new Point[m_numSamples];
		m_sample_target_val = new ValueType[m_numSamples];
		m_sample_residuals_grad = new Point[m_numSamples];
		m_sample_target_grad = new Point[m_numSamples];
		for (UInt i1 = 0; i1 < m_numSamples; i1++) {
			m_sample_residuals[i1] = fs->getSampleVal(i1);
			m_sample_target_val[i1] = m_sample_residuals[i1];
			m_sample_residuals_grad[i1] = fs->getSampleGrad(i1);
			m_sample_target_grad[i1] = m_sample_residuals_grad[i1];
			m_sample_pos[i1] = fs->getSamplePos(i1);
		}

		res_upd_val = new ValueType[m_numSamples];
		res_upd_wei = new ValueType[m_numSamples];
		res_upd_gra = new Point[m_numSamples];

		//! Allocate root node.
		m_root = new Cell(this, m_min, m_max, NULL);
		//! Add all points to the root node.
		m_root->PointsInNodeSR.reserve(m_numSamples);
		for (UInt i1 = 0; i1 < m_numSamples; i1++) m_root->PointsInNodeSR.push_back(i1);

		size1neighborhood = (UInt)pow(3.0f, (int)dim);
		oneneighborhood = new Point[size1neighborhood];
		for (int i1 = 0; i1 < (int)size1neighborhood; i1++) {
			for (int i2 = 0; i2 < dim; i2++) {
				oneneighborhood[i1][i2] = (ValueType)((i1/(int)pow((float)3,(i2)))%3)-1;
			}
			//oneneighborhood[i1].print();
		}

#ifdef USE_CUDA_JBS
		GPUapprox = NULL;
		if (Cell::FIT_LEVELS_CUDA > 0) {
			GPUapprox = new ApproximateRBF_GPU(m_numSamples, Cell::NUM_CENTERS_PER_NODE_CUDA, (float)THETA);
			GPUapprox->reserveMem();

			std::cerr << "Upload data to GPU...";
			GPUapprox->setSampleGradientValue((const float*)m_sample_target_grad, m_sample_target_val);
			GPUapprox->setSamplePosFromFloat3((const float*)m_sample_pos);	
			std::cerr << "done" << std::endl;
		}
#endif
	}

// ****************************************************************************************

	~RBFTree3() {
		delete[] oneneighborhood;
		delete[] m_sample_residuals;
		delete[] m_sample_pos;
		delete[] m_sample_target_val;
		delete[] m_sample_residuals_grad;
		delete[] m_sample_target_grad;
		delete[] res_upd_val;
		delete[] res_upd_wei;
		delete[] res_upd_gra;
		delete m_root;
#ifdef USE_CUDA_JBS
		if (GPUapprox != NULL) delete GPUapprox;
#endif
	}

// ****************************************************************************************

	void fitNextLevel(bool verboseOutput = true) {

		std::vector<Cell*> new_nodeFront;

		if (m_treeDepth == 0) { // fit first level
			m_root->fit();
			new_nodeFront.push_back(m_root);
		} else { // fit another level
			for (std::vector<Cell*>::iterator it = m_nodeFront.begin(); it != m_nodeFront.end(); ++it) {
				Cell* n = (*it);
				if (n->getMaxError() > m_max_error) { // Split node and compute functions.
					n->splitNode();
#pragma omp parallel for
					for (int i1 = 0; i1 < numChildren; i1++) {
						n->m_children[i1]->fit();
						
						#pragma omp critical 
						new_nodeFront.push_back(n->m_children[i1]);
					}
				} else {
					n->clearPointsInNodeSR();
				}
			}
		}


		// Update residuals
		//updateResiduals();

		//*********************************
		// Incremental residuals update
		//*********************************
		// Zero out resis

#pragma omp parallel for
		for (int i1 = 0; i1 < (int)m_numSamples; i1++) {
			res_upd_val[i1] = 0;
			res_upd_wei[i1] = 0;
			res_upd_gra[i1] = Point((ValueType)0);
		}


		// Compute contribution of recently fitted nodes:
		ValueType val; ValueType wei; Point grad;

//#pragma omp parallel for
		for (int i0 = 0; i0 < (int)new_nodeFront.size(); i0++) {
//		for (std::vector<Cell*>::iterator it = new_nodeFront.begin(); it != new_nodeFront.end(); ++it) {
			Cell* n = new_nodeFront[i0];//(*it);
			ValueType max_error = 0;

			for (int i1 = 0; i1 < n->PointsInNodeSR.size(); i1++) {
				const UInt& id = n->PointsInNodeSR[i1];
				const Point& pt = m_sample_pos[id];
#ifdef NO_CELL_BLENDING
				if (!n->PointIsIn(pt)) continue;
#endif
				wei = n->getWeightForPoint(pt);
				val = n->m_func->evalValAndGrad(pt, grad);
				res_upd_val[id] += val*wei;
				res_upd_wei[id] += wei;
				res_upd_gra[id] += grad*wei;
				ValueType error = fabs(m_sample_residuals[id]-val);
				if (error > max_error) max_error = error;
			}
			n->m_maxError = max_error;
		}
		// Update:
		ValueType maxError = 0;

#pragma omp parallel for
		for (int i1 = 0; i1 < (int)m_numSamples; i1++) {
			if (res_upd_wei[i1] > 0) {
				const Point& pos = m_sample_pos[i1];
#ifndef NO_INTER_LEVEL_BLENDING
				res_upd_wei[i1] = getTotalWeightAtLevel(pos, m_treeDepth);
#endif
				m_sample_residuals[i1] -= res_upd_val[i1]/res_upd_wei[i1];
				m_sample_residuals_grad[i1] -= res_upd_gra[i1]/res_upd_wei[i1];
				if (maxError < fabs(m_sample_residuals[i1])) maxError = fabs(m_sample_residuals[i1]);
			}
		}

#ifdef USE_CUDA_JBS
		if (m_treeDepth == 0 && Cell::FIT_LEVELS_CUDA > 1) {
			std::cerr << "Upload data to GPU...";
			GPUapprox->setSampleGradientValue((const float*)m_sample_residuals_grad, m_sample_residuals);
			std::cerr << "done" << std::endl;
		}
#endif



		// Clear nodefront
		m_nodeFront.clear();
		// Copy new nodefront
		//ValueType max_error = 0;
		for (std::vector<Cell*>::iterator it = new_nodeFront.begin(); it != new_nodeFront.end(); ++it) {
			m_nodeFront.push_back(*it);
		//	max_error = std::max(max_error, (*it)->getMaxError());
		}
		m_treeDepth++;
		if (verboseOutput) std::cerr << std::endl << "Max. Error at level: " << m_treeDepth << "   : " << maxError << " nodes added: " << new_nodeFront.size() << std::endl;
	}

// ****************************************************************************************

	static inline void findCell(const Point& pos, Point& mmin, Point& mmax, const UInt& level) {

		if (level == 0) return;
		else {
			Point center = (mmin+mmax)/2;
			for (UInt i = 0; i < dim; i++) {
				if (pos[i] > center[i]) {
					mmin[i] = center[i];
					mmax[i] = mmax[i];
				} else {
					mmin[i] = mmin[i];
					mmax[i] = center[i];
				}
			}
			findCell(pos, mmin, mmax, level-1);
		}
	}

// ****************************************************************************************

	//! For unknown reasons, this version is much slower than a specialized method
	inline ValueType getTotalWeightAtLevelnD(const Point& pos, const UInt& level) const {
		Point mmin = m_min;
		Point mmax = m_max;

		findCell(pos, mmin, mmax, level);
		Point nodeCenter = (mmin+mmax)/2;

		ValueType level_weight = 0;
		ValueType cellSize = mmax.x - mmin.x;
		ValueType searchRadius = mmax.dist(mmin)*m_search_radius;
		ValueType searchRadiusSq = searchRadius*searchRadius;

		Point pos2;
		Point pos2_temp;


		for (UInt i1 = 0; i1 < size1neighborhood; i1++) {
			pos2		= pos + oneneighborhood[i1]*cellSize;
			pos2_temp	= pos - oneneighborhood[i1]*cellSize;
			// reject boundary cases:
			bool reject = false;
			for (int d = 0; d < dim; d++) {
				if ((pos2_temp[d] < m_min[d])||(pos2_temp[d] > m_max[d])) {
					reject = true;
					break;
				}
			}
			if (!reject) {
				ValueType distSq = nodeCenter.squareDist(pos2);
				if (distSq < searchRadiusSq) {
					level_weight += getWeight(sqrt(distSq)/searchRadius);
				}
			}
		}
		
		return level_weight;
	}

// ****************************************************************************************

	inline ValueType getTotalWeightAtLevel(const Point& pos, const UInt& level) const;

// ****************************************************************************************

	inline ValueType getTotalWeightAtLevel2D(const Point& pos, const UInt& level) const {
		Point mmin = m_min;
		Point mmax = m_max;

		findCell(pos, mmin, mmax, level);
		Point nodeCenter = (mmin+mmax)/2;

		ValueType level_weight = 0;
		ValueType cellSize = mmax.x - mmin.x;
		ValueType searchRadius = mmax.dist(mmin)*m_search_radius;
		ValueType searchRadiusSq = searchRadius*searchRadius;

		Point pos2;

		for (int x1 = -1; x1 <=1; x1++) { 
			pos2.x = pos.x + cellSize*x1; 
			//if (((pos.x - cellSize*x1) < m_min.x)||((pos.x - cellSize*x1) > m_max.x)) continue; 
			for (int x2 = -1; x2 <=1; x2++) { 
				pos2.y = pos.y + cellSize*x2; 
				//if (((pos.y - cellSize*x2) < m_min.y)||((pos.y - cellSize*x2) > m_max.y)) continue; 
				ValueType distSq = nodeCenter.squareDist(pos2); 
				if (distSq < searchRadiusSq) { 
					level_weight += getWeight(sqrt(distSq)/searchRadius); 
				}
			}
		}
		return level_weight;
	}

// ****************************************************************************************

	inline ValueType getTotalWeightAtLevel3D(const Point& pos, const UInt& level) const {
		Point mmin = m_min;
		Point mmax = m_max;

		findCell(pos, mmin, mmax, level);

		Point nodeCenter = (mmin+mmax)/2;

		ValueType level_weight = 0;
		ValueType cellSize = mmax.x - mmin.x;
		ValueType searchRadius = mmax.dist(mmin)*m_search_radius;
		ValueType searchRadiusSq = searchRadius*searchRadius;

		Point pos2;

		for (int x1 = -1; x1 <=1; x1++) { 
			pos2.x = pos.x + cellSize*x1; 
			//if (((pos.x - cellSize*x1) < m_min.x)||((pos.x - cellSize*x1) > m_max.x)) continue; 
			for (int x2 = -1; x2 <=1; x2++) { 
				pos2.y = pos.y + cellSize*x2; 
				//if (((pos.y - cellSize*x2) < m_min.y)||((pos.y - cellSize*x2) > m_max.y)) continue; 
				for (int x3 = -1; x3 <=1; x3++) { 
					pos2.z = pos.z + cellSize*x3; 
					//if (((pos.z - cellSize*x3) < m_min.z)||((pos.z - cellSize*x3) > m_max.z)) continue; 
					ValueType distSq = nodeCenter.squareDist(pos2); 
					if (distSq < searchRadiusSq) { 
						level_weight += getWeight(sqrt(distSq)/searchRadius); 
					}
				}
			}
		}
		return level_weight;
	}

// ****************************************************************************************

	inline ValueType getTotalWeightAtLevel4D(const Point& pos, const UInt& level) const {
		Point mmin = m_min;
		Point mmax = m_max;

		findCell(pos, mmin, mmax, level);
		Point nodeCenter = (mmin+mmax)/2;

		ValueType level_weight = 0;
		ValueType cellSize = mmax.x - mmin.x;
		ValueType searchRadius = mmax.dist(mmin)*m_search_radius;
		ValueType searchRadiusSq = searchRadius*searchRadius;

		Point pos2;

		for (int x1 = -1; x1 <=1; x1++) { 
			pos2.x = pos.x + cellSize*x1; 
			//if (((pos.x - cellSize*x1) < m_min.x)||((pos.x - cellSize*x1) > m_max.x)) continue; 
			for (int x2 = -1; x2 <=1; x2++) { 
				pos2.y = pos.y + cellSize*x2; 
				//if (((pos.y - cellSize*x2) < m_min.y)||((pos.y - cellSize*x2) > m_max.y)) continue; 
				for (int x3 = -1; x3 <=1; x3++) { 
					pos2.z = pos.z + cellSize*x3; 
					//if (((pos.z - cellSize*x3) < m_min.z)||((pos.z - cellSize*x3) > m_max.z)) continue; 
					for (int x4 = -1; x4 <=1; x4++) { 
						pos2.w = pos.w + cellSize*x4; 
						//if (((pos.w - cellSize*x4) < m_min.w)||((pos.w - cellSize*x4) > m_max.w)) continue; 
						ValueType distSq = nodeCenter.squareDist(pos2); 
						if (distSq < searchRadiusSq) { 
							level_weight += getWeight(sqrt(distSq)/searchRadius); 
						}
					}
				}
			}
		}
		return level_weight;
	}

// ****************************************************************************************

	virtual inline ValueType eval(const Point& pos) const {
		Point grad;
		return evalValAndGrad(pos, grad);
	}

// ****************************************************************************************

	inline ValueType evalConfidence(const Point& pos) const {
		//m_numSamples
		std::vector<Cell*> nodes;
		std::vector<Cell*> new_nodes;
		nodes.push_back(m_root);

		ValueType conf = 0;

		int level = 0;

		while(nodes.size() > 0) {
			
			ValueType level_conf = 0;
			ValueType weights = 0;

			// Accumulate influence of real nodes.
			for (std::vector<Cell*>::iterator it = nodes.begin(); it != nodes.end(); ++it) {
				Cell* n = (*it);

				ValueType weight = n->getWeightForPoint(pos);
				if (weight > 0) {

					UInt multiplier = 1 << (dim*level);

					ValueType c = ((ValueType)n->PointsInNodeSR.size()*multiplier)/m_numSamples;
					level_conf += c*weight;
					weights += weight;

					// Add child_nodes.
					if (!n->isLeafNode()) {
						for (UInt i1 = 0; i1 < numChildren; i1++) new_nodes.push_back(n->m_children[i1]);
					}
				}
			}

			if (weights > 0) conf += level_conf/weights;

			nodes.clear();
			// Copy data for next level
			for (UInt i1 = 0; i1 < new_nodes.size(); i1++) nodes.push_back(new_nodes[i1]);
			new_nodes.clear();

			level++;
		}

		return conf;
	}

// ****************************************************************************************

	virtual inline ValueType evalValAndGrad(const Point& pos, Point& grad) const {


		if (!m_root->PointIsIn(pos)) {
			//std::cerr << "out" << std::endl;
			//std::cerr << pos << std::endl;
			//std::cerr << m_root->m_min << " <-> " << m_root->m_max << std::endl;
			return m_root->m_func->evalValAndGrad(pos, grad);
		}

		std::vector<Cell*> nodes;
		std::vector<Cell*> new_nodes;
		nodes.push_back(m_root);

		ValueType value = 0;
		grad = Point((ValueType)0);
		Point g;
		ValueType f;
		ValueType weight;
		Cell* n;

		int level = 0;

		while(nodes.size() > 0) {
			
			ValueType level_value = 0;
			ValueType level_weight = 0;
			Point level_gradient((ValueType)0);


			//int removeme = 0;

			// Accumulate influence of real nodes.
			for (std::vector<Cell*>::iterator it = nodes.begin(); it != nodes.end(); ++it) {
				n = (*it);
				
				

#ifdef NO_CELL_BLENDING
				//removeme++;
				//if (level == 1) {
				//	if (removeme != 4) {
				//		continue;
				//	}
				//}
				if (!n->PointIsIn(pos)) continue;
#endif
				weight = n->getWeightForPoint(pos);
				//weight = 1;
				if (weight > 0) {
					f = n->m_func->evalValAndGrad(pos, g);

					level_value += f*weight;
					level_gradient += g*weight;
					level_weight += weight;

					// Add child_nodes.
					if (!n->isLeafNode()) {
						for (UInt i1 = 0; i1 < numChildren; i1++) new_nodes.push_back(n->m_children[i1]);
					} 
				}
			}

#ifndef NO_INTER_LEVEL_BLENDING
			level_weight = getTotalWeightAtLevel(pos, level);
#endif

			if (level_weight > 0) {
				value += level_value/level_weight;
				grad += level_gradient/level_weight;
			}

			nodes.clear();
			// Copy data for next level
			for (UInt i1 = 0; i1 < new_nodes.size(); i1++) nodes.push_back(new_nodes[i1]);
			new_nodes.clear();

			level++;
			
		}
		return value;
	}

// ****************************************************************************************

	inline Point getMin() const {
		return m_min;
	}

// ****************************************************************************************

	inline Point getMax() const {
		return m_max;
	}

// ****************************************************************************************

	std::vector<Cell*> getNodes() {
		std::vector<Cell*> nodes;


		std::vector<Cell*> nodeFront;
		nodeFront.push_back(m_root);

		while(nodeFront.size() > 0) {
			std::vector<Cell*> newNodeFront;
			for (UInt i1 = 0; i1 < nodeFront.size(); i1++) {
				Cell* n = nodeFront[i1];
				nodes.push_back(n);
				if (!n->isLeafNode()) {
					for (UInt i2 = 0; i2 < numChildren; i2++) {
						newNodeFront.push_back(n->m_children[i2]);
					}
				}
			}
			nodeFront.clear();
			for (UInt i1 = 0; i1 < newNodeFront.size(); i1++) {
				nodeFront.push_back(newNodeFront[i1]);
			}
		}

		return nodes;
	}

// ****************************************************************************************

	virtual UInt getID() const {
		return ScalarFunction::TYPE_RBF_TREE_3;
	}

// ****************************************************************************************

	void print10resi() {
		for (UInt i1 = 0; i1 < 10; i1++) {
			std::cerr << "(" << m_sample_residuals_grad[i1] << "), " << m_sample_residuals[i1] << std::endl;
		}
	}

// ****************************************************************************************

private:

// ****************************************************************************************

	void updateResiduals() {
		for (UInt i1 = 0; i1 < m_numSamples; i1++) {
			const Point &pt = m_sample_pos[i1];
			Point grad;
			ValueType f = evalValAndGrad(pt, grad);
			m_sample_residuals[i1] = m_sample_target_val[i1] - f;
			m_sample_residuals_grad[i1] = m_sample_target_grad[i1] - grad;

			ValueType error_abs = fabs(m_sample_residuals[i1]/grad.length());
			Cell* n = m_root->findNode(pt);

			n->m_maxError = std::max(n->m_maxError, error_abs);
		}
	}

// ****************************************************************************************

	//! Root node.
	Cell* m_root;
	//! Number of Samples
	UInt m_numSamples;
	//! Sample Positions
	Point* m_sample_pos;
	//! Sample Values
	ValueType* m_sample_target_val;
	//! Current residuals
	ValueType* m_sample_residuals;
	//! Current residuals of the gradient
	Point* m_sample_residuals_grad;
	//! Sample Gradients
	Point* m_sample_target_grad;

	//! Temp variable will be used during incremental residual update. Stores \sum(weight*val)
	ValueType* res_upd_val;
	//! Temp variable will be used during incremental residual update. Stores \sum(weight)
	ValueType* res_upd_wei;
	//! Temp variable will be used during incremental residual update. Stores \sum(weight*grad)
	Point*	   res_upd_gra;

	//! Max corner
	Point m_max;
	//! Min corner
	Point m_min;
	//! search radius for kd-tree search
	ValueType m_search_radius;
	//! Maxiumum allowed error before cell split.
	ValueType m_max_error;
	//! The current tree depth.
	UInt m_treeDepth;

	//! 1-voxel-neighborhood, 9 for 2D, 27 for 3D etc.
	UInt size1neighborhood;
	//! index-lookup for 1-neighborhood
	Point* oneneighborhood;

	//! Stores the cells which have been fitted in the last step.
	std::vector<Cell*> m_nodeFront;

#ifdef USE_CUDA_JBS
	ApproximateRBF_GPU* GPUapprox;
#endif

};

template <> inline double RBFTree3<vec2d>::getTotalWeightAtLevel(const vec2d& pos, const UInt& level) const { return getTotalWeightAtLevel2D(pos, level); }
template <> inline float  RBFTree3<vec2f>::getTotalWeightAtLevel(const vec2f& pos, const UInt& level) const { return getTotalWeightAtLevel2D(pos, level); }
template <> inline double RBFTree3<vec3d>::getTotalWeightAtLevel(const vec3d& pos, const UInt& level) const { return getTotalWeightAtLevel3D(pos, level); }
//template <> inline float  RBFTree3<vec3f>::getTotalWeightAtLevel(const vec3f& pos, const UInt& level) const { return getTotalWeightAtLevelnD(pos, level); }
template <> inline float  RBFTree3<vec3f>::getTotalWeightAtLevel(const vec3f& pos, const UInt& level) const { return getTotalWeightAtLevel3D(pos, level); }
template <> inline double RBFTree3<vec4d>::getTotalWeightAtLevel(const vec4d& pos, const UInt& level) const { return getTotalWeightAtLevel4D(pos, level); }
template <> inline float  RBFTree3<vec4f>::getTotalWeightAtLevel(const vec4f& pos, const UInt& level) const { return getTotalWeightAtLevel4D(pos, level); }
template <class T> inline typename T::ValueType RBFTree3<T>::getTotalWeightAtLevel(const T& pos, const UInt& level) const { return getTotalWeightAtLevelnD(pos, level); }


#endif
