#ifndef COMPUTE_AABB_H
#define COMPUTE_AABB_H


#include <vector>


template <class T>
void computeAABB(const std::vector<T>& pts, T& mmin, T& mmax) {

	if (pts.size() == 0) return;

	// initialize mmin and mmax with pts[0]
	mmin = pts[0];
	mmax = pts[0];

	for (unsigned int i1 = 0; i1 < pts.size(); i1++) {
		const T& p = pts[i1];
		for (unsigned int c = 0; c < T::dim; c++) {
			if (p[c] < mmin[c]) mmin[c] = p[c];
			if (p[c] > mmax[c]) mmax[c] = p[c];
		}
	}

}

template <class T>
void computeAABB(T* pts, size_t numPts, int vecdim, T& mmin, T& mmax) {
    
    if (numPts == 0) return;
    
    // initialize mmin and mmax with pts[0]
    mmin = pts[0];
    mmax = pts[0];
    
    for (unsigned int i1 = 0; i1 < numPts; i1++) {
        const T& p = pts[i1];
        for (unsigned int c = 0; c < vecdim; c++) {
            if (p[c] < mmin[c]) mmin[c] = p[c];
            if (p[c] > mmax[c]) mmax[c] = p[c];
        }
    }
}

template <class T>
auto computeAABB(T* pts, size_t numPts, T& mmin, T& mmax) -> decltype(T::dim, void()) {
    computeAABB(pts, numPts, T::dim, mmin, mmax);
}

template <class T>
auto computeAABB(T* pts, size_t numPts, T& mmin, T& mmax) -> decltype(pts->size(), void()) {
    computeAABB(pts, numPts, static_cast<int>(pts->size()), mmin, mmax);
}



#endif
