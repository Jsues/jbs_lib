#ifndef GENERIC_QUADRIC_H
#define GENERIC_QUADRIC_H


#include <vector>
#include <iomanip>

#include "JBS_General.h"
#include "point2d.h"
#include "point3d.h"
#include "point4d.h"
//#include "nr_lu.h"
#include "nr_templates.h"
#include "FileIO/VolumeIO.h"
#include "MatrixnD.h"


namespace JBSlib {

// ****************************************************************************************

//! number of unknown coefficients (dimension-dependant) (Matrix + Vector + const).
const UInt numCoefficients[]={3 /* 1D */ ,6 /* 2D */ ,10 /* 3D */ ,15 /* 4D */ }; 
//! number of unknown coefficients in Matrix only (dimension-dependant).
const UInt numMatrixEntrys[]={1 /* 1D */ ,3 /* 2D */ , 6 /* 3D */ ,10 /* 4D */ }; 


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class MemoryPointersForSVD {

public:

// ****************************************************************************************

	typedef T ValueType;

// ****************************************************************************************

	MemoryPointersForSVD(UInt dim) {

		num_coefficients = numCoefficients[dim-1];

		//std::cerr << "num_coefficients = " << num_coefficients << std::endl;

		mat = new ValueType*[num_coefficients+1];
		mat2 = new ValueType*[num_coefficients+1];
		for (UInt i1 = 0; i1 < num_coefficients+1; i1++) {
			mat[i1] = new ValueType[num_coefficients+1];
			mat2[i1] = new ValueType[num_coefficients+1];
		}
		diag = new ValueType[num_coefficients+1];
		rhs = new ValueType[num_coefficients+1];
		sol = new ValueType[num_coefficients+1];
	}

// ****************************************************************************************

	~MemoryPointersForSVD() {
		for (UInt i1 = 0; i1 < num_coefficients+1; i1++) {
			delete[] mat[i1];
			delete[] mat2[i1];
		}
		delete[] mat;
		delete[] mat2;
		delete[] sol;
		delete[] diag;
		delete[] rhs;
	}

// ****************************************************************************************

	UInt num_coefficients;
	ValueType** mat;
	ValueType **mat2;
	ValueType* sol;
	ValueType *diag;
	ValueType* rhs;

};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


//! A quadric x*A*x + b*x + c.
template <class T>
class GenericQuadric {

public:

	typedef T Point;
	typedef typename Point::ValueType ValueType;
	static const int dim = Point::dim;


	const UInt num_coefficients;
	const UInt num_MatrixEntrys;

// ****************************************************************************************

	//! Constructs a 'zero' quadric.
	GenericQuadric() : num_coefficients(numCoefficients[dim-1]), num_MatrixEntrys(numMatrixEntrys[dim-1]) {
		coefficients = new ValueType[num_coefficients];
		for (UInt i1 = 0; i1 < num_coefficients; i1++) {
			coefficients[i1] = 0;
		}
	}

// ****************************************************************************************

	//! Fits a quadric to the points 'ps' with distances 'dists' and weights 'weights' with supplied memory_pointer.
	GenericQuadric(const std::vector<Point>& ps, const std::vector<ValueType>& dists, const std::vector<ValueType>& weights, MemoryPointersForSVD<ValueType>* memptr) : num_coefficients(numCoefficients[dim-1]), num_MatrixEntrys(numMatrixEntrys[dim-1]) {
		fit(ps, dists, weights, memptr);
	}

// ****************************************************************************************

	//! Fits a quadric to the points 'ps' with distances 'dists' and weights 'weights'.
	GenericQuadric(const std::vector<Point>& ps, const std::vector<ValueType>& dists, const std::vector<ValueType>& weights) : num_coefficients(numCoefficients[dim-1]), num_MatrixEntrys(numMatrixEntrys[dim-1]) {
		MemoryPointersForSVD<ValueType> memptr(dim);
		fit(ps, dists, weights, &memptr);
	}

// ****************************************************************************************

	void fit(const std::vector<Point>& ps, const std::vector<ValueType>& dists, const std::vector<ValueType>& weights, MemoryPointersForSVD<ValueType>* memptr) {
		coefficients = new ValueType[num_coefficients];

		UInt numPts = (UInt)ps.size();

		if (numPts <= num_coefficients) {
			for (UInt i1 = 0; i1 < num_coefficients; i1++) coefficients[i1] = 0;

			std::cerr << "GenericQuadric::fit(): instable, return" << std::endl;

			return;
		}

#if 0
		if (numPts != dists.size()) {
			std::cerr << "GenericQuadric::GenericQuadric(...): size of points array must equal size of distances array! exit!" << std::endl;
			exit(EXIT_FAILURE);
		}
		if (numPts != weights.size()) {
			std::cerr << "GenericQuadric::GenericQuadric(...): size of points array must equal size of weights array! exit!" << std::endl;
			exit(EXIT_FAILURE);
		}
		if (numPts < num_coefficients) {
			std::cerr << "Underestimated set of equations; we have " << num_coefficients << " unknowns but only " << numPts << " points" << std::endl;
		}
#endif


		// Derive for unknowns 'a_vw':
		UInt pos = 1;
		for (UInt v = 0; v < dim; v++) {
			for (UInt w = v; w < dim; w++) {
				// Write the pos'th row:
				memptr->rhs[pos] = fillOneDerivativeRow_a_vw(v,w,ps,dists,weights,memptr->mat[pos]);
				pos++;
			}
		}
		// Derive for unknowns 'b_v'
		for (UInt v = 0; v < dim; v++) {
			// Write the pos'th row:
			memptr->rhs[pos] = fillOneDerivativeRow_b_v(v,ps,dists,weights,memptr->mat[pos]);
			pos++;
		}
		// Derive for unknown 'c'
		memptr->rhs[pos] = fillOneDerivativeRow_c(ps,dists,weights,memptr->mat[pos]);


		// Print LGS:
		//for (UInt x = 1; x <= num_coefficients; x++) {
		//	for (UInt y = 1; y <= num_coefficients; y++) {
		//		std::cerr << std::fixed << std::setprecision(5) << 2.0*memptr->mat[x][y] << ", ";
		//	}
		//	std::cerr << " =  " << 2.0*memptr->rhs[x] << std::endl;
		//}

		// Solve LGS:
		// Use an SVD
		svdcmp<ValueType>(memptr->mat, num_coefficients, num_coefficients, memptr->diag, memptr->mat2);
		// Cancel small values in SVD
		ValueType dmax = 0.0;
		for (UInt i = 1; i < num_coefficients+1; ++i) dmax = std::max(dmax, memptr->diag[i]);
		for (UInt i = 1; i < num_coefficients+1; ++i) if (memptr->diag[i] < (dmax*1e-5)) memptr->diag[i] = 0.0;
		svbksb<ValueType>(memptr->mat, memptr->diag, memptr->mat2, num_coefficients, num_coefficients, memptr->rhs, memptr->sol);

		//std::cerr << "dmax: " << dmax << std::endl;

		// Copy (&Print) Solution:
		for (UInt i1 = 1; i1 <= num_coefficients; i1++) {
			coefficients[i1-1] = memptr->sol[i1];
			//std::cerr << std::fixed << std::setprecision(5) << memptr->sol[i1] << " " << coefficients[i1-1] << "    ";
		}
		//std::cerr << std::endl;
	}

// ****************************************************************************************

	//! Destructor.
	~GenericQuadric() {
		if (coefficients != NULL)
			delete[] coefficients;
	}

// ****************************************************************************************

	//! Evaluates the quadrics second derivative at 'pt'.
	SquareMatrixND<Point> getHessian(const Point& pt) const {

		SquareMatrixND<Point> second_der;

		for (UInt x = 0; x < dim; x++) {
			for (UInt y = 0; y < dim; y++) {
				second_der(x,y) = A(x,y);
			}
		}

		return second_der;
	}

// ****************************************************************************************

	//! Evaluates the quadrics gradient at 'pt'.
	inline Point evalGrad(const Point& pt) const {
		Point grad;

		for (UInt x = 0; x < dim; x++) {
			for (UInt y = 0; y < dim; y++) {
				grad[x] += A(x,y)*pt[y];
			}
			grad[x] *= 2;
			// Vector part.
			grad[x] += coefficients[num_MatrixEntrys+x];
		}

		return grad;
	}

// ****************************************************************************************

	//! Access to matrix A of the quadric.
	inline const ValueType A(UInt i, UInt j) const {
		return coefficients[matrixToArrayAccess(i,j)];
	}

// ****************************************************************************************

	//! Evaluates the quadric 'pt'A'pt' + b'pt' + c.
	inline ValueType eval(const Point& pt) const {

		ValueType ret = 0;
		UInt cnt = 0;

		// Diagonal part:
		for (UInt i1 = 0; i1 < dim; i1++) {
			ret += coefficients[cnt]*pt[i1]*pt[i1] /* diag */ + coefficients[num_MatrixEntrys+i1]*pt[i1]; // Vector
			cnt += dim-i1;
		}
		// Symmetric part
		cnt = 1;
		for (UInt x = 1; x < dim; x++) {
			for (UInt y = x; y < dim; y++) {
				ret += 2*coefficients[cnt]*pt[x-1]*pt[y];
				cnt++;
			}
			cnt++;
		}

		// constant part:
		ret += coefficients[num_coefficients-1];

		return ret;
	}

// ****************************************************************************************

	//! Prints Quadric to stderr.
	inline void print() const {
		for (UInt x = 0; x < dim; x++) {
			for (UInt y = 0; y < dim; y++) {
				std::cerr << std::fixed << std::setprecision(5) << A(x,y) << " ";
			}
			std::cerr << std::fixed << "   " << std::setprecision(5) << coefficients[num_MatrixEntrys+x] << " ";
			std::cerr << std::endl;
		}
		std::cerr << "c: " << coefficients[num_coefficients-1] << std::endl;
	}

// ****************************************************************************************

private:

// ****************************************************************************************

	//! Computes the index-lookup matrix -> array
	inline const UInt matrixToArrayAccess(UInt i, UInt j) const {
		UInt _i = std::min(i,j);
		UInt _j = std::max(i,j);
		return _i*dim + _j - ((_i*(_i+1)) >> 1);
		//UInt v = _i*dim + _j - ((_i*(_i+1)) >> 1);
		//return v;
	}

// ****************************************************************************************

	//! Fills in one row (the derivative w.r.t a_vw) of the matrix, returns right-hand-side
	inline ValueType fillOneDerivativeRow_a_vw(UInt v, UInt w, const std::vector<Point>& ps, const std::vector<ValueType>& dists, const std::vector<ValueType>& weights, ValueType* row) const {

		UInt pos = 1;

		// factors for 'a_op'
		for (UInt o = 0; o < dim; o++) {
			for (UInt p = o; p < dim; p++) {
				// Factor of a_op:
				ValueType factor = 0;
				for (UInt l = 0; l < ps.size(); l++) {
					Point x = ps[l];
					factor += x[o]*x[p]*x[v]*x[w];
				}
				if (v != w) factor *= 2;
				if (o != p) factor *= 2;
				row[pos] = factor;
				pos++;
			}
		}
		// factors for 'b_m'
		for (UInt m = 0; m < dim; m++) {
			// Factor of b_m:
			ValueType factor = 0;
			for (UInt l = 0; l < ps.size(); l++) {
				Point x = ps[l];
				factor += x[m]*x[v]*x[w];
			}
			if (v != w) factor *= 2;
			row[pos] = factor;
			pos++;
		}

		// factor for 'c'
		ValueType factor = 0;
		for (UInt l = 0; l < ps.size(); l++) {
			Point x = ps[l];
			factor += x[v]*x[w];
		}
		if (v != w) factor *= 2;
		row[pos] = factor;

		// Right-handside:
		ValueType rhs = 0;
		for (UInt l = 0; l < ps.size(); l++) {
			Point x = ps[l];
			rhs += dists[l]*x[v]*x[w];
		}
		if (v != w) rhs *= 2;

		return rhs;
	}

// ****************************************************************************************

	//! Fills in one row (the derivative w.r.t b_v) of the matrix, returns right-hand-side
	inline ValueType fillOneDerivativeRow_b_v(UInt v, const std::vector<Point>& ps, const std::vector<ValueType>& dists, const std::vector<ValueType>& weights, ValueType* row) const {

		UInt pos = 1;

		// factors for 'a_op'
		for (UInt o = 0; o < dim; o++) {
			for (UInt p = o; p < dim; p++) {
				// Factor of a_op:
				ValueType factor = 0;
				for (UInt l = 0; l < ps.size(); l++) {
					Point x = ps[l];
					factor += x[o]*x[p]*x[v];
				}
				if (o != p) factor *= 2;
				row[pos] = factor;
				pos++;
			}
		}

		// factors for 'b_m'
		for (UInt m = 0; m < dim; m++) {
			// Factor of b_m:
			ValueType factor = 0;
			for (UInt l = 0; l < ps.size(); l++) {
				Point x = ps[l];
				factor += x[m]*x[v];
			}
			row[pos] = factor;
			pos++;
		}

		// factor for 'c'
		ValueType factor = 0;
		for (UInt l = 0; l < ps.size(); l++) {
			Point x = ps[l];
			factor += x[v];
		}
		row[pos] = factor;

		// Right-handside:
		ValueType rhs = 0;
		for (UInt l = 0; l < ps.size(); l++) {
			Point x = ps[l];
			rhs += dists[l]*x[v];
		}

		return rhs;

	}

// ****************************************************************************************

	//! Fills in one row (the derivative w.r.t b_v) of the matrix, returns right-hand-side
	inline ValueType fillOneDerivativeRow_c(const std::vector<Point>& ps, const std::vector<ValueType>& dists, const std::vector<ValueType>& weights, ValueType* row) const {

		UInt pos = 1;

		// factors for 'a_op'
		for (UInt o = 0; o < dim; o++) {
			for (UInt p = o; p < dim; p++) {
				// Factor of a_op:
				ValueType factor = 0;
				for (UInt l = 0; l < ps.size(); l++) {
					Point x = ps[l];
					factor += x[o]*x[p];
				}
				if (o != p) factor *= 2;
				row[pos] = factor;
				pos++;
			}
		}

		// factors for 'b_m'
		for (UInt m = 0; m < dim; m++) {
			// Factor of b_m:
			ValueType factor = 0;
			for (UInt l = 0; l < ps.size(); l++) {
				Point x = ps[l];
				factor += x[m];
			}
			row[pos] = factor;
			pos++;
		}

		// factor for 'c'
		row[pos] = (ValueType)ps.size();

		// Right-handside:
		ValueType rhs = 0;
		for (UInt l = 0; l < ps.size(); l++) {
			Point x = ps[l];
			rhs += dists[l];
		}

		return rhs;

	}

// ****************************************************************************************

public:

	ValueType* coefficients;

};



// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


//! A low-variate Quadric, uses a dimens�on reduced 'GenericQuadric'.
template <class T>
class QuadricLowVariate {

public:

	typedef T Point;
	typedef typename Point::ValueType ValueType;
	typedef typename Point::lowerDegreePoint LowerDegreePoint;
	static const int dim = Point::dim;

// ****************************************************************************************

	//! Uses the plane defined by 'basis' as quadric.
	QuadricLowVariate(const SquareMatrixND<Point>& basis_, const Point& cog_) : basis(basis_), cog(cog_) {
		gq = new GenericQuadric<LowerDegreePoint>();
	}

// ****************************************************************************************

	//! Fits a quadric to the points 'ps' with weights 'weights' (with supplied memory pointer).
	QuadricLowVariate(const std::vector<Point>& ps, const std::vector<ValueType>& weights, const SquareMatrixND<Point>& basis_, const Point& cog_, MemoryPointersForSVD<ValueType>* memptr) : basis(basis_), cog(cog_) {
		trans_bas = basis.getTransposed();
		fit(ps, weights, memptr);
	}

// ****************************************************************************************

	//! Fits a quadric to the points 'ps' with weights 'weights'.
	QuadricLowVariate(const std::vector<Point>& ps, const std::vector<ValueType>& weights, const SquareMatrixND<Point>& basis_, const Point& cog_) : basis(basis_), cog(cog_) {
		trans_bas = basis.getTransposed();
		MemoryPointersForSVD<ValueType> memptr(dim-1);
		fit(ps, weights, &memptr);
	}

// ****************************************************************************************

	void fit(const std::vector<Point>& ps, const std::vector<ValueType>& weights, MemoryPointersForSVD<ValueType>* memptr) {

		UInt numPts = (UInt)ps.size();

		std::vector<LowerDegreePoint> ps2;
		ps2.reserve(numPts);
		std::vector<ValueType> dists;
		dists.reserve(numPts);

		// Transform points to basis
		Point tmp;
		for (UInt i1 = 0; i1 < numPts; i1++) {
			tmp = basis.vecTrans(ps[i1]-cog); 
			LowerDegreePoint ld_pt;
			for (UInt i2 = 0; i2 < dim-1; i2++) ld_pt[i2] = tmp[i2];
			ps2.push_back(ld_pt);
			dists.push_back(tmp[dim-1]);
		}

		// Print transformed points:
		//std::cerr << "Points transformed to lower dimension: " << std::endl;
		//for (UInt i1 = 0; i1 < numPts; i1++) {
		//	std::cerr << "(" << ps2[i1] << ") dist: " << dists[i1] << std::endl;
		//}

		gq = new GenericQuadric<LowerDegreePoint>(ps2, dists, weights, memptr);

	}

// ****************************************************************************************

	//! Destructor.
	~QuadricLowVariate() {
		delete gq;
	}

// ****************************************************************************************

	inline void print() const {
		std::cerr << "---------------------------------" << std::endl;
		std::cerr << "Lowvariate quadric with:" << std::endl;
		gq->print();
		std::cerr << "---------------------------------" << std::endl;
	}

// ****************************************************************************************

	//! Evaluates the second derivative of the low-dimensional quadric at 'pt'.
	SquareMatrixND<Point> getHessian(const Point& pt) const {

		SquareMatrixND<Point> second_der;

		Point ddd = pt-cog;

		Point tmp = basis.vecTrans(ddd);
		LowerDegreePoint ld_pt;
		for (UInt i1 = 0; i1 < dim-1; i1++) ld_pt[i1] = tmp[i1];
		SquareMatrixND<LowerDegreePoint> ld_s_d = gq->getHessian(ld_pt);

		for (UInt x = 0; x < dim-1; x++) {
			for (UInt y = 0; y < dim-1; y++) {
				second_der(x,y) = ld_s_d(x,y);
			}
		}
		second_der(dim-1, dim-1) = 1;

		return trans_bas*second_der;
	}

// ****************************************************************************************

	//! Evaluates the gradient of the low-dimensional quadric at 'pt'.
	inline Point evalGrad(const Point& pt) const {
		Point grad;

		Point ddd = pt-cog;

		Point tmp = basis.vecTrans(ddd);
		LowerDegreePoint ld_pt;
		for (UInt i1 = 0; i1 < dim-1; i1++) ld_pt[i1] = tmp[i1];

		LowerDegreePoint ld_grad = gq->evalGrad(ld_pt);
		for (UInt i1 = 0; i1 < dim-1; i1++) grad[i1] = -ld_grad[i1];
		grad[dim-1] = 1;
		Point grad2 = trans_bas.vecTrans(grad);

		return grad2;
	}


// ****************************************************************************************

	//! Evaluates the low-dimensional quadric at 'pt'.
	inline ValueType eval(const Point& pt) const {

		Point tmp = basis.vecTrans(pt-cog);

		LowerDegreePoint ld_pt;
		for (UInt i1 = 0; i1 < dim-1; i1++) ld_pt[i1] = tmp[i1];

		return (gq->eval(ld_pt)-tmp[dim-1]);
	}

// ****************************************************************************************

	//! Basis.
	SquareMatrixND<Point> basis;

	//! Center of gravity.
	Point cog;

	//! The low degree (dim-1) quadric.
	GenericQuadric<LowerDegreePoint>* gq;

private:

// ****************************************************************************************


	//! Transposed (= inverted) basis
	SquareMatrixND<Point> trans_bas;

// ****************************************************************************************

};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

};

#endif
