#ifndef _4D_FEATURE_TRACER_H
#define _4D_FEATURE_TRACER_H

#include "point4d.h"
#include "Polyline.h"
#include "operators/polyline4DTimeIntersect.h"

#include <fstream>
#include <string>

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

template <class T>
class FeatureTracer4D {

public:

// ****************************************************************************************
	
	//! Floatin point precision
	typedef T ValueType;

	//! Type of a 4D Point
	typedef typename point4d<ValueType> Point4D;

	//! Type of a 3D Point
	typedef typename point3d<ValueType> Point3D;

// ****************************************************************************************

	//! Constructor.
	FeatureTracer4D() {
		maxV = Point4D(-std::numeric_limits<ValueType>::max());
		minV = Point4D(std::numeric_limits<ValueType>::max());

		polylines = NULL;
		tracking  = new PolyLine<Point3D>;
	};

// ****************************************************************************************

	//! Destructor.
	~FeatureTracer4D() {
		delete tracking;
		if (polylines != NULL) delete[] polylines;	
	};

// ****************************************************************************************

	void loadFromFile(const char* filename) {

		std::ifstream inFile(filename);

		if (!inFile.is_open()) {
			std::cerr << "Error loading Trace file. skip" << std::endl;
			return;
		}

		int numP = 33;
		// First line is number of polylines:
		inFile >> numP;

		std::cerr << "Peek: " << inFile.peek() << std::endl;
		polylines = new PolyLine<Point4D>[numP];

		std::cerr << "numPolylines: " << numP << std::endl; exit(1);

		for (UInt i1 = 0; i1 < numPolyLines; i1++) {

			tracking->insertVertex(Point3D((ValueType)0));

			std::string s;
			inFile >> s;
			polylines[i1].readFromFile(s.c_str());
			Point4D minPL, maxPL;
			polylines[i1].ComputeMinAndMaxExt(minPL, maxPL);
			for (UInt i1 = 0; i1 < Point4D::dim; i1++) {
				if (minV[i1] > minPL[i1]) minV[i1] = minPL[i1];
				if (maxV[i1] < maxPL[i1]) maxV[i1] = maxPL[i1];
			}
		}

		// next line is number of line-segments in 'tracking':
		UInt numLineSegs;
		inFile >> numLineSegs;
		for (UInt i1 = 0; i1 < numLineSegs; i1++) {
			UInt v0, v1;
			inFile >> v0 >> v1;
			tracking->insertLine(v0, v1);
		}
	}

// ****************************************************************************************

	inline void ComputeTrackingAtTime(ValueType time) {
		for (UInt i1 = 0; i1 < getNumPolyLines(); i1++) {
			bool ok;
			Point3D pt = JBSlib::getTimeIntersection(&(polylines[i1]), time, ok);
			tracking->points[i1] = pt;
		}
	}

// ****************************************************************************************

	inline UInt getNumPolyLines() const {
		return numPolyLines;
	}

// ****************************************************************************************

	inline PolyLine<Point4D>* getPolyLines() {
		return polylines;
	}

// ****************************************************************************************

	inline ValueType getMinT() const {
		return minV.w;
	}

// ****************************************************************************************

	inline ValueType getMaxT() const {
		return maxV.w;
	}

// ****************************************************************************************

	inline PolyLine<Point3D>* getTracking() {
		return tracking;
	}

// ****************************************************************************************

private:

// ****************************************************************************************

	//! Number of 'streamlines'
	UInt numPolyLines;

	//! The 'streamlines'
	PolyLine<Point4D>* polylines;

	//! The 'tracking'
	PolyLine<Point3D>* tracking;

	//! min extension of the Polylines 
	Point4D minV;
	//! max extension of the Polylines 
	Point4D maxV;
};

#endif
