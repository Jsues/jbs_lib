#ifndef TAUCS_TOOLS_H
#define TAUCS_TOOLS_H


#include "Image.h"
#include <vector>
#include <iomanip>


Image* taucsMatrixToImage(const std::vector<double>& entries, const std::vector<int>& row_index, const std::vector<int>& col_ptr, int maxDim = 1000) {

	int dim = (int)col_ptr.size()-1;
	std::cerr << "dim " << dim << std::endl;

	Image* img = new Image((dim < maxDim) ? dim : maxDim, (dim < maxDim) ? dim : maxDim);

	// Init with white:
	for (int x = 0; x < dim && x < maxDim; x++)
		for (int y = 0; y < dim && y < maxDim; y++)
			img->setPixel(x,y,255);

	int index = 0;
	for (int i1 = 1; i1 <= dim && i1 < maxDim; i1++) {
		int end_index = col_ptr[i1];
		int x = i1-1;
		for (; index < end_index; index++) {
			int y = row_index[index];
			double e = entries[index];
			if (y < maxDim) img->setPixel(x,y,0);
		}
	}

	return img;
}



void taucsMatrixToStreamPlain(const std::vector<double>& entries, const std::vector<int>& row_index, const std::vector<int>& col_ptr, std::ostream& s) {

	s << std::endl << std::endl;
	for (UInt i1 = 0; i1 < entries.size(); i1++) {
		s << ((entries[i1] > 0) ? " " : "") << std::fixed << std::setprecision(4) << std::setw(4) << entries[i1] << " ";
	}
	s << std::endl;
	for (UInt i1 = 0; i1 < row_index.size(); i1++) {
		s << " " << std::fixed << std::setprecision(4) << std::setw(4) << (float)row_index[i1] << " ";
	}
	s << std::endl;
	for (UInt i1 = 0; i1 < col_ptr.size(); i1++) {
		s << col_ptr[i1] << " ";
	}
	s << std::endl;
}

void taucsMatrixToStream(const std::vector<double>& entries, const std::vector<int>& row_index, const std::vector<int>& col_ptr, std::ostream& s) {

	int dim = (int)col_ptr.size()-1;

	int current = 0;

	s << std::endl << std::endl;
	s << dim << "x" << dim << " matrix" << std::endl;

	for (int y = 0; y < dim; y++) {
		for (int x = 0; x < dim; x++) {
			if ((row_index[current] == x) && (col_ptr[y+1] > current)) {
				s << ((entries[current] > 0) ? " " : "") << std::fixed << std::setprecision(4) << std::setw(4) << entries[current] << "\t";
				current++;
			}
			else s << " 0.0000\t";
		}
		s << std::endl;
	}

}

#endif
