#ifndef PLANE_FITTER_H
#define PLANE_FITTER_H

#include <sstream>
#include "plane.h"
#include "MatrixnD.h"
#include "CircleFitter.h"
#include "PolyLine.h"
//#include "PointCloud.h"
#include "FileIO/PointCloudIO.h"


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template<class T>
T getWeight(T r) {
	r *= 1.5;
	if (r < 0.5)
		return (T)0.75 - r*r;
	else
		return (T)0.5*((T)1.5-r)*((T)1.5-r);
}


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


#define CONVERGENCE_EPS 0.0000001
#define MERGE_DIST 0.1

using namespace JBSlib;

template <class Point>
class PlaneFitter {

public:


// ****************************************************************************************


	typedef typename Point::ValueType ValueType;
	typedef point2d<ValueType> Point2D;
	typedef Plane<Point> PlaneType;
	typedef SquareMatrixND<Point> Mat;
	typedef std::pair<ValueType, int> pair_f_i;


// ****************************************************************************************


	PlaneFitter(const Point* pts, int numPts) : m_pts(pts), m_numPts(numPts) {
	}


// ****************************************************************************************


	void findPrinciplePlane() {
		PlaneType plane = getPCAPlane();
		m_planes.push_back(plane);
		computeScoreForCircleFit(0);
	}


// ****************************************************************************************


	void findPrincipleCircles(int numCircles = 3, int numTries = 20) {

		srand(12);

		ValueType bestResult = std::numeric_limits<ValueType>::max();
		int bestPlaneSet;

		for (int i1 = 0; i1 < numTries; i1++) {
			doKPlanesClustering(numCircles, 100, i1);
			ValueType score = computeScoreForCompleteConfiguration();
			//std::cerr << "Score: " << score << std::endl;
			if (score < bestResult) {
				bestResult = score;
				bestPlaneSet = i1;
			}
		}


		doKPlanesClustering(numCircles, 200, bestPlaneSet);
		int bestCircle = 0;
		ValueType bestScore = 0;
		for (int i1 = 0; i1 < getNumPlanes(); i1++) {
			ValueType score = computeScoreForCircleFit(i1);
			if (score > bestCircle) {
				bestCircle = i1;
				bestScore = score;
			}
		}
		// move best circle to front.
		Circle<Point2D> c = m_circles[0];
		PlaneType p = m_planes[0];
		m_circles[0] = m_circles[bestCircle];
		m_circles[bestCircle] = c;
		m_planes[0] = m_planes[bestCircle];
		m_planes[bestCircle] = p;

	}


// ****************************************************************************************


	inline int getNumPlanes() const {
		return (int)m_planes.size();
	}


// ****************************************************************************************


	void generateOrbits() {

		m_orbit_lines.clear();

		for (int planeID = 0; planeID < getNumPlanes(); planeID++) {
			const PlaneType& p = m_planes[planeID];
			
			Mat basis;
			Point b0, b1;
			p.getBasis(b0, b1);
			basis.setColI(p.getNormal(), 2);
			basis.setColI(b0, 1);
			basis.setColI(b1, 0);
			basis.transpose();

			const Circle<Point2D>& c = m_circles[planeID];

			Mat basis_inv = basis.getTransposed();
			
			PolyLine<Point> pl;
			const int numSegments = 300;
			for (int i1 = 0; i1 < numSegments; i1++) {
				double u = i1;
				u /= numSegments;
				u *= 2*M_PI;
				Point pt;
				pt.x = (ValueType)cos(u);
				pt.y = (ValueType)sin(u);
				pt.z = 0;
				pt *= c.r;
				pt.x += c.center.x;
				pt.y += c.center.y;

				Point pt3D = basis_inv.vecTrans(pt);
				pt3D += p.getNormal()*p.getd();
				pl.insertVertex(pt3D);
			}
			for (int i1 = 0; i1 < numSegments; i1++) {
				int i2 = (i1+1)%numSegments;
				pl.insertLine(i1, i2);
			}
			m_orbit_lines.push_back(pl);
		}

	}


// ****************************************************************************************


	ValueType getDistanceToPrimaryPlane(const Point& pt) {
		return (fabs(m_planes[0].getDistance(pt)));
	}


// ****************************************************************************************


	int getBestPlaneForVertex(const Point& pt) {

		int best_i = -1;
		int k = getNumPlanes();
		ValueType best_d = 10;
		for (int i2 = 0; i2 < k; i2++) {
			ValueType d = fabs(m_planes[i2].getDistance(pt));
			if (d < best_d) {
				best_d = d;
				best_i = i2;
			}
		}

		return best_i;
	}


// ****************************************************************************************

	std::vector<PlaneType> m_planes;
	std::vector< PolyLine<Point> > m_orbit_lines;
	std::vector< Circle<Point2D> > m_circles;

	//PointCloud<Point> pc;

private:

	std::vector< std::vector<pair_f_i> > m_pts_vec;
	const Point* m_pts;
	int m_numPts;


// ****************************************************************************************


	void fitPlane() {
		PlaneType p = getRandomPlane();
		//PlaneType p = getPCAPlane();
		std::cerr << "after get: "; p.print();
		optimizePlane(p);
		m_planes.push_back(p);
	}


// ****************************************************************************************


	PlaneType getRandomContainedPlane() {

		while (true) {
			int v0 = rand()%m_numPts;
			int v1 = rand()%m_numPts;
			int v2 = rand()%m_numPts;
			Point vec0 = m_pts[v0]-m_pts[v1];
			Point vec1 = m_pts[v0]-m_pts[v2];
			Point n = vec0^vec1;
			if (n.squaredLength() > 0) {
				n.normalize();
				ValueType d = m_pts[v0]|n;
				PlaneType p(n, d);
				return p;
			}
		}

	}


// ****************************************************************************************


	ValueType planeToPlaneDist(const PlaneType& p1, const PlaneType& p2) {
		Point n1 = p1.getNormal();
		Point n2 = p2.getNormal();
		ValueType d1 = p1.getd();
		ValueType d2 = p2.getd();
		
		return std::max(fabs(n1.x-n1.x),
						std::max (fabs(n1.y-n1.y),
							std::max (fabs(n1.z-n2.z), fabs(d1-d2))));

	}

// ****************************************************************************************


	void transformDistancesToWeights(std::vector<pair_f_i>& pts) {
		ValueType max_d = 0;
		int numPts = (int)pts.size();
		for (int i1 = 0; i1 < numPts; i1++) {
			if (pts[i1].first > max_d) max_d = pts[i1].first;
		}
		for (int i1 = 0; i1 < numPts; i1++) {
			ValueType w = getWeight(pts[i1].first/max_d);
			pts[i1].first = w;
		}
	}


// ****************************************************************************************


	bool doKPlanesClustering(int k, int numIter = 100, int seed = 0) {

		m_circles.clear();
		srand(seed);

		m_planes.clear();
		for (int i1 = 0; i1 < k; i1++) {
			PlaneType p = getRandomContainedPlane();
			m_planes.push_back(p);
		}

		m_pts_vec.clear();
		for (int i1 = 0; i1 < k; i1++) {
			m_pts_vec.push_back(std::vector<pair_f_i>());
		}


		for (int iter = 0; iter < numIter; iter++) {

			assignPointsToPlanes();

			bool allConverged = true;
			for (int i1 = 0; i1 < k; i1++) {
				PlaneType oldPlane(m_planes[i1]);
				transformDistancesToWeights(m_pts_vec[i1]);
				fitPlaneToVertices(m_pts_vec[i1], m_planes[i1]);
				if (planeToPlaneDist(oldPlane, m_planes[i1]) > CONVERGENCE_EPS)
					allConverged = false;
			}

			while (mergeClosePlanes()) {
				//std::cerr << "Merged in iter: " << iter << std::endl;
			}

			k = getNumPlanes();


			if (iter == numIter-1) {
				//std::cerr << "not conv" << std::endl;
				return false;
			}

			if (allConverged) {
				return true;
			}
		}

		return true;
	}


// ****************************************************************************************


	bool mergeClosePlanes() {

		int numPlanes = getNumPlanes();
		int i1, i2;
		for (i1 = 0; i1 < numPlanes; i1++) {
			for (i2 = i1+1; i2 < numPlanes; i2++) {
				if (planeToPlaneDist(m_planes[i1], m_planes[i2]) < MERGE_DIST) {
					//std::cerr << "Merging:" << std::endl;
					//m_planes[i1].print();
					//m_planes[i2].print();
					// merge
					m_planes[i1] = m_planes[numPlanes-1];
					m_planes.pop_back();
					return true;
				}
			}
		}
		return false;

	}


// ****************************************************************************************


	ValueType computeScoreForCompleteConfiguration() {
		ValueType total_score = 0;
		ValueType max_score = 0;
		int numPlanes = getNumPlanes();
		for (int i1 = 0; i1 < numPlanes; i1++) {
			ValueType score = computeScoreForPlane(i1);
			total_score += score;
			if (score > max_score)
				max_score = score;
		}

		//std::cerr << "Max score: " << max_score << std::endl;
		return total_score;
	}


// ****************************************************************************************


	ValueType computeScoreForPlane(int planeID) {

		const PlaneType& p = m_planes[planeID];
		
		Mat basis;
		basis;
		Point b0, b1;
		p.getBasis(b0, b1);
		basis.setColI(p.getNormal(), 2);
		basis.setColI(b0, 1);
		basis.setColI(b1, 0);
		basis.transpose();
		std::vector<Point2D> pts2d;
		std::vector<Point> pts3d;

		ValueType score = 0;
		for (int i1 = 0; i1 < (int)m_pts_vec[planeID].size(); i1++) {
			const pair_f_i& entry = m_pts_vec[planeID][i1];
			const Point& pt = m_pts[entry.second];
			Point pttrans = basis.vecTrans(pt);
			score += fabs(pttrans.z);
		}

		return score;
	}


// ****************************************************************************************


	ValueType computeScoreForCircleFit(int planeID) {

		const PlaneType& p = m_planes[planeID];
		
		Mat basis;
		basis;
		Point b0, b1;
		p.getBasis(b0, b1);
		basis.setColI(p.getNormal(), 2);
		basis.setColI(b0, 1);
		basis.setColI(b1, 0);
		basis.transpose();
		std::vector<Point2D> pts2d;
		std::vector<Point> pts3d;

		for (int i1 = 0; i1 < (int)m_pts_vec[planeID].size(); i1++) {
			const pair_f_i& entry = m_pts_vec[planeID][i1];
			const Point& pt = m_pts[entry.second];
			Point pttrans = basis.vecTrans(pt);
			pts2d.push_back(Point2D(pttrans.x, pttrans.y));
			pts3d.push_back(pttrans);
		}

		//std::stringstream ss;
		//ss << "plane_" << planeID << ".asc";
		//PointCloudWriter<Point>::writeASCFile(pts3d, ss.str().c_str());

		Circle<Point2D> c = fitCircle(pts2d);
		m_circles.push_back(c);

		ValueType score = 0;
		for (int i1 = 0; i1 < (int)m_pts_vec[planeID].size(); i1++) {
			Point2D dir = pts2d[i1]-c.center;
			dir.normalize();
			Point2D correspondent = c.center + dir * c.r;
			Point c3d(correspondent.x, correspondent.y, 0);
			ValueType dev = c3d.dist(pts3d[i1]);
			score += (c.r-dev)/c.r;
		}

		return score;
	}


// ****************************************************************************************


	void assignPointsToPlanes() {

		int k = getNumPlanes();
		for (int i1 = 0; i1 < k; i1++) {
			m_pts_vec[i1].clear();
		}
		// Optimize:
		for (int i1 = 0; i1 < m_numPts; i1++) {
			const Point& pt = m_pts[i1];
			int best_i = -1;
			ValueType best_d = 10;
			for (int i2 = 0; i2 < k; i2++) {
				ValueType d = fabs(m_planes[i2].getDistance(pt));
				if (d < best_d) {
					best_d = d;
					best_i = i2;
				}
			}
			m_pts_vec[best_i].push_back(pair_f_i(best_d, i1));
		}

	}


// ****************************************************************************************


	void fitPlaneToVertices(const std::vector<pair_f_i>& pts, PlaneType& p) {
		Point cog;

		int numP = (int)pts.size();
		ValueType sum_of_weights = 0;
		for (int i1 = 0; i1 < numP; i1++) {
			if (pts[i1].first == 0) continue;
			const ValueType& w = pts[i1].first;
			cog += m_pts[pts[i1].second]*w;
			sum_of_weights += w;
		}
		Mat cov;
		cog /= sum_of_weights;
		for (int i1 = 0; i1 < numP; i1++) {
			if (pts[i1].first == 0) continue;
			Point v = m_pts[pts[i1].second]-cog;
			const ValueType& w = pts[i1].first;
			v *= w;
			//std::cerr << pts[i1].first << " -> " << v << std::endl;
			Mat c(v);
			cov += c;
		}
		if (cov.isZero()) return;
		//cog.print();
		//cov.print();

		Mat ESystem;
		cov.calcEValuesAndVectorsCORRECT(ESystem);
		Point new_n = ESystem.getColI(2);
		ValueType new_d = cog|new_n;
		p.setNormal(new_n);
		p.setd(new_d);

		//p.print();
		//PlaneType new_p(new_n, new_d);
		//m_planes.push_back(new_p);
	}

	
// ****************************************************************************************


	/*
	MinQueue<ValueType, int> mq;
	void optimizePlane(PlaneType& p) {

		p.print();
		mq.clear();
		int numNeighbors = m_numPts/20;
		mq.setCapacity(numNeighbors);

		for (int i1 = 0; i1 < m_numPts; i1++) {
			const Point& pt = m_pts[i1];
			ValueType dist = fabs(p.getDistance(pt));
			mq.add(i1, dist);
		}
		ValueType maxVal = mq.getMaxValue();

		if (maxVal == 0) {
			std::cerr << "Found cluster" << std::endl;
			return;
		}

		//mq.print();

		std::vector<pair_f_i> pts;
		ValueType median;
		int cnt = 0;
		for (MinQueue<float, int>::SetType::const_iterator it = mq.queue.begin(); it != mq.queue.end(); ++it) {
			ValueType w = getWeight(it->first/maxVal);
			pair_f_i tmp(w, it->second);
			//std::cerr << it->first << " " << it->second << " " << w << std::endl;
			pts.push_back(tmp);
			cnt++;
			if (cnt == numNeighbors/2) median = it->first;
		}
		//mq.print();
		//std::cerr << "Median: " << median << std::endl;
		//exit(1);

		if (pts.size() == 0) {
			std::cerr << "No points found" << std::endl;
			return;
		}
		//m_planes.push_back(p);
		std::cerr << "before init_fit: "; p.print();
		fitPlaneToVertices(pts, p);
		//m_planes.push_back(p);
		std::cerr << "after init_fit: "; p.print();


		ValueType eps = 0.0001f;
		for (int iter = 0; iter < 100; iter++) {
			Point old_n = p.getNormal();
			ValueType old_d = p.getd();
			pts.clear();
			for (int i1 = 0; i1 < m_numPts; i1++) {
				const Point& pt = m_pts[i1];
				ValueType dist = fabs(p.getDistance(pt));
				if (dist < median) {
					ValueType w = getWeight(dist);
					pair_f_i tmp(w, i1);
					pts.push_back(tmp);
				}
			}
			fitPlaneToVertices(pts, p);
			//m_planes.push_back(p);
			Point new_n = p.getNormal();
			ValueType new_d = p.getd();
			if (fabs(old_n.x-new_n.x) < eps && fabs(old_n.y-new_n.y) < eps && fabs(old_n.z-new_n.z) < eps && fabs(old_d-new_d) < eps) break;
		}

		std::cerr << "after opt: "; p.print();
		//PlaneType new_p(new_n, new_d);
		//m_planes.push_back(new_p);

	}
	*/


// ****************************************************************************************


	PlaneType getPCAPlane() {
		std::vector<pair_f_i> pts;

		for (int i1 = 0; i1 < m_numPts; i1++) {
			pair_f_i pfi(1, i1);
			pts.push_back(pfi);
		}
		m_pts_vec.push_back(pts);

		PlaneType p(Point(1.0f, 0.0f, 0.0f), 0);
		fitPlaneToVertices(pts, p);

		return p;
	}


// ****************************************************************************************


	/*
	PlaneType getRandomPlane() {
        ValueType u = ((ValueType)rand())/RAND_MAX;
        ValueType v = ((ValueType)rand())/RAND_MAX;

        //std::cerr << u << " " << v << std::endl;

        ValueType phi = (ValueType)(2*M_PI*u);
        ValueType theta = std::acos(2*v-1.0f);


        ValueType x = sin(theta) * cos(phi);
        ValueType y = sin(theta) * sin(phi);
        ValueType z = cos(theta);

		Point n(x,y,z);

        ValueType d = ((ValueType)rand())/RAND_MAX;
		PlaneType p(n, d);
		return p;
	}
	*/


// ****************************************************************************************


};




#endif
