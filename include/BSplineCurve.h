#ifndef JBS_BSPLINECURVE_H
#define JBS_BSPLINECURVE_H

#include <vector>
#include <fstream>

#include "JBS_General.h"
#include "knotVector.h"



//! This class represents a single B-Spline curve.
template<class T>
class BSplineCurve {

public:

// ****************************************************************************************

	//! Type of a Control-point, for example a 3D Vector of floats.
	typedef T ControlPoint;

	//! Value-Type of Templates components.
	typedef typename T::ValueType ValueType;

// ****************************************************************************************

	//! Constructor.
	/*!
	\param knot Knot-vector.
	\param pts Array of control-points.
	*/
	BSplineCurve(KnotVector<ValueType> knot, std::vector<ControlPoint>& pts, bool isClosed=false) {

		m_n = (UInt)pts.size();
		m_deg = knot.getDegree();
		m_pts = pts;

		m_isClosed = isClosed;

		// We have m_n control-points -> we need m_n+deg+2 knots;
		if (knot.getNumOfPoints() != m_n) {

			if (m_isClosed) {
				if (knot.getNumOfPoints() != m_n+m_deg) {
					std::cerr << "BSplineCurve::BSplineCurve(...)" << std::endl;
					std::cerr << "Error knot is made for " << (int)knot.getNumOfPoints() << " points, pts.size() is " << m_n+m_deg << "!" << std::endl;
					exit(EXIT_FAILURE);
				}
			} else {
				std::cerr << "BSplineCurve::BSplineCurve(...)" << std::endl;
				std::cerr << "Error knot is made for " << (int)knot.getNumOfPoints() << " points, pts.size() is " << m_n << "!" << std::endl;
				exit(EXIT_FAILURE);
			}
		}

		m_knot = knot;

	};

// ****************************************************************************************

	//! Returns the length of the AABB of the Control-Points
	ValueType getDiagonalLength() {
		ControlPoint minv(100000);
		ControlPoint maxv(-100000);

		for (UInt i1 = 0; i1 < m_pts.size(); i1++) {
			for (UInt d = 0; d < ControlPoint::dim; d++) {
				if (maxv[d] < m_pts[i1][d]) maxv[d] = m_pts[i1][d];
				if (minv[d] > m_pts[i1][d]) minv[d] = m_pts[i1][d];
			}
		}

		return (maxv-minv).length();
	}

// ****************************************************************************************

	//! Returns the max-corner of the AABB
	ControlPoint getMax() {
		ControlPoint minv(100000);
		ControlPoint maxv(-100000);

		for (UInt i1 = 0; i1 < m_pts.size(); i1++) {
			for (UInt d = 0; d < ControlPoint::dim; d++) {
				if (maxv[d] < m_pts[i1][d]) maxv[d] = m_pts[i1][d];
				if (minv[d] > m_pts[i1][d]) minv[d] = m_pts[i1][d];
			}
		}

		return maxv;
	}

// ****************************************************************************************

	//! Returns the min-corner of the AABB
	ControlPoint getMin() {
		ControlPoint minv(100000);
		ControlPoint maxv(-100000);

		for (UInt i1 = 0; i1 < m_pts.size(); i1++) {
			for (UInt d = 0; d < ControlPoint::dim; d++) {
				if (maxv[d] < m_pts[i1][d]) maxv[d] = m_pts[i1][d];
				if (minv[d] > m_pts[i1][d]) minv[d] = m_pts[i1][d];
			}
		}

		return minv;
	}

// ****************************************************************************************

	//! inserts a new knot (and therefore a new Control-Point at 'u')
	void insertKnot(ValueType u) {
		// Check if u lies within range:
		if ((u < m_knot[0]) || (u > m_knot[(UInt)m_knot.size()-1])) {
			std::cerr << "Error, trying to insert knot out of range" << std::endl;
			std::cerr << "trying to insert at " << u << " Range is [" << m_knot[0] << ", " << m_knot[(UInt)m_knot.size()-1] << "]" << std::endl;
			std::cerr << "In BSplineCurve::insertKnot(ValueType u)" << std::endl;
			exit(EXIT_FAILURE);
		}

		// get position where the know will be inserted:
		int k = m_knot.getInsertPosition(u);
		int first_affected = k - m_deg;

		m_knot.print();

		std::cerr << "u: " << u << " k: " << k << std::endl;


		std::vector<ControlPoint> newPoints;
		newPoints.resize(m_deg);
		// Compute new positions:
		for (int i1 = first_affected+1; i1 <= k; i1++) {
			ValueType a = (u - m_knot[i1])/(m_knot[i1+m_deg] - m_knot[i1]);
			//std::cerr << "i1: " << i1 << "  a: " << a << std::endl;
			newPoints[i1 - first_affected - 1] = getPoint(i1-1)*(1-a) + getPoint(i1)*a;
			std::cerr << "Points " << (i1-1)%m_n << " " << i1%m_n << std::endl;
		}

		//std::cerr << "New points: " << std::endl;
		m_pts.insert(m_pts.begin()+k%m_n, ControlPoint(0.0f));
		std::cerr << "Insert at " << k << " = " << k%m_n << std::endl;
		for (UInt i1 = 0; i1 < newPoints.size(); i1++) {
			//newPoints[i1].print();
			int incr = 0;
			if (isClosed() && k >= (int)m_n) { std::cerr << "incr" << std::endl; incr = 1; }
			m_pts[(k-m_deg+i1+1+incr)%(m_n+1)] = newPoints[i1];
			std::cerr << (k-m_deg+i1+1+incr)%(m_n+1) << " ";
		}
		std::cerr << std::endl;

		//
		for (UInt i1 = 0; i1 < m_pts.size(); i1++) m_pts[i1].print();

		m_knot.insertKnot(u);
		m_n = m_n + 1;
	}

// ****************************************************************************************

	//! Constructor. Creates an uniform end-point-interpolating Knot-Vector.
	/*!
	\param deg Degree of BSpline Curve
	\param pts Array of control-points.
	*/
	BSplineCurve(UInt deg, std::vector<ControlPoint>& pts) {

		m_n = (UInt)pts.size();
		m_deg = deg;
		m_pts = pts;
		m_isClosed = false;

		m_knot = KnotVector<ValueType>(deg, m_n);

		// We have m_n control-points -> we need m_n+deg+2 knots;
		if (m_knot.getNumOfPoints() != m_n) {
			std::cerr << "BSplineCurve::BSplineCurve(...)" << std::endl;
			std::cerr << "Error knot is made for " << (int)m_knot.getNumOfPoints() << " points, pts.size() is " << m_n << "!" << std::endl;
			exit(EXIT_FAILURE);
		}
	};

// ****************************************************************************************

	BSplineCurve<ControlPoint> getDerivative() const {

		//std::cerr << "Jochen messed up some s**t in BSplineCurve<T>::getDerivative()" << std::endl;
		//std::cerr << "EXIT" << std::endl;
		//exit(EXIT_FAILURE);

		std::vector<ControlPoint> newPts;
		newPts.resize(m_pts.size()-1);
		// Compute new Controlpoints:
		for (UInt i1 = 0; i1 < m_pts.size()-1; i1++) {
			newPts[i1] = m_pts[i1+1] - m_pts[i1];
		}
		BSplineCurve<ControlPoint> derivative(m_deg-1, newPts);

		return derivative;
	}

// ****************************************************************************************

private:

	ValueType binsearch(ValueType lower, ValueType upper, const ControlPoint& last, ControlPoint& next, ValueType dist, ValueType max_deriv) {

		ValueType middle = (lower+upper)/2;
		next = eval(middle);

		ValueType current_dist = next.dist(last);

		std::cerr << "middle: " << middle << " current_dist: " << current_dist << std::endl;

		if (current_dist > dist+max_deriv) {
			return binsearch(middle, upper, last, next, dist, max_deriv);
		} else if (current_dist < dist-max_deriv) {
			return binsearch(lower, middle, last, next, dist, max_deriv);
		} else {
			return middle;
		}
	}

// ****************************************************************************************

public:

	//! Finds the next point on the curve which is between (dist-max_deriv and dist+max_deriv) away from point 'last'
	/*!
		\return the parameter value of the found point
	*/	
	ValueType eval(ValueType u, ValueType u_increment, const ControlPoint& last, ControlPoint& next, ValueType dist, ValueType max_deriv) {

		ValueType current_u = u+u_increment;

		// do a linear search to find initial interval:
		while (current_u < 1) {
			next = eval(current_u);
			if (last.dist(next) > (dist+max_deriv))
				break;
			current_u += u_increment;
		}

		return current_u;
		//std::cerr << "Upper bound: " << current_u << std::endl;

		// Do a binary search between u and current_u;
		return binsearch(u, current_u, current_u, next, dist, max_deriv);
	}

// ****************************************************************************************

	//! Evaluates the Curve at 'u'.
	ControlPoint eval(ValueType u) const {

		ControlPoint pt;

		if (!isClosed()) {
			for (UInt i1 = 0; i1 < m_n; i1++) {
				ValueType factor = m_knot.evalBasis(i1, u);
				pt += getPoint(i1)*factor;
			}
		} else {
			for (UInt i1 = 0; i1 < m_n+m_deg; i1++) {
				ValueType factor = m_knot.evalBasis(i1, u);
				pt += getPoint(i1)*factor;
			}
		}

		return pt;
	}

// ****************************************************************************************

	ControlPoint getPoint(UInt i1) const {
		if (!isClosed()) {
			return m_pts[i1];
		} else {
			return m_pts[(i1+m_n)%m_n];
		}
	}

// ****************************************************************************************

	//! Returns the range of full support.
	point2d<ValueType> getRange() const {
		return m_knot.getRange();
	}

// ****************************************************************************************

	//! Returns the control-point vector.
	inline std::vector<ControlPoint> getCtrlPts() const {
		return m_pts;
	}

// ****************************************************************************************

	//! Returns a pointer to the control-points.
	ControlPoint* getCtrlPtsPtr() {
		return &m_pts[0];
	}

// ****************************************************************************************

	//! Returns a reference to the control-points.
	std::vector<ControlPoint>& getCtrlPtsRef() {
		return m_pts;
	}

// ****************************************************************************************

	//! Returns the number of control-points.
	inline UInt getNumCtrlPts() const {
		return (UInt)m_pts.size();
	}

// ****************************************************************************************

	//! Prints the curve to stderr.
	void print() const {
		std::cerr << "B-Spline curve of degree " << m_deg << std::endl;
		m_knot.print();
		std::cerr << "Control-Points:" << std::endl;
		for (UInt i1 = 0; i1 < m_pts.size(); i1++) m_pts[i1].print();
	}

// ****************************************************************************************

	inline bool isClosed() const {
		return m_isClosed;
	}

// ****************************************************************************************

	//! Returns a const-pointer to the knot vector of the curve.
	inline const KnotVector<ValueType>* getKnot() const {
		return &m_knot;
	}

// ****************************************************************************************

	//! Returns the degree of the curve.
	inline UInt getDegree() const {
		return m_deg;
	}

// ****************************************************************************************

	//! The array of control points.
	std::vector<ControlPoint> m_pts;

private:

	//! The number of control_points == m_pts.size()!.
	UInt m_n;

	//! The degree of the Curve.
	UInt m_deg;

	//! The knot-vector.
	KnotVector<ValueType> m_knot;

	bool m_isClosed;

};
























//*******************************************************************************************************************************
//************************************************ OLD STUFF DEPRECATED!!!!!!!! ************************************************
//*******************************************************************************************************************************

//#ifndef NR_LU_SOLVE_IN_BSPLINE_H
//	#define NR_LU_SOLVE_IN_BSPLINE_H
//	#include "nr_wrap.h"
//#endif


//! This class represents a single B-Spline curve.
template<class T>
class BSplineCurveOLD {

public:

	//! Type of a Control-point, for example a 3D Vector of floats.
	typedef T ControlPoint;

	//! Type of a Point, for example a 3D Vector of floats.
	typedef T Point;

	//! Type of a Vector, for example a 3D Vector of floats.
	typedef T Vector;

	//! Value-Type of Templates components.
	typedef typename T::ValueType ValueType;

	//! Parametrization Type for scattered-data approximation.
	enum ParametrizationType {
		CHORD_LENGTH = 1
	};

// ****************************************************************************************

	//! Empty-Constructor.
	BSplineCurveOLD() {
		m_n = 0;
	  	m_deg = 0;
	}

// ****************************************************************************************

	//! Constructor.
	/*!
	\param deg Degree.
	\param knot Knot-vector.
	\param pts Array of control-points.
	*/
	BSplineCurveOLD(UInt deg, std::vector<float> knot, std::vector<ControlPoint>& pts) {

		m_n = (UInt)pts.size()-1;
		m_deg = deg;
		m_pts = pts;

		// We have m_n control-points -> we need m_n+deg+2 knots;
		if (knot.size() != m_n+m_deg+2) {
			std::cerr << "Error knot.size() is " << (int)knot.size() << ", should have been " << m_n+m_deg+2 << "!" << std::endl;
			exit(EXIT_FAILURE);
		}

		m_knot = knot;

	};

// ****************************************************************************************

	ValueType getRange() {
		return 1;
	}

// ****************************************************************************************

	//! Constructor
	/*!
	\param deg Degree.
	\param pts Array of control-points.
	*/
	BSplineCurveOLD(UInt deg, std::vector<ControlPoint>& pts) {

		// We have m_n control-points -> we need m_n+deg+1 knots;
		// Auto-generate (Endpoint-interpolating) knot-vector:
		for (UInt i1 = 0; i1 <= deg; i1++)
			m_knot.push_back(0.0);
		UInt tmp = (UInt)pts.size()-1-deg;
		for (UInt i1 = 1; i1 <= tmp; i1++) {
			float t = (float)i1/(float)(tmp+1);
			m_knot.push_back(t);
		}

		for (UInt i1 = 0; i1 <= deg; i1++)
			m_knot.push_back(1.0);

		m_n = (UInt)pts.size();
		m_deg = deg;
		m_pts = pts;
	};

// ****************************************************************************************

	//! Checks, if number of controlpoints, degree and knot.size() match
	bool checkConsistency() {
		std::cerr << "Consitency Check:" << std::endl;
		// Compare
		if (m_pts.size() != m_n+1)
			std::cerr << "m_pts.size()=" << (int)m_pts.size() << " does not match (m_n+1)=" << m_n+1 << std::endl;
		else
			std::cerr << "m_pts.size()=" << (int)m_pts.size() << " does match (m_n+1)=" << m_n+1 << std::endl;

		if (m_knot.size() != m_n+m_deg+2)
			std::cerr << "m_knot.size()=" << (int)m_knot.size() << " does not match m_n+m_deg+2)=" << m_n+m_deg+2 << std::endl;
		else
			std::cerr << "m_knot.size()=" << (int)m_knot.size() << " does match m_n+m_deg+2)=" << m_n+m_deg+2 << std::endl;

		return true;
	}

// ****************************************************************************************

	//! Approximates the points in 'pts' with a degree 'deg' B-Spline-Curve with 'deg+1' Control-Points
	void approximate(std::vector<T> pts, UInt deg, UInt numControlPoints, ParametrizationType pt) {

		// Parameterize the polyline pts
		std::vector<ValueType> params;
		if (pt == CHORD_LENGTH) {
			ValueType length = 0;
			for (UInt i1 = 0; i1 < pts.size()-1; i1++) {
				T vec = pts[i1]-pts[i1+1];
				length += vec.length();
			}

			params.push_back(0);
			for (UInt i1 = 0; i1 < pts.size()-1; i1++) {
				T vec = pts[i1]-pts[i1+1];
				params.push_back(params[i1]+vec.length()/length);
			}
		}

		// Create end-point-interpolating uniform knot vector:
		for (UInt i1 = 0; i1 <= deg; i1++)
			m_knot.push_back(0);
		for (UInt i1 = 0; i1 < numControlPoints-deg-1; i1++) {
			m_knot.push_back(float(i1+1)/float(numControlPoints-deg));
		}
		for (UInt i1 = 0; i1 <= deg; i1++)
			m_knot.push_back(1);

		//for (UInt i1 = 0; i1 < m_knot.size(); i1++)
		//	std::cerr << m_knot[i1] << " ";

		// We want to actually calculate a Bezier-Curve, therefore the number of ctrlpts = deg+1;
		// Endpoint interpolating ->
		m_pts.push_back(pts[0]);

		// Degree of the Bezier Curve
		int p = deg;
	
		// Number of Control-Points = h+1
		int h = numControlPoints-1;

		// Number of Sample-Points = n+1;
		int n = (int)pts.size()-1;

		// Intermediate array
		T* Qk = new T[n];
		for (int k = 1; k <= n-1; k++) {
			Qk[k] = pts[k] - pts[0]*Basis(0, p, params[k]) - pts[n]*Basis(h, p, params[k]);
		}

		T* Q = new T[h-1];
		for (int i = 1; i <= h-1; i++) {
			int row = i-1;
			Q[row] = T(); // Init with empty vector
			for (int k = 1; k <= n-1; k++) {
				Q[row] += Qk[k]*Basis(i, p, params[k]);
			}
		}

		ValueType** N = new ValueType*[n-1];
		for (int i = 1; i <= n-1; i++)
			N[i-1] = new ValueType[h-1];

		for (int i = 1; i <= n-1; i++) {
			int d_i = i-1;
			for (int j = 1; j <= h-1; j++) {
				int d_j = j-1;
				N[d_i][d_j] = Basis(j, p, params[i]);
			}
		}

		ValueType** N_t = new ValueType*[h-1];
		for (int i = 1; i <= h-1; i++)
			N_t[i-1] = new ValueType[n-1];
		for (int i = 1; i <= n-1; i++) {
			int d_i = i-1;
			for (int j = 1; j <=h-1; j++) {
				int d_j = j-1;
				N_t[d_j][d_i] = Basis(j, p, params[i]);
			}
		}

		ValueType** M = new ValueType*[h];
		for (int i = 0; i <= h-1; i++)
			M[i] = new ValueType[h];

		// Zero-Out M:
		for (int i = 0; i <= h-1; i++)
			for (int j = 0; j <= h-1; j++)
				M[i][j] = ValueType(0);


		// Calculate M = Transpose(N)*N:
		for (int i = 0; i < h-1; i++) {
			for (int j = 0; j < h-1; j++) {
				// Calculate Cell (i,j)
				M[i+1][j+1] =0;
				for (int k = 0; k < n-1; k++) {
					M[i+1][j+1] += N_t[i][k]*N[k][j];
				}
			}
		}

		// Solve using ludcmp
		int *indx = new int[h-1];
		float d;
		ludcmp(M, h-1, indx, &d);
		// Solve x-Component
		float* b1 = new float[h];
		for (int i1 = 0; i1 < h-1; i1++)
			b1[i1+1] = Q[i1][0];
		lubksb(M, h-1, indx, b1);
		// Solve y-Component
		float* b2 = new float[h];
		for (int i1 = 0; i1 < h-1; i1++)
			b2[i1+1] = Q[i1][1];
		lubksb(M, h-1, indx, b2);

		for (int i = 1; i <= h-1; i++) {
			m_pts.push_back(T(b1[i], b2[i]));
		}
		m_pts.push_back(pts[n]);

		// Set number of ctrl-pts
		m_n = (int)m_pts.size()-1;
		// Set degree
		m_deg = deg;

		//// Redo Knot-Vector:
		//m_knot.clear();
		//for (UInt i1 = 0; i1 <= deg; i1++)
		//	m_knot.push_back(0);
		//for (UInt i1 = 0; i1 < num_pts-deg-2; i1++) {
		//	m_knot.push_back(float(i1+1)/float(num_pts-deg-1));
		//}
		//for (UInt i1 = 0; i1 <= deg; i1++)
		//	m_knot.push_back(1);

		// clean up
		delete[] Q;
		delete[] Qk;
		for (int i = 0; i < n-1; i++)
			delete[] N[i];
		delete[] N;
		for (int i = 0; i < h-1; i++)
			delete[] N_t[i];
		delete[] N_t;
		for (int i = 0; i < h; i++)
			delete[] M[i];
		delete[] M;
//		delete[] indx;
		delete[] b1;
		delete[] b2;
	}
	
// ****************************************************************************************

	//! Prints out some relevant debug-data.
	void print() {
		std::cerr << "----------" << std::endl;
		std::cerr << "Degree = " << m_deg << std::endl;
		std::cerr << "Number of Control-Points = " << m_n+1 << std::endl;
		std::cerr << "Size of Control-Points Array = " << (int)m_pts.size() << std::endl;
		std::cerr << "Knot-Vector:";
		for (UInt i1 = 0; i1 < m_knot.size(); i1++)
			std::cerr << m_knot[i1] << " ";
		std::cerr << std::endl;
		std::cerr << "Control-Points:" << std::endl;
		for (UInt i1 = 0; i1 < m_pts.size(); i1++)
			m_pts[i1].print();
		std::cerr << "----------" << std::endl;
	}

// ****************************************************************************************

	//! Calculates the 'i'th Basis-Polynomial of degree 'degree' at 't'.
	ValueType Basis(UInt i, UInt degree, ValueType t) {

		if (t == 1 && i == m_n) {
			return 1;
		}
	    
		if  (degree == 0) {
			if ( ( t >= m_knot[i]) && ( t < m_knot[i+1]) ) {
				return 1;
			} else {
				return 0;
			}
		} else {
			
			ValueType const1, const2;
	      
			if ( (m_knot[i+degree+1] - m_knot[i]) == 0 ) {
				return 0;
			}
			if ( (m_knot[i+degree+1] - m_knot[i+1]) == 0 ) {
				const1 = ( t - m_knot[i] ) / ( m_knot[i+degree] - m_knot[i] );
				return (const1 * Basis(i, degree-1, t));
			}
			if ( (m_knot[i+degree] - m_knot[i]) == 0 ) {
				const2 = ( m_knot[i+degree+1] - t ) / ( m_knot[i+degree+1] - m_knot[i+1]);
				return (const2 * Basis(i+1, degree-1, t));
			}

			/* else */  
			const1 = ( t - m_knot[i] ) / ( m_knot[i+degree] - m_knot[i] );
			const2 = ( m_knot[i+degree+1] - t ) / ( m_knot[i+degree+1] - m_knot[i+1]);
			return (const1 * Basis(i, degree-1, t) + const2 * Basis(i+1, degree-1, t));
		}
	};


// ****************************************************************************************

	//! Evaluates the Curve at 'u'.
	Point eval(ValueType u) {

		//std::cerr << "Evaluate at u=" << u << std::endl;
		Point ret;

		for (UInt i1 = 0; i1 <= m_n; i1++) {
			float bas = Basis(i1, m_deg, u);
//			std::cerr << bas << " ";
			ret += m_pts[i1] * bas;
		}

		if (u > 1.0) {
			return m_pts[m_n-1];
		}

		return ret;

	};

// ****************************************************************************************

	//! Finds the (best) parametrization for the given point 'v' using Newtons method
	ValueType getParametrizationNewton(T v) {


		BSplineCurve<T>* d_curve = get_derivative();

		// Find an starting value x_0;
		ValueType x_0  = 0.5;
		ValueType best = ValueType(10e10);
		for (UInt i1 = 0; i1 <=10; i1++) {
			ValueType u = ValueType(i1)/ValueType(10);
			T pt = eval(u);
			ValueType dist = (pt-v).squaredLength();
			if (dist < best) {
				best = dist;
				x_0 = u;
			}
		}

		// We have our starting value, start the newton iteration
		ValueType x = x_0;
		for (int i1 = 0; i1 < 10; i1++) {
			T func = eval(x)-v;
			T deriv = d_curve->eval(x);
			x = x - (func|deriv / deriv.squaredLength());
		}


		std::cerr << "closest point: "; eval(x).print();

		return x;

	}

// ****************************************************************************************

	//! Returns the derivative of the curve
	BSplineCurve<T>* get_derivative() {

		// Create new ctrl_pts:
		std::vector<T> new_pts;

		cerr << " " << m_n << " " << (int)m_pts.size() << endl;

		for (UInt i1 = 0; i1 < m_n; i1++) {
			ValueType factor = ValueType(m_deg) / (m_knot[i1+m_deg+1]-m_knot[i1+1]);
			T diff = m_pts[i1+1]-m_pts[i1];
			//std::cerr << "Degree = " << m_deg << "  ";
			//diff.print();
			new_pts.push_back(diff*factor);
		}

		// Create new knot vector:
		std::vector<float> new_knot;
		for (UInt i1 = 1; i1 < m_knot.size()-1; i1++) {
			new_knot.push_back(m_knot[i1]);
		}

		BSplineCurve<T>* new_curve = new BSplineCurve<T>(m_deg-1, new_knot, new_pts);
		return new_curve;
	}

// ****************************************************************************************

	//! Prints the control-points to stderr
	void PrintCtrlPts() {
		std::cerr << "Controlpoints" << std::endl;
		for (UInt i1 = 0; i1 <= m_n; i1++)
			m_pts[i1].print();
	}

// ****************************************************************************************

	//! Prints the knot-vector to stderr
	void PrintKnotVector() {
		std::cerr << "Knotvector" << std::endl;
		for (UInt i1 = 0; i1 <= m_n+m_deg+1; i1++)
			std::cerr << m_knot[i1] << std::endl;
	}

// ****************************************************************************************

	//! Prints the degree to stderr
	void PrintDegree() {
		std::cerr << "Degree = " << m_deg << std::endl;
	}

// ****************************************************************************************

	//! Prints the number of control-points to stderr
	void NumCtrlPts() {
		std::cerr << "NumCtrlPts = " << m_n << " (+1)" << std::endl;
	}

// ****************************************************************************************

	//! Prints Debug infos
	void PrintDebug() {
		std::cerr << "------------DEBUG------------" << std::endl;
		PrintCtrlPts();
		PrintKnotVector();
		PrintDegree();
		NumCtrlPts();
		std::cerr << "------------DEBUG------------" << std::endl;
	}

// ****************************************************************************************

	//! Returns the knot vector of the curve
	std::vector<float> getKnot() {
		return m_knot;
	}

// ****************************************************************************************

	inline std::vector<ControlPoint> getCtrlPts() const {
		return m_pts;
	}

// ****************************************************************************************

	inline ControlPoint* getCtrlPtsPtr() const {
		return &m_pts[0];
	}

// ****************************************************************************************

	inline UInt getNumCtrlPts() const {
		return (UInt)m_pts.size();
	}

// ****************************************************************************************

private:

	//! (Number of control-points)-1.
	UInt m_n;
	  
	//! Degree.
	UInt m_deg;

	//! Knot-Vector.
	std::vector<float> m_knot;

	//! Array of control-points.
	std::vector<ControlPoint> m_pts;

};


#endif //JBS_BSPLINECURVE_H

