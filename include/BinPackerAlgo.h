#pragma once

#include "SimpleMesh.h"
#include "Image.h"


class BinPackerAlgo
{
public:
	
	class Shape {

	public:

		Shape() {
			skyline = NULL;
			baseline = NULL;
		}

		Shape(const Shape& other) : img(other.img), width(other.width), height(other.height) {
			id = other.id;
			skyline = new int[width];
			baseline = new int[width];
			for (int i1 = 0; i1 < width; i1++) {
				skyline[i1] = other.skyline[i1];
				baseline[i1] = other.baseline[i1];
			}
		}

		~Shape() {
			if (skyline != NULL) delete[] skyline;
			skyline = NULL;
			if (baseline != NULL) delete[] baseline;
			baseline = NULL;
		}

		void operator= (const Shape& other) {
			id = other.id;
			width = other.width;
			height = other.height;
			img = other.img;
			skyline = new int[width];
			baseline = new int[width];
			for (int i1 = 0; i1 < width; i1++) {
				skyline[i1] = other.skyline[i1];
				baseline[i1] = other.baseline[i1];
			}
		}

		bool operator<(const Shape& other) const {
			return (width < other.width);
		}

		int id;
		Image img;
		int* skyline;
		int* baseline;
		int width;
		int height;
	};

	BinPackerAlgo(const int textureSize, unsigned int gapsize=1);

	~BinPackerAlgo(void);

	void addShape(SimpleMesh* sm);

	void doPacking();

	void doPackingAdvanced();

	vec2f getTransformation(int i) const {
		return m_translations[i];
	}

	vec2f getMaxPos() const {
		int maxY = 0;
		for (int i1 = 0; i1 < m_textureSize; i1++) {
			maxY = std::max(maxY, m_skyline[i1]);
		}
		vec2f maxPos((float)m_textureSize-1, (float)maxY);
		maxPos /= (float)m_textureSize;

		return maxPos;
	}

private:

	Shape computeShape(SimpleMesh* sm, vec2d maxP);

	vec2i placeShape(int id, bool computeCostsOnly = false, double* costs = NULL);

	std::vector<Shape> m_shapes;

	std::vector<vec2f> m_translations;

	int* m_skyline;

	const int m_textureSize;

	int m_skylineMax;

#ifdef SAVE_PACKING_TO_IMG
	Image packing;
#endif

	unsigned int m_gapsize;
};
