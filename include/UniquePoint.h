#ifndef JBS_UNIQUE_POINT_H
#define JBS_UNIQUE_POINT_H


#include "JBS_General.h"
#include "point3d.h"

// For kD-tree
#include "ANN/ANN.h"


//! TODO: This class is SLOOOOOOOOOOW. Don't use it! Checks if a point is already contained in a PointSet and returns the index of the point.
template<class T>
class UniquePoint {

public:
	//! Constructs a new Object.
	/*!
		\param numPoints The expected number of unique points.
	*/
	UniquePoint(int numPoints = 30000) {
		usedPoints = 0;
		m_numPoints = numPoints;
		dataPts = annAllocPts(numPoints, 3);
		queryPt = annAllocPt(3);
		kdTree = new ANNkd_tree(dataPts, usedPoints, 3);
	}

	~UniquePoint() {
		delete kdTree;
		annDeallocPts(dataPts);
	}

	//! Insert a point.
	/*!
		\return true if the point is already in the set.
		\param index will be set to the index of the point.
	*/
	bool insertPoint(T p, int& index) {

		if (usedPoints < 1) {
			addPoint(p);
			index = (usedPoints-1);
			return false;
		}

		queryPt[0] = p[0]; queryPt[1] = p[1]; queryPt[2] = p[2];

		ANNidxArray nnIdx = new ANNidx[1];						// allocate near neigh indices
		ANNdistArray dists = new ANNdist[1];						// allocate near neighbor dists

		kdTree->annkSearch(queryPt, 1, nnIdx, dists);

		if (dists[0] < 0.000001) {
			//std::cerr << "Duplicate point" << std::endl;
			index = nnIdx[0];

			delete[] nnIdx;
			delete[] dists;

			return true;
		} else { // Point is not yet in the set!
			delete[] nnIdx;
			delete[] dists;

			addPoint(p);

			index = (usedPoints-1);
			return false;
		}
	}

private:

	void addPoint(T p) {

		if (usedPoints >= m_numPoints) {
			std::cerr << "Reallocating array...";
			ANNpointArray dataPts2 = annAllocPts(m_numPoints*2, 3);
			for (int i1 = 0; i1 < m_numPoints; i1++) {
				dataPts2[i1] = dataPts[i1];
			}
			annDeallocPts(dataPts);
			dataPts = dataPts2;
			m_numPoints *= 2;
			std::cerr << "size is now " << m_numPoints << std::endl;
		}

		dataPts[usedPoints][0] = p[0]; dataPts[usedPoints][1] = p[1]; dataPts[usedPoints][2] = p[2];
		usedPoints++;

		delete kdTree;

		kdTree = new ANNkd_tree(dataPts, usedPoints, 3);
	}

	//! ANN kD-tree.
	ANNkd_tree* kdTree;

	//! Stores the vectors of the kD-tree.
	ANNpointArray dataPts;

	//! Point used to query Neighbors.
	ANNpoint queryPt;

	//! Allocated size of the array.
	int m_numPoints;

	//! Number of points currently stored in the kd-Tree.
	int usedPoints;

};


#endif