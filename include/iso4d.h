#ifndef COMPUTE_ISO_4D_H
#define COMPUTE_ISO_4D_H


#include <iostream>
#include <fstream>
#include <vector>
#include "JBS_General.h"


/******************************************************************************************
All monotonous paths from 0000 to 1111:

0000 X 1111 with X =

0001 0011 0111 = 311
0001 0101 0111 = 343
0010 0011 0111 = 567
0010 0110 0111 = 615
0100 0101 0111 = 1111
0100 0110 0111 = 1127

0001 0011 1011 = 315
0001 1010 1011 = 427
0010 0011 1011 = 571
0010 1010 1011 = 683
1000 1001 1011 = 2203
1000 1010 1011 = 2219

0001 0101 1101 = 349
0001 1001 1101 = 413
0100 1100 1101 = 1229
0100 1001 1101 = 1181
1000 1001 1101 = 2205
1000 1100 1101 = 2253

0010 1010 1110 = 686
0010 0110 1110 = 622
0100 0110 1110 = 1134
0100 1100 1110 = 1230
1000 1100 1110 = 2254
1000 1010 1110 = 2222
******************************************************************************************/

namespace marching4d {

	const double EPSILON = 1e-50;

	const UChar numPathes = 24;
	const short pathes[24] = {	311, 343, 567, 615, 1111, 1127,
								315, 427, 571, 683, 2203, 2219,
								349, 413, 1229, 1181, 2205, 2253,
								686, 622, 1134, 1230, 2254, 2222	};

	const short edgeTableTetrahedron4D[32] = {
			0,									// all smaller 'iso'
			1 | 2 | 4 | 8,						// v0
			1 | 32 | 64 | 16,					// v1
			2 |4 | 8 | 16 | 32 | 64,			// v0 =  1, v1 =  2
			2 | 128 | 256 | 16,					// v2
			1 | 4 | 8 | 16 | 128 | 256,			// v0 =  1, v2 =  4
			512 | 64 | 8 | 128 | 16 | 2,		// v1 =  2, v2 =  4
			4 | 128 | 32 | 8 | 256 | 64,		// v3 =  8, v4 = 16
			4 | 32 | 512 | 128,					// v3
			8 | 1 | 2 | 512 | 32 | 128,			// v0 =  1, v3 =  8
			64 | 1 | 16 | 512 | 4 | 128,		// v1 =  2, v3 =  8
			2 | 16 | 128 | 8 | 64 | 512,		// v2 =  4, v4 = 16
			256 | 16 | 1 | 512 | 32 | 4,		// v2 =  4, v3 =  8
			1 | 16 | 32 | 8 | 256 | 512,		// v1 =  2, v4 = 16
			1 | 2 | 4 | 64 | 256 | 512,			// v0 =  1, v4 = 16
			8 | 64 | 256 | 512,					// v4
			8 | 64 | 256 | 512,					// v4
			1 | 2 | 4 | 64 | 256 | 512,			// v0 =  1, v4 = 16
			1 | 16 | 32 | 8 | 256 | 512,		// v1 =  2, v4 = 16
			256 | 16 | 1 | 512 | 32 | 4,		// v2 =  4, v3 =  8
			2 | 16 | 128 | 8 | 64 | 512,		// v2 =  4, v4 = 16
			64 | 1 | 16 | 512 | 4 | 128,		// v1 =  2, v3 =  8
			8 | 1 | 2 | 512 | 32 | 128,			// v0 =  1, v3 =  8
			4 | 32 | 512 | 128,					// v3
			4 | 128 | 32 | 8 | 256 | 64,		// v3 =  8, v4 = 16
			512 | 64 | 8 | 128 | 16 | 2,		// v1 =  2, v2 =  4
			1 | 4 | 8 | 16 | 128 | 256,			// v0 =  1, v2 =  4
			2 | 128 | 256 | 16,					// v2
			2 |4 | 8 | 16 | 32 | 64,			// v0 =  1, v1 =  2
			1 | 32 | 64 | 16,					// v1
			1 | 2 | 4 | 8,						// v0
			0									// all larger 'iso'
	};


	const char edgeTableTetrahedron[16] = {
		0,
		1 | 2 | 4,
		1 | 8 | 16,
		2 | 4 | 8 | 16,
		2 | 8 | 32,
		1 | 4 | 8 | 32,
		1 | 2 | 16 |32,
		4 | 16 | 32,
		4 | 16 | 32,
		1 | 2 | 16 |32,
		1 | 4 | 8 | 32,
		2 | 8 | 32,
		2 | 4 | 8 | 16,
		1 | 8 | 16,
		1 | 2 | 4,
		0
	};

	template <class T, class U>
	inline U linearInterpolate(T iso, const U& cellpos0, const U& cellpos1, T celliso0, T celliso1) {
		
		if (std::abs(iso-celliso0) < (T)EPSILON) {
			//std::cerr << "iso = cellilo0" << std::endl;
			//std::cerr << "val 0: " << celliso0 << " val 1: " << celliso1 << " iso: " << iso << std::endl;
			return(cellpos0);
		}
		if (std::abs(iso-celliso1) < (T)EPSILON) {
			//std::cerr << "iso = cellilo0" << std::endl;
			return(cellpos1);
		}
		if (std::abs(celliso0-celliso1) < (T)EPSILON) {
			//std::cerr << "celliso1 = cellilo0" << std::endl;
			return(cellpos0);
		}

		U temp_v0, temp_v1;
		T temp_val0, temp_val1;

		if (celliso0 < iso) {
			temp_v0 = cellpos0;
			temp_v1 = cellpos1;
			temp_val0 = celliso0;
			temp_val1 = celliso1;
		} else {
			temp_v0 = cellpos1;
			temp_v1 = cellpos0;
			temp_val0 = celliso1;
			temp_val1 = celliso0;
		}

		T mu = (iso - temp_val0) / (temp_val1 - temp_val0);

		//if (temp_v1.w != temp_v0.w) {
		//	std::cerr << "---" << std::endl;
		//	temp_v0.print();
		//	temp_v1.print();
		//	std::cerr << "val 0: " << temp_val0 << " val 1: " << temp_val1 << " iso: " << iso << std::endl;
		//	(temp_v0 + (temp_v1-temp_v0)*mu).print();
		//}


		//std::cerr << (temp_v0 + (temp_v1-temp_v0)*mu) << " / " << iso << std::endl;

		return temp_v0 + (temp_v1-temp_v0)*mu;
	}
};


// ****************************************************************************************


void stream_bin(short n);


// ****************************************************************************************




// ****************************************************************************************




#endif

