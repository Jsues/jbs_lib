#ifndef SVG_CANVAS_H
#define SVG_CANVAS_H

#include <iostream>
#include <vector>
#include <sstream>
#include <ctime>

#include "Polyline.h"
#include "PointCloud.h"
#include "RGB_Image.h"

namespace SVG {

	class SVG_Resizer {
	public:

		SVG_Resizer(vec2f minV, vec2f maxV, float wcm) {
			m_minV = minV;
			factor = 1000 / (maxV.x-minV.x);
			g_id = 0;
		}

		vec2f operator()(vec2f in) {
			in -= m_minV;
			in *= factor;
			return in;
		}
		vec2f operator()(vec3d in_) {
			vec2f in((float)in_.x, (float)in_.y);
			return operator()(in);
		}

		vec2f operator()(vec2d in_) {
			vec2f in((float)in_.x, (float)in_.y);
			return operator()(in);
		}

		UInt getGroupID() {
			g_id++;
			return g_id;
		}

		vec2f m_minV;
		float factor;

	private:
		UInt g_id;

	};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

	class SVG_BaseItem {
	public:
		SVG_BaseItem() {}

		//! writes the item to the stream 'os'
		virtual void draw(std::ostream &os, SVG_Resizer* resizer) = 0;
	};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

	template <class T>
	class SVG_PointCloud : public SVG_BaseItem{
	public:
		SVG_PointCloud(PointCloud<T>* pc) : m_pc(pc) {
			color1 = vec3f(0.5f,0.5f,0.5f);
			color2 = vec3f(0,0,0);
			radius = 3;
			lw = 1;
		}

		void setAttributes(float radius_ = 3, vec3f color_outline = vec3f(0.5f,0.5f,0.5f), vec3f color_fill = vec3f(0,0,0), float linewidth=1) {
			color1 = color_fill;
			color2 = color_outline;
			lw = linewidth;
			radius = radius_;
		}

		//! writes the item to the stream 'os'
		virtual void draw(std::ostream &os, SVG_Resizer* resizer) {
			UInt groupID = resizer->getGroupID();

			vec3f col1 = color1*255;
			vec3f col2 = color2*255;

			os << "<g id=\"" << groupID << "\" fill=\"rgb(" << (int)col1.x << "," << (int)col1.y << "," << (int)col1.z << ")\" stroke=\"rgb(" << (int)col2.x << "," << (int)col2.y << "," << (int)col2.z << ")\" stroke-width=\"" << lw << "\" >" << std::endl;
			for (UInt i1 = 0; i1 < (UInt)m_pc->getNumPts(); i1++) {
				T pt = m_pc->getPoints()[i1];
				vec2f pt2f = (*resizer)(pt);
				os << "<circle cx=\"" << pt2f.x << "\" cy=\"" << pt2f.y << "\" r=\"" << radius << "\" />" << std::endl;
			}
			os << "</g>" << std::endl;
		}

	private:
		PointCloud<T>* m_pc;
		std::vector< std::vector<UInt> > cycles;
		vec3f color1;
		vec3f color2;
		float lw;
		float radius;
	};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

	class SVG_Image : public SVG_BaseItem{
	public:
		SVG_Image(RGBImage* img) : m_img(img) {
		}

		//! writes the item to the stream 'os'
		virtual void draw(std::ostream &os, SVG_Resizer* resizer) {

			char* filename = "embedded_image_";
			char* ext = ".ppm";
			char* intermed = new char[10];

			srand((UInt)time(NULL));
			for (int i1 = 0; i1 < 9; i1++) {
				int rnd = rand() % 10;
				rnd += 48;
				intermed[i1] = (char)rnd;
			}
			intermed[9] = '\0';

			vec2f sp = (*resizer)(m_min);
			vec2f ep = (*resizer)(m_max);

			std::stringstream ss;
			ss << "embedded_image_" << std::string(intermed) << ".ppm";

			os << "<image x=\"" << sp.x << "\" y=\"" << sp.y << "\" width=\"" << ep.x << "px\" height=\"" << ep.y << "px\" xlink:href=\"" << ss.str().c_str() << "\"/>" << std::endl;
			m_img->writePPM(ss.str().c_str());
		}

		void setAttributes(vec2f min, vec2f max) {
			m_min = min;
			m_max = max;
		}

	private:

		RGBImage* m_img;
		vec2f m_min;
		vec2f m_max;

	};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

	template <class T>
	class SVG_PolyLine : public SVG_BaseItem{
	public:
		SVG_PolyLine(PolyLine<T>* pl) : m_pl(pl) {
			color = vec3f(0,0,0);
			fill = "none";
			lw = 3;
			cycles = pl->getAllCycles();
		}

		void setAttributes(vec3f col = vec3f(0,0,0), float linewidth=3, char* fill_ = "none") {
			color = col;
			lw = linewidth;
			fill = fill_;
		}

		//! writes the item to the stream 'os'
		virtual void draw(std::ostream &os, SVG_Resizer* resizer) {

			vec3f col = color*255;

			for (UInt i1 = 0; i1 < cycles.size(); i1++) {
				os << "<polyline fill=\"" << fill << "\" stroke=\"rgb(" << (int)col.x << "," << (int)col.y << "," << (int)col.z << ")\" stroke-width=\"" << lw << "\" points=\"";
				for (UInt i2 = 0; i2 < cycles[i1].size(); i2++) {
					UInt idx = cycles[i1][i2];
					T v = m_pl->points[idx].c;
					vec2f pt = (*resizer)(v);
					os << pt.x << ", " << pt.y << " ";
					if (i2 % 6 == 0) os << std::endl;
				}
				os << "\" />" << std::endl;
			}
		}

	private:
		PolyLine<T>* m_pl;
		std::vector< std::vector<UInt> > cycles;
		vec3f color;
		float lw;
		char* fill;

	};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

	class SVG_Canvas {
	public:

		SVG_Canvas(vec2f minV, vec2f maxV, float width_cm) : resizer(minV, maxV, width_cm) {
			init(minV, maxV, width_cm);
		}

		SVG_Canvas(vec2d minV, vec2d maxV, float width_cm) : resizer(vec2f((float)minV.x, (float)minV.y), vec2f((float)maxV.x, (float)maxV.y), width_cm) {
			init(vec2f((float)minV.x, (float)minV.y), vec2f((float)maxV.x, (float)maxV.y), width_cm);
		}

		void init(vec2f minV, vec2f maxV, float width_cm) {
			m_minV = minV;
			m_maxV = maxV;
			wPix = maxV.x-minV.x;
			hPix = maxV.y-minV.y;
			wcm = width_cm;
			hcm = width_cm*(hPix/wPix);
		}

		void draw(const char* filename) {
			std::cerr <<  "" << wcm << "  " << hcm << std::endl;
			std::ofstream file(filename);
			file << "<?xml version=\"1.0\" standalone=\"no\"?>" << std::endl;
			vec2f canvasMin = resizer(m_minV);
			vec2f canvasMax = resizer(m_maxV);
			file << "<svg width=\"" << wcm << "cm\" height=\"" << hcm << "cm\" viewBox=\"" << canvasMin.x << " " << canvasMin.y << " " << canvasMax.x << " " << canvasMax.y << "\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">" << std::endl;
			for (int i1 = 0; i1 < (int)items.size(); i1++) {
				items[i1]->draw(file, &resizer);
			}
			file << "</svg>" << std::endl;
		}

		void addChild(SVG_BaseItem* item) {
			items.push_back(item);
		}

	private:
		vec2f m_minV;
		vec2f m_maxV;
		float wPix;
		float hPix;
		float wcm;
		float hcm;
		std::vector<SVG_BaseItem*> items;
	public:
		SVG_Resizer resizer;
	};


};

#endif
