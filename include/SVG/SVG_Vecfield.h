#ifndef SVG_VECFIELD
#define SVG_VECFIELD

#include <vector>

#include "SVG_Canvas.h"
#include "point2d.h"

namespace SVG {

	// ****************************************************************************************
	// ****************************************************************************************
	// ****************************************************************************************

	template <class T>
	class SVG_VectorField : public SVG_BaseItem{
		public:
			SVG_VectorField(std::vector<T> pos_, std::vector<T> dir_) : pos(pos_), dir(dir_) {

			}

		//! writes the item to the stream 'os'
		virtual void draw(std::ostream &os, SVG_Resizer* resizer) {
			UInt groupID = resizer->getGroupID();

			os << "<g\nid=\"" << groupID << "\" transform=\"matrix(1,0,0,1,0,0)\">";
			os << "<path\n";
			os << " style=\"fill:#88a745;fill-opacity:1;fill-rule:evenodd;stroke:#708b39;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"\n";
			os << " d=\"M -5,20 L -5,-5 L -10,-5 L 0,-20 L 10,-5 L 5,-5 L 5,20 L -5,20  z\"\n";
			os << " id=\"path2394\"\n";
			os << " sodipodi:nodetypes=\"cccccccc\" />\n";
			os << "<path\n";
			os << " style=\"fill:#9bbb59;fill-opacity:1;fill-rule:evenodd;stroke:#9bbb59;stroke-width:0.5;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1\"\n";
			os << " d=\"M 0,-13 L -4,-7 L 4,-7 L 0,-13 z\"\n";
			os << " id=\"path2396\" />\n";
			os << "</g>\n";

			for (UInt i1 = 0; i1 < (UInt)pos.size(); i1++) {
				T p = pos[i1];
				T d = dir[i1];
				UInt gID = resizer->getGroupID();

				d.normalize();

				vec2f pt2f = (*resizer)(p);

				os << "<use x=\"0\" y=\"0\" inkscape:tiled-clone-of=\"#" << groupID << "\" xlink:href=\"#" << groupID << "\" transform=\"matrix(" << -d.y << "," << d.x << "," << d.x << "," << d.y << "," << pt2f.x+0 << "," << pt2f.y+0 << ")\" id=\"" << gID << "\"/>\n";
			}

			//os << "<g id=\"" << groupID << "\" fill=\"rgb(" << (int)col1.x << "," << (int)col1.y << "," << (int)col1.z << ")\" stroke=\"rgb(" << (int)col2.x << "," << (int)col2.y << "," << (int)col2.z << ")\" stroke-width=\"" << lw << "\" >" << std::endl;
			//for (UInt i1 = 0; i1 < (UInt)m_pc->getNumPts(); i1++) {
			//	T pt = m_pc->getPoints()[i1];
			//	vec2f pt2f = (*resizer)(pt);
			//	os << "<circle cx=\"" << pt2f.x << "\" cy=\"" << pt2f.y << "\" r=\"" << radius << "\" />" << std::endl;
			//}
			//os << "</g>" << std::endl;
		}

		private:
			std::vector<T> pos;
			std::vector<T> dir;
	};

};

#endif
