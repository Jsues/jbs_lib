#ifndef DETERMINANT_H
#define DETERMINANT_H


// ****************************************************************************************


template<class ValueType>
inline static ValueType determinant4x4(const point4d<ValueType>& a1, const point4d<ValueType>& a2, const point4d<ValueType>& a3, const point4d<ValueType>& a4) {

	const ValueType& a11 = a1.x;
	const ValueType& a12 = a1.y;
	const ValueType& a13 = a1.z;
	const ValueType& a14 = a1.w;
	const ValueType& a21 = a2.x;
	const ValueType& a22 = a2.y;
	const ValueType& a23 = a2.z;
	const ValueType& a24 = a2.w;
	const ValueType& a31 = a3.x;
	const ValueType& a32 = a3.y;
	const ValueType& a33 = a3.z;
	const ValueType& a34 = a3.w;
	const ValueType& a41 = a4.x;
	const ValueType& a42 = a4.y;
	const ValueType& a43 = a4.z;
	const ValueType& a44 = a4.w;

	// Formula according to Bronstein, Chapter 4.2.1.2
	ValueType det
	= a31*(a12*(a23*a44-a24*a43) + a13*(a24*a42-a22*a44) + a14*(a22*a43-a23*a42))
	- a32*(a11*(a23*a44-a24*a43) + a13*(a24*a41-a21*a44) + a14*(a21*a43-a23*a41))
	+ a33*(a11*(a22*a44-a24*a42) + a12*(a24*a41-a21*a44) + a14*(a21*a42-a22*a41))
	- a34*(a11*(a22*a43-a23*a42) + a12*(a23*a41-a21*a43) + a13*(a21*a42-a22*a41));

#ifdef _DEBUG
	if (det == 0)
		std::cerr << "Warning determinant = 0" << std::endl;
#endif

	return det;
}

// ****************************************************************************************

#endif
