#ifndef KINECT_SKELETON
#define KINECT_SKELETON

#include "point3d.h"
#include "Matrix3D.h"

#include "Polyline.h"

#define JOINT_EPSILON 0.9

struct bone {

	int start_v;
	int end_v;
	int parent_bone;

	bone() {}

	bone(int s, int e, int p = -1) : start_v(s), end_v(e), parent_bone(p) {}

	void operator=(const bone& other) {
		start_v = other.start_v;
		end_v = other.end_v;
		parent_bone = other.parent_bone;
	}

	mat3d orientation;
	double length;

};


class KinectSkeleton {

public:


	enum JOINTS {
		HEAD = 0,
		NECK = 1,
		L_SHOULDER = 2,
		R_SHOULDER = 3,
		TORSO = 4,
		L_ELBOW = 5,
		R_ELBOW = 6,
		L_HAND = 7,
		R_HAND = 8,
		L_HIP = 9,
		R_HIP = 10,
		L_KNEE = 11,
		R_KNEE = 12,
		L_FOOT = 13,
		R_FOOT = 14
	};

	enum BONES {
		B_BREAST = 0,
		B_L_SHOULDER = 1,
		B_R_SHOULDER = 2,
		B_HEAD = 3,
		B_L_ABDOMEN = 4,
		B_R_ABDOMEN = 5,
		B_L_UPPER_ARM = 6,
		B_R_UPPER_ARM = 7,
		B_L_UPPER_LEG = 8,
		B_R_UPPER_LEG = 9,
		B_L_LOWER_ARM = 10,
		B_R_LOWER_ARM = 11,
		B_L_LOWER_LEG = 12,
		B_R_LOWER_LEG = 13
	};

	KinectSkeleton() {

		bones[B_BREAST] = bone(NECK, TORSO, -1);
		bones[B_L_SHOULDER] = bone(NECK, L_SHOULDER, B_BREAST);
		bones[B_R_SHOULDER] = bone(NECK, R_SHOULDER, B_BREAST);
		bones[B_HEAD] = bone(NECK, HEAD, B_BREAST);
		bones[B_L_ABDOMEN] = bone(TORSO, L_HIP, B_BREAST);
		bones[B_R_ABDOMEN] = bone(TORSO, R_HIP, B_BREAST);
		bones[B_L_UPPER_ARM] = bone(L_SHOULDER, L_ELBOW, B_L_SHOULDER);
		bones[B_R_UPPER_ARM] = bone(R_SHOULDER, R_ELBOW, B_R_SHOULDER);
		bones[B_L_UPPER_LEG] = bone(L_HIP, L_KNEE, B_L_ABDOMEN);
		bones[B_R_UPPER_LEG] = bone(R_HIP, R_KNEE, B_R_ABDOMEN);
		bones[B_L_LOWER_ARM] = bone(L_ELBOW, L_HAND, B_L_UPPER_ARM);
		bones[B_R_LOWER_ARM] = bone(R_ELBOW, R_HAND, B_R_UPPER_ARM);
		bones[B_L_LOWER_LEG] = bone(L_KNEE, L_FOOT, B_L_UPPER_LEG);
		bones[B_R_LOWER_LEG] = bone(R_KNEE, R_FOOT, B_R_UPPER_LEG);


		segments[0] = vec3i(0,0,0);

		segments[0] = vec3i(0,1,0); // head - neck
		segments[1] = vec3i(1,2,0); // neck - left shoulder;
		segments[2] = vec3i(1,3,0); // neck - right shoulder;
		segments[3] = vec3i(2,4,0); // left shoulder - torso;
		segments[4] = vec3i(3,4,0); // right shoulder - torso;
		segments[5] = vec3i(2,5,0); // left shoulder - left elbow;
		segments[6] = vec3i(3,6,0); // right shoulder - right elbow;
		segments[7] = vec3i(5,7,0); // left elbow - left hand;
		segments[8] = vec3i(6,8,0); // right elbow - right hand;
		segments[9] = vec3i(4,9,0); // torso - left hip;
		segments[10] = vec3i(4,10,0); // torso - right hip;
		segments[11] = vec3i(9,11,0); // left hip - left knee;
		segments[12] = vec3i(10,12,0); // right hip - right knee;
		segments[13] = vec3i(11,13,0); // left knee - left foot;
		segments[14] = vec3i(12,14,0); // right knee - right foot;
	}

	void saveAsPinocchio(const char* fn) {
		std::ofstream fout(fn);

		vec3d& p = joints[1];
		fout << "0 " << p.x << " " << p.y << " " << p.z << " -1\n"; // neck
		p = joints[4];
		fout << "1 " << p.x << " " << p.y << " " << p.z << " 0\n"; // torso
		p = joints[2];
		fout << "2 " << p.x << " " << p.y << " " << p.z << " 0\n"; // left shoulder
		p = joints[3];
		fout << "3 " << p.x << " " << p.y << " " << p.z << " 0\n"; // right shoulder
		p = joints[0];
		fout << "4 " << p.x << " " << p.y << " " << p.z << " 0\n"; // head

		p = joints[9];
		fout << "5 " << p.x << " " << p.y << " " << p.z << " 1\n"; // left hip
		p = joints[10];
		fout << "6 " << p.x << " " << p.y << " " << p.z << " 1\n"; // right hip

		p = joints[5];
		fout << "7 " << p.x << " " << p.y << " " << p.z << " 2\n"; // left elbow
		p = joints[6];
		fout << "8 " << p.x << " " << p.y << " " << p.z << " 3\n"; // right elbow

		p = joints[11];
		fout << "9 " << p.x << " " << p.y << " " << p.z << " 5\n"; // left knee
		p = joints[12];
		fout << "10 " << p.x << " " << p.y << " " << p.z << " 6\n"; // right knee

		p = joints[7];
		fout << "11 " << p.x << " " << p.y << " " << p.z << " 7\n"; // left hand
		p = joints[8];
		fout << "12 " << p.x << " " << p.y << " " << p.z << " 8\n"; // right hand

		p = joints[13];
		fout << "13 " << p.x << " " << p.y << " " << p.z << " 9\n"; // left foot
		p = joints[14];
		fout << "14 " << p.x << " " << p.y << " " << p.z << " 10\n"; // right foot
	}

	inline void setHead(vec3d pos) {
		joints[HEAD] = pos;
	}

	inline void setNeck(vec3d pos) {
		joints[NECK] = pos;
	}

	inline void setLShoulder(vec3d pos) {
		joints[L_SHOULDER] = pos;
	}

	inline void setRShoulder(vec3d pos) {
		joints[R_SHOULDER] = pos;
	}

	inline void setTorso(vec3d pos) {
		joints[TORSO] = pos;
	}

	inline void setLElbow(vec3d pos) {
		joints[L_ELBOW] = pos;
	}

	inline void setRElbow(vec3d pos) {
		joints[R_ELBOW] = pos;
	}

	inline void setLHand(vec3d pos) {
		joints[L_HAND] = pos;
	}

	inline void setRHand(vec3d pos) {
		joints[R_HAND] = pos;
	}

	inline void setLHip(vec3d pos) {
		joints[L_HIP] = pos;
	}

	inline void setRHip(vec3d pos) {
		joints[R_HIP] = pos;
	}

	inline void setLKnee(vec3d pos) {
		joints[L_KNEE] = pos;
	}

	inline void setRKnee(vec3d pos) {
		joints[R_KNEE] = pos;
	}

	inline void setLFoot(vec3d pos) {
		joints[L_FOOT] = pos;
	}

	inline void setRFoot(vec3d pos) {
		joints[R_FOOT] = pos;
	}

	void saveToFile(const char* filename) {

		PolyLine<vec3d> pl;
		for (int i1 = 0; i1 <= 14; i1++) {
			pl.insertVertex(joints[i1]);
		}
		pl.insertLine(0,1); // head - neck
		pl.insertLine(1,2); // neck - left shoulder;
		pl.insertLine(1,3); // neck - right shoulder;
		pl.insertLine(2,4); // left shoulder - torso;
		pl.insertLine(3,4); // right shoulder - torso;
		pl.insertLine(2,5); // left shoulder - left elbow;
		pl.insertLine(3,6); // right shoulder - right elbow;
		pl.insertLine(5,7); // left elbow - left hand;
		pl.insertLine(6,8); // right elbow - right hand;
		pl.insertLine(4,9); // torso - left hip;
		pl.insertLine(4,10); // torso - right hip;
		pl.insertLine(9,11); // left hip - left knee;
		pl.insertLine(10,12); // right hip - right knee;
		pl.insertLine(11,13); // left knee - left foot;
		pl.insertLine(12,14); // right knee - right foot;

		pl.writeToFile(filename);
	}

	template <class T>
	void fitToMesh(const T* pts) {
		// Joint placement relative to pca_model
		vec3d lefthand = pts[6095]*2.0/3.0 + pts[6022]*1.0/3.0;
		setLHand(lefthand);
		vec3d leftelbow = (pts[5816] + pts[4029])/2.0;
		setLElbow(leftelbow);
		vec3d leftshoulder = (pts[3957] + pts[6202])/2.0;
		setLShoulder(leftshoulder);
		vec3d lefthip = (pts[3661] + pts[5392])/2.0;
		setLHip(lefthip);
		vec3d leftknee = (pts[4832] + pts[4978])/2.0;
		setLKnee(leftknee);
		vec3d leftfoot = (pts[4468] + pts[4411])/2.0;
		setLFoot(leftfoot);

		vec3d righthand = pts[2965]*2.0/3.0 + pts[2892]*1.0/3.0;
		setRHand(righthand);
		vec3d rightelbow = (pts[2686] + pts[812])/2.0;
		setRElbow(rightelbow);
		vec3d rightshoulder = (pts[740] + pts[3072])/2.0;
		setRShoulder(rightshoulder);
		vec3d righthip = (pts[444] + pts[2261])/2.0;
		setRHip(righthip);
		vec3d rightknee = (pts[1701] + pts[1847])/2.0;
		setRKnee(rightknee);
		vec3d rightfoot = (pts[1337] + pts[1280])/2.0;
		setRFoot(rightfoot);

		vec3d head = (pts[85] + pts[1178])/2.0;
		setHead(head);
		vec3d neck = (pts[56] + pts[1148])/2.0;
		setNeck(neck);
		vec3d torso = (pts[38] + pts[1136])/2.0;
		setTorso(torso);
	}


	void computeBoneLengths() {
		for (int i1 = 0; i1 < 14; i1++) {
			bone& b = bones[i1];
			double l = joints[b.start_v].dist(joints[b.end_v]);
			b.length = l;
		}
	}


	void recoverOriginalBoneLengths() {

		vec3d l_torso = transformToBoneCoords(joints[TORSO], B_BREAST);
		vec3d l_head = transformToBoneCoords(joints[HEAD], B_BREAST);
		vec3d l_l_shoulder = transformToBoneCoords(joints[L_SHOULDER], B_BREAST);
		vec3d l_r_shoulder = transformToBoneCoords(joints[R_SHOULDER], B_BREAST);

		vec3d l_l_elbow = bones[B_L_SHOULDER].orientation.vecTransTransposedMatrix(joints[L_ELBOW]-joints[L_SHOULDER]);
		vec3d l_r_elbow = bones[B_R_SHOULDER].orientation.vecTransTransposedMatrix(joints[R_ELBOW]-joints[R_SHOULDER]);
		vec3d l_l_hand = bones[B_L_UPPER_ARM].orientation.vecTransTransposedMatrix(joints[L_HAND]-joints[L_ELBOW]);
		vec3d l_r_hand = bones[B_R_UPPER_ARM].orientation.vecTransTransposedMatrix(joints[R_HAND]-joints[R_ELBOW]);
		vec3d l_l_hip = bones[B_BREAST].orientation.vecTransTransposedMatrix(joints[L_HIP]-joints[TORSO]);
		vec3d l_r_hip = bones[B_BREAST].orientation.vecTransTransposedMatrix(joints[R_HIP]-joints[TORSO]);
		vec3d l_l_knee = bones[B_L_ABDOMEN].orientation.vecTransTransposedMatrix(joints[L_KNEE]-joints[L_HIP]);
		vec3d l_r_knee = bones[B_R_ABDOMEN].orientation.vecTransTransposedMatrix(joints[R_KNEE]-joints[R_HIP]);
		vec3d l_l_foot = bones[B_L_UPPER_LEG].orientation.vecTransTransposedMatrix(joints[L_FOOT]-joints[L_KNEE]);
		vec3d l_r_foot = bones[B_R_UPPER_LEG].orientation.vecTransTransposedMatrix(joints[R_FOOT]-joints[R_KNEE]);

		l_torso.normalize();
		l_torso *= bones[B_BREAST].length;
		joints[TORSO] = transformFromBoneCoords(l_torso, B_BREAST);

		l_head.normalize();
		l_head *= bones[B_HEAD].length;
		joints[HEAD] = transformFromBoneCoords(l_head, B_BREAST);

		l_l_shoulder.normalize();
		l_l_shoulder *= bones[B_L_SHOULDER].length;
		joints[L_SHOULDER] = transformFromBoneCoords(l_l_shoulder, B_BREAST);

		l_r_shoulder.normalize();
		l_r_shoulder *= bones[B_R_SHOULDER].length;
		joints[R_SHOULDER] = transformFromBoneCoords(l_r_shoulder, B_BREAST);

		l_l_elbow.normalize();
		l_l_elbow *= bones[B_L_UPPER_ARM].length;
		joints[L_ELBOW] = bones[B_L_SHOULDER].orientation.vecTrans(l_l_elbow)+joints[L_SHOULDER];

		l_r_elbow.normalize();
		l_r_elbow *= bones[B_R_UPPER_ARM].length;
		joints[R_ELBOW] = bones[B_R_SHOULDER].orientation.vecTrans(l_r_elbow)+joints[R_SHOULDER];

		l_l_hand.normalize();
		l_l_hand *= bones[B_L_LOWER_ARM].length;
		joints[L_HAND] = bones[B_L_UPPER_ARM].orientation.vecTrans(l_l_hand)+joints[L_ELBOW];

		l_r_hand.normalize();
		l_r_hand *= bones[B_R_LOWER_ARM].length;
		joints[R_HAND] = bones[B_R_UPPER_ARM].orientation.vecTrans(l_r_hand)+joints[R_ELBOW];

		l_l_hip.normalize();
		l_l_hip *= bones[B_L_ABDOMEN].length;
		joints[L_HIP] = bones[B_BREAST].orientation.vecTrans(l_l_hip)+joints[TORSO];

		l_r_hip.normalize();
		l_r_hip *= bones[B_R_ABDOMEN].length;
		joints[R_HIP] = bones[B_BREAST].orientation.vecTrans(l_r_hip)+joints[TORSO];

		l_l_knee.normalize();
		l_l_knee *= bones[B_L_UPPER_LEG].length;
		joints[L_KNEE] = bones[B_L_ABDOMEN].orientation.vecTrans(l_l_knee)+joints[L_HIP];

		l_r_knee.normalize();
		l_r_knee *= bones[B_R_UPPER_LEG].length;
		joints[R_KNEE] = bones[B_R_ABDOMEN].orientation.vecTrans(l_r_knee)+joints[R_HIP];

		l_l_foot.normalize();
		l_l_foot *= bones[B_L_LOWER_LEG].length;
		joints[L_FOOT] = bones[B_L_UPPER_LEG].orientation.vecTrans(l_l_foot)+joints[L_KNEE];

		l_r_foot.normalize();
		l_r_foot *= bones[B_R_LOWER_LEG].length;
		joints[R_FOOT] = bones[B_R_UPPER_LEG].orientation.vecTrans(l_r_foot)+joints[R_KNEE];
	}


	vec3d transformToBoneCoords(const vec3d& ptIn, int BoneID) {
		const mat3d& m = bones[BoneID].orientation;
		vec3d ret = m.vecTransTransposedMatrix(ptIn-joints[bones[BoneID].start_v]);
		return ret;
	}

	vec3d transformFromBoneCoords(const vec3d& ptIn, int BoneID) {
		vec3d ret = bones[BoneID].orientation.vecTrans(ptIn);
		ret += joints[bones[BoneID].start_v];
		return ret;
	}

	//void update(const vec3d& refPoint) {
	//	joints[TORSO] = refPoint;
	//}

	inline void computeBoneOrientations() {

		// BREAST Bone
		const vec3d& b_t = joints[TORSO];
		const vec3d& b_n = joints[NECK];
		const vec3d& b_l_h = joints[L_HIP];
		const vec3d& b_r_h = joints[R_HIP];
		vec3d up = b_n-b_t; up.normalize();
		vec3d v0 = up^(b_l_h-b_t);
		vec3d v1 = up^(b_t-b_r_h);
		vec3d front = (v0^up)+(v1^up); front.normalize();
		vec3d side = front^up;
		bones[B_BREAST].orientation.setCol(1, up);
		bones[B_BREAST].orientation.setCol(0, front);
		bones[B_BREAST].orientation.setCol(2, side);


		// LEFT HIP Bone
		vec3d frontsave = front;
		up = b_l_h-b_t; up.normalize();
		vec3d tmp = up^front;
		front = up^tmp; front.normalize();
		side = front^up;
		bones[B_L_ABDOMEN].orientation.setCol(1, up);
		bones[B_L_ABDOMEN].orientation.setCol(0, front);
		bones[B_L_ABDOMEN].orientation.setCol(2, side);


		// RIGHT HIP Bone
		front = frontsave;
		up = b_r_h-b_t; up.normalize();
		tmp = up^front;
		front = up^tmp; front.normalize();
		side = front^up;
		bones[B_R_ABDOMEN].orientation.setCol(1, up);
		bones[B_R_ABDOMEN].orientation.setCol(0, front);
		bones[B_R_ABDOMEN].orientation.setCol(2, side);


		// LEFT UPPER LEG Bone
		const vec3d& b_l_k = joints[L_KNEE];
		up = b_l_k-b_l_h; up.normalize();
		vec3d tmp_hip = b_l_h-b_r_h;
		front = tmp_hip^up;
		front = front^up; front.normalize();
		side = front^up;
		bones[B_L_UPPER_LEG].orientation.setCol(1, up);
		bones[B_L_UPPER_LEG].orientation.setCol(0, front);
		bones[B_L_UPPER_LEG].orientation.setCol(2, side);


		// RIGHT UPPER LEG Bone
		const vec3d& b_r_k = joints[R_KNEE];
		up = b_r_k-b_r_h; up.normalize();
		front = tmp_hip^up;
		front = front^up; front.normalize();
		side = front^up;
		bones[B_R_UPPER_LEG].orientation.setCol(1, up);
		bones[B_R_UPPER_LEG].orientation.setCol(0, front);
		bones[B_R_UPPER_LEG].orientation.setCol(2, side);


		// LEFT LOWER LEG Bone
		const vec3f& b_l_f = joints[L_FOOT];
		up = b_l_f-b_l_k; up.normalize();
		tmp = b_l_k-b_l_h; tmp.normalize();
		if ((up|tmp) > JOINT_EPSILON) {
			//std::cerr << "badly defined left knee joint" << std::endl;
			front = bones[B_L_UPPER_LEG].orientation.getCol(0);
			tmp = up^front;
			front = tmp^up; front.normalize();
			side = front^up;
		} else {
			front = tmp^up; front.normalize();
			side = front^up;
		}
		bones[B_L_LOWER_LEG].orientation.setCol(1, up);
		bones[B_L_LOWER_LEG].orientation.setCol(0, front);
		bones[B_L_LOWER_LEG].orientation.setCol(2, side);


		// RIGHT LOWER LEG Bone
		const vec3f& b_r_f = joints[R_FOOT];
		up = b_r_f-b_r_k; up.normalize();
		tmp = b_r_k-b_r_h; tmp.normalize();
		if ((up|tmp) > JOINT_EPSILON) {
			//std::cerr << "badly defined right knee joint" << std::endl;
			front = bones[B_R_UPPER_LEG].orientation.getCol(0);
			tmp = up^front;
			front = tmp^up; front.normalize();
			side = front^up;
		} else {
			front = tmp^up; front.normalize();
			side = front^up;
		}
		bones[B_R_LOWER_LEG].orientation.setCol(1, up);
		bones[B_R_LOWER_LEG].orientation.setCol(0, front);
		bones[B_R_LOWER_LEG].orientation.setCol(2, side);


		// HEAD Bone
		const vec3f& b_h = joints[HEAD];
		up = b_h-b_n; up.normalize();
		front = bones[B_BREAST].orientation.getCol(0);
		tmp = up^front;
		front = tmp^up; front.normalize();
		side = front^up;
		bones[B_HEAD].orientation.setCol(1, up);
		bones[B_HEAD].orientation.setCol(0, front);
		bones[B_HEAD].orientation.setCol(2, side);

		// LEFT SHOULDER Bone
		const vec3f& b_l_s = joints[L_SHOULDER];
		up = b_l_s-b_n; up.normalize();
		side = up^(b_n-b_t); side.normalize();
		front = up^side;
		bones[B_L_SHOULDER].orientation.setCol(1, up);
		bones[B_L_SHOULDER].orientation.setCol(0, front);
		bones[B_L_SHOULDER].orientation.setCol(2, side);

		// RIGHT SHOULDER Bone
		const vec3f& b_r_s = joints[R_SHOULDER];
		up = b_r_s-b_n; up.normalize();
		side = up^(b_n-b_t); side.normalize();
		front = up^side;
		bones[B_R_SHOULDER].orientation.setCol(1, up);
		bones[B_R_SHOULDER].orientation.setCol(0, front);
		bones[B_R_SHOULDER].orientation.setCol(2, side);

		// LEFT UPPER ARM Bone
		const vec3f& b_l_e = joints[L_ELBOW];
		up = b_l_e-b_l_s; up.normalize();
		front = up^bones[B_L_SHOULDER].orientation.getCol(0);
		front = up^front; front.normalize();
		side = front^up;
		bones[B_L_UPPER_ARM].orientation.setCol(1, up);
		bones[B_L_UPPER_ARM].orientation.setCol(0, front);
		bones[B_L_UPPER_ARM].orientation.setCol(2, side);

		// RIGHT UPPER ARM Bone
		const vec3f& b_r_e = joints[R_ELBOW];
		up = b_r_e-b_r_s; up.normalize();
		front = up^bones[B_R_SHOULDER].orientation.getCol(0);
		front = up^front; front.normalize();
		side = front^up;
		bones[B_R_UPPER_ARM].orientation.setCol(1, up);
		bones[B_R_UPPER_ARM].orientation.setCol(0, front);
		bones[B_R_UPPER_ARM].orientation.setCol(2, side);


		// LEFT LOWER ARM Bone
		const vec3f b_l_ha = joints[L_HAND];
		up = b_l_ha-b_l_e; up.normalize();
		tmp = b_l_e-b_l_s; tmp.normalize();
		if ((up|tmp) > JOINT_EPSILON) {
			//std::cerr << "badly defined left elbow joint" << std::endl;
			front = bones[B_L_UPPER_ARM].orientation.getCol(0);
			tmp = up^front;
			front = tmp^up; front.normalize();
			side = front^up;
		} else {
			front = tmp^up; front.normalize();
			side = front^up;
		}
		bones[B_L_LOWER_ARM].orientation.setCol(1, up);
		bones[B_L_LOWER_ARM].orientation.setCol(0, front);
		bones[B_L_LOWER_ARM].orientation.setCol(2, side);

		// RIGHT LOWER ARM Bone
		const vec3f b_r_ha = joints[R_HAND];
		up = b_r_ha-b_r_e; up.normalize();
		tmp = b_r_e-b_r_s; tmp.normalize();
		if ((up|tmp) > JOINT_EPSILON) {
			//std::cerr << "badly defined right elbow joint" << std::endl;
			front = bones[B_R_UPPER_ARM].orientation.getCol(0);
			tmp = up^front;
			front = tmp^up; front.normalize();
			side = front^up;
		} else {
			front = tmp^up; front.normalize();
			side = front^up;
		}
		bones[B_R_LOWER_ARM].orientation.setCol(1, up);
		bones[B_R_LOWER_ARM].orientation.setCol(0, front);
		bones[B_R_LOWER_ARM].orientation.setCol(2, side);
	}


	inline void computeBoneOrientationsForPoseCorrection() {

		// BREAST Bone
		const vec3d& b_t = joints[TORSO];
		const vec3d& b_n = joints[NECK];
		const vec3d& b_l_h = joints[L_HIP];
		const vec3d& b_r_h = joints[R_HIP];
		vec3d up = b_n-b_t; up.normalize();
		vec3d v0 = up^(b_l_h-b_t);
		vec3d v1 = up^(b_t-b_r_h);
		vec3d front = (v0^up)+(v1^up); front.normalize();
		vec3d side = front^up;
		bones[B_BREAST].orientation.setCol(1, up);
		bones[B_BREAST].orientation.setCol(0, front);
		bones[B_BREAST].orientation.setCol(2, side);


		// LEFT HIP Bone
		vec3d frontsave = front;
		up = b_l_h-b_t; up.normalize();
		vec3d tmp = up^front;
		front = up^tmp; front.normalize();
		side = front^up;
		bones[B_L_ABDOMEN].orientation.setCol(1, up);
		bones[B_L_ABDOMEN].orientation.setCol(0, front);
		bones[B_L_ABDOMEN].orientation.setCol(2, side);


		// RIGHT HIP Bone
		front = frontsave;
		up = b_r_h-b_t; up.normalize();
		tmp = up^front;
		front = up^tmp; front.normalize();
		side = front^up;
		bones[B_R_ABDOMEN].orientation.setCol(1, up);
		bones[B_R_ABDOMEN].orientation.setCol(0, front);
		bones[B_R_ABDOMEN].orientation.setCol(2, side);


		// LEFT UPPER LEG Bone
		const vec3d& b_l_k = joints[L_KNEE];
		up = b_l_k-b_l_h; up.normalize();
		vec3d tmp_hip = b_l_h-b_r_h;
		front = tmp_hip^up;
		front = front^up; front.normalize();
		side = front^up;
		bones[B_L_UPPER_LEG].orientation.setCol(1, up);
		bones[B_L_UPPER_LEG].orientation.setCol(0, front);
		bones[B_L_UPPER_LEG].orientation.setCol(2, side);


		// RIGHT UPPER LEG Bone
		const vec3d& b_r_k = joints[R_KNEE];
		up = b_r_k-b_r_h; up.normalize();
		front = tmp_hip^up;
		front = front^up; front.normalize();
		side = front^up;
		bones[B_R_UPPER_LEG].orientation.setCol(1, up);
		bones[B_R_UPPER_LEG].orientation.setCol(0, front);
		bones[B_R_UPPER_LEG].orientation.setCol(2, side);


		// LEFT LOWER LEG Bone
		const vec3f& b_l_f = joints[L_FOOT];
		up = b_l_f-b_l_k; up.normalize();
		tmp = b_l_k-b_l_h; tmp.normalize();
		if ((up|tmp) > JOINT_EPSILON) {
			//std::cerr << "badly defined left knee joint" << std::endl;
			front = bones[B_L_UPPER_LEG].orientation.getCol(0);
			tmp = up^front;
			front = tmp^up; front.normalize();
			side = front^up;
		} else {
			front = tmp^up; front.normalize();
			side = front^up;
		}
		bones[B_L_LOWER_LEG].orientation.setCol(1, up);
		bones[B_L_LOWER_LEG].orientation.setCol(0, front);
		bones[B_L_LOWER_LEG].orientation.setCol(2, side);


		// RIGHT LOWER LEG Bone
		const vec3f& b_r_f = joints[R_FOOT];
		up = b_r_f-b_r_k; up.normalize();
		tmp = b_r_k-b_r_h; tmp.normalize();
		if ((up|tmp) > JOINT_EPSILON) {
			//std::cerr << "badly defined right knee joint" << std::endl;
			front = bones[B_R_UPPER_LEG].orientation.getCol(0);
			tmp = up^front;
			front = tmp^up; front.normalize();
			side = front^up;
		} else {
			front = tmp^up; front.normalize();
			side = front^up;
		}
		bones[B_R_LOWER_LEG].orientation.setCol(1, up);
		bones[B_R_LOWER_LEG].orientation.setCol(0, front);
		bones[B_R_LOWER_LEG].orientation.setCol(2, side);


		// HEAD Bone
		const vec3f& b_h = joints[HEAD];
		up = b_h-b_n; up.normalize();
		front = bones[B_BREAST].orientation.getCol(0);
		tmp = up^front;
		front = tmp^up; front.normalize();
		side = front^up;
		bones[B_HEAD].orientation.setCol(1, up);
		bones[B_HEAD].orientation.setCol(0, front);
		bones[B_HEAD].orientation.setCol(2, side);

		// LEFT SHOULDER Bone
		const vec3f& b_l_s = joints[L_SHOULDER];
		up = b_l_s-b_n; up.normalize();
		side = up^(b_n-b_t); side.normalize();
		front = up^side;
		bones[B_L_SHOULDER].orientation.setCol(1, up);
		bones[B_L_SHOULDER].orientation.setCol(0, front);
		bones[B_L_SHOULDER].orientation.setCol(2, side);

		// RIGHT SHOULDER Bone
		const vec3f& b_r_s = joints[R_SHOULDER];
		up = b_r_s-b_n; up.normalize();
		side = up^(b_n-b_t); side.normalize();
		front = up^side;
		bones[B_R_SHOULDER].orientation.setCol(1, up);
		bones[B_R_SHOULDER].orientation.setCol(0, front);
		bones[B_R_SHOULDER].orientation.setCol(2, side);

		// LEFT UPPER ARM Bone
		const vec3f& b_l_e = joints[L_ELBOW];
		up = b_l_e-b_l_s; up.normalize();
		front = up^bones[B_L_SHOULDER].orientation.getCol(0);
		front = up^front; front.normalize();
		side = front^up;
		bones[B_L_UPPER_ARM].orientation.setCol(1, up);
		bones[B_L_UPPER_ARM].orientation.setCol(0, front);
		bones[B_L_UPPER_ARM].orientation.setCol(2, side);

		// RIGHT UPPER ARM Bone
		const vec3f& b_r_e = joints[R_ELBOW];
		up = b_r_e-b_r_s; up.normalize();
		front = up^bones[B_R_SHOULDER].orientation.getCol(0);
		front = up^front; front.normalize();
		side = front^up;
		bones[B_R_UPPER_ARM].orientation.setCol(1, up);
		bones[B_R_UPPER_ARM].orientation.setCol(0, front);
		bones[B_R_UPPER_ARM].orientation.setCol(2, side);


		// LEFT LOWER ARM Bone
		const vec3f b_l_ha = joints[L_HAND];
		up = b_l_ha-b_l_e; up.normalize();
		tmp = up^frontsave;
		front = tmp^up; front.normalize();
		side = front^up;
		bones[B_L_LOWER_ARM].orientation.setCol(1, up);
		bones[B_L_LOWER_ARM].orientation.setCol(0, front);
		bones[B_L_LOWER_ARM].orientation.setCol(2, side);

		// RIGHT LOWER ARM Bone
		const vec3f b_r_ha = joints[R_HAND];
		up = b_r_ha-b_r_e; up.normalize();
		tmp = up^frontsave;
		front = tmp^up; front.normalize();
		side = front^up;
		bones[B_R_LOWER_ARM].orientation.setCol(1, up);
		bones[B_R_LOWER_ARM].orientation.setCol(0, front);
		bones[B_R_LOWER_ARM].orientation.setCol(2, side);
	}


	vec3d joints[15];
	vec3i segments[15];

	bone bones[14];

private:



};


#endif
