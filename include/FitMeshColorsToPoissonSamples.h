#ifndef FIT_MESH_COLORS_TO_POISSON_SAMPLES_H
#define FIT_MESH_COLORS_TO_POISSON_SAMPLES_H


#include "SimpleMesh.h"
#include "PoissonSampleMesh.h"

#ifdef USETAUCS
extern "C" {
#include <taucs.h>
}
#else
#include "umfpack.h"
#endif



// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

#ifdef USETAUCS
bool checkForTaucsError(int i) {

	//std::cerr << "checkForTaucsError" << std::endl;


	if (i != TAUCS_SUCCESS) {
		//std::cerr << "Solution error." << std::endl;
	
		if (i==TAUCS_ERROR)				std::cerr << "Generic error." << std::endl;
		if (i==TAUCS_ERROR_NOMEM)		std::cerr << "NOMEM error." << std::endl;
		if (i==TAUCS_ERROR_BADARGS)		std::cerr << "BADARGS error." << std::endl;
		if (i==TAUCS_ERROR_MAXDEPTH)	std::cerr << "MAXDEPTH error." << std::endl;
		if (i==TAUCS_ERROR_INDEFINITE)	std::cerr << "NOT POSITIVE DEFINITE error." << std::endl;

		return false;
	} else {
		//std::cerr << "No taucs error found" << std::endl;
	}

	return true;
}
#endif


//******************************************************************************************
//******************************************************************************************
//******************************************************************************************


typedef PoissonSampleOnMesh<vec3f>::PoissonSample<vec3f> PoissonSample;


static void FitMeshColorsToPoissonSamples(SimpleMesh* sm, const PoissonSample* psamples, const int numPs) {

	for (int i1 = 0; i1 < sm->getNumV(); i1++) sm->vList[i1].param = vec2d(0.0);
	for (int i1 = 0; i1 < (int)sm->eList.size(); i1++) sm->eList[i1]->cotangent_weight = 0;

	int numUnknown = sm->getNumV();
	int numT = sm->getNumT();
	std::vector< vec3f > rhs;
	rhs.reserve(numUnknown);
	for (int i1 = 0; i1 < numUnknown; i1++) rhs.push_back( vec3f(0.0f) );

	double* perTriangleWeights = new double[numT];
	for (int i1 = 0; i1 < numT; i1++) perTriangleWeights[i1] = 0;

	for (int i1 = 0; i1 < numPs; i1++) {
		const PoissonSample& ps = psamples[i1];
		perTriangleWeights[ps.triID] += 1;
	}

	for (int i1 = 0; i1 < numT; i1++) {
		perTriangleWeights[i1] = sm->tList[i1]->getArea() / perTriangleWeights[i1];
	}

	for (int i1 = 0; i1 < numPs; i1++) {
		const PoissonSample& ps = psamples[i1];
		Triangle* t = sm->tList[ps.triID];
		const int v0 = t->v0();
		const int v1 = t->v1();
		const int v2 = t->v2();
		Edge* e01 = sm->vList[v0].eList.getEdge(v0, v1);
		Edge* e12 = sm->vList[v1].eList.getEdge(v1, v2);
		Edge* e20 = sm->vList[v2].eList.getEdge(v2, v0);

		vec3f barys = ps.barys;
		vec3f color = ps.color;
		barys *= (float)perTriangleWeights[ps.triID];
		color *= (float)perTriangleWeights[ps.triID];

		e01->cotangent_weight += barys[0]*barys[1];
		e12->cotangent_weight += barys[1]*barys[2];
		e20->cotangent_weight += barys[2]*barys[0];
		sm->vList[v0].param.x += barys[0]*barys[0];
		sm->vList[v1].param.x += barys[1]*barys[1];
		sm->vList[v2].param.x += barys[2]*barys[2];

		rhs[v0] += color*barys[0];
		rhs[v1] += color*barys[1];
		rhs[v2] += color*barys[2];
	}


	// Build crs matrix
	std::vector<double> A_entries;
	std::vector<int> A_row_index;
	std::vector<int> A_col_ptr;
	A_entries.reserve(numUnknown*8);	// each row has on average 7 entries (8 is a backup to reduce the re-allocate probability)
	A_row_index.reserve(numUnknown*8);	// each row has on average 7 entries
	A_col_ptr.reserve(numUnknown+1);
	A_col_ptr.push_back(0);



	for (int i1 = 0; i1 < numUnknown; i1++) {

		std::set< std::pair<int, double> > neighbors;
		double sum = 0;
		for (UInt i2 = 0; i2 < sm->vList[i1].getNumN(); i2++) {
			int n = sm->vList[i1].getNeighbor(i2, i1);
			double w = sm->vList[i1].eList[i2]->cotangent_weight;
			neighbors.insert(std::make_pair(n, w));
		}

		if (sm->vList[i1].param.x == 0) {
			std::cerr << "kl";
			sm->vList[i1].param.x = 1;
		}
		neighbors.insert(std::make_pair(i1, sm->vList[i1].param.x));
		for (std::set< std::pair<int, double> >::const_iterator it = neighbors.begin(); it != neighbors.end(); it++) {
			int nID = it->first;

#ifdef USETAUCS
			if (nID > i1) continue; // only store the upper half
#endif

			double value = it->second;
			A_entries.push_back(value);
			A_row_index.push_back(nID);
		}
		A_col_ptr.push_back(A_entries.size());
	}

#ifdef USETAUCS
	taucs_ccs_matrix  A; // a matrix to solve Ax=b in CCS format
	void* F;
	A.n = numUnknown;
	A.m = numUnknown;
	A.colptr = &A_col_ptr[0];
	A.rowind = &A_row_index[0];
	A.values.d = &A_entries[0];
	A.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
	char* options[] = {"taucs.factor.LLT=true", NULL};
	F = NULL;
	void* opt_arg[] = { NULL };
	int i = taucs_linsolve(&A,&F,0,NULL,NULL,options,NULL);
	checkForTaucsError(i);
#else
    void *Symbolic, *Numeric;
	int result1 = umfpack_di_symbolic(numUnknown, numUnknown, &A_col_ptr[0], &A_row_index[0], &A_entries[0], &Symbolic, NULL, NULL);
	int result2 = umfpack_di_numeric(&A_col_ptr[0], &A_row_index[0], &A_entries[0], Symbolic, &Numeric, NULL, NULL);
#endif


	// Reorganize rhs:
	double* rhs_p = new double[3*numUnknown];
	double* x = new double[3*numUnknown];
	for (int i1 = 0; i1 < numUnknown; i1++) {
		rhs_p[i1+numUnknown*0] = rhs[i1].x;
		rhs_p[i1+numUnknown*1] = rhs[i1].y;
		rhs_p[i1+numUnknown*2] = rhs[i1].z;
	}


	std::cerr << "solve" << std::endl;
#ifdef USETAUCS
	i = taucs_linsolve(&A,&F,2,x,rhs_p,options,NULL);
	checkForTaucsError(i);
#else
	for (int i1 = 0; i1 < 3; i1++) {
		double* xx = x+i1*numUnknown;
		double* bb = rhs_p+i1*numUnknown;
		umfpack_di_solve(UMFPACK_A, &A_col_ptr[0], &A_row_index[0], &A_entries[0], xx, bb, Numeric, NULL, NULL);
	}
#endif
	std::cerr << "done" << std::endl;

	for (int i1 = 0; i1 < numUnknown; i1++) {
		//std::cerr << x[i1] << std::endl;
		double col_r = x[i1];
		double col_g = x[i1+numUnknown];
		double col_b = x[i1+numUnknown*2];


		// Crop to [0...1]
		col_r = (col_r < 1.0) ? col_r : 1.0;
		col_r = (col_r > 0.0) ? col_r : 0.0;
		col_g = (col_g < 1.0) ? col_g : 1.0;
		col_g = (col_g > 0.0) ? col_g : 0.0;
		col_b = (col_b < 1.0) ? col_b : 1.0;
		col_b = (col_b > 0.0) ? col_b : 0.0;

		//if (col != 0 && col != 1) std::cerr << col << std::endl;
		//if (col != col) std::cerr << col << std::endl;

		//if (col >= 0 && col <= 1) 
		sm->vList[i1].color = vec3f((float)col_r, (float)col_g, (float)col_b);
		//else {
		//	std::cerr << col << std::endl;
		//	sm->vList[i1].color = vec3f((float)1, (float)0, (float)0);
		//}
	}


	delete[] perTriangleWeights;

	delete[] rhs_p;
	delete[] x;
}


#endif

