#ifndef TENSOR_3D_THIRD_H
#define TENSOR_3D_THIRD_H


#include <iomanip>


#include "JBS_General.h"
#include "Matrix3D.h"
#include "point3d.h"


//******************************************************************************************
//******************************************************************************************


//! A 3D third level Tensor (can be visualized as a 3x3x3 Volume)
template<class T>
class ThirdLevelTensor3D {

public:

	//! Value-Type of Templates components (i.e. float or double).
	typedef T			ValueType;
	typedef Matrix3D<T>	Matrix;
	typedef point3d<T>	Vector;

//******************************************************************************************

	//! Empty constructor, initializes all entries with zero
	ThirdLevelTensor3D() {
		for (int x = 0; x < 3; x++)
			entries[x] = Matrix();
	}

//******************************************************************************************

	Matrix& operator[](unsigned int i) {
		if (i < 3)
			return entries[i];
		else {
			std::cerr << "Out of bounds access to ThirdLevelTensor3D" << std::endl;
			exit(EXIT_FAILURE);
		}
	}

//******************************************************************************************

	Matrix operator*(Vector vec) {
		return entries[0]*vec.x + entries[1]*vec.y + entries[2]*vec.z;
	}

//******************************************************************************************

	//! Prints the tensor.
	void print() {
		std::cerr << "---- 3D Tensor of third order ----" << std::endl;
		for (int x = 0; x < 3; x++) {
			entries[x].print();
		}
		std::cerr << "----------------------------------" << std::endl;
	};

//******************************************************************************************

private:

	Matrix entries[3];

};

typedef ThirdLevelTensor3D<double> tensor3_3D_d;

#endif
