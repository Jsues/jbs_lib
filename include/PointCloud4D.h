#ifndef POINTCLOUD_4D_H
#define POINTCLOUD_4D_H

#include <vector>
#include <iostream>
#include <fstream>
#include <map>
#include <limits>

#include "point3d.h"
#include "point4d.h"
#include "JBS_General.h"


#ifdef max
	#undef max
#endif


// ****************************************************************************************


//! 4D PointCloud with 4D normals.
template <class T>
class PointCloud4D {

public:

	//! Value-Type of Templates components.
	typedef T ValueType;
	typedef point4d<T> Point;


// ****************************************************************************************


	PointCloud4D() {
		min_p = Point(std::numeric_limits<ValueType>::max());
		max_p = Point(-(std::numeric_limits<ValueType>::max()));
	}


// ****************************************************************************************


	void Read4PNFile(const char* filename, ValueType time_scaler = 1) {
		// Eingabedatei oeffnen
		std::ifstream InFile(filename);

		if (!InFile.is_open()) {
			std::cerr << "PointCloud4D::Read4PNFile(...) : Could not open file: " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		Point p;
		Point n;
		InFile >> p >> n;
		while(InFile.good()) {
			p.w *= time_scaler;
			n.normalize();
			insertPoint(p,n);
			InFile >> p >> n;  // next read
		}

        InFile.close();

		std::cerr << "Read " << (UInt)coords.size() << " points and " << (UInt)normals.size() << " normals" << std::endl;
	}

// ****************************************************************************************


	void Read4PNFileBin(const char* filename, ValueType time_scaler = 1) {

		std::ifstream fin(filename, std::ifstream::binary);

		if (!fin.good()) {
			std::cerr << "Error reading file in 'PointCloud4D<T>::Read4PNFileBin(char* filename)'" << std::endl;
			return;
		}

		char numByteForDT;
		UInt numV;
		// read header
		fin.read((char*)&numByteForDT, 1);
		fin.read((char*)&numV, sizeof(UInt));

		if (numByteForDT != sizeof(ValueType)) {
			std::cerr << "Could not read PointCloud4D, because of following error" << std::endl;
			std::cerr << "The sizeof the datatype is wrong." << std::endl;
			std::cerr << "The File uses " << (UInt)numByteForDT << " Bytes per floating point" << std::endl;
			std::cerr << "The PointCloud uses " << sizeof(ValueType) << " Bytes per floating point" << std::endl;
			std::cerr << "Probably a float/double issue" << std::endl;
			std::cerr << "EXIT" << std::endl;
			exit(EXIT_FAILURE);
		}


		std::cerr << "PC contains " << numV << " points" << std::endl;


		coords.resize(numV);
		normals.resize(numV);

		fin.read((char*)&coords[0], 4*numByteForDT*numV);
		fin.read((char*)&normals[0], 4*numByteForDT*numV);

		fin.close();

		for (UInt i1 = 0; i1 < numV; i1++) {

			Point& p = coords[i1];
			
			p.w *= time_scaler;

			ValueType time = p.w;
			if (timestepmap.find(time) == timestepmap.end()) {
				timestepmap.insert(std::pair<ValueType, UInt>(time, (UInt)i1));
			}

			if (p.x < min_p.x) min_p.x = p.x;
			if (p.y < min_p.y) min_p.y = p.y;
			if (p.z < min_p.z) min_p.z = p.z;
			if (p.w < min_p.w) min_p.w = p.w;
			if (p.x > max_p.x) max_p.x = p.x;
			if (p.y > max_p.y) max_p.y = p.y;
			if (p.z > max_p.z) max_p.z = p.z;
			if (p.w > max_p.w) max_p.w = p.w;

		}

	}

// ****************************************************************************************


	void Write4PNFileBin(const char* filename) {
		// Eingabedatei oeffnen
		std::ofstream fout(filename, std::ios::binary);

		if (!fout.good()) {
			std::cerr << "Error writing file in 'PointCloud4D<T>::Write4PNFileBin(char* filename)'" << std::endl;
			return;
		}

		// write header:
		UInt numV = (UInt)coords.size();
		char numByteForDT = sizeof(ValueType);

		// write header
		fout.write((char*)&numByteForDT, 1);
		fout.write((char*)&numV, sizeof(UInt));

		// write vertices:
		fout.write((char*)&coords[0], 4*numByteForDT*numV);

		// write normals:
		fout.write((char*)&normals[0], 4*numByteForDT*numV);

		fout.close();

	}


// ****************************************************************************************


	void Write4PNFile(const char* filename) {
		// Eingabedatei oeffnen
		std::ofstream OfFile(filename);

		std::cerr << "Writing point cloud to file...";
		if (!OfFile.is_open()) {
			std::cerr << "PointCloud4D::Read4PNFile(...) : Could not write to file: " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		for (UInt i1 = 0; i1 < coords.size(); i1++) {
			OfFile << coords[i1].x << " " << coords[i1].y << " " << coords[i1].z << " " << coords[i1].w << " " << " " << normals[i1].x << " " << normals[i1].y << " " << normals[i1].z << " " << normals[i1].w << " " << std::endl;
		}
		OfFile.close();
		std::cerr << "done" << std::endl;

		std::cerr << "Wrote " << (UInt)coords.size() << " points and " << (UInt)normals.size() << " normals" << std::endl;
	}

	
// ****************************************************************************************


	inline Point* getPoints() {
		return &coords[0];
	}


// ****************************************************************************************


	inline Point* getNormals() {
		return &normals[0];
	}


// ****************************************************************************************


	inline UInt getNumPts() const {
		return (UInt)coords.size();
	}


// ****************************************************************************************


	void insertPoint(Point p, Point n) {
		coords.push_back(p);
		normals.push_back(n);

		ValueType time = p.w;
		if (timestepmap.find(time) == timestepmap.end()) {
			timestepmap.insert(std::pair<ValueType, UInt>(time, (UInt)coords.size()-1));
		}

		if (p.x < min_p.x) min_p.x = p.x;
		if (p.y < min_p.y) min_p.y = p.y;
		if (p.z < min_p.z) min_p.z = p.z;
		if (p.w < min_p.w) min_p.w = p.w;
		if (p.x > max_p.x) max_p.x = p.x;
		if (p.y > max_p.y) max_p.y = p.y;
		if (p.z > max_p.z) max_p.z = p.z;
		if (p.w > max_p.w) max_p.w = p.w;
	}


// ****************************************************************************************

	void recomputeMinMax() {
		if (getNumPts() == 0)
			return;
		
		min_p = max_p = coords[0];
		for (UInt i1 = 1; i1 < getNumPts(); ++i1) {
			const Point& p = coords[i1];
			if (p.x < min_p.x) min_p.x = p.x;
			if (p.y < min_p.y) min_p.y = p.y;
			if (p.z < min_p.z) min_p.z = p.z;
			if (p.w < min_p.w) min_p.w = p.w;
			if (p.x > max_p.x) max_p.x = p.x;
			if (p.y > max_p.y) max_p.y = p.y;
			if (p.z > max_p.z) max_p.z = p.z;
			if (p.w > max_p.w) max_p.w = p.w;
		}
	}

// ****************************************************************************************


	//! Skales the point cloud such that it is contained in the unit cube.
	void normalize() {
		recomputeMinMax();
		Point move = min_p;
		ValueType scale = max_p.x-min_p.x;
		if (scale < max_p.y-min_p.y) scale =  max_p.y-min_p.y;
		if (scale < max_p.z-min_p.z) scale =  max_p.z-min_p.z;
		if (scale < max_p.w-min_p.w) scale =  max_p.w-min_p.w;

		Point disp = max_p-min_p;
		disp /= scale;
		disp /= 2;
		disp -= Point((ValueType)0.5);

		for (UInt i1 = 0; i1 < getNumPts(); ++i1) {
			Point& p = coords[i1];
			p -= min_p;
			p /= scale;
			//p -= disp;
		}
		recomputeMinMax();

		std::cerr << "min: " << min_p << std::endl;
		std::cerr << "max: " << max_p << std::endl;
	}


// ****************************************************************************************


	void getMinMax(Point& min_, Point& max_) {
		min_ = min_p;
		max_ = max_p;
	}


// ****************************************************************************************


	void printTimeSteps() {
		std::cerr << "Number of timesteps: " << (UInt)timestepmap.size() << std::endl;
		for (typename std::map<ValueType, UInt>::iterator it = timestepmap.begin(); it != timestepmap.end(); ++it) {
			std::cerr << "Timestep: " << it->first << " starts at: " << it->second << std::endl;
		}
	}



// ****************************************************************************************


	inline UInt getNumTimeSteps() const {
		return (UInt)timestepmap.size();
	}


// ****************************************************************************************


	inline void getNumRangeOfPointSteps(UInt step, UInt& startIndex, UInt& endIndex) {
		if (step >= getNumTimeSteps()) {
			std::cerr << "PointCloud4D::getNumPointsInTimeStep(...), timestep out of range" << std::endl;
		}

		typename std::map<ValueType, UInt>::iterator it = timestepmap.begin();
		UInt i1 = 0;
		for ( ; it != timestepmap.end(); it++) {
			if (i1 == step) {
				startIndex = it->second;
				break;	
			}
			i1++;
		}
		it++;
		if (it == timestepmap.end()) {
			endIndex = (UInt)coords.size();
		} else {
			endIndex = it->second;
		}
	}


// ****************************************************************************************


	inline UInt getTimeStepStart(UInt step) {

		if (step >= getNumTimeSteps()) {
			std::cerr << "PointCloud4D::getTimeStepStart(...), timestep out of range" << std::endl;
			return 0;
		}

		typename std::map<ValueType, UInt>::iterator it = timestepmap.begin();
		UInt i1 = 0;
		for ( ; it < timestepmap.end(); it++) {
			if (i1 == step) return it->second;
			i1++;
		}

		return 0;
	}


// ****************************************************************************************

	void toggleNormals() {
		if (getNumPts() == 0)
			return;
		
		min_p = max_p = coords[0];
		for (UInt i1 = 1; i1 < getNumPts(); ++i1) {
			Point& n = normals[i1];
			n *= -1;
		}

	}


// ****************************************************************************************
private:


// ****************************************************************************************

	Point min_p;
	Point max_p;

	std::vector< Point > coords;
	std::vector< Point > normals;
	std::map<ValueType, UInt> timestepmap;

};

#endif
