#ifndef GET_CENTERS_H
#define GET_CENTERS_H

#include <set>
#include <map>

#include "PointCloud.h"
#include "ANN/ANN.h"
#include "rng.h"


template <class Point>
std::set<UInt> selectPointsRandomly(PointCloudNormals<Point>* pc1, UInt numPointsToFit, int seed=1) {
	RNG randnum(seed);
	std::set<UInt> selected;
	int numPts = pc1->getNumPts();
	while (selected.size() < numPointsToFit) {
		long tmp = abs(randnum.rand_int31()) % numPts;
		selected.insert((UInt)tmp);
	}
	return selected;
}

template <class Point>
std::set<UInt> selectPointsUniformly(PointCloudNormals<Point>* pc, typename Point::ValueType distance, ANNkd_tree* kdTree = NULL) {

	RNG randnum(1);
	
	std::set<UInt> selected;

	UInt numPts = (UInt)pc->getNumPts();
	int* flag = new int[numPts];

	std::map<UInt, UInt> list;
	UInt dim = Point::dim;

	ANNpointArray xx = NULL;

	if (kdTree == NULL) {
		ANNpointArray dataPts = annAllocPts(numPts, dim);
		xx = dataPts;
		for (UInt i1 = 0; i1 < numPts; i1++) {
			flag[i1] = 1; // initially set all points to be contained
			Point pt = pc->getPoints()[i1];
			for (UInt i2 = 0; i2 < dim; i2++) dataPts[i1][i2] = pt[i2];
			double tmp = randnum.uniform(0.0, 100000000.0);
			UInt tmp_i = (UInt)tmp;
			list.insert(std::pair<UInt, UInt>(tmp_i, i1));
		}
		kdTree = new ANNkd_tree(dataPts, numPts, dim);
	}

	for (UInt i1 = 0; i1 < numPts; i1++) {
		flag[i1] = 1; // initially set all points to be contained
		double tmp = randnum.uniform(0.0, 100000000.0);
		UInt tmp_i = (UInt)tmp;
		list.insert(std::pair<UInt, UInt>(tmp_i, i1));
	}

	ANNpoint queryPt = annAllocPt(dim);
	// Allocate arrays for NN-Search:
	ANNidxArray nnIdx = new ANNidx[numPts];							// allocate near neigh indices
	ANNdistArray dists = new ANNdist[numPts];						// allocate near neighbor dists

	Point::ValueType rad_square = distance*distance;

	for (std::map<UInt, UInt>::const_iterator it = list.begin(); it != list.end(); it++) {
		UInt id = it->second;

		if (flag[id] == 0) // point is already canceled, skip
			continue;

		Point pt = pc->getPoints()[id];
		
		for (UInt i2 = 0; i2 < dim; i2++) queryPt[i2] = pt[i2];
		int num_pts = kdTree->annkFRSearch(queryPt, rad_square, 0);		// Get number of points within radius
		kdTree->annkFRSearch(queryPt, rad_square, num_pts, nnIdx, dists);

		for (UInt i2 = 1; i2 < (UInt)num_pts; i2++) {
			flag[nnIdx[i2]] = 0;
		}
	}

	for (UInt i1 = 0; i1 < numPts; i1++) {
		if (flag[i1] == 1) {
			selected.insert(i1);
		}
	}
	delete[] nnIdx;
	delete[] dists;
	delete[] flag;
	annDeallocPt(queryPt);

	if (xx != NULL) {
		delete kdTree;
		annDeallocPts(xx);
	}

	return selected;
}

#endif
