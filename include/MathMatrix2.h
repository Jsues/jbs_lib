#ifndef MATH_MATRIX_2_H
#define MATH_MATRIX_2_H


#include "point2d.h"
#include "point3d.h"
#include "point3d.h"
#include "nr_templates.h" // for eigenvectors/eigenvalues (jacobi)
#include <algorithm>
#include <iomanip>
#include <vector>

// ****************************************************************************************


//! A n-dimensional Square-Matrix
/*!
	Ordering (for operator()) and set/get:<br>
	0/0 0/1 0/2 ... 0/n<br>
	1/0 1/1 1/2 ... 1/n<br>
	...<br>
	n/0 n/1 n/2 ... n/n<br>
*/
template<class T>
class MathMatrix2 {

public:

	typedef T ValueType;

// ****************************************************************************************

	//! Constructs an empty SquareMatrix.
	MathMatrix2(int n) : dim(n) {
		m_nn = dim*dim;
		m = new ValueType[m_nn];
		memset(m, 0, m_nn*sizeof(ValueType));
	}

// ****************************************************************************************

	//! Copy Constructor.
	MathMatrix2(const MathMatrix2<ValueType>& other) : dim(other.dim), m_nn(other.m_nn) {
		m = new ValueType[m_nn];
		for (UInt i1 = 0; i1 < m_nn; i1++) {
			m[i1] = other.m[i1];
		}
	}

// ****************************************************************************************

	~MathMatrix2() {
		delete[] m;
	}

// ****************************************************************************************

	inline int getDim() const { return dim; }
// ****************************************************************************************

	//! Sets all entries to zero
	void zeroOut() {
		for (UInt i1 = 0; i1 < m_nn; i1++) {
			m[i1] = 0;
		}
	}

// ****************************************************************************************

	//! Overwrites current matrix with the identity matrix.
	inline void setToIdentity() {
		for (UInt i1 = 0; i1 < m_nn; i1++) m[i1] = 0;
		for (UInt i1 = 0; i1 < dim; i1++) (*this)(i1,i1) = 1;
	}

// ****************************************************************************************

	//! Copy the values from the upper left matrix half to the lower right
	inline void symmetrize() {
		for (int i1 = 0; i1 < dim; i1++) {
			for (int i2 = i1+1; i2 < dim; i2++) {
				m[i2*dim+i1] = m[i1*dim+i2];
			}
		}
	}

// ****************************************************************************************

	//! Access element 'i','j'.
	inline const ValueType& operator() (const int i, const int j) const {
		return m[i*dim+j];	
	};

// ****************************************************************************************

	//! Access element 'i','j'.
	inline ValueType& operator() (const int i, const int j) {
		return m[i*dim+j];	
	};

// ****************************************************************************************

	//! Multiplies all entries by the factor 'fac'.
	void operator*=(const ValueType fac) {
		for (UInt i1 = 0; i1 < m_nn; i1++) {
			m[i1]*=fac;
		}
	}

// ****************************************************************************************

	//! returns (*this)-'other'.
	MathMatrix2<ValueType> operator-(const MathMatrix2<ValueType>& other) const {
		MathMatrix2<ValueType> ret(dim);
		for (UInt i1 = 0; i1 < m_nn; i1++) {
			ret.m[i1] = m[i1]-other.m[i1];
		}
		return ret;
	}

// ****************************************************************************************

	//! returns (*this)+'other'.
	MathMatrix2<ValueType> operator+(const MathMatrix2<ValueType>& other) const {
		MathMatrix2<ValueType> ret(dim);
		for (UInt i1 = 0; i1 < m_nn; i1++) {
			ret.m[i1] = m[i1]+other.m[i1];
		}
		return ret;
	}

// ****************************************************************************************

	//! returns (*this)*'fac'.
	MathMatrix2<ValueType> operator*(const ValueType fac) const {
		MathMatrix2<ValueType> ret(dim);
		for (UInt i1 = 0; i1 < m_nn; i1++) {
			ret.m[i1] = m[i1]*fac;
		}
		return ret;
	}

// ****************************************************************************************

	//! Adds 'other' to the current matrix.
	void operator+=(const MathMatrix2<ValueType>& other) {
		for (UInt i1 = 0; i1 < m_nn; i1++) {
			m[i1] += other.m[i1];
		}
	}

// ****************************************************************************************

	//! Matrixmultiplication: returns (*this)*'other'.
	MathMatrix2<ValueType> operator*(const MathMatrix2<ValueType>& other) const {

		MathMatrix2<ValueType> ret(dim);

		for(int i1 = 0; i1 < dim; i1++) {
			for(int i2 = 0; i2 < dim; i2++)	{
				ret(i1,i2) = 0.0;
				for(int i3 = 0; i3 < dim; i3++)
					ret(i1,i2) += (*this)(i1,i3) * other(i3,i2);
				
			}
		}
		return ret;
	}


// ****************************************************************************************

	//! Prints matrix to stderr.
	void print(int precision = 5) const {
		for (int i1 = 0; i1 < dim; i1++) {
			for (int i2 = 0; i2 < dim; i2++) {
				std::cerr << ((m[i1*dim+i2] >= 0) ? " " : "") << std::fixed << std::setprecision(4) << std::setw(4) << m[i1*dim+i2] << "\t";
			}
			std::cerr << std::endl;
		}
	}

// ****************************************************************************************

	//! Prints matrix to stderr.
	void printMaple(int precision = 20, const char* name = "mapleMatrix.txt") const {

		std::ofstream mapfile(name);

		mapfile << "A := Matrix([";
		for (int i1 = 0; i1 < dim; i1++) {
			mapfile << "[";
			for (int i2 = 0; i2 < dim-1; i2++) {
				mapfile << std::fixed << std::setprecision(precision) << m[i1*dim+i2] << ", ";
			}
			mapfile << std::fixed << std::setprecision(precision) << m[i1*dim+dim-1];
			mapfile << "]";
			if (i1 < dim-1) mapfile << ", ";
			mapfile << std::endl;
		}
		mapfile << "]);";

		mapfile.close();
	}

// ****************************************************************************************

	void printQuirin(int precision = 5, const char* name = "qMatrix.txt") const {
		
		std::ofstream file(name);

		file << dim << std::endl;

		for (int i1 = 0; i1 < dim; i1++) {
			for (int i2 = 0; i2 < dim; i2++) {
				file << std::fixed << std::setprecision(precision) << m[i1*dim+i2] << " ";
			}
			file << std::endl;
		}
	}

// ****************************************************************************************

	inline int nrows() const {
		return dim;
	}

// ****************************************************************************************

	inline int ncols() const {
		return dim;
	}

// ****************************************************************************************

	//! Calculates the eigenvalues and eigenvectors of the matrix. Note that the eigensystem is not necessarily right handed!
	ValueType* calcEValuesAndVectors(MathMatrix2<ValueType>& ESystem) {

		// Use jacobi's method:
		// Build dim x dim matrix NR-style:
		float** CV = new float*[dim+1];
		for(int i1 = 0; i1 < dim+1; i1++)
			CV[i1] = new float[dim+1];
		float* lambda = new float[dim+1];
		float** v = new float*[dim+1];
		for(int i1 = 0; i1 < dim+1; i1++)
			v[i1] = new float[dim+1];


		for (int i = 0; i < dim; i++) {
			for (int j = 0; j < dim; j++) {
				CV[i+1][j+1] = (float)(*this)(i,j);
			}
		}

		int num_of_required_jabobi_rotations;

		if (!jacobi(CV, dim, lambda, v, &num_of_required_jabobi_rotations)) {
			std::cerr << "ERROR: could not compute Eigenvectors in SquareMatrixND::calcEValuesAndVectors(...)" << std::endl;
			print();
			std::cerr << "EXIT" << std::endl;
			exit(EXIT_FAILURE);
		}

		ValueType* eigenvalues = new ValueType[dim];
		for (int i = 0; i < dim; i++) {
			for (int j = 0; j < dim; j++) {
				ESystem(i,j) = CV[i+1][j+1];
			}
			eigenvalues[i] = lambda[i+1];
		}

		// clean up
		for(int i1 = 0; i1 < dim+1; i1++) delete[] CV[i1];
		delete[] CV;

		for(int i1 = 0; i1 < dim+1; i1++) delete[] v[i1];
		delete[] v;

		delete[] lambda;

		return eigenvalues;
	}

// ****************************************************************************************

	//! Returns the inverted matrix.
	inline MathMatrix2<ValueType> getInverted() const {

		// Build dim x dim matrix NR-style:
		float** CV = new float*[dim+1];
		for(int i1 = 0; i1 < dim+1; i1++)
			CV[i1] = new float[dim+1];

		for (int i = 0; i < dim; i++) {
			for (int j = 0; j < dim; j++) {
				CV[i+1][j+1] = (float)(*this)(i,j);
			}
		}

		int indx[dim+1];
		ValueType d[dim+1];
		ludcmp(CV, dim, indx, d);

		MathMatrix2<ValueType> InvMat(dim);

		// Get the inverse
		ValueType b[dim+1];
		for (int j = 0; j < dim; j++)
		{
			for (int i = 0; i < dim; i++) b[i+1] = 0.0;
			b[j+1] = 1.0;
			lubksb(CV, dim, indx, b);
			for (int i = 0; i < dim; i++)
				InvMat(i,j) = b[i+1];
		}

		for(int i1 = 0; i1 < dim+1; i1++) delete[] CV[i1];
		delete[] CV;

		return InvMat;
	}

// ****************************************************************************************

	//! Returns the transposed matrix.
	inline MathMatrix2<ValueType> getTransposed() const {

		MathMatrix2<ValueType> TransMat(dim);

		for (int i1 = 0; i1 < dim; i1++) {
			for (int i2 = 0; i2 < dim; i2++) {
				TransMat.m[i1*dim+i2] = m[i2*dim+i1];
			}
		}
		return TransMat;
	}

// ****************************************************************************************

	//! Transposes the matrix in-place.
	inline void transpose() {
		ValueType m2[m_nn];
		for (UInt i1 = 0; i1 < m_nn; i1++) m2[i1] = m[i1];

		for (UInt i1 = 0; i1 < dim; i1++) {
			for (UInt i2 = 0; i2 < dim; i2++) {
				m[i1*dim+i2] = m2[i2*dim+i1];
			}
		}

	}

// ****************************************************************************************

	//! Computes the cholesky factoryzation of the Matrix (Assumes the Matrix is symmetric and positive definite) and stores it in the lower left of the matrix. 'p' is the diagonal part of the matrix.
	bool CHOLESKY_decomp(MathMatrix2<ValueType>& A, ValueType* p) {
		ValueType sum;

		for (int i1 = 0; i1 < dim; i1++) {
			for (int i2 = i1; i2 < dim; i2++) {
				A(i1, i2) = (*this)(i1,i2);
			}
		}

		int k;

		for (int i=0;i<dim;i++) {
			for (int j=i;j<dim;j++) {
				for (sum=A(i,j),k=i-1;k>=0;k--) sum -= A(i,k)*A(j,k);
				if (i == j) {
					if (sum <= 0.0) {
						//std::cerr << "dim: " << dim << "  i: " << i << "  j: " << j << "  sum: " << sum << std::endl;
						//std::cerr << "MathMatrix<T>::CHOLESKY_decomp() failed" << std::endl;
						return false;
					}
					p[i]=sqrt(sum);
				} else {
					A(j,i)=sum/p[i];
				}
			}
		}
		return true;
	}

// ****************************************************************************************

	//! Backsolve Cholesky decomposition.
	void CHOLESKY_bksolve(ValueType* p, const ValueType* b, ValueType* x_result) {
		ValueType sum;
		int k;
		for (int i=0;i<dim;i++) {
			for (sum=b[i],k=i-1;k>=0;k--) sum -= (*this)(i,k)*x_result[k];
			x_result[i]=sum/p[i];
		}
		for (int i=dim-1;i>=0;i--) {
			for (sum=x_result[i],k=i+1;k<dim;k++) sum -= (*this)(k,i)*x_result[k];
			x_result[i]=sum/p[i];
		}
	}

// ****************************************************************************************

	//! Backsolves the SVD-decomposition obtained by 'SVD_decomp(...)'.
	void SVD_bksolve(MathMatrix2<ValueType>& U_m, ValueType* sigma, MathMatrix2<ValueType>& V_m, ValueType* b, ValueType* x_result) {

		int jj,j,i;
		ValueType s;

		int m=dim;
		int n=dim;
		ValueType* tmp = new ValueType[n];
		for (j=0;j<n;j++) {
			s=0.0;
			if (sigma[j] != 0.0) {
				for (i=0;i<m;i++) s += U_m.m[i*dim+j]*b[i];
				s /= sigma[j];
			}
			tmp[j]=s;
		}
		for (j=0;j<n;j++) {
			s=0.0;
			for (jj=0;jj<n;jj++) s += V_m.m[j*dim+jj]*tmp[jj];
			x_result[j]=s;
		}

		delete[] tmp;

	}

// ****************************************************************************************

	//! Computes the SVD-decomposition of the matrix.
	bool SVD_decomp(MathMatrix2<ValueType>& U_m, ValueType* sigma, MathMatrix2<ValueType>& V_m, int maxNumIter = 30) const {

		for (UInt i1 = 0; i1 < m_nn; i1++) {
			U_m.m[i1] = m[i1];
		}

		bool flag;
		int i,its,j,jj,k,l,nm;
		ValueType anorm,c,f,g,h,s,scale,x,y,z;

		int m=dim;
		int n=dim;
		ValueType* rv1 = new ValueType[n];
		g=scale=anorm=0.0;
		for (i=0;i<n;i++) {
			l=i+2;
			rv1[i]=scale*g;
			g=s=scale=0.0;
			if (i < m) {
				for (k=i;k<m;k++) scale += fabs(U_m.m[k*dim+i]);
				if (scale != 0.0) {
					for (k=i;k<m;k++) {
						U_m.m[k*dim+i] /= scale;
						s += U_m.m[k*dim+i]*U_m.m[k*dim+i];
					}
					f=U_m.m[i*dim+i];
					g = -SIGN(sqrt(s),f);
					h=f*g-s;
					U_m.m[i*dim+i]=f-g;
					for (j=l-1;j<n;j++) {
						for (s=0.0,k=i;k<m;k++) s += U_m.m[k*dim+i]*U_m.m[k*dim+j];
						f=s/h;
						for (k=i;k<m;k++) U_m.m[k*dim+j] += f*U_m.m[k*dim+i];
					}
					for (k=i;k<m;k++) U_m.m[k*dim+i] *= scale;
				}
			}
			sigma[i]=scale *g;
			g=s=scale=0.0;
			if (i+1 <= m && i != n) {
				for (k=l-1;k<n;k++) scale += fabs(U_m.m[i*dim+k]);
				if (scale != 0.0) {
					for (k=l-1;k<n;k++) {
						U_m.m[i*dim+k] /= scale;
						s += U_m.m[i*dim+k]*U_m.m[i*dim+k];
					}
					f=U_m.m[i*dim+l-1];
					g = -SIGN(sqrt(s),f);
					h=f*g-s;
					U_m.m[i*dim+l-1]=f-g;
					for (k=l-1;k<n;k++) rv1[k]=U_m.m[i*dim+k]/h;
					for (j=l-1;j<m;j++) {
						for (s=0.0,k=l-1;k<n;k++) s += U_m.m[j*dim+k]*U_m.m[i*dim+k];
						for (k=l-1;k<n;k++) U_m.m[j*dim+k] += s*rv1[k];
					}
					for (k=l-1;k<n;k++) U_m.m[i*dim+k] *= scale;
				}
			}
			anorm=std::max(anorm,(fabs(sigma[i])+fabs(rv1[i])));
		}
		for (i=n-1;i>=0;i--) {
			if (i < n-1) {
				if (g != 0.0) {
					for (j=l;j<n;j++)
						V_m.m[j*dim+i]=(U_m.m[i*dim+j]/U_m.m[i*dim+l])/g;
					for (j=l;j<n;j++) {
						for (s=0.0,k=l;k<n;k++) s += U_m.m[i*dim+k]*V_m.m[k*dim+j];
						for (k=l;k<n;k++) V_m.m[k*dim+j] += s*V_m.m[k*dim+i];
					}
				}
				for (j=l;j<n;j++) V_m.m[i*dim+j]=V_m.m[j*dim+i]=0.0;
			}
			V_m.m[i*dim+i]=1.0;
			g=rv1[i];
			l=i;
		}
		for (i=std::min(m,n)-1;i>=0;i--) {
			l=i+1;
			g=sigma[i];
			for (j=l;j<n;j++) U_m.m[i*dim+j]=0.0;
			if (g != 0.0) {
				g=(ValueType)1.0/g;
				for (j=l;j<n;j++) {
					for (s=0.0,k=l;k<m;k++) s += U_m.m[k*dim+i]*U_m.m[k*dim+j];
					f=(s/U_m.m[i*dim+i])*g;
					for (k=i;k<m;k++) U_m.m[k*dim+j] += f*U_m.m[k*dim+i];
				}
				for (j=i;j<m;j++) U_m.m[j*dim+i] *= g;
			} else for (j=i;j<m;j++) U_m.m[i*dim+k]=0.0;
			++U_m.m[i*dim+i];
		}
		for (k=n-1;k>=0;k--) {
			for (its=0;its<30;its++) {
				flag=true;
				for (l=k;l>=0;l--) {
					nm=l-1;
					if (fabs(rv1[l])+anorm == anorm) {
						flag=false;
						break;
					}
					if (fabs(sigma[nm])+anorm == anorm) break;
				}
				if (flag) {
					c=0.0;
					s=1.0;
					for (i=l-1;i<k+1;i++) {
						f=s*rv1[i];
						rv1[i]=c*rv1[i];
						if (fabs(f)+anorm == anorm) break;
						g=sigma[i];
						h=pythag(f,g);
						sigma[i]=h;
						h=(ValueType)1.0/h;
						c=g*h;
						s = -f*h;
						for (j=0;j<m;j++) {
							y=U_m.m[j*dim+nm];
							z=U_m.m[j*dim+i];
							U_m.m[j*dim+nm]=y*c+z*s;
							U_m.m[j*dim+i]=z*c-y*s;
						}
					}
				}
				z=sigma[k];
				if (l == k) {
					if (z < 0.0) {
						sigma[k] = -z;
						for (j=0;j<n;j++) V_m.m[j*dim+k] = -V_m.m[j*dim+k];
					}
					break;
				}
				if (its == maxNumIter-1) {
					std::cerr << "MathMatrix2::SVD_decomp(...): no convergence in " << maxNumIter << " svdcmp iterations" << std::endl;
					delete[] rv1;
					return false;
				}
				x=sigma[l];
				nm=k-1;
				y=sigma[nm];
				g=rv1[nm];
				h=rv1[k];
				f=((y-z)*(y+z)+(g-h)*(g+h))/((ValueType)2.0*h*y);
				g=pythag<ValueType>(f,(ValueType)1.0);
				f=((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;
				c=s=1.0;
				for (j=l;j<=nm;j++) {
					i=j+1;
					g=rv1[i];
					y=sigma[i];
					h=s*g;
					g=c*g;
					z=pythag(f,h);
					rv1[j]=z;
					c=f/z;
					s=h/z;
					f=x*c+g*s;
					g=g*c-x*s;
					h=y*s;
					y *= c;
					for (jj=0;jj<n;jj++) {
						x=V_m.m[jj*dim+j];
						z=V_m.m[jj*dim+i];
						V_m.m[jj*dim+j]=x*c+z*s;
						V_m.m[jj*dim+i]=z*c-x*s;
					}
					z=pythag(f,h);
					sigma[j]=z;
					if (z) {
						z=(ValueType)1.0/z;
						c=f*z;
						s=h*z;
					}
					f=c*g+s*y;
					x=c*y-s*g;
					for (jj=0;jj<m;jj++) {
						y=U_m.m[jj*dim+j];
						z=U_m.m[jj*dim+i];
						U_m.m[jj*dim+j]=y*c+z*s;
						U_m.m[jj*dim+i]=z*c-y*s;
					}
				}
				rv1[l]=0.0;
				rv1[k]=f;
				sigma[k]=x;
			}
		}

		delete[] rv1;

		return true;
	}

// ****************************************************************************************

	//! Backsolbe LU decomposition
	static void solvebksb(const MathMatrix2<ValueType>& A, const std::vector<int>& indx, T* b) {
		int i,ii=0,ip,j;
		T sum;

		for (i=0;i<A.dim;i++) {
			ip=indx[i];
			sum=b[ip];
			b[ip]=b[i];
			if (ii != 0)
			for (j=ii-1;j<i;j++) sum -= A(i,j)*b[j];
			else if (sum != 0.0)
			ii=i+1;
			b[i]=sum;
		}
		for (i=A.dim-1;i>=0;i--) {
			sum=b[i];
			for (j=i+1;j<A.dim;j++) sum -= A(i,j)*b[j];
			b[i]=sum/A(i,i);
		}
	};

// ****************************************************************************************

	//! Compute the LU decomposition.
	void decompLU(MathMatrix2<ValueType>& A, std::vector<int>& indx) {

		T my_TINY = T(1.0e-20);

		int i, imax, j, k;
		T big, dum, sum, temp;
		T d;

		for (UInt i1 = 0; i1 < m_nn; i1++)
			A.m[i1] = m[i1];

		// Set size of permutation array
		indx.resize(dim);

		// Auxiliariy arrays
		T* vv = new T[dim];

		// Compute LU decomposition
		d=1.0;
		for (i = 0; i < dim; i++) {
			big = 0.0;
			for (j = 0; j < dim; j++)	{
				if ((temp = fabs(A(i,j))) > big)
					big = temp;
			}
			if (big == 0.0) {
				std::cerr << "MathMatrix2::decompLU(): Singular matrix in routine ludcmp" << std::endl;
				print();
				exit(EXIT_FAILURE);
			}
			vv[i] = T(1.0)/big;
		}
		for (j = 0; j < dim; j++) {
			for (i = 0; i < j; i++) {
				sum = A(i,j);
					for (k = 0; k < i; k++)
						sum -= A(i,k)*A(k,j);
				A(i,j) = sum;
			}
			big=0.0;
			for (i = j; i < dim; i++) {
				sum = A(i,j);
				for (k = 0; k < j; k++)
					sum -= A(i,k)*A(k,j);
				A(i,j) = sum;
				if ((dum = vv[i]*fabs(sum)) >= big) {
					big = dum;
					imax = i;
				}
			}
			if (j != imax) {
				for (k = 0; k < dim; k++) {
					dum=A(imax,k);
					A(imax,k) = A(j,k);
					A(j,k) = dum;
				}
				d = -d;
				vv[imax] = vv[j];
			}
			indx[j] = imax;
			if (A(j,j) == 0.0)
				A(j,j) = my_TINY;
			if (j != dim-1) {
				dum=T(1.0)/(A(j,j));
				for (i = j+1; i < dim; i++)
					A(i,j) *= dum;
			}
		}
		delete[] vv;
	};


// ****************************************************************************************

	//! Returns Matrix*Vector.
	inline ValueType* vecTrans(const ValueType* pt) const {
		ValueType* ret = new ValueType[dim];

		for (int i1 = 0; i1 < dim; i1++) {
			ret[i1] = 0;
			for (int i2 = 0; i2 < dim; i2++) {
				ret[i1] += m[i1*dim+i2]*pt[i2];
			}
		}
		return ret;
	}

// ****************************************************************************************

	//! Dataarray.
	ValueType* m;

private:

	int dim;
	UInt m_nn;

};


// ****************************************************************************************


#endif
