#ifndef GET_OFF_SURFACE_POINTS_H
#define GET_OFF_SURFACE_POINTS_H

#include "PointCloud.h"
#include "rbf.h"
#include "getCenters.h"
#include "ANN/ANN.h"

static const UInt numPointsForClosestPointsTest = 6;


template <class T>
int mySignum(T x) {
	if (x > 0) return 1;
	else return -1;
}


//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************


//! Computes the 'numPointsForClosestPointsTest' closest points 'pi' to 'pt' in 'pc'. If 'pt' lies on the same side of all 'pi' it is valid and 'dist' is the distance to the normal-plane of the closest point.
template <class Point>
bool isValidOffSurfacePoint(const PointCloudNormals<Point>* pc, const Point& pt, typename Point::ValueType& dist, Point& dir, ANNkd_tree* kdTree) {

	UInt dim = Point::dim;

	ANNpoint queryPt = annAllocPt(dim);
	for (UInt i1 = 0; i1 < dim; i1++) queryPt[i1] = pt[i1];
	// Allocate arrays for NN-Search:
	ANNidxArray nnIdx = new ANNidx[numPointsForClosestPointsTest];							// allocate near neigh indices
	ANNdistArray dists = new ANNdist[numPointsForClosestPointsTest];						// allocate near neighbor dists

	kdTree->annkSearch(queryPt, numPointsForClosestPointsTest, nnIdx, dists);
	int nnId = nnIdx[0];

	UInt numValids = 0;
	dist = (pt-pc->getPoints()[nnId])|(pc->getNormals()[nnId]);
	dir = (pt-pc->getPoints()[nnId]);
	//dir *= dist;
	dir.normalize();
	if (fabs((pt-pc->getPoints()[nnId]).getNormalized()|(pc->getNormals()[nnId])) > 0.8)
		numValids++;

	int sign = mySignum(dist);
	for (UInt i1 = 1; i1 < numPointsForClosestPointsTest; i1++) {
		nnId = nnIdx[i1];
		Point::ValueType d = (pt-pc->getPoints()[nnId])|(pc->getNormals()[nnId]);
		if (fabs((pt-pc->getPoints()[nnId]).getNormalized()|(pc->getNormals()[nnId])) > 0.8)
			numValids++;

		if (mySignum(d) != sign) {
			delete[] nnIdx;
			delete[] dists;
			annDeallocPt(queryPt);
			return false;
		}
	}


	delete[] nnIdx;
	delete[] dists;
	annDeallocPt(queryPt);

	if (numValids < 4) return false;

	return true;
}


//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************


template <class Point>
RBFFunctionSamples<Point>* getFunctionSamplesNoOffSurfacePoints(const PointCloudNormals<Point>* pc) {
	RBFFunctionSamples<Point>* fs = new RBFFunctionSamples<Point>;
	fs->numNonZeroSamples = (UInt)0;
	//! Add zero constraints
	for (UInt i1 = 0; i1 < (UInt)pc->getNumPts(); i1++) {
		fs->positions.push_back(pc->getPoints()[i1]);
		fs->directions.push_back(pc->getNormals()[i1]);
	}

	return fs;
}


//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************


template <class Point>
RBFFunctionSamples<Point>* getFunctionSamples(const PointCloudNormals<Point>* pc, typename Point::ValueType offSurfacePointsSpacing = 0.05, typename Point::ValueType offSurfacePtDistance = 0.02, bool addUniformSamples = true, ANNkd_tree* kdTree = NULL) {
	RBFFunctionSamples<Point>* fs = new RBFFunctionSamples<Point>;

	UInt numPts = (UInt)pc->getNumPts();
	static const UInt dim = Point::dim;

	ANNpointArray dataPts = NULL;
	// Build kd-Tree of point-cloud
	if (kdTree == NULL) {
		dataPts = annAllocPts(numPts, dim);
			for (UInt i1 = 0; i1 < numPts; i1++) {
				Point pt = pc->getPoints()[i1];
				for (UInt i2 = 0; i2 < dim; i2++) dataPts[i1][i2] = pt[i2];
			}
		kdTree = new ANNkd_tree(dataPts, numPts, dim);
	}

	std::set<UInt> constrainsIDs = selectPointsUniformly(pc, offSurfacePointsSpacing, kdTree);
	fs->positions.reserve(constrainsIDs.size() + numPts);
	fs->values.reserve(constrainsIDs.size());

	//std::cerr << "Number of constraints: " << constrainsIDs.size() << std::endl;

	for (std::set<UInt>::iterator it = constrainsIDs.begin(); it != constrainsIDs.end(); it++) {
		UInt index = *it;
		Point pt = pc->getPoints()[index];
		Point n = pc->getNormals()[index];
		Point osp = pt + n*offSurfacePtDistance;

		// Test if point is valid:
		Point::ValueType dist=0;
		Point dir;
		if (isValidOffSurfacePoint(pc, osp, dist, dir, kdTree)) {
			fs->positions.push_back(osp);
			fs->values.push_back(dist);
			fs->directions.push_back(dir);
		}
		offSurfacePtDistance *= -1;
	}

	if (addUniformSamples) {
		Point minV = pc->getMin();
		Point maxV = pc->getMax();
		Point diag = maxV-minV;
		diag *= (Point::ValueType)0.02;
		minV -= diag;
		maxV += diag;

		UInt numSamplesPerDim = 10;
		UInt numSamples = numSamplesPerDim;
		for (UInt i1 = 0; i1 < dim-1; i1++) numSamples *= numSamplesPerDim;
		std::cerr << "numSamples: " << numSamples << std::endl;

		Point uvw;
		Point osp;
		for (UInt i1 = 0; i1 < numSamples; i1++) {
			int x = i1;
			for (UInt i2 = 0; i2 < dim; i2++) {
				uvw[i2] = (Point::ValueType)(x%numSamplesPerDim);
				x = (x-(int)uvw[i2])/numSamplesPerDim;
				uvw[i2] /= (numSamplesPerDim-1);
			}
			for (UInt i2 = 0; i2 < dim; i2++) osp[i2] = minV[i2]*uvw[i2]+maxV[i2]*(1-uvw[i2]);
			// Test if point is valid:
			Point::ValueType dist=0;
			Point dir;
			if (isValidOffSurfacePoint(pc, osp, dist, dir, kdTree)) {
				fs->positions.push_back(osp);
				fs->values.push_back(dist);
				fs->directions.push_back(dir);
			}
		}
	}


	fs->numNonZeroSamples = (UInt)fs->values.size();

	//! Add zero constraints
	for (UInt i1 = 0; i1 < numPts; i1++) {
		fs->positions.push_back(pc->getPoints()[i1]);
		fs->directions.push_back(pc->getNormals()[i1]);
	}

	// Clean up
	delete kdTree;
	if (dataPts != NULL)
		annDeallocPts(dataPts);


	return fs;
}


//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************


//template <class Point>
//RBFFunctionSamples<Point>* computeImprovedFunctionSamples(RBFFunctionSamples<Point>* oldFS, const RBFStack<Point>& rbf_stack) {
//	RBFFunctionSamples<Point>* newFS = new RBFFunctionSamples<Point>;
//
//	UInt numRBFs = rbf_stack.getNumRBFs();
//	Point grad;
//
//	for (UInt i1 = 0; i1 < oldFS->positions.size(); i1++) {
//		Point pos = oldFS->positions[i1];
//		Point::ValueType val = rbf_stack.eval(pos, grad);
//		double error = oldFS->getFunctionValue(i1)-val;
//		newFS->positions.push_back(pos);
//		newFS->values.push_back(error);
//	}
//	newFS->numNonZeroSamples = (UInt)newFS->values.size();
//
//	return newFS;
//}


//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

#endif
