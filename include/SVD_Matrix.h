#ifndef SVD_MATRIX_H
#define SVD_MATRIX_H


#include "nr_templates.h"


template<class ValueType>
class SVDMatrix {
public:

	SVDMatrix(const SVDMatrix& other) : n(other.n), m(other.m) {
		mat	= new ValueType*[n+1];
		U	= new ValueType*[n+1];
		for (int i1 = 0; i1 < n+1; i1++) {
			mat[i1] = new ValueType[m+1];
			for (int i2 = 0; i2 < m+1; i2++) {
				mat[i1][i2] = other.mat[i1][i2];
			}
			U[i1] = new ValueType[m+1];
		}
		sigma = new ValueType[(m>n)?m+1:n+1];
		Vt = new ValueType*[m+1];
		for (int i1 = 0; i1 < m+1; i1++) Vt[i1] = new ValueType[m+1];
	}

	SVDMatrix(int n_, int m_) : n(n_), m(m_) {
		mat	= new ValueType*[n+1];
		U	= new ValueType*[n+1];
		for (int i1 = 0; i1 < n+1; i1++) {
			mat[i1] = new ValueType[m+1];
			U[i1] = new ValueType[m+1];
		}
		sigma = new ValueType[(m>n)?m+1:n+1];
		Vt = new ValueType*[m+1];
		for (int i1 = 0; i1 < m+1; i1++) Vt[i1] = new ValueType[m+1];
	}
	
// ****************************************************************************************

	~SVDMatrix(){
		for (int i1 = 0; i1 < n+1; i1++) {
			delete[] mat[i1];
			delete[] U[i1];
		}
		for (int i1 = 0; i1 < m+1; i1++) {
			delete[] Vt[i1];
		}
		delete[] mat;
		delete[] U;
		delete[] sigma;
		delete[] Vt;
	}
	
// ****************************************************************************************

	void zeroOutElements() {
		for (int i1 = 1; i1 <= n; i1++) {
			for (int i2 = 1; i2 <= m; i2++) {
				mat[i1][i2] = 0;
			}
		}
	}

// ****************************************************************************************

	std::vector<ValueType> multiply(const std::vector<ValueType>& rhs) const {
		
		if (rhs.size() != m) {
			std::cerr << "Wrong dimension in SVDMatrix::multiply" << std::endl;
			exit(1);
		}

		std::vector<ValueType> res;

		for (int i1 = 1; i1 <= n; i1++) {
			ValueType val = 0;
			for (int i2 = 1; i2 <= m; i2++) {
				val += mat[i1][i2] * rhs[i2-1];
			}
			res.push_back(val);
		}
		return res;
	}

// ****************************************************************************************

	void printWithRHS(const std::vector<ValueType>& rhs) {
		for (int i1 = 1; i1 <= n; i1++) {
			for (int i2 = 1; i2 <= m; i2++) {
				std::cerr << mat[i1][i2] << " \t ";
			}
			std::cerr << " \t = " << rhs[i1] << " \t ";
			std::cerr << std::endl;
		}
	}

// ****************************************************************************************

	void print() {
		for (int i1 = 1; i1 <= n; i1++) {
			for (int i2 = 1; i2 <= m; i2++) {
				std::cerr << mat[i1][i2] << " \t ";
			}
			std::cerr << std::endl;
		}
	}

// ****************************************************************************************

	SVDMatrix<ValueType> computePseudoInverse(ValueType eps = (ValueType)0.00001) {
		computeSVD();

		for (int i1 = 0; i1 <= ((m>n)?m:n); i1++) {
			if (sigma[i1] < eps) sigma[i1] = 0;
		}

		ValueType** Ut = new ValueType*[m+1];
		for (int i1 = 0; i1 < m+1; i1++) {
			Ut[i1] = new ValueType[n+1];
			for (int i2 = 0; i2 < n+1; i2++) {
				Ut[i1][i2] = U[i2][i1]/sigma[i1];
			}
		}

		SVDMatrix<ValueType> PseudoInverse(m,n);
		for (int x = 1; x <= m; x++) {
			for (int y = 1; y <= n; y++) {
				ValueType val = 0;
				for (int i = 1; i <= m; i++) {
					val += Vt[x][i]*Ut[i][y];
				}
				PseudoInverse(x-1,y-1) = val;
			}
		}

		return PseudoInverse;
	}

// ****************************************************************************************

	void computeSVD() {

		// copy 'mat' into 'U'
		for (int i1 = 1; i1 <= n; i1++) for (int i2 = 1; i2 <= m; i2++) U[i1][i2] = mat[i1][i2];

		svdcmp(U, n, m, sigma, Vt);

		//std::cerr << "----" << std::endl;
		//std::cerr << "----" << std::endl;
		//for (int i1 = 1; i1 <= n; i1++) {
		//	for (int i2 = 1; i2 <= m; i2++) std::cerr << U[i1][i2] << " \t ";
		//	std::cerr << std::endl;
		//}

		//std::cerr << "----" << std::endl;
		//for (int i1 = 1; i1 <= ((m < n) ? m : n); i1++) std::cerr << sigma[i1] << " ";
		//std::cerr << std::endl;
		//std::cerr << "----" << std::endl;
		//for (int i1 = 1; i1 <= m; i1++) {
		//	for (int i2 = 1; i2 <= m; i2++) std::cerr << Vt[i1][i2] << " \t ";
		//	std::cerr << std::endl;
		//}
		//std::cerr << "----" << std::endl;
		//std::cerr << "----" << std::endl;

	}

// ****************************************************************************************

	void backSolveSVD(ValueType* b, ValueType* x, ValueType eps = (ValueType)0.00001) {

		for (int i1 = 0; i1 <= ((m>n)?m:n); i1++) {
			if (sigma[i1] < eps) sigma[i1] = 0;
		}

		svbksb(U, sigma, Vt, n, m, b, x);
		//for (int i1 = 1; i1 <= m; i1++) std::cerr << x[i1] << " ";
		//std::cerr << std::endl;
	}

// ****************************************************************************************

	//! Access element 'i','j'.
	inline const ValueType& operator() (const int i, const int j) const {
		return m[i+1][j+1];	
	};

// ****************************************************************************************

	//! Access element 'i','j'.
	inline ValueType& operator() (const int i, const int j) {
		return mat[i+1][j+1];	
	};

// ****************************************************************************************

private:
	ValueType** mat;
	ValueType** U;
	ValueType*  sigma;
	ValueType** Vt;
	int m;
	int n;
};

#endif
