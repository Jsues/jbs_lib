#ifndef RGB_IMAGE_JBS_H
#define RGB_IMAGE_JBS_H

typedef unsigned int UInt;

#include <cassert>
#include <fstream>

#include "point3d.h"
#include "Image.h"
#include "Matrix3D.h"
#include "swapByteOrder.h"


struct sgiHeader {
	short	magic;
	char	storage;
	char	bpc;
	unsigned short dim;
	unsigned short xsize;
	unsigned short ysize;
	unsigned short zsize;
	int		pixmin;
	int		pixmax;
	int		dummy;
	char	info[80];
	int		colormap;
	char	dummy2[404];

	sgiHeader(unsigned short x, unsigned short y) {
		magic = swapByteOrder((short)474);
		storage = 0;
		bpc = 1;
		dim = swapByteOrder((unsigned short)3);
		xsize = swapByteOrder((unsigned short)x);
		ysize = swapByteOrder((unsigned short)y);
		zsize = swapByteOrder((unsigned short)3);
		pixmin = swapByteOrder(0);
		pixmax = swapByteOrder(255);
		colormap = 0;
		for (int i = 0; i < 80; i++) info[i] = 0;
		for (int i = 0; i < 404; i++) dummy2[i] = 0;
	}
};


typedef point3d<unsigned char> rgb_triple;

rgb_triple rgb_to_yuv(rgb_triple rgb) {

	Matrix3D<double> trans( 0.299, 0.587, 0.114,
							-0.14713, -0.28886, 0.436,
							0.615, -0.51499, -0.10001 );
	vec3d rgb_d(rgb.x, rgb.y, rgb.z);
	vec3d yuv_d = trans.vecTrans(rgb_d);

	rgb_triple yuv;
	yuv.x = (unsigned char)yuv_d.x;
	yuv.y = (unsigned char)((yuv_d.y+111.18)/0.872);
	yuv.z = (unsigned char)((yuv_d.z+156.825)/1.23);

	return yuv;
}


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

//! An RGB-Image, mainly an 2D Array of char's that can read and write .raw files.
class RGBImage
{
public:


// ****************************************************************************************

	//! Constructor.
	RGBImage() {
		m_data = NULL;
	}


// ****************************************************************************************

	//! Constructor.
	RGBImage(UInt dim_x, UInt dim_y) {
		m_dim_x = dim_x;
		m_dim_y = dim_y;
		m_dim = m_dim_x*m_dim_y;

		m_data = new rgb_triple[m_dim];

	}

// ****************************************************************************************

	void resize(UInt dim_x, UInt dim_y) {

		std::cerr << "Resize image" << std::endl;

		if (m_data != NULL) delete[] m_data;

		m_dim_x = dim_x;
		m_dim_y = dim_y;
		m_dim = m_dim_x*m_dim_y;

		m_data = new rgb_triple[m_dim];

	}

// ****************************************************************************************

	//! Destructor.
	~RGBImage(void) {
		if (m_data == NULL) delete[] m_data;
	}

// ****************************************************************************************

	void initWithCol(rgb_triple col) {
		for (unsigned int i1 = 0; i1 < m_dim; i1++)
			m_data[i1] = col;
	}

// ****************************************************************************************

	//! Loads an image from a .RAW file.
	bool readRAW(const char* filename, UInt dim_x, UInt dim_y) {

		std::fstream FileBin(filename, std::ios::in|std::ios::binary); 
		if (!FileBin.is_open()) {
			std::cerr << "RGBImage::readRAW(..): Could not read file " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		m_dim_x = dim_x;
		m_dim_y = dim_y;
		m_dim = m_dim_x*m_dim_y;

		// Check size:
		FileBin.seekg( 0, std::ios::end );
		long fileSize = (long)FileBin.tellg();
		if ((fileSize/3) != m_dim) {
			std::cerr << "RGBImage::readRAW(..): Image " << filename << " has wrong size" << std::endl;
			return false;
		}

		if (m_data == NULL) delete[] m_data;

		std::cerr << "File is " << fileSize << " Bytes" << std::endl;
		FileBin.seekg( 0, std::ios::beg );

		m_data = new rgb_triple[m_dim];
		FileBin.read((char*)m_data, fileSize);

		FileBin.close();

		return true;
	}

// ****************************************************************************************

	RGBImage* toYUV() {

		RGBImage* yuv_image = new RGBImage(m_dim_x, m_dim_y);

		for (UInt i1 = 0; i1 < m_dim; i1++) {
			rgb_triple yuv = rgb_to_yuv(m_data[i1]);
			yuv_image->m_data[i1] = yuv;
		}

		return yuv_image;
	}

// ****************************************************************************************

	//! Returns a component of the RGB triple as B/W-Image
	Image* getComponent(UInt i) {
		if (i >= 3) {
			std::cerr << "RGBImage::getComponent(..): Funny guy, use either component 0=red, 1=green or 2=blue" << std::endl << "EXIT" << std::endl;
			exit(EXIT_FAILURE);
		}

		Image* newImg = new Image(m_dim_x, m_dim_y);

		for (UInt i1 = 0; i1 < m_dim; i1++) {
			//std::cerr << (int) m_data[i1].x << " " << (int) m_data[i1].y << " " << (int) m_data[i1].z << std::endl;
			newImg->m_data[i1] = m_data[i1][i];
		}

		return newImg;
	}

// ****************************************************************************************

	//! Writes the image to a .RAW file.
	bool writeRAW(const char* filename) {
		std::fstream FileBin(filename, std::ios::out|std::ios::binary); 
		if (!FileBin.is_open()) {
			std::cerr << "Could not write file " << filename << std::endl;
			return false;
		}

		std::cerr << "Wrote file " << filename << " dimx: " << m_dim_x << " dimy: " << m_dim_y << std::endl;
		FileBin.write((char*)m_data, m_dim*3);
		FileBin.close();

		return true;
	}

// ****************************************************************************************

	//! Writes the image to a .PPM file.
	bool writeSGI(const char* filename) {

		sgiHeader sgiHead(m_dim_x, m_dim_y);

		//std::cerr << std::endl <<  "Sizeof Header: " << sizeof(sgiHeader) << std::endl;

		std::ofstream FileOut(filename, std::ios::binary); 
		FileOut.write((char*)&sgiHead, sizeof(sgiHeader));
		for (int channel = 0; channel < 3; channel++) {
			for (unsigned int y = 0; y < m_dim_y; y++) {
				for (unsigned int x = 0; x < m_dim_x; x++) {
					FileOut.write((char*)&m_data[pos(x,y)][channel], 1);
				}
			}
		}
		
		FileOut.close();

		return true;
	}

// ****************************************************************************************

	////! Writes the image to a .TIF file.
	//bool writeTIF(const char* filename) {

	//	/* Write the header */
	//	FILE* fptr = fopen(filename, "wb");
	//	fprintf(fptr,"4d4d002a");    /* Little endian & TIFF identifier */
	//	int offset = m_dim_x * m_dim_y * 3 + 8;
	//	putc((offset & 0xff000000) / 16777216,fptr);
	//	putc((offset & 0x00ff0000) / 65536,fptr);
	//	putc((offset & 0x0000ff00) / 256,fptr);
	//	putc((offset & 0x000000ff),fptr);


	//	fwrite((void*)m_data, 1, 3*m_dim, fptr);

	///* Write the footer */
	//fprintf(fptr,"000e");  /* The number of directory entries (14) */

	///* Width tag, short int */
	//fprintf(fptr,"0100000300000001");
	//fputc((m_dim_x & 0xff00) / 256,fptr);    /* Image width */
	//fputc((m_dim_x & 0x00ff),fptr);
	//fprintf(fptr,"0000");

	///* Height tag, short int */
	//fprintf(fptr,"0101000300000001");
	//fputc((m_dim_y & 0xff00) / 256,fptr);    /* Image height */
	//fputc((m_dim_y & 0x00ff),fptr);
	//fprintf(fptr,"0000");

	///* Bits per sample tag, short int */
	//fprintf(fptr,"0102000300000003");
	//offset = m_dim_x * m_dim_y * 3 + 182;
	//putc((offset & 0xff000000) / 16777216,fptr);
	//putc((offset & 0x00ff0000) / 65536,fptr);
	//putc((offset & 0x0000ff00) / 256,fptr);
	//putc((offset & 0x000000ff),fptr);

	///* Compression flag, short int */
	//fprintf(fptr,"010300030000000100010000");

	///* Photometric interpolation tag, short int */
	//fprintf(fptr,"010600030000000100020000");

	///* Strip offset tag, long int */
	//fprintf(fptr,"011100040000000100000008");

	///* Orientation flag, short int */
	//fprintf(fptr,"011200030000000100010000");

	///* Sample per pixel tag, short int */
	//fprintf(fptr,"011500030000000100030000");

	///* Rows per strip tag, short int */
	//fprintf(fptr,"0116000300000001");
	//fputc((m_dim_y & 0xff00) / 256,fptr);
	//fputc((m_dim_y & 0x00ff),fptr);
	//fprintf(fptr,"0000");

	///* Strip byte count flag, long int */
	//fprintf(fptr,"0117000400000001");
	//offset = m_dim_x * m_dim_y * 3;
	//putc((offset & 0xff000000) / 16777216,fptr);
	//putc((offset & 0x00ff0000) / 65536,fptr);
	//putc((offset & 0x0000ff00) / 256,fptr);
	//putc((offset & 0x000000ff),fptr);

	///* Minimum sample value flag, short int */
	//fprintf(fptr,"0118000300000003");
	//offset = m_dim_x * m_dim_y * 3 + 188;
	//putc((offset & 0xff000000) / 16777216,fptr);
	//putc((offset & 0x00ff0000) / 65536,fptr);
	//putc((offset & 0x0000ff00) / 256,fptr);
	//putc((offset & 0x000000ff),fptr);

	///* Maximum sample value tag, short int */
	//fprintf(fptr,"0119000300000003");
	//offset = m_dim_x * m_dim_y * 3 + 194;
	//putc((offset & 0xff000000) / 16777216,fptr);
	//putc((offset & 0x00ff0000) / 65536,fptr);
	//putc((offset & 0x0000ff00) / 256,fptr);
	//putc((offset & 0x000000ff),fptr);

	///* Planar configuration tag, short int */
	//fprintf(fptr,"011c00030000000100010000");

	///* Sample format tag, short int */
	//fprintf(fptr,"0153000300000003");
	//offset = m_dim_x * m_dim_y * 3 + 200;
	//putc((offset & 0xff000000) / 16777216,fptr);
	//putc((offset & 0x00ff0000) / 65536,fptr);
	//putc((offset & 0x0000ff00) / 256,fptr);
	//putc((offset & 0x000000ff),fptr);

	///* End of the directory entry */
	//fprintf(fptr,"00000000");

	///* Bits for each colour channel */
	//fprintf(fptr,"000800080008");

	///* Minimum value for each component */
	//fprintf(fptr,"000000000000");

	///* Maximum value per channel */
	//fprintf(fptr,"00ff00ff00ff");

	///* Samples per pixel for each channel */
	//fprintf(fptr,"000100010001");

	//return true;
	//}


// ****************************************************************************************

	//! Writes the image to a .PPM file.
	bool writePPM(const char* filename) {

		std::ofstream FileOut(filename); 
		FileOut << "P3\n";
		FileOut << "# generated by JBSlib SVG tools\n";
		FileOut << m_dim_x << " " << m_dim_y << "\n";
		FileOut << "256\n";

		for (UInt i2 = 0; i2 < m_dim_y; i2++) {
			for (UInt i1 = 0; i1 < m_dim_x; i1++) {
				rgb_triple pt = m_data[pos(i1, i2)];
				FileOut << (int)pt.x << " " << (int)pt.y << " " << (int)pt.z << std::endl;
			}
		}
		FileOut.close();

		return true;
	}

// ****************************************************************************************

// ****************************************************************************************

	//! Returns the pixel at ('x', 'y').
	rgb_triple getPixel(UInt x, UInt y) {
#ifdef _DEBUG
		assert(pixel_exists(x,y));
#endif
		return m_data[pos(x,y)];
	}

// ****************************************************************************************

	//! Sets the pixel at ('x', 'y').
	void setPixel(UInt x, UInt y, rgb_triple col) {
#ifdef _DEBUG
		assert(pixel_exists(x,y));
#endif
		m_data[pos(x,y)] = col;
	}

// ****************************************************************************************

	//! Returns the picture dimension in x-direction.
	const inline UInt getDimX() {
		return m_dim_x;
	}

// ****************************************************************************************

	//! Returns the picture dimension in y-direction.
	const inline UInt getDimY() {
		return m_dim_y;
	}

// ****************************************************************************************

	//! Tests if pixel ('x', 'y') exists.
	const inline bool pixel_exists(UInt x, UInt y) {
		if (x < m_dim_x && y < m_dim_y) return true;
		return false;
	}

// ****************************************************************************************

	//! Returns the position of the pixel ('x', 'y') in the 1D array m_data.
	const inline UInt pos(UInt x, UInt y) {
		return y*m_dim_x+x;
	}

// ****************************************************************************************

	rgb_triple* m_data;

protected:
	UInt m_dim_x;
	UInt m_dim_y;
	//! Total dimension = dim_x * dim_y;
	UInt m_dim;

};

#endif
