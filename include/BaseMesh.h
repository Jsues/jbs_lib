#ifndef BASE_MESH_H
#define BASE_MESH_H


#include <vector>
#include "point3d.h"


class TrivialTriangle {

public:
	TrivialTriangle(int v0_, int v1_, int v2_) {
		v0 = v0_;
		v1 = v1_;
		v2 = v2_;
	}

	int v0, v1, v2;
};



// A very simple mesh class
class BaseMesh
{

public:
	BaseMesh() {};
	~BaseMesh() {};


	//! Inserts a Triangle with the given indicees.
	inline void insertTriangle(int v0, int v1, int v2) {
		tris.push_back(TrivialTriangle(v0, v1, v2));
	}

	//! Inserts a Vertex.
	inline void insertVertex(double x, double y, double z) {
		points.push_back(vec3d(x,y,z));
	}

	inline int getNumT() {
		return (int)tris.size();
	};

	inline int getNumV() {
		return (int)points.size();
	};

	//! Recomputes all vertices as pt_new = (pt+move)*scale;
	inline void resize(double scale, vec3d move) {
		for (int i1 = 0; i1 < getNumV(); i1++) {
			points[i1] = (points[i1]+move)*scale;
		}
	}


	std::vector<vec3d> points;
	std::vector<TrivialTriangle> tris;

private:
};

#endif
