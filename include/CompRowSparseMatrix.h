#ifndef JBS_COMP_ROW_SPARSE_MATRIX
#define JBS_COMP_ROW_SPARSE_MATRIX

#include <vector>
#include <iostream>
#include <fstream>


#include "Image.h"

#ifndef NO_UMFPACK
#include "mvvd.h"					// F�r "VECTOR_double"
#include "comprow_double.h"			// F�r "CompRow_Mat_double"
#include "diagpre_double.h"			// F�r "DiagPreconditioner_double"
#include "bicgstab.h"				// F�r "BiCGSTAB(A, x, b, D, maxit, tol)"
#endif


//! A simple compressed row sparse matrix class for sparselib.
class CompRowSparseMatrix {
public:

	CompRowSparseMatrix() {
	}

	~CompRowSparseMatrix() {
	}

	//! Solve Ax=0 using sparselib's BiCGSTAB method.
	void BiCGSTAB_solve(double* b, double* x) {
#ifdef _DEBUG
		std::cerr << "Solving using BiCGSTAB" << std::endl;
#endif
		// use sparselib to solve Ax=b
		CompRow_Mat_double A(dim, dim, (int)entries.size(), &entries[0], &col_ptr[0], &row_ind[0]);
		DiagPreconditioner_double D(A);
		VECTOR_double x_sl(dim, 0.00); 			 // L�sungsvektor
		VECTOR_double b_sl(dim, 0.00); 			 // L�sungsvektor

		// Fill b:
		for (int i1 = 0; i1 < dim; i1++) {
			b_sl(i1) = b[i1];
			x_sl(i1) = x[i1];
		}

		double tol = 10e-10;									// Convergence tolerance
		int maxit = 10000;										// Maximum iterations	

		int result = BiCGSTAB(A, x_sl, b_sl, D, maxit, tol);		// Solve system

		//std::cerr << "Result: x = (";
		//for (int i1 = 0; i1 < dim; i1++) {
		//	std::cerr << x_sl(i1) << " ";
		//}
		//std::cerr << ")" << std::endl;

		// Copy into original array again:
		for (int i1 = 0; i1 < dim; i1++) x[i1] = x_sl(i1);
	}

	void print() {
		std::cerr << "---- Number of entries: " << entries.size() << " " << std::endl;
		std::cerr << "---- Compressed Row Matrix : ---" << std::endl;
		std::cerr << "---- Entries: ---" << std::endl;
		for (unsigned int i1 = 0; i1 < entries.size(); i1++) std::cerr << entries[i1] << " ";
		std::cerr << std::endl << "-----------------" << std::endl << "---- Row_ind: " << std::endl;
		for (unsigned int i1 = 0; i1 < row_ind.size(); i1++) std::cerr << row_ind[i1] << " ";
		std::cerr << std::endl << "-----------------" << std::endl << "---- Col_ptr: " << std::endl;
		for (unsigned int i1 = 0; i1 < col_ptr.size(); i1++) std::cerr << col_ptr[i1] << " ";
		std::cerr << std::endl << "-----------------" << std::endl;
		std::cerr << std::endl;
	}

	void printToImage(char* filename) {

		std::cerr << "Writing " << dim << "x" << dim << " Matrix to file " << filename << std::endl;
		Image* img = new Image(dim, dim);

		// Init with white:
		for (int x = 0; x < dim; x++)
			for (int y = 0; y < dim; y++)
				img->setPixel(x,y,255);

		// mark values as dots:
		int current_col_ptr_pos = 0;
		int current_line = 0;
		for (unsigned int i1 = 0; i1 < entries.size(); i1++) {
			int x = row_ind[i1];
			if ((int)i1 >= col_ptr[current_col_ptr_pos+1]) {
				current_col_ptr_pos++;
				current_line++;
			}
			int y = current_line;
			img->setPixel(x, y, 0);
		}

		img->writeRAW(filename);

		delete img;
	}

	void printToImageGrayScale(char* filename) {

		std::cerr << "Writing " << dim << "x" << dim << " Matrix to file " << filename << std::endl;
		Image* img = new Image(dim, dim);

		// Init with white:
		for (int x = 0; x < dim; x++)
			for (int y = 0; y < dim; y++)
				img->setPixel(x,y,255);

		// Find maximum value:
		double max_value = 0;
		for (UInt i1 = 0; i1 < entries.size(); i1++) {
			if (max_value < fabs(entries[i1])) max_value = fabs(entries[i1]);
		}

		// mark values as dots:
		int current_col_ptr_pos = 0;
		int current_line = 0;
		for (unsigned int i1 = 0; i1 < entries.size(); i1++) {
			int x = row_ind[i1];
			if ((int)i1 >= col_ptr[current_col_ptr_pos+1]) {
				current_col_ptr_pos++;
				current_line++;
			}
			int y = current_line;
			int color = 128-int((max_value*128)/entries[i1]);
			img->setPixel(x, y, color);

			//std::cerr << "[" << x << ", " << y << "]   ";
		}

		img->writeRAW(filename);

		delete img;
	}

	void freeMemory() {
		entries.clear();
		row_ind.clear();
		col_ptr.clear();
	}

	std::vector<double> entries;
	std::vector<int> row_ind;
	std::vector<int> col_ptr;

	int dim;

private:

};

#endif
