#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoIndexedLineSet.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoMaterial.h>

#include "skeleton.h"

SoSeparator* skeletonToIV(KinectSkeleton* ks) {

	SoSeparator* sep = new SoSeparator;

	SoCoordinate3* coord = new SoCoordinate3;
	coord->point.setNum(60);
	sep->addChild(coord);

	SoDrawStyle* ds = new SoDrawStyle;
	ds->lineWidth = 3.0;
	sep->addChild(ds);

	SoIndexedLineSet* ls = new SoIndexedLineSet;
	ls->coordIndex.setNum(3*14);
	{
	int32_t indices[] = {0, 1, -1 };
	ls->coordIndex.setValues(0, 3, indices);
	}{
	int32_t indices[] = {1, 2, -1 };
	ls->coordIndex.setValues(3, 3, indices);
	}{
	int32_t indices[] = {1, 3, -1 };
	ls->coordIndex.setValues(6, 3, indices);
	}{
	int32_t indices[] = {1, 4, -1 };
	ls->coordIndex.setValues(9, 3, indices);
	}{
	int32_t indices[] = {2, 5, -1 };
	ls->coordIndex.setValues(12, 3, indices);
	}{
	int32_t indices[] = {3, 6, -1 };
	ls->coordIndex.setValues(15, 3, indices);
	}{
	int32_t indices[] = {5, 7, -1 };
	ls->coordIndex.setValues(18, 3, indices);
	}{
	int32_t indices[] = {6, 8, -1 };
	ls->coordIndex.setValues(21, 3, indices);
	}{
	int32_t indices[] = {4, 9, -1 };
	ls->coordIndex.setValues(24, 3, indices);
	}{
	int32_t indices[] = {4, 10, -1 };
	ls->coordIndex.setValues(27, 3, indices);
	}{
	int32_t indices[] = {9, 11, -1 };
	ls->coordIndex.setValues(30, 3, indices);
	}{
	int32_t indices[] = {10, 12, -1 };
	ls->coordIndex.setValues(33, 3, indices);
	}{
	int32_t indices[] = {11, 13, -1 };
	ls->coordIndex.setValues(36, 3, indices);
	}{
	int32_t indices[] = {12, 14, -1 };
	ls->coordIndex.setValues(39, 3, indices);
	}
	sep->addChild(ls);

	SoDrawStyle* ds2 = new SoDrawStyle;
	ds2->lineWidth = 4.0;

	{
		SoMaterial* mat = new SoMaterial;
		mat->diffuseColor.set1Value(0, SbColor(1,0,0));
		sep->addChild(mat);

		sep->addChild(ds2);
		SoIndexedLineSet* ls_coord = new SoIndexedLineSet;
		ls_coord->coordIndex.setNum(3*3*15);
		for (int i1 = 0; i1 < 15; i1++) {
			int32_t indices[] = {i1, 15+3*i1, -1 };
			ls_coord->coordIndex.setValues(45+3*i1, 3, indices);
		}
		sep->addChild(ls_coord);
	}
	{
		SoMaterial* mat = new SoMaterial;
		mat->diffuseColor.set1Value(0, SbColor(0,1,0));
		sep->addChild(mat);

		sep->addChild(ds2);
		SoIndexedLineSet* ls_coord = new SoIndexedLineSet;
		ls_coord->coordIndex.setNum(3*3*15);
		for (int i1 = 0; i1 < 15; i1++) {
			int32_t indices[] = {i1, 15+(3*i1)+1, -1 };
			ls_coord->coordIndex.setValues(45+3*i1+3, 3, indices);
		}
		sep->addChild(ls_coord);
	}
	{
		SoMaterial* mat = new SoMaterial;
		mat->diffuseColor.set1Value(0, SbColor(1,1,0));
		sep->addChild(mat);

		sep->addChild(ds2);
		SoIndexedLineSet* ls_coord = new SoIndexedLineSet;
		ls_coord->coordIndex.setNum(3*3*15);
		for (int i1 = 0; i1 < 15; i1++) {
			int32_t indices[] = {i1, 15+(3*i1)+2, -1 };
			ls_coord->coordIndex.setValues(45+3*i1+6, 3, indices);
		}
		sep->addChild(ls_coord);
	}


	for (int i1 = 0; i1 < 15; i1++) {
		const vec3f& pt = ks->joints[i1];
		coord->point.set1Value(i1, pt.x, pt.y, pt.z);
	}

	//ks.computeBoneOrientations();

	for (int i1 = 0; i1 < 15; i1++) {
		const vec3f& pt = ks->joints[i1];
		//const mat3f& mat = g_initialOrientations.orient[i1];
		mat3f mat;


		if (i1 == 1)
			mat = mat3f(ks->bones[3].orientation.a);
		if (i1 == 2)
			mat = mat3f(ks->bones[6].orientation.a);
		if (i1 == 3)
			mat = mat3f(ks->bones[7].orientation.a);
		if (i1 == 4)
			mat = mat3f(ks->bones[0].orientation.a);
		if (i1 == 5)
			mat = mat3f(ks->bones[10].orientation.a);
		if (i1 == 6)
			mat = mat3f(ks->bones[11].orientation.a);
		if (i1 == 9)
			mat = mat3f(ks->bones[8].orientation.a);
		if (i1 == 10)
			mat = mat3f(ks->bones[9].orientation.a);
		if (i1 == 11)
			mat = mat3f(ks->bones[12].orientation.a);
		if (i1 == 12)
			mat = mat3f(ks->bones[13].orientation.a);

		for (int c = 0; c < 3; c++) {
			vec3f e;
			e[c] = 0.1f;
			e = mat.vecTrans(e);
			e += pt;
			//e = vec3f(0,0,0);
			coord->point.set1Value(15+i1*3+c, e.x, e.y, e.z);
		}
	}

	return sep;
}