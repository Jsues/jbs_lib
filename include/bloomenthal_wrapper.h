#ifndef BLOOMENTHAL_WRAPPER_H
#define BLOOMENTHAL_WRAPPER_H

#define NO_BLOOMENTHAL_NORMALS

#include "ScalarFunction.h"
#include "polygonizer.h"


template <class Point>
class BloomImplicitFunctionwrapper : public ImplicitFunction {
public:
	BloomImplicitFunctionwrapper(ScalarFunction<Point>* func) {
		m_func = func;
	}
	float eval (float x, float y, float z) {
		Point::ValueType f = m_func->eval(Point(x,y,z));
		return (float)f;
	}
	void evalGrad(Bloom_POINT* pt, Bloom_POINT* grad) {
		Point g;
		m_func->evalValAndGrad(Point(pt->x, pt->y, pt->z), g);
		g.normalize();
		grad->x = (float)g.x; grad->y = (float)g.y; grad->z = (float)g.z;
	}
private:
	ScalarFunction<Point>* m_func;
};



void printOFFFile(Polygonizer* pol, const char* filename) {
	std::ofstream OutFile(filename);
	// Fehler abfangen
	if (OutFile.fail()) {
		std::cerr << "Fehler beim �ffnen der Datei!" << std::endl;
		std::cerr << "Could not write to [" << filename << "]" << std::endl;
		return;
	}

	OutFile << "OFF" << std::endl;
	int numV = (int)pol->no_vertices();
	int numT = (int)pol->no_triangles();
	int numE = 0;

	OutFile << numV << " " << numT <<  " " << numE << std::endl;
	for (int i1 = 0; i1 < numV; i1++) {
		OutFile << pol->gvertices[i1].x << " " << pol->gvertices[i1].y << " " << pol->gvertices[i1].z << std::endl;
	}
	for (int i1 = 0; i1 < numT; i1++) {
		OutFile << "3 " << pol->gtriangles[i1].v0 << " " << pol->gtriangles[i1].v1 << " " << pol->gtriangles[i1].v2 << std::endl;
	}
	OutFile.close();
}


void printBOBJFile(Polygonizer* pol, const char* filename, vec3f move = vec3f(0,0,0), float scale = 1) {
	std::ofstream OutFile(filename, std::ios::binary);
	// Fehler abfangen
	if (OutFile.fail()) {
		std::cerr << "Fehler beim �ffnen der Datei!" << std::endl;
		std::cerr << "Could not write to [" << filename << "]" << std::endl;
		return;
	}

	int numV = (int)pol->no_vertices();
	int numT = (int)pol->no_triangles();


	vec3f pt;
	char ver = 0;
	char tri = 1;
	for (int i1 = 0; i1 < numV; i1++) {
		OutFile.write((char*)&ver, sizeof(char));
		pt = vec3f((float*)&pol->gvertices[i1]);
		pt = (pt+move)*scale;
		OutFile.write((char*)&pt, 3*sizeof(float));
	}
	vec3i t;
	for (int i1 = 0; i1 < numT; i1++) {
		OutFile.write((char*)&tri, sizeof(char));
		t = vec3i(pol->gtriangles[i1].v0+1, pol->gtriangles[i1].v1+1, pol->gtriangles[i1].v2+1);
		OutFile.write((char*)&t, 3*sizeof(int));
	}
	OutFile.close();
}


#endif
