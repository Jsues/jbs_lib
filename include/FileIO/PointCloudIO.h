#ifndef POINT_CLOUD_READER_H
#define POINT_CLOUD_READER_H


#include <iostream>
#include <fstream>
#include <string>
#include <cctype>       // std::toupper
#include "PointCloud.h"
//#include "kd_TreePointCloud.h"
#include "point3d.h"
#include "point4d.h"
#include "point2d.h"



template <class T> 
PointCloudNormals<T>* readPtxFile(const char* filename) {
	
	typedef typename T::ValueType ValueType;

	std::ifstream fin(filename, std::ifstream::binary);

	if (!fin.good()) {
		std::cerr << "Error reading file in 'readBinaryPointCloudNormals<T>(char* filename)'" << std::endl;
		return NULL;
	}

	PointCloudNormals<T>* pc = new PointCloudNormals<T>();

	// Read header:
	int n, m;
	vec3f p3;
	vec4f p4;


	fin >> n >> m;
	for (int i1 = 0; i1 < 4; i1++)
		fin >> p3.x >> p3.y >> p3.z;
	for (int i1 = 0; i1 < 4; i1++)
		fin >> p4.x >> p4.y >> p4.z >> p4.w;

	T pos;
	T col;
	float intensity;

	while(!fin.eof()) {
		fin >> pos.x >> pos.y >> pos.z >> intensity >> col.x >> col.y >> col.z;

		if (pos.squaredLength() > 0) {
			col /= 255;
			pc->insertPoint(pos, col);
		}
	}

	return pc;
}


template <class T> 
PointCloudNormals<T>* readBinaryPointCloudNormalsKSD(const char* filename) {
	
	typedef typename T::ValueType ValueType;

	std::ifstream fin(filename, std::ifstream::binary);

	if (!fin.good()) {
		std::cerr << "Error reading file in 'readBinaryPointCloudNormals<T>(char* filename)'" << std::endl;
		return NULL;
	}

	PointCloudNormals<T>* pc = new PointCloudNormals<T>();
	
	char numBytes;
	UInt numPoints;
	UInt numFaces;
	// read header
	fin.read((char*)&numBytes, 1);
	fin.read((char*)&numPoints, sizeof(UInt));
	fin.read((char*)&numFaces, sizeof(UInt));

	//numPoints = (numPoints > 800000) ? 800000 : numPoints;

	//std::cerr << "numBytes: " << (int)numBytes << " numPoints: " << numPoints << std::endl;

	if (numBytes == sizeof(double)) {
		vec3d* tmp = new vec3d[2*numPoints];
		fin.read((char*)tmp, 2*numPoints*3*numBytes);

		if (numBytes == sizeof(T::ValueType)) {
			for (UInt i1 = 0; i1 < numPoints; i1++) {
				pc->insertPoint(T((ValueType)tmp[i1].x, (ValueType)tmp[i1].y, (ValueType)tmp[i1].z), T((ValueType)tmp[i1+numPoints].x, (ValueType)tmp[i1+numPoints].y, (ValueType)tmp[i1+numPoints].z));
			}
		} else {
			for (UInt i1 = 0; i1 < numPoints; i1++) {
				//tmp[i1].print();

				//if ((i1%5) != 0) continue;

				T p((T::ValueType)tmp[i1].x, (T::ValueType)tmp[i1].y, (T::ValueType)tmp[i1].z);
				T n((T::ValueType)tmp[i1+numPoints].x, (T::ValueType)tmp[i1+numPoints].y, (T::ValueType)tmp[i1+numPoints].z);

				//if (i1 > 100) exit(1);
				pc->insertPoint(p, n);
			}
		}
		delete[] tmp;

	} else if (numBytes == sizeof(float)) {
		vec3f* tmp = new vec3f[2*numPoints];
		fin.read((char*)tmp, 2*numPoints*3*numBytes);

		if (numBytes == sizeof(T::ValueType)) {
			for (UInt i1 = 0; i1 < numPoints; i1++) {
				pc->insertPoint(tmp[i1], tmp[i1+numPoints]);
			}
		} else {
			for (UInt i1 = 0; i1 < numPoints; i1++) {

				//if ((i1 > 1000) || (i1%2 == 2)) continue;

				T p(tmp[i1].x, tmp[i1].y, tmp[i1].z);
				T n(tmp[i1+numPoints].x, tmp[i1+numPoints].y, tmp[i1+numPoints].z);
				pc->insertPoint(p, n);
			}
		}
		delete[] tmp;

	}
	//std::cerr << "pc->size(): " << pc->getNumPts() << std::endl;



	fin.close();

	return pc;

}


template <class T> 
PointCloudNormals<T>* readBinaryPointCloudNormals(const char* filename) {
	
	typedef typename T::ValueType ValueType;

	std::ifstream fin(filename, std::ifstream::binary);

	if (!fin.good()) {
		std::cerr << "Error reading file in 'readBinaryPointCloudNormals<T>(char* filename)'" << std::endl;
		std::cerr << "filename: " << filename << std::endl;
		return NULL;
	}

	PointCloudNormals<T>* pc = new PointCloudNormals<T>();
	
	char numBytes;
	UInt numPoints;
	// read header
	fin.read((char*)&numBytes, 1);
	fin.read((char*)&numPoints, sizeof(UInt));

	//numPoints = (numPoints > 800000) ? 800000 : numPoints;

	//std::cerr << "numBytes: " << (int)numBytes << " numPoints: " << numPoints << std::endl;

	if (numBytes == sizeof(double)) {
		vec3d* tmp = new vec3d[2*numPoints];
		fin.read((char*)tmp, 2*numPoints*3*numBytes);

		if (numBytes == sizeof(T::ValueType)) {
			for (UInt i1 = 0; i1 < numPoints; i1++) {
				pc->insertPoint(T((ValueType)tmp[i1].x, (ValueType)tmp[i1].y, (ValueType)tmp[i1].z), T((ValueType)tmp[i1+numPoints].x, (ValueType)tmp[i1+numPoints].y, (ValueType)tmp[i1+numPoints].z));
			}
		} else {
			for (UInt i1 = 0; i1 < numPoints; i1++) {
				//tmp[i1].print();

				//if ((i1%5) != 0) continue;

				T p((T::ValueType)tmp[i1].x, (T::ValueType)tmp[i1].y, (T::ValueType)tmp[i1].z);
				T n((T::ValueType)tmp[i1+numPoints].x, (T::ValueType)tmp[i1+numPoints].y, (T::ValueType)tmp[i1+numPoints].z);

				//if (i1 > 100) exit(1);
				pc->insertPoint(p, n);
			}
		}
		delete[] tmp;

	} else if (numBytes == sizeof(float)) {
		vec3f* tmp = new vec3f[2*numPoints];
		fin.read((char*)tmp, 2*numPoints*3*numBytes);

		if (numBytes == sizeof(T::ValueType)) {
			for (UInt i1 = 0; i1 < numPoints; i1++) {
				pc->insertPoint(tmp[i1], tmp[i1+numPoints]);
			}
		} else {
			for (UInt i1 = 0; i1 < numPoints; i1++) {

				//if ((i1 > 1000) || (i1%2 == 2)) continue;

				T p(tmp[i1].x, tmp[i1].y, tmp[i1].z);
				T n(tmp[i1+numPoints].x, tmp[i1+numPoints].y, tmp[i1+numPoints].z);
				pc->insertPoint(p, n);
			}
		}
		delete[] tmp;

	}
	//std::cerr << "pc->size(): " << pc->getNumPts() << std::endl;



	fin.close();

	return pc;

}


static PointCloudNormals<vec3f>* readSDFile(const char* filename, std::vector<vec3f>& markers) {
	

	std::ifstream fin(filename, std::ifstream::binary);

	if (!fin.good()) {
		std::cerr << "Error reading file in 'readSDFile<T>(char* filename)'" << std::endl;
		return NULL;
	}

	PointCloudNormals<vec3f>* pc = new PointCloudNormals<vec3f>();
	
	char numBytes;
	UInt numPoints;
	// read header
	fin.read((char*)&numBytes, 1);
	fin.read((char*)&numPoints, sizeof(UInt));

	//numPoints = (numPoints > 800000) ? 800000 : numPoints;

	//std::cerr << "numBytes: " << (int)numBytes << " numPoints: " << numPoints << std::endl;

	if (numBytes == sizeof(double)) {

		exit(1);

	} else if (numBytes == sizeof(float)) {
		vec3f* tmp = new vec3f[2*numPoints];
		fin.read((char*)tmp, 2*numPoints*3*numBytes);

		for (UInt i1 = 0; i1 < numPoints; i1++) {
			pc->insertPoint(tmp[i1], tmp[i1+numPoints]);
		}
		delete[] tmp;

	}

	markers.clear();
	markers.resize(5);
	fin.read((char*)&markers[0], sizeof(float)*3*5);

	fin.close();

	return pc;

}


class PointCloudReader
{
public:

//****************************************************************************************************

	PointCloudReader(){};

//****************************************************************************************************

	PointCloud<vec2d>* readOFFFileDOUBLE(const char* filename) {

		PointCloud<vec2d>* pc = new PointCloud<vec2d>();

		// Eingabedatei oeffnen
		std::ifstream InFile(filename);

		if (!InFile.is_open()) {
			std::cerr << "PointCloudReader::readOFFFile(...) : File not found : " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		// Erste Zeile lautet
		// **number of points : 7049 **
		char  s1[10];
		int dim, numV, numF, numE;
		InFile >> s1 >> dim >> numV >> numF >> numE;

		if (dim != 2) {
			std::cerr << "PointCloudReader::readOFFFile(" << filename << ") Invalid OFF-File, dimension should be 2 but is " << dim << std::endl;
			exit(EXIT_FAILURE);
		}

		vec2d pt;

		for (int i1 = 0; i1 < numV; i1++) {
			InFile >> pt;
			pc->insertPoint(pt);
		}

		return pc;

	}

//****************************************************************************************************

	PointCloud<vec2f>* readOFFFile(const char* filename) {

		PointCloud<vec2f>* pc = new PointCloud<vec2f>();

		// Eingabedatei oeffnen
		std::ifstream InFile(filename);

		if (!InFile.is_open()) {
			std::cerr << "PointCloudReader::readOFFFile(...) : File not found : " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		// Erste Zeile lautet
		// **number of points : 7049 **
		char  s1[10];
		int dim, numV, numF, numE;
		InFile >> s1 >> dim >> numV >> numF >> numE;

		if (dim != 2) {
			std::cerr << "PointCloudReader::readOFFFile(" << filename << ") Invalid OFF-File, dimension should be 2 but is " << dim << std::endl;
			exit(EXIT_FAILURE);
		}

		vec2f pt;

		for (int i1 = 0; i1 < numV; i1++) {
			InFile >> pt;
			pc->insertPoint(pt);
		}

		return pc;

	}

//****************************************************************************************************

	static PointCloudNormals<vec3f>* readPCNFileFromExtension(const char* filename) {

		std::string s(filename);
		UInt pos = (UInt) s.rfind(".");
        
		if (pos == s.npos) {
			std::cerr << "ERROR in PointCloudReader::readPCNFileFromExtension(...)" << std::endl;
			std::cerr << "could not determine file extension from " << s << std::endl;
			std::cerr << "EXIT" << std::endl;
			exit(EXIT_FAILURE);
		}

		std::string ext = s.substr(pos+1);

		std::transform(ext.begin(), ext.end(), ext.begin(), std::tolower);

		
		if (ext.compare(std::string("pwn")) == 0) {
			return readPWNFile<vec3f>(filename, 1, true);
		} else if (ext.compare(std::string("pcb")) == 0) {
			return readBinaryPointCloudNormals<vec3f>(filename);
		} else if (ext.compare(std::string("poi")) == 0) {
			return readPoissonFileBinary(filename);
		} else if (ext.compare(std::string("asc")) == 0) {
			PointCloudReader pr;
			return (PointCloudNormals<vec3f>*)pr.readASCFile(filename);
		} else if (ext.compare(std::string("obj")) == 0) {
			return readOBJPC(filename);
		} else {
			std::cerr << "Unknown extension: " << ext << " specified" << std::endl;
		}

		return NULL;
	}

//****************************************************************************************************

	static PointCloudNormals<vec3f>* readOBJPC(const char* filename) {

		PointCloudNormals<vec3f>* pc = new PointCloudNormals<vec3f>();

		std::ifstream InFile(filename);
		// Fehler abfangen
		if (InFile.fail()) {
			std::cerr << "Fehler beim Oeffnen der Datei: " << filename << std::endl;
			exit(1);
			return NULL;
		}

		char typ;	
		float x,y,z;
		char line[2000];

		std::vector<vec3f> normals;
		std::vector<vec3f> points;
		bool bEnd = false;
		while(!InFile.eof() || bEnd) {
			InFile >> typ;
			if (InFile.eof()) break;
			switch(typ) {
				case 'v':
					// This may be a vertex 'v' or a vertex normal 'vn'
					if (InFile.peek() == 'n') { // dude, it's a normal
						InFile >> typ;
						InFile >> x >> y >> z;
						normals.push_back(vec3f(x,y,z));
					} else {
						InFile >> x >> y >> z;
						points.push_back(vec3f(x,y,z));
					}
					if (InFile.eof()) {bEnd = true; break;};
					break;
				default:
					InFile.getline(line, 2000);	//TODO this is ugly I know
					break;
			}
		}

		
		InFile.close();
	
		if (points.size() != normals.size()) {
			std::cerr << "Error num points doesn't match num normals" << std::endl;
			exit(1);
		}

		for (UInt i1 = 0; i1 < points.size(); i1++) pc->insertPoint(points[i1], normals[i1]);

		return pc;
	}


//****************************************************************************************************

	static PointCloudNormals<vec3f>* readPoissonFileBinary(const char* filename) {

		PointCloudNormals<vec3f>* pc = new PointCloudNormals<vec3f>();

		// Eingabedatei oeffnen
		std::ifstream FileBin(filename, std::ifstream::binary);

		if (!FileBin.is_open()) {
			std::cerr << "PointCloudReader::readPoissonFileBinary(...) : File not found : " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		FileBin.seekg( 0, std::ios::end );
		long fileSize = (long)FileBin.tellg();
		FileBin.seekg( 0, std::ios::beg );


		char sizeofDT = sizeof(vec3f::ValueType);
		UInt numBytesPerV = sizeofDT*3;

		UInt numV = fileSize/(2*numBytesPerV);

		vec3f* ptsAndNrmls = new vec3f[2*numV];

		FileBin.read((char*)ptsAndNrmls, fileSize);

		for (UInt i1 = 0; i1 < 2*numV; i1+=2) {
			pc->insertPoint(ptsAndNrmls[i1], ptsAndNrmls[i1+1]);
		}

		delete[] ptsAndNrmls;
		
		return pc;

	}

//****************************************************************************************************

	template <class Point>
	PointCloud<Point>* readXYZFile(const char* filename) {
		PointCloud<Point>* pc = new PointCloudNormals<Point>();

		// Eingabedatei oeffnen
		std::ifstream InFile(filename);

		if (!InFile.is_open()) {
			std::cerr << "PointCloudReader::readXYZFile(...) : File not found : " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		// Erste Zeile lautet
		// 7049
		int numOfPoints;
//		InFile >> s1 >> s2 >> s3 >> s4 >> numOfPoints >> s5;
		InFile >> numOfPoints;

		Point pt, n;
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			for (UInt i2 = 0; i2 < Point::dim; i2++) InFile >> pt[i2];
			//pt /= 100;
			pc->insertPoint(pt);
		}
		InFile.close();

		//std::cerr << "Read " << numOfPoints << " points" << std::endl;

		return pc;
	}

//****************************************************************************************************

	template <class Point>
	PointCloudNormals<Point>* readXYZFileWithNormals(const char* filename) {
		PointCloudNormals<Point>* pc = new PointCloudNormals<Point>();

		// Eingabedatei oeffnen
		std::ifstream InFile(filename);

		if (!InFile.is_open()) {
			std::cerr << "PointCloudReader::readXYZFile(...) : File not found : " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		// Erste Zeile lautet
		// 7049
		int numOfPoints;
//		InFile >> s1 >> s2 >> s3 >> s4 >> numOfPoints >> s5;
		InFile >> numOfPoints;

		Point pt, n;
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			for (UInt i2 = 0; i2 < Point::dim; i2++) InFile >> pt[i2];
			for (UInt i2 = 0; i2 < Point::dim; i2++) InFile >> n[i2];
			//pt /= 100;
			pc->insertPoint(pt, n);
		}
		InFile.close();

		//std::cerr << "Read " << numOfPoints << " points" << std::endl;

		return pc;

	}


//****************************************************************************************************

	PointCloud<vec3f>* readBinNormalMapFile(const char* filename) {

		std::cerr << "Read bin file" << std::endl;

		std::ifstream filein(filename, std::ios::binary);

		int numPoints;
		filein.read((char*)&numPoints, sizeof(int));
		//filein >> numPoints;

		std::cerr << "NumPoints: " << numPoints << std::endl;

		vec3d* normals = new vec3d[numPoints];
		filein.read((char*)&normals[0], sizeof(double)*numPoints*3);

		PointCloud<vec3f>* pc = new PointCloud<vec3f>();
		for (int i1 = 0; i1 < numPoints; i1++) {
			const vec3d& p = normals[i1];
			pc->insertPoint(vec3f((float)p.x, (float)p.y, (float)p.z));
		}

		return pc;

	}

//****************************************************************************************************

	static PointCloud<vec3f>* readASCFile(const char* filename) {

		PointCloud<vec3f>* pc = new PointCloud<vec3f>();

		// Eingabedatei oeffnen
		std::ifstream InFile(filename);

		if (!InFile.is_open()) {
			std::cerr << "PointCloudReader::readASCFile(...) : File not found : " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		// Erste Zeile lautet
		// **number of points : 7049 **
		char  s1[10], s2[20], s3[8], s4[20], s5[4];
		int numOfPoints;
		InFile >> s1 >> s2 >> s3 >> s4 >> numOfPoints >> s5;

		vec3f pt;
		// Lese Punkte ein:
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			InFile >> pt[0] >> pt[1] >> pt[2];

			//pt.print();
			//if (pt.length() > 1000) {
			//	std::cerr << i1;
			//}

			pc->insertPoint(pt);
		}
		InFile.close();

		std::cerr << "Read " << numOfPoints << " points" << std::endl;

		return pc;
	};


//****************************************************************************************************

	template <class Point>
	static PointCloudNormals<Point>* readPWNFile(const char* filename, int useOnlyAnyXPoint = 1, bool beQuiet = false) {

		PointCloudNormals<Point>* pc = new PointCloudNormals<Point>();

		// Eingabedatei oeffnen
		std::ifstream InFile(filename);

		if (!InFile.is_open()) {
			std::cerr << "PointCloudReader::readPWNFile(...) : File not found : " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		// Erste Zeile lautet
		// 7049
		int numOfPoints;
//		InFile >> s1 >> s2 >> s3 >> s4 >> numOfPoints >> s5;
		InFile >> numOfPoints;

		Point pt;
		// Lese Punkte ein:
		std::vector<Point> pts;
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			for (UInt i2 = 0; i2 < Point::dim; i2++) InFile >> pt[i2];
			//pt /= 100;
			pts.push_back(pt);
		}
		std::vector<Point> normls;
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			for (UInt i2 = 0; i2 < Point::dim; i2++) InFile >> pt[i2];
			normls.push_back(pt);
		}

		for (int i1 = 0; i1 < numOfPoints; i1++) {
			if (i1%useOnlyAnyXPoint == 0) {
				pc->insertPoint(pts[i1], normls[i1]);
			}

			//if (rand()%40 == 0) i1+=rand()%40;
		}
		InFile.close();

		//vec3f center = (pc->getMax()+pc->getMin())/2.0f;
		//for (int i1 = 0; i1 < numOfPoints; i1++) {
		//	pc->getPoints()[i1] -= center;
		//}
		//pc->recomputeMinMax();

		if (!beQuiet) std::cerr << numOfPoints << " points ranging from " << pc->getMin() << " to " << pc->getMax() << std::endl;

		return pc;
	};


//****************************************************************************************************


	//void readASCFile(char* filename, kdTreePointCloud<vec3f>* pc) {

	//	// Eingabedatei oeffnen
	//	std::ifstream InFile(filename);

	//	if (!InFile.is_open()) {
	//		std::cerr << "PointCloudReader::readASCFile(...) : File not found : " << filename << std::endl;
	//		exit(EXIT_FAILURE);
	//	}

	//	// Erste Zeile lautet
	//	// **number of points : 7049 **
	//	char  s1[10], s2[4], s3[8], s4[3], s5[4];
	//	int numOfPoints;
	//	InFile >> s1 >> s2 >> s3 >> s4 >> numOfPoints >> s5;

	//	vec3f pt;
	//	// Lese Punkte ein:
	//	for (int i1 = 0; i1 < numOfPoints; i1++) {
	//		InFile >> pt[0] >> pt[1] >> pt[2];
	//		pc->insertPoint(pt);
	//	}
	//	InFile.close();

	//	std::cerr << "Read " << numOfPoints << " points" << std::endl;
	//};


protected:

private:
};

template <class T>
class PointCloudWriter
{
public:
	PointCloudWriter(PointCloud<T>* pc) {
		m_pc = pc;
	};


	template <class T>
	static void writeASCFile(const std::vector<T>& pc, const char* filename) {

		//std::cerr << "writing file " << filename << std::endl;

		// Ausgabedatei oeffnen
		std::ofstream OutFile(filename);

		// Erste Zeile lautet
		// **number of points : 7049 **
		int numOfPoints = pc.size();
		OutFile << "**number of points : " << numOfPoints << " **" << std::endl;

		// Schreibe Punkte:
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			vec3d p(pc[i1][0], pc[i1][1], 0);
			if (T::dim == 3)
				p.z = pc[i1][2];
			OutFile << p[0] << " " << p[1] << " " << p[2] << std::endl;
		}
		OutFile.close();

	}


	void writeASCFile(const char* filename) {

		// Ausgabedatei oeffnen
		std::ofstream OutFile(filename);

		// Erste Zeile lautet
		// **number of points : 7049 **
		int numOfPoints = m_pc->getNumPts();
		OutFile << "**number of points : " << numOfPoints << " **" << std::endl;

		T* pts = m_pc->getPoints();
		// Schreibe Punkte:
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			OutFile << pts[i1][0] << " " << pts[i1][1] << " " << pts[i1][2] << std::endl;
		}
		OutFile.close();

		return;
	};

	static void writePoissonRecFileDirect(PointCloudNormals<T>* pc, const char* filename) {
		// Ausgabedatei oeffnen
		std::ofstream OutFile(filename);

		// Erste Zeile lautet
		// **number of points : 7049 **
		int numOfPoints = pc->getNumPts();
		OutFile << numOfPoints << std::endl;

		T* pts = pc->getPoints();
		T* normals = &pc->m_normals[0];
		// Schreibe Punkte:
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			OutFile << pts[i1][0] << " " << pts[i1][1] << " " << pts[i1][2] << " " << normals[i1][0] << " " << normals[i1][1] << " " << normals[i1][2] << std::endl;
		}
		OutFile.close();

		return;
	}

	static void writeWaveletFileBinary(PointCloudNormals<T>* pc, const char* filename) {
		// Ausgabedatei oeffnen
		std::ofstream fout(filename, std::ofstream::binary);

		if (!fout.good()) {
			std::cerr << "Error writing file in 'PointCloudWriter<T>::writePWNFileBinary(PointCloudNormals<T>* pc, const char* filename)'" << std::endl;
			return;
		}

		pc->recomputeMinMax();
		T minVT = pc->getMin();
		T maxVT = pc->getMax();
		vec3f minV((float)minVT.x, (float)minVT.x, (float)minVT.x);
		vec3f maxV((float)maxVT.x, (float)maxVT.x, (float)maxVT.x);
		int numOfPoints = pc->getNumPts();

		// Write header:
		fout.write((char*)&minV, 3*sizeof(float));
		fout.write((char*)&maxV, 3*sizeof(float));
		fout.write((char*)&numOfPoints, sizeof(int));

		vec3f* n_and_p = new vec3f[2*numOfPoints];
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			T p = pc->getPoints()[i1];
			T n = pc->getNormals()[i1];
			n_and_p[2*i1+0] = vec3f((float)n.x, (float)n.y, (float)n.z);
			n_and_p[2*i1+1] = vec3f((float)p.x, (float)p.y, (float)p.z);
		}

		UInt numBytes = numOfPoints*sizeof(float)*6;
		fout.write((char*)n_and_p, numBytes);

		fout.close();

		delete[] n_and_p;

		return;
	}

	static void writePWNFileBinary(PointCloudNormals<T>* pc, const char* filename) {
		// Ausgabedatei oeffnen
		std::ofstream fout(filename, std::ofstream::binary);

		if (!fout.good()) {
			std::cerr << "Error writing file in 'PointCloudWriter<T>::writePWNFileBinary(PointCloudNormals<T>* pc, const char* filename)'" << std::endl;
			std::cerr << "filename is: " << filename << std::endl;
			return;
		}
		// Erste Zeile lautet
		UInt numOfPoints = pc->getNumPts();
		char sizeofDT = sizeof(T::ValueType);
		fout.write((char*)&sizeofDT, sizeof(char));
		fout.write((char*)&numOfPoints, sizeof(UInt));

		// Schreibe Punkte:
		T* pts = pc->getPoints();
		UInt numBytes = numOfPoints*sizeofDT*3;
		fout.write((char*)pts, numBytes);

		// Schreibe Normalen:
		T* nrmls = pc->getNormals();
		fout.write((char*)nrmls, numBytes);

		fout.close();

		return;
	}

	static void writePoissonFileBinary(PointCloudNormals<T>* pc, const char* filename) {
		// Ausgabedatei oeffnen
		std::ofstream fout(filename, std::ofstream::binary);

		if (!fout.good()) {
			std::cerr << "Error writing file in 'PointCloudWriter<T>::writePoissonFileBinary(PointCloudNormals<T>* pc, const char* filename)'" << std::endl;
			return;
		}
		// Erste Zeile lautet

		// Schreibe Punkte:
		T* pts = pc->getPoints();
		T* nrmls = pc->getNormals();

		char sizeofDT = sizeof(T::ValueType);
		UInt numBytesPerV = sizeofDT*3;
		for (int i1 = 0; i1 < pc->getNumPts(); i1++) {
			fout.write((char*)&pts[i1], numBytesPerV);
			fout.write((char*)&nrmls[i1], numBytesPerV);
		}

		fout.close();

		return;
	}

	static void writeXYZFile(PointCloudNormals<T>* pc, const char* filename) {
		// Ausgabedatei oeffnen
		std::ofstream OutFile(filename);

		// Erste Zeile lautet
		// **number of points : 7049 **
		int numOfPoints = pc->getNumPts();
		OutFile << numOfPoints << std::endl;

		T* pts = pc->getPoints();
		T* normals = &pc->m_normals[0];
		// Schreibe Punkte:
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			for (UInt i2 = 0; i2 < T::dim; i2++) OutFile << pts[i1][i2] << " ";
			for (UInt i2 = 0; i2 < T::dim; i2++) OutFile << normals[i1][i2] << " ";
			OutFile << std::endl;
		}
		OutFile.close();

		return;
	}

	static void writePWNFileDirect(PointCloudNormals<T>* pc, const char* filename) {
		// Ausgabedatei oeffnen
		std::ofstream OutFile(filename);

		// Erste Zeile lautet
		// **number of points : 7049 **
		int numOfPoints = pc->getNumPts();
		OutFile << numOfPoints << std::endl;

		T* pts = pc->getPoints();
		// Schreibe Punkte:
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			for (UInt i2 = 0; i2 < T::dim; i2++) OutFile << pts[i1][i2] << " ";
			OutFile << std::endl;
		}
		T* normals = &pc->m_normals[0];
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			for (UInt i2 = 0; i2 < T::dim; i2++) OutFile << normals[i1][i2] << " ";
			OutFile << std::endl;
		}
		OutFile.close();

		return;
	}

	void writePWNFile(PointCloud<T>* normals_pc, const char* filename) {

		// Ausgabedatei oeffnen
		std::ofstream OutFile(filename);

		// Erste Zeile lautet
		// **number of points : 7049 **
		int numOfPoints = m_pc->getNumPts();
		OutFile << numOfPoints << std::endl;

		T* pts = m_pc->getPoints();
		// Schreibe Punkte:
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			OutFile << pts[i1][0] << " " << pts[i1][1] << " " << pts[i1][2] << std::endl;
		}
		T* normals = normals_pc->getPoints();
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			OutFile << normals[i1][0] << " " << normals[i1][1] << " " << normals[i1][2] << std::endl;
		}
		OutFile.close();

		return;
	};

protected:

private:
	PointCloud<T>* m_pc;
};


template <class Point>
PointCloudNormals<Point>* readPC4Dbinary(const char* filename, typename Point::ValueType time_scaler = 1) {

	PointCloudNormals<Point>* pc = new PointCloudNormals<Point>;
	std::ifstream fin(filename, std::ifstream::binary);

	if (!fin.good()) {
		std::cerr << "Error reading file in 'PointCloud4D<T>::Read4PNFileBin(char* filename)'" << std::endl;
		return NULL;
	}

	char numByteForDT;
	UInt numV;
	// read header
	fin.read((char*)&numByteForDT, 1);
	fin.read((char*)&numV, sizeof(UInt));

	if (numByteForDT != sizeof(Point::ValueType)) {
		std::cerr << "Could not read PointCloud4D, because of following error" << std::endl;
		std::cerr << "The sizeof the datatype is wrong." << std::endl;
		std::cerr << "The File uses " << (UInt)numByteForDT << " Bytes per floating point" << std::endl;
		std::cerr << "The PointCloud uses " << sizeof(Point::ValueType) << " Bytes per floating point" << std::endl;
		std::cerr << "Probably a float/double issue" << std::endl;
		std::cerr << "EXIT" << std::endl;
		exit(EXIT_FAILURE);
	}

	std::cerr << "PC contains " << numV << " points" << std::endl;

	pc->m_pts.resize(numV);
	pc->m_normals.resize(numV);

	fin.read((char*)&pc->m_pts[0], 4*numByteForDT*numV);
	fin.read((char*)&pc->m_normals[0], 4*numByteForDT*numV);
	fin.close();

	for (UInt i1 = 0; i1 < numV; i1++) {

		Point& p = pc->m_pts[i1];
		
		p.w *= time_scaler;

		if (p.x < pc->min.x) pc->min.x = p.x;
		if (p.y < pc->min.y) pc->min.y = p.y;
		if (p.z < pc->min.z) pc->min.z = p.z;
		if (p.w < pc->min.w) pc->min.w = p.w;
		if (p.x > pc->max.x) pc->max.x = p.x;
		if (p.y > pc->max.y) pc->max.y = p.y;
		if (p.z > pc->max.z) pc->max.z = p.z;
		if (p.w > pc->max.w) pc->max.w = p.w;

	}

	return pc;

}


#endif
