#ifndef BSPLINE_CURVE_IO_H
#define BSPLINE_CURVE_IO_H

#include <fstream>
#include <string>
#include "BSplineCurve.h"

//! Class for BSplineCurve File IO.
template<class T>
class BSplineCurveIO {

public:

	BSplineCurveIO() {};

// ****************************************************************************************

	//! Type of a Control-point, for example a 3D Vector of floats.
	typedef T ControlPoint;

// ****************************************************************************************

	//! Write curve to file 'filename'
	static void writeCurve(BSplineCurve<ControlPoint>* curve, char* filename) {
		std::ofstream fileOut(filename);

		fileOut << curve->getDegree() << std::endl;

		if (curve->isClosed())	fileOut << "closed" << std::endl;
		else					fileOut << "open" << std::endl;

		// Knot vector:
		for (UInt i1 = 0; i1 < curve->getKnot()->size(); i1++) {
			fileOut << (*(curve->getKnot()))[i1];
			if (i1 < curve->getKnot()->size()-1)	fileOut << " ";
			else								fileOut << std::endl;
		}
		// Control-Points:
		for (UInt i1 = 0; i1 < curve->getNumCtrlPts(); i1++) {
			fileOut << curve->m_pts[i1] << std::endl;
		}
		fileOut.close();
	}

// ****************************************************************************************

	//! Creat a new curve from reading the file 'filename'
	static BSplineCurve<ControlPoint>* readCurve(char* filename) {

		std::ifstream fileIn(filename);

		// Fehler abfangen
		if (fileIn.fail()) {
			std::cerr << "Fehler beim Oeffnen der Datei!" << std::endl;
			std::cerr << "Kann Datei " << filename << " nicht lesen!" << std::endl;
			return NULL;
		}

		int degree;
		fileIn >> degree;

		std::string type;
		fileIn >> type;

		bool isClosed = false;
		if (type.compare(std::string("closed")) == 0)
			isClosed = true;

		// read one (\n)!!
		char xx[10];
		fileIn.read(xx, 1);

		std::vector<ControlPoint::ValueType> knotvalues;
		while (fileIn.peek() != '\n') {
			ControlPoint::ValueType tmp;
			fileIn >> tmp;
			knotvalues.push_back(tmp);
		}

		std::vector<ControlPoint> ctrl_pts;

		while (fileIn.peek() != EOF) {
			ControlPoint tmp;
			fileIn >> tmp;
			if (fileIn.peek() != EOF) {
				ctrl_pts.push_back(tmp);
			}
		}

		std::cerr << "Degree: " << degree << " open? " << type << std::endl;
		for (UInt i1 = 0; i1 < knotvalues.size(); i1++) {
			std::cerr << "'" << knotvalues[i1] << "' ";
		}
		std::cerr << std::endl;

		for (UInt i1 = 0; i1 < ctrl_pts.size(); i1++)
			ctrl_pts[i1].print();

		KnotVector<ControlPoint::ValueType> knot(degree, knotvalues);
		return new BSplineCurve<ControlPoint>(knot, ctrl_pts, isClosed);
	}

// ****************************************************************************************
private:

};

#endif
