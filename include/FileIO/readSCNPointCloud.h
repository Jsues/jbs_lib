#ifndef READ_SCN_POINTCLOUD_H
#define READ_SCN_POINTCLOUD_H


#include "PointCloud.h"
#include "point3d.h"
#include <iostream>
#include <fstream>


vec3f getNormalInScan(int*** scans, PointCloud<vec3f>* pc, int x, int y, int z, int x_max, int y_max) {

	
	int id = scans[z][y][x];

	vec3f current = pc->getPoints()[id];

	vec3f left, right, top, bottom;
	if (x >= 1) left = pc->getPoints()[scans[z][y][x-1]];
	if (x < x_max-1) pc->getPoints()[scans[z][y][x+1]];
	if (y >= 1) top = pc->getPoints()[scans[z][y-1][x]];
	if (y < y_max-1) bottom = pc->getPoints()[scans[z][y+1][x]];

	vec3f n;
	if (left.squaredLength() > 0 && top.squaredLength() > 0) n += (left-current)^(top-current);
	if (top.squaredLength() > 0 && right.squaredLength() > 0) n += (top-current)^(right-current);
	if (right.squaredLength() > 0 && bottom.squaredLength() > 0) n += (right-current)^(bottom-current);
	if (bottom.squaredLength() > 0 && left.squaredLength() > 0) n += (bottom-current)^(left-current);

	if (n.squaredLength() == 0) return n;
	else return n.getNormalized();
}


PointCloudNormals<vec3f>* readSCNPC(const char* fn) {

	std::ifstream fin(fn);

	if (!fin.good()) {
		std::cerr << "Error reading file " << fn << " in readSCNPC(...)" << std::endl;
		return NULL;
	}

	char tmp[400];
	fin.getline(tmp, 400);

	PointCloud<vec3f> pc;
	vec3f p;
	vec3i coords;

	int x_min = INT_MAX;
	int x_max = -INT_MAX;
	int y_min = INT_MAX;
	int y_max = -INT_MAX;
	int z_min = INT_MAX;
	int z_max = -INT_MAX;

	while(!fin.eof()) {
		fin >> p >> coords;

		p /= 1000;

		if (x_min > coords.x) x_min = coords.x;
		if (y_min > coords.y) y_min = coords.y;
		if (z_min > coords.z) z_min = coords.z;
		if (x_max < coords.x) x_max = coords.x;
		if (y_max < coords.y) y_max = coords.y;
		if (z_max < coords.z) z_max = coords.z;

		if (!fin.eof()) pc.insertPoint(p);
	}

	std::cerr << "x: [" << x_min << " : " << x_max << "]\n";
	std::cerr << "y: [" << y_min << " : " << y_max << "]\n";
	std::cerr << "z: [" << z_min << " : " << z_max << "]\n";

	int x_offset = -x_min;
	int x_tot = x_max+x_offset;

	int*** scans = new int**[z_max+1];
	for (int i1 = 0; i1 <= z_max; i1++) {
		scans[i1] = new int*[y_max+1];
		for (int i2 = 0; i2 <= y_max; i2++) {
			scans[i1][i2] = new int[x_tot+1];
			for (int i3 = 0; i3 < x_tot; i3++) {
				scans[i1][i2][i3] = -1;
			}
		}
	}

	// Jump to begin again
	fin.clear();
	fin.seekg(0, std::ios::beg);
	fin.getline(tmp, 400);

	int cnt = 0;
	while(!fin.eof()) {
		fin >> p >> coords;

		//p.print();
		//coords.print();

		if (!fin.eof()) {
			scans[coords.z][coords.y][coords.x+x_offset] = cnt;
			cnt++;
		}
	}

	fin.close();

	PointCloudNormals<vec3f>* pc2 = new PointCloudNormals<vec3f>;

	for (int i1 = 0; i1 <= z_max; i1++) {
		for (int i2 = 0; i2 <= y_max; i2++) {
			for (int i3 = 0; i3 < x_tot; i3++) {

				int id = scans[i1][i2][i3];

				if (id == -1) continue;

				vec3f n = getNormalInScan(scans, &pc, i3, i2, i1, x_tot, y_max);

				if (i1 == 0 || i1 == 2 || i1 == 4 || i1 == 6) n *= -1;

				if (n.squaredLength() > 0) {
					pc2->m_pts.push_back(pc.getPoint(id));
					pc2->m_normals.push_back(n);
				}
			}
		}
	}


	for (int i1 = 0; i1 < z_max; i1++) {
		for (int i2 = 0; i2 < y_max; i2++) {
			delete[] scans[i1][i2];
		}
		delete[] scans[i1];
	}
	delete[] scans;


	return pc2;

}


#endif
