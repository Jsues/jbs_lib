#ifndef VECFIELDWRITER_H
#define VECFIELDWRITER_H


#include <iostream>
#include <fstream>
#include "Volume.h"
#include "point3d.h"


class VecfieldWriter
{
public:
	VecfieldWriter(Volume<vec3f>* vol) {
		m_vol = vol;
	};

	//! Writes the vectorfield to a .iv file.
	/*!
		\param scale is used to scale all vectors.
	*/
	void writeIVFile(char* filename, Volume<double>* vol, float scale = 1.0f) {
		std::fstream FileOut(filename, std::ios::out); 
		if (!FileOut.is_open()) {
			std::cerr << "VecfieldWriter::writeIVFile(..): Could not write file " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		int dx = m_vol->getDimX();
		int dy = m_vol->getDimY();
		int dz = m_vol->getDimZ();

		// Write header
		FileOut << "#Inventor V2.1 ascii" << std::endl << std::endl;
		FileOut << "\tSeparator {" << std::endl;
		FileOut << "\t\tCoordinate3 {" << std::endl;
		FileOut << "\t\t\tpoint [" << std::endl;

		// Write vector start/endpoints
		for (int x = 0; x < dx; x++) {
			for (int y = 0; y < dy; y++) {
				for (int z = 0; z < dz; z++) {

					float dx2 = (float(x)*float(m_vol->max[0]-m_vol->min[0]))/float(dx);
					float dy2 = (float(y)*float(m_vol->max[1]-m_vol->min[1]))/float(dy);
					float dz2 = (float(z)*float(m_vol->max[2]-m_vol->min[2]))/float(dz);

					vec3f center = vec3f(dz2, dy2, dx2);
					vec3f vec = m_vol->get(x,y,z);

					vec3f pt0 = center - vec*scale;
					vec3f pt1 = center + vec*scale;
					FileOut << "\t\t\t" << pt0[0] << " " << pt0[1] << " " << pt0[2] << "," << std::endl;
					FileOut << "\t\t\t" << pt1[0] << " " << pt1[1] << " " << pt1[2] << "," << std::endl;
				}
			}
		}

		FileOut << "\t\t]" << std::endl << "\t}" << std::endl;

		// Write vector as lines
		FileOut << "\tIndexedLineSet {" << std::endl;
		FileOut << "\t\tcoordIndex[" << std::endl;
		int i1 = 0;
		for (int x = 0; x < dx; x++) {
			for (int y = 0; y < dy; y++) {
				for (int z = 0; z < dz; z++) {
					double density = vol->get(x,y,z);
					//if (density > 40)
						FileOut << "\t\t" << i1*2 << ", " << i1*2+1 << ", -1," << std::endl;
					i1++;
				}
			}
		}

		FileOut << "\t\t]" << std::endl;
		FileOut << "\t}" << std::endl;
		FileOut << "}" << std::endl;

		//FileOut << "PointSet { }" << std::endl << "}" << std::endl;
		FileOut.close();

	};



private:
	Volume<vec3f>* m_vol;
};

#endif
