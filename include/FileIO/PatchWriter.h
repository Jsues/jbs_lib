#ifndef PATCH_WRITER_H
#define PATCH_WRITER_H


#include <iostream>
#include <fstream>
#include "BSplinePatch.h"

//! Writes a BSplinePatch to a file.
class PatchWriter
{
public:

	//! Constructor.
	PatchWriter(){};

	//! Writes a .BSP file from a BSplinePatch.
	/*!
		File format is:<br>
		- First line:<br>
		<b>m n p q</b><br>
		where 'm' and 'n' denotes the number of controlpoints in u- and v-direction<br>
		and 'p' and 'q' denotes the degree of the patch.
		- Second and third line:<br>
		<b>U1 U2 ... UN</b><br>
		<b>V1 V2 ... VN</b><br>
		Knot vectors in u- and v-direction.
		- Following lines hold the controlpoints, X Y Z each:<br>
		<b>X1 Y1 Z1</b>
		<b>X2 Y2 Z2</b>
		<b>...</b>
		<b>Xn Yn Zn</b>.

		A simple patch (degree 2x2, four ctrlpts in u- and 4 in v-direction) would be:
		- 4 4 2 2<br>
		0 0 0 0.5 1 1 1<br>
		0 0 0 0.5 1 1 1<br>
		0 0 0<br>
		1 0 1<br>
		2 0 1<br>
		3 0 0<br>
		0 1 1<br>
		1 1 2<br>
		2 1 2<br>
		3 1 1<br>
		0 2 1<br>
		1 2 2<br>
		2 2 2<br>
		3 2 1<br>
		0 3 0<br>
		1 3 1<br>
		2 3 1<br>
		3 3 0<br>
	*/
	void writeBSPFile(char* filename, BSplinePatch<vec3f>* patch) {
		// Eingabedatei oeffnen
		std::ofstream OutFile(filename);
		// Fehler abfangen
		if (OutFile.fail()) {
			std::cerr << "Can't write to file" << std::endl;
		}

		// Erste Zeile lautet
		// m n p q
		// wobei 'm' und 'n' Anzahl der Kontrollpunkte in u- bzw. v-Richtung sind
		// und 'p' und 'q' der Grad in u- bzw. v-Richtung ist.
		int m, n, p, q;
		m = patch->getNumCtrlPtsU();
		n = patch->getNumCtrlPtsV();
		p = patch->getDegreeU();
		q = patch->getDegreeV();

		OutFile << m << " " << n << " " << p << " " << q << std::endl;
		// Jetzt kommen die Knotenvectoren:
		std::vector<float> knot_u = patch->getKnotU();
		for (unsigned int i1 = 0; i1 < knot_u.size(); i1++)
			OutFile << knot_u[i1] << " ";
		OutFile << std::endl;
		std::vector<float> knot_v = patch->getKnotV();
		for (unsigned int i1 = 0; i1 < knot_v.size(); i1++)
			OutFile << knot_v[i1] << " ";
		OutFile << std::endl;

		// Und nun die Kontrollpunkte:
		vec3f* ctrlpts = patch->getCtrlPts();
		int numctrlpts = m*n;
		for (int i1 = 0; i1 < numctrlpts; i1++)
			OutFile << ctrlpts[i1][0] << " " << ctrlpts[i1][1] << " " << ctrlpts[i1][2] << std::endl;

		OutFile.close();

	};

protected:

private:
};


#endif
