#ifndef VOLUME_IO_H
#define VOLUME_IO_H


#include <iostream>
#include <fstream>
#include "Volume.h"



//******************************************************************************************


//! Writes Volume to a file
template<class T>
class VolumeWriter
{
public:

//******************************************************************************************

	VolumeWriter(Volume<T>* vol) {
		m_vol = vol;
	};

//******************************************************************************************

	void writeRAWFile(const char* filename) {

		std::cout << "Writing file " << filename << std::endl;
		
		std::ofstream FileBin(filename, std::ios::out|std::ios::binary); 
		if (!FileBin.is_open()) {
			std::cerr << "VolumeWriter::writeRAWFile(..): Could not write file " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		int dx = m_vol->getDimX();
		int dy = m_vol->getDimY();
		int dz = m_vol->getDimZ();

		std::cerr << "Writing " << dx << " x " << dy << " x " << dz << " Volume" << std::endl;

		for (int x = 0; x < dx; x++) {
			for (int y = 0; y < dy; y++) {
				FileBin.write((char*)&(m_vol->getData()[m_vol->getPosFromTuple(x,y,0)]), dz*sizeof(T)*m_vol->m_dim);
			}
		}
		FileBin.close();

		return;
	};

//******************************************************************************************

	void writeRAWFilePSP(const char* filename) {

		std::cout << "Writing file " << filename << std::endl;
		
		std::ofstream FileBin(filename, std::ios::out|std::ios::binary); 
		if (!FileBin.is_open()) {
			std::cerr << "VolumeWriter::writeRAWFile(..): Could not write file " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		int dx = m_vol->getDimX();
		int dy = m_vol->getDimY();
		int dz = m_vol->getDimZ();

		std::cerr << "Writing " << dx << " x " << dy << " x " << dz << " Volume" << std::endl;

		for (int z = 0; z < dz; z++) {
			for (int x = 0; x < dx; x++) {
				for (int y = 0; y < dy; y++) {
					T data = m_vol->get(x,y,z);
					FileBin.write((char*)&data, sizeof(T)*m_vol->m_dim);
				}
			}
		}

		FileBin.close();

		return;
	};

//******************************************************************************************

protected:

private:
	Volume<T>* m_vol;
};


//******************************************************************************************


//! Reads a Volume from a .raw file
template<class T>
class VolumeReader
{
public:

//******************************************************************************************

	VolumeReader() {
	};

//******************************************************************************************

	Volume<T>* readRAWFile(char* filename, int& dx) {

		std::fstream FileBin(filename, std::ios::in|std::ios::binary); 
		if (!FileBin.is_open()) {
			std::cerr << "VolumeReader::readRAWFile(..): Could not read file " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		// Check size:
		FileBin.seekg( 0, std::ios::end );
		long fileSize = FileBin.tellg();

		fileSize /= sizeof(T);

		FileBin.close();

		double dx_d = pow((double)fileSize, 1.0/3.0);
		dx = (int) dx_d;

		if (dx_d-dx > 0.5)
			dx++;

		std::cerr << "assuming cubic volume " << dx << "x" << dx << "x" << dx << std::endl;

        return readRAWFile(filename, dx, dx, dx);
	}

//******************************************************************************************

	Volume<T>* readRAWFileReverse(char* filename, int dx, int dy, int dz) {

		std::fstream FileBin(filename, std::ios::in|std::ios::binary); 
		if (!FileBin.is_open()) {
			std::cerr << "VolumeReader::readRAWFile(..): Could not read file " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		// Check size:
		FileBin.seekg( 0, std::ios::end );
		long fileSize = FileBin.tellg();
		FileBin.seekg( 0, std::ios::beg );
		int expectedFileSize = dx*dy*dz*sizeof(T);

		if (expectedFileSize != fileSize) {
			std::cerr << "Wrong filesize in VolumeReader::readRAWFile(...)";
			std::cerr << "expected filesize: " << expectedFileSize << " filesize: " << fileSize << std::endl;
			std::cerr << "exit" << std::endl;
			exit(EXIT_FAILURE);
		}

		Volume<T>* vol = new Volume<T>(vec3f(0,0,0), vec3f(1,1,1), dx, dy, dz);
		vol->init();

		T data;

		std::cerr << "Reading file";
		for (int z = 0; z < dz; z++) {
			//std::cerr << ".";
			for (int y = 0; y < dy; y++) {
				for (int x = 0; x < dx; x++) {
					FileBin.read((char*)&data, sizeof(T));
					//std::cerr << "data: " << data << std::endl;
					vol->set(x,y,z, data);

					if (data > vol->maxValue)
						vol->maxValue = data;
				}
			}
		}
		std::cerr << "done" << std::endl;
		FileBin.close();

		return vol;
	};

//******************************************************************************************

	Volume<T>* readRAWFile(char* filename, int dx, int dy, int dz) {

		std::fstream FileBin(filename, std::ios::in|std::ios::binary); 
		if (!FileBin.is_open()) {
			std::cerr << "VolumeReader::readRAWFile(..): Could not read file " << filename << std::endl;
			exit(EXIT_FAILURE);
		}

		// Check size:
		FileBin.seekg( 0, std::ios::end );
		long fileSize = FileBin.tellg();
		FileBin.seekg( 0, std::ios::beg );
		int expectedFileSize = dx*dy*dz*sizeof(T);

		if (expectedFileSize != fileSize) {
			std::cerr << "Wrong filesize in VolumeReader::readRAWFile(...)";
			std::cerr << "expected filesize: " << expectedFileSize << " filesize: " << fileSize << std::endl;
			std::cerr << "exit" << std::endl;
			exit(EXIT_FAILURE);
		}

		Volume<T>* vol = new Volume<T>(vec3f(0,0,0), vec3f(1,1,1), dx, dy, dz);
		vol->init();

		T data;

		std::cerr << "Reading file";
		for (int x = 0; x < dx; x++) {
			//std::cerr << ".";
			for (int y = 0; y < dy; y++) {
				for (int z = 0; z < dz; z++) {
					FileBin.read((char*)&data, sizeof(T));
					//std::cerr << "data: " << data << std::endl;
					vol->set(x,y,z, data);

					if (data > vol->maxValue)
						vol->maxValue = data;
				}
			}
		}
		std::cerr << "done" << std::endl;
		FileBin.close();

		return vol;
	};

//******************************************************************************************

protected:

private:
	Volume<double>* m_vol;
};

#endif
