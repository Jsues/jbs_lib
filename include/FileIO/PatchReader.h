#ifndef PATCH_READER_H
#define PATCH_READER_H


#include <iostream>
#include <fstream>
#include "BSplinePatch.h"

//! Creates a BSplinePatch from a file.
class PatchReader
{
public:

	//! Constructor.
	PatchReader(){};

	//! Creates a BSplinePatch from a .BSP file.
	/*!
		File format is:<br>
		- First line:<br>
		<b>m n p q</b><br>
		where 'm' and 'n' denotes the number of controlpoints in u- and v-direction<br>
		and 'p' and 'q' denotes the degree of the patch.
		- Second and third line:<br>
		<b>U1 U2 ... UN</b><br>
		<b>V1 V2 ... VN</b><br>
		Knot vectors in u- and v-direction.
		- Following lines hold the controlpoints, X Y Z each:<br>
		<b>X1 Y1 Z1</b>
		<b>X2 Y2 Z2</b>
		<b>...</b>
		<b>Xn Yn Zn</b>.

		A simple patch (degree 2x2, four ctrlpts in u- and 4 in v-direction) would be:
		- 4 4 2 2<br>
		0 0 0 0.5 1 1 1<br>
		0 0 0 0.5 1 1 1<br>
		0 0 0<br> !Important, x first!
		1 0 1<br>
		2 0 1<br>
		3 0 0<br>
		0 1 1<br>
		1 1 2<br>
		2 1 2<br>
		3 1 1<br>
		0 2 1<br>
		1 2 2<br>
		2 2 2<br>
		3 2 1<br>
		0 3 0<br>
		1 3 1<br>
		2 3 1<br>
		3 3 0<br>
	*/
	BSplinePatch<vec3f>* readBSPFile(char* filename) {
		// Eingabedatei oeffnen
		std::ifstream InFile(filename);
		// Fehler abfangen
		if (InFile.fail())
		{
			std::cerr << "Fehler beim Oeffnen der Datei!" << std::endl;
			return NULL;
		}

		// Erste Zeile lautet
		// m n p q
		// wobei 'm' und 'n' Anzahl der Kontrollpunkte in u- bzw. v-Richtung sind
		// und 'p' und 'q' der Grad in u- bzw. v-Richtung ist.
		int m, n, p, q;
		InFile >> m >> n >> p >> q;
		// Jetzt kommen die Knotenvectoren:
		std::vector<float> knot_u;
		for (int i1 = 0; i1 < m+p+1; i1++) {
			float tmp;
			InFile >> tmp;
			knot_u.push_back(tmp);
		}
		std::vector<float> knot_v;
		for (int i1 = 0; i1 < n+q+1; i1++) {
			float tmp;
			InFile >> tmp;
			knot_v.push_back(tmp);
		}

		// Und nun die Kontrollpunkte:
		std::vector<vec3f> ctrlpts;
		for (int i1 = 0; i1 < m*n; i1++) {
			vec3f pt;
			InFile >> pt[0] >> pt[1] >> pt[2];
			ctrlpts.push_back(pt);
		}
		// Fertig eingelesen.
		InFile.close();
		ControlGrid<vec3f> grid(m,n, ctrlpts);
		grid.print();


		BSplinePatch<vec3f>* patch = new BSplinePatch<vec3f>(p, q, knot_u, knot_v, grid);
		return patch;

	};

protected:

private:
};


#endif
