#ifndef JBS_QUADRIC_H
#define JBS_QUADRIC_H


#include "JBS_General.h"
#include "point3d.h"
#include "point4d.h"
#include "MatrixnD.h"

// ****************************************************************************************


//! A quadric a*x� + b*y� + c*z� + d*xyz + e*xy + f*xz + g*yz + h*x + i*y + j*z + k;
template <class T>
class Quadric3D {

public:

	//! Value-Type of Templates components.
	typedef T ValueType; 

	//! Type of a 3D point.
	typedef point3d<ValueType> Point;

	Quadric3D(ValueType a_, ValueType b_, ValueType c_, ValueType d_, ValueType e_, ValueType f_, ValueType g_, ValueType h_, ValueType i_, ValueType j_, ValueType k_) {
		set(a_, b_, c_, d_, e_, f_, g_, h_, i_, j_, k_);
	}

	void set(ValueType a_, ValueType b_, ValueType c_, ValueType d_, ValueType e_, ValueType f_, ValueType g_, ValueType h_, ValueType i_, ValueType j_, ValueType k_) {
		a = a_;
		b = b_;
		c = c_;
		d = d_;
		e = e_;
		f = f_;
		g = g_;
		h = h_;
		i = i_;
		j = j_;
		k = k_;
	}

	inline ValueType eval(const Point p) const {
		return 
			a*p.x*p.x + b*p.y*p.y + c*p.z*p.z +
			d*p.x*p.y*p.z +
			e*p.x*p.y + f*p.x*p.z + g*p.y*p.z +
			h*p.x + i*p.y + j*p.z +
			k;
	};

	inline ValueType eval(const ValueType x, const ValueType y, const ValueType z) const {
		return eval(Point(x,y,z));
	};

	void setMinMax(Point min_p_, Point max_p_) {
		min_p = min_p_;
		max_p = max_p_;
	}

	void setNumSamples(UInt numSamples_) {
		numSamples = numSamples_;
	}

	inline point3d<ValueType> pos(const UInt x, const UInt y, const UInt z) const {
		Point pos;
		ValueType s = ((ValueType)x)/numSamples;
		ValueType t = ((ValueType)y)/numSamples;
		ValueType u = ((ValueType)z)/numSamples;

		pos.x = min_p.x*(1-s) + max_p.x*s;
		pos.y = min_p.y*(1-t) + max_p.y*t;
		pos.z = min_p.z*(1-u) + max_p.z*u;

		return pos;
	}

	inline UInt getNumSamples() const {
		return numSamples;
	}

protected:

	//! Coefficients
	ValueType a,b,c,d,e,f,g,h,i,j,k;

	Point min_p, max_p;

	UInt numSamples;
};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


//! A rotating quadric.
template <class T>
class RotQuadric3D {

public:

	//! Value-Type of Templates components.
	typedef T ValueType; 

	//! Type of a 3D point.
	typedef point3d<ValueType> Point;
	//! Type of a 4D point.
	typedef point4d<ValueType> Point4D;

// ****************************************************************************************

	//! Constructor
	RotQuadric3D(ValueType a1, ValueType a2, ValueType a3, ValueType a4, ValueType a5, ValueType a6, ValueType a7, ValueType a8, ValueType a9, ValueType a10) {
		a = a1;
		b = a2;
		c = a3;
		d = a4;
		e = a5;
		f = a6;

		g = a7;
		h = a8;
		i = a9;

		j = a10;
	}

// ****************************************************************************************

	inline ValueType eval(const Point p) const {
		return 
			a*p.x*p.x + 2*b*p.y*p.x + 2*c*p.x*p.z +
			d*p.y*p.y + 2*e*p.y*p.z +
			f*p.z*p.z +

			g*p.x +
			h*p.y +
			i*p.z +

			j;
	};

// ****************************************************************************************

	inline Point4D evalGradRot(const Point4D p) const {

		ValueType w = p.w; ValueType x = p.x; ValueType y = p.y; ValueType z = p.z;
		ValueType cos_ = cos(w);
		ValueType sin_ = sin(w);

		ValueType dx =  2*a*(cos_*x-sin_*y)*cos_ + 2*d*(sin_*x+cos_*y)*sin_;
		ValueType dy = -2*a*(cos_*x-sin_*y)*sin_ + 2*d*(sin_*x+cos_*y)*cos_;
		ValueType dz =  2*f*z;
		ValueType dw =  2*a*(cos_*x-sin_*y)*(-sin_*x-cos_*y) + 2*d*(sin_*x+cos_*y)*(cos_*x-sin_*y);


		float temp_a = a * (1-w*3);
		dx = 2*a*x*(1-w*3);
		dy = 2*d*y;
		dz = 2*f*z;
		dw = -3*a*x*x;

		return Point4D(dx, dy, dz, dw);
	}

// ****************************************************************************************

	inline ValueType eval(const Point p, ValueType angle) const {

		ValueType w = angle; ValueType x = p.x; ValueType y = p.y; ValueType z = p.z;
		ValueType cos_ = cos(w);
		ValueType sin_ = sin(w);

		float temp_a = a * (1-angle*3);

//		ValueType ret = a*(cos_*x-sin_*y)*(cos_*x-sin_*y) + d*(sin_*x+cos_*y)*(sin_*x+cos_*y) + f*z*z + j;
		ValueType ret = temp_a*x*x + d*y*y + f*z*z + j;
		return ret;
	}

// ****************************************************************************************

	inline void getMinMax(Point& minV, Point& maxV) {

		SquareMatrixND<Point> mat;
		mat(0,0) = a; mat(1,0) = b; mat(2,0) = c;
		mat(0,1) = b; mat(1,1) = d; mat(2,1) = e;
		mat(0,2) = c; mat(1,2) = e; mat(2,2) = f;


		SquareMatrixND<Point> system;
		vec3f e_vs = mat.calcEValuesAndVectors(system);
	}

// ****************************************************************************************

private:

	//! Coefficients
	ValueType a,b,c,d,e,f,  g,h,i,  j;
};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


//! A time-varying quadric that does some wicked stuff;
template <class T>
class TimeQuadric3D : public Quadric3D<T> {

public:

	//! Value-Type of Templates components.
	typedef T ValueType; 

	//! Type of a 3D point.
	typedef point3d<ValueType> Point;

	//! Type of a 4D point.
	typedef point4d<ValueType> Point4;

	TimeQuadric3D(ValueType a_, ValueType b_, ValueType c_, ValueType d_, ValueType e_, ValueType f_, ValueType g_, ValueType h_, ValueType i_, ValueType j_, ValueType k_) : Quadric3D<ValueType>(a_, b_, c_, d_, e_, f_, g_, h_, i_, j_, k_) {
	}

	inline ValueType eval(const Point4 p) const {

		ValueType factor = (ValueType)0.001;
		ValueType w_1 = p.w*factor;
		ValueType w_2 = p.w*factor*p.w*factor;
		ValueType w_3 = p.w*factor*p.w*factor*p.w*factor;

		return 10*
			a*p.x*p.x*w_1 + b*p.y*p.y/(w_1*w_1) + c*p.z*p.z*w_1 +
			d*p.x*p.y*p.z*w_3 +
			e*p.x*p.y*w_2 + f*p.x*p.z/w_2 + g*p.y*p.z*w_2 +
			h*p.x + i*p.y + j*p.z +
			k;
	};

	inline ValueType eval(const Point p, ValueType time) const {
		Point4 p4(p.x, p.y, p.z, time);

		return eval(p4);

	};

};

#endif
