#include "SimpleMesh.h"
#include "FileIO/MeshReader.h"

struct QHullPolygon {
	std::vector<UInt> verts;
	void insert(UInt v) { verts.push_back(v); }
};

void writeMeshVerticesToQHullFile(char* filename, SimpleMesh* sm) {
	std::ofstream fout(filename);

	fout << "3 " << sm->getNumV() << std::endl;
	for (UInt i1 = 0; i1 < (UInt)sm->getNumV(); i1++) {
		vec3d pt = sm->vList[i1].c;
		fout << pt.x << " " << pt.y << " " << pt.z << std::endl;
	}
	fout.close();
}

std::vector<QHullPolygon> getHullPolys(SimpleMesh* sm) {

	// Write mesh vertices to file.
	writeMeshVerticesToQHullFile("points.qhull", sm);

	// call qhull
	char* systemcall = "qconvex.exe o <points.qhull >hull.off";
	int Errcode = system(systemcall);
	if (Errcode == 1) {
		std::cerr << systemcall << std::endl;
		std::cerr << "errcode: " << Errcode << std::endl;
		std::cerr << "EXIT" << std::endl;
		exit(EXIT_FAILURE);
	}

	std::vector<QHullPolygon> polygons;
	std::ifstream InFile("hull.off");

	char string1[5];
	InFile >> string1;

	int numV = 0;
	int numT = 0;
	int numE = 0;

	InFile >> numV >> numT >> numE;

	// Read points
	double x, y, z;
	for(int i1 = 0; i1 < numV; i1++) {
		InFile >> x >> y >> z;
	}
	int num_vs, v;
	for(int i1 = 0; i1 < numT; i1++) {
		QHullPolygon poly;
		InFile >> num_vs;
		for (int i2 = 0; i2 < num_vs; i2++) {
			InFile >> v;
			poly.insert(v);
		}
		polygons.push_back(poly);
	}

	return polygons;
}

SimpleMesh* getHullMesh(SimpleMesh* sm) {

	// Write mesh vertices to file.
	writeMeshVerticesToQHullFile("points.qhull", sm);

	// call qhull
	char* systemcall = "qconvex.exe o <points.qhull >hull.off";
	int Errcode = system(systemcall);
	if (Errcode == 1) {
		std::cerr << systemcall << std::endl;
		std::cerr << "errcode: " << Errcode << std::endl;
		std::cerr << "EXIT" << std::endl;
		exit(EXIT_FAILURE);
	}

	// read mesh from file
	MeshReader mr;
	SimpleMesh* sm2 = mr.readOFFFile("hull.off");

	return sm2;
}