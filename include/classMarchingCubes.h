#ifndef CLASS_MARCHING_CUBES_H
#define CLASS_MARCHING_CUBES_H

#include "point3d.h"

#define EPSILON_ZERO 0.00000001

namespace JBSlib {

	struct MC_Triangle {
	   vec3f p[3];
	};

	//****************************************************************************************************************

	template <class VEC>
	struct MC_Gridcell{
	   VEC p[8];
	   typename VEC::ValueType val[8];
	};

	//****************************************************************************************************************

	template<class VEC>
	class MarchingCubes {

	public:
		typedef MC_Gridcell<VEC> MC_CG;

		template <class T, class U>
		inline U VertexInterp(T iso, const U& cellpos0, const U& cellpos1, T celliso0, T celliso1) {
			
			if (std::abs(iso-celliso0) < (T)EPSILON_ZERO) {
				//std::cerr << "iso = cellilo0" << std::endl;
				//std::cerr << "val 0: " << celliso0 << " val 1: " << celliso1 << " iso: " << iso << std::endl;
				return(cellpos0);
			}
			if (std::abs(iso-celliso1) < (T)EPSILON_ZERO) {
				//std::cerr << "iso = cellilo0" << std::endl;
				return(cellpos1);
			}
			if (std::abs(celliso0-celliso1) < (T)EPSILON_ZERO) {
				//std::cerr << "celliso1 = cellilo0" << std::endl;
				return(cellpos0);
			}

			U temp_v0, temp_v1;
			T temp_val0, temp_val1;

			if (celliso0 < iso) {
				temp_v0 = cellpos0;
				temp_v1 = cellpos1;
				temp_val0 = celliso0;
				temp_val1 = celliso1;
			} else {
				temp_v0 = cellpos1;
				temp_v1 = cellpos0;
				temp_val0 = celliso1;
				temp_val1 = celliso0;
			}

			T mu = (iso - temp_val0) / (temp_val1 - temp_val0);

			U interp = temp_v0 + (temp_v1-temp_v0)*mu;

//			std::cerr << cellpos0 << " (" << celliso0 << ")  " << cellpos1 << " (" << celliso1 << ")  =  " << interp << std::endl;

			return interp;
		}


		/*
		   Given a grid cell and an isolevel, calculate the triangular
		   facets required to represent the isosurface through the cell.
		   Return the number of triangular facets, the array "triangles"
		   will be loaded up with the vertices at most 5 triangular facets.
			0 will be returned if the grid cell is either totally above
		   of totally below the isolevel.
		*/
		int Polygonise() {
		   
			int ntriang;
			int cubeindex;

			cubeindex = 0;
			if (grid.val[0] < isolevel) cubeindex |= 1;
			if (grid.val[1] < isolevel) cubeindex |= 2;
			if (grid.val[2] < isolevel) cubeindex |= 4;
			if (grid.val[3] < isolevel) cubeindex |= 8;
			if (grid.val[4] < isolevel) cubeindex |= 16;
			if (grid.val[5] < isolevel) cubeindex |= 32;
			if (grid.val[6] < isolevel) cubeindex |= 64;
			if (grid.val[7] < isolevel) cubeindex |= 128;

			/* Cube is entirely in/out of the surface */
			if (edgeTable[cubeindex] == 0)
				return 0;

			/* Find the vertices where the surface intersects the cube */
			if (edgeTable[cubeindex] & 1)
				vertlist[0] = VertexInterp(isolevel,grid.p[0],grid.p[1],grid.val[0],grid.val[1]);
			if (edgeTable[cubeindex] & 2)
				vertlist[1] = VertexInterp(isolevel,grid.p[1],grid.p[2],grid.val[1],grid.val[2]);
			if (edgeTable[cubeindex] & 4)
				vertlist[2] = VertexInterp(isolevel,grid.p[2],grid.p[3],grid.val[2],grid.val[3]);
			if (edgeTable[cubeindex] & 8)
				vertlist[3] = VertexInterp(isolevel,grid.p[3],grid.p[0],grid.val[3],grid.val[0]);
			if (edgeTable[cubeindex] & 16)
				vertlist[4] = VertexInterp(isolevel,grid.p[4],grid.p[5],grid.val[4],grid.val[5]);
			if (edgeTable[cubeindex] & 32)
				vertlist[5] = VertexInterp(isolevel,grid.p[5],grid.p[6],grid.val[5],grid.val[6]);
			if (edgeTable[cubeindex] & 64)
				vertlist[6] = VertexInterp(isolevel,grid.p[6],grid.p[7],grid.val[6],grid.val[7]);
			if (edgeTable[cubeindex] & 128)
				vertlist[7] = VertexInterp(isolevel,grid.p[7],grid.p[4],grid.val[7],grid.val[4]);
			if (edgeTable[cubeindex] & 256)
				vertlist[8] = VertexInterp(isolevel,grid.p[0],grid.p[4],grid.val[0],grid.val[4]);
			if (edgeTable[cubeindex] & 512)
				vertlist[9] = VertexInterp(isolevel,grid.p[1],grid.p[5],grid.val[1],grid.val[5]);
			if (edgeTable[cubeindex] & 1024)
				vertlist[10] = VertexInterp(isolevel,grid.p[2],grid.p[6],grid.val[2],grid.val[6]);
			if (edgeTable[cubeindex] & 2048)
				vertlist[11] = VertexInterp(isolevel,grid.p[3],grid.p[7],grid.val[3],grid.val[7]);

			/* Create the triangle */
			ntriang = 0;
			for (int i=0; triTable[cubeindex][i] != -1; i+=3) {
				tris[ntriang].p[0] = vertlist[triTable[cubeindex][i  ]];
				tris[ntriang].p[1] = vertlist[triTable[cubeindex][i+1]];
				tris[ntriang].p[2] = vertlist[triTable[cubeindex][i+2]];
				ntriang++;
			}

			return(ntriang);

		}

		MC_CG grid;
		typename VEC::ValueType isolevel;
		MC_Triangle tris[6];
		VEC vertlist[12];

	private:

		static const int edgeTable[256];
		static const int triTable[256][16];

	};

};

#endif
