#ifndef COMPUTE_ISO_OCTREE_H
#define COMPUTE_ISO_OCTREE_H


#include "ScalarFunction.h"
#include "PointCloud.h"

// Begin Misha Kazhdan Include
#include "IsoOctree.h"
#include "VertexData.h"
#include "Ply.h"
#include "MishaTime.h"
#include "MAT.h"
template<class VertexData,class T>
class MyNodeData
{
public:
	int mcIndex;
	Point3D<T> center;
	VertexData v;
};
// End Misha Kazhdan Include


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class IsoOctreeBuilder {

public:

// ****************************************************************************************

	typedef OctNode<MyNodeData<VertexValue<T>,T>,T>	MyOctNode;
	typedef IsoOctree<MyNodeData<VertexValue<T>,T>,T,VertexValue<T>>			MyTreeType;
	typedef point3d<T> vec3;
	typedef Point3D<T> point3;
	static const UInt MAX_VERTS_PER_CELL = 3;

// ****************************************************************************************

	bool PointInCube(point3 ctr, T w, vec3 p) {
		if(p[0]<ctr.coords[0]-w/2) return false;
		if(p[0]>ctr.coords[0]+w/2) return false;
		if(p[1]<ctr.coords[1]-w/2) return false;
		if(p[1]>ctr.coords[1]+w/2) return false;
		if(p[2]<ctr.coords[2]-w/2) return false;
		if(p[2]>ctr.coords[2]+w/2) return false;

		return true;
	}

// ****************************************************************************************

	void setChildren(std::vector<UInt>& indices, MyOctNode* node, typename MyOctNode::NodeIndex nIdx) {

		long long key;
		T w;
		point3 ctr,g,p;
		if (indices.size() < MAX_VERTS_PER_CELL) return;
		if (nIdx.depth == m_maxTreeDepth)	return;

		if(!node->children)	node->initChildren();
		MyOctNode::CenterAndWidth(nIdx,ctr,w);

		//std::cerr << "Init node at " << ctr[0] << " " << ctr[1] << " " << ctr[2] << "  with width: " << w << std::endl;

		// Set center
		key=MyOctNode::CenterIndex(nIdx,m_maxTreeDepth);
		vec3 grad;
		T val = m_func->evalValAndGrad(vec3(ctr.coords), grad);
		g.coords[0] = grad[0]; g.coords[1] = grad[1]; g.coords[2] = grad[2];
		m_tree->cornerValues[key]=VertexValue<T>(val, g);

		// Set the edge mid-points
		for(int i=0;i<Cube::EDGES;i++) {
			int o,i1,i2;
			Cube::FactorEdgeIndex(i,o,i1,i2);
			p=ctr;
			p[0]-=w/2;
			p[1]-=w/2;
			p[2]-=w/2;
			p[o]=ctr[o];
			switch(o) {
			case 0:
				p[1]+=w*i1;
				p[2]+=w*i2;
				break;
			case 1:
				p[0]+=w*i1;
				p[2]+=w*i2;
				break;
			case 2:
				p[0]+=w*i1;
				p[1]+=w*i2;
				break;
			}
			key=MyOctNode::EdgeIndex(nIdx,i,m_maxTreeDepth);

			val = m_func->evalValAndGrad(vec3(p.coords), grad);
			g.coords[0] = grad[0]; g.coords[1] = grad[1]; g.coords[2] = grad[2];

			if( m_tree->cornerValues.find(key)==m_tree->cornerValues.end() || fabs(m_tree->cornerValues[key].value())>fabs(val) )
				m_tree->cornerValues[key]=VertexValue<T>(val, g);
		}

		// set the face mid-points
		for(int i=0;i<Cube::FACES;i++) {
			int dir,off;
			Cube::FactorFaceIndex(i,dir,off);
			p=ctr;
			p[dir]+=-w/2+w*off;
			key=MyOctNode::FaceIndex(nIdx,i,m_maxTreeDepth);

			val = m_func->evalValAndGrad(vec3(p.coords), grad);
			g.coords[0] = grad[0]; g.coords[1] = grad[1]; g.coords[2] = grad[2];

			if( m_tree->cornerValues.find(key)==m_tree->cornerValues.end() || fabs(m_tree->cornerValues[key].value())>fabs(val) )
				m_tree->cornerValues[key]=VertexValue<T>(val, g);
		}

		// Propagate to children
		int retCount=0;
		for(int i=0;i<Cube::CORNERS;i++) {
			std::vector<UInt> new_indices;
			MyOctNode::CenterAndWidth(nIdx.child(i),ctr,w);

			for(UInt j=0; j<indices.size(); j++) {
				point3 ctr2;
				T w2=w;
				ctr2[0]=ctr[0];
				ctr2[1]=ctr[1];
				ctr2[2]=ctr[2];
				if(PointInCube(ctr2,w2,points[indices[j]]))
					new_indices.push_back(indices[j]);
			}
			setChildren(new_indices, &node->children[i],nIdx.child(i));
			if(new_indices.size()) retCount++;
		}

	}

// ****************************************************************************************

	MyTreeType* getIsoOctree(UInt maxTreeDepth, ScalarFunction<vec3>* func, PointCloud<vec3>* pc) {
		m_maxTreeDepth = maxTreeDepth;
		m_func = func;
		m_pc = pc;
		points = m_pc->getPoints();

		// Initialize root cell
		m_tree = new MyTreeType;
		m_tree->maxDepth = m_maxTreeDepth;

		MyOctNode::NodeIndex nIdx;
		vec3 pos;
		vec3 grad;
		T val;
		point3 g;

		// Set corners
		for(int c=0;c<Cube::CORNERS;c++) {
			int x,y,z;
			Cube::FactorCornerIndex(c,x,y,z);

			pos = vec3(T(x), T(y), T(z));

			//std::cerr << "c: " << c << "  " << p[0] << " " << p[1] << " " << p[2] << std::endl;
			val = func->evalValAndGrad(pos, grad);
			g.coords[0] = grad[0]; g.coords[1] = grad[1]; g.coords[2] = grad[2];
			m_tree->cornerValues[MyOctNode::CornerIndex(nIdx,c,m_maxTreeDepth)]=VertexValue<T>(val, g);
		}
		if(!m_tree->tree.children) m_tree->tree.initChildren();


		std::vector<UInt> indices;
		for (UInt i1 = 0; i1 < (UInt)m_pc->getNumPts(); i1++) indices.push_back(i1);

		setChildren(indices, &m_tree->tree, nIdx);
		// propagate to children:

		return m_tree;
	}

// ****************************************************************************************

private:

	UInt m_maxTreeDepth;
	ScalarFunction<vec3>* m_func;
	PointCloud<vec3>* m_pc;
	vec3* points;
	MyTreeType* m_tree;

};



#endif
