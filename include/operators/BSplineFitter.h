#ifndef BSPLINE_FITTER_H
#define BSPLINE_FITTER_H

#include "JBS_General.h"
#include "BSplineCurve.h"
#include "SVD_Matrix.h"


template<class Point>
BSplineCurve<Point>* fitBSpline(Point* pts, typename Point::ValueType* params, UInt numPts, UInt degree, UInt numCtrlPts) {

	typedef Point::ValueType ValueType;


	KnotVector<ValueType> knot(degree, numCtrlPts);
	std::vector<Point> ctrl_pts;
	ctrl_pts.resize(numCtrlPts);

	SVDMatrix<ValueType> mat_A(numPts, numCtrlPts);

	for (UInt n = 0; n < numPts; n++) {
		for (UInt m = 0; m < numCtrlPts; m++) {
			mat_A(n,m) = knot.evalBasis(m, params[n]);
		}
	}

	mat_A.computeSVD();

	ValueType* x = new ValueType[numCtrlPts+1];
	ValueType* b = new ValueType[numPts+1];
	for (UInt i1 = 0; i1 < Point::dim; i1++) {
		for (UInt n = 0; n < numPts; n++) b[n+1] = pts[n][i1];
		mat_A.backSolveSVD(b, x);
		for (UInt n = 0; n < numCtrlPts; n++)
			ctrl_pts[n][i1] = x[n+1];
	}
	delete[] b;
	delete[] x;


	BSplineCurve<Point>* curve = new BSplineCurve<Point>(knot, ctrl_pts, false);
	return curve;
}

#endif
