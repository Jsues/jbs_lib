#include "../SimpleMesh.h"


//! Scales and moves a mesh such that it lies entirely in the range of [0..1]^3
void normalizeMesh(SimpleMesh* mesh) {

	// Compute extend:
	vec3d min = 1.e10;
	vec3d max = -1.e10;

	for (unsigned int i1 = 0; i1 < mesh->vList.size(); i1++) {

		double x = mesh->vList[i1].c.x;
		double y = mesh->vList[i1].c.y;
		double z = mesh->vList[i1].c.z;

		if(x < min[0])
			min[0] = x;
		if(x > max[0])
			max[0] = x;
		if(y < min[1])
			min[1] = y;
		if(y > max[1])
			max[1] = y;
		if(z < min[2])
			min[2] = z;
		if(z > max[2])
			max[2] = z;
	}

	vec3d diag = max-min;
	min.print();
	max.print();
	diag.print();

	double maxx = diag.x;
	if (diag.y > maxx) maxx = diag.y;
	if (diag.z > maxx) maxx = diag.z;

	for (unsigned int i1 = 0; i1 < mesh->vList.size(); i1++) {

		vec3d v = mesh->vList[i1].c;

		v -= min;
		v.x /= maxx;
		v.y /= maxx;
		v.z /= maxx;

		mesh->vList[i1].c = v;
	}

}