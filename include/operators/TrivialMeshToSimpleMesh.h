#ifndef TRIVIALMESH_TO_SIMPLE_MESH_H
#define TRIVIALMESH_TO_SIMPLE_MESH_H


#include "TrivialMesh.h"
#include "SimpleMesh.h"
#include "jbsoctree.h"
#include "FindDuplicatesOctree.h"



template <class T>
SimpleMesh* TrivialMeshToSimpleMesh(TrivialMesh<T>* tm) {

	SimpleMesh* sm = new SimpleMesh;

	const int dim = 3;
	UInt numPts = tm->getNumTris()*3;

	T p0, p1, p2;

	T min, max;

	tm->ComputeMinAndMaxExt(min, max);
	T diag = (max-min)/50;
	min -= diag;
	max += diag;

	JBSlib::DFOcTree< JBSlib::DFOcNode<T> > octree(10, min, max);

	// Fill Data points:
	for (UInt i1 = 0; i1 < tm->getNumTris(); i1++) {
		p0 = tm->tris[i1].v0;
		p1 = tm->tris[i1].v1;
		p2 = tm->tris[i1].v2;

		//std::cerr << i1 << std::endl;
		UInt pos_v0, pos_v1, pos_v2;
		
		if (octree.insertObject(pos_v0, p0))
			sm->insertVertex(vec3d(p0.x, p0.y, p0.z));
		if (octree.insertObject(pos_v1, p1))
			sm->insertVertex(vec3d(p1.x, p1.y, p1.z));
		if (octree.insertObject(pos_v2, p2))
			sm->insertVertex(vec3d(p2.x, p2.y, p2.z));

        sm->insertTriangle(pos_v0, pos_v1, pos_v2);
	}

	std::cerr << "Added " << octree.pointsAdded << " points, " << octree.pointsDuplicate << " were duplicates" << std::endl;

    return sm;
}


// ****************************************************************************************


template <class T>
SimpleMesh* TrivialMeshToSimpleMeshUseOldOctree(TrivialMesh<T>* tm) {

	SimpleMesh* sm = new SimpleMesh;

	const int dim = 3;
	UInt numPts = tm->getNumTris()*3;

	T p0, p1, p2;

	T min, max;

	tm->ComputeMinAndMaxExt(min, max);
	T diag = (max-min)/50;
	min -= diag;
	max += diag;

	JBSlib::OcTree<JBSlib::OcPoint3<typename T::ValueType> > octree(numPts, 10, min, max);

	// Fill Data points:
	for (UInt i1 = 0; i1 < tm->getNumTris(); i1++) {
		p0 = tm->tris[i1].v0;
		p1 = tm->tris[i1].v1;
		p2 = tm->tris[i1].v2;

		//std::cerr << i1 << std::endl;
		UInt pos_v0, pos_v1, pos_v2;
		
		if (octree.insertObjectAvoidMultiple(pos_v0, p0))
			sm->insertVertex(vec3d(p0.x, p0.y, p0.z));
		if (octree.insertObjectAvoidMultiple(pos_v1, p1))
			sm->insertVertex(vec3d(p1.x, p1.y, p1.z));
		if (octree.insertObjectAvoidMultiple(pos_v2, p2))
			sm->insertVertex(vec3d(p2.x, p2.y, p2.z));

        sm->insertTriangle(pos_v0, pos_v1, pos_v2);
	}

	std::cerr << "Added " << octree.pointsAdded << " points, " << octree.pointsDuplicate << " were duplicates" << std::endl;

	//for (UInt i1 = 0; i1 < octree.cells.size(); i1++) {
	//	JBSlib::OcNode<JBSlib::OcPoint3<T::ValueType> >* node = octree.cells[i1];
	//	if (node->isLeafNode()) {
	//		node->printContent();
	//	}
	//}

	for (UInt i1 = 0; i1 < tm->getNumTris(); i1++) {
	}

    return sm;
}


#endif
