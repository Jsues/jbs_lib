#ifndef COMPUTE_ISO_OCTREE_MT_H
#define COMPUTE_ISO_OCTREE_MT_H


#include <boost/thread/thread.hpp>
#include "operators/computeIsoOctree.h"

template <class T> class IsoOctreeBuilderMT;

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

#ifndef UINT_PAIR
#define UINT_PAIR
struct UIntpair {
	UIntpair() : i1(0), i2(0) {}
	UIntpair(UInt i1_, UInt i2_) : i1(i1_), i2(i2_) {}
	UInt i1, i2;
};
#endif

struct lazyEvalStruct {
	lazyEvalStruct(long long k, vec3f p) : key(k), pos(p) {}
	long long key;
	vec3f pos;
	float val;
};
// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

template <class T>
class Sample_thread {
public:

	typedef typename IsoOctreeBuilderMT<T>::vec3 vec3;
	typedef typename IsoOctreeBuilderMT<T>::point3 point3;

	Sample_thread(UIntpair startEnd, IsoOctreeBuilderMT<T>* isoBuilder) : m_StartEnd(startEnd), m_isoBuilder(isoBuilder) {
		m_func = isoBuilder->m_func;
		m_tree = isoBuilder->m_tree;
	}

	void operator()() {
		vec3 grad;
		for (UInt i1 = m_StartEnd.i1; i1 < m_StartEnd.i2; i1++) {
			const vec3& pos = m_isoBuilder->lazyEvals[i1].pos;
			m_isoBuilder->lazyEvals[i1].val = m_func->evalValAndGrad(pos, grad);
			m_isoBuilder->lazyEvals[i1].pos = grad;
		}
	}

private:

	IsoOctreeBuilderMT<T>* m_isoBuilder;
	UIntpair m_StartEnd;
	ScalarFunction< vec3 >* m_func;
	typename IsoOctreeBuilderMT<T>::MyTreeType* m_tree;
};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class IsoOctreeBuilderMT {

public:

// ****************************************************************************************

	typedef OctNode<MyNodeData<VertexValue<T>,T>,T>	MyOctNode;
	typedef IsoOctree<MyNodeData<VertexValue<T>,T>,T,VertexValue<T>>			MyTreeType;
	typedef point3d<T> vec3;
	typedef Point3D<T> point3;
	static const UInt MAX_VERTS_PER_CELL = 3;
	friend class Sample_thread<T>;

// ****************************************************************************************

	bool PointInCube(point3 ctr, T w, vec3 p) {
		if(p[0]<ctr.coords[0]-w/2) return false;
		if(p[0]>ctr.coords[0]+w/2) return false;
		if(p[1]<ctr.coords[1]-w/2) return false;
		if(p[1]>ctr.coords[1]+w/2) return false;
		if(p[2]<ctr.coords[2]-w/2) return false;
		if(p[2]>ctr.coords[2]+w/2) return false;

		return true;
	}

// ****************************************************************************************

	void setChildren(std::vector<UInt>& indices, MyOctNode* node, typename MyOctNode::NodeIndex nIdx) {

		long long key;
		T w;
		point3 ctr,p;
		if (indices.size() < MAX_VERTS_PER_CELL) return;
		if (nIdx.depth == m_maxTreeDepth)	return;

		if(!node->children)	node->initChildren();
		MyOctNode::CenterAndWidth(nIdx,ctr,w);

		//std::cerr << "Init node at " << ctr[0] << " " << ctr[1] << " " << ctr[2] << "  with width: " << w << std::endl;

		// Set center
		key=MyOctNode::CenterIndex(nIdx,m_maxTreeDepth);
		vec3 grad;
		
		lazyEvals.push_back(lazyEvalStruct(key, vec3(ctr.coords)));
		//point3 g;
		//T val;
		//val = m_func->evalValAndGrad(vec3(ctr.coords), grad);
		//g.coords[0] = grad[0]; g.coords[1] = grad[1]; g.coords[2] = grad[2];
		//m_tree->cornerValues[key]=VertexValue<T>(val, g);

		// Set the edge mid-points
		for(int i=0;i<Cube::EDGES;i++) {
			int o,i1,i2;
			Cube::FactorEdgeIndex(i,o,i1,i2);
			p=ctr;
			p[0]-=w/2;
			p[1]-=w/2;
			p[2]-=w/2;
			p[o]=ctr[o];
			switch(o) {
			case 0:
				p[1]+=w*i1;
				p[2]+=w*i2;
				break;
			case 1:
				p[0]+=w*i1;
				p[2]+=w*i2;
				break;
			case 2:
				p[0]+=w*i1;
				p[1]+=w*i2;
				break;
			}
			key=MyOctNode::EdgeIndex(nIdx,i,m_maxTreeDepth);

			lazyEvals.push_back(lazyEvalStruct(key, vec3(p.coords)));
			//val = m_func->evalValAndGrad(vec3(p.coords), grad);
			//g.coords[0] = grad[0]; g.coords[1] = grad[1]; g.coords[2] = grad[2];
			//if( m_tree->cornerValues.find(key)==m_tree->cornerValues.end() || fabs(m_tree->cornerValues[key].value())>fabs(val) )
			//	m_tree->cornerValues[key]=VertexValue<T>(val, g);
		}

		// set the face mid-points
		for(int i=0;i<Cube::FACES;i++) {
			int dir,off;
			Cube::FactorFaceIndex(i,dir,off);
			p=ctr;
			p[dir]+=-w/2+w*off;
			key=MyOctNode::FaceIndex(nIdx,i,m_maxTreeDepth);

			lazyEvals.push_back(lazyEvalStruct(key, vec3(p.coords)));
			//val = m_func->evalValAndGrad(vec3(p.coords), grad);
			//g.coords[0] = grad[0]; g.coords[1] = grad[1]; g.coords[2] = grad[2];
			//if( m_tree->cornerValues.find(key)==m_tree->cornerValues.end() || fabs(m_tree->cornerValues[key].value())>fabs(val) )
			//	m_tree->cornerValues[key]=VertexValue<T>(val, g);
		}

		// Propagate to children
		int retCount=0;
		for(int i=0;i<Cube::CORNERS;i++) {
			std::vector<UInt> new_indices;
			MyOctNode::CenterAndWidth(nIdx.child(i),ctr,w);

			for(UInt j=0; j<indices.size(); j++) {
				point3 ctr2;
				T w2=w;
				ctr2[0]=ctr[0];
				ctr2[1]=ctr[1];
				ctr2[2]=ctr[2];
				if(PointInCube(ctr2,w2,points[indices[j]]))
					new_indices.push_back(indices[j]);
			}
			setChildren(new_indices, &node->children[i],nIdx.child(i));
			if(new_indices.size()) retCount++;
		}

	}

// ****************************************************************************************

	void computeLazyEvals(UInt numThreads = 8) {

		UInt numLazyEvals = (UInt)lazyEvals.size();

		//T val;
		//vec3 grad;
		//point3 g;

		//for (UInt i1 = 0; i1 < numLazyEvals; i1++) {
		//	const long long& key = lazyEvals[i1].first;
		//	const vec3& pos = lazyEvals[i1].second;

		//	val = m_func->evalValAndGrad(pos, grad);
		//	g.coords[0] = grad[0]; g.coords[1] = grad[1]; g.coords[2] = grad[2];
		//	if( m_tree->cornerValues.find(key)==m_tree->cornerValues.end() || fabs(m_tree->cornerValues[key].value())>fabs(val) )
		//		m_tree->cornerValues[key]=VertexValue<T>(val, g);
		//}
		//return;

		UIntpair* ranges = new UIntpair[numThreads];
		ranges[0].i1 = 0;
		for (UInt i1 = 1; i1 < numThreads; i1++) {
			double fac = (double)i1/(double)numThreads;
			ranges[i1-1].i2 = (UInt)((double)numLazyEvals*fac);
			ranges[i1].i1 = ranges[i1-1].i2+1;
		}
		ranges[numThreads-1].i2 = numLazyEvals;

		// Launch threads:
		boost::thread** threads = new boost::thread*[numThreads];
		for (UInt i1 = 0; i1 < numThreads; i1++) {
			Sample_thread<T> thread_i(ranges[i1], this);
			threads[i1] = new boost::thread(thread_i);
		}

		// Collect threads
		for (UInt i1 = 0; i1 < numThreads; i1++) {
			threads[i1]->join();
		}
		// Delete threads
		for (UInt i1 = 0; i1 < numThreads; i1++) {
			delete threads[i1];
		}

		// Write values into octree:
		point3 g;
		for (UInt i1 = 0; i1 < (UInt)lazyEvals.size(); i1++) {
			const vec3f& grad = lazyEvals[i1].pos;
			g[0] = grad.x; g[1] = grad.y; g[2] = grad.z;
			m_tree->cornerValues[lazyEvals[i1].key]=VertexValue<T>(lazyEvals[i1].val, g);
		}

	}

// ****************************************************************************************

	MyTreeType* getIsoOctree(UInt maxTreeDepth, ScalarFunction<vec3>* func, std::vector<vec3>* points_, int numThreads = 8) {
		m_maxTreeDepth = maxTreeDepth;
		m_func = func;
		m_points = points_;
		points = &(*m_points)[0];

		// Initialize root cell
		m_tree = new MyTreeType;
		m_tree->maxDepth = m_maxTreeDepth;

		MyOctNode::NodeIndex nIdx;
		vec3 pos;
		vec3 grad;
		T val;
		point3 g;

		// Set corners
		for(int c=0;c<Cube::CORNERS;c++) {
			int x,y,z;
			Cube::FactorCornerIndex(c,x,y,z);

			pos = vec3(T(x), T(y), T(z));

			//std::cerr << "c: " << c << "  " << p[0] << " " << p[1] << " " << p[2] << std::endl;
			val = func->evalValAndGrad(pos, grad);
			g.coords[0] = grad[0]; g.coords[1] = grad[1]; g.coords[2] = grad[2];
			m_tree->cornerValues[MyOctNode::CornerIndex(nIdx,c,m_maxTreeDepth)]=VertexValue<T>(val, g);
		}
		if(!m_tree->tree.children) m_tree->tree.initChildren();


		std::vector<UInt> indices;
		for (UInt i1 = 0; i1 < (UInt)m_points->size(); i1++) indices.push_back(i1);

		// propagate to children:
		setChildren(indices, &m_tree->tree, nIdx);

		computeLazyEvals(numThreads);

		return m_tree;
	}

// ****************************************************************************************

private:

	UInt m_maxTreeDepth;
	ScalarFunction<vec3>* m_func;
	std::vector<vec3>* m_points;
	vec3* points;
	MyTreeType* m_tree;

	std::vector<lazyEvalStruct> lazyEvals;

};



#endif
