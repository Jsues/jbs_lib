#ifndef COMPUTE_VERTEX_CURVATURE_H
#define COMPUTE_VERTEX_CURVATURE_H

#include <set>

#include "SimpleMesh.h"
#include "operators/fitQuadricToPoints.h"


inline double computeCurvatureAtVertex(const SimpleMesh* sm, UInt i1, vec3d normal, bool use2ring = false) {

	vec3d ref_point = sm->vList[i1].c;
	std::vector<vec3d> points;

	if (sm->vList[i1].eList.size() <= 5) { // Isolated point or triangle
		return 0;
	}

	if (sm->vList[i1].eList.size() >= 5 && !use2ring) {
		// Use 1-ring
		for (UInt i2 = 0; i2 < sm->vList[i1].eList.size(); i2++) {
			int other_v = (sm->vList[i1].eList[i2]->v0 == i1) ? sm->vList[i1].eList[i2]->v1 : sm->vList[i1].eList[i2]->v0;
			points.push_back(sm->vList[other_v].c);
		}

	} else {
		// Use 2-ring
		std::vector<int> ring_1;
		std::set<int> ring_2;
		for (UInt i2 = 0; i2 < sm->vList[i1].eList.size(); i2++) {
			int other_v = (sm->vList[i1].eList[i2]->v0 == i1) ? sm->vList[i1].eList[i2]->v1 : sm->vList[i1].eList[i2]->v0;
			ring_1.push_back(other_v);
		}
		for (UInt i2 = 0; i2 < ring_1.size(); i2++) {
			int the_v = ring_1[i2];
			ring_2.insert(the_v);
			for (UInt i3 = 0; i3 < sm->vList[the_v].eList.size(); i3++) {
				int other_v = (sm->vList[the_v].eList[i3]->v0 == the_v) ? sm->vList[the_v].eList[i3]->v1 : sm->vList[the_v].eList[i3]->v0;
				ring_2.insert(other_v);
			}
		}
		for (std::set<int>::iterator it = ring_2.begin(); it != ring_2.end(); it++) {
			points.push_back(sm->vList[*it].c);
		}
	}

	Quadric<double> quad = fitQuadricToPoints(ref_point, points, normal);

	double curvature = quad.getGaussianCurvature();

	return abs(curvature);
}

#endif
