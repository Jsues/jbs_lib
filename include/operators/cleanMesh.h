#ifndef JBS_CLEAN_MESH
#define JBS_CLEAN_MESH


#include "SimpleMesh.h"
#include "FileIO/MeshWriter.h"
#include <map>
#include <iostream>
#include <fstream>


namespace JBSlib {

void cleanMesh(SimpleMesh* sm, bool useTrisWithMarker = true, const char* filename = "clean.off") {


	std::map<int, int> newVertexIndex;

	std::cerr << "Start mesh cleaning" << std::endl;
	std::vector<vec3d> newVerts;

	for (unsigned int i1 = 0; i1 < sm->tList.size(); i1++) {
		Triangle* tri = sm->tList[i1];

		if (tri->marker != useTrisWithMarker) continue;

		// check if vertices are contained in 'newVertexIndex'
		if (newVertexIndex.find(tri->v0()) == newVertexIndex.end()) {
			newVertexIndex.insert(std::pair<int, int>(tri->v0(), (int)newVertexIndex.size()));
			newVerts.push_back(sm->vList[tri->v0()].c);
			tri->set_v0((int)newVertexIndex.size()-1);
		} else {
			tri->set_v0(newVertexIndex[tri->v0()]);
		}

		if (newVertexIndex.find(tri->v1()) == newVertexIndex.end()) {
			newVertexIndex.insert(std::pair<int, int>(tri->v1(), (int)newVertexIndex.size()));
			newVerts.push_back(sm->vList[tri->v1()].c);
			tri->set_v1((int)newVertexIndex.size()-1);
		} else {
			tri->set_v1(newVertexIndex[tri->v1()]);
		}

		if (newVertexIndex.find(tri->v2()) == newVertexIndex.end()) {
			newVertexIndex.insert(std::pair<int, int>(tri->v2(), (int)newVertexIndex.size()));
			newVerts.push_back(sm->vList[tri->v2()].c);
			tri->set_v2((int)newVertexIndex.size()-1);
		} else {
			tri->set_v2(newVertexIndex[tri->v2()]);
		}
	}
	//MeshWriter::writeOFFFile("removed.off", sm, true, false);

	int num_old_v = (int)sm->vList.size();
	// Now set all vertices again:
	for (unsigned int i1 = 0; i1 < newVerts.size(); i1++) {
		sm->vList[i1].c = newVerts[i1];
	}
	sm->vList.resize(newVerts.size());
	sm->setNumV((int)newVerts.size());
	std::cerr << (int)newVerts.size() << " of " << num_old_v << " vertices kept." << std::endl;

	//MeshWriter::writeBINFile("clean.bin", sm, true, true);
	MeshWriter::writeOFFFile(filename, sm, true, false);

	std::cerr << "Mesh cleaning done" << std::endl;

}


void cleanMesh2(SimpleMesh* sm, bool useTrisWithMarker = true) {


	std::map<int, int> newVertexIndex;

	std::cerr << "Start mesh cleaning" << std::endl;
	SimpleMesh* sm_out = new SimpleMesh();

	for (unsigned int i1 = 0; i1 < sm->tList.size(); i1++) {
		Triangle* tri = sm->tList[i1];

		if (tri->marker != useTrisWithMarker) continue; 

		// check if vertices are contained in 'newVertexIndex'
		if (newVertexIndex.find(tri->v0()) == newVertexIndex.end()) {
			newVertexIndex.insert(std::pair<int, int>(tri->v0(), (int)newVertexIndex.size()));
			sm_out->insertVertex(sm->vList[tri->v0()].c);
			tri->set_v0((int)newVertexIndex.size()-1);
		} else {
			tri->set_v0(newVertexIndex[tri->v0()]);
		}

		if (newVertexIndex.find(tri->v1()) == newVertexIndex.end()) {
			newVertexIndex.insert(std::pair<int, int>(tri->v1(), (int)newVertexIndex.size()));
			sm_out->insertVertex(sm->vList[tri->v1()].c);
			tri->set_v1((int)newVertexIndex.size()-1);
		} else {
			tri->set_v1(newVertexIndex[tri->v1()]);
		}

		if (newVertexIndex.find(tri->v2()) == newVertexIndex.end()) {
			newVertexIndex.insert(std::pair<int, int>(tri->v2(), (int)newVertexIndex.size()));
			sm_out->insertVertex(sm->vList[tri->v2()].c);
			tri->set_v2((int)newVertexIndex.size()-1);
		} else {
			tri->set_v2(newVertexIndex[tri->v2()]);
		}
	}

	sm->vList.clear();

	for (unsigned int i1 = 0; i1 < sm->tList.size(); i1++) {
		Triangle* tri = sm->tList[i1];

		if (tri->marker != useTrisWithMarker) continue;

		sm_out->insertTriangle(tri->v0(), tri->v1(), tri->v2());
	}

	delete sm;

	std::cerr << "Mesh cleaning done" << std::endl;

}


}

#endif