#ifndef TRIVIALLINE_TO_POLYLINE_H
#define TRIVIALLINE_TO_POLYLINE_H

#include "JBSoctree.h"

template <class T>
PolyLine<T>* TrivialLineToPolyLine(TrivialLine<T>* tl) {
	PolyLine<T>* pl = new PolyLine<T>;

	int numPts = (int)tl->lines.size()*2;
	T min_v, max_v;
	tl->ComputeMinAndMaxExt(min_v, max_v);
	T diag = (max_v-min_v).length();
	min_v -= diag*(T::ValueType)0.05;
	max_v += diag*(T::ValueType)0.05;

	//std::cerr << "Min: " << min_v << " Max: " << max_v << std::endl;

	JBSlib::OcTree<JBSlib::OcPoint3<T::ValueType> > octree(numPts, 6, min_v, max_v);

	T p0, p1;

	// Fill Data points:
	for (UInt i1 = 0; i1 < tl->lines.size(); i1++) {
		p0 = tl->lines[i1].v0;
		p1 = tl->lines[i1].v1;

		//std::cerr << p0 << "   " << p1 << std::endl;

		//std::cerr << i1 << std::endl;
		UInt pos_v0, pos_v1;
		
		if (octree.insertObjectAvoidMultiple(pos_v0, p0))
			pl->insertVertex(p0);
		if (octree.insertObjectAvoidMultiple(pos_v1, p1))
			pl->insertVertex(p1);

        if (pos_v0 != pos_v1) pl->insertLine(pos_v0, pos_v1);
	}

	return pl;
}

#endif
