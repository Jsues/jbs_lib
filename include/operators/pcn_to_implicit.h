#ifndef PCN_TO_IMPLICIT
#define PCN_TO_IMPLICIT

#include "RBFTree3.h"
#include "PointCloud.h"
#include "getOffsurfacePoints.h"
#ifndef NO_MPU
#include "mpu_wrapper.h"
#endif
#include "HoppeImplicitFunc.h"


template <class T>
ScalarFunctionWithConfidence<T>* getRBFFit(const PointCloudNormals<T>* pc, int maxTreeDepth = 7, typename T::ValueType maxCellError = (typename T::ValueType)0.001, float cell_overlap = 0.65f, bool verboseOutput = true) {

	RBFFunctionSamples<T>* fs = getFunctionSamplesNoOffSurfacePoints(pc);

	RBFTree3<T>* tree = new RBFTree3<T>(fs, maxCellError, cell_overlap, T((typename T::ValueType)-0.05), T((typename T::ValueType)1.05));

	if (verboseOutput) std::cerr << "building RBF Tree..." << std::endl;
	for (int i1 = 0; i1 < maxTreeDepth; i1++) {
		if (verboseOutput) std::cerr << "Fitting level: " << i1 << "                              \r";
		tree->fitNextLevel(verboseOutput);
	}
	if (verboseOutput) std::cerr << "building RBF Tree...done" << std::endl;

	delete fs;
	return tree;
}


template <class T>
ScalarFunction<T>* getHoppeFit(PointCloudNormals<T>* pc) {
	HoppeImplicitFit<T>* func = new HoppeImplicitFit<T>(pc);
	return func;
}


template <class T>
ScalarFunction<T>* getMPU_MarcoFit(const PointCloudNormals<T>* pc, int maxTreeDepth = 15, typename T::ValueType maxCellError = (typename T::ValueType)0.001) {

	MPU_MarcoWrapper<T>::MPU* octree = new MPU_MarcoWrapper<T>::MPU();
	octree->insertPoints(pc->getPoints(), pc->getNormals(), pc->getNumPts());

	octree->build(maxCellError, maxTreeDepth);

	MPU_MarcoWrapper<T>* func = new MPU_MarcoWrapper<T>(octree);

	return func;

}

template <class T>
ScalarFunction<T>* getMPU_JochenFit(const PointCloudNormals<T>* pc, int maxTreeDepth = 15, typename T::ValueType maxCellError = (typename T::ValueType)0.001) {

	MPU_JochenWrapper<T>::MPU* octree = new MPU_JochenWrapper<T>::MPU();
	octree->insertPoints(pc->getPoints(), pc->getNormals(), pc->getNumPts());
	octree->build(maxCellError, maxTreeDepth);
	MPU_JochenWrapper<T>* func = new MPU_JochenWrapper<T>(octree);

	return func;

}

#endif

