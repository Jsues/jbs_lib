#ifndef FITMESHTOFUNCTION_H
#define FITMESHTOFUNCTION_H


#define SEARCH_DISTANCE_SCALER 1.25
#define CONVERGENCE_EPSILON 0.0001

#define SMOOTHAFTERPROJECTION


#include "SimpleMesh.h"
#include "ScalarFunction.h"
#include "SparseMatrix.h" // Umfpack
#include "PointCloud.h"
#include "operators/MeshFairing.h"
#include "operators/computeVertexNormals.h"
#define USENOTAUCS
#include "ARAP.h"

#include <sstream>
#include <algorithm>
#include <ANN/ANN.h>



vec3d getVertexNormal(SimpleMesh* sm, UInt id) {

	vec3d normal(0,0,0);
	for (UInt i3 = 0; i3 < sm->vList[id].eList.size(); i3++) {
		Edge* e = sm->vList[id].eList[i3];
		for (UInt i4 = 0; i4 < e->tList.size(); i4++) {
			Triangle* t = e->tList[i4];
			double area = t->getArea();
			vec3d n = t->getNormal();
			normal += n*area;
		}
	}

	normal.normalize();
	return normal;
}

template <class T>
bool projectPointOntoImplicit(ScalarFunction<T>* func, vec3d& pos_d) {

	T pos((typename T::ValueType)pos_d.x, (typename T::ValueType)pos_d.y, (typename T::ValueType)pos_d.z);

	T grad;
	typename T::ValueType val = func->evalValAndGrad(pos, grad);

	int num_it = 0;

	while (fabs(val) > (typename T::ValueType)CONVERGENCE_EPSILON) {
		grad.normalize();
		pos -= (grad*val*(typename T::ValueType)0.2);

		val = func->evalValAndGrad(pos, grad);

		num_it++;
		if (num_it > 30) {
#ifdef DEBUG_OUTPUT
			std::cerr << "failed to converge" << std::endl;
#endif
			pos_d = vec3d(pos.x, pos.y, pos.z);
			return false;
		}
	}

	pos_d = vec3d(pos.x, pos.y, pos.z);
	return true;
}


struct FitMeshToImpl {

	//! Constructor
	FitMeshToImpl() : numOuterIter(5), numInnerIter(5), writeDebugMeshes(false) {
	}

	//! number of iterations (projection / ARAP optimization)
	UInt numOuterIter;

	//! number of iterations for ARAP optimization
	UInt numInnerIter;

	//! If set to 'true', a debug mesh is written after each projection / ARAP optimization
	bool writeDebugMeshes;
};


template <class T>
void FitMeshToImplicitFunctionARAP(SimpleMesh* sm, SimpleMesh* urshape, ScalarFunction<T>* func, PointCloud<T>* pc, const FitMeshToImpl& config) {


	//**********************************************************
	// Build kd-tree
	//**********************************************************
	UInt numPoints = pc->getNumPts();
	ANNpointArray dataPts = annAllocPts(numPoints, 3);
	for (UInt i1 = 0; i1 < numPoints; i1++) {
		dataPts[i1][0] = pc->getPoints()[i1][0]; dataPts[i1][1] = pc->getPoints()[i1][1]; dataPts[i1][2] = pc->getPoints()[i1][2];
	}
	ANNpoint queryPt = annAllocPt(3);
	ANNkd_tree* kdTree = new ANNkd_tree(dataPts, numPoints, 3);

	//**********************************************************
	// Probe median point distance
	//**********************************************************
	int* ids = new int[6];
	double* dists = new double[6];
	std::vector<typename T::ValueType> med_dist_vec;
	for (UInt i1 = 0; i1 < numPoints; i1 += numPoints/100) {
		queryPt[0] = pc->getPoints()[i1][0];
		queryPt[1] = pc->getPoints()[i1][1];
		queryPt[2] = pc->getPoints()[i1][2];
		kdTree->annkSearch(queryPt, 6, ids, dists, 0);
		for (UInt i2 = 1; i2 < 6; i2++) {
			med_dist_vec.push_back((typename T::ValueType)dists[i2]);
			//std::cerr << ids[i1] << " " << dists[i2] << std::endl;
		}
	}
	delete[] ids;
	delete[] dists;
	std::sort(med_dist_vec.begin(), med_dist_vec.end()); 
	double medDistSquare = med_dist_vec[med_dist_vec.size()/2];
	double medDist = sqrt(medDistSquare);
	//std::cerr << medDist << std::endl;
	double searchDistSquare = (SEARCH_DISTANCE_SCALER*medDist)*(SEARCH_DISTANCE_SCALER*medDist);

	//**********************************************************
	// Initialize Arrays for ARAP-Optimization
	//**********************************************************
	UInt numOfPoints = sm->getNumV();
	SquareMatrixND<vec3d>* rots = new SquareMatrixND<vec3d>[numOfPoints];
	T* currentGradients = new T[numOfPoints];
	double* b = new double[4*numOfPoints];
	double* x = new double[4*numOfPoints];
	typename T::ValueType* confs = new typename T::ValueType[numOfPoints];

	MeshWriter mw;

	for (UInt i1 = 0; i1 < config.numOuterIter; i1++) {

		vec3d* normals = computeVertexNormals(sm);

		//**********************************************************
		// Project current mesh onto implicit
		//**********************************************************
		for (UInt i2 = 0; i2 < numOfPoints; i2++) {
			vec3f func_grad;
			func->evalValAndGrad(sm->vList[i2].c, func_grad);
			func_grad.normalize();
			if ((func_grad | normals[i2]) < 0.4) {
				sm->vList[i2].has_trace = false;
			} else {
				if (!projectPointOntoImplicit(func, sm->vList[i2].c)) {
					sm->vList[i2].has_trace = false;
				} else {
					sm->vList[i2].has_trace = true;
				}
			}
		}

#ifdef SMOOTHAFTERPROJECTION
		// smooth mesh a little.
		JBSlib::CurvatureSmooth(sm);
#endif

		int havetrace = 0;
		std::cerr << "has trace test...";
		for (UInt i2 = 0; i2 < numOfPoints; i2++) {
			// Remove those who lost trace
			queryPt[0] = sm->vList[i2].c[0];
			queryPt[1] = sm->vList[i2].c[1];
			queryPt[2] = sm->vList[i2].c[2];
			UInt numInRadius = kdTree->annkFRSearch(queryPt, searchDistSquare, 0);
			//std::cerr << numInRadius << std::endl;
			if (numInRadius == 0) {
				sm->vList[i2].has_trace = false;
			} else {
				sm->vList[i2].has_trace = true;
				havetrace++;
			}
			//std::cerr << sm->vList[i1].has_trace << "  -  " << sm->vList[i1].is_boundary_point << std::endl;

			// estimate tangent-planes for ARAP optimization
			T pos((typename T::ValueType)queryPt[0], (typename T::ValueType)queryPt[1], (typename T::ValueType)queryPt[2]);
			func->evalValAndGrad(pos, currentGradients[i2]);
		}
		std::cerr << "done" << std::endl;
		std::cerr << havetrace << " vertices have trace" << std::endl;

		if (config.writeDebugMeshes) {
			std::stringstream ss;
			ss << "proj_" << i1 << ".off";
			mw.writeOFFFile(ss.str().c_str(), sm);
		}

		//return;

		for (UInt ARAP_iter = 0; ARAP_iter < config.numInnerIter; ARAP_iter++) {
			//**********************************************************
			// Find ideal rotations
			//**********************************************************
			std::cerr << "estimating rotations..." << "                                \r";
			for (UInt i2 = 0; i2 < numOfPoints; i2++) {
				SquareMatrixND<vec3d> COV;
				UInt v0 = i2;
				for (UInt i3 = 0; i3 < (UInt)urshape->vList[v0].getNumN(); i3++) {
					UInt v1 = urshape->vList[v0].getNeighbor(i3, v0);
					SquareMatrixND<vec3d> tmp;
					tmp.addFromTensorProduct(urshape->vList[v0].c-urshape->vList[v1].c, sm->vList[v0].c-sm->vList[v1].c);
					const double& w_ij = urshape->vList[v0].eList[i3]->cotangent_weight;
					tmp *= w_ij;
					COV += tmp;
				}
				SquareMatrixND<vec3d> U, V;
				vec3d sigma;
				bool SVD_result = COV.SVD_decomp(U, sigma, V);
				if (SVD_result) {
					V.transpose();
					SquareMatrixND<vec3d> rot = U*V;
					double det = rot.getDeterminant();
					if (det < 0) {
						int sm_id = 0;
						double smallest = sigma[sm_id];
						for (UInt dd = 1; dd < 3; dd++) {
							if (sigma[dd] < smallest) {
								smallest = sigma[dd];
								sm_id = dd;
							}
						}
						// flip sign of entries in colums 'sm_id' in 'U'
						U.m[sm_id + 0] *= -1;
						U.m[sm_id + 3] *= -1;
						U.m[sm_id + 6] *= -1;
						rot = U*V;
					}
					rots[i2] = rot;
				} else { // SVD failed!
					//std::cerr << "SVD failed" << std::endl;
					rots[i2].setToIdentity();
				}
			}


			//**********************************************************
			// Find vertex positions which match rotations best
			//**********************************************************
			UInt nn = numOfPoints*4;
			SparseMatrixCol<double> smc(nn);
			// init b with zero:
			for (UInt i2 = 0; i2 < nn; i2++) b[i2] = 0;

			std::vector<double> entries;
			std::vector<int> row_index;				// row indices, 0-based
			std::vector<int> col_ptr;				// pointers to where columns begin in rowind and values 0-based, length is (n+1)
			col_ptr.push_back(0);
			// *********************
			// Build taucs matrix direct:
			// *********************

			UInt current_row = 0;
			std::cerr << "building LGS..." << "                                \r";
			for (UInt i2 = 0; i2 < numOfPoints; i2++) {

				const vec3d& p0 = urshape->vList[i2].c;
				UInt numN = (UInt)urshape->vList[i2].getNumN();

				// derive for p'_i.x and p'_i.y
				for (UInt i3 = 0; i3 < numN; i3++) {
					UInt neighbor = urshape->vList[i2].getNeighbor(i3, i2);
					const vec3d& p1 = urshape->vList[neighbor].c;

					double w_ij = urshape->vList[i2].eList[i3]->cotangent_weight;
					smc.add(4*i2+0,			current_row+0,  w_ij);
					smc.add(4*i2+1,			current_row+1,  w_ij);
					smc.add(4*i2+2,			current_row+2,  w_ij);
					smc.add(4*neighbor+0,	current_row+0, -w_ij);
					smc.add(4*neighbor+1,	current_row+1, -w_ij);
					smc.add(4*neighbor+2,	current_row+2, -w_ij);

					// right_hand_side:
					SquareMatrixND<vec3d> rot_avg = rots[i2]+rots[neighbor];
					vec3d rhs = (rot_avg.vecTrans(p0-p1)) * w_ij * 0.5;
					b[4*i2+0] += rhs.x;
					b[4*i2+1] += rhs.y;
					b[4*i2+2] += rhs.z;
				}
				smc.insert(4*i2+3, current_row+0, currentGradients[i2].x);
				smc.insert(4*i2+3, current_row+1, currentGradients[i2].y);
				smc.insert(4*i2+3, current_row+2, currentGradients[i2].z);

				// derive for \lambda_i
				if (!sm->vList[i2].has_trace) {
					smc.insert(4*i2+3, current_row+3, 1);
					b[4*i2+3] = 0;
				} else {
					const vec3d& p0hat = sm->vList[i2].c;
					smc.insert(4*i2+0, current_row+3, currentGradients[i2].x);
					smc.insert(4*i2+1, current_row+3, currentGradients[i2].y);
					smc.insert(4*i2+2, current_row+3, currentGradients[i2].z);
					b[4*i2+3] = p0hat.x*currentGradients[i2].x + p0hat.y*currentGradients[i2].y + p0hat.z*currentGradients[i2].z;
				}
				current_row += 4;
			}

			std::cerr << "solving LGS..." << "                                \r";
			CompColSparseMatrix ccsm;


			smc.getCompColSparseMatrix(ccsm);

			//ccsm.printToImage("xxx.raw");
			//exit(1);


			ccsm.computeLU_Umfpack();
			ccsm.backSolve_Umfpack(b, x);


			for (UInt i2 = 0; i2 < numOfPoints; i2++) {
				double p_x = x[4*i2+0];
				double p_y = x[4*i2+1];
				double p_z = x[4*i2+2];

				if (isInfinity(p_x)) p_x = 0;
				if (isInfinity(p_y)) p_y = 0;
				if (isInfinity(p_z)) p_z = 0;

				sm->vList[i2].c = vec3d(p_x,p_y,p_z);
			}
		}


		if (config.writeDebugMeshes) {
			std::stringstream ss2;
			ss2 << "arap_" << i1 << ".off";
			mw.writeOFFFile(ss2.str().c_str(), sm);
		}

	}

	delete kdTree;
	annDeallocPt(queryPt);
	annDeallocPts(dataPts);

	delete[] x;
	delete[] b;
	delete[] rots;
	delete[] currentGradients;
}



#endif
