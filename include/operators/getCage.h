#ifndef GET_CAGE_H
#define GET_CAGE_H


#include "SimpleMesh.h"
#include "Volume.h"


int imax(int x, int y) {
	if (x > y) return x;
	return y;
}


void markCube(UInt x, UInt y, UInt z, Volume<int>* vol, SimpleMesh* cage) {

	int val000 = vol->get(x,y,z);
	int val010 = vol->get(x,y+1,z);
	int val001 = vol->get(x,y,z+1);
	int val011 = vol->get(x,y+1,z+1);
	int val100 = vol->get(x+1,y,z);
	int val110 = vol->get(x+1,y+1,z);
	int val101 = vol->get(x+1,y,z+1);
	int val111 = vol->get(x+1,y+1,z+1);


	cage->insertEdge(val000-2, val001-2);
	cage->insertEdge(val000-2, val010-2);
	cage->insertEdge(val000-2, val100-2);

	cage->insertEdge(val110-2, val100-2);
	cage->insertEdge(val110-2, val010-2);
	cage->insertEdge(val110-2, val111-2);

	cage->insertEdge(val101-2, val001-2);
	cage->insertEdge(val101-2, val100-2);
	cage->insertEdge(val101-2, val111-2);

	cage->insertEdge(val011-2, val001-2);
	cage->insertEdge(val011-2, val010-2);
	cage->insertEdge(val011-2, val111-2);

}



SimpleMesh* getRectilinearCage(SimpleMesh* sm, int maxres, Volume<int>*& vol, SimpleMesh* sm_fullymeshed = NULL, bool fillcage = false, bool dilate = false) {

	sm->recomputeMinMax();
	vec3d pt_min = sm->min;
	vec3d pt_max = sm->max;
	vec3d diag = pt_max-pt_min;
	double brick_size = std::max(pt_max[0] - pt_min[0], std::max(pt_max[1] - pt_min[1], pt_max[2] - pt_min[2])) / (float)maxres;
	brick_size *= 1.0000001;
	int brickX = (unsigned int)((pt_max[0] - pt_min[0]) / brick_size);
	int brickY = (unsigned int)((pt_max[1] - pt_min[1]) / brick_size);
	int brickZ = (unsigned int)((pt_max[2] - pt_min[2]) / brick_size);
	brickX += 2;
	brickY += 2;
	brickZ += 2;


	vec3d bb_center = (pt_min+pt_max)/2.0;
	pt_min.x = bb_center.x-(1+brickX)*(brick_size/2.0);
	pt_min.y = bb_center.y-(1+brickY)*(brick_size/2.0);
	pt_min.z = bb_center.z-(1+brickZ)*(brick_size/2.0);


	vol = new Volume<int>;
	vol->initWithDims(pt_min, pt_min + vec3f(((float)brickX+1)*(float)brick_size,((float)brickY+1)*(float)brick_size,((float)brickZ+1)*(float)brick_size), brickX+2, brickY+2, brickZ+2);
	vol->zeroOutMemory();
	Volume<int>* vol2 = new Volume<int>;
	vol2->initWithDims(pt_min, pt_min + vec3f(((float)brickX+1)*(float)brick_size,((float)brickY+1)*(float)brick_size,((float)brickZ+1)*(float)brick_size), brickX+2, brickY+2, brickZ+2);
	vol2->zeroOutMemory();


	vec3f cell((float)brick_size);
	cell /= 2;


	for (int i1 = 0; i1 < sm->getNumV(); i1++) {
		const vec3d& pt = sm->vList[i1].c;
		vec3f p((float)pt.x, (float)pt.y, (float)pt.z);

		vec3i idx = vol->worldPosToIndices(p);
		int x = idx.x;
		int y = idx.y;
		int z = idx.z;
		vol->set(x, y, z, 1);
		if (vol->get(x+1,y,z) != 1) vol->set(x+1,y,z, 2);
		if (vol->get(x,y+1,z) != 1) vol->set(x,y+1,z, 2);
		if (vol->get(x,y,z+1) != 1) vol->set(x,y,z+1, 2);
		if (vol->get(x+1,y+1,z) != 1) vol->set(x+1,y+1,z, 2);
		if (vol->get(x,y+1,z+1) != 1) vol->set(x,y+1,z+1, 2);
		if (vol->get(x+1,y,z+1) != 1) vol->set(x+1,y,z+1, 2);
		if (vol->get(x+1,y+1,z+1) != 1) vol->set(x+1,y+1,z+1, 2);
	}

	if (dilate) {
		Volume<int>* vol3 = new Volume<int>;
		vol3->initWithDims(pt_min, pt_min + vec3f(((float)brickX+1)*(float)brick_size,((float)brickY+1)*(float)brick_size,((float)brickZ+1)*(float)brick_size), brickX+2, brickY+2, brickZ+2);
		vol3->zeroOutMemory();

		vol->dilate(vol3);
		delete vol;
		vol = vol3;
	}


	if (fillcage) {
		int numFilled = vol->seedFill(0,0,0, 3);
		numFilled += vol->seedFill(0,vol->dy-1,0, 3);
		numFilled += vol->seedFill(0,0,vol->dz-1, 3);
		numFilled += vol->seedFill(0,vol->dy-1,vol->dz-1, 3);
		numFilled += vol->seedFill(vol->dx-1,0,0, 3);
		numFilled += vol->seedFill(vol->dx-1,vol->dy-1,0, 3);
		numFilled += vol->seedFill(vol->dx-1,0,vol->dz-1, 3);
		numFilled += vol->seedFill(vol->dx-1,vol->dy-1,vol->dz-1, 3);

		int numFilled2 = 0;
		for (UInt x = 0; x < vol->dx; x++) {
			for (UInt y = 0; y < vol->dy; y++) {
				for (UInt z = 0; z < vol->dz; z++) {
					int val = vol->get(x,y,z);
					if (val == 3) vol->set(x,y,z, 0);
					else if (val == 0) {
						vol->set(x,y,z, 1);
						numFilled2++;
					}
				}
			}
		}

		std::cerr << numFilled << " outer voxels found, " << numFilled2 << " inner voxels filled" << std::endl;
		//if (vol->get(0,0,0) != 0) {
		//	std::cerr << "Error filling cage: voxel (0,0,0) is set" << std::endl;
		//} else {
		//	// Seed-fill outer:
		//}
	}


	SimpleMesh* cage = new SimpleMesh;


	int idx = 2;
	for (UInt x = 0; x < vol->getDimX(); x++) {
		for (UInt y = 0; y < vol->getDimY(); y++) {
			for (UInt z = 0; z < vol->getDimZ(); z++) {
				int val = vol->get(x,y,z);
				if (val > 0) {
					vol2->set(x,y,z, val);
					vec3f p = vol->pos(x,y,z);
					cage->insertVertex(p);
					vol->set(x,y,z, idx);
					idx++;
				}
			}
		}
	}

	for (UInt x = 0; x < vol->getDimX(); x++) {
		for (UInt y = 0; y < vol->getDimY(); y++) {
			for (UInt z = 0; z < vol->getDimZ(); z++) {

				int vala = vol2->get(x,y,z);
				if (vala == 1) {
					markCube(x,y,z, vol, cage);
				}


				//if (val > 0) {
				//	if (x < vol->getDimX()-1) {
				//		int val2 = vol->get(x+1,y,z);
				//		if (val2 > 0) cage->insertEdge(val-2, val2-2);
				//	}

				//	if (y < vol->getDimY()-1) {
				//		int val3 = vol->get(x,y+1,z);
				//		if (val3 > 0) cage->insertEdge(val-2, val3-2);
				//	}

				//	if (z < vol->getDimZ()-1) {
				//		int val4 = vol->get(x,y,z+1);
				//		if (val4 > 0) cage->insertEdge(val-2, val4-2);
				//	}
				//}
			}
		}
	}

	if (sm_fullymeshed != NULL) {
		int idx = 2;
		for (UInt x = 0; x < vol->getDimX(); x++) {
			for (UInt y = 0; y < vol->getDimY(); y++) {
				for (UInt z = 0; z < vol->getDimZ(); z++) {
					int val = vol->get(x,y,z);
					if (val > 0) {
						vec3f p = vol->pos(x,y,z);
						sm_fullymeshed->insertVertex(p);
					}
				}
			}
		}

		for (UInt x = 0; x < vol->getDimX(); x++) {
			for (UInt y = 0; y < vol->getDimY(); y++) {
				for (UInt z = 0; z < vol->getDimZ(); z++) {
					int val = vol->get(x,y,z);
					
					if (val > 0) {
						for (UInt xx = imax(0, x-1); (xx <= x+1) && (xx < vol->getDimX()); xx++) {
							for (UInt yy = imax(0, y-1); (yy <= y+1) && (yy < vol->getDimY()); yy++) {
								for (UInt zz = imax(0, z-1); (zz <= z+1) && (zz < vol->getDimZ()); zz++) {
									if (xx == x && yy == y && zz == z) continue;
									int val2 = vol->get(xx,yy,zz);
									if (val2 > 0) sm_fullymeshed->insertEdge(val-2, val2-2);
								}
							}
						}
					}

				}
			}
		}
	}
	

	delete vol2;

#ifdef SHOW_DEBUG_INFO
	std::cerr << "Cage has " << cage->getNumV() << " nodes" << std::endl;
	std::cerr << "Cage has " << cage->eList.size() << " edges" << std::endl;
#endif

	return cage;
}

#endif
