#ifndef FIT_QUADRIC_TO_POINTS
#define FIT_QUADRIC_TO_POINTS


#include <vector>

#include "../point3d.h"
#include "../Matrix3D.h"
#include "../MathMatrix.h"
#include "../JBS_General.h"



//**************************************************************************************************************

template <class T>
class Quadric {

public:
	//! Value-Type of Templates components.
	typedef T ValueType; 

	Quadric() {
		a = b = c = d = e = (ValueType)0;
	}

	Quadric(ValueType a_, ValueType b_, ValueType c_, ValueType d_, ValueType e_) {
		a = a_;
		b = b_;
		c = c_;
		d = d_;
		e = e_;
	}

	void print() {
		std::cerr << a << "*xx + " << b << "*xy + " << c << "*yy + " << d << "*x + " << e << "*y" << std::endl;
	}

	//! evaluates the quadric at (x,y), i.e returns a scalar f(x,y).
	T eval(T x, T y) {
		return a*x*x + b*x*y + c*y*y + d*x + e*y;
	}

	//! returns the Gaussian curvature at (0,0).
	T getGaussianCurvature() {
		return (4*a*c-b*b)/((1+d*d+e*e)*(1+d*d+e*e));
	}

	//! returns the Mean curvature at (0,0).
	T getMeanCurvature() {
		return (a+c+a*e*e+c*d*d-b*d*e)/sqrt((1+d*d+e*e)*(1+d*d+e*e)*(1+d*d+e*e));
	}

	T getabsKappa1plusabsKappa2() {
		T H = getMeanCurvature();
		T K = getGaussianCurvature();

		T kappa1 = (-2*H + sqrt(4*H*H - 4*K)) / (-2);
		T kappa2 = K/kappa1;

		//T K2 = kappa1*kappa2;
		//T H2 = (kappa1+kappa2)*0.5;
		//std::cerr << "H: " << H << " / " << H2 << "   K: " << K << " / " << K2 << std::endl;
		//std::cerr << "kappa1: " << kappa1 << "  kappa2: " << kappa2 << std::endl;
		return fabs(kappa1)+fabs(kappa2);
	}

	//! returns the normal at (0,0).
	point3d<T> getNormal() {
		point3d<T> normal(-d, -e, 1);
		normal.normalize();
		return normal;
	}

private:

	ValueType a,b,c,d,e;

};

//**************************************************************************************************************

template <class T>
Quadric<T> fitQuadricToPointsIter(point3d<T> ref_point, std::vector< point3d<T> > points, point3d<T> normal) {

	UInt numPoints = (UInt)points.size();

	// Build orthonormal system:
	Matrix3D<T> TMP_1(1, 0, 0,    0, 1, 0,    0, 0, 1);
	Matrix3D<T> TMP_2(normal, normal);
	TMP_1 = TMP_1-TMP_2;
	point3d<T> ref_axis(1,0,0);
	if ((ref_axis-normal).squaredLength() < 0.00001)
        ref_axis = point3d<T>(0,1,0);

	point3d<T> x_dir = TMP_1.vecTrans(ref_axis);
	x_dir.normalize();
	point3d<T> y_dir = x_dir^normal;

	Matrix3D<T> rotationM(x_dir.x, x_dir.y, x_dir.z, y_dir.x, y_dir.y, y_dir.z, normal.x, normal.y, normal.z);

	point3d<T>* pointsTrans = new point3d<T>[numPoints];

	// Transform points:
	for (UInt i1 = 0; i1 < numPoints; i1++) {
		pointsTrans[i1] = rotationM.vecTrans(ref_point-points[i1]);
	}

	// Build the system:
	DoRo::Matrix<T> A(numPoints, 5);
	DoRo::Matrix<T> b(numPoints, 1);
	for (UInt i1 = 0; i1 < numPoints; i1++) {
		A(i1, 0) = pointsTrans[i1].x*pointsTrans[i1].x;
		A(i1, 1) = pointsTrans[i1].x*pointsTrans[i1].y;
		A(i1, 2) = pointsTrans[i1].y*pointsTrans[i1].y;
		A(i1, 3) = pointsTrans[i1].x;
		A(i1, 4) = pointsTrans[i1].y;
		b(i1, 0) = pointsTrans[i1].z;
	}
	//A.print();
	//b.print();

	DoRo::Matrix<T> A_t;
	A.transposed(A_t);

	DoRo::Matrix<T> A_t_A = A_t*A;
	DoRo::Matrix<T> A_t_A_I;

	A_t_A.inverse(A_t_A_I);
	//A_t_A_I.print();

	DoRo::Matrix<T> x(numPoints, 1);
	x = A_t*b;
	x = A_t_A_I*x;
	//x.print();

	delete[] pointsTrans;

	Quadric<T> quad(x(0, 0), x(1, 0), x(2, 0), x(3, 0), x(4, 0));

	return quad;

}

//**************************************************************************************************************

template <class T>
Quadric<T> fitQuadricToPoints(point3d<T> ref_point, std::vector< point3d<T> > points, point3d<T> original_normal=point3d<T>(0,0,0)) {


	UInt numPoints = (UInt)points.size();


	if (numPoints < 5) {
		std::cerr << "fitQuadricToPoints(...) Error!" << std::endl;
		std::cerr << "I need at least 5 points to fit a general quadric, you gave me " << numPoints << "! exit" << std::endl;
		exit(EXIT_FAILURE);
	}

	Matrix3D<T> CV;
	for (UInt i1 = 0; i1 < numPoints; i1++) {
		point3d<T> local = ref_point-points[i1];
		Matrix3D<T> CV_temp(local, local);
		CV = CV+CV_temp;
	}
	CV.calcEValuesAndVectors();
	point3d<T> normal = CV.getEV(2);

	// if an original normal is given, orient our normal consistently
	if (original_normal.squaredLength() != 0)
		if ((normal|original_normal) < 0)
			normal *= -1;

	Quadric<T> quad;
	for (int i1 = 0; i1 < 1; i1++) {
		quad = fitQuadricToPointsIter(ref_point, points, normal);
		original_normal = quad.getNormal();
	}



    return quad;

}

//**************************************************************************************************************


#endif

