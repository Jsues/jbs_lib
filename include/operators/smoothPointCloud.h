#ifndef SMOOTH_POINTCLOUD_H
#define SMOOTH_POINTCLOUD_H

#include "PointCloud.h"
#include "ANN/ANN.h"

template <class Point>
void smoothPointCloudAndNormals(PointCloudNormals<Point>* pc, int num) {

	const int dim = Point::dim;
	UInt numPts = pc->getNumPts();
	ANNpoint queryPt = annAllocPt(dim);
	ANNpointArray dataPts = annAllocPts(numPts, dim);

	for (UInt i1 = 0; i1 < numPts; i1++) {
		for (UInt x = 0; x < dim; x++) {
			dataPts[i1][x] = pc->getPoints()[i1][x];
		}
	}

	ANNkd_tree* kdTree = new ANNkd_tree(dataPts, numPts, dim);

	int* ids = new int[numPts];
	double* dists = new double[numPts];

	for (int i1 = 0; i1 < pc->getNumPts(); i1++) {

		const Point& pt = pc->getPoints()[i1];
		for (UInt x = 0; x < dim; x++) {
			queryPt[x] = pt[x];
		}
		kdTree->annkSearch(queryPt, num, ids, dists, 0);

		Point avg;
		for (int i2 = 0; i2 < num; i2++) {
			const Point& p = pc->getPoints()[ids[i2]];
			avg += p;
		}
		avg /= (Point::ValueType)num;

		SquareMatrixND<Point> cov;
		for (int i2 = 0; i2 < num; i2++) {
			const Point& p = pc->getPoints()[ids[i2]];
			Point d = p-avg;
			cov.addFromTensorProduct(d,d);
		}


		SquareMatrixND<Point> ESystem;
		cov.calcEValuesAndVectorsCORRECT(ESystem);

		Point nnew = ESystem.getColI(2);
		Point& nold = pc->getNormals()[i1];
		if ((nold|nnew) < 0) nnew *= -1;
		nold = nnew;

		Point vec = avg-pc->getPoints()[i1];
		Point::ValueType dist = vec|nold;
		Point newPos = pc->getPoints()[i1] + nold*dist;
		pc->getPoints()[i1] = newPos;
	}

	delete[] ids;
	delete[] dists;

	delete kdTree;
	annDeallocPts(dataPts);
	annDeallocPt(queryPt);
	annClose();			// close ANN

}

#endif
