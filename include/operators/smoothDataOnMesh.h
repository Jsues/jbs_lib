#ifndef SMOOTH_DATA_ON_MESH_H
#define SMOOTH_DATA_ON_MESH_H

#include "SimpleMesh.h"

template <class T>
T smoothScaleDependent(UInt id, SimpleMesh* sm, T* data) {
	T smoothed((double)0);

	double length = 0;
	for (UInt i1 = 0; i1 < sm->vList[id].getNumN(); i1++) {
		double l = sm->vList[id].eList[i1]->getLength();
		if (l == 0) l = std::numeric_limits<double>::min();
		T val = data[sm->vList[id].eList[i1]->getOther(id)];
		smoothed += (val*(T::ValueType)l);
		length += l;
	}
	smoothed /= (T::ValueType)length;

	return smoothed;
}

template <class T>
void smoothDataOnMesh(SimpleMesh* sm, T* data) {
	UInt numV = (UInt)sm->getNumV();

	T* data_temp = new T[numV];

	for (UInt i1 = 0; i1 < numV; i1++) {
		data_temp[i1] = smoothScaleDependent(i1, sm, data);
	}

	for (UInt i1 = 0; i1 < numV; i1++) {
		data[i1] = data_temp[i1];
	}

	delete[] data_temp;
}

#endif
