#ifndef MESH_SELECTION_SMOOTH_H
#define MESH_SELECTION_SMOOTH_H

#include "SimpleMesh.h"

// Choose Parametrization method.
enum SMOOTHING_METHOD {
	UMBRELLA = 0,
	INVERSE_UMBRELLA = 1,
	TAUBIN = 2,
	CURVATURE_FLOW = 3
};

void SmoothMeshSelection(SimpleMesh* sm, SMOOTHING_METHOD method) {

}

#endif
