#ifndef GET_VERTEX_FAN_H
#define GET_VERTEX_FAN_H

#include "SimpleMesh.h"

//! Reverts the elements in 'vec', i.e. turns 1 2 5 6 8 2 - into - 2 8 6 5 2 1
template <typename T>
void reverse_vector_order(std::vector<T>& vec) {
	size_t lenght = vec.size();
	for (size_t i = 0; i < lenght/2; i++) {
		std::swap(vec[i], vec[lenght-1-i]);
	}
}

//! Computes the fan around vertex 'id'
/*!
	Duplicates the first vertex of the fan, i.e. a fan looks like this: <br>
	8 12 13 14 <b>8</b>

	Works also for boundary vertices. Again, fan[0] == fan[size-1]!!!!
*/
inline bool getVertexFan(const SimpleMesh* sm, int id, std::vector<int>& fan) {

	//! isolated point
	if (sm->vList[id].eList.size() < 2) return true;

	Edge* first_e = sm->vList[id].eList[0];
	Edge* e = first_e;
	int first_other = e->getOther(id);

	Triangle* t = e->tList[0];
	int cnt = 0;

	bool orientation_correct = false;
	// Handle orientation correctly
	if (	((t->v0() == id) && (t->v1() == first_other))
		||	((t->v1() == id) && (t->v2() == first_other))
		||	((t->v2() == id) && (t->v0() == first_other))	) {
			orientation_correct = true;
	}

	do {

		fan.push_back(e->getOther(id));
		//std::cerr << e->getOther(id) << " - ";
		if      ((t->e0 != e) && ((t->e0->v0 == id) || (t->e0->v1 == id))) e = t->e0;
		else if ((t->e1 != e) && ((t->e1->v0 == id) || (t->e1->v1 == id))) e = t->e1;
		else if ((t->e2 != e) && ((t->e2->v0 == id) || (t->e2->v1 == id))) e = t->e2;
		else {
			std::cerr << "error" << std::endl;
			exit(1);
		}

		//*******************************************
		//* Handle boundary case
		//*******************************************
		if (e->tList.size() != 2) { // boundary;
			fan.clear();
			first_e = e;

			orientation_correct = false;
			first_other = e->getOther(id);
			// Handle orientation correctly
			if (	((t->v0() == id) && (t->v1() == first_other))
				||	((t->v1() == id) && (t->v2() == first_other))
				||	((t->v2() == id) && (t->v0() == first_other))	) {
					orientation_correct = true;
			}

			fan.push_back(first_other);
			if      ((t->e0 != e) && ((t->e0->v0 == id) || (t->e0->v1 == id))) e = t->e0;
			else if ((t->e1 != e) && ((t->e1->v0 == id) || (t->e1->v1 == id))) e = t->e1;
			else if ((t->e2 != e) && ((t->e2->v0 == id) || (t->e2->v1 == id))) e = t->e2;
			else {
				std::cerr << "Error" << std::endl;
			}
			fan.push_back(e->getOther(id));

			do {

				if (e->tList.size() < 2) { // Reached other boundary;
					//std::cerr << "Reached boundary" << std::endl;
					fan.push_back(fan[0]);
					if (!orientation_correct) {
						//std::cerr << "Rev vec order" << std::endl;
						fan.pop_back();
						reverse_vector_order(fan);
						fan.push_back(fan[0]);
					}
					return true;
				}

				Triangle* t_new = (e->tList[0] == t) ? e->tList[1] : e->tList[0];
				t = t_new;

				if      ((t->e0 != e) && ((t->e0->v0 == id) || (t->e0->v1 == id))) e = t->e0;
				else if ((t->e1 != e) && ((t->e1->v0 == id) || (t->e1->v1 == id))) e = t->e1;
				else if ((t->e2 != e) && ((t->e2->v0 == id) || (t->e2->v1 == id))) e = t->e2;
				else {
					std::cerr << "error" << std::endl;
					exit(1);
				}

				fan.push_back(e->getOther(id));

			} while (e != first_e);
			std::cerr << "getVertexFan: should never arrive here" << std::endl;
			std::cerr << "vertex: " << id << " valence: " << sm->vList[id].getNumN() << std::endl;
			std::cerr << "isbounaryPoint: " << ((sm->vList[id].is_boundary_point) ? "true" : "false") << std::endl;
			for (UInt i2 = 0; i2 < fan.size(); i2++) std::cerr << fan[i2] << " ";
			std::cerr << std::endl;
			return true;
		}
		//*******************************************
		//* End handle boundary case
		//*******************************************

		Triangle* t_new = (e->tList[0] == t) ? e->tList[1] : e->tList[0];
		t = t_new;

	} while (e != first_e);
	fan.push_back(e->getOther(id));

	if (!orientation_correct) reverse_vector_order(fan);
	return false;

}

#endif
