#ifndef ARAP_PARAMETRIZATION_H
#define ARAP_PARAMETRIZATION_H

#include "ARAP.h"
#include "SVD_Matrix.h"
#include "operators/getVertexFan.h"
#include "operators/DeformationTrans.h"


std::vector< std::vector<vec2d> > getFlatedFans(SimpleMesh* sm, std::vector< std::vector<int> >& lookup) {
	sm->computeBoundaryVertices();

	std::vector< std::vector<vec2d> > flatedFans;

	for (UInt i1 = 0; i1 < (UInt)sm->getNumV(); i1++) {
		const vec3d& center = sm->vList[i1].c;
		std::vector<vec2d> fanEdges;
		std::vector<int> fan;

		getVertexFan(sm, i1, fan);

		std::vector<int> vertexLookup;
		for (UInt i2 = 0; i2 < sm->vList[i1].getNumN(); i2++) {
			int id = 0;
			int other = sm->vList[i1].getNeighbor(i2, i1);
			for (UInt i3 = 1; i3 < fan.size()-1; i3++) {
				if (fan[i3] == other) id = i3;
			}
			vertexLookup.push_back(id);
		}
		lookup.push_back(vertexLookup);

		double total_angle = 0;
		double sum;
		std::vector<double> angles;

		angles.push_back(0);
		if (!sm->vList[i1].is_boundary_point) {
			for (UInt i2 = 0; i2 < fan.size()-1; i2++) {
				int v0 = fan[i2];
				int v1 = fan[i2+1];

				vec3d vec0 = center-sm->vList[v0].c;
				vec3d vec1 = center-sm->vList[v1].c;
				vec0.normalize();
				vec1.normalize();
				double angle = acos(vec0|vec1);
				total_angle += angle;
				angles.push_back(total_angle);
			}
			sum = 2*M_PI;
		} else {
			for (UInt i2 = 0; i2 < fan.size()-2; i2++) {
				int v0 = fan[i2];
				int v1 = fan[i2+1];

				vec3d vec0 = center-sm->vList[v0].c;
				vec3d vec1 = center-sm->vList[v1].c;
				vec0.normalize();
				vec1.normalize();
				double angle = acos(vec0|vec1);
				total_angle += angle;
				angles.push_back(total_angle);
			}
			sum = total_angle; //M_PI;
		}

		double mult = 1;
		//if (sm->vList[i1].c.z > 0.45) mult = 3;

		// Flaten:
		double angle = 0;
		double fac = sum/total_angle;
		for (UInt i2 = 0; i2 < fan.size()-1; i2++) {
			int v0 = fan[i2];

			vec3d vec0 = center-sm->vList[v0].c;
			double length = vec0.length();
			angle = angles[i2]*fac;
			vec2d vec(cos(angle), sin(angle));

			//Edge* e = sm->vList[i1].eList.getEdge(i1, v0);
			//if (e->marker) mult = 0.2;
			//else mult = 2;

			vec *= length*mult;


			//vec.print();
			fanEdges.push_back(vec);
		}

		std::vector<vec2d> fanEdges2;
		for (UInt i2 = 0; i2 < fanEdges.size(); i2++) {
			fanEdges2.push_back(fanEdges[vertexLookup[i2]]);
		}

		flatedFans.push_back(fanEdges2);
		//std::cerr << total_angle << std::endl;
	}

	return flatedFans;
}


std::vector< std::vector<vec3d> > alignFlatedFans(SimpleMesh* mesh, std::vector< std::vector<vec2d> >& flatedFans) {
	std::vector< std::vector<vec3d> > alignedFans;

	UInt numV = (UInt)mesh->getNumV();
	SimpleMesh* sm2 = new SimpleMesh(*mesh);

	computeUrshapeWeightsChordal(mesh);

	for (UInt i1 = 0; i1 < numV; i1++) {
		// set fan
		UInt numN = (UInt)mesh->vList[i1].getNumN();
		sm2->vList[i1].c = vec3d(0,0,0);
		for (UInt i2 = 0; i2 < numN; i2++) {
			int neighbor = mesh->vList[i1].getNeighbor(i2, i1);
			sm2->vList[neighbor].c = vec3d(flatedFans[i1][i2].x, flatedFans[i1][i2].y, 0);
		}
		SquareMatrixND<vec3d> rot = computeOptimalRigidTrans(mesh, sm2, i1);

		std::vector<vec3d> fan;
		for (UInt i2 = 0; i2 < numN; i2++) {
			vec3d v = rot.vecTrans(vec3d(flatedFans[i1][i2].x, flatedFans[i1][i2].y, 0));
			fan.push_back(v);
		}
		alignedFans.push_back(fan);
	}

	delete sm2;

	return alignedFans;
}


void computeARAPParametrization(SimpleMesh* topology, std::vector<vec2d>& parametrization, const std::vector< std::vector<vec3d> >& alignedFans, PrecomputedMatrix* pm, UInt numInnerIter = 20) {

	typedef SquareMatrixND<vec3d> MAT;
	UInt numV = (UInt)topology->getNumV();
	UInt numT = (UInt)topology->getNumT();

	MAT* rots = new MAT[numT];

	int* vertexLookup = new int[numV];
	int unconstrained = 0;
	for (UInt i1 = 0; i1 < numV; i1++) {
		vertexLookup[i1] = unconstrained;
		if (!topology->vList[i1].bool_flag) {
			unconstrained++;
		}
	}

	std::vector< double > rhs_vec(2*numV);
	double* rhs_x = &rhs_vec[0];
	double* rhs_y = &rhs_vec[numV];

	std::vector<double> permutedRightHandSide_vec(numV);
	std::vector<double> permutedVariables_vec(numV);
	double* permutedRightHandSide	= &permutedRightHandSide_vec[0];
	double* permutedVariables		= &permutedVariables_vec[0];

	std::vector<double> x_vec(2*numV);
	double* x_x = &x_vec[0];
	double* x_y = &x_vec[numV];


	MAT mat_temp;
	for (UInt iter = 0; iter < numInnerIter; iter++) {

		// Compute best rotations for each vertex fan:
		for (UInt i1 = 0; i1 < numV; i1++) {
			MAT COV;
			UInt v0 = i1;
			for (UInt i2 = 0; i2 < (UInt)topology->vList[v0].getNumN(); i2++) {
				UInt v1 = topology->vList[v0].getNeighbor(i2, v0);
				MAT tmp;
				vec3d p_dash_i(parametrization[v0].x, parametrization[v0].y, 0);
				vec3d p_dash_j(parametrization[v1].x, parametrization[v1].y, 0);

				vec3d org = alignedFans[v0][i2];
				//org = topology->vList[v0].c-topology->vList[v1].c;

				tmp.addFromTensorProduct(-org, (p_dash_i-p_dash_j));
				const double& w_ij = topology->vList[v0].eList[i2]->cotangent_weight;
				tmp *= w_ij;
				COV += tmp;
			}
			MAT U, V;
			vec3d sigma;
			bool SVD_result = COV.SVD_decomp(U, sigma, V);
			if (SVD_result) {
				V.transpose();
				MAT rot = U*V;

				double det = rot.getDeterminant();
				if (det < 0) {
					int sm_id = 0;
					double smallest = sigma[sm_id];
					for (UInt dd = 1; dd < 3; dd++) {
						if (sigma[dd] < smallest) {
							smallest = sigma[dd];
							sm_id = dd;
						}
					}
					// flip sign of entries in colums 'sm_id' in 'U'
					U.m[sm_id + 0] *= -1;
					U.m[sm_id + 3] *= -1;
					U.m[sm_id + 6] *= -1;
					rot = U*V;
				}
				rot.transpose();
				rots[i1] = rot;
			} else { // SVD failed!
				std::cerr << "SVD failed" << std::endl;
				rots[i1].setToIdentity();
			}

			//if (i1 == 10) exit(1);
			//std::cerr << "---" << std::endl;
			//rots[i1].print();
		}


		//**********************************************************
		// Find vertex positions which match rotations best
		//**********************************************************
		int cnt = 0;
		for (UInt i1 = 0; i1 < numV; i1++) {
			if (!topology->vList[i1].bool_flag) { // point is unconstrained:
				vec3d rhs(0,0,0);

				for (UInt i2 = 0; i2 < topology->vList[i1].getNumN(); i2++) {
					Edge* e = topology->vList[i1].eList[i2];
					int other = e->getOther(i1);

					const double& w_ij = topology->vList[i1].eList[i2]->cotangent_weight;

					if (topology->vList[other].bool_flag) { // other one is constrained, put to right side
						vec3d tmp(parametrization[other].x, parametrization[other].y, 0);
						tmp *= w_ij;
						rhs += tmp;
					}
					//! Right-hand side
					MAT rot = rots[i1]+rots[other];
					vec3d org = -alignedFans[i1][i2];
					for (UInt i3 = 0; i3 < topology->vList[other].getNumN(); i3++) {
						if (topology->vList[other].getNeighbor(i3, other) == i1) {
							org += alignedFans[other][i3];
							org /= 2;
						}
					}
					vec3d org2 = topology->vList[i1].c-topology->vList[other].c;
					//double ddd = org.dist(org2);
					//if (ddd > 0.01) {
					//	std::cerr << "----" << std::endl;
					//	org.print();
					//	org2.print();
					//	std::cerr << ddd << std::endl;
					//	org = org2;
					//}
					vec3d t = rot.vecTrans(org);
					t *= (w_ij / 2.0);
					rhs += t;
				}
				rhs_x[cnt] = rhs.x;
				rhs_y[cnt] = rhs.y;
				cnt++;
			}
		}

		taucs_vec_permute(cnt, TAUCS_DOUBLE, rhs_x, permutedRightHandSide, pm->permutation);
		taucs_supernodal_solve_llt(pm->factorization, permutedVariables, permutedRightHandSide);
		taucs_vec_permute(cnt, TAUCS_DOUBLE, permutedVariables, x_x, pm->inversePermutation);
		taucs_vec_permute(cnt, TAUCS_DOUBLE, rhs_y, permutedRightHandSide, pm->permutation);
		taucs_supernodal_solve_llt(pm->factorization, permutedVariables, permutedRightHandSide);
		taucs_vec_permute(cnt, TAUCS_DOUBLE, permutedVariables, x_y, pm->inversePermutation);

		for (UInt i1 = 0; i1 < numV; i1++) if (!topology->vList[i1].bool_flag) {
			parametrization[i1].x = x_x[vertexLookup[i1]];
			parametrization[i1].y = x_y[vertexLookup[i1]];
		}

	}
	
	delete[] rots;
}


#endif
