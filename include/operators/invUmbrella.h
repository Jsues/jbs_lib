#ifndef INV_UMBRELLA_H
#define INV_UMBRELLA_H



//******************************************************************************************

inline void inverseUmbrellaSmoothVertex(SimpleMesh* mesh, int v) {

	//// Don't move boundary points
	//if (mesh->vList[v].is_boundary_point)
	//	return;


	vec3d pi;
	double valenz = double(mesh->vList[v].eList.size());

	// Compute centroid:
	vec3d ci(0.0, 0.0, 0.0);
	for (UInt i2 = 0; i2 < valenz; i2++) {
		int neighbor;
		(mesh->vList[v].eList[i2]->v0 == v) ? neighbor = mesh->vList[v].eList[i2]->v1 : neighbor = mesh->vList[v].eList[i2]->v0;
		ci += mesh->vList[neighbor].c;
	}
	if (mesh->vList[v].eList.size() > 0)
		ci /= valenz;

	pi = ci;

	//mesh->vList[v].c = pi;
	//return;

	double divider = 1.0;

	for (UInt i2 = 0; i2 < mesh->vList[v].eList.size(); i2++) {
		int neighbor;
		(mesh->vList[v].eList[i2]->v0 == v) ? neighbor = mesh->vList[v].eList[i2]->v1 : neighbor = mesh->vList[v].eList[i2]->v0;

		vec3d pj = mesh->vList[neighbor].c;
		double valenz_j = double(mesh->vList[neighbor].eList.size());
		vec3d cj(0.0, 0.0, 0.0);
		for (UInt i3 = 0; i3 < valenz_j; i3++) {
			int neighbor2;
			(mesh->vList[neighbor].eList[i3]->v0 == neighbor) ? neighbor2 = mesh->vList[neighbor].eList[i3]->v1 : neighbor2 = mesh->vList[neighbor].eList[i3]->v0;
			if (neighbor2 == v) {
				divider += 1.0/(valenz_j*valenz);
			} else {
				cj += mesh->vList[neighbor2].c;
			}
		}
		cj /= valenz_j;
		cj -= pj;
		cj /= valenz;

		pi -= cj;
	}

	pi /= divider;
	mesh->vList[v].c = pi;

}


#endif
