#ifndef POLYGONIZE_FUNCTION_H
#define POLYGONIZE_FUNCTION_H

#include <map>

#include "marchingCubes.h"
#include "ScalarFunction.h"
#include "SimpleMesh.h"
#include "volume.h"
#include "operators/samplevolumemt.h"

struct sortOp {
	inline bool operator()(const vec3f& p1, const vec3f& p2) const {
		if (	 (p1.x < p2.x) ||
				((p1.x == p2.x) && (p1.y < p2.y)) ||
				((p1.x == p2.x) && (p1.y == p2.y) && (p1.z < p2.z)))
				return true;
		else
			return false;
	}
};

typedef std::map<vec3f, UInt, sortOp> mp;

// ****************************************************************************************

template <class Point>
SimpleMesh* PolygonizeFunction(ScalarFunction<Point>* func, Point minV, Point maxV, int stepsMax=100, float iso = 0, UInt numThreads = 8, Volume<typename Point::ValueType>* vol = NULL, JBS_Timer* t = NULL) {

	typedef Point::ValueType ValueType;

	if (t != NULL) t->log("Sample Volume");
	bool cleanUpVolume = false;
	if (vol == NULL) {
		vol = sampleFunctionToVolumeMultiThreaded<Point::ValueType>(minV, maxV, stepsMax, func, numThreads);
		cleanUpVolume = true;
	}

	mp point_map;
	vec3i indices;
	SimpleMesh* sm = new SimpleMesh;

	if (t != NULL) t->log("Run Marching-Cubes");
	for (UInt x = 0; x < vol->getDimX()-1; x++) {
		for (UInt y = 0; y < vol->getDimY()-1; y++) {
			for (UInt z = 0; z < vol->getDimZ()-1; z++) {


				vec3f tmp;
				tmp = vol->pos(x+1,y,z);
				cell.p[0] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x,y,z);
				cell.p[1] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x,y+1,z);
				cell.p[2] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x+1,y+1,z);
				cell.p[3] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x+1,y,z+1);
				cell.p[4] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x,y,z+1);
				cell.p[5] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x,y+1,z+1);
				cell.p[6] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x+1,y+1,z+1);
				cell.p[7] = vec3f(tmp[0], tmp[1], tmp[2]);

				cell.val[0] = (float)vol->get(x+1,y,z);
				cell.val[1] = (float)vol->get(x,y,z);
				cell.val[2] = (float)vol->get(x,y+1,z);
				cell.val[3] = (float)vol->get(x+1,y+1,z);
				cell.val[4] = (float)vol->get(x+1,y,z+1);
				cell.val[5] = (float)vol->get(x,y,z+1);
				cell.val[6] = (float)vol->get(x,y+1,z+1);
				cell.val[7] = (float)vol->get(x+1,y+1,z+1);

				int notUsed;
				int numTris = Polygonise(cell, iso, tris, notUsed);

				if (numTris == 0)
					continue;

				for (int i1 = 0; i1 < numTris; i1++) {
					vec3f v0(tris[i1].p[0].x, tris[i1].p[0].y, tris[i1].p[0].z);
					vec3f v1(tris[i1].p[1].x, tris[i1].p[1].y, tris[i1].p[1].z);
					vec3f v2(tris[i1].p[2].x, tris[i1].p[2].y, tris[i1].p[2].z);

					mp::iterator it = point_map.find(v0);
					if (it == point_map.end()) {
						UInt id = (UInt)point_map.size();
						point_map.insert(std::make_pair(v0, id));
						sm->insertVertex((double)v0.x, (double)v0.y, (double)v0.z);
						indices.x = id;
					} else {
						indices.x = it->second;
					}

					it = point_map.find(v1);
					if (it == point_map.end()) {
						UInt id = (UInt)point_map.size();
						point_map.insert(std::make_pair(v1, id));
						sm->insertVertex((double)v1.x, (double)v1.y, (double)v1.z);
						indices.y = id;
					} else {
						indices.y = it->second;
					}

					it = point_map.find(v2);
					if (it == point_map.end()) {
						UInt id = (UInt)point_map.size();
						point_map.insert(std::make_pair(v2, id));
						sm->insertVertex((double)v2.x, (double)v2.y, (double)v2.z);
						indices.z = id;
					} else {
						indices.z = it->second;
					}

					sm->insertTriangle(indices.x, indices.y, indices.z);
				}
			}
		}
	}

	if (cleanUpVolume) delete vol;

	return sm;
}

// ****************************************************************************************
// ****************************************************************************************

template <class Point>
SimpleMesh* PolygonizeFunctionST(ScalarFunction<Point>* func, Point minV, Point maxV, int stepsMax=100, float iso = 0, Volume<typename Point::ValueType>* vol = NULL) {

	typedef Point::ValueType ValueType;

	bool cleanUpVolume = false;
	if (vol == NULL) {
		vol = sampleFunctionToVolumeSingleThreaded<Point::ValueType>(minV, maxV, stepsMax, func);
		cleanUpVolume = true;
	}

	mp point_map;
	vec3i indices;
	SimpleMesh* sm = new SimpleMesh;

	for (UInt x = 0; x < vol->getDimX()-1; x++) {
		for (UInt y = 0; y < vol->getDimY()-1; y++) {
			for (UInt z = 0; z < vol->getDimZ()-1; z++) {


				vec3f tmp;
				tmp = vol->pos(x+1,y,z);
				cell.p[0] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x,y,z);
				cell.p[1] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x,y+1,z);
				cell.p[2] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x+1,y+1,z);
				cell.p[3] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x+1,y,z+1);
				cell.p[4] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x,y,z+1);
				cell.p[5] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x,y+1,z+1);
				cell.p[6] = vec3f(tmp[0], tmp[1], tmp[2]);
				tmp = vol->pos(x+1,y+1,z+1);
				cell.p[7] = vec3f(tmp[0], tmp[1], tmp[2]);

				cell.val[0] = (float)vol->get(x+1,y,z);
				cell.val[1] = (float)vol->get(x,y,z);
				cell.val[2] = (float)vol->get(x,y+1,z);
				cell.val[3] = (float)vol->get(x+1,y+1,z);
				cell.val[4] = (float)vol->get(x+1,y,z+1);
				cell.val[5] = (float)vol->get(x,y,z+1);
				cell.val[6] = (float)vol->get(x,y+1,z+1);
				cell.val[7] = (float)vol->get(x+1,y+1,z+1);

				int notUsed;
				int numTris = Polygonise(cell, iso, tris, notUsed);

				if (numTris == 0)
					continue;

				for (int i1 = 0; i1 < numTris; i1++) {
					vec3f v0(tris[i1].p[0].x, tris[i1].p[0].y, tris[i1].p[0].z);
					vec3f v1(tris[i1].p[1].x, tris[i1].p[1].y, tris[i1].p[1].z);
					vec3f v2(tris[i1].p[2].x, tris[i1].p[2].y, tris[i1].p[2].z);

					mp::iterator it = point_map.find(v0);
					if (it == point_map.end()) {
						UInt id = (UInt)point_map.size();
						point_map.insert(std::make_pair(v0, id));
						sm->insertVertex((double)v0.x, (double)v0.y, (double)v0.z);
						indices.x = id;
					} else {
						indices.x = it->second;
					}

					it = point_map.find(v1);
					if (it == point_map.end()) {
						UInt id = (UInt)point_map.size();
						point_map.insert(std::make_pair(v1, id));
						sm->insertVertex((double)v1.x, (double)v1.y, (double)v1.z);
						indices.y = id;
					} else {
						indices.y = it->second;
					}

					it = point_map.find(v2);
					if (it == point_map.end()) {
						UInt id = (UInt)point_map.size();
						point_map.insert(std::make_pair(v2, id));
						sm->insertVertex((double)v2.x, (double)v2.y, (double)v2.z);
						indices.z = id;
					} else {
						indices.z = it->second;
					}

					sm->insertTriangle(indices.x, indices.y, indices.z);
				}
			}
		}
	}

	if (cleanUpVolume) delete vol;

	return sm;
}

// ****************************************************************************************

template <class Point>
Point projectOntoImplicit(ScalarFunction<Point>* func, Point pos) {

	Point ret = pos;
	Point dir;
	Point::ValueType val = func->evalValAndGrad(ret, dir);


	double convergence_epsilon = 0.0005f;

	int num_it = 0;
	while (fabs(val) > convergence_epsilon) {
		dir.normalize();
		Point::ValueType multiplier = (val*0.4f);
		ret -= (dir*multiplier);

		val = func->evalValAndGrad(ret, dir);

		num_it++;
		if (num_it > 50) {
			return ret;
		}
	}
	return ret;
}

// ****************************************************************************************

template <class Point>
void ProjectMeshOntoImplicit(SimpleMesh* sm, ScalarFunction<Point>* func) {

	UInt numPts = sm->getNumV();
	for (UInt i1 = 0; i1 < numPts; i1++) {
		vec3d ptnew = projectOntoImplicit(func, sm->vList[i1].c);
		sm->vList[i1].c = ptnew;
	}
}

#endif
