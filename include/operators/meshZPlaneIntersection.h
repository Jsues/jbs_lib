#ifndef MESH_Z_PLANE_INTERSECTION_H
#define MESH_Z_PLANE_INTERSECTION_H

#include <map>

#include "TrivialMesh.h"
#include "Polyline.h"
#include "MatrixnD.h"
#include "operators/TrivialLineToPolyLine.h"
#include "SimpleMesh.h"

template <class T>
TrivialLine<T>* computeZIntersection(const TrivialMesh<T>* mesh, typename T::ValueType z, std::map<float, int>* tri_map = NULL) {

	TrivialLine<T>* tl = new TrivialLine<T>();

	UInt startIndex, endIndex;

	if (tri_map == NULL) {
		startIndex = 0;
		endIndex = mesh->getNumTris();
	} else {
		std::map<float, int>::iterator it;
		startIndex = 0;
		for (it = tri_map->begin(); it != tri_map->end(); ++it) {
			if (it->first > z) {
				endIndex = it->second;
				break;
			}
		}
	}

	//std::cerr << "startIndex: " << startIndex << "  endIndex: " << endIndex << std::endl;

	
    T lines[2];
	int num_line_pts;
	T p0, p1;

	for (UInt i1 = startIndex; i1 < endIndex; i1++) {
		const trivialTriangle<T>* tri = &(mesh->tris[i1]);

		num_line_pts = 0;

		if ((tri->v0.z >= z && tri->v1.z < z) || (tri->v0.z < z && tri->v1.z >= z)) {

			if (tri->v0.z >= z) {
				p0 = tri->v0;
				p1 = tri->v1;
			} else {
				p0 = tri->v1;
				p1 = tri->v0;
			}

			T::ValueType d1 = fabs(p0.z - z);
			T::ValueType d2 = fabs(p1.z - z);
			T::ValueType dist = d1+d2;
			d1 /= dist;
			d2 /= dist;
			lines[num_line_pts] = p0 * d2 + p1 * d1;
			num_line_pts++;
		}
		if ((tri->v0.z >= z && tri->v2.z < z) || (tri->v0.z < z && tri->v2.z >= z)) {

			if (tri->v0.z >= z) {
				p0 = tri->v0;
				p1 = tri->v2;
			} else {
				p0 = tri->v2;
				p1 = tri->v0;
			}

			T::ValueType d1 = fabs(p0.z - z);
			T::ValueType d2 = fabs(p1.z - z);
			T::ValueType dist = d1+d2;
			d1 /= dist;
			d2 /= dist;
			lines[num_line_pts] = p0 * d2 + p1 * d1;
			num_line_pts++;
		}
		if ((tri->v2.z >= z && tri->v1.z < z) || (tri->v2.z < z && tri->v1.z >= z)) {

			if (tri->v2.z >= z) {
				p0 = tri->v2;
				p1 = tri->v1;
			} else {
				p0 = tri->v1;
				p1 = tri->v2;
			}

			T::ValueType d1 = fabs(p0.z - z);
			T::ValueType d2 = fabs(p1.z - z);
			T::ValueType dist = d1+d2;
			d1 /= dist;
			d2 /= dist;
			lines[num_line_pts] = p0 * d2 + p1 * d1;
			num_line_pts++;
		}
		if (num_line_pts == 2) {
			tl->insertSegment(lines[0], lines[1]);
		}

	}

	//tl->print();
	return tl;
}

//! Caution, the function may be buggy, rotation in z-direction may not work?
PolyLine<vec3d>* getIntersectionsWithPlane(SimpleMesh* sm, vec3d n, double d) {

	n.normalize();

	vec3d helper(1,0,0);
	if ((n|helper) > 0.9) {
		helper = vec3d(0,1,0);
	}

	vec3d n1 = n^helper;
	n1.normalize();
	vec3d n2 = n1^n;


	SquareMatrixND<vec3d> mat;
	mat.setColI(n1, 1);
	mat.setColI(n2, 2);
	mat.setColI(n, 0);

	int numT = sm->getNumT();
	TrivialMesh<vec3d>* tm = new TrivialMesh<vec3d>(numT);

	for (int i1 = 0; i1 < numT; i1++) {
		const Triangle* t = sm->tList[i1];
		const vec3d& p0 = sm->vList[t->v0()].c;
		const vec3d& p1 = sm->vList[t->v1()].c;
		const vec3d& p2 = sm->vList[t->v2()].c;

		tm->insertTriangle(mat.vecTrans(p0), mat.vecTrans(p1), mat.vecTrans(p2));
		//tm->insertTriangle(p0, p1, p2);
	}

	TrivialLine<vec3d>* tl = computeZIntersection(tm, d);

	//tl->print();

	PolyLine<vec3d>* pl = TrivialLineToPolyLine(tl);
	delete tl;

	SquareMatrixND<vec3d> matInv = mat.getTransposed();
	for (int i1 = 0; i1 < (int)pl->getNumV(); i1++) {
		pl->points[i1].c = matInv.vecTrans(pl->points[i1].c);
	}

	delete tm;

	return pl;
}

#endif
