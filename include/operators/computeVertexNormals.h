#ifndef  COMPUTE_VERTEX_NORMALS_H
#define  COMPUTE_VERTEX_NORMALS_H

#include "../SimpleMesh.h"

//! Computes the normals for all mesh vertices:
inline vec3d* computeVertexNormals(const SimpleMesh* mesh) {

	int numV = (int)mesh->vList.size();
	vec3d* normals = new vec3d[numV];

	// Initialize with 0
	for (int i1 = 0; i1 < numV; i1++)
		normals[i1] = 0.0;

	for (unsigned int i1 = 0; i1 < mesh->tList.size(); i1++) {
		vec3d norm = mesh->tList[i1]->getNormal();

		normals[mesh->tList[i1]->v0()] += norm;
		normals[mesh->tList[i1]->v1()] += norm;
		normals[mesh->tList[i1]->v2()] += norm;
	}

	// Normalize them
	for (int i1 = 0; i1 < numV; i1++) {
		if (normals[i1].squaredLength() > 0)
			normals[i1].normalize();
	}


	return normals;
}


//! Computes the normals for all mesh vertices:
template <class Point>
inline std::vector<Point> computeVertexNormals(const std::vector<Point>& verts, const std::vector<vec3i>& tris) {

	int numV = (int)verts.size();
	std::vector<Point> normals(numV);

	// Initialize with 0
	for (int i1 = 0; i1 < numV; i1++)
		normals[i1] = 0.0;

	for (unsigned int i1 = 0; i1 < tris.size(); i1++) {
		const vec3i& t = tris[i1];

		vec3d e1 = verts[t.y]-verts[t.x];
		vec3d e2 = verts[t.z]-verts[t.x];
		vec3d norm = e1^e2;

		normals[t.x] += norm;
		normals[t.y] += norm;
		normals[t.z] += norm;
	}

	// Normalize them
	for (int i1 = 0; i1 < numV; i1++) {
		if (normals[i1].squaredLength() > 0)
			normals[i1].normalize();
	}


	return normals;
}

#endif
