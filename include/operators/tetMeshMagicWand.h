#ifndef TET_MESH_MAGIC_WAND_H
#define TET_MESH_MAGIC_WAND_H


#include "StreamingIndexedTetraMesh.h"


namespace JBSlib {

	//! Do not use this, it would be private if this was possible for global classes!
	template <class T>
	std::set<UInt> addConnectedTetsToFile(StreamingIndexedTetraMesh<T>* tetMesh, UInt seedVert, std::ofstream& fout, bool& ok, UInt& numMarkedretval) {

		std::set<UInt> try_next;

		int firstTet = -1;
		// Determine first tetrahedron:
		for (UInt i1 = 0; i1 < tetMesh->vList[seedVert].getNumEdges(); i1++) {
			UInt firstEdge = tetMesh->vList[seedVert].getEdge(i1);
			if (tetMesh->eList[firstEdge].getNumTets() > 0) {
				firstTet = tetMesh->eList[firstEdge].getTet(0);
				break;
			}
		}

		if (firstTet == -1) {
			std::cerr << "ERROR: JBSlib::addConnectedTetsToFile(...)" << std::endl;
			std::cerr << "Startvertex " << seedVert << " is isolated!" << std::endl;
			ok = false;
			return try_next;
		}

		std::vector<UInt> unprocessedTets;
		int counter = 0;

		UInt numTets = tetMesh->getNumT();
		bool* tetMarker = new bool[numTets];
		for (UInt i1 = 0; i1 < numTets; i1++) tetMarker[i1] = false;

		// add first one to queue:
		unprocessedTets.push_back(firstTet);
		tetMarker[firstTet] = true;

        while (unprocessedTets.size() > 0) {
			UInt current = unprocessedTets.back();
			unprocessedTets.pop_back();

			counter++;
			if (counter%1000 == 0)
				std::cerr << counter << "            \r";

			// Insert all neighboring tets that have not yet been marked into unprocessedTets:
			for (unsigned int i0 = 0; i0 < 4; i0++) {

				UInt e = tetMesh->tList[current].getEdge(i0);

				for (unsigned int i1 = 0; i1 < tetMesh->eList[e].getNumTets(); i1++) {
					UInt t = tetMesh->eList[e].getTet(i1);
					if (!tetMarker[t]) {
						tetMarker[t] = true;
						unprocessedTets.push_back(t);
					}
				}
			}
		}

		// Count number of marked tets;
		UInt numMarked = 0;
		for (UInt i1 = 0; i1 < numTets; i1++) if (tetMarker[i1]) numMarked++;

		std::cerr << "Marked " << numMarked << " of " << numTets << " tets" << std::endl;

		float max_t = -1000000000;

		// Write out marked tets:
		for (UInt i1 = 0; i1 < numTets; i1++) {
			if (tetMarker[i1]) {
				UInt v0 = tetMesh->tList[i1].getVertex(0);
				UInt v1 = tetMesh->tList[i1].getVertex(1);
				UInt v2 = tetMesh->tList[i1].getVertex(2);
				UInt v3 = tetMesh->tList[i1].getVertex(3);

				fout.write((char*)&v0, sizeof(UInt));
				fout.write((char*)&v1, sizeof(UInt));
				fout.write((char*)&v2, sizeof(UInt));
				fout.write((char*)&v3, sizeof(UInt));

				for (UInt i2 = 0; i2 < 4; i2++) {
					point4d<T> pt = tetMesh->vList[tetMesh->tList[i1].getVertex(i2)].getPoint();
					if (pt.w >= max_t) {
						max_t = pt.w;
						try_next.insert(tetMesh->tList[i1].getVertex(i2));
					}
				}
			}
		}

        delete[] tetMarker;

		numMarkedretval = numMarked;

		return try_next;

	}


	//! Selects an connected area of the tetMesh starting at the vertex 'seedPoint'. Does only select such tetrahedra that are connected with the start tetrahedron.
	template <class T>
	void tetMeshMagicWand(StreamingIndexedTetraMesh<T>* tetMesh, UInt seedVert) {

		std::string output_filename_tet_file  = JBSlib::replaceFileExtension(tetMesh->vertexFile.c_str(), "selected.itmtets");
		std::string output_filename_meta_file = JBSlib::replaceFileExtension(tetMesh->metaFile.c_str(), "selected.metafile");

		// copy metafile:
		std::vector< timesliceInfo<T> > sliceInfo;
		for (UInt i1 = 0; i1 < tetMesh->sliceInfo.size(); i1++) {
			sliceInfo.push_back(tetMesh->sliceInfo[i1]);
		}

		const char* filename = output_filename_tet_file.c_str();
		std::ofstream fout(filename, std::ofstream::binary);
		if (!fout.good()) {
			std::cerr << "Error writing file in 'JBSlib::tetMeshMagicWand(...)'" << std::endl;
			return;
		}


		bool xxx;
		// do first slice:
		UInt numMarked;
		std::set<UInt> try_next = addConnectedTetsToFile(tetMesh, seedVert, fout, xxx, numMarked);

		bool ok = tetMesh->readNextSlice();
		while (ok) {

			UInt sliceID = *(tetMesh->slicesInMemory.begin());
			sliceInfo[sliceID].indexOfFirstTetrahedron = numMarked;

			// Find seed vertex:
			bool breaker = true;
			//std::cerr << try_next.size() << std::endl;
			for (std::set<UInt>::iterator it = try_next.begin(); it != try_next.end() && breaker; it++) {
				UInt test_v = (*it);
				//std::cerr << "test with: " << test_v << std::endl;
				// Check if vertex (*it) is connected:
				for (UInt i1 = 0; i1 < tetMesh->vList[test_v].getNumEdges(); i1++) {
					UInt firstEdge = tetMesh->vList[test_v].getEdge(i1);
					if (tetMesh->eList[firstEdge].getNumTets() > 0) {
						seedVert = test_v; // Found connected vertex;
						breaker = false;
						break;
					}
				}
			}
			if (breaker) {
				std::cerr << "Error in JBSlib::tetMeshMagicWand() no connected vertex found" << std::endl;
			}

			UInt tmp = 0;
			try_next = addConnectedTetsToFile(tetMesh, seedVert, fout, ok, tmp);
			numMarked += tmp;

			if (ok) {
				ok = tetMesh->readNextSlice();
			}
		}

		std::cerr << "done, marked " << numMarked << " tetrahedra" << std::endl;
		fout.close();

		// write last:
		sliceInfo[sliceInfo.size()-1].indexOfFirstTetrahedron = numMarked;

		// Write new metafile:
		std::ofstream fout2(output_filename_meta_file.c_str());
		fout2 << "vertexfile: " << tetMesh->vertexFile.c_str() << std::endl;
		fout2 << "tetrafile: " << output_filename_tet_file.c_str() << std::endl;
		fout2 << "normalfile: null" << std::endl;
		fout2 << "numVerts: " << tetMesh->getNumV() << std::endl;
		fout2 << "numTets: " << numMarked << std::endl;
		for (UInt i1 = 0; i1 < sliceInfo.size()-1; i1++) {
			fout2 << std::setprecision(20) << sliceInfo[i1].timeStart << " " << sliceInfo[i1].indexOfFirstTetrahedron << std::endl;
		}
		fout2.close();

	}

};


#endif


