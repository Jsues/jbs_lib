#ifndef DEFORMATION_TRANSPLANTATION_H
#define DEFORMATION_TRANSPLANTATION_H


#include "AnimatedMesh.h"
#include "ARAP.h"
#include "SVD_Matrix.h"
#include "operators/getVertexFan.h"
#include "MathMatrix.h"
#include "operators/alignPointClouds.h"


// ****************************************************************************************


inline vec3d computeFourthPoint(const vec3d& v0, const vec3d& v1, const vec3d& v2) {
	vec3d fin = (v1-v0)^(v2-v0);
	double length = fin.length();
	fin /= sqrt(length);
	fin += v0;
	return fin;
}


// ****************************************************************************************


//! Computes the deformation gradient of the triangle 'id' in mesh 'sm1' to 'sm2'
SquareMatrixND<vec3d> computeDeformationGradient(SimpleMesh* sm_org, SimpleMesh* sm_def, int id) {

	Triangle* t = sm_org->tList[id];
	const vec3d& v0 = sm_org->vList[t->v0()].c;
	const vec3d& v1 = sm_org->vList[t->v1()].c;
	const vec3d& v2 = sm_org->vList[t->v2()].c;
	vec3d v3 = computeFourthPoint(v0, v1, v2);

	const vec3d& v0_t = sm_def->vList[t->v0()].c;
	const vec3d& v1_t = sm_def->vList[t->v1()].c;
	const vec3d& v2_t = sm_def->vList[t->v2()].c;
	vec3d v3_t = computeFourthPoint(v0_t, v1_t, v2_t);

	SquareMatrixND<vec3d> W, W_t;
	W.setColI(v1-v0, 0);
	W.setColI(v2-v0, 1);
	W.setColI(v3-v0, 2);
	W_t.setColI(v1_t-v0_t, 0);
	W_t.setColI(v2_t-v0_t, 1);
	W_t.setColI(v3_t-v0_t, 2);

	W = W.getInverted();

	return W_t*W;

}


// ****************************************************************************************


SquareMatrixND<vec3d> compute_abcdef(const SimpleMesh* sm, int idx) {

	Triangle* t = sm->tList[idx];

	vec3d dir1 = sm->vList[t->v1()].c - sm->vList[t->v0()].c;
	vec3d dir2 = sm->vList[t->v2()].c - sm->vList[t->v0()].c;

	SVDMatrix<double> Wj(3,2);
	for (UInt i1 = 0; i1 < 3; i1++) {
		Wj(i1, 0) = dir1[i1];
		Wj(i1, 1) = dir2[i1];
	}

	SVDMatrix<double> Wj_pseudo_SVD = Wj.computePseudoInverse();
	vec3d vec1(Wj_pseudo_SVD(0,0), Wj_pseudo_SVD(0,1), Wj_pseudo_SVD(0,2));
	vec3d vec2(Wj_pseudo_SVD(1,0), Wj_pseudo_SVD(1,1), Wj_pseudo_SVD(1,2));
	vec3d vec0 = -vec1-vec2;
	SquareMatrixND<vec3d> Wj_pseudo;
	Wj_pseudo.setColI(vec0, 0);
	Wj_pseudo.setColI(vec1, 1);
	Wj_pseudo.setColI(vec2, 2);

	return Wj_pseudo;
}


// ****************************************************************************************


struct DefTransInfo {
	PrecomputedMatrix* pm;
	std::vector<SquareMatrixND<vec3d> > Affines;
	std::vector<SquareMatrixND<vec3d> > Pseudos;
};


// ****************************************************************************************


DefTransInfo* computeSystemMatrixForDeformationTransfer(SimpleMesh* target_pose_0) {

	SimpleMesh* sm = target_pose_0;

	UInt m = (UInt)sm->getNumT();
	UInt n = (UInt)sm->getNumV();

	DefTransInfo* defInfo = new DefTransInfo;
	PrecomputedMatrix* pm = new PrecomputedMatrix;
	defInfo->pm = pm;

	// Compute AtA directly:
	int dim = n;
	pm->col_ptr.push_back(0);

	defInfo->Pseudos.resize(m);
	for (UInt i1 = 0; i1 < m; i1++) {
		defInfo->Pseudos[i1] = compute_abcdef(target_pose_0, i1);
		sm->tList[i1]->intFlag = i1;
	}

	pm->entries.push_back(1);
	pm->row_index.push_back(0);
	pm->col_ptr.push_back(pm->entries.size());
	for (UInt i1 = 1; i1 < n; i1++) {
		// Processing vertex i1;
		std::set<int> neighbors;
		for (UInt i2 = 0; i2 < sm->vList[i1].getNumN(); i2++) {
			neighbors.insert(sm->vList[i1].getNeighbor(i2, i1));
		}
		neighbors.insert(i1);

		for (std::set<int>::const_iterator it = neighbors.begin(); it != neighbors.end(); it++) {
			int n = (*it);

			if (n > (int)i1) continue; // Process only the lower triangle
			if (n == 0) continue;

			// We have an entry at (i1,n);
			double entry = 0;
			if (i1 == n) {
				std::set<int> allreadyProcessed;
				for (UInt i3 = 0; i3 < sm->vList[i1].getNumN(); i3++) {
					Edge* e = sm->vList[i1].eList[i3];
					for (UInt i2 = 0; i2 < e->tList.size(); i2++) {
						Triangle* t = e->tList[i2];

						if (allreadyProcessed.find(t->intFlag) != allreadyProcessed.end()) {
							continue;
						}
						allreadyProcessed.insert(t->intFlag);

						const SquareMatrixND<vec3d>& pseudo = defInfo->Pseudos[t->intFlag];

						int v_ = 0;
						for (UInt i3 = 1; i3 < 3; i3++) {
							if (t->getV(i3) == i1) v_ = i3;
						}
						entry += (pseudo.getColI(v_)|pseudo.getColI(v_));
					}
				}
			} else {
				Edge* e = sm->vList[i1].eList.getEdge(i1, n);
				for (UInt i2 = 0; i2 < e->tList.size(); i2++) {
					Triangle* t = e->tList[i2];
					const SquareMatrixND<vec3d>& pseudo = defInfo->Pseudos[t->intFlag];

					int v_i1 = 0;
					int v_n = 0;
					for (UInt i3 = 1; i3 < 3; i3++) {
						if (t->getV(i3) == i1) v_i1 = i3;
						if (t->getV(i3) == n)   v_n = i3;
					}
					entry += (pseudo.getColI(v_i1)|pseudo.getColI(v_n));
				}
			}
			pm->entries.push_back(entry);
			pm->row_index.push_back(n);
		}
		pm->col_ptr.push_back(pm->entries.size());
	}



	// create TAUCS matrix
	pm->A.n = n;
	pm->A.m = n;
	pm->A.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
	pm->A.colptr = &pm->col_ptr[0];
	pm->A.rowind = &pm->row_index[0];
	pm->A.values.d = &pm->entries[0];
	
	//printTaucsMatrixToFile("mat.txt", &pm->A);

	//taucs_logfile("stdout");

	// Compute permutation
	taucs_ccs_order(&pm->A, &pm->permutation, &pm->inversePermutation, "amd");
	// Compute permuted matrix template
	taucs_ccs_matrix* matrixTemplatePermuted = taucs_ccs_permute_symmetrically(&pm->A, pm->permutation, pm->inversePermutation);
	// Compute symbolic factorization of the matrix template
	pm->factorization = taucs_ccs_factor_llt_symbolic(matrixTemplatePermuted);
	// Compute permutation of the matrix
	pm->permutedMatrix = taucs_ccs_permute_symmetrically(&pm->A, pm->permutation, pm->inversePermutation);
	// Compute numeric factorization of the matrix
	taucs_ccs_factor_llt_numeric(pm->permutedMatrix, pm->factorization);

	return defInfo;
}


// ****************************************************************************************


void DeformTemplate(SimpleMesh* source_pose_0, SimpleMesh* source_pose_1, SimpleMesh* target_pose_0, SimpleMesh* output, DefTransInfo* defInfo) {

	SimpleMesh* sm = target_pose_0;

	UInt m = (UInt)sm->getNumT();
	UInt n = (UInt)sm->getNumV();

	PrecomputedMatrix* pm = defInfo->pm;


	defInfo->Affines.resize(m);
	for (UInt i1 = 0; i1 < m; i1++) {
		defInfo->Affines[i1] = computeDeformationGradient(source_pose_0, source_pose_1, i1);
	}


	//***************************************************************
	// Compute RHS
	//***************************************************************

	std::vector<double> x(n);
	std::vector<double> b(n);
	for (int comp = 0; comp<3; comp++) {

		for (UInt i1 = 0; i1 < n; i1++) {
			std::set<int> allreadyProcessed;
			double entry = 0;
			for (UInt i3 = 0; i3 < sm->vList[i1].getNumN(); i3++) {
				Edge* e = sm->vList[i1].eList[i3];
				for (UInt i2 = 0; i2 < e->tList.size(); i2++) {
					Triangle* t = e->tList[i2];

					if (allreadyProcessed.find(t->intFlag) != allreadyProcessed.end()) {
						continue;
					}
					allreadyProcessed.insert(t->intFlag);

					const SquareMatrixND<vec3d>& pseudo = defInfo->Pseudos[t->intFlag];
					const SquareMatrixND<vec3d>& affine = defInfo->Affines[t->intFlag];

					int v_ = 0;
					for (UInt i3 = 1; i3 < 3; i3++) {
						if (t->getV(i3) == i1) v_ = i3;
					}
					entry += (pseudo.getColI(v_)|affine.getRowI(comp));
				}
			}
			b[i1] = entry;
		}
		b[0] = 0;

		std::vector<double> permutedVariables_vec(n);
		std::vector<double> permutedRightHandSide_vec(n);
		double* permutedRightHandSide	= &permutedRightHandSide_vec[0];
		double* permutedVariables		= &permutedVariables_vec[0];

		// solve the linear system
		taucs_vec_permute(n, TAUCS_DOUBLE, &b[0], permutedRightHandSide, pm->permutation);
		// Solve for the unknowns
		taucs_supernodal_solve_llt(pm->factorization, permutedVariables, permutedRightHandSide);
		// Compute inverse permutation of the unknowns
		taucs_vec_permute(n, TAUCS_DOUBLE, permutedVariables, &x[0], pm->inversePermutation);

		for (UInt i1 = 0; i1 < n; i1++) {
			output->vList[i1].c[comp] = x[i1];
		}
	}
}

// ****************************************************************************************


AnimatedMesh* transformDeformationDefTrans(const AnimatedMesh& am_in, SimpleMesh* mesh_source_fit_to_target) {

	SimpleMesh sm_temp(*mesh_source_fit_to_target);
	DefTransInfo* defInfo = computeSystemMatrixForDeformationTransfer(mesh_source_fit_to_target);

	SimpleMesh* firstAniFrame = am_in.basemesh;
	SimpleMesh targetMesh(*firstAniFrame);

	AnimatedMesh* am_out = new AnimatedMesh;
	UInt numFrames = am_in.getNumFrames();
	UInt numV = (UInt)mesh_source_fit_to_target->getNumV();

	am_out->basemesh = mesh_source_fit_to_target;
	vec3d* verts_frame_1 = new vec3d[mesh_source_fit_to_target->getNumV()];
	for (UInt i1 = 0; i1 < numV; i1++) verts_frame_1[i1] = mesh_source_fit_to_target->vList[i1].c;
	am_out->frames.push_back(verts_frame_1);

	for (UInt i1 = 1; i1 < numFrames; i1++) {
		std::cerr << "Transfering frame " << i1 << " of " << numFrames << "                                   \r";
		vec3d* verts_frame_i = new vec3d[mesh_source_fit_to_target->getNumV()];

		for (UInt i2 = 0; i2 < numV; i2++)
			targetMesh.vList[i2] = am_in.frames[i1][i2];

		DeformTemplate(firstAniFrame, &targetMesh, mesh_source_fit_to_target, &sm_temp, defInfo);

		// Re-align:
		RigidTransformWithScale<vec3d> t = getOptimalAlignment(&targetMesh, &sm_temp);
		t.setScale(1);
		for (UInt i2 = 0; i2 < (UInt)sm_temp.getNumV(); i2++) {
			verts_frame_i[i2] = t.vecTrans(sm_temp.vList[i2].c);
		}

		am_out->frames.push_back(verts_frame_i);
	}

	return am_out;
}


// ****************************************************************************************


void DeformationTransfer(SimpleMesh* source_pose_0, SimpleMesh* source_pose_1, SimpleMesh* target_pose_0, SimpleMesh* output) {

	DefTransInfo* defInfo = computeSystemMatrixForDeformationTransfer(target_pose_0);
	DeformTemplate(source_pose_0, source_pose_1, target_pose_0, output, defInfo);

}


// ****************************************************************************************


//! Computes the best rigid matrix that transforms 'sm2' into 'sm1'. Must be inverted if you wish to transform sm1->sm2!
SquareMatrixND<vec3d> computeOptimalRigidTrans(SimpleMesh* sm1, SimpleMesh* sm2, int id) {

	SquareMatrixND<vec3d> COV;
	UInt v0 = id;
	for (UInt i3 = 0; i3 < (UInt)sm1->vList[v0].getNumN(); i3++) {
		UInt v1 = sm1->vList[v0].getNeighbor(i3, v0);
		SquareMatrixND<vec3d> tmp;
		const vec3d& p_i = sm1->vList[v0].c;
		const vec3d& p_j = sm1->vList[v1].c;
		const vec3d& p_dash_i = sm2->vList[v0].c;
		const vec3d& p_dash_j = sm2->vList[v1].c;
		tmp.addFromTensorProduct((p_i-p_j), (p_dash_i-p_dash_j));
		double w = 1;//sm1->vList[v0].eList[i3]->cotangent_weight;
		COV += tmp * w;
	}
	//COV.print();
	SquareMatrixND<vec3d> U, V;
	vec3d sigma;
	bool SVD_result = COV.SVD_decomp(U, sigma, V);
	//std::cerr << "Sigma: "; sigma.print();
	SquareMatrixND<vec3d> rot;
	if (SVD_result) {

		V.transpose();
		rot = U*V;

		double det = rot.getDeterminant();
		if (det < 0) {
			//std::cerr << "Flip sign" << std::endl;
			int sm_id = 0;
			double smallest = sigma[sm_id];
			for (UInt dd = 1; dd < 3; dd++) {
				if (sigma[dd] < smallest) {
					smallest = sigma[dd];
					sm_id = dd;
				}
			}
			// flip sign of entries in colums 'sm_id' in 'U'
			U.m[sm_id + 0] *= -1;
			U.m[sm_id + 3] *= -1;
			U.m[sm_id + 6] *= -1;
			rot = U*V;
		}
	} else { // SVD failed!
		std::cerr << "SVD failed" << std::endl;
	}

	return rot;

}

//! Computes the best affine matrix that transforms 'sm2' into 'sm1'. Must be inverted if you wish to transform sm1->sm2!
SquareMatrixND<vec3d> computeOptimalAffineTrans2(SimpleMesh* sm1, SimpleMesh* sm2, int id) {

	std::vector<int> fan;
	getVertexFan(sm1, id, fan);
	UInt numV = fan.size();

	SVDMatrix<double> M(3*numV, 9);
	M.zeroOutElements();
	std::vector<double> b(3*numV+1);

	for (UInt i1 = 0; i1 < numV; i1++) {
		vec3d p = sm1->vList[id].c-sm1->vList[fan[i1]].c;
		vec3d p_dash = sm2->vList[id].c-sm2->vList[fan[i1]].c;

		for (UInt i2 = 0; i2 < 3; i2++) {
			b[3*i1+i2 + 1] = p[i2];
			M(3*i1+i2, i2*3+0) = p_dash.x;
			M(3*i1+i2, i2*3+1) = p_dash.y;
			M(3*i1+i2, i2*3+2) = p_dash.z;
		}
	}
	M.computeSVD();
	std::vector<double> x(10);
	M.backSolveSVD(&b[0], &x[0]);

	SquareMatrixND<vec3d> trans(&x[1]);
	return trans;
}

//! Computes the best affine matrix that transforms 'sm2' into 'sm1'. Must be inverted if you wish to transform sm1->sm2!
SquareMatrixND<vec3d> computeOptimalAffineTrans(SimpleMesh* sm1, SimpleMesh* sm2, int id) {

	std::vector<int> fan;
	getVertexFan(sm1, id, fan);
	UInt numV = fan.size();

	SVDMatrix<double> M(3*numV, 9);
	M.zeroOutElements();
	std::vector<double> b(3*numV+1);

	for (UInt i1 = 0; i1 < numV; i1++) {
		vec3d p = sm1->vList[id].c-sm1->vList[fan[i1]].c;
		vec3d p_dash = sm2->vList[id].c-sm2->vList[fan[i1]].c;

		for (UInt i2 = 0; i2 < 3; i2++) {
			b[3*i1+i2 + 1] = p[i2];
			M(3*i1+i2, i2*3+0) = p_dash.x;
			M(3*i1+i2, i2*3+1) = p_dash.y;
			M(3*i1+i2, i2*3+2) = p_dash.z;
		}
	}
	M.computeSVD();
	std::vector<double> x(10);
	M.backSolveSVD(&b[0], &x[0]);

	SquareMatrixND<vec3d> trans(&x[1]);
	return trans;
}


AnimatedMesh* transformDeformationAffine(const AnimatedMesh& am_in, SimpleMesh* mesh_source_fit_to_target) {

	AnimatedMesh* am_out = new AnimatedMesh;
	UInt numFrames = am_in.getNumFrames();
	UInt numV = (UInt)mesh_source_fit_to_target->getNumV();

	am_out->basemesh = mesh_source_fit_to_target;
	vec3d* verts_frame_1 = new vec3d[mesh_source_fit_to_target->getNumV()];
	for (UInt i1 = 0; i1 < numV; i1++) verts_frame_1[i1] = mesh_source_fit_to_target->vList[i1].c;
	am_out->frames.push_back(verts_frame_1);
	for (UInt i1 = 1; i1 < numFrames; i1++) {
	}

	return am_out;

}

AnimatedMesh* transformDeformationRigid(const AnimatedMesh& am_in, SimpleMesh* mesh_source_fit_to_target, double global_scaler, const SquareMatrixND<vec3d>& global_align) {

	AnimatedMesh* am_out = new AnimatedMesh;
	UInt numFrames = am_in.getNumFrames();
	UInt numV = (UInt)mesh_source_fit_to_target->getNumV();

	am_out->basemesh = mesh_source_fit_to_target;
	vec3d* verts_frame_1 = new vec3d[mesh_source_fit_to_target->getNumV()];
	for (UInt i1 = 0; i1 < numV; i1++) verts_frame_1[i1] = mesh_source_fit_to_target->vList[i1].c;
	am_out->frames.push_back(verts_frame_1);
	for (UInt i1 = 1; i1 < numFrames; i1++) {
		vec3d* verts_frame_i = new vec3d[mesh_source_fit_to_target->getNumV()];
		for (UInt i2 = 0; i2 < numV; i2++) {
			vec3d dir = am_in.frames[i1-1][i2] - am_in.frames[i1][i2];
			dir *= global_scaler;
			vec3d dir_tar = global_align.vecTrans(dir);
			vec3d newpos = am_out->frames[i1-1][i2] - dir_tar;
			verts_frame_i[i2] = newpos;
		}
		am_out->frames.push_back(verts_frame_i);
	}

	return am_out;
}


SimpleMesh* DeformationTransferNaive(SimpleMesh* sm1, SimpleMesh* sm2 , SimpleMesh* mesh_source_fit_to_target, const std::vector<SquareMatrixND<vec3d> >& ts_targetmesh, const double* localScales) {

	SimpleMesh* sm_out = new SimpleMesh(*mesh_source_fit_to_target);
	UInt numV = (UInt)mesh_source_fit_to_target->getNumV();
	for (UInt i2 = 0; i2 < numV; i2++) {
		vec3d dir = sm1->vList[i2].c - sm2->vList[i2].c;
		dir *= localScales[i2];
		vec3d dir_tar = ts_targetmesh[i2].vecTrans(dir);
		sm_out->vList[i2].c = mesh_source_fit_to_target->vList[i2].c - dir_tar;
	}

	return sm_out;
}


AnimatedMesh* transFormDeformationLocalLocal(const AnimatedMesh& am_in, SimpleMesh* mesh_source_fit_to_target, const std::vector<SquareMatrixND<vec3d> >& ts_targetmesh, const double* localScales) {

	AnimatedMesh* am_out = new AnimatedMesh;
	UInt numFrames = am_in.getNumFrames();
	UInt numV = (UInt)mesh_source_fit_to_target->getNumV();

	am_out->basemesh = mesh_source_fit_to_target;
	vec3d* verts_frame_1 = new vec3d[mesh_source_fit_to_target->getNumV()];
	for (UInt i1 = 0; i1 < numV; i1++) verts_frame_1[i1] = mesh_source_fit_to_target->vList[i1].c;
	am_out->frames.push_back(verts_frame_1);
	for (UInt i1 = 1; i1 < numFrames; i1++) {
		vec3d* verts_frame_i = new vec3d[mesh_source_fit_to_target->getNumV()];
		for (UInt i2 = 0; i2 < numV; i2++) {
			vec3d dir = am_in.frames[i1-1][i2] - am_in.frames[i1][i2];
			dir *= localScales[i2];
			vec3d dir_tar = ts_targetmesh[i2].vecTrans(dir);
			vec3d newpos = am_out->frames[i1-1][i2] - dir_tar;
			verts_frame_i[i2] = newpos;
		}
		am_out->frames.push_back(verts_frame_i);
	}

	return am_out;
}


void computeLocalRigidTransformations(SimpleMesh* firstAniFrame, SimpleMesh* mesh_source_fit_to_target, std::vector<SquareMatrixND<vec3d> >& ts_targetmesh) {

	UInt numV = (UInt)firstAniFrame->getNumV();

	computeUrshapeWeightsChordal(firstAniFrame);
	// Recompute TS:
	for (UInt i2 = 0; i2 < numV; i2++) {
		SquareMatrixND<vec3d> COV;
		UInt v0 = i2;
		for (UInt i3 = 0; i3 < (UInt)firstAniFrame->vList[v0].getNumN(); i3++) {
			UInt v1 = firstAniFrame->vList[v0].getNeighbor(i3, v0);
			SquareMatrixND<vec3d> tmp;
			const vec3d& p_i = firstAniFrame->vList[v0].c;
			const vec3d& p_j = firstAniFrame->vList[v1].c;
			const vec3d& p_dash_i = mesh_source_fit_to_target->vList[v0].c;
			const vec3d& p_dash_j = mesh_source_fit_to_target->vList[v1].c;
			tmp.addFromTensorProduct((p_i-p_j) , (p_dash_i-p_dash_j));
			const double& w_ij = firstAniFrame->vList[v0].eList[i3]->cotangent_weight;
			tmp *= w_ij;
			COV += tmp;
		}
		//COV.print();
		SquareMatrixND<vec3d> U, V;
		vec3d sigma;
		bool SVD_result = COV.SVD_decomp(U, sigma, V);
		if (SVD_result) {

			V.transpose();
			SquareMatrixND<vec3d> rot = U*V;

			double det = rot.getDeterminant();
			if (det < 0) {
				int sm_id = 0;
				double smallest = sigma[sm_id];
				for (UInt dd = 1; dd < 3; dd++) {
					if (sigma[dd] < smallest) {
						smallest = sigma[dd];
						sm_id = dd;
					}
				}
				// flip sign of entries in colums 'sm_id' in 'U'
				U.m[sm_id + 0] *= -1;
				U.m[sm_id + 3] *= -1;
				U.m[sm_id + 6] *= -1;
				rot = U*V;
			}
			rot.transpose();
			ts_targetmesh[i2] = rot;
		} else { // SVD failed!
			std::cerr << "SVD failed" << std::endl;
			ts_targetmesh[i2].setToIdentity();
		}
	}
}

#endif
