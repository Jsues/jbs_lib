#ifndef MESH_ORIENT_CONSISTENT
#define MESH_ORIENT_CONSISTENT


#include "../SimpleMesh.h"
#include "../JBS_General.h"
#include <vector>
#include <queue>

namespace JBSlib {

	//! Orients the triangles in a mesh consistently.
	void orientConsistent(SimpleMesh* sm) {
		std::queue<Triangle*> unprocessedTris;

		// unmark all triagles:
		for (UInt i1 = 0; i1 < sm->tList.size(); i1++)
			sm->tList[i1]->marker = false;

		// start with triangle 0:
		unprocessedTris.push(sm->tList[0]);
		sm->tList[0]->marker = true;


		while (unprocessedTris.size() > 0) {
			Triangle* current = unprocessedTris.front();
			unprocessedTris.pop();

			// Insert all neighboring triangles that have not yet been marked into unprocessedTris:
			for (unsigned int i0 = 0; i0 < 3; i0++) {
				Edge* e = current->e0;
				if (i0 == 1)
					e = current->e1;
				if (i0 == 2)
					e = current->e2;

				vec3d normal = current->getNormal();

				for (unsigned int i1 = 0; i1 < e->tList.size(); i1++) {
					if (!e->tList[i1]->marker) {
						// Check if the triangle should be 'inverted':
						Triangle* other = e->tList[i1];

						bool order1 = false;
						bool order2 = false;

						for (unsigned int i2 = 0; i2 < 3; i2++) {
							if (other->getV(i2) == e->v0 && other->getV((i2+1)%3) == e->v1) order1 = true;
							if (current->getV(i2) == e->v0 && current->getV((i2+1)%3) == e->v1) order2 = true;
						}

						if (order1 == order2) {
							e->tList[i1]->invertTriangle();
						}
						e->tList[i1]->marker = true;
						unprocessedTris.push(e->tList[i1]);
					}
				}
			}
		}

		// Check if all triangles have been marked now:
		int not_marked = 0;
		for (unsigned int i1 = 0; i1 < sm->tList.size(); i1++)
			if (!sm->tList[i1]->marker)
				not_marked++;

		std::cerr << "Found " << not_marked << " unmarked triangles" << std::endl;


		return;
	}

};

#endif
