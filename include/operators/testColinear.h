#ifndef TEST_COLINEAR_H
#define TEST_COLINEAR_H


#include "point3d.h"


template<typename Point>
bool areColinear(const Point& v0, const Point& v1, const Point& v2, typename Point::ValueType epsilon = 0) {
	const Point& a = v0;
	Point		 d = (v1-v0);
	const Point& c = v2;

	//v0.print();
	//v1.print();
	//v2.print();

	typename Point::ValueType lambda = (-d.x*a.x-d.y*a.y-d.z*a.z + d.x*c.x+d.y*c.y+d.z*c.z) / (d.x*d.x+d.y*d.y+d.z*d.z);

	Point ptOnLine = a + d*lambda;

	//std::cerr << fabs(c.dist(ptOnLine)) << "  " << epsilon << std::endl;

	return (fabs(c.dist(ptOnLine)) <= epsilon);
}


#endif
