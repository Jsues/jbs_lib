#ifndef FITMESHTOFUNCTION_2_H
#define FITMESHTOFUNCTION_2_H


#include <map>


#include "ARAP.h"
#include "ScalarFunction.h"
#include "FileIO/MeshWriter.h"
#include "PointCloud.h"
#include "projMeshOntoImplicit.h"



#ifdef USEINVENTOR
#include <Inventor/Win/SoWin.h>
#include <Inventor/Win/viewers/SoWinExaminerViewer.h>
#include "iv_stuff/fancy_materials.h"
#include "iv_stuff/ImportExportCamera.h"
#include "iv_stuff/storeSnapshot.h"
#include "iv_stuff/saveAsIV.h"
#include "meshToIV.h"
#endif

#ifdef CHECK_NORMAL_REJECTS
#include "operators/computeVertexNormals.h"
#endif


template<class T>
inline bool isnan(T value) {
	return value != value;
}

template<class T>
inline bool isinfinity(T value)
{
	return std::numeric_limits<T>::has_infinity &&
	value == std::numeric_limits<T>::infinity();
}

template<class T>
inline bool isnan_or_inf(T value) {
	if (isinfinity(value)) return true;
	if (isnan(value)) return true;
	return false;
}


#define SEARCH_DISTANCE_SCALER 1


//template <class T>
//std::pair<double, double> getRealDist(SimpleMesh* sm, SimpleMesh* urshape, ScalarFunction<T>* func) {
//	std::pair<double, double> EDist;
//	EDist.first = 0;
//	EDist.second = 0;
//
//	UInt numOfPoints = (UInt)sm->getNumV();
//	for (UInt i1 = 0; i1 < numOfPoints; i1++) {
//		const vec3d& p0 = sm->vList[i1].c;
//		T grad;
//		typename T::ValueType val = func->evalValAndGrad(p0, grad);
//		EDist.second += val*val;
//	}
//
//	return EDist;
//}

template <class T>
typename T::ValueType getEDist(SimpleMesh* sm, T* currentGradients, double* currentCs) {

	double EDist = 0;

	UInt numOfPoints = (UInt)sm->getNumV();
	for (UInt i1 = 0; i1 < numOfPoints; i1++) {
		const vec3d& p0 = sm->vList[i1].c;

		double error = currentCs[i1]-(p0|currentGradients[i1]);
		EDist += error*error;
	}

	return (T::ValueType)EDist;
}




struct FitMeshToImplicitFunctionARAP2_Options {

	//! Constructor
	FitMeshToImplicitFunctionARAP2_Options() : numOuterIter(5), numInnerIter(5), writeDebugMeshes(false), verboseOutput(false), multiplier(1) {
	}

	//! number of iterations (projection / ARAP optimization)
	UInt numOuterIter;

	//! number of iterations for ARAP optimization
	UInt numInnerIter;

	//! If set to 'true', a debug mesh is written after each projection / ARAP optimization
	bool writeDebugMeshes;

	//! fitting process prints some additional infos if this is set to true.
	bool verboseOutput;

	//! Blending weight for fitting vs. rigid.
	double alpha;

	double multiplier;

	int frame_id;

	vec3f pc_move;
	float pc_scale;
};


template <class T>
void FitMeshToImplicitFunctionARAP2(SimpleMesh* sm, SimpleMesh* urshape, ScalarFunction<T>* func, PointCloudNormals<T>* pc, FitMeshToImplicitFunctionARAP2_Options config) {


	double previous_error = std::numeric_limits<typename T::ValueType>::max();


	typename T::ValueType alpha = (typename T::ValueType)config.alpha;

	//**********************************************************
	// Build kd-tree
	//**********************************************************
	UInt numPoints = pc->getNumPts();
	ANNpointArray dataPts = annAllocPts(numPoints, 3);
	for (UInt i1 = 0; i1 < numPoints; i1++) {
		dataPts[i1][0] = pc->getPoints()[i1][0]; dataPts[i1][1] = pc->getPoints()[i1][1]; dataPts[i1][2] = pc->getPoints()[i1][2];
	}
	ANNpoint queryPt = annAllocPt(3);
	ANNkd_tree* kdTree = new ANNkd_tree(dataPts, numPoints, 3);

	//**********************************************************
	// Probe median point distance
	//**********************************************************
	int* ids = new int[6];
	double* dists = new double[6];
	std::vector<typename T::ValueType> med_dist_vec;
	for (UInt i1 = 0; i1 < numPoints; i1 += numPoints/100) {
		queryPt[0] = pc->getPoints()[i1][0];
		queryPt[1] = pc->getPoints()[i1][1];
		queryPt[2] = pc->getPoints()[i1][2];
		kdTree->annkSearch(queryPt, 6, ids, dists, 0);
		for (UInt i2 = 1; i2 < 6; i2++) {
			med_dist_vec.push_back((typename T::ValueType)dists[i2]);
			//std::cerr << ids[i1] << " " << dists[i2] << std::endl;
		}
	}
	delete[] ids;
	delete[] dists;
	std::sort(med_dist_vec.begin(), med_dist_vec.end()); 
	double medDistSquare = med_dist_vec[med_dist_vec.size()/2];
	double medDist = sqrt(medDistSquare);
	//std::cerr << medDist << std::endl;
	double searchDistSquare = (SEARCH_DISTANCE_SCALER*medDist)*(SEARCH_DISTANCE_SCALER*medDist);


	//**********************************************************
	// Initialize Arrays for ARAP-Optimization
	//**********************************************************
	UInt numOfPoints = sm->getNumV();
	SquareMatrixND<vec3d>* rots = new SquareMatrixND<vec3d>[numOfPoints];
	T* currentGradients = new T[numOfPoints];
	//typename T::ValueType* currentVals = new T::ValueType[numOfPoints];
	double* currentCs = new double[numOfPoints];
	double* b = new double[3*numOfPoints];
	double* x = new double[3*numOfPoints];
	
	MeshWriter mw;

	void* factorization;
	int* permutation;
	int* inversePermutation;


	for (UInt outerIter = 0; outerIter < config.numOuterIter; outerIter++) {

		// Precompute 'constraints'
		UInt unconstrained = 0;

#ifdef CHECK_NORMAL_REJECTS
		vec3d* current_normals = computeVertexNormals(sm);
#endif

		for (UInt id = 0; id < numOfPoints; id++) {
			const vec3d& pos = sm->vList[id].c;
			T grad;
			typename T::ValueType val = func->evalValAndGrad(pos, grad);
			if (grad.squaredLength() > 0) grad.normalize();

			grad *=-1;




			vec3d pt_after_proj = pos - grad*val;

			queryPt[0] = pt_after_proj[0];
			queryPt[1] = pt_after_proj[1];
			queryPt[2] = pt_after_proj[2];

			UInt numInRadius = kdTree->annkFRSearch(queryPt, searchDistSquare*2, 0);

			//std::cerr << numInRadius << std::endl;
			if (numInRadius == 0) {
				val = 0;
				grad = vec3d(0,0,0);
				unconstrained++;
				sm->vList[id].color.x = 1;
				sm->vList[id].has_trace = false;
			} else {
				sm->vList[id].color.x = 0;
				sm->vList[id].has_trace = true;
			}

			if (isnan_or_inf(grad.squaredLength())) {
				std::cerr << "Error: grad is:" << grad << std::endl;
				grad = T(0,0,0);
			}
			if (isnan_or_inf(val)) {
				std::cerr << "Error: val is:" << val << std::endl;
				val = 0;
			}
			if (isnan_or_inf((pos|grad) - val)) {
				std::cerr << "Error: (pos|grad) - val is:" << (pos|grad) - val << std::endl;
				grad = T(0,0,0);
			}
			bool is_boundary = false;
			for (UInt i2 = 0; i2 < sm->vList[id].eList.size(); i2++) if (sm->vList[id].eList[i2]->tList.size() < 2) is_boundary = true;
			//if (is_boundary && sm->vList[id].has_trace) {
			//	val = 0;
			//	grad = vec3d(0,0,0);
			//	unconstrained++;
			//	sm->vList[id].param.x = 1;
			//	sm->vList[id].has_trace = false;
			//}

			// check for normal rejects:
#ifdef CHECK_NORMAL_REJECTS
			//std::cerr << (current_normals[id]|grad) << std::endl;
			if (((current_normals[id]|-grad) < 0.7) && sm->vList[id].has_trace){
				val = 0;
				grad = vec3d(0,0,0);
				unconstrained++;
				sm->vList[id].color.x = 2;
				sm->vList[id].has_trace = false;
			}
#endif


			currentGradients[id] = grad;
			//currentVals[id] = val;
			currentCs[id] = (pos|grad) - val;


		}
		if (config.verboseOutput) std::cerr << unconstrained << " of " << numOfPoints << " are unconstrained" << std::endl;
std::cerr << unconstrained << " of " << numOfPoints << " are unconstrained" << std::endl;
		//return;

		// Precompute system matrix:
		std::vector<double> entries;
		std::vector<int> row_index;				// row indices, 0-based
		std::vector<int> col_ptr;				// pointers to where columns begin in rowind and values 0-based, length is (n+1)
		col_ptr.push_back(0);

		UInt current_row = 0;
		if (config.verboseOutput) std::cerr << "building System Matrix..." << "                                \r";
		for (UInt i = 0; i < numOfPoints; i++) {

			const vec3d& p0 = urshape->vList[i].c;
			UInt numN = (UInt)urshape->vList[i].getNumN();
			vec3d n_i = currentGradients[i];
			std::map<int, SquareMatrixND<vec3d> > row;
			SquareMatrixND<vec3d> tmp;
			tmp.setToIdentity();
			tmp *= (2*urshape->vList[i].param.x);
			SquareMatrixND<vec3d> tmp2(n_i);
			tmp2 *= alpha;
			tmp += tmp2;
			row.insert(std::make_pair(i, tmp));

			// derive for p'_i.x and p'_i.y
			for (UInt i3 = 0; i3 < numN; i3++) {
				UInt neighbor = urshape->vList[i].getNeighbor(i3, i);
				const vec3d& p1 = urshape->vList[neighbor].c;

				const double& w_ij = urshape->vList[i].eList[i3]->cotangent_weight;
				SquareMatrixND<vec3d> tmp;
				tmp.setToIdentity();
				tmp *= (-w_ij*2);
				row.insert(std::make_pair(neighbor, tmp));
			}

			// Fill in matrix
			for (UInt line = 0; line < 3; line++) {
				for (std::map<int, SquareMatrixND<vec3d> >::const_iterator it = row.begin(); it != row.end(); it++) {
					UInt first_pos = it->first * 3;
					if (first_pos <= current_row) {
						entries.push_back(it->second(0, line));
						row_index.push_back(first_pos+0);
					}
					if ((first_pos+1) <= current_row) {
						entries.push_back(it->second(1, line));
						row_index.push_back(first_pos+1);
					}
					if ((first_pos+2) <= current_row) {
						entries.push_back(it->second(2, line));
						row_index.push_back(first_pos+2);
					}
				}
				col_ptr.push_back((UInt)entries.size());
				current_row++;
			}
		}

		// create TAUCS matrix
		taucs_ccs_matrix  A; // a matrix to solve Ax=b in CCS format
		A.n = numOfPoints*3;
		A.m = numOfPoints*3;
		A.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
		A.colptr = &col_ptr[0];
		A.rowind = &row_index[0];
		A.values.d = &entries[0];

		void* F = NULL;
		char* options[] = {"taucs.factor.LLT=true", NULL};
		void* opt_arg[] = { NULL };

		// Pre-factor matrix
		//taucs_logfile("stdout");

#ifdef CHECK_MATRIX_BEFORE_TAUCS
		for (UInt i = 0; i < entries.size(); i++) {
			if (isnan_or_inf(entries[i])) {
				std::cerr << entries[i] << std::endl;
				entries[i] = 1;
			}
		}
#endif


		if (outerIter == 0) {
			// Compute permutation
			taucs_ccs_order(&A, &permutation, &inversePermutation, "amd");
			// Compute permuted matrix template
			taucs_ccs_matrix* matrixTemplatePermuted = taucs_ccs_permute_symmetrically(&A, permutation, inversePermutation);
			// Compute symbolic factorization of the matrix template
			factorization = taucs_ccs_factor_llt_symbolic(matrixTemplatePermuted);
			delete matrixTemplatePermuted;
		}

		double* permutedRightHandSide = new double[numOfPoints*3];
		double* permutedVariables = new double[numOfPoints*3];

		if (config.verboseOutput) std::cerr << "computing numeric factorization...";

		// Compute permutation of the matrix
		taucs_ccs_matrix* permutedMatrix = taucs_ccs_permute_symmetrically(&A, permutation, inversePermutation);

		// Compute numeric factorization of the matrix
		taucs_ccs_factor_llt_numeric(permutedMatrix, factorization);	
//		checkForTaucsError(taucs_retval);

		if (config.verboseOutput) std::cerr << "done" << std::endl;

		std::vector<double> xv(numOfPoints*3);
		taucs_double* x = &xv[0]; // the unknown vector to solve Ax=b

		for (UInt innerIter = 0; innerIter < config.numInnerIter; innerIter++) {

			// recompute c_i;
			//for (UInt id = 0; id < numOfPoints; id++) {
			//	const vec3d& pos = sm->vList[id].c;
			//	currentCs[id] = (pos|currentGradients[id]) - currentVals[id];
			//}

			//**********************************************************
			// Find ideal rotations
			//**********************************************************
			if (config.verboseOutput) std::cerr << "estimating rotations..." << "                                \r";
			for (UInt id = 0; id < numOfPoints; id++) {
				SquareMatrixND<vec3d> COV;
				UInt v0 = id;
				for (UInt i3 = 0; i3 < (UInt)urshape->vList[v0].getNumN(); i3++) {
					UInt v1 = urshape->vList[v0].getNeighbor(i3, v0);
					SquareMatrixND<vec3d> tmp;
					tmp.addFromTensorProduct(urshape->vList[v0].c-urshape->vList[v1].c, sm->vList[v0].c-sm->vList[v1].c);
					const double& w_ij = urshape->vList[v0].eList[i3]->cotangent_weight;
					tmp *= w_ij;
					COV += tmp;
				}
				SquareMatrixND<vec3d> U, V;
				vec3d sigma;
				bool SVD_result = COV.SVD_decomp(U, sigma, V);
				if (SVD_result) {
					V.transpose();
					SquareMatrixND<vec3d> rot = U*V;
					double det = rot.getDeterminant();
					if (det < 0) {
						int sm_id = 0;
						double smallest = sigma[sm_id];
						for (UInt dd = 1; dd < 3; dd++) {
							if (sigma[dd] < smallest) {
								smallest = sigma[dd];
								sm_id = dd;
							}
						}
						// flip sign of entries in colums 'sm_id' in 'U'
						U.m[sm_id + 0] *= -1;
						U.m[sm_id + 3] *= -1;
						U.m[sm_id + 6] *= -1;
						rot = U*V;
					}
					rot.transpose();
					rots[id] = rot;
				} else { // SVD failed!
					//std::cerr << "SVD failed" << std::endl;
					rots[id].setToIdentity();
				}
			}

			//double EDef = getEDef(sm, urshape, rots);
			//double EDist = getEDist<T>(sm, currentGradients, currentCs);

			//if (config.verboseOutput)
			//	std::cerr << std::endl << "Total: " << EDef + alpha*EDist << "  (EDef = " << EDef << ",  EDist = " << EDist << ")" << std::endl;

			//double ETotal = EDef + alpha*EDist;

			////if (ETotal > previous_error) { // break all loops
			////	outerIter = config.numOuterIter;
			////	innerIter = config.numInnerIter;
			////}
			//previous_error = ETotal;


			//**********************************************************
			// Find vertex positions which match rotations best
			//**********************************************************
			// init b with zero:
			for (UInt i = 0; i < 3*numOfPoints; i++) b[i] = 0;

			if (config.verboseOutput) std::cerr << "building rhs..." << "                                \r";
			for (UInt i = 0; i < numOfPoints; i++) {

				const vec3d& p0 = urshape->vList[i].c;
				UInt numN = (UInt)urshape->vList[i].getNumN();


				// Fetch constraints
				vec3d n_i = currentGradients[i];
				double c_i = currentCs[i];

				// rhs:
				b[3*i+0] = b[3*i+1] = b[3*i+2] = 0;
				vec3d rhs = n_i * (alpha * c_i);
				b[3*i+0] += rhs.x;
				b[3*i+1] += rhs.y;
				b[3*i+2] += rhs.z;

				// derive for p'_i.x and p'_i.y
				for (UInt i3 = 0; i3 < numN; i3++) {
					UInt neighbor = urshape->vList[i].getNeighbor(i3, i);
					const vec3d& p1 = urshape->vList[neighbor].c;

					// right_hand_side:
					const double& w_ij = urshape->vList[i].eList[i3]->cotangent_weight;
					SquareMatrixND<vec3d> rot_avg = rots[i]+rots[neighbor];
					vec3d rhs = (rot_avg.vecTrans(p0-p1)) * w_ij;
					b[3*i+0] += rhs.x;
					b[3*i+1] += rhs.y;
					b[3*i+2] += rhs.z;
				}
			}


			if (config.verboseOutput) std::cerr << "solving LGS..." << "                                \r";

			//if (outerIter == 1)
			//	for (UInt i4 = 0; i4 < entries.size(); i4++)
			//		std::cerr << entries[i4] << std::endl;

			// create TAUCS right-hand size
			taucs_double* b_taucs = b; // right hand side vector to solve Ax=b
			//taucs_logfile("stdout");
			// Compute permutation of the right hand side
			taucs_vec_permute(numOfPoints*3, TAUCS_DOUBLE, b_taucs, permutedRightHandSide, permutation);
			// Solve for the unknowns
			taucs_supernodal_solve_llt(factorization, permutedVariables, permutedRightHandSide);
			// Compute inverse permutation of the unknowns
			taucs_vec_permute(numOfPoints*3, TAUCS_DOUBLE, permutedVariables, x, inversePermutation);


			for (UInt i2 = 0; i2 < numOfPoints; i2++) {
				double p_x = x[3*i2+0];
				double p_y = x[3*i2+1];
				double p_z = x[3*i2+2];

				if (isInfinity(p_x)) p_x = 0;
				if (isInfinity(p_y)) p_y = 0;
				if (isInfinity(p_z)) p_z = 0;

				sm->vList[i2].c = vec3d(p_x,p_y,p_z);
			}
		}

		alpha *= (T::ValueType)config.multiplier;
		// Free allocated memory
		taucs_ccs_free(permutedMatrix);
		delete [] permutedRightHandSide;
		delete [] permutedVariables;
		taucs_supernodal_factor_free_numeric(factorization);

#ifdef CHECK_NORMAL_REJECTS
		delete[] current_normals;
#endif


	}


	double EDef = getEDef(sm, urshape, rots);
	double EDist = getEDist<T>(sm, currentGradients, currentCs);
	std::cerr << std::endl << "Total: " << EDef + alpha*EDist << "  (EDef = " << EDef << ",  EDist = " << EDist << ")" << std::endl;

	delete [] permutation;
	delete [] inversePermutation;
	taucs_supernodal_factor_free(factorization);

	// CleanUp ann.
	delete kdTree;
	annDeallocPts(dataPts);
	annDeallocPt(queryPt);
	annClose();			// close ANN



	delete[] rots;
	delete[] currentGradients;
	//delete[] currentVals;
	delete[] currentCs;
	delete[] b;
	delete[] x;
}



#endif
