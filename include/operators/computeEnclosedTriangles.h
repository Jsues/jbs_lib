#ifndef COMPUTE_ENCLOSED_TRIANGLES
#define COMPUTE_ENCLOSED_TRIANGLES

#include <vector>
#include <queue>
#include <set>
#include "SimpleMesh.h"

//! Computes all triangles that are enclosed by the path 'edge_path'. It is assumed, that the start vertex is repeated as the last vertex, for example 23 43 11 9 12 <b>23</b>.
void ComputeEnclosedTriangles(std::vector<int> edge_path, SimpleMesh* sm) {

	if (edge_path.size() < 2) {
		std::cerr << "ComputeEnclosedTriangles: to few edges is selection path!" << std::endl;
		exit(EXIT_FAILURE);
	}

	std::set<Edge*> boundingEdges;
	for (unsigned int i1 = 0; i1 < edge_path.size()-1; i1++) {
		int v0 = edge_path[i1];
		int v1 = edge_path[(i1+1)%edge_path.size()];

		Edge* e = sm->vList[v0].eList.getEdge(v0, v1);

		if (e != NULL) {
			boundingEdges.insert(e);
		}

		// Not found?! Why does this happen, try the other vertex

		e = sm->vList[v1].eList.getEdge(v0, v1);

		if (e != NULL) {
			boundingEdges.insert(e);
		} else {
			std::cerr << "ComputeEnclosedTriangles: two successive entries in selection path do not form an edge in sm!" << std::endl;
			std::cerr << "at edge " << i1 << " of " << (int)edge_path.size() << std::endl;
			std::cerr << "Edge e(" << v0 << ", " << v1 << ")" << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	Edge* firstEdge = *boundingEdges.begin();
	Triangle* seedTriangle = firstEdge->tList[0];

	seedTriangle->marker = true;
	std::queue<Triangle*> unprocessedTris;
	unprocessedTris.push(seedTriangle);

	while (!unprocessedTris.empty()) {
		Triangle* current = unprocessedTris.front();
		unprocessedTris.pop();

		// Insert all neighboring triangles (that have not yet been marked into unprocessedTris) unless the edge is bounded:
		for (unsigned int i0 = 0; i0 < 3; i0++) {
			Edge* e = current->e0;
			if (i0 == 1)
				e = current->e1;
			if (i0 == 2)
				e = current->e2;

			// Check if 'e' is contained in 'boundingEdges', otherwise add Triangles adjacent to 'e' into our list
			if (boundingEdges.find(e) != boundingEdges.end())
				continue;				

			for (unsigned int i1 = 0; i1 < e->tList.size(); i1++) {
				if (!e->tList[i1]->marker) {
					e->tList[i1]->marker = true;
					unprocessedTris.push(e->tList[i1]);
				}
			}
		}
	}


}

#endif
