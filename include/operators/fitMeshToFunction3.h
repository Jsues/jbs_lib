#ifndef FITMESHTOFUNCTION_3_H
#define FITMESHTOFUNCTION_3_H


#include "fitMeshToFunction2.h"



struct FitMeshToImplicitFunctionARAP3_Options {

	//! Constructor
	FitMeshToImplicitFunctionARAP3_Options() : numOuterIter(5), numInnerIter(2), verboseOutput(false) {
	}

	//! number of iterations (projection / ARAP optimization)
	UInt numOuterIter;

	//! number of iterations for ARAP optimization
	UInt numInnerIter;

	//! Blending weight for fitting vs. rigid.
	double alpha;

	//! id of the frame which is reconstructed at the moment
	int frame_id;

	//! fitting process prints some additional infos if this is set to true.
	bool verboseOutput;

	vec3f pc_move;
	float pc_scale;
};


template <class T>
void FitMeshToImplicitFunctionARAP3(SimpleMesh* sm, SimpleMesh* urshape, HoppeImplicitFit<T>* func, PointCloudNormals<T>* pc, FitMeshToImplicitFunctionARAP3_Options config) {

	typedef T Point;
	typedef typename T::ValueType ValueType;

	double previous_error = std::numeric_limits<ValueType>::max();
	ValueType alpha = (ValueType)config.alpha;


	ValueType medDistSquare = func->getMedian6NNSpacing(100);
	ValueType medDist = sqrt(medDistSquare);
	ValueType searchDistSquare = (3*medDist)*(3*medDist);


	//**********************************************************
	// Initialize Arrays for ARAP-Optimization
	//**********************************************************
	UInt numOfPoints = sm->getNumV();
	SquareMatrixND<vec3d>* rots = new SquareMatrixND<vec3d>[numOfPoints];
	T* currentGradients = new T[numOfPoints];
	typename T::ValueType* currentVals = new T::ValueType[numOfPoints];
	double* currentCs = new double[numOfPoints];
	double* b = new double[3*numOfPoints];
	double* x = new double[3*numOfPoints];
	
	void* factorization;
	int* permutation;
	int* inversePermutation;

	for (UInt outerIter = 0; outerIter < config.numOuterIter; outerIter++) {

		// Precompute 'constraints'
		UInt unconstrained = 0;

#ifdef CHECK_NORMAL_REJECTS
		vec3d* current_normals = computeVertexNormals(sm);
#endif

		for (UInt id = 0; id < numOfPoints; id++) {
			const vec3d& pos = sm->vList[id].c;

			Point pt((ValueType)pos.x, (ValueType)pos.y, (ValueType)pos.z);
			UInt nearestPt = func->getNearestID(pos);
			const Point& other = func->m_pc->getPoints()[nearestPt];
			Point grad = func->m_pc->getNormals()[nearestPt];
			Point diff = other-pt;
			ValueType val = diff|grad;
			vec3d pt_after_proj = pos - grad*val;

			UInt numInRadius = func->getNumInRadius(Point((ValueType)pt_after_proj.x, (ValueType)pt_after_proj.y, (ValueType)pt_after_proj.z), searchDistSquare);

			//std::cerr << numInRadius << std::endl;
			if (numInRadius == 0) {
				val = 0;
				grad = Point(0,0,0);
				unconstrained++;
				sm->vList[id].color.x = 1;
				sm->vList[id].has_trace = false;
			} else {
				sm->vList[id].color.x = 0;
				sm->vList[id].has_trace = true;
			}

#ifdef DO_NAN_INF_CHECKS
			if (isnan_or_inf(grad.squaredLength())) {
				std::cerr << "Error: grad is:" << grad << std::endl;
				grad = T(0,0,0);
			}
			if (isnan_or_inf(val)) {
				std::cerr << "Error: val is:" << val << std::endl;
				val = 0;
			}
			if (isnan_or_inf((pos|grad) - val)) {
				std::cerr << "Error: (pos|grad) - val is:" << (pos|grad) - val << std::endl;
				grad = T(0,0,0);
			}
#endif

#ifdef CHECK_NORMAL_REJECTS
			// check for normal rejects:
			if (((current_normals[id]|grad) < 0.7) && sm->vList[id].has_trace){
				val = 0;
				grad = vec3d(0,0,0);
				unconstrained++;
				sm->vList[id].color.x = 2;
				sm->vList[id].has_trace = false;
			}
#endif
			currentGradients[id] = grad;
			currentVals[id] = val;
			currentCs[id] = (pos|grad) - val;
		}

		// Precompute system matrix:
		std::vector<double> entries;
		std::vector<int> row_index;				// row indices, 0-based
		std::vector<int> col_ptr;				// pointers to where columns begin in rowind and values 0-based, length is (n+1)
		col_ptr.push_back(0);

		UInt current_row = 0;
		if (config.verboseOutput) std::cerr << "building System Matrix..." << "                                \r";
		for (UInt i = 0; i < numOfPoints; i++) {

			const vec3d& p0 = urshape->vList[i].c;
			UInt numN = (UInt)urshape->vList[i].getNumN();
			vec3d n_i = currentGradients[i];
			std::map<int, SquareMatrixND<vec3d> > row;
			SquareMatrixND<vec3d> tmp;
			tmp.setToIdentity();
			tmp *= (2*urshape->vList[i].param.x);
			SquareMatrixND<vec3d> tmp2(n_i);
			tmp2 *= alpha;
			tmp += tmp2;
			row.insert(std::make_pair(i, tmp));

			// derive for p'_i.x and p'_i.y
			for (UInt i3 = 0; i3 < numN; i3++) {
				UInt neighbor = urshape->vList[i].getNeighbor(i3, i);
				const vec3d& p1 = urshape->vList[neighbor].c;

				const double& w_ij = urshape->vList[i].eList[i3]->cotangent_weight;
				SquareMatrixND<vec3d> tmp;
				tmp.setToIdentity();
				tmp *= (-w_ij*2);
				row.insert(std::make_pair(neighbor, tmp));
			}

			// Fill in matrix
			for (UInt line = 0; line < 3; line++) {
				for (std::map<int, SquareMatrixND<vec3d> >::const_iterator it = row.begin(); it != row.end(); it++) {
					UInt first_pos = it->first * 3;
					if (first_pos <= current_row) {
						entries.push_back(it->second(0, line));
						row_index.push_back(first_pos+0);
					}
					if ((first_pos+1) <= current_row) {
						entries.push_back(it->second(1, line));
						row_index.push_back(first_pos+1);
					}
					if ((first_pos+2) <= current_row) {
						entries.push_back(it->second(2, line));
						row_index.push_back(first_pos+2);
					}
				}
				col_ptr.push_back((UInt)entries.size());
				current_row++;
			}
		}

		// create TAUCS matrix
		taucs_ccs_matrix  A; // a matrix to solve Ax=b in CCS format
		A.n = numOfPoints*3;
		A.m = numOfPoints*3;
		A.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
		A.colptr = &col_ptr[0];
		A.rowind = &row_index[0];
		A.values.d = &entries[0];

		void* F = NULL;
		char* options[] = {"taucs.factor.LLT=true", NULL};
		void* opt_arg[] = { NULL };

		// Pre-factor matrix
		//taucs_logfile("stdout");

#ifdef CHECK_MATRIX_BEFORE_TAUCS
		for (UInt i = 0; i < entries.size(); i++) {
			if (isnan_or_inf(entries[i])) {
				std::cerr << entries[i] << std::endl;
				entries[i] = 1;
			}
		}
#endif


		if (outerIter == 0) {
			// Compute permutation
			taucs_ccs_order(&A, &permutation, &inversePermutation, "amd");
			// Compute permuted matrix template
			taucs_ccs_matrix* matrixTemplatePermuted = taucs_ccs_permute_symmetrically(&A, permutation, inversePermutation);
			// Compute symbolic factorization of the matrix template
			factorization = taucs_ccs_factor_llt_symbolic(matrixTemplatePermuted);
			delete matrixTemplatePermuted;
		}

		double* permutedRightHandSide = new double[numOfPoints*3];
		double* permutedVariables = new double[numOfPoints*3];

		if (config.verboseOutput) std::cerr << "computing numeric factorization...";

		// Compute permutation of the matrix
		taucs_ccs_matrix* permutedMatrix = taucs_ccs_permute_symmetrically(&A, permutation, inversePermutation);

		// Compute numeric factorization of the matrix
		taucs_ccs_factor_llt_numeric(permutedMatrix, factorization);	
//		checkForTaucsError(taucs_retval);

		if (config.verboseOutput) std::cerr << "done" << std::endl;

		vector<double> xv(numOfPoints*3);
		taucs_double* x = &xv[0]; // the unknown vector to solve Ax=b

		for (UInt innerIter = 0; innerIter < config.numInnerIter; innerIter++) {

			// recompute c_i;
			//for (UInt id = 0; id < numOfPoints; id++) {
			//	const vec3d& pos = sm->vList[id].c;
			//	currentCs[id] = (pos|currentGradients[id]) - currentVals[id];
			//}

			//**********************************************************
			// Find ideal rotations
			//**********************************************************
			if (config.verboseOutput) std::cerr << "estimating rotations..." << "                                \r";
			for (UInt id = 0; id < numOfPoints; id++) {
				SquareMatrixND<vec3d> COV;
				UInt v0 = id;
				for (UInt i3 = 0; i3 < (UInt)urshape->vList[v0].getNumN(); i3++) {
					UInt v1 = urshape->vList[v0].getNeighbor(i3, v0);
					SquareMatrixND<vec3d> tmp;
					tmp.addFromTensorProduct(urshape->vList[v0].c-urshape->vList[v1].c, sm->vList[v0].c-sm->vList[v1].c);
					const double& w_ij = urshape->vList[v0].eList[i3]->cotangent_weight;
					tmp *= w_ij;
					COV += tmp;
				}
				SquareMatrixND<vec3d> U, V;
				vec3d sigma;
				bool SVD_result = COV.SVD_decomp(U, sigma, V);
				if (SVD_result) {
					V.transpose();
					SquareMatrixND<vec3d> rot = U*V;
					double det = rot.getDeterminant();
					if (det < 0) {
						int sm_id = 0;
						double smallest = sigma[sm_id];
						for (UInt dd = 1; dd < 3; dd++) {
							if (sigma[dd] < smallest) {
								smallest = sigma[dd];
								sm_id = dd;
							}
						}
						// flip sign of entries in colums 'sm_id' in 'U'
						U.m[sm_id + 0] *= -1;
						U.m[sm_id + 3] *= -1;
						U.m[sm_id + 6] *= -1;
						rot = U*V;
					}
					rots[id] = rot;
				} else { // SVD failed!
					//std::cerr << "SVD failed" << std::endl;
					rots[id].setToIdentity();
				}
			}

			double EDef = getEDef(sm, urshape, rots);
			double EDist = getEDist<T>(sm, currentGradients, currentCs);

			if (config.verboseOutput)
				std::cerr << std::endl << "Total: " << EDef + alpha*EDist << "  (EDef = " << EDef << ",  EDist = " << EDist << ")" << std::endl;

			double ETotal = EDef + alpha*EDist;

			//if (ETotal > previous_error) { // break all loops
			//	outerIter = config.numOuterIter;
			//	innerIter = config.numInnerIter;
			//}
			previous_error = ETotal;


			//**********************************************************
			// Find vertex positions which match rotations best
			//**********************************************************
			// init b with zero:
			for (UInt i = 0; i < 3*numOfPoints; i++) b[i] = 0;

			if (config.verboseOutput) std::cerr << "building rhs..." << "                                \r";
			for (UInt i = 0; i < numOfPoints; i++) {

				const vec3d& p0 = urshape->vList[i].c;
				UInt numN = (UInt)urshape->vList[i].getNumN();


				// Fetch constraints
				vec3d n_i = currentGradients[i];
				double c_i = currentCs[i];

				// rhs:
				b[3*i+0] = b[3*i+1] = b[3*i+2] = 0;
				vec3d rhs = n_i * (alpha * c_i);
				b[3*i+0] += rhs.x;
				b[3*i+1] += rhs.y;
				b[3*i+2] += rhs.z;

				// derive for p'_i.x and p'_i.y
				for (UInt i3 = 0; i3 < numN; i3++) {
					UInt neighbor = urshape->vList[i].getNeighbor(i3, i);
					const vec3d& p1 = urshape->vList[neighbor].c;

					// right_hand_side:
					const double& w_ij = urshape->vList[i].eList[i3]->cotangent_weight;
					SquareMatrixND<vec3d> rot_avg = rots[i]+rots[neighbor];
					vec3d rhs = (rot_avg.vecTrans(p0-p1)) * w_ij;
					b[3*i+0] += rhs.x;
					b[3*i+1] += rhs.y;
					b[3*i+2] += rhs.z;
				}
			}


			if (config.verboseOutput) std::cerr << "solving LGS..." << "                                \r";

			// create TAUCS right-hand size
			taucs_double* b_taucs = b; // right hand side vector to solve Ax=b
			//taucs_logfile("stdout");
			// Compute permutation of the right hand side
			taucs_vec_permute(numOfPoints*3, TAUCS_DOUBLE, b_taucs, permutedRightHandSide, permutation);
			// Solve for the unknowns
			taucs_supernodal_solve_llt(factorization, permutedVariables, permutedRightHandSide);
			// Compute inverse permutation of the unknowns
			taucs_vec_permute(numOfPoints*3, TAUCS_DOUBLE, permutedVariables, x, inversePermutation);


			double movement = 0;
			for (UInt i2 = 0; i2 < numOfPoints; i2++) {
				double p_x = x[3*i2+0];
				double p_y = x[3*i2+1];
				double p_z = x[3*i2+2];

				if (isInfinity(p_x)) p_x = 0;
				if (isInfinity(p_y)) p_y = 0;
				if (isInfinity(p_z)) p_z = 0;
				vec3d newpos(p_x,p_y,p_z);
				movement += newpos.dist(sm->vList[i2].c);
				sm->vList[i2].c = newpos;
			}
			std::cerr << "Movement: " << movement << std::endl;

#ifdef WRITE_INTERMED_DEBUG_MESHES
			SimpleMesh sm_tmp(*sm);
			sm_tmp.resize(config.pc_scale, config.pc_move);
			for (int i3 = 0; i3 < sm_tmp.getNumV(); i3++) {
				sm_tmp.vList[i3].color.x = sm->vList[i3].color.x;
				sm_tmp.vList[i3].bool_flag = sm->vList[i3].bool_flag;
			}

			std::stringstream ss3;
			ss3 << "frame_" << std::setfill('0') << std::setw(4) << config.frame_id << "_" << std::setfill('0') << std::setw(4) << outerIter << "_" << std::setw(4) << innerIter << ".obj";

			for (int i3 = 0; i3 < sm_tmp.getNumV(); i3++) {
				if (sm_tmp.vList[i3].color.x == 0) sm_tmp.vList[i3].color = vec3f(1,0,0);
				else if (sm_tmp.vList[i3].color.x == 1) sm_tmp.vList[i3].color = vec3f(0,1,0);
				else if (sm_tmp.vList[i3].color.x == 2) sm_tmp.vList[i3].color = vec3f(0,0,1);
			}

			MeshWriter::writeOBJFileWithNormals(ss3.str().c_str(), &sm_tmp);
#endif

		}

		// Free allocated memory
		taucs_ccs_free(permutedMatrix);
		delete [] permutedRightHandSide;
		delete [] permutedVariables;
		taucs_supernodal_factor_free_numeric(factorization);

#ifdef CHECK_NORMAL_REJECTS
		delete[] current_normals;
#endif

	}


	double EDef = getEDef(sm, urshape, rots);
	double EDist = getEDist<T>(sm, currentGradients, currentCs);
	std::cerr << std::endl << "Total: " << EDef + alpha*EDist << "  (EDef = " << EDef << ",  EDist = " << EDist << ")" << std::endl;


	delete [] permutation;
	delete [] inversePermutation;
	taucs_supernodal_factor_free(factorization);


	delete[] rots;
	delete[] currentGradients;
	delete[] currentVals;
	delete[] currentCs;
	delete[] b;
	delete[] x;
}



#endif
