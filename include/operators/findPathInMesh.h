#ifndef FIND_PATH_IN_MESH_H
#define FIND_PATH_IN_MESH_H

#include "../SimpleMesh.h"
#include "hsv2rgb.h"
#include <vector>
#include <set>
#include <queue>



//! Mr.Masters priority queue.
class HeapNode {
	public:
		int     idx_;
		double  key_;
		HeapNode() {}
		HeapNode( int idx, double key) { idx_ = idx; key_ = key;}
		~HeapNode(){}
		inline bool operator >  (const HeapNode& x) const {
			return (this->key_ >   x.key_);
		}
		inline bool operator >= (const HeapNode& x) const {
			return (this->key_ >=  x.key_);
		}
		inline bool operator <  (const HeapNode& x) const {
			return (this->key_ <   x.key_);
		}
		inline bool operator <= (const HeapNode& x) const {
			return (this->key_ <=  x.key_);
		}
};

typedef std::priority_queue< HeapNode, std::vector<HeapNode>, std::greater<HeapNode> > HeapType;


void computeDistanceToAllVerts(SimpleMesh* sm, int start) {


	unsigned int numV = (unsigned int)sm->vList.size();
	double* costs	 = new double[numV];
	for (unsigned int i1 = 0; i1 < numV; i1++) {
		costs      [i1] = DBL_MAX;
	}

	HeapType*      queue_;
	queue_ = new HeapType;
	// the first one to push, so we can pop it!
	queue_->push(HeapNode(start, 0.0));
	costs[start]=0.0f;

	char* isVisited = new char[numV];
	for (unsigned int i1 = 0; i1 < numV; i1++) isVisited[i1] = false;

	int index;
	double dist;

	while(!queue_->empty()) {
		// Remove 'best' Vertex from remainingVertices.
		index = queue_->top().idx_;
		dist  = queue_->top().key_;
		queue_->pop();

		if (isVisited[index]) { // We have already processed this vertex.
			continue;
		}
		isVisited[index] = true;

		for (unsigned int i1 = 0; i1 < sm->vList[index].eList.size(); i1++) {
			int neighbor = (sm->vList[index].eList[i1]->v0 == index) ? sm->vList[index].eList[i1]->v1 : sm->vList[index].eList[i1]->v0;
			double edge_length = sm->vList[index].c.dist(sm->vList[neighbor].c);
			/* ******************** QUEUE ************ */
			double allcost = costs[index]+edge_length;
			if (allcost < costs[neighbor]) {
				queue_->push(HeapNode(neighbor, allcost));
				costs[neighbor] = allcost;
			}
		}
	}


	double max_cost = 0;
	for (UInt i1 = 0; i1 < numV; i1++) {
		if (costs[i1] == DBL_MAX) { // unreachable point!
			costs[i1] = 0;
		}
		if (costs[i1] > max_cost) max_cost = costs[i1];
	}
	max_cost *= 1.5; // color_adjustment

	// Store cost in vertex colors:
	for (UInt i1 = 0; i1 < numV; i1++) {
		sm->vList[i1].color = JBSlib::hsv2rgb<float>((float)costs[i1]/(float)max_cost, 1.0f, 1.0f);
		sm->vList[i1].param.x = (float)costs[i1]/(float)max_cost;
	}

	delete[] costs;
	delete[] isVisited;

}


std::vector<double> computeDistanceToAllTrianglesOnMeshDualGraph(SimpleMesh* sm, int start) {

	unsigned int numT = (unsigned int)sm->getNumT();
	int* predecessor = new int[numT];
	std::vector<double> costs;
	costs.resize(numT);
	std::vector<bool> visitedFlag;
	visitedFlag.resize(numT);
	for (unsigned int i1 = 0; i1 < numT; i1++) {
		predecessor[i1] = -1;
		costs      [i1] = DBL_MAX;
		visitedFlag[i1] = false;
	}

	HeapType* queue = new HeapType;
	// the first one to push, so we can pop it!
	queue->push(HeapNode(start, 0.0));
	visitedFlag[start] = true;
	costs[start]=0.0f;

	int index;
	double dist;

	while(!queue->empty()) {
		// Remove 'best' Vertex from remainingVertices.
		index = queue->top().idx_;
		dist  = queue->top().key_;
		queue->pop();

		Triangle* t = sm->tList[index];
		for (unsigned int i1 = 0; i1 < 3; i1++) {
			Edge* e;
			if (i1 == 0) e = t->e0;
			else if (i1 == 1) e = t->e1;
			else e = t->e2;

			if (e->tList.size() < 2) continue;

			Triangle* neighbor_t;
			if (e->tList[0] == t) neighbor_t = e->tList[1];
			else neighbor_t = e->tList[0];
			int neighbor = neighbor_t->intFlag;

			if (visitedFlag[neighbor]) continue;

			double edge_length = t->getCenter().dist(neighbor_t->getCenter());

			/* ******************** QUEUE ************ */
			float allcost = (float)costs[index]+(float)edge_length;
			if (allcost < costs[neighbor]) {
				queue->push(HeapNode(neighbor, allcost));
				visitedFlag[neighbor] = true;
				costs      [neighbor] = allcost;
				predecessor[neighbor] = index;
			}
		}
	}

	delete[] predecessor;

	return costs;

}


std::vector<unsigned int> DijkstraConvConc(SimpleMesh* sm, int end, int start, bool useOnlyConvexEdges) {
	//std::cerr << "Compute path from " << end << " to " << start << std::endl;

	unsigned int numV = (unsigned int)sm->vList.size();
	int* predecessor = new int[numV];
	double* costs	 = new double[numV];
	for (unsigned int i1 = 0; i1 < numV; i1++) {
		predecessor[i1] = -1;
		costs      [i1] = DBL_MAX;
	}

	HeapType*      queue_;
	queue_ = new HeapType;
	// the first one to push, so we can pop it!
	queue_->push(HeapNode(start, 0.0));
	costs[start]=0.0f;

	std::set<int> visitedVertices;

	int index;
	double dist;

	while(!queue_->empty()) {
		// Remove 'best' Vertex from remainingVertices.
		index = queue_->top().idx_;
		dist  = queue_->top().key_;
		queue_->pop();
		visitedVertices.insert(index);

		if (index == end)
			break;

		for (unsigned int i1 = 0; i1 < sm->vList[index].eList.size(); i1++) {
			if (sm->vList[index].eList[i1]->isConvex() != useOnlyConvexEdges) continue;

			int neighbor = (sm->vList[index].eList[i1]->v0 == index) ? sm->vList[index].eList[i1]->v1 : sm->vList[index].eList[i1]->v0;
			double edge_length = sm->vList[sm->vList[index].eList[i1]->v0].c.dist(sm->vList[sm->vList[index].eList[i1]->v1].c);
			/* ******************** QUEUE ************ */
			float allcost = (float)costs[index]+(float)edge_length;
			if (allcost < costs[neighbor]) {
				queue_->push(HeapNode(neighbor, allcost));
				costs      [neighbor] = allcost;
				predecessor[neighbor] = index;
			}
		}
	}

	int current = end;
	std::vector<unsigned int> shortest_path_temp;
	while(current != -1) {
		shortest_path_temp.push_back(current);
		current = predecessor[current];
	}
	if (shortest_path_temp[shortest_path_temp.size()-1] != start)
		shortest_path_temp.push_back(start);

	return shortest_path_temp;
}




std::vector<unsigned int> Dijkstra(SimpleMesh* sm, int end, int start) {
	//std::cerr << "Compute path from " << end << " to " << start << std::endl;

	unsigned int numV = (unsigned int)sm->vList.size();
	int* predecessor = new int[numV];
	double* costs	 = new double[numV];
	for (unsigned int i1 = 0; i1 < numV; i1++) {
		predecessor[i1] = -1;
		costs      [i1] = DBL_MAX;
	}

	HeapType*      queue_;
	queue_ = new HeapType;
	// the first one to push, so we can pop it!
	queue_->push(HeapNode(start, 0.0));
	costs[start]=0.0f;

	std::set<int> visitedVertices;

	int index;
	double dist;

	while(!queue_->empty()) {
		// Remove 'best' Vertex from remainingVertices.
		index = queue_->top().idx_;
		dist  = queue_->top().key_;
		queue_->pop();
		visitedVertices.insert(index);

		if (index == end)
			break;

		for (unsigned int i1 = 0; i1 < sm->vList[index].eList.size(); i1++) {
			int neighbor = (sm->vList[index].eList[i1]->v0 == index) ? sm->vList[index].eList[i1]->v1 : sm->vList[index].eList[i1]->v0;
			double edge_length = sm->vList[sm->vList[index].eList[i1]->v0].c.dist(sm->vList[sm->vList[index].eList[i1]->v1].c);
			/* ******************** QUEUE ************ */
			float allcost = (float)costs[index]+(float)edge_length;
			if (allcost < costs[neighbor]) {
				queue_->push(HeapNode(neighbor, allcost));
				costs      [neighbor] = allcost;
				predecessor[neighbor] = index;
			}
		}
	}

	int current = end;
	std::vector<unsigned int> shortest_path_temp;
	while(current != -1) {
		shortest_path_temp.push_back(current);
		current = predecessor[current];
	}
	if (shortest_path_temp[shortest_path_temp.size()-1] != start)
		shortest_path_temp.push_back(start);

	delete[] predecessor;
	delete[] costs;

	return shortest_path_temp;
}


//****************************************************************************************************


std::vector<unsigned int> FindMeshInPath_SUCKY_VERSION(SimpleMesh* sm, int start, int end) {
	std::vector<unsigned int> path;

	// Insert startVertex:
	path.push_back(start);

	vec3d start_v = sm->vList[start].c;
	vec3d end_v = sm->vList[end].c;

	vec3d dir = start_v-end_v;
	dir.normalize();

	int current = start;
	vec3d current_v = start_v;
	int next;
	double next_angle;

	std::cerr << "Compute path from " << start << " to " << end << std::endl;
	do {
		next_angle = -10000.0;
		for (unsigned int i1 = 0; i1 < sm->vList[current].eList.size(); i1++) {
			int n_ = (sm->vList[current].eList[i1]->v0 == current) ? sm->vList[current].eList[i1]->v1 : sm->vList[current].eList[i1]->v0;
			vec3d n_v = sm->vList[n_].c;
			vec3d dir2 = current_v - n_v;
			dir2.normalize();
			if ((dir | dir2) > next_angle) {
				next = n_;
				next_angle = (dir | dir2);
			}
		}
		current = next;
        path.push_back(current);
		current_v = sm->vList[current].c;
		dir = current_v-end_v;
		dir.normalize();
		//std::cerr << path.size() << " " << current << std::endl;
		//current_v.print();
		//dir.print();
		//sm->vList[end].c.print();
	} while (next != end);

	std::cerr << "Path computed" << std::endl;

	return path;
}

#endif
