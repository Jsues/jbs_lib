#ifndef TRACE_STREAM_LINE_H
#define TRACE_STREAM_LINE_H


#include "Polyline.h"
#include "SimpleMesh.h"


Triangle* findCell(SimpleMesh* sm, vec3d pos, Triangle* tri) {

	vec3d barys = tri->barycentric(vec2d(pos.x, pos.y));

	Edge* e;

	if (barys.x > 0) {
		if (barys.y > 0) {
			if (barys.z > 0) { // Point lies in triangle
				return tri;
			} else {
				e = tri->e2;
			}
		} else {
			e = tri->e1;
		}
	} else { // try neighbot at edge e0:
		e = tri->e0;
	}

	if (e->tList.size() == 2) {
		if (e->tList[0] == tri)
			return findCell(sm, pos, e->tList[1]);
		else
			return findCell(sm, pos, e->tList[0]);
	} else {
		std::cerr << "Error, hit boundary!" << std::endl;
        exit(EXIT_FAILURE);
	}

}

Triangle* getDirectionVectorAt(SimpleMesh* sm, vec3d pos, Triangle* tri, vec3f& vec) {

	tri->print();
	Triangle* cell = findCell(sm, pos, tri);
	cell->print();
	vec3d barys = cell->barycentric(vec2d(pos.x, pos.y));
	vec = tri->getInterpolatedColor(barys);
	return cell;

}


PolyLine<vec3d> traceStreamLine(float stepsize, SimpleMesh* sm, vec3d startPos, UInt startTriangleIndex) {
	PolyLine<vec3d> pl;

	if (startTriangleIndex >= sm->tList.size()) {
		std::cerr << "ERROR in traceStreamLine(...):" << std::endl;
		std::cerr << "Triangle with index " << startTriangleIndex << " does not exits in mesh, max. index is " << (UInt)sm->tList.size() << std::endl;
		std::cerr << "EXIT" << std::endl;
		exit(EXIT_FAILURE);
	}

	std::cerr << "Tracing streamline from triangle " << startTriangleIndex << " at position " << startPos << " with stepsize " << stepsize << std::endl;

	Triangle* startTri = sm->tList[startTriangleIndex];

	pl.insertVertex(startPos);

	float maxz = sm->max.z;

	for (UInt i1 = 0; i1 < 2000; i1++) {

		vec3f direction;
		startTri = getDirectionVectorAt(sm, startPos, startTri, direction);
		//std::cerr << i1 << ": " << direction << "   startTri: " << startTri << "   tri: " << tri << std::endl;
		startPos += vec3d(direction.x, direction.y, direction.z)*stepsize;
		
		pl.insertVertex(startPos);

		if (startPos.z > maxz-stepsize)
			break;
	}

	for (UInt i1 = 0; i1 < pl.getNumV()-1; i1++) {
		pl.insertLine(i1, i1+1);
	}

	return pl;
}


#endif
