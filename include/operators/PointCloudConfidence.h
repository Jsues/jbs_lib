#ifndef POINTCLOUD_CONFIDENCE_H
#define POINTCLOUD_CONFIDENCE_H


#include "PointCloud.h"
#include "ANN/ANN.h"
#include "ScalarFunction.h"

// ****************************************************************************************
//! The density-function of a PointCloud
template <class T>
class DensityFunction : public ScalarFunction<T> {

public:

// ****************************************************************************************

	typedef T Point;
	static const UInt dim = Point::dim;
	typedef typename Point::ValueType ValueType;

// ****************************************************************************************

	//! Initializes a Density Function for the Point Cloud 'pc' if 'sigma' == 0, the algorithm tries to estimate a suitable sigma
	DensityFunction(PointCloud<Point>* pc, ValueType cropAt = ValueType(0.001), ValueType sigma=0) {

		numANNalloc = pc->getNumPts();

		if (numANNalloc < 5) {
			std::cerr << "DensityFunction::DensityFunction() Error, pointcloud contains only " << numANNalloc << " points" << std::endl;
			return;
		}

		dataPts = annAllocPts(numANNalloc, dim);
		queryPt = annAllocPt(dim);

		const Point* pts = pc->getPoints();

		for (UInt i1 = 0; i1 < numANNalloc; i1++) {
			for (UInt x = 0; x < dim; x++) {
				dataPts[i1][x] = (double)pts[i1][x];
			}
		}

		nnIdx = new int[numANNalloc];
		dist = new double[numANNalloc];
		kdTree = new ANNkd_tree(dataPts, numANNalloc, dim);

		if (sigma == 0) { // guess sigma

			const int numSamples = 50;
			const int numDirectNeighbors = 4;

			std::vector<double> med_dists;

			int increment = (int)((float)numANNalloc/numSamples);
			for (UInt i1 = 0; i1 < numANNalloc; i1+=increment) {
				for (UInt i = 0; i < dim; ++i) {
					queryPt[i] = dataPts[i1][i];
				}
				kdTree->annkSearch(queryPt, numDirectNeighbors+1, nnIdx, dist);
				for (int i = 1; i <= numDirectNeighbors; i++) {
					med_dists.push_back(dist[i]);
				}
			}
			std::sort(med_dists.begin(), med_dists.end());
			double med_dist_sq = med_dists[med_dists.size()/2];
			//double med_dist = sqrt(med_dist_sq);
			//std::cerr << "Medium distance: " << med_dist << std::endl;

			// m_2_sig_sq = 4 * med_dist^2
			m_2_sig_sq = med_dist_sq * 4.0;
			// Enlarge m_2_sig_sq a little:
			m_2_sig_sq *= 2;

		} else {
			m_2_sig_sq = sigma;
		}

		m_searchRadiusSq = (-log(cropAt)*m_2_sig_sq);

		//std::cerr << "m_2_sig_sq: " << m_2_sig_sq << std::endl;
		//std::cerr << "cropAt: " << cropAt << std::endl;
		//std::cerr << "log(cropAt): " << log(cropAt) << std::endl;
		//std::cerr << "m_searchRadiusSq: " << m_searchRadiusSq << std::endl;
		//exit(1);
	}

// ****************************************************************************************

	//! Initializes a Density Function for the Point Cloud 'pc' if 'sigma' == 0, the algorithm tries to estimate a suitable sigma
	~DensityFunction() {
		delete[] nnIdx;
		delete[] dist;
		delete kdTree;
		annDeallocPts(dataPts);
		annDeallocPt(queryPt);
		annClose();
	}

// ****************************************************************************************

	ValueType eval(const Point& pos) const {

		int num_pts = getPointsWithinRange(pos);

		ValueType val = 0;
		for (int i1 = 0; i1 < num_pts; i1++) {
			val += (ValueType)exp(-dist[i1]/m_2_sig_sq);
		}

		return val;
	}

// ****************************************************************************************

	ValueType evalValAndGrad(const Point& pos, Point& grad) const {

		grad = Point(ValueType(0));

		int num_pts = getPointsWithinRange(pos);

		ValueType val = 0;
		for (int i1 = 0; i1 < num_pts; i1++) {
			ValueType the_exp = (ValueType)exp(-dist[i1]/m_2_sig_sq);
			val += the_exp;
			int id = nnIdx[i1];
			Point grad_incr;
			for (UInt d = 0; d < dim; d++)
				grad_incr[d] = ((ValueType)dataPts[id][d])-pos[d];
			grad_incr *= the_exp;
			grad += grad_incr;
		}

		if (val == 0) {
			val = (ValueType)0.0001;
			kdTree->annkSearch(queryPt, 1, nnIdx, dist);
			for (UInt d = 0; d < dim; d++)
				grad[d] = (ValueType)dataPts[nnIdx[0]][d]-pos[d];
			grad *= val;
		}


		return val;
	}

// ****************************************************************************************

	UInt getID() const {
		return ScalarFunction::TYPE_DENSITY_FUNC;
	}

// ****************************************************************************************

private:

// ****************************************************************************************

	//! looksup the points within sqrt('m_searchRadiusSq') distance around 'pos' and stores their IDs in 'nnIdx' and the (squared)distances in 'dist'. Returns the number of points
	int getPointsWithinRange(const Point& pos) const {
		for (UInt i = 0; i < dim; ++i) {
			queryPt[i] = pos[i];
		}

		int num_pts = kdTree->annkFRSearch(queryPt, m_searchRadiusSq, 0);

		kdTree->annkFRSearch(queryPt, m_searchRadiusSq, num_pts, nnIdx, dist);

		//if (num_pts >= 2) {
		//	kdTree->annkFRSearch(queryPt, m_searchRadiusSq, num_pts, nnIdx, dist);
		//} else {
		//	num_pts = 2;
		//	kdTree->annkSearch(queryPt, num_pts, nnIdx, dist);
		//}

		return num_pts;
	}

// ****************************************************************************************

	//! Number of points allocated for the kD-Tree.
	unsigned int numANNalloc;

	//! The ANN kD-Tree.
	ANNkd_tree* kdTree;

	//! Points of the kd-tree.
	ANNpointArray dataPts;
	
	//! Point structure for kd-tree queries
	ANNpoint queryPt;
	
	//! Index value from kd-tree queries
	int* nnIdx;
	
	//! Distance value from kd-tree queries
	double* dist;

	//! the part in exp(-(x-c)�/(m_2_sig_sq))
	double m_2_sig_sq;

	//! the squared search radius where e(radius) = cropAt.
	double m_searchRadiusSq;
};


#endif
