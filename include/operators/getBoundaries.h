#ifndef GET_BOUNDARIES_H
#define GET_BOUNDARIES_H

#include <algorithm>
#include "SimpleMesh.h"


bool sort_vec_pred( const std::vector<int>& x, const std::vector<int>& y ) {
	return (x.size() > y.size());
}

std::vector< std::vector <int> > getAllMeshBoundaries(SimpleMesh* sm) {

	std::vector< std::vector <int> > boundaries;

	sm->computeBoundaryVertices();
	for (int i1 = 0; i1 < sm->getNumV(); i1++) {
		if (sm->vList[i1].is_boundary_point) {
			// Test, if point is already in a boundary loop
			if (sm->vList[i1].param.x == 16) continue;

			// Find first edge:
			Edge* e = NULL;
			for (UInt i2 = 0; i2 < sm->vList[i1].getNumN(); i2++) {
				if (sm->vList[i1].eList[i2]->tList.size() == 1) {
					e = sm->vList[i1].eList[i2];
					break;
				}
			}
			if (e == NULL) {
				std::cerr << "Error in getAllMeshBoundaries(SimpleMesh* sm)" << std::endl;
			}

			bool included;
			std::vector<int> b = sm->getBoundaryStartAtEdge(e, included);
			// set all Vertices in 'b' to 16
			for (UInt i2 = 0; i2 < b.size(); i2++) {
				sm->vList[b[i2]].param.x = 16;
			}
			boundaries.push_back(b);

		}
	}

	std::sort(boundaries.begin(), boundaries.end(), sort_vec_pred);

	return boundaries;
}

double getBoundaryLength(const std::vector<int>& boundary, const SimpleMesh* sm) {

	double l = 0;
	for (int i1 = 0; i1 < (int)boundary.size()-1; i1++) {
		const int& v0 = boundary[i1];
		const int& v1 = boundary[i1+1];
		l += sm->vList[v0].c.dist(sm->vList[v1].c);
	}
	return l;
}


std::vector<int> getLongestMeshBoundary(SimpleMesh* sm) {

	std::vector< std::vector <int> > allB = getAllMeshBoundaries(sm);

	double lmax = 0;
	int maxIndex = 0;
	for (int i1 = 0; i1 < (int)allB.size(); i1++) {
		double l = getBoundaryLength(allB[i1], sm);
		if (l > lmax) {
			lmax = l;
			maxIndex = i1;
		}
	}

	return allB[maxIndex];

}


#endif
