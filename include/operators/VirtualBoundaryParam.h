#ifndef VIRTUAL_BOUNDARY_PARAM_H
#define VIRTUAL_BOUNDARY_PARAM_H


#include "TriangleWrapper.h"
#include "SimpleMesh.h"
#include "PolyLine.h"


void BoundaryUnfold2(SimpleMesh* sm, std::vector<int>& boundary) {

	std::cerr << "BoundaryUnfold" << std::endl;

	JBS_Timer t;
	t.log("Boundary Unfold");
	std::vector<vec3d> moves;
	std::vector<int> move_vs;

	// For all boundary vertices, check whether they are contained in a triangle of which they are not a vertex:
	for (UInt i1 = 0; i1 < boundary.size(); i1++) {
		int id = boundary[i1];

		vec3d pt = sm->vList[id].c;
		vec3d pt_org = pt;
		// Bruteforcefind
		for (UInt i2 = 0; i2 < (UInt)sm->getNumT(); i2++) {
			Triangle* t = sm->tList[i2];

			if (t->m_v0 == id || t->m_v1 == id || t->m_v2 == id) continue;

			vec3d barys = t->barycentric_with_proj(pt);
			if (barys.x >= 0 && barys.y >= 0 && barys.z >= 0) {
				
				std::cerr << "intersect" << std::endl;
				// get Boundary direction:
				vec3d boundaryDir;
				double eLengths = 0;
				for (UInt i3 = 0; i3 < sm->vList[id].getNumN(); i3++) {
					int neighbor = sm->vList[id].getNeighbor(i3, id);
					const vec3d& n = sm->vList[neighbor].c;
					vec3d dir = n-pt_org;
					double l = dir.length();
					if (l > 0) {
						dir /= l;
						eLengths += l;
					}
					boundaryDir += dir;
				}
				eLengths /= sm->vList[id].getNumN();
				boundaryDir.normalize();
				boundaryDir *= eLengths;

				moves.push_back(boundaryDir);
				move_vs.push_back(id);

				//break;
			}
		}
	}

	for (UInt i1 = 0; i1 < move_vs.size(); i1++) {
		vec3d dir = -moves[i1]/3.0;
		dir.print();
		sm->vList[move_vs[i1]].c -= dir;
	}

	t.log();
	t.print();
}


//! Checks if the lines (p0-p1) and (p2-p3) intersect
bool checkIntersection(const vec2d& p0, const vec2d& p1, const vec2d& p2, const vec2d& p3) {


	double denom = (p3.y-p2.y)*(p1.x-p0.x) - (p3.x-p2.x)*(p1.y-p0.y);
	double nom1 = (p3.x-p2.x)*(p0.y-p2.y) - (p3.y-p2.y)*(p0.x-p2.x);
	double nom2 = (p1.x-p0.x)*(p0.y-p2.y) - (p1.y-p0.y)*(p0.x-p2.x);

	if (denom == 0) { // parallel
		if ((nom1 == 0) && (nom2 == 0)) {
			std::cerr << "Lines are coincident" << std::endl;
			return true;
		}
		return false;
	}

	nom1/=denom;
	nom2/=denom;

	if ((nom1 >= 0 && nom1 <= 1) && (nom2 >= 0 && nom2 <= 1)) {
		std::cerr << "interesection" << std::endl;
		std::cerr << nom1 << " " << nom2 << std::endl;
		p0.print();
		p1.print();
		p2.print();
		p3.print();
		return true;
	}
	return false;
}


int BoundaryUnfold(SimpleMesh* sm, std::vector<vec2d>& param, std::vector<int>& boundary) {

	std::set<int> penetratingVerts;

	for (UInt i1 = 0; i1 < boundary.size()-1; i1++) {
		const int& v0 = boundary[i1];
		const int& v1 = boundary[i1+1];

		const vec2d& p0 = param[v0];
		const vec2d& p1 = param[v1];

		for (UInt i2 = i1+2; i2 < boundary.size()-1; i2++) {

			const int& v2 = boundary[i2];
			const int& v3 = boundary[i2+1];
			const vec2d& p2 = param[v2];
			const vec2d& p3 = param[v3];

			if (v0 == v3) continue;

			if (checkIntersection(p0, p1, p2, p3)) {
				// Find out which vertex "penetrates"
				Triangle* t0 = sm->vList[v0].eList.getEdge(v0, v1)->tList[0];
				Triangle* t1 = sm->vList[v2].eList.getEdge(v2, v3)->tList[0];

				vec3d bary = t1->barycentric_with_proj(vec3d(p0.x, p0.y, 0));
				if (bary.x >= 0 && bary.y >= 0 && bary.z >= 0) penetratingVerts.insert(v0);

				bary = t1->barycentric_with_proj(vec3d(p1.x, p0.y, 1));
				if (bary.x >= 0 && bary.y >= 0 && bary.z >= 0) penetratingVerts.insert(v1);

				bary = t0->barycentric_with_proj(vec3d(p2.x, p2.y, 0));
				if (bary.x >= 0 && bary.y >= 0 && bary.z >= 0) penetratingVerts.insert(v2);

				bary = t0->barycentric_with_proj(vec3d(p3.x, p3.y, 0));
				if (bary.x >= 0 && bary.y >= 0 && bary.z >= 0) penetratingVerts.insert(v3);

			}
		}
	}

	for (std::set<int>::const_iterator it = penetratingVerts.begin(); it != penetratingVerts.end(); ++it) {
		int id = *it;
		const vec3d& pt = sm->vList[id].c;
		vec3d boundaryDir;
		double eLengths = 0;
		for (UInt i3 = 0; i3 < sm->vList[id].getNumN(); i3++) {
			int neighbor = sm->vList[id].getNeighbor(i3, id);
			const vec3d& n = sm->vList[neighbor].c;
			vec3d dir = n-pt;
			double l = dir.length();
			if (l > 0) {
				dir /= l;
				eLengths += l;
			}
			boundaryDir += dir;
		}
		eLengths /= sm->vList[id].getNumN();
		boundaryDir.normalize();
		boundaryDir *= eLengths;

		boundaryDir.print();
		//boundaryDir = vec3d(1,1,1);

		boundaryDir /= 3;
		sm->vList[id].c += boundaryDir;
		param[id] += vec2d(boundaryDir.x, boundaryDir.y);
	}

	std::cerr << "penetratingVerts.size(): " << penetratingVerts.size() << std::endl;

	return penetratingVerts.size();
}


void doTrivialBoundaryOverlaps(SimpleMesh* sm, std::vector<vec2d>& param, std::vector<int>& boundary) {

	// For all boundary vertices, check whether they are contained in a triangle of which they are not a vertex:
	for (UInt i1 = 0; i1 < boundary.size(); i1++) {
		int id = boundary[i1];

		vec3d pt = sm->vList[id].c;
		vec3d pt_org = pt;

		int v1 = sm->vList[id].eList[0]->getOther(id);
		int v2 = sm->vList[id].eList[1]->getOther(id);
		Edge* e = sm->vList[v1].eList.getEdge(v1,v2);

		for (UInt i2 = 0; i2 < e->tList.size(); i2++) {
			Triangle* t = e->tList[i2];
			if (t->getOther(v1,v2) == id) continue;

			vec3d barys = t->barycentric_with_proj(pt);
			if (barys.x >= 0 && barys.y >= 0 && barys.z >= 0) {
				
				std::cerr << "intersect" << std::endl;
				// get Boundary direction:
				vec3d boundaryDir;
				double eLengths = 0;
				for (UInt i3 = 0; i3 < sm->vList[id].getNumN(); i3++) {
					int neighbor = sm->vList[id].getNeighbor(i3, id);
					const vec3d& n = sm->vList[neighbor].c;
					vec3d dir = n-pt_org;
					double l = dir.length();
					if (l > 0) {
						dir /= l;
						eLengths += l;
					}
					boundaryDir += dir;
				}
				eLengths /= sm->vList[id].getNumN();
				boundaryDir.normalize();
				boundaryDir *= eLengths;

				sm->vList[id].c += boundaryDir;
				param[id] += vec2d(boundaryDir.x, boundaryDir.y);
			}

		}

	}

}


void unfoldEdge(Edge* e, SimpleMesh* sm) {
	// Find overfolded vertex:
	Triangle* t0 = e->tList[0];
	Triangle* t1 = e->tList[1];

	Triangle* t;
	int other_corner;

	int overfolded_v;
	if (sm->vList[t0->getOther(e->v0, e->v1)].is_boundary_point) {
		overfolded_v = t0->getOther(e->v0, e->v1);
		other_corner = t1->getOther(e->v0, e->v1);
		t = t1;
	}
	else {
		overfolded_v = t1->getOther(e->v0, e->v1);
		other_corner = t0->getOther(e->v0, e->v1);
		t = t0;
	}

	int pos = 0;
	if (t->m_v1 == other_corner) pos = 1;
	else if (t->m_v2 == other_corner) pos = 2;

	vec3d center = t->getInterpolatedPos(vec3d(1.0/3.0));
	vec3d barys(0.5);
	barys[pos] = 0;
	vec3d bound  = t->getInterpolatedPos(barys);

	vec3d ddd = bound-center;
	ddd / 5;

	sm->vList[overfolded_v].c = bound+ddd;

}


void unfoldTrivialBoundaryOverlaps(SimpleMesh* sm, std::vector<vec2d>& param, std::vector<int>& boundary) {

	sm->computeBoundaryVertices();

	for (UInt i1 = 0; i1 < sm->eList.size(); i1++) {
		Edge* e = sm->eList[i1];

		if (sm->vList[e->v0].is_boundary_point || sm->vList[e->v1].is_boundary_point) {
			double da = e->computeDihedralAngle(); // da should be 0
			if (da > 0.001) { // Unfold
				unfoldEdge(e, sm);
			}
		}
	}

}

SimpleMesh* VirtualBoundaryParam(SimpleMesh* sm, std::vector<vec2d>& param, std::vector<int>& boundary) {

	//BoundaryUnfold2(sm, boundary);
	unfoldTrivialBoundaryOverlaps(sm, param, boundary);
	for (UInt i1 = 0; i1 < (UInt)sm->getNumV(); i1++) {
		param[i1] = vec2d(sm->vList[i1].c.x, sm->vList[i1].c.y);
	}
	//doTrivialBoundaryOverlaps(sm, param, boundary);
	MeshWriter::writeOFFFile("trivial_unfolded.off", sm);

	do {
	} while (BoundaryUnfold(sm, param, boundary) > 0);
	//BoundaryUnfold(sm, param, boundary);
	//BoundaryUnfold(sm, param, boundary);
	//BoundaryUnfold(sm, param, boundary);
	//for (UInt i1 = 0; i1 < (UInt)sm->getNumV(); i1++) {
	//	param[i1] = vec2d(sm->vList[i1].c.x, sm->vList[i1].c.y);
	//}
	MeshWriter::writeOFFFile("unfolded.off", sm);
	//exit(1);

	// compute average edge Length
	UInt numE = sm->eList.size();
	double avgLength = 0;
	for (UInt i1 = 0; i1 < numE; i1++) {
		Edge* e = sm->eList[i1];
		const int& v0 = e->v0; const int& v1 = e->v1;
		avgLength += param[v0].dist(param[v1]);
	}
	avgLength /= numE;


	// find a point within the mesh (i.e. inside the hole).
	Edge* e = sm->vList[boundary[0]].eList.getEdge(boundary[0], boundary[1]);
	int other = e->tList[0]->getOther(boundary[0], boundary[1]);
	vec2d innerPoint = param[other];


	std::vector<std::pair<int, int>> hull =	computeConvexHullTriangle(&param[0], param.size());

	PolyLine<vec2d> pl;
	std::map<int, int> vertexLookup;
	std::vector<int> vertexLookupRev;

	for (UInt i1 = 0; i1 < boundary.size()-1; i1++) {
		int v0 = boundary[i1];
		if (vertexLookup.find(v0) == vertexLookup.end()) {
			vertexLookup.insert(std::pair<int,int>(v0, vertexLookup.size()));
			vertexLookupRev.push_back(v0);
			pl.insertVertex(param[v0]);
		}
	}
	for (UInt i1 = 0; i1 < boundary.size()-1; i1++) {
		pl.insertLine(i1, (i1+1)%(boundary.size()-1));
	}
	std::cerr << pl.getNumV() << " numV" << std::endl;

	for (UInt i1 = 0; i1 < hull.size(); i1++) {
		int v0 = hull[i1].first;
		int v1 = hull[i1].second;
		if (vertexLookup.find(v0) == vertexLookup.end()) {
			vertexLookup.insert(std::pair<int,int>(v0, vertexLookup.size()));
			pl.insertVertex(param[v0]);
		}
		if (vertexLookup.find(v1) == vertexLookup.end()) {
			vertexLookup.insert(std::pair<int,int>(v1, vertexLookup.size()));
			pl.insertVertex(param[v1]);
		}	
	}

	SimpleMesh* smadd = constrainedDelaunay(&pl, innerPoint, avgLength*avgLength);

	// Now add new points and triangles:
	for (UInt i1 = boundary.size()-1; i1 < (UInt)smadd->getNumV(); i1++) {
		sm->insertVertex(smadd->vList[i1].c);
		vertexLookupRev.push_back(sm->getNumV()-1);
	}


	for (UInt i1 = 0; i1 < (UInt)smadd->getNumT(); i1++) {
		Triangle* t = smadd->tList[i1];

		int v0 = vertexLookupRev[t->v0()];
		int v1 = vertexLookupRev[t->v1()];
		int v2 = vertexLookupRev[t->v2()];

		Edge* e0 = sm->vList[v0].eList.getEdge(v0,v1);
		Edge* e1 = sm->vList[v1].eList.getEdge(v1,v2);
		Edge* e2 = sm->vList[v2].eList.getEdge(v2,v0);

		if ((e0 != NULL && e0->tList.size() >= 2) || (e1 != NULL && e1->tList.size() >= 2) || (e2 != NULL && e2->tList.size() >= 2)) {
			std::cerr << "Error, insert triangle at full edge!" << std::endl;
			continue;
		}

		sm->insertTriangle(v0,v1,v2);
	}

	//sm->cleanValence0Vertices();
	MeshWriter::writeOFFFile("virtBound.off", sm);

	system("meshParamMeanValue.exe virtBound.off 3 virtBound_mean.off");
	SimpleMesh* new_sm = MeshReader::readOFFFile("virtBound_mean.off");

	return new_sm;

	//std::vector<int> boundary2 = sm->getBoundary();
	//Parameterizer* parametr = new Parameterizer(sm, boundary2);
	//parametr->doParametrization(Parameterizer::CHORD, Parameterizer::USEMESHVERTICES);
	//parametr->createOFFFile("param2.off");

	//for (UInt i1 = 0; i1 < (UInt)sm->getNumV(); i1++) {
	//	sm->vList[i1].c = vec3d(parametr->parametrization[i1].x, parametr->parametrization[i1].y, 0);
	//}
}


#endif
