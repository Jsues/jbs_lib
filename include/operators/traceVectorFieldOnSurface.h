#ifndef TRACE_VECTORFIELD_ON_SURFACE
#define TRACE_VECTORFIELD_ON_SURFACE


#include "Polyline.h"
#include "SimpleMesh.h"


int im_a_temporary_variable;


Triangle* findTriOnMesh(SimpleMesh* sm, vec3d pos, Triangle* tri, bool& stop) {

	//im_a_temporary_variable++;

	//if (im_a_temporary_variable > 4) {
	//	std::cerr << "lala" << std::endl;

	//	std::cerr << "blub" << std::endl;
	//}

	//pos.print();
	vec3d barys = tri->barycentric_with_proj(pos);
	//pos.print();

	Edge* e;

	if (barys.x >= 0) {
		if (barys.y >= 0) {
			if (barys.z >= 0) { // Point lies in triangle
				return tri;
			} else {
				e = tri->e2;
			}
		} else {
			e = tri->e1;
		}
	} else { // try neighbor at edge e0:
		e = tri->e0;
	}

	if (e->tList.size() == 2) {
		if (e->tList[0] == tri)
			return findTriOnMesh(sm, pos, e->tList[1], stop);
		else
			return findTriOnMesh(sm, pos, e->tList[0], stop);
	} else {
		stop = true;
		return tri;
	}

}

Triangle* getDirectionVectorOnMeshAt(SimpleMesh* sm, vec3d pos, Triangle* tri, vec3f& vec, bool& stop) {
	Triangle* cell = findTriOnMesh(sm, pos, tri, stop);
	vec3d barys = cell->barycentric(pos);
	vec = tri->getInterpolatedColor(barys);
	return cell;
}

vec3d projPointOnMesh(SimpleMesh* sm, vec3d pos, Triangle* tri) {
	bool stop;
	Triangle* cell = findTriOnMesh(sm, pos, tri, stop);
	vec3d barys = cell->barycentric_with_proj(pos);
	return pos;
}


PolyLine<vec3d> traceVectorFieldOnSurfaceEuler(float stepsize, SimpleMesh* sm, vec3d startPos, UInt startTriangleIndex) {

	PolyLine<vec3d> pl;

	if (startTriangleIndex >= sm->tList.size()) {
		std::cerr << "ERROR in traceStreamLine(...):" << std::endl;
		std::cerr << "Triangle with index " << startTriangleIndex << " does not exits in mesh, max. index is " << (UInt)sm->tList.size() << std::endl;
		std::cerr << "EXIT" << std::endl;
		exit(EXIT_FAILURE);
	}

	std::cerr << "Tracing streamline from triangle " << startTriangleIndex << " at position " << startPos << " with stepsize " << stepsize << std::endl;
	Triangle* startTri = sm->tList[startTriangleIndex];
	pl.insertVertex(startPos);
	float maxz = sm->max.z;
	for (UInt i1 = 0; i1 < 200; i1++) {
		bool stop = false;
		vec3f direction;
		startTri = getDirectionVectorOnMeshAt(sm, startPos, startTri, direction, stop);
		startPos = projPointOnMesh(sm, (startPos+vec3d(direction.x, direction.y, direction.z)*stepsize), startTri);
		pl.insertVertex(startPos);
		if (stop)
			break;
	}

	for (UInt i1 = 0; i1 < pl.getNumV()-1; i1++) {
		pl.insertLine(i1, i1+1);
	}

	return pl;

}

PolyLine<vec3d> traceVectorFieldOnSurfaceRK4(float stepsize, SimpleMesh* sm, vec3d startPos, UInt startTriangleIndex) {
	if (startTriangleIndex >= sm->tList.size()) {
		std::cerr << "ERROR in traceStreamLine(...):" << std::endl;
		std::cerr << "Triangle with index " << startTriangleIndex << " does not exits in mesh, max. index is " << (UInt)sm->tList.size() << std::endl;
		std::cerr << "EXIT" << std::endl;
		exit(EXIT_FAILURE);
	}

	Triangle* startTri = sm->tList[startTriangleIndex];
	traceVectorFieldOnSurfaceRK4(stepsize, sm, startPos, startTriangleIndex);
}

PolyLine<vec3d> traceVectorFieldOnSurfaceRK4(float stepsize, SimpleMesh* sm, vec3d startPos, Triangle* startTri) {

	PolyLine<vec3d> pl;

	std::cerr << "Tracing streamline from triangle " << startTri << " at position " << startPos << " with stepsize " << stepsize << std::endl;
	pl.insertVertex(startPos);
	float maxz = sm->max.z;
	for (UInt i1 = 0; i1 < 1500; i1++) {
		bool stop = false;
		vec3f k1, k2, k3, k4;
		getDirectionVectorOnMeshAt(sm, startPos, startTri, k1, stop);
		getDirectionVectorOnMeshAt(sm, startPos+vec3d(k1.x, k1.y, k1.z)*(stepsize/2), startTri, k2, stop);
		getDirectionVectorOnMeshAt(sm, startPos+vec3d(k2.x, k2.y, k2.z)*(stepsize/2), startTri, k3, stop);
		startTri = getDirectionVectorOnMeshAt(sm, startPos+vec3d(k3.x, k3.y, k3.z)*stepsize, startTri, k4, stop);

		vec3f rk_add = (k1 + k2*2 + k3*2 + k4)*(stepsize/6);

		startPos = projPointOnMesh(sm, (startPos+vec3d(rk_add.x, rk_add.y, rk_add.z)), startTri);
		pl.insertVertex(startPos);
		if (stop)
			break;
	}

	for (UInt i1 = 0; i1 < pl.getNumV()-1; i1++) {
		pl.insertLine(i1, i1+1);
	}

	return pl;

}

#endif
