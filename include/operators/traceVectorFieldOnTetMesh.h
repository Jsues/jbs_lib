#ifndef TRACE_VECTORFIELD_ON_TETMESH_H
#define TRACE_VECTORFIELD_ON_TETMESH_H


#include "StreamingIndexedTetraMesh.h"


#define EPSILON_FOR_BARY_INSIDE -1e-7


template <class T>
int findTetContainingPoint(StreamingIndexedTetraMesh<T>* tetMesh, const point4d<T>& pos, UInt StartTetID, bool& stop, UInt wearecommingfrom = 0) {

	point4d<T> barys = tetMesh->tList[StartTetID].barycentric_with_proj(pos);

	UInt edgeID;

	T currentsmallest = 0;


	if (barys.x < EPSILON_FOR_BARY_INSIDE) {
		if (currentsmallest > barys.x) {
			currentsmallest = barys.x;
			edgeID = tetMesh->tList[StartTetID].getEdge(0);
		}
	}
	if (barys.y < EPSILON_FOR_BARY_INSIDE) {
		if (currentsmallest > barys.y) {
			currentsmallest = barys.y;
			edgeID = tetMesh->tList[StartTetID].getEdge(1);
		}
	}
	if (barys.z < EPSILON_FOR_BARY_INSIDE) {
		if (currentsmallest > barys.z) {
			currentsmallest = barys.z;
			edgeID = tetMesh->tList[StartTetID].getEdge(2);
		}
	}
	if (barys.w < EPSILON_FOR_BARY_INSIDE) {
		if (currentsmallest > barys.w) {
			currentsmallest = barys.w;
			edgeID = tetMesh->tList[StartTetID].getEdge(3);
		}
	}

	if (currentsmallest == 0) {
		return StartTetID;
	}

	// Check the tetrahedron adjacent to the edge 'edgeID'
	if (tetMesh->eList[edgeID].getNumTets() < 2) {

		// Try to load the slice which contains the tet
		UInt nextSliceID;
		
		if ( tetMesh->edgeLiesOnSliceBoundary(edgeID, nextSliceID)) {

			UInt v0 = tetMesh->eList[edgeID].getVertex(0);
			UInt v1 = tetMesh->eList[edgeID].getVertex(1);
			UInt v2 = tetMesh->eList[edgeID].getVertex(2);

			if (nextSliceID == tetMesh->sliceInfo.size()-2) { // reached the end
				stop = true;
				return StartTetID;
			}

			tetMesh->removeAllCurrentTets();
			tetMesh->readTimeSlice(nextSliceID);

			// get new ID of the edge.
			int newEdgeID = tetMesh->vList[v0].getEdge(v0, v1, v2);
			assert(newEdgeID != -1);
			edgeID = newEdgeID;
			assert(tetMesh->eList[edgeID].getNumTets() == 1);

			UInt nextTet = tetMesh->eList[edgeID].getTet(0);
			if (nextTet == wearecommingfrom) {
				return StartTetID;
			}

			return findTetContainingPoint(tetMesh, pos, nextTet, stop, StartTetID);

		} else { // reached a 'real' boundary
			stop = true;
			return StartTetID;
		}
	} else {
		UInt nextTet;
		if (tetMesh->eList[edgeID].getTet(0) == StartTetID) {
			nextTet = tetMesh->eList[edgeID].getTet(1);
		} else {
			nextTet = tetMesh->eList[edgeID].getTet(0);
		}

		if (nextTet == wearecommingfrom) {
			return StartTetID;
		}

		return findTetContainingPoint(tetMesh, pos, nextTet, stop, StartTetID);

	}

	// We may never arrive here. return something to make the complier happy!
	return -1;
}

template <class T>
int getDirectionVectorOnMeshAt(StreamingIndexedTetraMesh<T>* tetMesh, const point4d<T>& pos, UInt StartTetID, point4d<T>& vec, bool& stop) {

	int tetID = findTetContainingPoint(tetMesh, pos, StartTetID, stop);

	point4d<T> barys = tetMesh->tList[tetID].barycentric(pos);

	vec = tetMesh->tList[tetID].getInterpolatedNormal(barys);
	return tetID;

}

template <class T>
PolyLine< point4d<T> > traceVectorFieldOnTetMeshEuler(T stepsize, StreamingIndexedTetraMesh<T>* tetMesh, point4d<T> pos, UInt StartTetID, UInt maxSteps = 100) {

	PolyLine< point4d<T> > pl;

	std::cerr << "Tracing streamline from tetrahedron " << StartTetID << " at position " << pos << " with stepsize " << stepsize << std::endl;

	pl.insertVertex(pos);

	for (UInt i1 = 0; i1 < maxSteps; i1++) {

		bool stop = false;
		point4d<T> direction;

		StartTetID = getDirectionVectorOnMeshAt(tetMesh, pos, StartTetID, direction, stop);

		pos = pos + direction*stepsize;
		pos = tetMesh->tList[StartTetID].projPointOnTet(pos);

		std::cerr << "pos: "; pos.print();

		pl.insertVertex(pos);
		if (stop)
			break;

	}

	for (UInt i1 = 0; i1 < pl.getNumV()-1; i1++) {
		pl.insertLine(i1, i1+1);
	}

	return pl;

}


template <class T>
PolyLine< point4d<T> > traceVectorFieldOnTetMeshRK4(T stepsize, StreamingIndexedTetraMesh<T>* tetMesh, point4d<T> pos, UInt StartTetID, UInt maxSteps = 100) {

	PolyLine< point4d<T> > pl;

	std::cerr << "Tracing streamline from tetrahedron " << StartTetID << " at position " << pos << " with stepsize " << stepsize << std::endl;

	pl.insertVertex(pos);

	for (UInt i1 = 0; i1 < maxSteps; i1++) {

		bool stop = false;
		point4d<T> direction;

		vec4f k1, k2, k3, k4;
		getDirectionVectorOnMeshAt(tetMesh, pos, StartTetID, k1, stop);
		k1.print();
		getDirectionVectorOnMeshAt(tetMesh, pos+k1*(stepsize/2), StartTetID, k2, stop);
		k2.print();
		getDirectionVectorOnMeshAt(tetMesh, pos+k2*(stepsize/2), StartTetID, k3, stop);
		k3.print();
		StartTetID = getDirectionVectorOnMeshAt(tetMesh, pos+k3*stepsize, StartTetID, k4, stop);
		k4.print();

		vec4f rk_add = (k1 + k2*2 + k3*2 + k4)*(stepsize/6);
		std::cerr << "----" << std::endl << k1 << " / " << rk_add << "----" << std::endl << std::endl;

		pos = pos + rk_add;
		pos = tetMesh->tList[StartTetID].projPointOnTet(pos);

		std::cerr << "pos: "; pos.print();

		pl.insertVertex(pos);
		if (stop) {
			std::cerr << "stop" << std::endl;
			break;
		}

	}

	for (UInt i1 = 0; i1 < pl.getNumV()-1; i1++) {
		pl.insertLine(i1, i1+1);
	}

	return pl;

}


#endif
