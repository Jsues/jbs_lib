#ifndef POLYLINE_4D_TIME_INTERSECT_H
#define POLYLINE_4D_TIME_INTERSECT_H

#include "../Polyline.h"
#include "../point4d.h"


namespace JBSlib {

	template <class T>
	point3d<T> getTimeIntersection(const PolyLine< point4d<T> >* pl, T time, bool& ok) {
		point3d<T> intersection;

		std::cerr << time << std::endl;

		point4d<T> minV, maxV;
		pl->ComputeMinAndMaxExt(minV, maxV);

		ok = false;

		if (time < minV.w || time > maxV.w) {
			std::cerr << "Out of bounds intersection in JBSlib::getTimeIntersection(...)" << std::endl;
			std::cerr << "You requested: " << time << " min is: " << minV.w << " max is: " << maxV.w << std::endl;

			T minT = std::numeric_limits<T>::max();
			T maxT = -std::numeric_limits<T>::max();
			if (time < minV.w) {
				// Find point with min w-Coordinate:
				for (UInt i1 = 0; i1 < pl->points.size(); i1++) {
					if (pl->points[i1].c.w < minT) {
						minT = pl->points[i1].c.w;
						intersection = point3d<T>(pl->points[i1].c.x, pl->points[i1].c.y, pl->points[i1].c.z);
					}
				}
			}
			if (time > minV.w) {
				// Find point with min w-Coordinate:
				for (UInt i1 = 0; i1 < pl->points.size(); i1++) {
					if (pl->points[i1].c.w > maxT) {
						maxT = pl->points[i1].c.w;
						intersection = point3d<T>(pl->points[i1].c.x, pl->points[i1].c.y, pl->points[i1].c.z);
					}
				}
			}
			return intersection;
		}

		for (std::list<LineSegment>::const_iterator it = pl->lines.begin(); it != pl->lines.end(); ++it) {
			UInt v0 = it->v0;
			UInt v1 = it->v1;

			point4d<T> p0 = pl->points[v0].c;
			point4d<T> p1 = pl->points[v1].c;
			//p0.print();
			//p1.print();

			if (p0.w <= time && p1.w >= time) {
				ok = true;
				T w_span = p1.w-p0.w;
				T w_dot = (time-p0.w)/w_span;
				for (UInt i2 = 0; i2 < 3; i2++) {
					intersection[i2] = p0[i2]+(p1-p0)[i2];
				}
				return intersection;
			}

		}
		std::cerr << "ERROR in JBSlib::getTimeIntersection(...)" << std::endl;
		std::cerr << "You requested: " << time << " no intersection found at this time!" << std::endl;

		return intersection;
	}

};

#endif
