#ifndef UMBRELLA_MINIMIZE_SELECTION_TNT_H
#define UMBRELLA_MINIMIZE_SELECTION_TNT_H



#include "SimpleMesh.h"

void SmoothMeshTNT(SimpleMesh* sm);
void SmoothMeshTNT_Ver2(SimpleMesh* sm);

#endif

