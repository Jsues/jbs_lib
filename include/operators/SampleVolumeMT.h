#ifndef SAMPLE_VOLUME_MT_H
#define SAMPLE_VOLUME_MT_H

#ifndef NO_BOOST
#include <boost/thread/thread.hpp>
#endif


#include "ScalarFunction.h"
#include "Volume.h"

#ifndef UINT_PAIR
#define UINT_PAIR
struct UIntpair {
	UIntpair() : i1(0), i2(0) {}
	UIntpair(UInt i1_, UInt i2_) : i1(i1_), i2(i2_) {}
	UInt i1, i2;
};
#endif

template <class ValueType>
class MC_thread {
public:
	MC_thread(UIntpair startEnd, Volume<ValueType>* vol, ScalarFunction< point3d<ValueType> >* func) : m_StartEnd(startEnd), m_vol(vol), m_func(func) {}

	void operator()() {
		ValueType d;
		for (UInt i = m_StartEnd.i1; ((i < m_StartEnd.i2) && (i < m_vol->getDimX())); ++i) {
			//std::cerr << i << " / " << brickX << "            \r";
			for (UInt j = 0; j < m_vol->getDimY(); ++j) {
				for (UInt k = 0; k < m_vol->getDimZ(); ++k) {
					vec3d pos = m_vol->pos(i,j,k);
					d = m_func->eval(pos);
					m_vol->set(i,j,k, (ValueType)d);
				}
			}
		}
	}

private:
	ScalarFunction< point3d<ValueType> >* m_func;
	Volume<ValueType>* m_vol;
	UIntpair m_StartEnd;
};


template <class ValueType>
Volume<ValueType>* sampleFunctionToVolumeSingleThreaded(point3d<ValueType> minV, point3d<ValueType> maxV, UInt stepsMax, ScalarFunction< point3d<ValueType> >* func) {

	ValueType brick_size = std::max(maxV[0] - minV[0], std::max(maxV[1] - minV[1], maxV[2] - minV[2])) / (ValueType)stepsMax;
	//std::cerr << "brick_size: " << brick_size << std::endl;
	const int brickX = 2 + (unsigned int)((maxV[0] - minV[0]) / brick_size);
	const int brickY = 2 + (unsigned int)((maxV[1] - minV[1]) / brick_size);
	const int brickZ = 2 + (unsigned int)((maxV[2] - minV[2]) / brick_size);
	Volume<ValueType>* vol = new Volume<ValueType>;
	vol->initWithDims(minV, maxV, brickX, brickY, brickZ);

	//ValueType d;
	//for (int i = 0; i < brickX; ++i) {
	//	//std::cerr << i << " / " << brickX << "            \r";
	//	for (int j = 0; j < brickY; ++j) {
	//		for (int k = 0; k < brickZ; ++k) {
	//			vec3d pos = vol->pos(i,j,k);
	//			d = func->eval(pos);
	//			vol->set(i,j,k, (float)d);
	//		}
	//	}
	//}

	UIntpair range;
	range.i1 = 0;
	range.i2 = brickX;
	MC_thread<ValueType> x(range, vol, func);
	x.operator()();

	return vol;
}

#ifndef NO_BOOST
template <class ValueType>
Volume<ValueType>* sampleFunctionToVolumeMultiThreaded(point3d<ValueType> minV, point3d<ValueType> maxV, UInt stepsMax, ScalarFunction< point3d<ValueType> >* func, UInt numThreads = 8) {

	ValueType brick_size = std::max(maxV[0] - minV[0], std::max(maxV[1] - minV[1], maxV[2] - minV[2])) / (ValueType)stepsMax;
	//std::cerr << "brick_size: " << brick_size << std::endl;
	const int brickX = (unsigned int)((maxV[0] - minV[0]) / brick_size);
	const int brickY = (unsigned int)((maxV[1] - minV[1]) / brick_size);
	const int brickZ = (unsigned int)((maxV[2] - minV[2]) / brick_size);
	Volume<ValueType>* vol = new Volume<ValueType>;
	vol->initWithDims(minV, maxV, brickX, brickY, brickZ);

	//std::cerr << "brickX: " << brickX << "  brickY: " << brickY << "  brickZ: " << brickZ << std::endl;

	UIntpair* ranges = new UIntpair[numThreads];
	ranges[0].i1 = 0;
	for (UInt i1 = 1; i1 < numThreads; i1++) {
		double fac = (double)i1/(double)numThreads;
		ranges[i1-1].i2 = (UInt)((double)brickX*fac);
		ranges[i1].i1 = (UInt)((double)brickX*fac);
	}
	ranges[numThreads-1].i2 = (UInt)brickX;

	//for (UInt i1 = 0; i1 < numThreads; i1++) {
	//	std::cerr << "thread " << i1 << ": " << ranges[i1].i1 << " to " << ranges[i1].i2 << std::endl;
	//}

	// Launch threads:
	boost::thread** threads = new boost::thread*[numThreads];
	for (UInt i1 = 0; i1 < numThreads; i1++) {
		MC_thread<ValueType> MC_thread_data_i(ranges[i1], vol, func);
		threads[i1] = new boost::thread(MC_thread_data_i);
	}

	// Collect threads
	for (UInt i1 = 0; i1 < numThreads; i1++) {
		threads[i1]->join();
	}
	// Delete threads
	for (UInt i1 = 0; i1 < numThreads; i1++) {
		delete threads[i1];
	}

	delete[] threads;
	delete[] ranges;

	return vol;
}
#else
template <class ValueType>
Volume<ValueType>* sampleFunctionToVolumeMultiThreaded(point3d<ValueType> minV, point3d<ValueType> maxV, UInt stepsMax, ScalarFunction< point3d<ValueType> >* func, UInt numThreads = 8) {
	return sampleFunctionToVolumeSingleThreaded(minV, maxV, stepsMax, func);
}

#endif

#endif
