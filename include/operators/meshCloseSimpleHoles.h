#ifndef MESH_CLOSE_SIMPLE_HOLES
#define MESH_CLOSE_SIMPLE_HOLES

#include "../SimpleMesh.h"
#include "operators/FillHoleBarequet.h"
#include <vector>

namespace JBSlib {

	//! Closes holes in the mesh where posible
	void closeSimpleHoles(SimpleMesh* sm) {

		int fillHolesUpToSize = 15;

		bool tmp;
		for (UInt i1 = 0; i1 < sm->eList.size(); i1++) {
			Edge* e = sm->eList[i1];

			if ((e->tList.size() == 1) && (!e->marker)) {
				std::vector<int> hole = sm->getBoundaryStartAtEdge(e, tmp);
				if (hole.size() < fillHolesUpToSize && hole.size() > 2) {
					FillHoleBarequet(sm, hole);
				} else {
				}
			}
		}

	}
}


#endif

