#ifndef MESH_FAIRING_H
#define MESH_FAIRING_H

#include "SimpleMesh.h"


namespace JBSlib {

//******************************************************************************************

//! Computes the cotangens of x.
double JBS_cot(double x) {
	return cos(x)/sin(x);
}

// ****************************************************************************************
// ****************************************************************************************

	vec3d UmbrellaSmoth(SimpleMesh* sm, int v, double lambda) {

		// Compute centroid of fan:
		vec3d centroid(0,0,0);
		for (UInt i2 = 0; i2 < sm->vList[v].eList.size(); i2++) {
			int neighbor;
			(sm->vList[v].eList[i2]->v0 == v) ? neighbor = sm->vList[v].eList[i2]->v1 : neighbor = sm->vList[v].eList[i2]->v0;
			centroid += sm->vList[neighbor].c;
		}
		if (sm->vList[v].eList.size() > 0) {
			centroid /= float(sm->vList[v].eList.size());
		} else {
#ifdef DEBUG
			std::cerr << "ReduceMesh::UmbrellaSmoth: Ooops, none-connected vertex found! index=" << v << std::endl;
#endif
			centroid = sm->vList[v].c;
		}

		vec3d d = (centroid - sm->vList[v].c)*lambda;

		return (sm->vList[v].c + d);
	}

// ****************************************************************************************

	vec3d CurvatureSmooth(SimpleMesh* sm, int v, double lambda) {

		// Compute curvature normal;
		vec3d curvature_normal(0,0,0);

		double area = 0.0;
		double  sum = 0.0;
		for (UInt i2 = 0; i2 < sm->vList[v].eList.size(); i2++) {
			Edge* e = sm->vList[v].eList[i2];
			int other_v = (e->v0 == v) ? e->v1 : e->v0;

			
			if (e->tList.size() != 2) {
				return sm->vList[v].c;
				//std::cerr << "ReduceMesh::CurvatureSmoth(...): Edge with " << (int) e->tList.size() << " Triangles found!" << std::endl;
				//exit(EXIT_FAILURE);
			}

			//area += e->tList[0]->getArea()*0.5; // Each triangle will be processed twice!
			//area += e->tList[1]->getArea()*0.5; // Each triangle will be processed twice!

			//double angle0 = e->tList[0]->getAngleAtVertex(e->tList[0]->getOther(e->v0, e->v1));
			//double angle1 = e->tList[1]->getAngleAtVertex(e->tList[1]->getOther(e->v0, e->v1));
			//double factor = JBS_cot(angle0)+JBS_cot(angle1);

			e->computeCotangentWeights();
			double factor = e->cotangent_weight;

			curvature_normal += (sm->vList[other_v].c-sm->vList[v].c)*factor;
			sum += factor;
		}


		//curvature_normal /= (4*area);
		curvature_normal /= sum;

		return (sm->vList[v].c + curvature_normal);

	}

// ****************************************************************************************

	void UmbrellaSmooth(SimpleMesh* sm, double lambda = 1) {

		UInt num_pts = (UInt)sm->vList.size();
		vec3d* new_pos = new vec3d[num_pts];

		for (UInt i1 = 0; i1 < num_pts; i1++) {
			vec3d v_new = CurvatureSmooth(sm, i1, lambda);
			new_pos[i1] = v_new;
		}

		for (UInt i1 = 0; i1 < num_pts; i1++) {
			sm->vList[i1].c = new_pos[i1];
		}

		delete[] new_pos;
	}

// ****************************************************************************************

	void CurvatureSmooth(SimpleMesh* sm, double lambda = 1) {

		UInt num_pts = (UInt)sm->vList.size();
		vec3d* new_pos = new vec3d[num_pts];

		for (UInt i1 = 0; i1 < num_pts; i1++) {
			vec3d v_new = CurvatureSmooth(sm, i1, lambda);
			new_pos[i1] = v_new;
		}

		for (UInt i1 = 0; i1 < num_pts; i1++) {
			sm->vList[i1].c = new_pos[i1];
		}

		delete[] new_pos;
	}

// ****************************************************************************************

	vec3d InverseUmbrellaSmooth(SimpleMesh* sm, int v) {

		vec3d ci;

		for (UInt i2 = 0; i2 < sm->vList[v].getNumN(); i2++) {
			int neighbor = sm->vList[v].eList[i2]->getOther(v);
			ci += sm->vList[neighbor].c;
		}
		if (sm->vList[v].getNumN() > 0)
			ci /= sm->vList[v].getNumN();

		vec3d ret = ci;

		for (UInt i2 = 0; i2 < sm->vList[v].getNumN(); i2++) {

			int neighbor = sm->vList[v].eList[i2]->getOther(v);


			const vec3d& pj = sm->vList[neighbor].c;
			vec3d cj;
			for (UInt i3 = 0; i3 < sm->vList[neighbor].getNumN(); i3++) {
				int neighbor2 = sm->vList[neighbor].eList[i3]->getOther(neighbor);
				cj += sm->vList[neighbor2].c;
			}
			cj /= (double)sm->vList[neighbor].eList.size();

			ret -= (cj-pj)/(sm->vList[v].getNumN());
		}

		return ci;

	}

// ****************************************************************************************

	void TaubinSmooth(SimpleMesh* sm) {
		double kpb = 0.01;
		double lambda = 0.6;
		double mue = 1.0f/(kpb-(1.0f/lambda));

		UmbrellaSmooth(sm, lambda);
		UmbrellaSmooth(sm, mue);
	}

// ****************************************************************************************

	void InverseUmbrellaSmooth(SimpleMesh* sm) {

		UInt num_pts = (UInt)sm->vList.size();
		vec3d* new_pos = new vec3d[num_pts];

		for (UInt i1 = 0; i1 < num_pts; i1++) {
			vec3d v_new = InverseUmbrellaSmooth(sm, i1);
			new_pos[i1] = v_new;
		}

		for (UInt i1 = 0; i1 < num_pts; i1++) {
			sm->vList[i1].c = new_pos[i1];
		}

		delete[] new_pos;

	}

// ****************************************************************************************

	void RecoverConstraints(SimpleMesh* sm, const std::vector<vec3d>& initialPos, double d) {

		double d_sq = d*d;

		for (UInt i1 = 0; i1 < (UInt)sm->getNumV(); i1++) {
			const vec3d& o = initialPos[i1];
			const vec3d& v = sm->vList[i1].c;
			if (o.squaredDist(v) > d_sq) {
				vec3d diff = v-o;
				diff.normalize();
				diff *= d;
				sm->vList[i1].c = o + diff;
			}
		}
	}
};

#endif

