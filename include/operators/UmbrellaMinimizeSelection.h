#ifndef UMBRELLA_MINIMIZE_SELECTION_H
#define UMBRELLA_MINIMIZE_SELECTION_H



#include "SimpleMesh.h"

// Choose Parametrization method.
enum FAIRING_METHOD {
	UMBRELLA = 0,
	INVERSE_UMBRELLA = 1
};

void SmoothMesh(SimpleMesh* sm, FAIRING_METHOD method);

#endif

