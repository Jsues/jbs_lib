#ifndef COMPUTE_DISTANCE_POINTCLOUD_MESH_H
#define COMPUTE_DISTANCE_POINTCLOUD_MESH_H

#include "operators/findClosestPointOnSurface.h"
#include "SimpleMesh.h"
#include "PointCloud.h"

struct distPCMesh {
	double avg;
	double var;
};


template <class Point>
distPCMesh computeDistancePointCloudMesh(PointCloud<Point>* pc, SimpleMesh* sm) {

	FindClosestPointsOnMesh cpom(sm);
	Point* pts = pc->getPoints();

	UInt numPts = (UInt)pc->getNumPts();

	double* dists = new double[numPts];
	double avg = 0;
	for (UInt i1 = 0; i1 < numPts; i1++) {
		vec3d pt(pts[i1].x, pts[i1].y, pts[i1].z);
		vec3d corr = cpom.getClosestPoint(pt);
		double dist = corr.dist(pt);
		avg += dist;
		dists[i1] = dist;
	}
	avg /= numPts;

	// compute variance
	double var = 0;
	for (UInt i1 = 0; i1 < numPts; i1++) {
		double diff = dists[i1]-avg;
		var += diff*diff;
	}
	var /= numPts;

	distPCMesh ret;
	ret.avg = avg;
	ret.var = var;

	delete[] dists;

	return ret;
}

#endif
