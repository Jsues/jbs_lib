#ifndef MAKE_FILENAME_H
#define MAKE_FILENAME_H


#include <sstream>
#include <string>
#include <iomanip>


std::string makeFilename(const char* prefix, int num, const char* type) {
	std::stringstream ss;
	ss << prefix << std::setw(4) << std::setfill('0') << num << type;
	return ss.str();
}


#endif
