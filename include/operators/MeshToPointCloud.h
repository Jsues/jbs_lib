#ifndef MESH_TO_POINTCLOUD
#define MESH_TO_POINTCLOUD


#include "../JBS_General.h"
#include "../PointCloud.h"
#include "../SimpleMesh.h"
#include "../rng.h"


//! Converts a SimpleMesh to a PointCloud with an arbitrary number of points
PointCloudNormals<vec3d>* ConvertMeshToPointCloudDoublewithNoise(SimpleMesh* sm, int numOfPoints, double noise = 0) {

	// Compute Area of mesh:
	int numT = sm->getNumT();
	double totalArea = 0.0;
	for (int i1 = 0; i1 < numT; i1++)
		totalArea += sm->tList[i1]->getArea();

	std::cerr << "Area: " << totalArea;
	PointCloudNormals<vec3d>* pc = new PointCloudNormals<vec3d>;

	RNG generator;

	double diag = sm->max.dist(sm->min);

	for (int i1 = 0; i1 < numT; i1++) {
		double triangleArea = sm->tList[i1]->getArea();
		double percentage = triangleArea / totalArea;
		double points_to_add = numOfPoints*percentage;
		int i_points_to_add = (int)points_to_add;
		double remainer = points_to_add-i_points_to_add;
		
		double rand = generator.uniform();
		if (rand < remainer)
			i_points_to_add++;

		for (int i2 = 0; i2 < i_points_to_add; i2++) {
			double u = generator.uniform();
			double v = generator.uniform();			
			if (u+v > 1) {
				u = 1.0-u;
				v = 1.0-v;
			}
			double w = 1.0-u-v;

			vec3d pt = sm->vList[sm->tList[i1]->v0()].c*u + sm->vList[sm->tList[i1]->v1()].c*v + sm->vList[sm->tList[i1]->v2()].c*w;
			vec3d normal = sm->tList[i1]->getNormal();

			vec3d pt_noise(generator.normal(0, noise), generator.normal(0, noise), generator.normal(0, noise));
			pt_noise *= diag;
			vec3d normal_noise(generator.normal(0, 3*noise), generator.normal(0, 3*noise), generator.normal(0, 3*noise));
			pt += pt_noise;
			normal += normal_noise;
			normal.normalize();

			pc->insertPoint(pt, normal);
	
		}
	}


    return pc;
};


//! Converts a SimpleMesh to a PointCloud with an arbitrary number of points
PointCloudNormals<vec3f>* ConvertMeshToPointCloudwithNoise(SimpleMesh* sm, int numOfPoints, double noise = 0) {

	// Compute Area of mesh:
	int numT = sm->getNumT();
	double totalArea = 0.0;
	for (int i1 = 0; i1 < numT; i1++)
		totalArea += sm->tList[i1]->getArea();

	std::cerr << "Area: " << totalArea;
	PointCloudNormals<vec3f>* pc = new PointCloudNormals<vec3f>;

	RNG generator;

	double diag = sm->max.dist(sm->min);

	for (int i1 = 0; i1 < numT; i1++) {
		double triangleArea = sm->tList[i1]->getArea();
		double percentage = triangleArea / totalArea;
		double points_to_add = numOfPoints*percentage;
		int i_points_to_add = (int)points_to_add;
		double remainer = points_to_add-i_points_to_add;
		
		double rand = generator.uniform();
		if (rand < remainer)
			i_points_to_add++;

		for (int i2 = 0; i2 < i_points_to_add; i2++) {
			double u = generator.uniform();
			double v = generator.uniform();			
			if (u+v > 1) {
				u = 1.0-u;
				v = 1.0-v;
			}
			double w = 1.0-u-v;

			vec3d pt = sm->vList[sm->tList[i1]->v0()].c*u + sm->vList[sm->tList[i1]->v1()].c*v + sm->vList[sm->tList[i1]->v2()].c*w;
			vec3d normal = sm->tList[i1]->getNormal();

			vec3d pt_noise(generator.normal(0, noise), generator.normal(0, noise), generator.normal(0, noise));
			pt_noise *= diag;
			vec3d normal_noise(generator.normal(0, 3*noise), generator.normal(0, 3*noise), generator.normal(0, 3*noise));
			pt += pt_noise;
			normal += normal_noise;
			normal.normalize();

			vec3f pt_f((float)pt.x, (float)pt.y, (float)pt.z);
			vec3f normal_f((float)normal.x, (float)normal.y, (float)normal.z);

			pc->insertPoint(pt_f, normal_f);
	
		}
	}


    return pc;
};


//! Converts a SimpleMesh to a PointCloud with an arbitrary number of points
PointCloud<vec3d> ConvertMeshToPointCloud(SimpleMesh* sm, int numOfPoints, PointCloud<vec3d>* normals = NULL) {

	// Compute Area of mesh:
	int numT = sm->getNumT();
	double totalArea = 0.0;
	for (int i1 = 0; i1 < numT; i1++)
		totalArea += sm->tList[i1]->getArea();

	std::cerr << "Area: " << totalArea;
	PointCloud<vec3d> pc;

	RNG generator;

	for (int i1 = 0; i1 < numT; i1++) {
		double triangleArea = sm->tList[i1]->getArea();
		double percentage = triangleArea / totalArea;
		double points_to_add = numOfPoints*percentage;
		int i_points_to_add = (int)points_to_add;
		double remainer = points_to_add-i_points_to_add;
		

		double rand = generator.uniform();
		if (rand < remainer)
			i_points_to_add++;

		for (int i2 = 0; i2 < i_points_to_add; i2++) {
			double u = generator.uniform();
			double v = generator.uniform();			
			if (u+v > 1) {
				u = 1.0-u;
				v = 1.0-v;
			}
			double w = 1.0-u-v;

			vec3d pt = sm->vList[sm->tList[i1]->v0()].c*u + sm->vList[sm->tList[i1]->v1()].c*v + sm->vList[sm->tList[i1]->v2()].c*w;
			pc.insertPoint(pt);

			if (normals != NULL) {
				vec3d normal = sm->tList[i1]->getNormal();
				normals->insertPoint(normal);
			}
		}
	}


    return pc;
};


#endif

