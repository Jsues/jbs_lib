#ifndef MESH_MAGIC_WAND_H
#define MESH_MAGIC_WAND_H

#include "../SimpleMesh.h"
#include "../BaseMesh.h"
#include <vector>

namespace JBSlib {

	//! Selects an connected area of the mesh starting at the vertex 'seedPoint'. Does only select such triangles that are connected with am dihedral angle (n1|n2) > angle with the current triangle.
	void magicWand(SimpleMesh* sm, int seedPoint, float angle = -10) {
		BaseMesh* selection = new BaseMesh();


		std::vector<Triangle*> unprocessedTris;

		int counter = 0;


		// mark initial fan (the fan around Vertex 'seedPoint':
		for (unsigned int i1 = 0; i1 < sm->vList[seedPoint].eList.size(); i1++) {
			for (unsigned int i2 = 0; i2 < sm->vList[seedPoint].eList[i1]->tList.size(); i2++) {
				sm->vList[seedPoint].eList[i1]->tList[i2]->marker = true;
				unprocessedTris.push_back(sm->vList[seedPoint].eList[i1]->tList[i2]);
			}
		}

		while (unprocessedTris.size() > 0) {
			Triangle* current = unprocessedTris[unprocessedTris.size()-1];				
			unprocessedTris.pop_back();

			counter++;
			if (counter%1000 == 0)
				std::cerr << counter << "            \r";

			// Insert all neighboring triangles that have not yet been marked into unprocessedTris:
			for (unsigned int i0 = 0; i0 < 3; i0++) {
				Edge* e = current->e0;
				if (i0 == 1)
					e = current->e1;
				if (i0 == 2)
					e = current->e2;

				for (unsigned int i1 = 0; i1 < e->tList.size(); i1++) {
					if (!e->tList[i1]->marker) {
						// Check if the angle is to large:
						if (fabs(current->getNormal() | e->tList[i1]->getNormal()) > angle) {
							e->tList[i1]->marker = true;
							unprocessedTris.push_back(e->tList[i1]);
						}
					}
				}
			}
		}



	}

};

#endif

