#ifndef MESHDECIMATION_H
#define MESHDECIMATION_H


#include <vector>
#include <map>
#include <algorithm>
#include "SimpleMesh.h"
#include "operators/computeVertexCurvature.h"
#include "operators/computeVertexNormals.h"
#include "operators/FillHoleBarequet.h"
#include "operators/testColinear.h"
#include "operators/getVertexFan.h"
#include "PointCloud.h"
#include "iv_stuff/ImportExportCamera.h"
#include "MultiResMesh.h"




SimpleMesh* computeCoarserLevel(SimpleMesh* sm, PointCloud<vec3d>* pc, double lambda = 0.5) {

	std::cerr << "computing set of removable vertices...";

	UInt numV = (UInt)sm->vList.size();
	vec3d* normals = computeVertexNormals(sm);

	double* areas = new double[numV];
	double* curvatures = new double[numV];
	double max_area = 0;
	double max_curvature = 0;

	for (UInt i1 = 0; i1 < numV; i1++) {
		double area = getAreaAtFan(sm, i1);
		double curvature = getCurvatureAtFan(sm, normals[i1], i1);

		sm->vList[i1].bool_flag = false;

		areas[i1] = area;
		curvatures[i1] = curvature;
		if (area > max_area) max_area = area;
		if (curvature > max_curvature) max_curvature = curvature;
	}

	std::vector<p_v_i> pri_queue;
	pri_queue.reserve(numV);


	for (UInt i1 = 0; i1 < numV; i1++) {
		double weight = (lambda)*areas[i1]/max_area + (1-lambda)*curvatures[i1]/max_curvature;
		// Multiply weight by Vertex valence to remove low valence vertices first.
		weight *= sm->vList[i1].eList.size();
		sm->vList[i1].color.x = (float)weight;
		pri_queue.push_back(p_v_i(weight, i1));
	}
	std::sort(pri_queue.begin(), pri_queue.end());

	delete[] areas;
	delete[] curvatures;
	delete[] normals;

	std::vector<UInt> markForDelete;
	for (UInt i1 = 0; i1 < numV; i1++) {
		UInt id = pri_queue[i1].index;
		if (sm->vList[id].getNumN() > 12) // do not delete
			continue;
		if (sm->vList[id].bool_flag == false) { // mark for delete
			markForDelete.push_back(id);
			// mark 1-ring as not-deleteable
			for (UInt i2 = 0; i2 < sm->vList[id].eList.size(); i2++) {
				int neighbor = sm->vList[id].eList[i2]->getOther(id);
				sm->vList[neighbor].bool_flag = true;
			}
		}
	}

	std::cerr << "done" << std::endl;

	JBS_Timer t;

	// Set triangle marker to false:
	for (UInt i1 = 0; i1 < sm->tList.size(); i1++) {
		sm->tList[i1]->marker = false;
	}

	t.log("Collapse possible edges");
	std::vector<edgeCollapsInfo> eColInfo;
	// Remove points by collaps
	int decrement = 0;
	for (UInt i1 = 0; i1 < markForDelete.size(); i1++) {
		UInt v0 = markForDelete[i1];
		if (sm->vList[v0].eList.size() < 2) {
			std::cerr << "isolated point!" << std::endl;
		} else {
			//pc->insertPoint(sm->vList[v0].c);
			edgeCollapsInfo eci;
			sm->edgeCollapse(v0,&eci);
			//eColInfo.push_back(eci);
			decrement++;
		}
	}

	int numVbefore = sm->getNumV();
	sm->cleanUpMarkedTriangles();
	sm->cleanValence0Vertices(&eColInfo);

	t.log();
	t.print();

	//for (UInt i1 = 0; i1 < eColInfo.size(); i1++) {
	//	eColInfo[i1].undoCollaps(sm);
	//}

	std::cerr << "Vertices before: " << numVbefore << " deleted: " << markForDelete.size() << " after: " << sm->getNumV() << std::endl;



	return sm;


	//********************************************************
	// OLD STUFF
	//********************************************************


	SimpleMesh* sm_coarse = new SimpleMesh;
	int* newIndices = new int[numV];
	for (UInt i1 = 0; i1 < numV; i1++) {
		newIndices[i1] = -1;
	}

	for (UInt i1 = 0; i1 < numV; i1++) {
		if (sm->vList[i1].bool_flag == true) { // survivor
			newIndices[i1] = sm_coarse->getNumV();
			sm_coarse->insertVertex(sm->vList[i1].c);
		}
	}

	// retriangulate
	for (UInt i1 = 0; i1 < markForDelete.size(); i1++) {

		UInt id = markForDelete[i1];

		pc->insertPoint(sm->vList[id].c);

		std::vector<int> fan;
		bool isBoundaryPoint = getVertexFan(sm, id, fan);

		std::vector<int> boundary;
		boundary.reserve(fan.size());
		for (UInt i2 = 0; i2 < fan.size(); i2++) {
			boundary.push_back(newIndices[fan[i2]]);
		}

		if (!isBoundaryPoint) {
			FillHoleBarequet(sm_coarse, boundary);
		} else { // Is a boundary point!
			if (boundary.size() <= 3) { // Point is the tip of a single boundary triangle
				// do nothing!
			} else if (boundary.size() == 4) {
				// Check if triangle is a sqrt3-configuration
				bool sqrt3config = false;
				Edge* e = sm->vList[boundary[0]].eList.getEdge(boundary[0], boundary[1]);
				if (e != NULL) {
					Triangle t(boundary[0], boundary[1], boundary[2], 0,0,0,0);
					for (UInt i1 = 0; i1 < e->tList.size(); i1++) {
						if (e->tList[i1]->compare(t) == 3)
							sqrt3config = true;
					}
				}
				//! Don't insert colinear = area 0 boundary triangles
				if (areColinear(sm_coarse->vList[boundary[0]].c, sm_coarse->vList[boundary[1]].c, sm_coarse->vList[boundary[2]].c, 0.1)) {
					std::cerr << "Colinear Boundary triangle!" << std::endl;
				}
				if (sqrt3config) {
					std::cerr << "Sqrt3 configuration found!" << std::endl;
				} else {
					FillHoleBarequet(sm_coarse, boundary);
				}
			} else {
				FillHoleBarequet(sm_coarse, boundary);
			}
		}
	}


	// Now add all triangles which have not been affected by the decimation
	for (UInt i1 = 0; i1 < sm->tList.size(); i1++) {
		Triangle* t = sm->tList[i1];
		if ( (sm->vList[t->v0()].bool_flag) &&
			 (sm->vList[t->v1()].bool_flag) &&
			 (sm->vList[t->v2()].bool_flag)    ) {
			sm_coarse->insertTriangle(newIndices[t->v0()], newIndices[t->v1()], newIndices[t->v2()]);
		}
	}

	// Barequet may have produced duplicate triangles. find them and remove them:
	std::set<Edge*> val4edges;
	// Remove all duplicate triangles:
	for (UInt i1 = 0; i1 < sm_coarse->eList.size(); i1++) {
		Edge* e = sm_coarse->eList[i1];
		if (e->tList.size() == 4) {
			val4edges.insert(e);
		}
	}
	for (std::set<Edge*>::iterator it = val4edges.begin(); it != val4edges.end(); it++) {
		Edge* e = (*it);
		//std::cerr << "Edge: " << e->v0 << "-" << e->v1 << std::endl;
		std::map<int, int> v_cnt;
		for (UInt i1 = 0; i1 < e->tList.size(); i1++) {
			Triangle* t = e->tList[i1];
			int other = t->getOther(e->v0, e->v1);
			std::map<int, int>::iterator v_cnt_it = v_cnt.find(other);
			if (v_cnt_it == v_cnt.end()) {
				v_cnt.insert(std::make_pair(other, 1));
			} else {
				(*v_cnt_it).second ++;
			}
		}
		for (std::map<int, int>::iterator v_cnt_it = v_cnt.begin(); v_cnt_it != v_cnt.end(); v_cnt_it++) {
			if ((*v_cnt_it).second == 2) {
				int v_id = (*v_cnt_it).first;
				std::cerr << "Delete vertex " << v_id << std::endl;
				sm_coarse->removeVertex(v_id);
			}
		}
	}

	//for (UInt i1 = 0; i1 < sm_coarse->eList.size(); i1++) {
	//	Edge* e = sm_coarse->eList[i1];
	//	int v0 = e->v0;
	//	int v1 = e->v1;
	//	if (e->tList.size() > 2) std::cerr << "eList.size() is" << e->tList.size() << std::endl;
	//	if (e->tList.size() >= 2) {
	//		if ( e->tList[0]->getOther(v0, v1) == e->tList[1]->getOther(v0, v1) ) {
	//			sm->removeTriangle(e->tList[0]);
	//			std::cerr << "Caught duplicate triangle!" << std::endl;
	//			std::cerr << "eList.size() " << e->tList.size() << std::endl;
	//		}
	//	}
	//}


	std::cerr << "Vertices before: " << sm->getNumV() << " deleted: " << markForDelete.size() << " after: " << sm_coarse->getNumV() << std::endl;

	delete[] areas;
	delete[] curvatures;
	delete[] normals;
	delete[] newIndices;

	return sm_coarse;

}

#endif
