#ifndef COMPUTE_AVERAGE_EDGE_LENGHT_H
#define COMPUTE_AVERAGE_EDGE_LENGHT_H

#include "SimpleMesh.h"

double computeAverageEdgeLength(SimpleMesh* sm) {
	double l = 0;

	UInt numE = (UInt)sm->eList.size();

	for (UInt i1 = 0; i1 < numE; i1++) {
		Edge* e = sm->eList[i1];
		l += e->getLength();
	}

	l /= numE;

	return l;
}

#endif
