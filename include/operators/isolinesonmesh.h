#ifndef ISO_LINES_ON_MESH_H
#define ISO_LINES_ON_MESH_H

#include "linInterpolate.h"
#include "PolyLine.h"

PolyLine<vec3d>* getIsoLines(SimpleMesh* sm, double iso) {

	PolyLine<vec3d>* pl = new PolyLine<vec3d>;

	for (int i1 = 0; i1 < sm->getNumT(); i1++) {
		Triangle* t = sm->tList[i1];
		int v0 = t->v0();
		int v1 = t->v1();
		int v2 = t->v2();

		int b = 0;

		if ((sm->vList[v0].param.x > iso && sm->vList[v1].param.x < iso) || (sm->vList[v0].param.x < iso && sm->vList[v1].param.x > iso)) {
			pl->insertVertex(LinInterpolate(sm->vList[v0].c, sm->vList[v1].c, sm->vList[v0].param.x, sm->vList[v1].param.x, iso));
			b++;
		}
		if ((sm->vList[v0].param.x > iso && sm->vList[v2].param.x < iso) || (sm->vList[v0].param.x < iso && sm->vList[v2].param.x > iso)) {
			pl->insertVertex(LinInterpolate(sm->vList[v0].c, sm->vList[v2].c, sm->vList[v0].param.x, sm->vList[v2].param.x, iso));
			b++;
		}
		if ((sm->vList[v2].param.x > iso && sm->vList[v1].param.x < iso) || (sm->vList[v2].param.x < iso && sm->vList[v1].param.x > iso)) {
			pl->insertVertex(LinInterpolate(sm->vList[v2].c, sm->vList[v1].c, sm->vList[v2].param.x, sm->vList[v1].param.x, iso));
			b++;
		}

		if (b > 0) {
			pl->insertLine(pl->getNumV()-2, pl->getNumV()-1);
		}

	}


	return pl;

}

#endif

