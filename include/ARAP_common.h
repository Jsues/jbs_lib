#ifndef ARAP_COMMON_H
#define ARAP_COMMON_H

#include "SimpleMesh.h"
#include "MatrixnD.h"

//******************************************************************************************

class IntegerDoublePair {
public:
	inline IntegerDoublePair(int id_, double val_) : id(id_), val(val_) {}
	int id;
	double val;
	inline bool operator<(const IntegerDoublePair& other) const {
		return (id < other.id);
	}
};

void computeUrshapeWeightsUniform(SimpleMesh* urshape);
void computeUrshapeWeightsPositive(SimpleMesh* urshape);
void computeUrshapeWeights(SimpleMesh* urshape);
void computeUrshapeWeightsChordal(SimpleMesh* urshape);
void getEDefPerVertex(SimpleMesh* sm, SimpleMesh* urshape, SquareMatrixND<vec3d>* rots, std::vector<double>& errors);
double getEDef(SimpleMesh* sm, SimpleMesh* urshape, SquareMatrixND<vec3d>* rots);
void FindOptimalRotations(SimpleMesh* sm, SimpleMesh* urshape, std::vector< SquareMatrixND<vec3d> >& rots_vec);

#endif
