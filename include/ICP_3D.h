#ifndef ICP_3D_H
#define ICP_3D_H


#include "point3d.h"
#include "kd_PointCloud.h"
#include "Matrix3D.h"


class ICP_3D {

public:

	//! Constructor.
	ICP_3D();

	//! Destructor.
	~ICP_3D();

	//! Set the model point cloud, i.e. the point cloud that <b>won't</b> be moved by the ICP.
	void setModelData(char* model_file);

	//! Set the data point cloud, i.e. the point cloud that <b>will</b> be moved by the ICP.
	void setData(char* data_file);

	//! Applies a rotation ('mat') and a translation ('trans') to the data point cloud.
	void applyTransformToData(mat3f mat, vec3f trans);

	//! writes the current 'data' point cloud to 'filename'
	void writeCurrentDataPointCloud(char* filename);

	void DoICP(int num) {
		for (int i1 = 0; i1 < num; i1++) {
			std::cerr << "Performing Step " << i1 << std::endl;
			perform1ICPStep();
		}
	}

	//! The model point cloud.
	kdPointCloud<vec3f>* model;

	//! The data point cloud.
	PointCloud<vec3f>* data;

private:

	//! Performs one ICP Step.
	void perform1ICPStep();

	//! Center of gravity of the model point cloud.
	vec3f model_cog;

	//! Center of gravity of the data point cloud.
	vec3f data_cog;

};

#endif

