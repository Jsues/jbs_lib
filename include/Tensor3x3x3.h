#ifndef JBS_TENSOR_3x3x3_H
#define JBS_TENSOR_3x3x3_H

#include <iostream>

#include "point3d.h"
#include "Matrix3D.h"

//! 3x3x3 Tensor.
template <class T>
class Tensor3x3x3 {

public:

	Tensor3x3x3() {
	};

	~Tensor3x3x3() {
	};

	//! Set the value at [x][y][z]. 
	void set(unsigned int x, unsigned int y, unsigned int z, T value) {
		data_tensor[x][y][z] = value;
	}

	//! Print tensor to stdcerr.
	void print() {
		std::cerr << "---TENSOR---" << std::endl;
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				for (int z = 0; z < 3; z++) {
					std::cerr << data_tensor[x][y][z] << " ";
				}
				std::cerr << std::endl;
			}
			std::cerr << "--" << std::endl;
		}
		std::cerr << "---FINISHED---" << std::endl;
	}

	//! Sets the tensor to be the epsilon-tensor.
	void SetEpsilonTensor() {
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				for (int z = 0; z < 3; z++) {
					if (x == y || x == z || y == z) {
						data_tensor[x][y][z] = T(0);
					} else {
						int num_perm = getInversionNumber(x, y, z);
						if (num_perm%2 == 0) {
							data_tensor[x][y][z] = T(-1);
						} else {
							data_tensor[x][y][z] = T(1);
						}
					}
				}
			}
		}
	}

	//! Multiply tensor with vector.
	Matrix3D<T> operator|(point3d<T> val) {
		Matrix3D<T> ret;

		ret.a[0] = val.x * data_tensor[0][0][0] + val.y * data_tensor[0][0][1] + val.z * data_tensor[0][0][2];
		ret.a[1] = val.x * data_tensor[0][1][0] + val.y * data_tensor[0][1][1] + val.z * data_tensor[0][1][2];
		ret.a[2] = val.x * data_tensor[0][2][0] + val.y * data_tensor[0][2][1] + val.z * data_tensor[0][2][2];
		ret.a[3] = val.x * data_tensor[1][0][0] + val.y * data_tensor[1][0][1] + val.z * data_tensor[1][0][2];
		ret.a[4] = val.x * data_tensor[1][1][0] + val.y * data_tensor[1][1][1] + val.z * data_tensor[1][1][2];
		ret.a[5] = val.x * data_tensor[1][2][0] + val.y * data_tensor[1][2][1] + val.z * data_tensor[1][2][2];
		ret.a[6] = val.x * data_tensor[2][0][0] + val.y * data_tensor[2][0][1] + val.z * data_tensor[2][0][2];
		ret.a[7] = val.x * data_tensor[2][1][0] + val.y * data_tensor[2][1][1] + val.z * data_tensor[2][1][2];
		ret.a[8] = val.x * data_tensor[2][2][0] + val.y * data_tensor[2][2][1] + val.z * data_tensor[2][2][2];
		return ret;
	}

private:

	//! Returns the number of inversions in the permutation xyz.
	int getInversionNumber(int x, int y, int z) {
		int perm = 0;
		if (x > y) perm++;
		if (x > z) perm++;
		if (y > z) perm++;
		return perm;
	};

	union {
		T data_tensor[3][3][3];
		T data_array[27];
	};

};


#endif

