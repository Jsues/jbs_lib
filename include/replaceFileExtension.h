#ifndef REPLACE_FILE_EXTENSION_H
#define REPLACE_FILE_EXTENSION_H

#include <iostream>
#include <string>
#include "JBS_General.h"

namespace JBSlib {

	std::string replaceFileExtension(const char* filename_old, const char* new_extension) {
		std::string f_o(filename_old);
		UInt pos = (UInt) f_o.rfind(".");
		//UInt length = (UInt) f_o.length();


		std::string f_capped = f_o.substr(0, pos);
		f_capped = f_capped + std::string(".") + std::string(new_extension);

		return f_capped;
	}

};

#endif
