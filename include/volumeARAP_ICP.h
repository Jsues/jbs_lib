#ifndef VOLUME_ARAP_ICP
#define VOLUME_ARAP_ICP

//#define USE_TIMER
#define SHOW_DEBUG_INFO

#ifdef GET_TIMINGS
#include "..\VARAP_def_cmdline\PrecTimer.h"
#endif

#include "PointCloud.h"
#include "PolyLine.h"
#include "SimpleMesh.h"
#include "operators/getCage.h"
#include "operators/computeVertexNormals.h"
#include "HoppeImplicitFunc.h"
#include "kd_PointCloud.h"
#include "MathMatrix.h"
#include "timer.h"
#include "MatrixnD.h"
#include <vector> 


#ifdef USE_UMFPACK
#include "umfpack.h"
#else
extern "C" {
#include <taucs.h>
}
#endif

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


//! The barycentric coordinates of a point within a cell of the VARAP grid.
struct inCellCoordinates {

	//! The barycentric coordinates with respect to the points indexed in 'indices'.
	double weights[8];

	//! The indices of the points that span the VARAP grid cell.
	int indices[8];
};


//! A correspondence.
struct VICP_Correspondence {

	VICP_Correspondence(int i, vec3d p) : idx(i), pos(p) {}

	//! The index of the source point.
	int idx;

	//! the corresponding target position.
	vec3d pos;
};


// ****************************************************************************************
// ****************************************************************************************


void setUniformWeights(SimpleMesh* urshape) {
	for (UInt i1 = 0; i1 < urshape->eList.size(); i1++) {
		urshape->eList[i1]->cotangent_weight = 1.0;
	}
	for (UInt i1 = 0; i1 < urshape->vList.size(); i1 ++) {
		urshape->vList[i1].param.x = urshape->vList[i1].getNumN();
	}
}


// ****************************************************************************************
// ****************************************************************************************


//! A class for non-rigidly registering a triangle mesh to a point cloud using the Volumetric As-Rigid-As_Possible (VARAP) ICP.
class VARAP_ICP {

public:

	friend class VARAP_Register;

// ****************************************************************************************

	//! Constructor.
	/*
		@param templatemesh The mesh that will be deformed such that it matches the target point cloud.
		@param target_pc The target point cloud.
	*/
	VARAP_ICP(SimpleMesh* template_mesh, kdPointCloudNormals<vec3f>* target_pc) : m_target_pc(target_pc), m_template_mesh(template_mesh) {
		m_numV = m_template_mesh->getNumV();
		m_is_boundary_point.resize(target_pc->m_points.size());
		//computeBoundaryPoints();
		m_cage_fullymeshed = NULL;
		NORMAL_THRESHOLD = 0.8;
		SLIDING_THRESHOLD = 0.7;
	}
	
// ****************************************************************************************

	//! Destructor
	~VARAP_ICP() {
		if (m_cage_fullymeshed != NULL) delete m_cage_fullymeshed;
		if (m_cage_working != NULL) delete m_cage_working;
	}

// ****************************************************************************************

	//! Builds a voxel cage for the template mesh.
	/*
		@param maxRes the maximal resolution of the voxel cage. Voxels will be cubes, thus 'maxRes' is the number of cubes in the direction of maximum extend of the template mesh.
		@param vol A pointer to the volume that will be created (pointer will be set per reference).
		@param fillCage specifies whether the cage should be filled.
	*/
	void buildCage(int maxRes, Volume<int>*& vol, bool fillCage = false, bool dilate = false) {

		m_cage_fullymeshed = new SimpleMesh;

		//std::cerr << m_template_mesh->getNumT() << "  -  " << m_template_mesh->getNumV() << std::endl;

		m_cage_org = getRectilinearCage(m_template_mesh, maxRes, vol, m_cage_fullymeshed, fillCage, dilate);

		float dist = vol->pos(0,0,0).dist(vol->pos(0,0,1));

		m_baryCords.reserve(m_numV);

		
		// Compute tri-linear coordinates of all mesh vertices.
		for (int i = 0; i < m_numV; i++) {
			const vec3d& pt = m_template_mesh->vList[i].c;
			vec3f p((float)pt.x, (float)pt.y, (float)pt.z);

			vec3i v = vol->worldPosToIndices(p);
			vec3f crop = vol->pos(v.x, v.y, v.z);
			vec3f d = p-crop;
			d /= dist;

			inCellCoordinates icc;
			icc.indices[0] = (vol->get(v.x, v.y, v.z)-2);
			icc.indices[1] = (vol->get(v.x+1, v.y, v.z)-2);
			icc.indices[2] = (vol->get(v.x+1, v.y+1, v.z)-2);
			icc.indices[3] = (vol->get(v.x, v.y+1, v.z)-2);
			icc.indices[4] = (vol->get(v.x, v.y, v.z+1)-2);
			icc.indices[5] = (vol->get(v.x+1, v.y, v.z+1)-2);
			icc.indices[6] = (vol->get(v.x+1, v.y+1, v.z+1)-2);
			icc.indices[7] = (vol->get(v.x, v.y+1, v.z+1)-2);
			icc.weights[0] = (1-d.z)*(1-d.y)*(1-d.x);
			icc.weights[1] = (1-d.z)*(1-d.y)*(d.x);
			icc.weights[2] = (1-d.z)*(d.y)*(d.x);
			icc.weights[3] = (1-d.z)*(d.y)*(1-d.x);
			icc.weights[4] = (d.z)*(1-d.y)*(1-d.x);
			icc.weights[5] = (d.z)*(1-d.y)*(d.x);
			icc.weights[6] = (d.z)*(d.y)*(d.x);
			icc.weights[7] = (d.z)*(d.y)*(1-d.x);
			m_baryCords.push_back(icc);
		}
		

		setUniformWeights(m_cage_org);

		m_cage_working  = new SimpleMesh;
		for (int i1 = 0; i1 < m_cage_org->getNumV(); i1++) m_cage_working->insertVertex(m_cage_org->vList[i1].c);
		for (int i1 = 0; i1 < (int)m_cage_org->eList.size(); i1++) {
			Edge* e = m_cage_org->eList[i1];
			m_cage_working->insertEdge(e->v0, e->v1);
		}

	}

// ****************************************************************************************


	//! Builds a voxel cage for the template mesh.
	/*
		@param maxRes the maximal resolution of the voxel cage. Voxels will be cubes, thus 'maxRes' is the number of cubes in the direction of maximum extend of the template mesh.
		@param fillCage specifies whether the cage should be filled.
	*/
	void buildCage(int maxRes, bool fillCage = false) {
		Volume<int>* vol;
		buildCage(maxRes, vol, fillCage);
		delete vol;
	}


// ****************************************************************************************

	vec3f getNormalMatrixForVertex(int i1) const {
		const inCellCoordinates& icc = m_baryCords[i1];
		const vec3f& p0 = m_cage_working->vList[icc.indices[0]].color;
		const vec3f& p1 = m_cage_working->vList[icc.indices[1]].color;
		const vec3f& p2 = m_cage_working->vList[icc.indices[2]].color;
		const vec3f& p3 = m_cage_working->vList[icc.indices[3]].color;
		const vec3f& p4 = m_cage_working->vList[icc.indices[4]].color;
		const vec3f& p5 = m_cage_working->vList[icc.indices[5]].color;
		const vec3f& p6 = m_cage_working->vList[icc.indices[6]].color;
		const vec3f& p7 = m_cage_working->vList[icc.indices[7]].color;

		vec3f rot = p0*(float)icc.weights[0] + p1*(float)icc.weights[1] + p2*(float)icc.weights[2] + p3*(float)icc.weights[3] + p4*(float)icc.weights[4] + p5*(float)icc.weights[5] + p6*(float)icc.weights[6] + p7*(float)icc.weights[7];
		return rot;
	}

// ****************************************************************************************
	
	//! Updates the vertices of the template mesh according to the current working cage 'm_cage_working'.
	void updateMeshVertices() {

#ifdef SHOW_DEBUG_INFO
		double move = 0;
#endif

		for (int i1 = 0; i1 < m_numV; i1++) {
			const inCellCoordinates& icc = m_baryCords[i1];
			const vec3d& p0 = m_cage_working->vList[icc.indices[0]].c;
			const vec3d& p1 = m_cage_working->vList[icc.indices[1]].c;
			const vec3d& p2 = m_cage_working->vList[icc.indices[2]].c;
			const vec3d& p3 = m_cage_working->vList[icc.indices[3]].c;
			const vec3d& p4 = m_cage_working->vList[icc.indices[4]].c;
			const vec3d& p5 = m_cage_working->vList[icc.indices[5]].c;
			const vec3d& p6 = m_cage_working->vList[icc.indices[6]].c;
			const vec3d& p7 = m_cage_working->vList[icc.indices[7]].c;

			vec3d newpos = p0*icc.weights[0] + p1*icc.weights[1] + p2*icc.weights[2] + p3*icc.weights[3] + p4*icc.weights[4] + p5*icc.weights[5] + p6*icc.weights[6] + p7*icc.weights[7];
#ifdef SHOW_DEBUG_INFO
			move += m_template_mesh->vList[i1].c.dist(newpos);
#endif
			m_template_mesh->vList[i1].c = newpos;
		}

#ifdef SHOW_DEBUG_INFO
		//std::cerr << "Movement: " << move << std::endl;
		std::cerr << move << std::endl;
#endif

	}
	
// ****************************************************************************************

	//! Returns the (possibly deformed) working cage.
	SimpleMesh* getWorkingCage() {
		return m_cage_working;
	}

// ****************************************************************************************

	//! Returns the (possibly deformed) template mesh.
	SimpleMesh* getTemplateMesh() {
		return m_template_mesh;
	}

// ****************************************************************************************

	void setNumV(int newNumV) {
		m_numV = newNumV;
	}

// ****************************************************************************************

	//! Sets correspondences. Use this function for non-rigid alignment.
	void setCorrespondences(std::vector<VICP_Correspondence>& targetCorr) {

		m_correspondences.clear();
		for (unsigned int i1 = 0; i1 < targetCorr.size(); i1++)
			m_correspondences.push_back(targetCorr[i1]);
	}

// ****************************************************************************************

	//! Finds for each point in 'm_template_mesh' the closest point in 'm_target_pc' and, if the two points have compatible normals, adds a new correspondence for these points into 'm_correspondences'.
	void findCorrespondencesForSelectedVerts(const std::vector<int>& verts) {

		m_correspondences.clear();
		vec3d* mesh_normals = computeVertexNormals(m_template_mesh);

		for (UInt i1 = 0; i1 < verts.size(); i1++) {
			int id = verts[i1];
			const vec3d& p = m_template_mesh->vList[id].c;
			const vec3d& n = mesh_normals[id];

			int idx = m_target_pc->getNearestPoint(p);

			vec3d cp = m_target_pc->m_points[idx];
			vec3d cn = m_target_pc->m_normals[idx];

			if (((cn|n) > NORMAL_THRESHOLD) && !m_is_boundary_point[idx]){ // Is a valid correspondence
				m_template_mesh->vList[id].color = vec3f(1,0,0);

				// Sliding check
				if (((cp-p).getNormalized()|n) < SLIDING_THRESHOLD) {
					m_template_mesh->vList[id].color = vec3f(0,0,1);
				} else {
					m_correspondences.push_back(VICP_Correspondence(id, cp));
				}

			} else {
				m_template_mesh->vList[id].color = vec3f(0,0,1);
			}
		}

		delete[] mesh_normals;
	}
	
// ****************************************************************************************

	//! Finds for each point in 'm_template_mesh' the closest point in 'm_target_pc' and, if the two points have compatible normals, adds a new correspondence for these points into 'm_correspondences'.
	void findCorrespondencesSARAPInsideOutside(std::vector<vec3d>& targetNormal, std::vector<double>& targetDistances, std::vector<double>& weights) {

		vec3d* mesh_normals = computeVertexNormals(m_template_mesh);

		for (int i1 = 0; i1 < m_numV; i1++) {
			const vec3d& p = m_template_mesh->vList[i1].c;
			const vec3d& n = mesh_normals[i1];

			int idx = m_target_pc->getNearestPoint(p);

			vec3d cp = m_target_pc->m_points[idx];
			vec3d cn = m_target_pc->m_normals[idx];


			if (((cn|n) > 0.8) && !m_is_boundary_point[idx]){ // Is a valid correspondence
				m_template_mesh->vList[i1].color = vec3f(1,0,0);
				targetNormal[i1] = cn;
				targetDistances[i1] = cp|cn;

				double weight = 1;
				if ((p|cn) > targetDistances[i1]) weight = 1000;
				weights[i1] = weight;
			} else {
				targetNormal[i1] = vec3d(0,0,0);
				targetDistances[i1] = 0;
			}
		}

		delete[] mesh_normals;
	}

// ****************************************************************************************

	//! Finds for each point in 'm_template_mesh' the closest point in 'm_target_pc' and, if the two points have compatible normals, adds a new correspondence for these points into 'm_correspondences'.
	void findCorrespondences(int increment = 1) {

		m_correspondences.clear();
		vec3d* mesh_normals = computeVertexNormals(m_template_mesh);

		for (int i1 = 0; i1 < m_numV; i1 += increment) {
			const vec3d& p = m_template_mesh->vList[i1].c;
			const vec3d& n = mesh_normals[i1];

			int idx = m_target_pc->getNearestPoint(p);

			vec3d cp = m_target_pc->m_points[idx];
			vec3d cn = m_target_pc->m_normals[idx];

			if (((cn|n) > NORMAL_THRESHOLD) && !m_is_boundary_point[idx]){ // Is a valid correspondence
				m_template_mesh->vList[i1].color = vec3f(1,0,0);

				// Sliding check
				if (((cp-p).getNormalized()|n) < SLIDING_THRESHOLD) {
					m_template_mesh->vList[i1].color = vec3f(0,0,1);
				} else {
					m_correspondences.push_back(VICP_Correspondence(i1, cp));
				}

			} else {
				m_template_mesh->vList[i1].color = vec3f(0,0,1);
			}
		}

		delete[] mesh_normals;
	}

// ****************************************************************************************


	//void doRegistrationStepTestSparseMatrixDeleteMeLater(double weight = 0.5, int numIter = 5) {

	//	int numUnknown = m_cage_org->getNumV();
	//	int numC = (int)m_correspondences.size();
	//	
	//	RandAccessCompRowMatrix<double> A(numC, numUnknown);
	//	RandAccessCompColMatrix<double> Acol(numC, numUnknown);

	//	for (int i1 = 0; i1 < numC; i1++) {
	//		const VICP_Correspondence& c = m_correspondences[i1];
	//		const inCellCoordinates& icc = m_baryCords[c.idx];

	//		for (int i2 = 0; i2 < 8; i2++) {
	//			A.setIgnorePrevious(i1, icc.indices[i2], icc.weights[i2]);
	//			Acol.setBottomDown(i1, icc.indices[i2], icc.weights[i2]);
	//		}
	//	}

	//	std::ofstream fout("xxx.txt");
	//	A.print(fout);


	//	std::vector<double> entries;
	//	std::vector<int> row_index;
	//	std::vector<int> col_ptr;
	//	Acol.getAtAForTAUCS(entries, row_index, col_ptr);

	//	std::ofstream fout2("test2.txt");
	//	taucsMatrixToStream(entries, row_index, col_ptr, fout2);

	//}

	

// ****************************************************************************************


	void computeRotationsAtNodes() {

		int numUnknown = m_cage_org->getNumV();

		// Compute optimal rotations:
#pragma omp parallel for
		for (int i2 = 0; i2 < (int)numUnknown; i2++) {
			SquareMatrixND<vec3d> COV;
			UInt v0 = i2;
			const vec3d& p_i = m_cage_org->vList[v0].c;
			const vec3d& p_dash_i = m_cage_working->vList[v0].c;
			for (UInt i3 = 0; i3 < (UInt)m_cage_org->vList[v0].getNumN(); i3++) {
				UInt v1 = m_cage_org->vList[v0].getNeighbor(i3, v0);
				const vec3d& p_j = m_cage_org->vList[v1].c;
				const vec3d& p_dash_j = m_cage_working->vList[v1].c;
				COV.addFromTensorProduct((p_i-p_j), (p_dash_i-p_dash_j));
			}
			SquareMatrixND<vec3d> U, V;
			vec3d sigma;
			bool SVD_result = COV.SVD_decomp(U, sigma, V);
			U.transpose();
			SquareMatrixND<vec3d> rot = V*U;
			m_cage_working->vList[v0].color = rot.getRotVec();
		}

		normalizeRotOrientations();
	}

	
// ****************************************************************************************

	void normalizeRotOrientations() {

		std::vector<bool> processed(m_cage_working->getNumV());

		std::list<int> queue;
		queue.push_back(0);
		processed[0] = true;

		while (queue.size() > 0) {
			int idx = queue.front();
			queue.pop_front();

			const vec3f& rot = m_cage_working->vList[idx].color;

			for (UInt i1 = 0; i1 < m_cage_working->vList[idx].getNumN(); i1++) {
				int n = m_cage_working->vList[idx].getNeighbor(i1, idx);

				if (processed[n]) continue;

				processed[n] = true;
				queue.push_back(n);

				// find best rot
				vec3f& rot_n = m_cage_working->vList[n].color;

				float l = rot_n.length();
				float newl = l-(float)M_PI2;
				vec3f rrr = rot_n.getNormalized();
				rrr *= newl;

				if ((rrr|rot) > (rot_n|rot)) rot_n = rrr;
			}

		}
		
	}
	
// ****************************************************************************************
//#define SINGLE_CALL_TAUCS
// ****************************************************************************************

	//! Performs a registration step (i.e., tries to deform the cage such that the vertices come closer to their corresponding points).
	/*
		@param weight weight to balance between a tight fit (weight = 0.000001) and a rigid deformation (weHydrodensitometrie nuight = infinity).
		@param numIter number of inner iterations for recovering rigidity.
	*/
	void doRegistrationStep(double weight = 0.5, int numIter = 5, std::vector<VICP_Correspondence>* hc = NULL) {

		double weight_half = weight/2.0;


#ifdef GET_TIMINGS

double init_zero;
double build_matrix;
double factor_matrix;
double time_svd = 0;
double time_rhs = 0;
double time_solve = 0;
double time_copy;
Timer precise_timer;
precise_timer.elapsedTimeSeconds();
#endif


		int numUnknown = m_cage_org->getNumV();

		for (int i1 = 0; i1 < m_cage_fullymeshed->getNumV(); i1++) m_cage_fullymeshed->vList[i1].param = vec2d(0.0);
		for (int i1 = 0; i1 < (int)m_cage_fullymeshed->eList.size(); i1++) m_cage_fullymeshed->eList[i1]->cotangent_weight = 0;
		
		std::vector<double> rhs(3*numUnknown);

		double* rhs_x = &rhs[0];
		double* rhs_y = &rhs[numUnknown];
		double* rhs_z = &rhs[2*numUnknown];
		int numC = (int)m_correspondences.size();

#ifdef SHOW_DEBUG_INFO
//		std::cerr << "numCorrs: " << numC << std::endl;
#endif


#ifdef GET_TIMINGS
init_zero = precise_timer.elapsedTimeSeconds();
#endif


		for (int i1 = 0; i1 < numC; i1++) {
			const VICP_Correspondence& c = m_correspondences[i1];
			const inCellCoordinates& icc = m_baryCords[c.idx];

			// Set vertex weights:
			for (int i2 = 0; i2 < 8; i2++) {
				const int& v1 = icc.indices[i2];
				const double& w1 = icc.weights[i2];
				m_cage_fullymeshed->vList[v1].param.x += w1*w1;

				// Set edge weights
				for (int i3 = i2+1; i3 < 8; i3++) {
					const int& v2 = icc.indices[i3];
					const double& w2 = icc.weights[i3];
					Edge* e = m_cage_fullymeshed->vList[v1].eList.getEdge(v1,v2);
					e->cotangent_weight += w1*w2;
				}

				rhs_x[v1] += c.pos.x*w1;
				rhs_y[v1] += c.pos.y*w1;
				rhs_z[v1] += c.pos.z*w1;
			}
		}

		// If we have hard constraints
		if (hc != NULL) {
			for (UInt i1 = 0; i1 < hc->size(); i1++) {

				float hard_weight = 1000.0f;

				const VICP_Correspondence& c = (*hc)[i1];
				const inCellCoordinates& icc = m_baryCords[c.idx];

				// Set vertex weights:
				for (int i2 = 0; i2 < 8; i2++) {
					const int& v1 = icc.indices[i2];
					const double& w1 = icc.weights[i2];
					m_cage_fullymeshed->vList[v1].param.x += w1*w1*hard_weight;

					// Set edge weights
					for (int i3 = i2+1; i3 < 8; i3++) {
						const int& v2 = icc.indices[i3];
						const double& w2 = icc.weights[i3];
						Edge* e = m_cage_fullymeshed->vList[v1].eList.getEdge(v1,v2);
						e->cotangent_weight += w1*w2 * hard_weight;
					}

					rhs_x[v1] += c.pos.x*w1 * hard_weight;
					rhs_y[v1] += c.pos.y*w1 * hard_weight;
					rhs_z[v1] += c.pos.z*w1 * hard_weight;
				}
			}
		}

		
		std::vector<double> entries;
		std::vector<int> row_index;
		std::vector<int> col_ptr;
		col_ptr.push_back(0);


		for (int i1 = 0; i1 < numUnknown; i1++) {

			std::set< std::pair<int, double> > neighbors;
			double sum = 0;

			for (UInt i2 = 0; i2 < m_cage_fullymeshed->vList[i1].getNumN(); i2++) {
				int n = m_cage_fullymeshed->vList[i1].getNeighbor(i2, i1);
				double w = m_cage_fullymeshed->vList[i1].eList[i2]->cotangent_weight;
				
				const Edge* e = m_cage_org->vList[i1].eList.getEdge(i1, n);
				if (e != NULL) { // Add volume ARAP regularizer part:
					w -= e->cotangent_weight*weight;
				}

				if (w != 0) {
					neighbors.insert(std::make_pair(n, w));
				}
			}

			double vertWeight = m_cage_fullymeshed->vList[i1].param.x;

			// Add volume ARAP regularizer part:
			vertWeight += m_cage_org->vList[i1].param.x*weight;

			neighbors.insert(std::make_pair<int, double>(i1, vertWeight));

			for (std::set< std::pair<int, double> >::const_iterator it = neighbors.begin(); it != neighbors.end(); it++) {
				int nID = it->first;

#ifndef USE_UMFPACK // If we use taucs, only store the upper half
				if (it->first < i1) continue;
#endif

				entries.push_back(it->second);
				row_index.push_back(it->first);
			}

			col_ptr.push_back((int)entries.size());
		}


#ifdef GET_TIMINGS
build_matrix = precise_timer.elapsedTimeSeconds();
#endif


		std::vector<double> xv(numUnknown*3);

		// Pre-factor matrix
#ifdef USE_UMFPACK
	    void *Symbolic, *Numeric;
		int result1 = umfpack_di_symbolic(numUnknown, numUnknown, &col_ptr[0], &row_index[0], &entries[0], &Symbolic, NULL, NULL);
		int result2 = umfpack_di_numeric(&col_ptr[0], &row_index[0], &entries[0], Symbolic, &Numeric, NULL, NULL);
#else
		// create TAUCS matrix
		taucs_ccs_matrix A;
		A.n = numUnknown;
		A.m = numUnknown;
		A.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
		A.colptr = &col_ptr[0];
		A.rowind = &row_index[0];
		A.values.d = &entries[0];

		taucs_double* xvec = &xv[0]; // the unknown vector to solve Ax=b

		void* F = NULL;
		char* options[] = {"taucs.factor.LLT=true", NULL};

		int* permutation;
		int* inversePermutation;
		void* factorization;
		taucs_ccs_matrix* permutedMatrix;
		taucs_ccs_matrix* matrixTemplatePermuted;

		// Compute permutation
		taucs_ccs_order(&A, &permutation, &inversePermutation, "metis"); // there was "amd" here, but that doesn't work with x64, use 'metis' for 64 bit
		// Compute permuted matrix template
		matrixTemplatePermuted = taucs_ccs_permute_symmetrically(&A, permutation, inversePermutation);
		// Compute symbolic factorization of the matrix template
		factorization = taucs_ccs_factor_llt_symbolic(matrixTemplatePermuted);
		// Compute permutation of the matrix
		permutedMatrix = taucs_ccs_permute_symmetrically(&A, permutation, inversePermutation);
		// Compute numeric factorization of the matrix
		taucs_ccs_factor_llt_numeric(permutedMatrix, factorization);

		std::vector<double> permutedRightHandSide_vec(numUnknown);
		std::vector<double> permutedVariables_vec(numUnknown);
		double* permutedRightHandSide	= &permutedRightHandSide_vec[0];
		double* permutedVariables		= &permutedVariables_vec[0];
#endif

#ifdef GET_TIMINGS
factor_matrix = precise_timer.elapsedTimeSeconds();
#endif

		std::vector< SquareMatrixND<vec3d> > rots_vec(numUnknown);
		SquareMatrixND<vec3d>* rots = &rots_vec[0];

		std::vector<double> final_rhs(3*numUnknown);


		//std::ofstream fout("convergence.txt");


		for (int iter = 0; iter < numIter; iter++) {

			std::copy(rhs.begin(), rhs.end(), final_rhs.begin());
			
#ifdef GET_TIMINGS
precise_timer.elapsedTimeSeconds();
#endif

			// Compute optimal rotations:
#pragma omp parallel for
			for (int i2 = 0; i2 < (int)numUnknown; i2++) {
				SquareMatrixND<vec3d> COV;
				UInt v0 = i2;
				const vec3d& p_i = m_cage_org->vList[v0].c;
				const vec3d& p_dash_i = m_cage_working->vList[v0].c;
				for (UInt i3 = 0; i3 < (UInt)m_cage_org->vList[v0].getNumN(); i3++) {
					UInt v1 = m_cage_org->vList[v0].getNeighbor(i3, v0);
					const vec3d& p_j = m_cage_org->vList[v1].c;
					const vec3d& p_dash_j = m_cage_working->vList[v1].c;
					//const double& w_ij = m_cage_org->vList[v0].eList[i3]->cotangent_weight;
					//COV.addFromTensorProduct((p_i-p_j)*w_ij , (p_dash_i-p_dash_j));
					// w_ij = 1!!!
					//const double& w_ij = m_cage_org->vList[v0].eList[i3]->cotangent_weight;
					COV.addFromTensorProduct((p_i-p_j), (p_dash_i-p_dash_j));
				}
				SquareMatrixND<vec3d> U, V;
				vec3d sigma;
				bool SVD_result = COV.SVD_decomp(U, sigma, V);
				//if (SVD_result) {
				U.transpose();
				SquareMatrixND<vec3d> rot = V*U;
				rots[i2] = rot;
				//} else { // SVD failed!
				//	std::cerr << "SVD failed" << std::endl;
				//	rots[i2].setToIdentity();
				//}
			}

#ifdef GET_TIMINGS
time_svd += precise_timer.elapsedTimeSeconds();
#endif


			// Add rhs:
#pragma omp parallel for
			for (int i1 = 0; i1 < numUnknown; i1++) {
				int numE = m_cage_org->vList[i1].getNumN();
				const vec3d& p0 = m_cage_org->vList[i1].c;
				for (int i2 = 0; i2 < numE; i2++) {

					//const double& w_ij = m_cage_org->vList[i1].eList[i2]->cotangent_weight;

					int other = m_cage_org->vList[i1].eList[i2]->getOther(i1);
					const vec3d& p1 = m_cage_org->vList[other].c;

					SquareMatrixND<vec3d> rot = rots[i1]+rots[other];

					vec3d diff = rot.vecTrans(p0-p1)*weight_half; // 0.5 because rot should be divided by 2.0!!
					//diff = rot.vecTrans(p0-p1)*w_ij*weight*0.5; // 0.5 because rot should be divided by 2.0!!

					final_rhs[i1] += diff.x;
					final_rhs[i1+numUnknown] += diff.y;
					final_rhs[i1+2*numUnknown] += diff.z;
				}
			}

#ifdef GET_TIMINGS
time_rhs += precise_timer.elapsedTimeSeconds();
#endif



#ifdef USE_UMFPACK
			for (int c = 0; c < 3; c++) {
				double* b = &final_rhs[c*numUnknown];
				double* x = &xv[c*numUnknown];
				umfpack_di_solve(UMFPACK_A, &col_ptr[0], &row_index[0], &entries[0], x, b, Numeric, NULL, NULL);
			}
#else
			// solve
			taucs_vec_permute(numUnknown, TAUCS_DOUBLE, &final_rhs[0], permutedRightHandSide, permutation);
			// Solve for the unknowns
			taucs_supernodal_solve_llt(factorization, permutedVariables, permutedRightHandSide);
			// Compute inverse permutation of the unknowns
			taucs_vec_permute(numUnknown, TAUCS_DOUBLE, permutedVariables, xvec+0, inversePermutation);

			taucs_vec_permute(numUnknown, TAUCS_DOUBLE, &final_rhs[numUnknown], permutedRightHandSide, permutation);
			taucs_supernodal_solve_llt(factorization, permutedVariables, permutedRightHandSide);
			taucs_vec_permute(numUnknown, TAUCS_DOUBLE, permutedVariables, &xvec[numUnknown], inversePermutation);

			taucs_vec_permute(numUnknown, TAUCS_DOUBLE, &final_rhs[numUnknown*2], permutedRightHandSide, permutation);
			taucs_supernodal_solve_llt(factorization, permutedVariables, permutedRightHandSide);
			taucs_vec_permute(numUnknown, TAUCS_DOUBLE, permutedVariables, &xvec[numUnknown*2], inversePermutation);
#endif

#ifdef GET_TIMINGS
time_solve += precise_timer.elapsedTimeSeconds();
#endif


#pragma omp parallel for
			//double move = 0;
			for (int i1 = 0; i1 < numUnknown; i1++) {
				//vec3d p(xv[i1], xv[i1+numUnknown], xv[i1+2*numUnknown]);
				////move += p.dist(m_cage_working->vList[i1].c);
				//m_cage_working->vList[i1].c = p;
				//vec3d p(xv[i1], xv[i1+numUnknown], xv[i1+2*numUnknown]);
				//move += p.dist(m_cage_working->vList[i1].c);
				m_cage_working->vList[i1].c.x = xv[i1];
				m_cage_working->vList[i1].c.y = xv[i1+numUnknown];
				m_cage_working->vList[i1].c.z = xv[i1+2*numUnknown];
			}
			//fout << move << std::endl;
			//std::cerr << iter << " move1: " << move << std::endl;
		}


#ifdef GET_TIMINGS
time_copy = precise_timer.elapsedTimeSeconds();
#endif


		//fout.close();

#ifdef USE_UMFPACK
	umfpack_di_free_symbolic(&Symbolic);
	umfpack_di_free_numeric(&Numeric);
#else
    // Free allocated memory
    if(factorization != NULL) taucs_supernodal_factor_free(factorization);
    //if(factorization != NULL) taucs_supernodal_factor_free_numeric(factorization);
    if(permutation != NULL) delete [] permutation;
    if(inversePermutation != NULL) delete [] inversePermutation;
    if(matrixTemplatePermuted != NULL) taucs_ccs_free(matrixTemplatePermuted);
	if(permutedMatrix != NULL) taucs_ccs_free(permutedMatrix);
#endif


#ifdef GET_TIMINGS
	std::cerr << "   initialize: " << init_zero*1000 << " ms" << std::endl;
	std::cerr << "   build matrix: " << build_matrix*1000 << " ms" << std::endl;
	std::cerr << "   factor matrix: " << factor_matrix*1000 << " ms" << std::endl;
	std::cerr << "   time svd (avg): " << time_svd/numIter*1000 << " ms" << std::endl;
	std::cerr << "   time rhs (avg): " << time_rhs/numIter*1000 << " ms" << std::endl;
	std::cerr << "   time rhs+svd (avg): " << (time_rhs+time_svd)/numIter*1000 << " ms" << std::endl;
	std::cerr << "   time solve (avg): " << time_solve/numIter*1000 << " ms" << std::endl;
	std::cerr << "   time copy: " << time_copy*1000 << std::endl;
#endif

}

// ****************************************************************************************

	void saveCorrespondencesToFile() {
		PolyLine<vec3d> pl;

		for (int i1 = 0; i1 < (int)m_correspondences.size(); i1++) {
			const vec3d& p1 = m_correspondences[i1].pos;
			const vec3d& p2 = m_template_mesh->vList[m_correspondences[i1].idx].c;
			pl.insertVertex(p1);
			pl.insertVertex(p2);
			pl.insertLine(i1*2, i1*2+1);
		}
		pl.writeToFile("corrs.pl");
	}

// ****************************************************************************************

	void replaceTargetPC(kdPointCloudNormals<vec3f>* new_target_pc) {
		delete m_target_pc;
		m_target_pc = new_target_pc;
		m_is_boundary_point.resize(m_target_pc->m_points.size());
		computeBoundaryPoints();
	}

// ****************************************************************************************

	double NORMAL_THRESHOLD;
	double SLIDING_THRESHOLD;

private:

// ****************************************************************************************

	//! Computes the boundary points of the target point cloud. Stores for each point of 'm_target_pc' whether it is a boundary point in 'm_is_boundary_point'.
	void computeBoundaryPoints() {
		std::cerr << "Computing boundary points...";

		const int k = 20;
		ANNidx  nnidx[k];
		ANNdist nndist[k];

		for (int i1 = 0; i1 < (int)m_target_pc->m_points.size(); i1++) {

			const vec3f p = m_target_pc->m_points[i1];
			m_target_pc->getkNearestPoints(p, k, nnidx, nndist);

			// Compute COG of closest points:
			vec3f cog(0,0,0);
			for (int i2 = 0; i2 < k; i2++) {
				const vec3f np = m_target_pc->m_points[nnidx[i2]];
				cog += np;
			}
			cog /= k;

			float dist = p.dist(cog);
			dist /= sqrt((float)nndist[k-1]);

			if (dist > 0.5) m_is_boundary_point[i1] = true;
		}

		if (false) {
			PointCloudNormals<vec3f> b;

			for (int i1 = 0; i1 < (int)m_target_pc->m_points.size(); i1++) {
				if (m_is_boundary_point[i1]) {
					const vec3f& p = m_target_pc->m_points[i1];
					const vec3f& n = m_target_pc->m_normals[i1];
					b.insertPoint(p,n);
				}
			}
			
			//PointCloudWriter<vec3f>::writePWNFileBinary(&b, "boundary.pcb");
		}

		std::cerr << "done" << std::endl;
	}

// ****************************************************************************************

	//! The target point cloud.
	kdPointCloudNormals<vec3f>* m_target_pc;

	//! The template mesh that will be deformed such that it matches the target point cloud.
	SimpleMesh* m_template_mesh;

	//! The number of points in 'm_template_mesh'.
	int m_numV;

	//! The barycentric coordinates of the mesh vertices with respect to the VARAP grid.
	std::vector<inCellCoordinates> m_baryCords;

	//! The initially undeformed VARAP cage.
	SimpleMesh* m_cage_org;
	
	//! The working VARAP cage that is deformed to perform the registration.
	SimpleMesh* m_cage_working;

	//! Defines for each point in 'm_target_pc' whether it lies on the boundary of the point cloud. This array is filled by the function 'computeBoundaryPoints'.
	std::vector<bool> m_is_boundary_point;

	//! The fully meshed cage. I.e., all 27 vertices v_j of the 8 cells that share an vertex v_i are connected to v_i by an edge.
	SimpleMesh* m_cage_fullymeshed;

public:

	//! Array of correpondences for VARAP ICP.
	std::vector<VICP_Correspondence> m_correspondences;
};



#endif

