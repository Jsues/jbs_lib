#ifndef JBS_MESH_NOISER_H
#define JBS_MESH_NOISER_H

#include "JBS_General.h"
#include "SimpleMesh.h"
#include "random.h"
#include <ctime>


//! This function takes a 'SimpleMesh*' or one of its derived childs adds noise to the vertices of the mesh.
/*!
	\param mesh The input mesh
	\param amplitude The width of the is a gausian distributed variable with mean 0 and variance 1 which is multiplied withh 'amplitude'.
	\return the mesh with noise.
*/
SimpleMesh* addNoise(SimpleMesh* mesh, float amplitude) {
	srand(int(time(NULL)));

	int numOfPoints = (int)mesh->vList.size();

	float phi, theta;
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		phi = (float(rand()) / RAND_MAX) * float(M_PI);
		theta = (float(rand()) / RAND_MAX) * float(M_PI2);

		vec3d disp(sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta));
		disp *= (amplitude * float(getGaussian0to1()));

		mesh->vList[i1].c = mesh->vList[i1].c + disp;
	}

	return mesh;
}


#endif
