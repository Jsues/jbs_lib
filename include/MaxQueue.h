#ifndef MAX_QUEUE_H
#define MAX_QUEUE_H

#include <set>
#include <vector>

template <class KeyType, class IndexType>
class MaxQueue {
public:

	typedef std::pair<KeyType, IndexType> key;

	MaxQueue(int c) : capacity(c) {}

	MaxQueue() : capacity(0) {}

	void setCapacity(int c) {
		capacity = c;
	}

	void clear() {
		queue.clear();
	}

	void add(IndexType pos, KeyType val) {

		if ((int)queue.size() < capacity) {
			queue.insert(std::make_pair(val, pos));
		} else {
			if (queue.begin()->first < val) {
				queue.erase(queue.begin());
				queue.insert(std::make_pair(val, pos));
			}
		}
	}

	void print() {
		
		for(std::set<key>::iterator it = queue.begin(); it != queue.end(); it++) {
			std::cerr << it->first << " " << it->second << std::endl;
		}

	}

	int capacity;

	std::set< key > queue;
};


template <class KeyType, class IndexType>
class MinQueue {
public:

	typedef std::pair<KeyType, IndexType> key;
	typedef std::set< key, std::greater<key> > SetType;

	MinQueue(int c) : capacity(c) {}

	MinQueue() : capacity(0) {}

	void setCapacity(int c) {
		capacity = c;
	}

	void clear() {
		queue.clear();
	}

	void add(IndexType pos, KeyType val) {

		if ((int)queue.size() < capacity) {
			queue.insert(std::make_pair(val, pos));
		} else {
			if (queue.begin()->first > val) {
				queue.erase(queue.begin());
				queue.insert(std::make_pair(val, pos));
			}
		}
	}

	inline KeyType getMaxValue() const {
		return queue.begin()->first;
	}

	void print() {
		
		for(SetType::iterator it = queue.begin(); it != queue.end(); it++) {
			std::cerr << it->first << " " << it->second << std::endl;
		}

	}


	int capacity;

	SetType queue;
};


#endif