#ifndef MESH_VOLUME_H
#define MESH_VOLUME_H

#include "SimpleMesh.h"

//! Computes the volume enclosed by a (closed) mesh.
double computeMeshVolume(const SimpleMesh* sm) {

	int numT = sm->getNumT();

	double vol = 0.0;
	for (int i1 = 0; i1 < numT; i1++) {
		const Triangle* t = sm->tList[i1];
		const vec3d& p0 = sm->vList[t->v0()].c;
		const vec3d& p1 = sm->vList[t->v1()].c;
		const vec3d& p2 = sm->vList[t->v2()].c;

		vol += p0|(p1^p2);
	}

	vol /= 6.0;

	return vol;
}

#endif
