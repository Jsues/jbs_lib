#ifndef ARAP_H
#define ARAP_H

#include "MatrixnD.h"
#include "SimpleMesh.h"
#include "taucs_tools.h"
#include "Timer.h"
//#include "umfpack.h"
#include <set>

extern "C" {
#include <taucs.h>
}

#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif

//******************************************************************************************

void printTaucsMatrixToFile(char* file, taucs_ccs_matrix* A) {

	std::ofstream fileout(file);

	int current = 0;
	for (int i1 = 1; i1 < A->m; i1++) {
		int rowEnd = A->colptr[i1];
		double next = (double)A->values.d[current];
		double x = A->rowind[current];

		for (int i1 = 0; i1 < A->n; i1++) {
			if (i1 != x) fileout << "0 ";
			else {
				fileout << next << " ";
				current++;
				next = A->values.d[current];
				x = A->rowind[current];

				if (current == rowEnd) {
					for (; i1 < A->n; i1++) fileout << "0 ";
				}
			}
		}
		fileout << std::endl;
	}

	fileout.close();
}

//******************************************************************************************

class IntegerDoublePair {
public:
	inline IntegerDoublePair(int id_, double val_) : id(id_), val(val_) {}
	int id;
	double val;
	inline bool operator<(const IntegerDoublePair& other) const {
		return (id < other.id);
	}
};

//******************************************************************************************

void computeUrshapeWeightsUniform(SimpleMesh* urshape) {
	for (UInt i1 = 0; i1 < urshape->eList.size(); i1++) {
		urshape->eList[i1]->cotangent_weight = 1.0;
	}
	for (UInt i1 = 0; i1 < urshape->vList.size(); i1 ++) {
		urshape->vList[i1].param.x = urshape->vList[i1].getNumN();
	}
}

//******************************************************************************************

void computeUrshapeWeightsPositive(SimpleMesh* urshape) {
	// Compute cotangent weights of urshape:
	for (UInt i1 = 0; i1 < urshape->eList.size(); i1++) {
		urshape->eList[i1]->computeCotangentWeights();
		if (urshape->eList[i1]->cotangent_weight < 0) urshape->eList[i1]->cotangent_weight = 0;
	}
	for (UInt i1 = 0; i1 < urshape->vList.size(); i1 ++) {
		double sum = 0;
		for (UInt i2 = 0; i2 < urshape->vList[i1].getNumN(); i2++) sum += urshape->vList[i1].eList[i2]->cotangent_weight;
		urshape->vList[i1].param.x = sum;
	}
}

//******************************************************************************************

void computeUrshapeWeights(SimpleMesh* urshape) {
	// Compute cotangent weights of urshape:
	for (UInt i1 = 0; i1 < urshape->eList.size(); i1++) {
		urshape->eList[i1]->computeCotangentWeights();
	}
	for (UInt i1 = 0; i1 < urshape->vList.size(); i1 ++) {
		double sum = 0;
		for (UInt i2 = 0; i2 < urshape->vList[i1].getNumN(); i2++) sum += urshape->vList[i1].eList[i2]->cotangent_weight;
		urshape->vList[i1].param.x = sum;
	}
}

//******************************************************************************************

void computeUrshapeWeightsChordal(SimpleMesh* urshape) {
	// Compute cotangent weights of urshape:
	for (UInt i1 = 0; i1 < urshape->eList.size(); i1++) {
		urshape->eList[i1]->cotangent_weight = urshape->eList[i1]->getLength();
	}
	for (UInt i1 = 0; i1 < urshape->vList.size(); i1 ++) {
		double sum = 0;
		for (UInt i2 = 0; i2 < urshape->vList[i1].getNumN(); i2++) sum += urshape->vList[i1].eList[i2]->cotangent_weight;
		urshape->vList[i1].param.x = sum;
	}
}

//******************************************************************************************

double getEDef(SimpleMesh* sm, SimpleMesh* urshape, SquareMatrixND<vec3d>* rots) {
	double EDef = 0;

	UInt numOfPoints = (UInt)sm->getNumV();
	for (UInt i1 = 0; i1 < numOfPoints; i1++) {
		double Efan = 0;
		const vec3d& p0 = sm->vList[i1].c;
		const vec3d& p_dash_0 = urshape->vList[i1].c;
		const SquareMatrixND<vec3d>& R = rots[i1];

		for (UInt i2 = 0; i2 < sm->vList[i1].getNumN(); i2++) {
			int other = sm->vList[i1].getNeighbor(i2, i1);
			const vec3d& p1 = sm->vList[other].c;
			const vec3d& p_dash_1 = urshape->vList[other].c;

			vec3d v = p0-p1;
			vec3d v_dash = p_dash_0-p_dash_1;

			vec3d trans = R.vecTrans(v);
			Efan += fabs(urshape->vList[i1].eList[i2]->cotangent_weight * trans.squaredDist(v_dash));
			//Efan += trans.squaredDist(p0-p1);
		}
		EDef += Efan;
	}

	return EDef;
}

//******************************************************************************************

bool checkForTaucsError(int i) {

	if (i != TAUCS_SUCCESS) {
		std::cerr << "TAUCS error." << std::endl;
	
		if (i==TAUCS_ERROR)				std::cerr << "Generic error." << std::endl;
		if (i==TAUCS_ERROR_NOMEM)		std::cerr << "NOMEM error." << std::endl;
		if (i==TAUCS_ERROR_BADARGS)		std::cerr << "BADARGS error." << std::endl;
		if (i==TAUCS_ERROR_MAXDEPTH)	std::cerr << "MAXDEPTH error." << std::endl;
		if (i==TAUCS_ERROR_INDEFINITE)	std::cerr << "NOT POSITIVE DEFINITE error." << std::endl;

		return false;
	}

	return true;
}

//******************************************************************************************

#ifndef USENOTAUCS

//******************************************************************************************

struct PrecomputedMatrix {
	std::vector<double> entries;
	std::vector<int> row_index;
	std::vector<int> col_ptr;
	taucs_ccs_matrix  A;
	void* factorization;
	int* permutation;
	int* inversePermutation;
	taucs_ccs_matrix* permutedMatrix;
};

//******************************************************************************************

PrecomputedMatrix* ARAP_computeMatrix(SimpleMesh* sm, SimpleMesh* urshape) {

	PrecomputedMatrix* pm = new PrecomputedMatrix;

	std::cerr << pm << std::endl;

	UInt numOfPoints = (UInt)sm->getNumV();

	int* vertexLookup = new int[numOfPoints];
	int unconstrained = 0;
	for (UInt i1 = 0; i1 < numOfPoints; i1++) {
		vertexLookup[i1] = unconstrained;
		if (!sm->vList[i1].bool_flag) {
			unconstrained++;
		}
	}

	std::cerr << "unconstrained points: " << unconstrained << std::endl;

	pm->col_ptr.push_back(0);
	int cnt = 0;
	for (UInt i1 = 0; i1 < numOfPoints; i1++) {
		if (!sm->vList[i1].bool_flag) { // point is unconstrained
			std::set<IntegerDoublePair> ents;
			ents.insert( IntegerDoublePair(cnt, urshape->vList[i1].param.x) );
			for (UInt i2 = 0; i2 < sm->vList[i1].eList.size(); i2++) {
				Edge* e = sm->vList[i1].eList[i2];
				int other = e->getOther(i1);
				if (!sm->vList[other].bool_flag) { // other one is unconstrained, instert into system matrix
					ents.insert( IntegerDoublePair(vertexLookup[other], -urshape->vList[i1].eList[i2]->cotangent_weight) );
				}
			}
			for (std::set<IntegerDoublePair>::const_iterator it = ents.begin(); it != ents.end(); ++it) {
				if (it->id >= cnt) {
					pm->entries.push_back(it->val);
					pm->row_index.push_back(it->id);
				}
			}
			pm->col_ptr.push_back((int)pm->entries.size());
			cnt++;
		}
	}

	// create TAUCS matrix
	pm->A.n = cnt;
	pm->A.m = cnt;
	pm->A.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
	pm->A.colptr = &pm->col_ptr[0];
	pm->A.rowind = &pm->row_index[0];
	pm->A.values.d = &pm->entries[0];

	// Compute permutation
	taucs_ccs_order(&pm->A, &pm->permutation, &pm->inversePermutation, "metis"); // there was "amd" here, but that doesn't work with x64
	// Compute permuted matrix template
	taucs_ccs_matrix* matrixTemplatePermuted = taucs_ccs_permute_symmetrically(&pm->A, pm->permutation, pm->inversePermutation);
	// Compute symbolic factorization of the matrix template
	pm->factorization = taucs_ccs_factor_llt_symbolic(matrixTemplatePermuted);
	// Compute permutation of the matrix
	pm->permutedMatrix = taucs_ccs_permute_symmetrically(&pm->A, pm->permutation, pm->inversePermutation);
	// Compute numeric factorization of the matrix
	taucs_ccs_factor_llt_numeric(pm->permutedMatrix, pm->factorization);

	delete matrixTemplatePermuted;

	delete[] vertexLookup;

	return pm;
}

//******************************************************************************************

void  ARAP_Deform_with_given_factorization(SimpleMesh* sm, SimpleMesh* urshape, PrecomputedMatrix* pm, UInt numInnerIter = 20) {

	UInt numOfPoints = (UInt)sm->getNumV();

	//JBS_Timer t;

	//t.log("build lookup table");
	int* vertexLookup = new int[numOfPoints];
	int unconstrained = 0;
	for (UInt i1 = 0; i1 < numOfPoints; i1++) {
		vertexLookup[i1] = unconstrained;
		if (!sm->vList[i1].bool_flag) {
			unconstrained++;
		}
	}

	//std::cerr << unconstrained << " of " << numOfPoints << " unconstrained" << std::endl;

	//t.log("alloc memory");
	std::vector< SquareMatrixND<vec3d> > rots_vec(numOfPoints);
	SquareMatrixND<vec3d>* rots = &rots_vec[0];
	std::vector< double > rhs_vec(3*numOfPoints);
	double* rhs_x = &rhs_vec[0];
	double* rhs_y = &rhs_vec[numOfPoints];
	double* rhs_z = &rhs_vec[2*numOfPoints];

	std::vector<double> permutedRightHandSide_vec(numOfPoints);
	std::vector<double> permutedVariables_vec(numOfPoints);
	double* permutedRightHandSide	= &permutedRightHandSide_vec[0];
	double* permutedVariables		= &permutedVariables_vec[0];

	std::vector<double> x_vec(3*numOfPoints);
	double* x_x = &x_vec[0];
	double* x_y = &x_vec[numOfPoints];
	double* x_z = &x_vec[2*numOfPoints];


//#define WRITE_LOG
#ifdef WRITE_LOG
	std::ofstream file1("before.txt");
	std::ofstream file2("after.txt");
#endif


	for (UInt ARAP_iter = 0; ARAP_iter < numInnerIter; ARAP_iter++) {
		//**********************************************************
		// Find ideal rotations
		//**********************************************************
		//std::cerr << "estimating rotations..." << "                                \r";
		//t.log("estimating rotations");
#pragma omp parallel for
		for (int i2 = 0; i2 < (int)numOfPoints; i2++) {
			//if (sm->vList[i2].bool_flag) continue;
			SquareMatrixND<vec3d> COV;
			UInt v0 = i2;
			for (UInt i3 = 0; i3 < (UInt)urshape->vList[v0].getNumN(); i3++) {
				UInt v1 = urshape->vList[v0].getNeighbor(i3, v0);
				SquareMatrixND<vec3d> tmp;
				const vec3d& p_i = urshape->vList[v0].c;
				const vec3d& p_j = urshape->vList[v1].c;
				const vec3d& p_dash_i = sm->vList[v0].c;
				const vec3d& p_dash_j = sm->vList[v1].c;

				tmp.addFromTensorProduct((p_i-p_j) , (p_dash_i-p_dash_j));
				const double& w_ij = urshape->vList[v0].eList[i3]->cotangent_weight;
				tmp *= w_ij;
				COV += tmp;
			}
			//COV.print();
			SquareMatrixND<vec3d> U, V;
			vec3d sigma;
			bool SVD_result = COV.SVD_decomp(U, sigma, V);
			if (SVD_result) {

				V.transpose();
				SquareMatrixND<vec3d> rot = U*V;

				double det = rot.getDeterminant();
				if (det < 0) {
					int sm_id = 0;
					double smallest = sigma[sm_id];
					for (UInt dd = 1; dd < 3; dd++) {
						if (sigma[dd] < smallest) {
							smallest = sigma[dd];
							sm_id = dd;
						}
					}
					// flip sign of entries in colums 'sm_id' in 'U'
					U.m[sm_id + 0] *= -1;
					U.m[sm_id + 3] *= -1;
					U.m[sm_id + 6] *= -1;
					rot = U*V;
				}
				rot.transpose();
				rots[i2] = rot;
			} else { // SVD failed!
				std::cerr << "SVD failed" << std::endl;
				rots[i2].setToIdentity();
			}

			//if (i2 == 10) exit(1);
			//std::cerr << "---" << std::endl;
			//rots[i2].print();
		}

#ifdef WRITE_LOG
		double EDef = getEDef(sm, urshape, rots);
		file1 << ARAP_iter << " " << EDef << std::endl;
#endif

		//**********************************************************
		// Find vertex positions which match rotations best
		//**********************************************************
		//t.log("build rhs");
		int cnt = 0;
//#pragma omp parallel for
		for (int i1 = 0; i1 < (int)numOfPoints; i1++) {
			if (!sm->vList[i1].bool_flag) { // point is unconstrained:
				vec3d rhs(0,0,0);
				//vec3d p_i = sm->vList[i1].c;

				const vec3d& p_0 = urshape->vList[i1].c;

				for (UInt i2 = 0; i2 < sm->vList[i1].eList.size(); i2++) {
					Edge* e = sm->vList[i1].eList[i2];
					const int other = e->getOther(i1);

					const double& w_ij = urshape->vList[i1].eList[i2]->cotangent_weight;

					if (sm->vList[other].bool_flag) { // other one is constrained, put to right side
						vec3d tmp = sm->vList[other].c;
						tmp *= w_ij;
						rhs += tmp;
					}
					//! Right-hand side
					SquareMatrixND<vec3d> rot = rots[i1]+rots[other];
					const vec3d& p_1 = urshape->vList[other].c;
					vec3d t = rot.vecTrans(p_0-p_1);
					//vec3d t = rot.vecTrans(sm->vList[i1].c-sm->vList[other].c);			// <- This does not work well in AniTransplant! Seems to be wrong, doesn't work in ARAP_Gui either
					t *= (w_ij / 2.0);
					rhs += t;
				}
				rhs_x[cnt] = rhs.x;
				rhs_y[cnt] = rhs.y;
				rhs_z[cnt] = rhs.z;
				cnt++;
			}
		}

		//t.log("solve");
		// permutate right hand side
		taucs_vec_permute(cnt, TAUCS_DOUBLE, rhs_x, permutedRightHandSide, pm->permutation);
		// Solve for the unknowns
		taucs_supernodal_solve_llt(pm->factorization, permutedVariables, permutedRightHandSide);
		// Compute inverse permutation of the unknowns
		taucs_vec_permute(cnt, TAUCS_DOUBLE, permutedVariables, x_x, pm->inversePermutation);

		taucs_vec_permute(cnt, TAUCS_DOUBLE, rhs_y, permutedRightHandSide, pm->permutation);
		taucs_supernodal_solve_llt(pm->factorization, permutedVariables, permutedRightHandSide);
		taucs_vec_permute(cnt, TAUCS_DOUBLE, permutedVariables, x_y, pm->inversePermutation);

		taucs_vec_permute(cnt, TAUCS_DOUBLE, rhs_z, permutedRightHandSide, pm->permutation);
		taucs_supernodal_solve_llt(pm->factorization, permutedVariables, permutedRightHandSide);
		taucs_vec_permute(cnt, TAUCS_DOUBLE, permutedVariables, x_z, pm->inversePermutation);


		//t.log("update mesh vertices");
#pragma omp parallel for
		for (int i1 = 0; i1 < (int)numOfPoints; i1++) if (!sm->vList[i1].bool_flag) {
			const int& id = vertexLookup[i1];
			sm->vList[i1].c.x = x_x[id];
			sm->vList[i1].c.y = x_y[id];
			sm->vList[i1].c.z = x_z[id];
		}

#ifdef WRITE_LOG
		EDef = getEDef(sm, urshape, rots);
		file2 << ARAP_iter << " " << EDef << std::endl;
#endif

	}
	//t.print();

	delete[] vertexLookup;

}

//******************************************************************************************

void ARAP_Deform2(SimpleMesh* sm, SimpleMesh* urshape, UInt numInnerIter = 20) {

	PrecomputedMatrix* pm = ARAP_computeMatrix(sm, urshape);
	ARAP_Deform_with_given_factorization(sm, urshape, pm, numInnerIter);

}

//******************************************************************************************

void ARAP_Deform(SimpleMesh* sm, SimpleMesh* urshape, UInt numInnerIter = 20) {

	UInt numOfPoints = (UInt)sm->getNumV();
	SquareMatrixND<vec3d>* rots = new SquareMatrixND<vec3d>[numOfPoints];

	int* vertexLookup = new int[numOfPoints];
	int unconstrained = 0;
	for (UInt i1 = 0; i1 < numOfPoints; i1++) {
		vertexLookup[i1] = unconstrained;
		if (!sm->vList[i1].bool_flag) {
			unconstrained++;
		}
	}

	// allocate TAUCS solution vector
	std::vector<double> xv(unconstrained*3);
	taucs_double* x = &xv[0]; // the unknown vector to solve Ax=b
	std::vector<double> right_hand_sides(unconstrained*3); // right-hand size vector object
	double* rhs_x = &right_hand_sides[0];
	double* rhs_y = &right_hand_sides[unconstrained];
	double* rhs_z = &right_hand_sides[unconstrained*2];

	// build system matrix
	std::vector<double> entries;
	std::vector<int> row_index;
	std::vector<int> col_ptr;
	col_ptr.push_back(0);
	int cnt = 0;
	for (UInt i1 = 0; i1 < numOfPoints; i1++) {
		if (!sm->vList[i1].bool_flag) { // point is unconstrained
			std::set<IntegerDoublePair> ents;
			ents.insert( IntegerDoublePair(cnt, urshape->vList[i1].param.x) );
			vec3d p_i = sm->vList[i1].c;
			for (UInt i2 = 0; i2 < sm->vList[i1].eList.size(); i2++) {
				Edge* e = sm->vList[i1].eList[i2];
				int other = e->getOther(i1);
				if (!sm->vList[other].bool_flag) { // other one is unconstrained, instert into system matrix
					ents.insert( IntegerDoublePair(vertexLookup[other], -urshape->vList[i1].eList[i2]->cotangent_weight) );
				}
			}
			for (std::set<IntegerDoublePair>::const_iterator it = ents.begin(); it != ents.end(); ++it) {
				if (it->id >= cnt) {
					entries.push_back(it->val);
					row_index.push_back(it->id);
				}
			}
			col_ptr.push_back((int)entries.size());
			cnt++;
		}
	}

	// create TAUCS matrix
	taucs_ccs_matrix  A;
	A.n = cnt;
	A.m = cnt;
	A.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
	A.colptr = &col_ptr[0];
	A.rowind = &row_index[0];
	A.values.d = &entries[0];

	void* F = NULL;
	char* options[] = {"taucs.factor.LLT=true", NULL};
	void* opt_arg[] = { NULL };
	// Pre-factor matrix
	//taucs_logfile("stdout");
	int i = taucs_linsolve(&A,&F,0,NULL,NULL,options,NULL);
	checkForTaucsError(i);

	for (UInt ARAP_iter = 0; ARAP_iter < numInnerIter; ARAP_iter++) {
		//**********************************************************
		// Find ideal rotations
		//**********************************************************
		std::cerr << "estimating rotations..." << "                                \r";
		for (UInt i2 = 0; i2 < numOfPoints; i2++) {
			SquareMatrixND<vec3d> COV;
			UInt v0 = i2;
			for (UInt i3 = 0; i3 < (UInt)urshape->vList[v0].getNumN(); i3++) {
				UInt v1 = urshape->vList[v0].getNeighbor(i3, v0);
				SquareMatrixND<vec3d> tmp;
				const vec3d& p_i = urshape->vList[v0].c;
				const vec3d& p_j = urshape->vList[v1].c;
				const vec3d& p_dash_i = sm->vList[v0].c;
				const vec3d& p_dash_j = sm->vList[v1].c;
				tmp.addFromTensorProduct((p_i-p_j) , (p_dash_i-p_dash_j));
				const double& w_ij = urshape->vList[v0].eList[i3]->cotangent_weight;
				tmp *= w_ij;
				COV += tmp;
			}
			//COV.print();
			SquareMatrixND<vec3d> U, V;
			vec3d sigma;
			bool SVD_result = COV.SVD_decomp(U, sigma, V);
			if (SVD_result) {

				V.transpose();
				SquareMatrixND<vec3d> rot = U*V;

				double det = rot.getDeterminant();
				if (det < 0) {
					int sm_id = 0;
					double smallest = sigma[sm_id];
					for (UInt dd = 1; dd < 3; dd++) {
						if (sigma[dd] < smallest) {
							smallest = sigma[dd];
							sm_id = dd;
						}
					}
					// flip sign of entries in colums 'sm_id' in 'U'
					U.m[sm_id + 0] *= -1;
					U.m[sm_id + 3] *= -1;
					U.m[sm_id + 6] *= -1;
					rot = U*V;
				}
				rots[i2] = rot;
			} else { // SVD failed!
				std::cerr << "SVD failed" << std::endl;
				rots[i2].setToIdentity();
			}
		}

		double EDef = getEDef(sm, urshape, rots);
		std::cerr << "EDef: " << EDef << std::endl;

		//**********************************************************
		// Find vertex positions which match rotations best
		//**********************************************************
		int cnt = 0;
		for (UInt i1 = 0; i1 < numOfPoints; i1++) {
			if (!sm->vList[i1].bool_flag) { // point is unconstrained:
				vec3d rhs(0,0,0);
				//vec3d p_i = sm->vList[i1].c;

				for (UInt i2 = 0; i2 < sm->vList[i1].eList.size(); i2++) {
					Edge* e = sm->vList[i1].eList[i2];
					int other = e->getOther(i1);
					vec3d p_j = sm->vList[other].c;

					const double& w_ij = urshape->vList[i1].eList[i2]->cotangent_weight;

					if (sm->vList[other].bool_flag) { // other one is constrained, put to right side
						vec3d tmp = p_j;
						tmp *= w_ij;
						rhs += tmp;
					}
					//! Right-hand side
					SquareMatrixND<vec3d> rot = rots[i1]+rots[other];
					vec3d t = rot.vecTrans(urshape->vList[i1].c-urshape->vList[other].c);
					t *= (w_ij / 2.0);
					rhs += t;
				}
				rhs_x[cnt] = rhs.x;
				rhs_y[cnt] = rhs.y;
				rhs_z[cnt] = rhs.z;
				cnt++;
			}
		}

		// solve the linear system
		i = taucs_linsolve(&A,&F,3,x,(taucs_double*)&right_hand_sides[0],options,NULL);
		checkForTaucsError(i);
		for (UInt i1 = 0; i1 < numOfPoints; i1++) if (!sm->vList[i1].bool_flag) {
			sm->vList[i1].c.x = x[vertexLookup[i1]];
			sm->vList[i1].c.y = x[unconstrained+vertexLookup[i1]];
			sm->vList[i1].c.z = x[2*unconstrained+vertexLookup[i1]];
		}
	}

	//free the factorization
	i = taucs_linsolve(NULL,&F,0,NULL,NULL,NULL,NULL);


	delete[] rots;

}
#endif

#endif
