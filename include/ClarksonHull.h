#ifndef CLARKSON_HULL
#define CLARKSON_HULL


#include <vector>
#include "point4d.h"
#include "JBS_General.h"


struct Triple {

	Triple() {}

	Triple(UInt v0_, UInt v1_, UInt v2_) {
		v[0] = v0_;
		v[1] = v1_;
		v[2] = v2_;
	}

	void print() {
		std::cerr << v[0] << " " << v[1] << " " << v[2] << " " << std::endl;
	}

	UInt v[3];
};

struct Quadruple {

	Quadruple() {}

	Quadruple(UInt v0_, UInt v1_, UInt v2_, UInt v3_) {
		v[0] = v0_;
		v[1] = v1_;
		v[2] = v2_;
		v[3] = v3_;
	}

	void print() {
		std::cerr << v[0] << " " << v[1] << " " << v[2] << " " << v[3] << std::endl;
	}

	UInt v[4];
};

std::vector<Quadruple> buildConvexHull(vec4d* pts_, UInt pts_size_);
std::vector<Triple> buildConvexHull(vec3d* pts_, UInt pts_size_);



#endif

