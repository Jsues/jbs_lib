#ifndef CIRCLE_FITTER_H
#define CIRCLE_FITTER_H

#include <vector>
#include "point2d.h"
#include "MatrixnD.h"
//#include "SVD_Matrix.h"
#include "volume.h"
#include "FileIO/VolumeIO.h"

template <class Point>
struct Circle {
	Point center;
	typename Point::ValueType r;

	void print() const {
		std::cerr << "Circle with center " << center.x << "/" << center.y << " and radius " << r << std::endl;
	}
};



template <class Point>
Circle<Point> fitCircle(const std::vector<Point>& pts) {

	typedef typename Point::ValueType ValueType;

	int dx = 64;
	int dy = dx;
	int dz = dx/2;

	Volume<int> vol(vec3f(-1,-1,0), vec3f(1,1,1), dx, dy, dz, 1);

	int numPts = (int)pts.size();
	int numInnerSamples = (int)sqrt((double)pts.size());
	vol.init();
	vol.zeroOutMemory();

	srand(0);

	for (int i1 = 0; i1 < numPts; i1++) {
		int v0 = i1;
		const Point& p0 = pts[v0];
		for (int i2 = 0; i2 < numInnerSamples; i2++) {
			int v1 = rand()%numPts;
			int v2 = rand()%numPts;

			const Point& p1 = pts[v1];
			const Point& p2 = pts[v2];

			Point e0 = p1-p0;
			Point e1 = p2-p0;
			Point s0 = (p0+p1)/2;
			Point s1 = (p0+p2)/2;
			Point d0(e0.y, -e0.x);
			Point d1(e1.y, -e1.x);
			d0.normalize();
			d1.normalize();

			ValueType a =  d0.x;
			ValueType b = -d1.x;
			ValueType c =  d0.y;
			ValueType d = -d1.y;

			SquareMatrixND<Point> A;
			A(0,0) = a;
			A(0,1) = b;
			A(1,0) = c;
			A(1,1) = d;

			Point rhs = s1-s0;

			ValueType det = A.getDeterminant();
			SquareMatrixND<Point> A2;
			A2(0,0) = d;
			A2(0,1) = -b;
			A2(1,0) = -c;
			A2(1,1) = a;
			A2 /= det;
			Point res = A2.vecTrans(rhs);

			if (res.x != res.x || res.y != res.y) continue;

			Point center = s0 + d0*res.x;

			ValueType radius = center.dist(p0);

			if (center.x < -1 || center.y < -1 || center.x > 1 || center.y > 1 || radius > 1) continue;
			vec3i pos = vol.worldPosToIndices(vec3f((float)center.x, (float)center.y, (float)radius));
			vol.add(pos.x, pos.y, pos.z, 1);
		}
	}

	int maxVal, minVal;
	vol.computeMinMaxValues(minVal, maxVal);
	//std::cerr << "[" << minVal << " - " << maxVal << "]" << std::endl;

	//Volume<char> vol_vis(vec3f(-1,-1,0), vec3f(1,1,1), dx, dy, dz, 1);
	//vol_vis.init();
	//for (int i1 = 0; i1 < 32*32*8; i1++) {
	//	float val = (float)vol.get(i1);
	//	val /= maxVal;
	//	val *= 255;
	//	//if (val > 10) val = 255;
	//	char valNew = (char) val;
	//	vol_vis.set(i1, valNew);
	//}
	//VolumeWriter<char> vw(&vol_vis);
	//vw.writeRAWFilePSP("vol3.raw");


	//vec3i ppp = findMultiResMaximum(&vol);
	//std::cerr << "Multires: " << ppp << std::endl;
	vec3i ppp = vol.getPosOfMaxEntry();
	//std::cerr << "Singleres: " << ppp << std::endl;
	vec3f pppp((float)ppp.x, (float)ppp.y, (float)ppp.z);
	pppp += vec3f(0.5f);
	vec3f pos_f = vol.pos(pppp.x, pppp.y, pppp.z);
	//std::cerr << "Circle with center " << pos_f.x << "/" << pos_f.y << " and radius " << pos_f.z << std::endl;

	Circle<Point> circ;
	circ.center = Point(pos_f.x, pos_f.y);
	circ.r = pos_f.z;


	// refine radius:
	ValueType radJitter = (ValueType)1.0/dz;
	ValueType avgRad = 0;
	int cnt = 0;
	for (int i1 = 0; i1 < numPts; i1++) {
		int v0 = i1;
		const Point& p0 = pts[v0];
		ValueType rad = p0.dist(circ.center);
		if (fabs(rad-circ.r) < radJitter) {
			cnt++;
			avgRad += rad;
		}
	}
	avgRad /= cnt;
	circ.r = avgRad;
	//std::cerr << "Circle with center " << pos_f.x << "/" << pos_f.y << " and radius " << avgRad << std::endl;



	return circ;

}

#endif
