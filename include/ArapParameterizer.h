#ifndef ARAP_PARAMETERIZER_H
#define ARAP_PARAMETERIZER_H

#include "point2d.h"
#include "point3d.h"
#include "MatrixnD.h"


#include "SimpleMesh.h"

#include <vector>

#ifndef NO_TAUCS
extern "C" {
#include <taucs.h>
}
#include "taucs_tools.h"
#endif

#ifndef NO_UMFPACK
#include "umfpack.h"
#endif

//#define USE_ASAP

//#define USE_UNIFORM_WEIGHTS
//#define USE_LU_FACTORIZATION

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

#ifndef NO_TAUCS
bool checkForTaucsError(int i) {

	//std::cerr << "checkForTaucsError" << std::endl;


	if (i != TAUCS_SUCCESS) {
		//std::cerr << "Solution error." << std::endl;
	
		if (i==TAUCS_ERROR)				std::cerr << "Generic error." << std::endl;
		if (i==TAUCS_ERROR_NOMEM)		std::cerr << "NOMEM error." << std::endl;
		if (i==TAUCS_ERROR_BADARGS)		std::cerr << "BADARGS error." << std::endl;
		if (i==TAUCS_ERROR_MAXDEPTH)	std::cerr << "MAXDEPTH error." << std::endl;
		if (i==TAUCS_ERROR_INDEFINITE)	std::cerr << "NOT POSITIVE DEFINITE error." << std::endl;

		return false;
	} else {
		//std::cerr << "No taucs error found" << std::endl;
	}

	return true;
}
#endif


//******************************************************************************************
//******************************************************************************************
//******************************************************************************************

template <class ValueType>
class ArapParameterizer {

public:


//******************************************************************************************


	typedef point3d<ValueType> Point3D;

	typedef point3d<int> Tri;

	typedef point2d<ValueType> Point2D;

	typedef SquareMatrixND<Point2D> Mat2D;

	struct ParamTri {
		Point2D p0;
		Point2D p1;
		Point2D p2;
		ValueType w_01;
		ValueType w_02;
		ValueType w_12;
		const Point2D& getV(int i) const {
			if (i==0) return p0;
			if (i==1) return p1;
			if (i==2) return p2;
			else {
				std::cerr << "Out of bounds access" << std::endl;
				return p0;
			}
		}

		ValueType getWeightOfEdge(int i, int j) {
#ifdef USE_UNIFORM_WEIGHTS
			return 1;
#endif
			if (i == 0 && j == 1 || i == 1 && j == 0) {
				return w_01;
			}
			else if (i == 0 && j == 2 || i == 2 && j == 0) {
				return w_02;
			}
			else {
				return w_12;
			}
		}
	};


//******************************************************************************************


	ArapParameterizer(SimpleMesh* sm, const Point2D* param) : m_sm(sm) {

		m_numV = sm->getNumV();
		m_param = new Point2D[m_numV];
		for (int i1 = 0; i1 < m_numV; i1++) m_param[i1] = param[i1];

		m_numT = sm->getNumT();
		m_param_tris = new ParamTri[m_numT];
		for (int i1 = 0; i1 < m_numT; i1++) {
			Tri t(sm->tList[i1]->v0(), sm->tList[i1]->v1(), sm->tList[i1]->v2());
			const Point3D& p0 = sm->vList[t.x].c;
			const Point3D& p1 = sm->vList[t.y].c;
			const Point3D& p2 = sm->vList[t.z].c;

			ValueType length_p0_p1 = p0.dist(p1);
			ValueType square_length_p0_p2 = p0.squareDist(p2);
			m_param_tris[i1].p0 = Point2D(0, 0);
			m_param_tris[i1].p1 = Point2D(length_p0_p1, 0);

			ValueType angle = (p2-p0)|(p1-p0);

			ValueType x = angle/length_p0_p1;
			ValueType y = sqrt(square_length_p0_p2-x*x);

			ValueType angle2 = (p2-p0)|(p1-p0);
			ValueType angle_test = Point2D(x, y)|m_param_tris[i1].p1;

			if ((angle2 / angle_test) < 0) {
				y *= -1;
				std::cerr << angle2 << " " << angle_test << std::endl;
			}

			m_param_tris[i1].p2 = Point2D(x,y);

			Point3D e02 = p2-p0;
			Point3D e12 = p2-p1;
			Point3D e01 = p1-p0;
			m_param_tris[i1].w_01 = (e02|e12)/(e02^e12).length();
			m_param_tris[i1].w_12 = (e01|e02)/(e01^e02).length();
			m_param_tris[i1].w_02 = (-e12|e01)/(e12^e01).length();

			//if (t.x == 275 && t.y == 257) {
			//	std::cerr << "> " << m_param_tris[i1].w_01 << std::endl;
			//	sm->vList[t.x].eList.getEdge(t.x, t.y)->computeCotangentWeightsVERBOSE();
			//}
			//if (t.x == 275 && t.z == 257) {
			//	std::cerr << "> " << m_param_tris[i1].w_02 << std::endl;
			//	sm->vList[t.x].eList.getEdge(t.x, t.z)->computeCotangentWeightsVERBOSE();
			//}

			// Move triangle to barycenter:
			Point2D bary = m_param_tris[i1].p0+m_param_tris[i1].p1+m_param_tris[i1].p2;
			bary /= 3;
			m_param_tris[i1].p0 -= bary;
			m_param_tris[i1].p1 -= bary;
			m_param_tris[i1].p2 -= bary;

			//CheckTri(i1);
		}

		// Store the triangle ID of each triangle in the triangles intFlag:
		for (int i1 = 0; i1 < m_numT; i1++) {
			m_sm->tList[i1]->intFlag = i1;
		}

		m_Lt.resize(m_numT);
		m_rhs.resize(m_numV);

		buildSystem();
	}

	
//******************************************************************************************


	~ArapParameterizer() {
		delete[] m_param;
		delete[] m_param_tris;

#ifndef NO_TAUCS
		// free the factorization
		int i = taucs_linsolve(NULL,&m_F,0,NULL,NULL,NULL,NULL);
		delete m_F;
		checkForTaucsError(i);
#endif
	}


//******************************************************************************************


	void CheckTri(int i) {

		Tri t(m_sm->tList[i]->v0(), m_sm->tList[i]->v1(), m_sm->tList[i]->v2());
		const Point3D& x0 = m_sm->vList[t.x].c;
		const Point3D& x1 = m_sm->vList[t.y].c;
		const Point3D& x2 = m_sm->vList[t.z].c;

		const Point2D& p0 = m_param_tris[i].p0;
		const Point2D& p1 = m_param_tris[i].p1;
		const Point2D& p2 = m_param_tris[i].p2;

		std::cerr << "-----------------" << std::endl;
		std::cerr << x0.dist(x1)-p0.dist(p1) << std::endl;
		std::cerr << x0.dist(x2)-p0.dist(p2) << std::endl;
		std::cerr << x2.dist(x1)-p2.dist(p1) << std::endl;
		std::cerr << ((x0-x1)|(x0-x2)) - ((p0-p1)|(p0-p2)) << std::endl;
		std::cerr << ((x0-x1)|(x1-x2)) - ((p0-p1)|(p1-p2)) << std::endl;
		std::cerr << ((x0-x2)|(x1-x2)) - ((p0-p2)|(p1-p2)) << std::endl;
	}


//******************************************************************************************


	void buildSystem() {

		int numUnknown = m_numV-1;

		m_A_entries.reserve(numUnknown*7);	// each row has on average 6 entries (7 is a backup to reduce the re-allocate probability)
		m_A_row_index.reserve(numUnknown*7);	// each row has on average 6 entries
		m_A_col_ptr.reserve(numUnknown+1);
		m_A_col_ptr.push_back(0);

		// Vertex 0 is known to be (0,0)!!
		for (int i1 = 0; i1 < numUnknown; i1++) {
			int vertexID = i1+1;

			double sum_of_w = 0;
			std::set< std::pair<int, double> > neighbors;
			for (UInt i2 = 0; i2 < m_sm->vList[vertexID].getNumN(); i2++) {
				int n = m_sm->vList[vertexID].getNeighbor(i2, vertexID);
				//double w = m_sm->vList[vertexID].eList[i2]->tList.size();
				double w = m_sm->vList[vertexID].eList[i2]->cotangent_weight;
#ifdef USE_UNIFORM_WEIGHTS
				w = m_sm->vList[vertexID].eList[i2]->tList.size();
#endif
				//if (w < 0) {
				//	std::cerr << vertexID << " & " << n << "  ->  w: " << w << std::endl;
				//}

				sum_of_w += w;
				neighbors.insert(std::make_pair(n, -w));
			}
			int numN = neighbors.size();

			if (sum_of_w < 0)
				std::cerr << vertexID << "  ->  sumofweights: " << sum_of_w << std::endl;

			neighbors.insert(std::make_pair(vertexID, sum_of_w));
			for (std::set< std::pair<int, double> >::const_iterator it = neighbors.begin(); it != neighbors.end(); it++) {
				int nID = it->first;

#ifndef NO_TAUCS
				if (nID > vertexID) continue; // only store the upper half
#endif
				if (nID == 0) continue;

				double value = it->second;

				m_A_entries.push_back(value);
				m_A_row_index.push_back(nID-1);
			}
			m_A_col_ptr.push_back(m_A_entries.size());
		}

		//taucsMatrixToStreamPlain(m_A_entries, m_A_row_index, m_A_col_ptr, std::cerr);
		//std::ofstream fout("matrix.txt");
		//taucsMatrixToStream(m_A_entries, m_A_row_index, m_A_col_ptr, fout);

		// factor matrix only
		std::cerr << "factor matrix" << std::endl;
#ifndef NO_TAUCS
		// now solve for best positions of the shrink-warp mesh.
		m_A.n = numUnknown;
		m_A.m = numUnknown;
		m_A.colptr = &m_A_col_ptr[0];
		m_A.rowind = &m_A_row_index[0];
		m_A.values.d = &m_A_entries[0];
#ifdef USE_LU_FACTORIZATION
		m_A.flags = (TAUCS_DOUBLE);
		char* options[] = {"taucs.factor.LU=true", NULL};
#else
		m_A.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
		char* options[] = {"taucs.factor.LLT=true", NULL};
#endif
		//std::ofstream mat("test1.txt");
		//taucsMatrixToStream(m_A_entries, m_A_row_index, m_A_col_ptr, mat);
		//taucs_ccs_write_ijv(&m_A, "test.txt");
		m_F = NULL;
		void* opt_arg[] = { NULL };
		int i = taucs_linsolve(&m_A,&m_F,0,NULL,NULL,options,NULL);
		checkForTaucsError(i);
#endif

#ifndef NO_UMFPACK
		int result1 = umfpack_di_symbolic(numUnknown, numUnknown, &m_A_col_ptr[0], &m_A_row_index[0], &m_A_entries[0], &Symbolic, NULL, NULL);
		int result2 = umfpack_di_numeric(&m_A_col_ptr[0], &m_A_row_index[0], &m_A_entries[0], Symbolic, &Numeric, NULL, NULL);
#endif
		std::cerr << "done" << std::endl;

	}


//******************************************************************************************


	void doArapIter() {

		recomputeLts();

		// compute RHS
		int numUnknown = m_numV-1;

		// Vertex 0 is known to be (0,0)!!
		for (int i1 = 0; i1 < numUnknown; i1++) {
			int vertexID = i1+1;

			Point2D p(0,0);
			int numN = m_sm->vList[vertexID].getNumN();
			for (int i2 = 0; i2 < numN; i2++) {
				int neighborID = m_sm->vList[vertexID].getNeighbor(i2, vertexID);
				const Edge* const e = m_sm->vList[vertexID].eList[i2];
				for (int i3 = 0; i3 < (int)e->tList.size(); i3++) {
					const Triangle* const t = e->tList[i3];
					int tID = t->intFlag;
					int v_start = -1;
					int v_end = -1;
					for (int i4 = 0; i4 < 3; i4++) if (t->getV(i4) == vertexID) v_start = i4;
					for (int i4 = 0; i4 < 3; i4++) if (t->getV(i4) == neighborID) v_end = i4;

					if (v_start == -1 || v_end == -1) std::cerr << "something evil happened in ArapParameterizer::doArapIter()" << std::endl;

					double weight = m_param_tris[tID].getWeightOfEdge(v_start, v_end);

					p += (m_param_tris[tID].getV(v_start)-m_param_tris[tID].getV(v_end)) * weight;
				}
			}
			m_rhs[i1] = p;
		}

		std::vector<double> rhs;
		std::vector<double> x_vec;
		rhs.resize(numUnknown*2);
		x_vec.resize(numUnknown*2);

		// Change order of rhs from xyxyxy to xxxyyy
		for (int i1 = 0; i1 < numUnknown; i1++) {
			rhs[i1] = m_rhs[i1].x;
			rhs[i1+numUnknown] = m_rhs[i1].y;
		}

		// solve the linear system
		double* b = &rhs[0];
		double* x = &x_vec[0];
//#ifdef USE_LU_FACTORIZATION
//		char* options[] = {"taucs.factor.LU=true", NULL};
//#else
		char* options[] = {"taucs.factor.LLT=true", NULL};
//#endif
		std::cerr << "solve" << std::endl;
#ifndef NO_TAUCS
		int i = taucs_linsolve(&m_A,&m_F,2,x,b,options,NULL);
		checkForTaucsError(i);


		std::ofstream bout("b.txt");
		bout << "right hand side b:" << std::endl;
		for (int i1 = 0; i1 < numUnknown; i1++) {
			bout << b[i1] << std::endl;
		}

		std::ofstream xout("x.txt");
		xout << "solution x:" << std::endl;
		for (int i1 = 0; i1 < numUnknown; i1++) {
			xout << x[i1] << std::endl;
		}


#endif
#ifndef NO_UMFPACK
		umfpack_di_solve(UMFPACK_A, &m_A_col_ptr[0], &m_A_row_index[0], &m_A_entries[0], x, b, Numeric, NULL, NULL);
		double* bb = &rhs[numUnknown];
		double* xx = &x_vec[numUnknown];
		umfpack_di_solve(UMFPACK_A, &m_A_col_ptr[0], &m_A_row_index[0], &m_A_entries[0], xx, bb, Numeric, NULL, NULL);
#endif
		std::cerr << "done" << std::endl;


		// Set the new values:
		// Vertex 0 is known to be (0,0)!!
		m_param[0] = Point2D(0,0);
		for (int i1 = 0; i1 < numUnknown; i1++) {
			int vertexID = i1+1;
			m_param[vertexID] = Point2D(x[i1], x[i1+numUnknown]);
		}
	}


//******************************************************************************************


	void saveParamAsOFF(const char* filename = "iter.off") {
		std::ofstream fout(filename);
		fout << "OFF\n" << m_numV << " " << m_numT << " 0\n";

		for (int i1 = 0; i1 < m_numV; i1++) {
			fout << m_param[i1].x << " " << m_param[i1].y << " 0" << std::endl;
		}
		for (int i1 = 0; i1 < m_numT; i1++) {
			const Triangle* const t = m_sm->tList[i1];
			fout << "3 " << t->m_v0 << " " << t->m_v1 << " " << t->m_v2 << std::endl;
		}
		fout.close();

	}


//******************************************************************************************


	void saveTriParamAsOFF(bool move = false) {
		std::ofstream fout("triParam.off");
		fout << "OFF\n" << 3*m_numT << " " << m_numT << " 0\n";
		ValueType x_disp = 0;
		for (int i1 = 0; i1 < m_numT; i1++) {
			fout << m_param_tris[i1].p0.x+x_disp << " " << m_param_tris[i1].p0.y << " 0" << std::endl;
			fout << m_param_tris[i1].p1.x+x_disp << " " << m_param_tris[i1].p1.y << " 0" << std::endl;
			fout << m_param_tris[i1].p2.x+x_disp << " " << m_param_tris[i1].p2.y << " 0" << std::endl;

			if (move) x_disp += m_param_tris[i1].p1.x;
		}
		for (int i1 = 0; i1 < m_numT; i1++) {
			fout << "3 " << i1*3 << " " << i1*3+1 << " " << i1*3+2 << std::endl;
		}
		fout.close();
	}


//******************************************************************************************

	void PCA_align() {
		vec2d cog;
		for (int i1 = 0; i1 < m_numV; i1++) {
			cog += m_param[i1];
		}
		cog /= m_numV;

		Mat2D cov;
		for (int i1 = 0; i1 < m_numV; i1++) {
			Point2D v = m_param[i1]-cog;
			cov.addFromTensorProduct(v,v);
		}
		
		Mat2D ESystem;
		Point2D evals = cov.calcEValuesAndVectorsCORRECT(ESystem);
		ESystem.transpose();

		for (int i1 = 0; i1 < m_numV; i1++) {
			m_param[i1] = ESystem.vecTrans(m_param[i1]);
		}
	}

	
//******************************************************************************************

	template <class T>
	T tmin(T a, T b) {
		if (a<b) return a;
		return b;
	}
	
	template <class T>
	T tmax(T a, T b) {
		if (a>b) return a;
		return b;
	}
//******************************************************************************************


	void normalizeParam01() {
		Point2D minV(FLT_MAX,FLT_MAX);
		Point2D maxV(-FLT_MAX, -FLT_MAX);
		for (int i1 = 0; i1 < m_numV; i1++) {
			const Point2D& pt = m_param[i1];
			minV.x = tmin(minV.x, pt.x);
			maxV.x = tmax(maxV.x, pt.x);
			minV.y = tmin(minV.y, pt.y);
			maxV.y = tmax(maxV.y, pt.y);
		}

		std::cerr << "min: " << minV << "  ->  max: " << maxV << std::endl;
		ValueType fac = tmax(maxV.x-minV.x, maxV.y-minV.y);
		for (int i1 = 0; i1 < m_numV; i1++) {
			m_param[i1] -= minV;
			m_param[i1] /= fac;
		}

	}


//******************************************************************************************


	const Point2D* getParam() const {
		return m_param;
	}

private:

#ifndef NO_UMFPACK
    void *Symbolic, *Numeric;
#endif


//******************************************************************************************


	void recomputeLts() {

		for (int i1 = 0; i1 < m_numT; i1++) {

			const Point2D& x0 = m_param_tris[i1].p0;
			const Point2D& x1 = m_param_tris[i1].p1;
			const Point2D& x2 = m_param_tris[i1].p2;
			Point2D bary1 = (x0+x1+x2)/3;
			m_param_tris[i1].p0 -= bary1;
			m_param_tris[i1].p1 -= bary1;
			m_param_tris[i1].p2 -= bary1;

			Tri t(m_sm->tList[i1]->v0(), m_sm->tList[i1]->v1(), m_sm->tList[i1]->v2());

			const Point2D& u0 = m_param[t.x];
			const Point2D& u1 = m_param[t.y];
			const Point2D& u2 = m_param[t.z];

			Mat2D J;
#ifdef USE_UNIFORM_WEIGHTS
			J.addFromTensorProduct(u0-u1,x0-x1);
			J.addFromTensorProduct(u1-u2,x1-x2);
			J.addFromTensorProduct(u2-u0,x2-x0);
#else
			Mat2D TM0;
			TM0.addFromTensorProduct(u0-u1,x0-x1);
			TM0 *= m_param_tris[i1].getWeightOfEdge(0,1);
			Mat2D TM1;
			TM1.addFromTensorProduct(u1-u2,x1-x2);
			TM1 *= m_param_tris[i1].getWeightOfEdge(1,2);
			Mat2D TM2;
			TM2.addFromTensorProduct(u2-u0,x2-x0);
			TM2 *= m_param_tris[i1].getWeightOfEdge(2,0);
			J = TM0+TM1+TM2;
#endif


			const ValueType& a = J(0,0);
			const ValueType& b = J(0,1);
			const ValueType& c = J(1,0);
			const ValueType& d = J(1,1);

			Mat2D R;
			R(0,0) = a+d;
			R(0,1) = b-c;
			R(1,0) = c-b;
			R(1,1) = a+d;
			ValueType denom = sqrt((a+d)*(a+d) + (b-c)*(b-c));
			R /= denom;

			if ((a+d == 0) && (b-c == 0)) {
				std::cerr << "Arbitrary Rot allowed" << std::endl;
				R.setToIdentity();
			}

			Point2D bary;
			bary = u0+u1+u2;
			bary /= 3;

			Point2D x0new = R.vecTrans(x0);
			Point2D x1new = R.vecTrans(x1);
			Point2D x2new = R.vecTrans(x2);

//#ifdef USE_ASAP
//
//			// scale
//			ValueType lengthInParam = bary.dist(u0) + bary.dist(u1) + bary.dist(u2);
//			ValueType lengthInTri = bary1.dist(x0) + bary1.dist(x1) + bary1.dist(x2);
//
//			ValueType fac = lengthInParam/lengthInTri;
//			x0new *= fac;
//			x1new *= fac;
//			x2new *= fac;
//
//#endif

			m_param_tris[i1].p0 = bary+x0new;
			m_param_tris[i1].p1 = bary+x1new;
			m_param_tris[i1].p2 = bary+x2new;
		}
	}


//******************************************************************************************

	int m_numV;
	int m_numT;
	const SimpleMesh* m_sm;
	Point2D* m_param;
	ParamTri* m_param_tris;

#ifndef NO_TAUCS
	taucs_ccs_matrix  m_A; // a matrix to solve Ax=b in CCS format
	void* m_F;
#endif


	std::vector<double> m_A_entries;
	std::vector<int> m_A_row_index;
	std::vector<int> m_A_col_ptr;
	std::vector<Mat2D> m_Lt;
	std::vector<Point2D> m_rhs;
};

typedef ArapParameterizer<double> ArapParameterizerD;

#endif