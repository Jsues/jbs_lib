#ifndef ANIMATED_MESH_IV
#define ANIMATED_MESH_IV


#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>
#include <Inventor/nodes/SoTexture2.h>
#include <Inventor/nodes/SoTextureCoordinate2.h>
#include <Inventor/nodes/SoNormal.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoPendulum.h>


#include "AnimatedMesh.h"
#include "PointCloud4D.h"
#include "iv_stuff/fancy_materials.h"
#include "MeshToIV.h"
#include "rigid_transform.h"
#include "Timer.h"
#include "operators/computeVertexNormals.h"


class AnimatedMeshIV : public AnimatedMesh {

public:

	AnimatedMeshIV() : AnimatedMesh() {
		coinArraysPrepared = false;
	}

	~AnimatedMeshIV() {
		freeArrays();
	}

	void freeArrays() {
		for (UInt i1 = 0; i1 < frames_float.size(); i1++) delete[] frames_float[i1];
		for (UInt i1 = 0; i1 < frames_float_normals.size(); i1++) delete[] frames_float_normals[i1];
	}

	virtual void prepareCoinArrays() {

		frames_float.clear();
		frames_float_normals.clear();

		coinArraysPrepared = true;
		// Prepare coords as float for coin visualization
		for (UInt i1 = 0; i1 < frames.size(); i1++) {
			vec3f* tmp = new vec3f[basemesh->vList.size()];
			for (UInt i2 = 0; i2 < basemesh->vList.size(); i2++) {
				const vec3f& pt = frames[i1][i2];
				tmp[i2] = vec3f((float)pt.x, (float)pt.y, (float)pt.z);
			}
			frames_float.push_back(tmp);
		}
		std::cerr << "precompute normals";
		// Precompute normals for whole animation
		for (UInt i1 = 0; i1 < frames.size(); i1++) {
			for (UInt i2 = 0; i2 < basemesh->vList.size(); i2++) {
				basemesh->vList[i2].c = frames[i1][i2];
			}
			vec3d* normsdouble = computeVertexNormals(basemesh);
			vec3f* norms = new vec3f[basemesh->vList.size()];
			for (UInt i2 = 0; i2 < basemesh->vList.size(); i2++) {
				norms[i2] = -vec3f((float)normsdouble[i2].x, (float)normsdouble[i2].y, (float)normsdouble[i2].z);
			}
			delete[] normsdouble;
			frames_float_normals.push_back(norms);
		}
		std::cerr << "done" << std::endl;
	}

	virtual SoSeparator* getIVMesh(SoMaterial* mat = NULL, int dir = 0, vec3d displace = vec3d(0,0,0), bool useTexture = false, bool useColors = false, bool usePendulum = false, SimpleMesh* meshWithColor = NULL, float tex_factor = 1) {

		if (!coinArraysPrepared) prepareCoinArrays();

		SoSeparator* ivmesh = new SoSeparator;
		ivmesh->setName("meshToIv");

		SoShapeHints* sh = new SoShapeHints();
		//sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
		//sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
		sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
		sh->creaseAngle = 0.4f;
		ivmesh->addChild(sh);

		if (meshWithColor != NULL) {

			SoLightModel* lm = new SoLightModel();
			lm->model = SoLightModel::BASE_COLOR;
			ivmesh->addChild(lm);

			mat = new SoMaterial;
			int numPts = meshWithColor->getNumV();
			mat->diffuseColor.setNum(numPts);
			for (int i1 = 0; i1 < numPts; i1++) {
				vec3f c = meshWithColor->vList[i1].color;
				mat->diffuseColor.set1Value(i1, c.x, c.y, c.z);
			}

			ivmesh->addChild(mat);
			SoMaterialBinding* matbind = new SoMaterialBinding;
			matbind->value = SoMaterialBinding::PER_VERTEX_INDEXED;
			ivmesh->addChild(matbind);

			std::cerr << "using mesh colors" << std::endl;

		} else if (colors.size() == 0 || !useColors) {
			if (mat != NULL) ivmesh->addChild(mat);
		} else {
			std::cerr << "Using colors" << std::endl;
			SoMaterial* mat1 = JBSmaterials::matGray();
			SoMaterial* mat2 = JBSmaterials::matMediumBlue();
			SoMaterial* mat3 = JBSmaterials::matCoper();
			SoMaterial* mat = new SoMaterial;
			mat->diffuseColor.setNum(3);
			mat->diffuseColor.set1Value(0, mat1->diffuseColor[0]);
			mat->diffuseColor.set1Value(1, mat2->diffuseColor[0]);
			mat->diffuseColor.set1Value(2, mat3->diffuseColor[0]);
			mat->specularColor.setNum(3);
			mat->specularColor.set1Value(0, mat1->specularColor[0]);
			mat->specularColor.set1Value(1, mat2->specularColor[0]);
			mat->specularColor.set1Value(2, mat3->specularColor[0]);
			mat->ambientColor.setNum(3);
			mat->ambientColor.set1Value(0, mat1->ambientColor[0]);
			mat->ambientColor.set1Value(1, mat2->ambientColor[0]);
			mat->ambientColor.set1Value(2, mat3->ambientColor[0]);
			mat->shininess.setNum(2);
			mat->shininess.set1Value(0, mat1->shininess[0]);
			mat->shininess.set1Value(1, mat2->shininess[0]);
			mat->shininess.set1Value(2, mat3->shininess[0]);
			ivmesh->addChild(mat);
			SoMaterialBinding* matbind = new SoMaterialBinding;
			matbind->value = SoMaterialBinding::PER_VERTEX_INDEXED;
			ivmesh->addChild(matbind);
		}


		if (displace.squaredLength() > 0) {
			SoTranslation* t = new SoTranslation;
			t->translation.setValue(SbVec3f((float)displace.x, (float)displace.y, (float)displace.z));
			ivmesh->addChild(t);

			if (usePendulum) {
				SoPendulum* pend = new SoPendulum;
				SbRotation rot1(SbVec3f(1,0,0), 0.1f);
				SbRotation rot2(SbVec3f(1,0,0), -0.1f);
				pend->rotation0.setValue(rot1);
				pend->rotation1.setValue(rot2);
				pend->speed.setValue(0.1f);
				ivmesh->addChild(pend);
			}
		}


		UInt numV = basemesh->getNumV();


		if (useTexture) {

			SoMaterial* mat_blue = JBSmaterials::matLightBlue();
			ivmesh->addChild(mat_blue);
			SoMaterialBinding* matbind = new SoMaterialBinding;
			matbind->value = SoMaterialBinding::OVERALL;
			ivmesh->addChild(matbind);

			vec3d minV = basemesh->min;
			vec3d maxV = basemesh->max;
			vec3d diag = maxV-minV;
			double largest = diag.x;
			if (diag.y > largest) largest = diag.y;
			if (diag.z > largest) largest = diag.z;

			std::cerr << "largest: " << largest << std::endl;
			std::cerr << "minV: " << minV << std::endl;

			SbVec2f* texCoords = new SbVec2f[numV];
			// set texture coordinates:
			for (UInt i1 = 0; i1 < numV; i1++) {
				vec3d pos = basemesh->vList[i1].c;
				vec3d pos_norm = (pos-minV)/largest;

				pos_norm *= tex_factor;

				if (dir == 0) texCoords[i1] = SbVec2f((float)pos_norm.x, (float)pos_norm.y);
				else if (dir == 1) texCoords[i1] = SbVec2f((float)pos_norm.x, (float)pos_norm.z);
				else if (dir == 2) texCoords[i1] = SbVec2f((float)pos_norm.y, (float)pos_norm.z);
				else {
					double angle = -0.715;
					texCoords[i1] = SbVec2f((float)(cos(angle)*pos_norm.x-sin(angle)*pos_norm.y), (float)pos_norm.z);
				}
				//texCoords[i1][0] *= (float)1.6; // for weise
				//texCoords[i1][1] *= (float)1.6; // for weise
				texCoords[i1][0] *= (float)1.4; // for hand
				texCoords[i1][1] *= (float)1.4; // for hand
			}
			SoTexture2* texture = new SoTexture2();
			texture->filename.setValue("checkerboard.rgb");
			texture->model.setValue(SoTexture2::MODULATE);
			ivmesh->addChild(texture);

			SoTextureCoordinate2* texcoord = new SoTextureCoordinate2();
			texcoord->point.setValues(0, numV, texCoords);
			ivmesh->addChild(texcoord);
		}

		SoNormalBinding* nb = new SoNormalBinding;
		nb->value = SoNormalBinding::PER_VERTEX_INDEXED;
		int numPts = basemesh->getNumV();
		SbVec3f* Points = new SbVec3f[numPts];
		SbVec3f* Normals = new SbVec3f[numPts];
		for (int i1 = 0; i1 < numPts; i1++) {
			Points[i1][0] = frames_float[0][i1].x;
			Normals[i1][0] = frames_float_normals[0][i1].x;
			Points[i1][1] = frames_float[0][i1].y;
			Normals[i1][1] = frames_float_normals[0][i1].y;
			Points[i1][2] = frames_float[0][i1].z;
			Normals[i1][2] = frames_float_normals[0][i1].z;
		}
		coord3 = new SoCoordinate3;
		coord3->point.setValues(0, numPts, Points);
		ivmesh->addChild(coord3);
		delete[] Points;
		nns = new SoNormal;
		nns->vector.setValues(0, numPts, Normals);
		ivmesh->addChild(nns);
		delete[] Normals;

		//coordSelector->whichChild = 0;
		//ivmesh->addChild(coordSelector);

		// Create triangles

		SoIndexedFaceSet* ifs = new SoIndexedFaceSet();

		int numTris;

		if (meshWithColor != NULL) {
			numTris = meshWithColor->getNumT();
			ifs->coordIndex.setNum(numTris*4);
			for (int i1 = 0; i1 < numTris; i1++) {
				int v0 = meshWithColor->tList[i1]->v0();
				int v1 = meshWithColor->tList[i1]->v1();
				int v2 = meshWithColor->tList[i1]->v2();
				int32_t indices[] = {v0, v1, v2, -1 };
				ifs->coordIndex.setValues(i1*4, 4, indices);
			}
		} else {
			numTris = basemesh->getNumT();
			ifs->coordIndex.setNum(numTris*4);
			for (int i1 = 0; i1 < numTris; i1++) {
				int v0 = basemesh->tList[i1]->v0();
				int v1 = basemesh->tList[i1]->v1();
				int v2 = basemesh->tList[i1]->v2();
				int32_t indices[] = {v0, v1, v2, -1 };
				ifs->coordIndex.setValues(i1*4, 4, indices);
			}
		}


		if (colors.size() > 0) {
			// Create colors
			matIndex = &ifs->materialIndex;
			matIndex->setNum(numTris*5);
			for (int i1 = 0; i1 < numTris; i1++) {
				int v0 = basemesh->tList[i1]->v0();
				int v1 = basemesh->tList[i1]->v1();
				int v2 = basemesh->tList[i1]->v2();
				int m0 = colors[0][v0];
				int m1 = colors[0][v1];
				int m2 = colors[0][v2];
				int32_t mats[] = {m0, m1, m2, -1};
				matIndex->setValues(i1*4, 4, mats);
			}
		}

		ivmesh->addChild(ifs);

		return ivmesh;
	}

	void freeFramesDouble() {
		for (UInt i1 = 0; i1 < frames.size(); i1++) delete[] frames[i1];
	}

	virtual void showMeshNumber(UInt i) {

		//for (UInt i1 = 0; i1 < (UInt)basemesh->getNumV(); i1++) {
		//	vec3d v = frames_float[i][i1];
		//	coord3->point.set1Value(i1, SbVec3f((float)v.x, (float)v.y, (float)v.z));
		//}
		coord3->point.setValues(0, basemesh->getNumV(), (SbVec3f*)frames_float[i]);
		nns->vector.setValues(0, basemesh->getNumV(), (SbVec3f*)frames_float_normals[i]);

		//coordSelector->whichChild = i;

		//SbVec3f* cs = coord3->point.startEditing();
		//for (UInt i1 = 0; i1 < (UInt)basemesh->getNumV(); i1++) {
		//	const vec3f& v = frames_float[i][i1];
		//	cs[i1][0] = v.x;
		//	cs[i1][1] = v.y;
		//	cs[i1][2] = v.z;
		//}
		//coord3->point.finishEditing();

		/*
		if (colors.size() > 0) {
			int numTris = basemesh->getNumT();
			for (int i1 = 0; i1 < numTris; i1++) {
				int v0 = basemesh->tList[i1]->v0();
				int v1 = basemesh->tList[i1]->v1();
				int v2 = basemesh->tList[i1]->v2();
				int m0 = colors[i][v0];
				int m1 = colors[i][v1];
				int m2 = colors[i][v2];
				int32_t mats[] = {m0, m1, m2, -1};
				matIndex->setValues(i1*4, 4, mats);
			}
		}
		*/
	}

	//! Writes a binary file for coin.
	void writeBinaryFileForCoin(const char* filename, UInt maxFrames) {

		std::ofstream fout(filename, std::ofstream::binary);

		if (!fout.good()) {
			std::cerr << "Error writing file in 'AnimatedMeshIV::writeBinaryFileForCoin(char* filename)'" << std::endl;
			return;
		}

		//! Write header
		UInt numFrames = maxFrames;
		UInt numT = basemesh->getNumT();
		UInt numVperFrame = basemesh->getNumV();
		fout.write((char*)&numFrames, sizeof(UInt));
		fout.write((char*)&numT, sizeof(UInt));
		fout.write((char*)&numVperFrame, sizeof(UInt));

		//! Write points
		for (UInt i1 = 0; i1 < numFrames; i1++) {
			fout.write((char*)&frames_float[i1][0], sizeof(float)*3*numVperFrame);
		}
		//! Write normals
		for (UInt i1 = 0; i1 < numFrames; i1++) {
			fout.write((char*)&frames_float_normals[i1][0], sizeof(float)*3*numVperFrame);
		}

		//! Write triangles
		for (UInt i1 = 0; i1 < numT; i1++) {
			vec3i tri(basemesh->tList[i1]->v0(), basemesh->tList[i1]->v1(), basemesh->tList[i1]->v2());
			fout.write((char*)&tri, sizeof(int)*3);
		}
	}

	//! Returns the number of frames in the sequence.
	virtual UInt getNumFrames() const {
		if (frames_float.size() > 0)
			return (UInt) (frames_float.size()-1);
		return (UInt) frames.size();
	}

	//! Reads a binary file for coin.
	void readBinaryFileForCoin(const char* filename) {

		coinArraysPrepared = true;

		if (basemesh != NULL) delete basemesh;
		for (UInt i1 = 0; i1 < frames.size(); i1++) delete[] frames[i1];
		frames.clear();
		for (UInt i1 = 0; i1 < frames_float.size(); i1++) delete[] frames_float[i1];
		frames_float.clear();
		for (UInt i1 = 0; i1 < frames_float_normals.size(); i1++) delete[] frames_float_normals[i1];
		frames_float_normals.clear();


		std::ifstream fin(filename, std::ifstream::binary);

		if (!fin.good()) {
			std::cerr << "Error reading file in 'AnimatedMeshIV::readBinaryFileForCoin(char* filename)'" << std::endl;
			return;
		}

		// Get length of file
		fin.seekg (0, std::ios::end);
		long length = fin.tellg();
		fin.seekg (0, std::ios::beg);

		//! Read header
		UInt numFrames;
		UInt numT;
		UInt numVperFrame;
			
		fin.read((char*)&numFrames, sizeof(UInt));
		fin.read((char*)&numT, sizeof(UInt));
		fin.read((char*)&numVperFrame, sizeof(UInt));

		std::cerr << "numFrames: " << numFrames << " numT: " << numT << " numVperFrame: " << numVperFrame << std::endl;

		//UInt filesize = 3*sizeof(UInt) + 3*sizeof(float)*numFrames*numVperFrame + 3*sizeof(int)*numT;
		//std::cerr << "Expected filesize: " << filesize << std::endl;

		//! Read vertices;
		for (UInt i1 = 0; i1 < numFrames; i1++) {
			vec3f* pts = new vec3f[numVperFrame];
			fin.read((char*)pts, sizeof(float)*3*numVperFrame);
			frames_float.push_back(pts);
		}
		//! Read normals;
		for (UInt i1 = 0; i1 < numFrames; i1++) {
			vec3f* ns = new vec3f[numVperFrame];
			fin.read((char*)ns, sizeof(float)*3*numVperFrame);
			frames_float_normals.push_back(ns);
		}

		//! Read triangles;
		vec3i* tris = new vec3i[numT];
		fin.read((char*)tris, sizeof(int)*3*numT);


		long current_pos = fin.tellg();
		//std::cerr << current_pos << " " << length << std::endl;
		if (current_pos != length) { // Read colors
			for (UInt i1 = 0; i1 < numFrames; i1++) {
				int* cols = new int[numVperFrame];
				fin.read((char*)cols, sizeof(int)*numVperFrame);
				colors.push_back(cols);
			}
		}

		basemesh = new SimpleMesh;

		//! Fill first mesh:
		for (UInt i1 = 0; i1 < numVperFrame; i1++) {
			vec3d c(frames_float[0][i1].x, frames_float[0][i1].y, frames_float[0][i1].z);
			basemesh->insertVertex(c);
		}
		for (UInt i1 = 0; i1 < numT; i1++) {
			basemesh->insertTriangle(tris[i1].x, tris[i1].y, tris[i1].z);
		}
	}

	//! Transforms the animaion according to 'r_trans'.
	virtual void transformAnimation(const RigidTransform<vec3d>& r_trans) {

		for (UInt i1 = 0; i1 < frames.size(); i1++) {
			for (UInt i2 = 0; i2 < (UInt)basemesh->getNumV(); i2++) {
				vec3d& pt = frames[i1][i2];
				pt = r_trans.vecTrans(pt);
			}
		}

		for (UInt i2 = 0; i2 < (UInt)basemesh->getNumV(); i2++) {
			vec3d& pt = basemesh->vList[i2].c;
				pt = r_trans.vecTrans(pt);
		}

	}

	//! Resizes/Moves the animation.
	virtual void resizeAnimation(vec3d center, double max_side_length) {

		for (UInt i1 = 0; i1 < frames.size(); i1++) {
			for (UInt i2 = 0; i2 < (UInt)basemesh->getNumV(); i2++) {
				vec3d& pt = frames[i1][i2];
				pt /= max_side_length;
				pt -= center;
			}
		}

		for (UInt i2 = 0; i2 < (UInt)basemesh->getNumV(); i2++) {
			vec3d& pt = basemesh->vList[i2].c;
			pt /= max_side_length;
			pt -= center;
		}

	}

	//! Scales and moves the mesh such that the complete animation is contained in the [0:1] unit cube.
	virtual void normalizeAnimation() {
		vec3d v_min, v_max;
		getMinMax(v_min, v_max);

		vec3d center = (v_min+v_max)/2;
		vec3d diag = v_max-v_min;

		double max_side_length = diag[0];
		if (diag[1] > max_side_length) max_side_length = diag[1];
		if (diag[2] > max_side_length) max_side_length = diag[2];

		for (UInt i1 = 0; i1 < frames.size(); i1++) {
			for (UInt i2 = 0; i2 < (UInt)basemesh->getNumV(); i2++) {
				vec3d& pt = frames[i1][i2];
				pt -= center;
				pt /= max_side_length;
				pt += vec3d(0.5, 0.5, 0.5);
			}
		}

		for (UInt i2 = 0; i2 < (UInt)basemesh->getNumV(); i2++) {
			vec3d& pt = basemesh->vList[i2].c;
			pt -= center;
			pt /= max_side_length;
			pt += vec3d(0.5, 0.5, 0.5);
		}

		center.print();
		std::cerr << max_side_length << std::endl;
	}

	std::vector<vec3f*> frames_float;


private:


	std::vector<vec3f*> frames_float_normals;
	bool coinArraysPrepared;

	SoCoordinate3* coord3;
	SoNormal* nns;
	SoMFInt32* matIndex;

};


class PointCloud4DIV : public AnimatedMeshIV {

public:

	UInt maxPoints;


	virtual void getMinMax(vec3d& v_min, vec3d& v_max) const {
		m_pc->recomputeMinMax();
		vec4f mmin, mmax;
		m_pc->getMinMax(mmin, mmax);
		v_min = vec3d(mmin.x, mmin.y, mmin.z);
		v_max = vec3d(mmax.x, mmax.y, mmax.z);

	}

	PointCloud4DIV(PointCloud4D<float>* pc) : m_pc(pc) {
		maxPoints = 0;
		// compute maxPoints
		for (UInt i1 = 0; i1 < getNumFrames(); i1++) {
			UInt s, e;
			m_pc->getNumRangeOfPointSteps(i1, s, e);
			UInt diff = e-s;
			if (diff > maxPoints) maxPoints = diff+1;
		}
	}

	virtual SoSeparator* getIVMesh(SoMaterial* mat = NULL, int dir = 0, vec3d displace = vec3d(0,0,0), bool useTexture = false, bool useColors = false, bool usePendulum = false, SimpleMesh* meshWithColor = NULL, float tex_factor = 1) {

		SoSeparator* ivmesh = new SoSeparator;
		ivmesh->setName("pcToIv");

		if (mat != NULL) ivmesh->addChild(mat);

		if (displace.squaredLength() > 0) {
			SoTranslation* t = new SoTranslation;
			t->translation.setValue(SbVec3f((float)displace.x, (float)displace.y, (float)displace.z));
			ivmesh->addChild(t);
		}

		normals = new SoNormal;
		coord3  = new SoCoordinate3;
		normals->vector.setNum(maxPoints);
		coord3->point.setNum(maxPoints);


		// prepare coin arrays
		for (UInt i = 0; i < getNumFrames(); i++) {
			SbVec3f* pts = new SbVec3f[maxPoints];
			SbVec3f* nrl = new SbVec3f[maxPoints];
			UInt si, ei;
			m_pc->getNumRangeOfPointSteps(i, si, ei);
			for (UInt i2 = si; i2 < ei; i2++) {
				pts[i2-si] = SbVec3f(m_pc->getPoints()[i2].x, m_pc->getPoints()[i2].y, m_pc->getPoints()[i2].z);
				nrl[i2-si] = SbVec3f(m_pc->getNormals()[i2].x, m_pc->getNormals()[i2].y, m_pc->getNormals()[i2].z);
			}

			SbVec3f p3d_first(m_pc->getPoints()[si].x, m_pc->getPoints()[si].y, m_pc->getPoints()[si].z);
			SbVec3f n3d_first(m_pc->getNormals()[si].x, m_pc->getNormals()[si].y, m_pc->getNormals()[si].z);
			for (UInt i2 = ei-si; i2 < maxPoints; i2++) {
				pts[i2] = p3d_first;
				nrl[i2] = n3d_first;
			}
			coin_coords.push_back(pts);
			coin_normals.push_back(nrl);
		}


		coord3->point.setValues(0, maxPoints, coin_coords[0]);
		normals->vector.setValues(0, maxPoints, coin_normals[0]);
		//for (UInt i2 = si; i2 < ei; i2++) {
		//	vec3f p3d(m_pc->getPoints()[i2].x, m_pc->getPoints()[i2].y, m_pc->getPoints()[i2].z);
		//	vec3f n3d(m_pc->getNormals()[i2].x, m_pc->getNormals()[i2].y, m_pc->getNormals()[i2].z);
		//	coord3->point.set1Value(i2, SbVec3f((float)p3d.x, (float)p3d.y, (float)p3d.z));
		//	normals->vector.set1Value(i2, SbVec3f((float)n3d.x, (float)n3d.y, (float)n3d.z));
		//}
		//vec3f p3d_first(m_pc->getPoints()[si].x, m_pc->getPoints()[si].y, m_pc->getPoints()[si].z);
		//vec3f n3d_first(m_pc->getNormals()[si].x, m_pc->getNormals()[si].y, m_pc->getNormals()[si].z);
		//for (UInt i1 = ei-si; i1 < maxPoints; i1++) {
		//	coord3->point.set1Value(i1, SbVec3f((float)p3d_first.x, (float)p3d_first.y, (float)p3d_first.z));
		//	normals->vector.set1Value(i1, SbVec3f((float)n3d_first.x, (float)n3d_first.y, (float)n3d_first.z));
		//}

		ivmesh->addChild(coord3);
		ivmesh->addChild(normals);

		SoDrawStyle* ds = new SoDrawStyle;
		ds->pointSize = (float)4;
		ivmesh->addChild(ds);

		//SoShapeHints* sh = new SoShapeHints;
		//sh->shapeType = SoShapeHints::SOLID;
		//sh->vertexOrdering = SoShapeHints::CLOCKWISE;
		//iv_pc->addChild(sh);

		SoPointSet* pointset = new SoPointSet;
		ivmesh->addChild(pointset);

		return ivmesh;
	}


	virtual void showMeshNumber(UInt i) {

		UInt si, ei;
		m_pc->getNumRangeOfPointSteps(i, si, ei);

		if ((ei-si) > maxPoints) {
			std::cerr << "Error, pc frame contains to many points" << std::endl;
			exit(EXIT_FAILURE);
		}

		for (UInt i2 = si; i2 < ei; i2++) {
			vec3f p3d(m_pc->getPoints()[i2].x, m_pc->getPoints()[i2].y, m_pc->getPoints()[i2].z);
			vec3f n3d(m_pc->getNormals()[i2].x, m_pc->getNormals()[i2].y, m_pc->getNormals()[i2].z);
			coord3->point.set1Value(i2-si, SbVec3f((float)p3d.x, (float)p3d.y, (float)p3d.z));
			normals->vector.set1Value(i2-si, SbVec3f((float)n3d.x, (float)n3d.y, (float)n3d.z));
		}

		vec3f p3d_first(m_pc->getPoints()[si].x, m_pc->getPoints()[si].y, m_pc->getPoints()[si].z);
		vec3f n3d_first(m_pc->getNormals()[si].x, m_pc->getNormals()[si].y, m_pc->getNormals()[si].z);
		for (UInt i1 = ei-si; i1 < maxPoints; i1++) {
			coord3->point.set1Value(i1, SbVec3f((float)p3d_first.x, (float)p3d_first.y, (float)p3d_first.z));
			normals->vector.set1Value(i1, SbVec3f((float)n3d_first.x, (float)n3d_first.y, (float)n3d_first.z));
		}
	}

	virtual UInt getNumFrames() const {
		return m_pc->getNumTimeSteps();
	}

	virtual void resize(vec4f center, float max_side_length) {
		for (UInt i1 = 0; i1 < m_pc->getNumPts(); i1++) {
			vec4f& pt = m_pc->getPoints()[i1];
			pt -= center;
			pt /= max_side_length;
			pt += vec4f(0.5f, 0.5f, 0.5f, 0);
		}
	}

	//! Scales and moves the mesh such that the complete animation is contained in the [0:1] unit cube.
	virtual void normalizeAnimation() {

		vec4f v_min, v_max;
		m_pc->getMinMax(v_min, v_max);

		vec4f center = (v_min+v_max)/2;
		vec4f diag = v_max-v_min;

		float max_side_length = diag[0];
		if (diag[1] > max_side_length) max_side_length = diag[1];
		if (diag[2] > max_side_length) max_side_length = diag[2];

		resize(center, max_side_length);
	}

	PointCloud4D<float>* getPointCloud() { return m_pc; }

	void freePointCloud() {
		delete m_pc;
	}


private:

	std::vector<SbVec3f*> coin_coords;
	std::vector<SbVec3f*> coin_normals;

	PointCloud4D<float>* m_pc;

	SoCoordinate3* coord3;
	SoNormal* normals;
};

#endif
