#ifndef VOL_TO_SIMPLE_MESH_H
#define VOL_TO_SIMPLE_MESH_H


#include "Volume.h"
#include "marchingCubes.h"
#include "SimpleMesh.h"
#include "operators/TrivialMeshToSimpleMesh.h"


template <class T>
SimpleMesh* VolumeToSm(Volume<T>* vol, T isovalue) {

   TrivialMesh<vec3f>* mesh = new TrivialMesh<vec3f>(11000000);

   int dx = vol->dx;
   int dy = vol->dy;
   int dz = vol->dz;
   //dx = 50;

   float min_dens = 20;

   for (int x = 0; x < dx-1; x++) {
       std::cerr << x << " / " << dx << "            \r";
       for (int y = 0; y < dy-1; y++)
		   for (int z = 0; z < dz-1; z++) {
               processVolumeCell(vol, x, y, z, (float)isovalue, mesh);
		   }
   }
   std::cerr << "TrivialMesh with " << mesh->getNumTris() << " triangles" << std::endl;

   SimpleMesh* sm = TrivialMeshToSimpleMesh(mesh);
   delete mesh;

   return sm;
}


template <class T>
SimpleMesh* VolumeToSm(Volume<T>* vol, T isovalue, Volume<T>* dens) {

   TrivialMesh<vec3f>* mesh = new TrivialMesh<vec3f>(11000000);

   int dx = vol->dx;
   int dy = vol->dy;
   int dz = vol->dz;
   //dx = 50;

   float min_dens = 20;

   for (int x = 0; x < dx-1; x++) {
       std::cerr << x << " / " << dx << "            \r";
       for (int y = 0; y < dy-1; y++)
		   for (int z = 0; z < dz-1; z++) {
			   if (dens->get(x,y,z) < min_dens) continue;
			   if (dens->get(x,y,z+1) < min_dens) continue;
			   if (dens->get(x,y+1,z) < min_dens) continue;
			   if (dens->get(x,y+1,z+1) < min_dens) continue;
			   if (dens->get(x+1,y,z) < min_dens) continue;
			   if (dens->get(x+1,y,z+1) < min_dens) continue;
			   if (dens->get(x+1,y+1,z) < min_dens) continue;
			   if (dens->get(x+1,y+1,z+1) < min_dens) continue;
               processVolumeCell(vol, x, y, z, isovalue, mesh);
		   }
   }
   std::cerr << "TrivialMesh with " << mesh->getNumTris() << " triangles" << std::endl;

   SimpleMesh* sm = TrivialMeshToSimpleMesh(mesh);
   delete mesh;

   return sm;
}

#endif
