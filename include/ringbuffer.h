#ifndef JBS_RINGBUFFER_H
#define JBS_RINGBUFFER_H


#include <iostream>


template <class Type>
class RingBuffer {

public:

	RingBuffer() {
		m_data = NULL;
	}

	RingBuffer(unsigned int numElements_) {
		m_data = NULL;
		alloc(numElements_);
		initZero();
	}

	RingBuffer(const RingBuffer& other) {
		numElements = other.numElements;
		alloc(numElements);
		for (unsigned int i1 = 0; i1 < numElements; i1++) {
			m_data[i1] = other.m_data[i1];
		}
		pos_ptr = other.pos_ptr;
	}

	~RingBuffer() {
		if (m_data != NULL) delete[] m_data;
	}

	const RingBuffer& operator=(const RingBuffer& other) {
		numElements = other.numElements;
		alloc(numElements);
		for (unsigned int i1 = 0; i1 < numElements; i1++) {
			m_data[i1] = other.m_data[i1];
		}
		pos_ptr = other.pos_ptr;
		return *this;
	}

	Type operator[](unsigned int idx) const {
		int realIdx = (pos_ptr+numElements-idx)%numElements;
		return m_data[realIdx];
	}

	Type getAverage() const {
		Type avg(0.0);
		for (unsigned int i1 = 0; i1 < numElements; i1++) {
			avg += operator[](i1);
		}
		avg /= numElements;

		return avg;
	}

	void push_back(const Type& t) {
		pos_ptr = (pos_ptr+1)%numElements;
		m_data[pos_ptr] = t;
	}

	void print() const {
		for (unsigned int i1 = 0; i1 < numElements; i1++) {
			std::cerr << (*this)[i1] << " ";
		}
		std::cerr << std::endl;
	}

	void alloc(unsigned int numElements_) {
		numElements = numElements_;
		if (m_data != NULL) delete[] m_data;
		m_data = new Type[numElements];
	}

	void initZero() {
		pos_ptr = -1;
		for (unsigned int i1 = 0; i1 < numElements; i1++) {
			m_data[i1] = Type(0.0);
		}
	}

	unsigned int getNumElements() const {
		return numElements;
	}

protected:

	unsigned int numElements;
	int pos_ptr;
	Type* m_data;

};


#endif
