#ifndef CLINE_HULL_H
#define CLINE_HULL_H

#include "ClarksonHull.h"



std::vector<Quadruple> buildConvexHullCL(std::vector<vec4d> pts_);
std::vector<Triple> buildConvexHullCL(std::vector<vec3d> pts_);

#endif
