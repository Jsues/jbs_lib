#ifndef MULTIRES_MESH_H
#define MULTIRES_MESH_H


#include "SimpleMesh.h"
#include "operators/computeVertexCurvature.h"

//! A value/index pair.
struct p_v_i {
	p_v_i(double val, UInt ind) : value(val), index(ind) {}

	bool operator<(const p_v_i& other) {
		return value < other.value;
	}

	double value;
	UInt index;

};


inline double getAreaAtFan(SimpleMesh* sm, UInt i) {
	double area = 0;

	UInt valence = (UInt)sm->vList[i].eList.size();
	for (UInt i1 =  0; i1 < valence; i1++) {
		Edge* e = sm->vList[i].eList[i1];
		for (UInt i2 = 0; i2 < e->tList.size(); i2++) {
			Triangle* t = e->tList[i2];
			t->marker = false;
		}
	}
	for (UInt i1 =  0; i1 < valence; i1++) {
		Edge* e = sm->vList[i].eList[i1];
		for (UInt i2 = 0; i2 < e->tList.size(); i2++) {
			Triangle* t = e->tList[i2];
			if (!t->marker) {
				area += t->getArea();
			}
			t->marker = true;
		}
	}
	return area;
}


struct reduceStep {
	//! sm->vlist[basis[0]].c, sm->vlist[basis[1]].c and sm->vlist[basis[2]].c span the basis for the newly added point
	vec3i basis;
	//! local coordinates to the 'to be added' point in the above basis.
	vec3d local_coords;
	//! The vertices of the fan in which the new vertex will be inserted.
	std::vector<int> fan;


	//! marks whether the removed vertex was a boundary vertex
	bool isBoundaryVertex;
};


struct reduceInfo {
	int lastOldTriangle;
	std::vector<reduceStep> rSteps;
};


inline double getCurvatureAtFan(const SimpleMesh* sm, vec3d normal, UInt i) {
	return computeCurvatureAtVertex(sm, i, normal);
}


class MultiResMesh {

public:

	typedef std::vector<edgeCollapsInfo> collapseInfo;

	MultiResMesh(SimpleMesh* sm);

	~MultiResMesh();

	//! Simplifies the mesh up to depth 'max_depth' and computes the info required for refining.
	/*!
		\param lambda ist a weight between 0 and 1. it is used to balance between 'remove small fans' and remove 'low curvature fans'
		lambda = 0 means 'only care about curvature', ignore fans size and vice versa.
	*/
	void buildMeshHierarchy(UInt max_depth, double lambda = 0.5, SimpleMesh* secondMesh = NULL);

	SimpleMesh* getLevel(UInt level) {
		if (level == 0) {
			return m_sm;
		} else if (level <= m_max_depth) {
			return meshStack[level-1];
		} else {
			std::cerr << "out of bounds access to meshStack!" << std::endl;
			return NULL;
		}
	}

	SimpleMesh* refineMesh(const SimpleMesh* sm, int level);

	//! Resorts the vertices and triangles of 'sm' such that they are coherent with the 'level' mesh
	SimpleMesh* resortRefinedMesh(const SimpleMesh* sm, int level);

private:

	SimpleMesh* m_sm;
	UInt m_max_depth;
	//std::vector<collapseInfo> colInfoStack;
	std::vector<SimpleMesh*> meshStack;
	std::vector<reduceInfo> reduceInfoStack;
public:
	std::vector<int*> vertexLookUpTables;
};


#endif
