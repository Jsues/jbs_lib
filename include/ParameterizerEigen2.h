#ifndef PARAMETERIZER_EIGEN_2_H
#define PARAMETERIZER_EIGEN_2_H


#include <vector>
#include <map>
#include <set>
#include <fstream>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include "point3d.h"
#include "Matrix3D.h"
#include "timer.h"

typedef unsigned int UInt;

#ifndef M_PI2
#define M_PI2 6.28318530718
#endif


//! Computes a parameterization of the given mesh.
class ParameterizerEigen2
{
public:

	//! Choose of Parametrization method.
	enum METHOD {
		CHORD = 0,
		SHAPE_PRES = 1,
		COTANGENT = 2,
		UNIFORM = 3,
		MEAN_VALUE = 4
	};

	//! Choose of 2D-primitive on which boundary will be mapped
	enum BOUNDARY {
		CIRCLE = 0,
		RECT = 1,
		USEMESHVERTICES = 2
	};

	typedef std::pair<int, double> NWpair; 

	ParameterizerEigen2() {}

	~ParameterizerEigen2(void) {}


//******************************************************************************************

	//! Computes the parametrization of the mesh defined by 'pts' and 'tris' using method 'met' after fixing the boundary of the mesh on a shape defined by 'bound'. Returns the parameterization in 'parameterization'
	/*!
		\param boundary order of the boundary polygon. It is assumed that boundary[0] = boundary[boundary.size()-1]!!!
		\param parameterization array of 2D points where the parameterization will be written to. if METHOD==USEMESHVERTICES the UV position of the boundary vertices will be set to the corresponding values in 'parameterization'
	*/
    template <class Point2D, class Point3D, class Tri, class Index>
    static void parameterize(const std::vector<Point3D>& pts, const std::vector<Tri>& tris, const std::vector<Index>& boundary, enum METHOD met, enum BOUNDARY bound, std::vector<Point2D>& parameterization) {
        
        UInt numPts = pts.size();
		//parameterization.clear();
        parameterization.resize(numPts);
        std::vector<char> vertex_is_fixed(numPts, 0);
        for (unsigned int i1 = 0; i1 < boundary.size(); i1++) vertex_is_fixed[boundary[i1]] = 1;
        
        // Create list of neighbors per vertex
        std::vector<std::set<int>> vertex_neighbors(numPts);
        for (UInt i1 = 0; i1 < tris.size(); i1++) {
            const Tri& t = tris[i1];
            vertex_neighbors[t.x].insert(t.y); vertex_neighbors[t.x].insert(t.z);
            vertex_neighbors[t.y].insert(t.x); vertex_neighbors[t.y].insert(t.z);
            vertex_neighbors[t.z].insert(t.y); vertex_neighbors[t.z].insert(t.x);
        }
        
        // Build vertex lookup (index in vertex array to index in linear system)
        std::vector<int> vertexLookup(numPts);
        UInt numUnknown = 0;
        for (UInt i1 = 0; i1 < numPts; i1++) {
            if (!vertex_is_fixed[i1]) {
                vertexLookup[i1] = numUnknown;
                numUnknown++;
            }
        }
        
        // Fix boundary vertices on convex polygon
        fixBoundaryVertices(pts, boundary, parameterization, bound);
        
        // Build matrix & rhs
        std::vector<Eigen::Triplet<double>> entries;
        Eigen::VectorXd rhs_x;
        rhs_x.resize(numUnknown);
        Eigen::VectorXd rhs_y;
        rhs_y.resize(numUnknown);
        
        std::cout << "Searching for " << numUnknown << " unknowns" << std::endl;
        
        JBS_Timer t;
        //t.log("compute matrix entries");
        for (UInt v1 = 0; v1 < numPts; v1++) {
            if (vertex_is_fixed[v1]) continue;
            
            std::vector<NWpair> nwpairs;
            double sum;
            if (met == ParameterizerEigen2::UNIFORM) sum = getWeightsUniform(pts, vertex_neighbors[v1], nwpairs);
            else if (met == ParameterizerEigen2::CHORD) sum = getWeightsChordal(v1, pts, vertex_neighbors[v1], nwpairs);
			else if (met == ParameterizerEigen2::COTANGENT) sum = getWeightsCotangent(v1, pts, vertex_neighbors, nwpairs);
			else if (met == ParameterizerEigen2::MEAN_VALUE) sum = getWeightsMeanValue(v1, pts, vertex_neighbors, nwpairs);
            
            // insert diagonal entry
            entries.push_back(Eigen::Triplet<double>(vertexLookup[v1], vertexLookup[v1], sum));
            
            Point2D rhs(0.0, 0.0);
            for (UInt i2 = 0; i2 < nwpairs.size(); i2++) {
                int v2 = nwpairs[i2].first;
                typename Point2D::value_type w = nwpairs[i2].second;
                
                if (vertex_is_fixed[v2]) { // put onto rhs
                    rhs += parameterization[v2]*w;
                } else { // Put into system matrix
                    //if (vertexLookup[v1] < vertexLookup[v2])
                    entries.push_back(Eigen::Triplet<double>(vertexLookup[v1], vertexLookup[v2], -w));
                }
            }
            rhs_x[vertexLookup[v1]] = rhs.x;
            rhs_y[vertexLookup[v1]] = rhs.y;
        }
        
        //t.log("set sparse matrix");
        Eigen::SparseMatrix<double, Eigen::ColMajor> A;
        A.resize(numUnknown, numUnknown);
        A.setFromTriplets(entries.begin(), entries.end());
        
		Eigen::VectorXd X_x, X_y;

        //t.log("decompose matrix");
		if (met == ParameterizerEigen2::MEAN_VALUE) { // Mean-Value parameterization leads to non-symmetric matrix. Thus, LU Composition is necessary
			Eigen::SparseLU<Eigen::SparseMatrix<double, Eigen::ColMajor>, Eigen::COLAMDOrdering<int> > solverA;
			solverA.analyzePattern(A); 
			solverA.factorize(A); 
			//t.log("solve system 2x");
			X_x = solverA.solve(rhs_x);
			X_y = solverA.solve(rhs_y);
			//t.log();
			//t.print();
		} else { // Symmetric system, thus, sparse Cholesky is sufficient
			Eigen::SimplicialLDLT< Eigen::SparseMatrix<double> > solverA;
			solverA.compute(A);
			//t.log("solve system 2x");
			X_x = solverA.solve(rhs_x);
			X_y = solverA.solve(rhs_y);
			//t.log();
			//t.print();
		}

        
        for (UInt i1 = 0; i1 < numPts; i1++) {
            if (vertex_is_fixed[i1]) continue;
            int v0 = vertexLookup[i1];
            parameterization[i1].x = X_x[v0];
            parameterization[i1].y = X_y[v0];
        }
    }
    
private:

//******************************************************************************************
    
    template <class Point3D>
    static typename Point3D::value_type dist3D(const Point3D& left, const Point3D& right) {
        return sqrt(((left.x-right.x)*(left.x-right.x) + (left.y-right.y)*(left.y-right.y) + (left.z-right.z)*(left.z-right.z)));
    }
    
    template <class Point3D>
    static typename Point3D::value_type length3D(const Point3D& v) {
        return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
    }
    
    //! Vektor-/Kreuz-Produkt
    template <class Point3D>
    static Point3D cross(const Point3D& left, const Point3D& right) {
        return Point3D(left.y*right.z - left.z*right.y, left.z*right.x - left.x*right.z, left.x*right.y - left.y*right.x);
    }
    
    // ****************************************************************************************
    
    //! Skalarmultiplikation
    template <class Point3D>
    static typename Point3D::value_type dot(const Point3D& left, const Point3D& right) {
        return (left.x*right.x + left.y*right.y + left.z*right.z);
    }
    
    //******************************************************************************************

	template <class Point>
	static double getWeightsUniform(const std::vector<Point>& pts, const std::set<int>& neighbors, std::vector<NWpair>& nwpairs) {
		nwpairs.clear();
		double sum = 0;
		for (std::set<int>::const_iterator it = neighbors.begin(); it != neighbors.end(); it++) {
			double w = 1.0;
			sum += w;
			nwpairs.push_back(std::pair<int, double>(*it, w));
		}
		return sum;
	}

//******************************************************************************************

	template <class Point>
	static double getWeightsChordal(int vertex, const std::vector<Point>& pts, const std::set<int>& neighbors, std::vector<NWpair>& nwpairs) {
		nwpairs.clear();
		double sum = 0;
		for (std::set<int>::const_iterator it = neighbors.begin(); it != neighbors.end(); it++) {
			const int& n = *it;
			double w = dist3D(pts[vertex], pts[n]);
			sum += w;
			nwpairs.push_back(std::pair<int, double>(n, w));
		}
		return sum;
	}

//******************************************************************************************

	template <class Point>
	static double getWeightsCotangent(int v0, const std::vector<Point>& pts, const std::vector< std::set<int> >& edges, std::vector<NWpair>& nwpairs) {
		nwpairs.clear();
		double sum = 0;
		const Point& p0 = pts[v0];
		for (std::set<int>::const_iterator it = edges[v0].begin(); it != edges[v0].end(); it++) {
			const int& v1 = *it;

			std::vector<int> others = getIndicesOfOppositeVerticesOfTrisAttachedToEdge(edges, v0, v1);

			const Point& p1 = pts[v1];
			typename Point::value_type cotangent_weight = 0;
			for (unsigned int i2 = 0; i2 < others.size(); i2++) {
				const int& v2 = others[i2];
				const Point& p2 = pts[v2];

				Point vec0 = p0-p2;
				Point vec1 = p1-p2;
				//double cos_ = (vec0|vec1)/(vec0.length()*vec1.length());
				//double sin_ = (vec0^vec1).length()/(vec0.length()*vec1.length());
				cotangent_weight += (dot(vec0,vec1))/length3D(cross(vec0,vec1));
			}
			
			//if (cotangent_weight < 0) cotangent_weight = 0;
			nwpairs.push_back(std::pair<int, double>(v1, cotangent_weight));
			sum += cotangent_weight;
		}
		return sum;
	}
	
//******************************************************************************************

	template <class Point>
	static double getWeightsMeanValue(int v0, const std::vector<Point>& pts, const std::vector< std::set<int> >& edges, std::vector<NWpair>& nwpairs) {
		nwpairs.clear();
		double sum = 0;
		const Point& p0 = pts[v0];
		for (std::set<int>::const_iterator it = edges[v0].begin(); it != edges[v0].end(); it++) {
			const int& v1 = *it;

			std::vector<int> others = getIndicesOfOppositeVerticesOfTrisAttachedToEdge(edges, v0, v1);

			const Point& p1 = pts[v1];
			typename Point::value_type meanvalue_weight = 0;
			for (unsigned int i2 = 0; i2 < others.size(); i2++) {
				const int& v2 = others[i2];
				const Point& p2 = pts[v2];

				Point vec0 = p1-p0;
				Point vec1 = p2-p0;
                vec0 = vec0 / length3D(vec0);
                vec1 = vec1 / length3D(vec1);
				typename Point::value_type angle = acos(dot(vec0,vec1));

				meanvalue_weight += tan(angle/2);
			}
			
			meanvalue_weight /= 2*dist3D(p0,p1);

			//if (cotangent_weight < 0) cotangent_weight = 0;
			nwpairs.push_back(std::pair<int, double>(v1, meanvalue_weight));
			sum += meanvalue_weight;
		}
		return sum;
	}

//******************************************************************************************

	//! Finds all triangles attached to the edge 'v0' to 'v1' and returns their third indicees in an array
	static std::vector<int> getIndicesOfOppositeVerticesOfTrisAttachedToEdge(const std::vector< std::set<int> >& edges, int v0, int v1) {
		const std::set<int> v0_edges = edges[v0];
		const std::set<int> v1_edges = edges[v1];

		std::vector<int> other_points;
		for (std::set<int>::const_iterator it = v0_edges.begin(); it != v0_edges.end(); it++) {
			int other = *it;
			if (v1_edges.find(other) != v1_edges.end()) other_points.push_back(other);
		}
		return other_points;
	}

//******************************************************************************************

	template <class Point2D, class Point3D, class Index>
	static void fixBoundaryVertices(const std::vector<Point3D>& pts, const std::vector<Index>& boundary, std::vector<Point2D>& parametrization, enum BOUNDARY bound) {

		// Calculate length of boundary:
		double length = 0.0;
		for (unsigned int i1 = 0; i1 < boundary.size()-1; i1++) {
			const int& v0 = boundary[i1];
			const int& v1 = boundary[i1+1];
			length += dist3D(pts[v0], pts[v1]);
		}

		/************************************/
		// Parameterize boundary points
		/************************************/
		if (bound == ParameterizerEigen2::CIRCLE) {

			// First point of boundary is mapped to (1,0);
			parametrization[boundary[0]] = Point2D(1,0);

			double used_segsize = 0.0;
			for (unsigned int i1 = 0; i1 < boundary.size()-2; i1++) {
				// Calculate relative length of edge
				used_segsize += dist3D(pts[boundary[i1]], pts[boundary[i1+1]]);
				double phi = (used_segsize / length) * (M_PI2);
				parametrization[boundary[i1+1]] = Point2D(cos(phi), sin(phi));
			}

		//} else if (bound == ParameterizerEigen2::RECT) {
		//	// First point of boundary is mapped to (0,0);
		//	parametrization[boundary[0]] = vec2d(0,0);
		//	double used_segsize = 0.0;
		//	for (unsigned int i1 = 0; i1 < boundary.size()-2; i1++) {
		//		// Berechne Prozentsatz der L�nge:
		//		used_segsize += (mesh->getVertex(boundary[i1]) - mesh->getVertex(boundary[i1+1])).length();

		//		float used_segsize_percentage = (float)(used_segsize/length);

		//		if (used_segsize_percentage < 0.25f)
		//			parametrization[boundary[i1+1]] = vec2d(0, used_segsize_percentage * 4);
		//		else if ((used_segsize/length) < 0.5f)
		//			parametrization[boundary[i1+1]] = vec2d((used_segsize_percentage-0.25) * 4, 1);
		//		else if ((used_segsize/length) < 0.75f)
		//			parametrization[boundary[i1+1]] = vec2d(1, 1-(used_segsize_percentage-0.5f) * 4);
		//		else 
		//			parametrization[boundary[i1+1]] = vec2d(1-(used_segsize_percentage-0.75f) * 4, 0);
		//	}
		} else if (bound == ParameterizerEigen2::USEMESHVERTICES) {
			//for (UInt i1 = 0; i1 < boundary.size(); i1++) {
			//	const Point3D& pos = pts[boundary[i1]];
			//	parametrization[boundary[i1]] = Point2D(pos.x, pos.y);
			//}
		} else {
			std::cerr << "Error in ParameterizerEigen2:" << std::endl;
			std::cerr << "unknown boundary parameterization chosen.\nEXIT" << std::endl;
		}   
	}

};

//******************************************************************************************

#endif
