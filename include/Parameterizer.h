#ifndef PARAMETERIZER_H
#define PARAMETERIZER_H


#include <vector>

#include "JBS_General.h"
#include "SimpleMesh.h"
#include "Matrix3D.h"


//! Computes a parameterization of the given mesh.
class Parameterizer
{
public:

	//! Choose of Parametrization method.
	enum METHOD {
		CHORD = 0,
		SHAPE_PRES = 1,
		COTANGENT = 2,
		UNIFORM = 3,
		MEAN_VALUE = 4
	};

	//! Choose of 2D-primitive on which boundary will be mapped
	enum BOUNDARY {
		CIRCLE = 0,
		RECT = 1,
		USEMESHVERTICES = 2
	};

	Parameterizer(SimpleMesh* mesh_, std::vector<int> boundary_);

	~Parameterizer(void);
#ifndef NO_TAUCS
	std::vector<vec2d> doParametrization(enum METHOD met, enum BOUNDARY bound);
#endif
#ifndef NO_UMFPACK
	std::vector<vec2d> doParametrization(enum METHOD met, enum BOUNDARY bound);
#endif

	void createIVFile(const char *filename);
	void createOFFFile(const char *filename);

	std::vector<vec2d> parametrization;
private:

	void fixBoundaryVertices(enum BOUNDARY bound);


	SimpleMesh* mesh;
	std::vector<int> boundary;
};

#endif

