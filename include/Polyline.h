#ifndef POLYLINE_H
#define POLYLINE_H

#include <vector>
#include <list>
#include <limits>
#include <iomanip>
#include <fstream>
#include <map>
#include <set>

#include "point4d.h"
#include "JBS_General.h"


class LineSegment {

public:

	typedef unsigned int UInt;

// ****************************************************************************************

	LineSegment(UInt v0_, UInt v1_) : v0(v0_), v1(v1_) {
		marker = 0;
	}

// ****************************************************************************************

	void print() const {
		std::cerr << v0 << " - " << v1 << std::endl;
	}

// ****************************************************************************************

	UInt getOther(UInt v) {
		if (v == v0) return v1;
		return v0;
	}

// ****************************************************************************************

	//! index of line endpoint 0.
	UInt v0;

	//! index line endpoint 1.
	UInt v1;

	//! marker, may be used for any purpose.
	UInt marker;

private:


};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class LineVertex {

public:

// ****************************************************************************************

	//! Type of a Linepoint, for example a 2D Vector of floats.
	typedef T Point;

	//! Value-Type of Templates components.
	typedef typename T::value_type ValueType;

// ****************************************************************************************

	LineVertex(const Point& p) {
		c = p;
	}

// ****************************************************************************************

	bool isMarkedToDelete() {

		for (UInt i1 = 0; i1 < lines.size(); i1++)
			if (lines[i1]->marker != 1) return false;

		return true;
	}

// ****************************************************************************************

	Point c;
	std::vector<LineSegment*> lines;

private:
};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

template <class T>
class TrivialLineSegment {
public:

	//! Type of a Linepoint, for example a 2D Vector of floats.
	typedef T Point;

	TrivialLineSegment(Point v0_, Point v1_) : v0(v0_), v1(v1_) {
	}

	Point v0;
	Point v1;
};


template <class T>
class TrivialLine {

public:
	
// ****************************************************************************************

	//! Type of a Linepoint, for example a 2D Vector of floats.
	typedef T Point;

	//! Value-Type of Templates components.
	typedef typename T::ValueType ValueType;

// ****************************************************************************************

	TrivialLine() {
	}

// ****************************************************************************************

	void insertSegment(Point v0, Point v1) {
		lines.push_back(TrivialLineSegment<T>(v0,v1));
	}

// ****************************************************************************************

	void print() {
		std::cerr << "TrivialLine<T> has " << (int)lines.size() << " Segments" << std::endl;
	}

// ****************************************************************************************

#ifdef max
#undef max
#endif

	void ComputeMinAndMaxExt(Point& min_v, Point& max_v) const {
		min_v = Point(std::numeric_limits<ValueType>::max());
		max_v = Point(-(std::numeric_limits<ValueType>::max()));

		for (UInt i1 = 0; i1 < lines.size(); i1++) {
			for (UInt i2 = 0; i2 < Point::dim; i2++) {
				if (lines[i1].v0[i2] > max_v[i2]) max_v[i2] = lines[i1].v0[i2];
				if (lines[i1].v0[i2] < min_v[i2]) min_v[i2] = lines[i1].v0[i2];
				if (lines[i1].v1[i2] > max_v[i2]) max_v[i2] = lines[i1].v1[i2];
				if (lines[i1].v1[i2] < min_v[i2]) min_v[i2] = lines[i1].v1[i2];
			}
		}

	}

// ****************************************************************************************

	void loadFromFile(char* filename) {
		std::ifstream InFile(filename);

		UInt numLines;
		InFile >> numLines;
		for (UInt i1 = 0; i1 < numLines; i1++) {
			ValueType v0_x, v0_y, v0_z, v1_x, v1_y, v1_z;
			char c;
			InFile >> v0_x >> c >> v0_y >> c >> v0_z >> v1_x >> c >> v1_y >> c >> v1_z;
			insertSegment(Point(v0_x, v0_y, v0_z), Point(v1_x, v1_y, v1_z));

//			std::cerr << v0 << " " << v1 << std::endl;
		}

	}

// ****************************************************************************************

	void writeToFile(char* filename) {
		std::ofstream OutFile(filename);

		OutFile << (UInt)lines.size() << std::endl;
		for (UInt i1 = 0; i1 < lines.size(); i1++) {
			OutFile << lines[i1].v0 << " " << lines[i1].v1 << std::endl;
		}

		OutFile.close();
	}

// ****************************************************************************************

	std::vector<TrivialLineSegment<Point> > lines;
};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


//! An indexed Line
template <class T>
class PolyLine {

public:
	
// ****************************************************************************************

	typedef unsigned int UInt;

	//! Type of a Linepoint, for example a 2D Vector of floats.
	typedef T Point;

	//! Value-Type of Templates components.
	typedef typename T::value_type ValueType;

// ****************************************************************************************

	PolyLine() {
	}

// ****************************************************************************************

	PolyLine(const PolyLine<T>& pl) {
		//std::cerr << "calling copyconstructor" << std::endl; exit(1);
		for (UInt i1 = 0; i1 < pl.getNumV(); i1++) insertVertex(pl.points[i1].c);
		for (std::list<LineSegment>::const_iterator it = pl.lines.begin(); it != pl.lines.end(); it++) insertLine(it->v0, it->v1);
	}

// ****************************************************************************************
    
    PolyLine(const std::vector<T>& pts) {
        if (pts.empty()) { return; }
        insertVertex(pts[0]);
        for (const auto& p : pts) {
            insertVertex(p);
            insertLine(points.size() - 2, points.size() - 1);
        }
    }

// ****************************************************************************************

	PolyLine& operator=(const PolyLine<T>& pl) {
		points.clear();
		lines.clear();
		//std::cerr << "calling op=" << std::endl; exit(1);
		for (UInt i1 = 0; i1 < pl.getNumV(); i1++) insertVertex(pl.points[i1].c);
		for (std::list<LineSegment>::const_iterator it = pl.lines.begin(); it != pl.lines.end(); it++) insertLine(it->v0, it->v1);
		return *this;
	}

// ****************************************************************************************

	void insertVertex(Point p) {
		points.push_back(LineVertex<Point>(p));
	}

// ****************************************************************************************

//	Point getCenterOfLineSegment(UInt i) {
//		LineSegment line = lines[i];
//		Point lineCenter = (points[line.v1].c + points[line.v0].c)/2;
//		return lineCenter;
//	}
//
//// ****************************************************************************************
//
//	Point getNormalOfLineSegment(UInt i) {
//		Point n;
//		LineSegment line = lines[i];
//		Point lineDir = points[line.v1].c - points[line.v0].c;
//		n.x = lineDir.y;
//		n.y = lineDir.x;
//		return n;
//	}

// ****************************************************************************************

	void insertLine(UInt v0, UInt v1) {
		if (v0 >= points.size() || v1 >= points.size()) {
			std::cerr << "PolyLine<T>::insertLine(v0, v1): Index out of bounds, EXIT" << std::endl;
			return;
			exit(EXIT_FAILURE);
		}

		// Check if Line-Segment exists:
		//LineSegment new_ls(v0, v1);
		//for (UInt i1 = 0; i1 < points[v0].lines.size(); i1++) {
		//	LineSegment t_ls = points[v0].lines[i1];
		//	if (
		//}

		lines.push_back(LineSegment(v0, v1));
		LineSegment* ls = &(lines.back());
		points[v0].lines.push_back(ls);
		points[v1].lines.push_back(ls);

	}

// ****************************************************************************************

	std::vector< std::vector<UInt> > getAllCycles() {

		// Mark all line-segments unprocessed:
		for (std::list<LineSegment>::iterator it = lines.begin(); it != lines.end(); ++it) it->marker = 0;

		UInt numV = (UInt)points.size();
		std::vector< std::vector<UInt> > cycles;

		// Process open cycles
		for (UInt vert = 0; vert < numV; vert++) {
			if (points[vert].lines.size() != 1) continue; // Will be processed later.

			int last_vertex = -1;
			LineSegment* first_line = points[vert].lines[0];
			if (first_line->marker == 1) continue;

			LineSegment* current_line = first_line;
			std::vector<UInt> cycle;
			cycle.push_back(vert);

			do {
				current_line->marker = 1;
				UInt next_vertex = (current_line->v1 != last_vertex) ? current_line->v1 : current_line->v0;
				cycle.push_back(next_vertex);
	            
				if (points[next_vertex].lines.size() < 2) break; // dead end

				for (UInt i1 = 0; i1 < points[next_vertex].lines.size(); i1++) {
					if (points[next_vertex].lines[i1] != current_line) {
						current_line = points[next_vertex].lines[i1];
						break;
					}
				}
				last_vertex = next_vertex;
			} while (current_line != first_line);

			cycles.push_back(cycle);
		}

		// Process cycles
		for (UInt vert = 0; vert < numV; vert++) {
			if (points[vert].lines.size() != 2) continue;

			int last_vertex = -1;
			LineSegment* first_line = points[vert].lines[0];
			if (first_line->marker == 1) continue;

			LineSegment* current_line = first_line;
			std::vector<UInt> cycle;
			cycle.push_back(vert);

			do {
				current_line->marker = 1;
				UInt next_vertex = (current_line->v1 != last_vertex) ? current_line->v1 : current_line->v0;
				cycle.push_back(next_vertex);
	            
				if (points[next_vertex].lines.size() != 2) break; // dead end

				for (UInt i1 = 0; i1 < points[next_vertex].lines.size(); i1++) {
					if (points[next_vertex].lines[i1] != current_line) {
						current_line = points[next_vertex].lines[i1];
						break;
					}
				}
				last_vertex = next_vertex;
			} while (current_line != first_line);

			cycles.push_back(cycle);
		}

		//std::cerr << "Found " << (UInt)cycles.size() << " cycles" << std::endl;

		return cycles;
	}

// ****************************************************************************************

	std::vector<UInt> getCycle(UInt startAt = 0) const {

		std::vector<UInt> cycle;

		std::cerr << "Cycle starting at " << startAt << ":" << std::endl;
		if (points[startAt].lines.size() == 0) {
			std::cerr << "Error, point is isolated can't extract cycle!!!" << std::endl;
		}

		int last_vertex = -1;
		LineSegment* first_line = points[startAt].lines[0];
		LineSegment* current_line = first_line;

		do {

			UInt next_vertex = (current_line->v1 != last_vertex) ? current_line->v1 : current_line->v0;
			cycle.push_back(next_vertex);
            
			if (points[next_vertex].lines.size() != 2) {
				std::cerr << "WARNING: vertex " << next_vertex << " has " << (UInt)points[next_vertex].lines.size() << " lines!" << std::endl;
				break;
			}

			for (UInt i1 = 0; i1 < points[next_vertex].lines.size(); i1++) {
				if (points[next_vertex].lines[i1] != current_line) {
					current_line = (points[next_vertex].lines[i1]);
					break;
				}
			}
			last_vertex = next_vertex;
		} while (current_line != first_line);

		return cycle;
	}

// ****************************************************************************************

	void printCycle(UInt startAt = 0) const {

		UInt cycleSize = 0;

		std::cerr << "Cycle starting at " << startAt << ":" << std::endl;
		if (points[startAt].lines.size() == 0) {
			std::cerr << "Error, point is isolated can't extract cycle!!!" << std::endl;
		}

		int last_vertex = -1;
		LineSegment* first_line = points[startAt].lines[0];
		LineSegment* current_line = first_line;

		do {
			//std::cerr << "current_line: " << current_line->v0 << " " << current_line->v1 << "   last vertex: " << last_vertex << std::endl;

			UInt next_vertex = (current_line->v0 != last_vertex) ? current_line->v0 : current_line->v1;
			std::cerr << next_vertex << std::endl;
            
			cycleSize++;

			if (points[next_vertex].lines.size() != 2) {
				std::cerr << "WARNING: vertex " << next_vertex << " has " << points[next_vertex].lines.size() << " lines!" << std::endl;
				std::cerr << points[next_vertex].c.x << " " << points[next_vertex].c.y << " " << points[next_vertex].c.z << std::endl;
				std::cerr << points[62].c.x << " " << points[62].c.y << " " << points[62].c.z << std::endl;

				T tmp = points[next_vertex].c - points[62].c;
				tmp.print();
			}

			for (UInt i1 = 0; i1 < points[next_vertex].lines.size(); i1++) {
				if (points[next_vertex].lines[i1] != current_line) {
					current_line = points[next_vertex].lines[i1];
					break;
				}
			}
			last_vertex = next_vertex;
			//std::cin.get();

		} while (current_line != first_line);

		std::cerr << "CycleSize: " << cycleSize << "  numPoints: " << getNumV() << std::endl;
	}

// ****************************************************************************************

	void print() const {
		std::cerr << "Polyline:" << std::endl;
		std::cerr << "Points:" << std::endl;
		for (UInt i1 = 0; i1 < points.size(); i1++) {
			points[i1].c.print();
			for (UInt i2 = 0; i2 < points[i1].lines.size(); i2++) std::cerr << points[i1].lines[i2]->v0 << " <-> " << points[i1].lines[i2]->v1 << std::endl;
		}
		std::cerr << "Linesegments:" << std::endl;
		for (std::list<LineSegment>::const_iterator it = lines.begin(); it != lines.end(); it++)
			it->print();

	}

// ****************************************************************************************

	//! Returns the line (v0-v1) or NULL;
	LineSegment* getLine(UInt v0, UInt v1) {

		//std::cerr << "getLine(" << v0 << ", " << v1 << ")" << std::endl;

		for (UInt i1 = 0; i1 < points[v0].lines.size(); i1++) {
			if ((points[v0].lines[i1]->v0 == v0 && points[v0].lines[i1]->v1 == v1) || (points[v0].lines[i1]->v0 == v1 && points[v0].lines[i1]->v1 == v0))
				return points[v0].lines[i1];             
		}

		return NULL;
	}

// ****************************************************************************************

	void ComputeMinAndMaxExt(Point& min_v, Point& max_v) const {
		min_v = Point(std::numeric_limits<ValueType>::max());
		max_v = Point(-(std::numeric_limits<ValueType>::max()));

		for (UInt i1 = 0; i1 < points.size(); i1++) {
			for (UInt i2 = 0; i2 < Point::dim; i2++) {
				if (points[i1].c[i2] > max_v[i2]) max_v[i2] = points[i1].c[i2];
				if (points[i1].c[i2] < min_v[i2]) min_v[i2] = points[i1].c[i2];
			}
		}

	}

// ****************************************************************************************

	inline UInt getNumL() const {
		return (UInt)lines.size();
	}

// ****************************************************************************************

	inline UInt getNumV() const {
		return (UInt)points.size();
	}

// ****************************************************************************************

	bool readFromFile(const char* filename) {
		points.clear();
		lines.clear();

		std::ifstream fin(filename);

		if (!fin.good()) {
			std::cerr << "Error reading file in 'PolyLine<T>::readFromFile(char* filename)'" << std::endl;
			std::cerr << "Filename is: " << filename << std::endl;
			return false;
		}

		// read header:
		UInt numV, numL, dim;
		fin >> numV >> numL >> dim;

		if (dim != Point::dim) {
			std::cerr << "Error reading file in 'PolyLine<T>::readFromFile(char* filename)'" << std::endl;
			std::cerr << "Line vertices are of dimension " << dim << " PolyLine is of dimension " << Point::dim << std::endl;
			return false;
		}

		Point v;
		for (UInt i1 = 0; i1 < numV; i1++) {
			fin >> v;
			//v.print();
			insertVertex(v);
		}

		UInt v0, v1;
		for (UInt i1 = 0; i1 < numL; i1++) {
			fin >> v0 >> v1;
			//std::cerr << v0 << " " << v1 << std::endl;
			insertLine(v0, v1);
		}
		return true;
	}

// ****************************************************************************************

	void writeToOBJ(const char* filename) const {

		std::ofstream fout(filename);

		if (!fout.good()) {
			std::cerr << "Error writing file in 'PolyLine<T>::writeToOBJ(char* filename)'" << std::endl;
			std::cerr << "File: " << filename << " not good" << std::endl;
			return;
		}


		// Write points:
		for (UInt i1 = 0; i1 < getNumV(); i1++) {
			fout << "v ";
			for (UInt i2 = 0; i2 < Point::dim; i2++) {
				fout << points[i1].c[i2] << " ";
			}
			fout << " 0" << std::endl;
		}

		// Write lines:
		for (std::list<LineSegment>::const_iterator it = lines.begin(); it != lines.end(); ++it)
			fout << it->v0 << " " << it->v1 << std::endl;

		fout.close();

	}

// ****************************************************************************************

	void writeToFile(const char* filename) {

		std::ofstream fout(filename);

		if (!fout.good()) {
			std::cerr << "Error writing file in 'PolyLine<T>::writeToFile(char* filename)'" << std::endl;
			std::cerr << "File: " << filename << " not good" << std::endl;
			return;
		}

		// write header (numV numL dim)
		fout << getNumV() << " " << (int)lines.size() << " " << Point::dim << std::endl;

		// Write points:
		for (UInt i1 = 0; i1 < getNumV(); i1++) {
			for (UInt i2 = 0; i2 < Point::dim; i2++) {
				fout << points[i1].c[i2] << " ";
			}
			fout << std::endl;
		}

		// Write lines:
		for (std::list<LineSegment>::iterator it = lines.begin(); it != lines.end(); ++it)
			fout << it->v0 << " " << it->v1 << std::endl;

		fout.close();

	}

// ****************************************************************************************

	std::vector<LineVertex<Point> > points;
	std::list<LineSegment> lines;

private:


};

// ****************************************************************************************
// ****************************************************************************************

template <class T>
PolyLine< point3d<T> >* polyLine4Dto3D(PolyLine< point4d<T> >* pl) {

	PolyLine< point3d<T> >* pl3d = new PolyLine< point3d<T> >();

	for (UInt i1 = 0; i1 < pl->getNumV(); i1++) {
		point3d<T> vert3d(pl->points[i1].c.x, pl->points[i1].c.y, pl->points[i1].c.z);
		pl3d->insertVertex(vert3d);
	}


	for (std::list<LineSegment>::iterator it = pl->lines.begin(); it != pl->lines.end(); ++it) {
		pl3d->insertLine(it->v0, it->v1);
	}

	return pl3d;
}



// ****************************************************************************************
// ****************************************************************************************

template <class T>
PolyLine<T>* removeMarkedLines(PolyLine<T>* pl_in) {

	PolyLine<T>* pl_out = new PolyLine<T>;

	std::map<UInt, UInt> newVertexIDs;
	UInt new_num = 0;
	for (UInt i1 = 0; i1 < pl_in->getNumV(); i1++) {
		if (pl_in->points[i1].isMarkedToDelete()) {
		} else {
			pl_out->insertVertex(pl_in->points[i1].c);
			newVertexIDs.insert(std::pair<UInt, UInt>(i1, new_num++));
		}
	}

	for (std::list<LineSegment>::const_iterator it = pl_in->lines.begin(); it != pl_in->lines.end(); ++it) {
		if (it->marker == 1) continue;
		UInt new_v0 = newVertexIDs[it->v0];
		UInt new_v1 = newVertexIDs[it->v1];
		pl_out->insertLine(new_v0, new_v1);
	}


	return pl_out;
}



// ****************************************************************************************
// ****************************************************************************************
	
template <class T>
double getCrossProductY(const std::vector<int>& linestrip, const std::vector<T>& verts, int idx) {

	int numL = (int)linestrip.size();
	int prev = ((idx-1)+numL)%numL;
	int succ = (idx+1)%numL;

	//std::cerr << "get angle " << verts[prev] << " " << verts[idx] << " " << verts[succ] << std::endl;

	T line1 = verts[linestrip[prev]]-verts[linestrip[idx]];
	T line2 = verts[linestrip[idx]] -verts[linestrip[succ]];
	line1.normalize();
	line2.normalize();

	return (line1^line2).y;
}

	
template <class T>
bool isConcave(const std::vector<int>& linestrip, const std::vector<T>& verts, int idx, double anglesum) {
	double angle = getCrossProductY(linestrip, verts, idx);
	return (angle/anglesum < 0);
}

	
template <class T>
void getConvexHull(const PolyLine<T>* pl_in, PolyLine<T>* ret) {

	std::vector<int> linestrip;
	std::vector<T> verts;
	for (std::list<LineSegment>::const_iterator it = pl_in->lines.begin(); it != pl_in->lines.end(); ++it) linestrip.push_back(it->v0);
	for (unsigned int i1 = 0; i1 < pl_in->points.size(); i1++) verts.push_back(pl_in->points[i1].c);


	double anglesum = 0;
	for (unsigned int i1 = 0; i1 < linestrip.size(); i1++) {
		double angle = getCrossProductY(linestrip, verts, i1);
		anglesum += angle;
	}

	bool has_changed;
	do {
		has_changed = false;
		for (int i1 = 0; i1 < (int)linestrip.size();) {
			if (isConcave(linestrip, verts, i1, anglesum)) {
				// remove i1:
				linestrip.erase(linestrip.begin()+i1);
				has_changed = true;
			} else {
				i1++;
			}
		}
	} while (has_changed);

	// insert all vertices and the new lines:
	for (unsigned int i1 = 0; i1 < verts.size(); i1++) {
		ret->insertVertex(verts[i1]);
	}
	for (unsigned int i1 = 0; i1 < linestrip.size(); i1++) {
		int next = linestrip[(i1+1)%linestrip.size()];
		ret->insertLine(linestrip[i1], next);
	}
}



template <class T>
PolyLine<T> resamplePolyline(const PolyLine<T>& pl_in, int numSamples, float& step) {

	PolyLine<T> pl_resampled;

	std::vector<UInt> cycle = pl_in.getCycle();

	typename T::ValueType length = 0;
	for (unsigned int i1 = 0; i1 < cycle.size(); i1++) {
		length += pl_in.points[cycle[i1]].c.dist(pl_in.points[cycle[(i1+1)%cycle.size()]].c);
	}

	std::vector<T> new_points;

	typename T::ValueType segmentLength = length / numSamples;
	step = segmentLength;
	typename T::ValueType next_at_dist = 0;
	typename T::ValueType position = 0;
	// run over all lines and sample uniformly
	for (unsigned int i1 = 0; i1 < cycle.size(); i1++) {

		T v0  = pl_in.points[cycle[i1]].c;
		T v1  = pl_in.points[cycle[(i1+1)%cycle.size()]].c;
		T dir = v1-v0;
		typename T::ValueType segment_length = dir.length();
		dir.normalize();

		typename T::ValueType u_first_point = position;
		typename T::ValueType u_second_point = position+segment_length;

		if (u_second_point < next_at_dist) { // no point on this segment
			position = u_second_point;
			continue;
		}

		// else, sample segment
		while (u_second_point > next_at_dist) {
			typename T::ValueType dist_to_walk = next_at_dist-u_first_point;
			T next_point = v0+dir*dist_to_walk;
			new_points.push_back(next_point);
			next_at_dist += segmentLength;
		}
		position = u_second_point;
	}

	for (UInt i1 = 0; i1 < new_points.size(); i1++) pl_resampled.insertVertex(new_points[i1]);
	for (UInt i1 = 0; i1 < new_points.size(); i1++) pl_resampled.insertLine(i1, (i1+1)%new_points.size());

	return pl_resampled;
}


#endif
