#ifndef VOLUME_TO_IV_H
#define VOLUME_TO_IV_H

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoQuadMesh.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoTexture2.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoTextureCoordinate2.h>
#include <limits>

#include "Volume.h"
#include "hsv2rgb.h"
#include "RGB_Image.h"


class ColorRampDiss1Color {
public:
	static vec3ui getColor(double s, double min_, double max_) {


		max_ = max_*0.7;

		double u = (s-min_)/(max_-min_);

		if (u > 1) u = 1;

		vec3d blackcolor(0.12157, 0.2863, 0.4902);
		vec3d whitecolor(1,1,1);

		vec3d rgb = blackcolor*(1-u)+whitecolor*(u);
		rgb *= 255;
		vec3ui rgb2((UInt)rgb.x, (UInt)rgb.y, (UInt)rgb.z);

		return rgb2;
	}

};

class ColorRampRedToBlueLinear {
public:
	static vec3ui getColor(double s, double max_, double min_) {


		double u = (s-min_)/(max_-min_);
		s = u;

		//s *= 0.4;
		s *= 0.7;


		vec3d rgb = JBSlib::hsv2rgb<double>(s, 0.8, 0.8);
		rgb *= 255;
		vec3ui rgb2((UInt)rgb.x, (UInt)rgb.y, (UInt)rgb.z);

		return rgb2;
	}

};

class ColorRampRedToBlueLinearLessSat {
public:
	static vec3ui getColor(double s, double max_, double min_) {


		double u = (s-min_)/(max_-min_);
		s = u;

		//s *= 0.4;
		s *= 0.7;


		double sat = 0.3+(0.6-std::min(s*0.9, 0.6));

		sat = std::max(sat, 0.3);
		sat = std::min(sat, 0.9);

		vec3d rgb = JBSlib::hsv2rgb<double>(s, sat, 0.8);
		rgb *= 255;
		vec3ui rgb2((UInt)rgb.x, (UInt)rgb.y, (UInt)rgb.z);

		return rgb2;
	}

};

class ColorRampDiss {
public:
	static vec3ui getColor(double s, double max_, double min_) {

		if (s < min_) s = min_;
		if (s > max_) s = max_;

		double u = (s-min_)/(max_-min_);

		//std::cerr << u << " ";

		vec3d c1(29,68,130);
		vec3d c2(255,90,55);
		vec3d col;

		if (u < 0.5) {
			u *= 2;
			col = c1*(1-u) + vec3d(255,255,255)*u;
		} else {
			u -= 0.5;
			u *= 2;
			col = vec3d(255,255,255)*(1-u) + c2*u;
		}
		vec3ui cc((UInt)col.x, (UInt)col.y, (UInt)col.z);

		return cc;
	}
};

class ColorRampRedWhiteBlue {
public:
	static vec3ui getColor(double s, double max_, double min_) {

		if (s < min_) s = min_;
		if (s > max_) s = max_;

		double u = (s-min_)/(max_-min_);

		//std::cerr << u << " ";

		vec3d col;

		if (u < 0.5) {
			u *= 2;
			col = vec3d(0,0,1)*(1-u) + vec3d(1,1,1)*u;
		} else {
			u -= 0.5;
			u *= 2;
			col = vec3d(1,1,1)*(1-u) + vec3d(1,0,0)*u;
		}
		col *= 255;
		vec3ui cc((UInt)col.x, (UInt)col.y, (UInt)col.z);

		return cc;
	}
};

class ColorRampRedToBlue {
public:
	static vec3ui getColor(double s, double max_, double min_) {


		double u = (s-min_)/(max_-min_);
		s = u;

		u = std::max<double>(0.0, u);
		u = std::min<double>(1.0, u);

		for (int i = 0; i < 0; i++) {
			s = u;
			u = -2*s*s*s + 3*s*s;
		}

		double min = -6.0/18.0;
		double max = 1/36.0;

		double mm = max-min;
		double uu = min + (mm)*u;

		if (uu < 0) uu += 1.0;

		double u2 = 1;
		if ((u > 0.4) && (u < 0.6))
			u2 = fabs(u-0.5)*10.0;


		vec3d rgb = JBSlib::hsv2rgb<double>(uu, u2*0.8, 0.8);
		rgb *= 255;
		vec3ui rgb2((UInt)rgb.x, (UInt)rgb.y, (UInt)rgb.z);

		return rgb2;
	}
};

class ColorRampHSV2 {
public:
	static vec3ui getColor(double s, double max_, double min_) {

		s = std::max<double>(min_, s);
		s = std::min<double>(max_, s);

		double u = (s-min_)/(max_-min_);

		UInt numIter = 2;
		for (UInt i = 0; i < numIter; i++) {
			s = u;
			u = -2*s*s*s + 3*s*s;
		}

		double min = 0.0;
		double max = 2.0/3.0;

		double mm = max-min;
		double uu = min + (mm)*u;

		//if (uu < 0) uu += 1.0;

		double u2 = 1;
		//if ((u > 0.35) && (u < 0.65))
		//	u2 = fabs(u-0.5)*10/1.5;

		vec3d rgb = JBSlib::hsv2rgb<double>(uu, u2*0.75, 1);
		rgb *= 255;
		vec3ui rgb2((UInt)rgb.x, (UInt)rgb.y, (UInt)rgb.z);

		return rgb2;
	}
};

class ColorRampHSV {
public:
	static vec3ui getColor(double s, double max, double min) {
		
		double u = (s-min)/(max-min);
		
		vec3d rgb = JBSlib::hsv2rgb<double>(u, 1, 1);
		rgb *= 255;
		vec3ui rgb2((UInt)rgb.x, (UInt)rgb.y, (UInt)rgb.z);

		return rgb2;
	}
};


template <class T, class ColorRamp>
SoSeparator* getSliceImage(Volume<T>* vol, int sliceID, int dir, T min = 0, T max = 0, RGBImage* img = NULL) {

	SoSeparator* sep = new SoSeparator;

	vec3f minV = vol->getMin();
	vec3f maxV = vol->getMax();

	vec3f p0, p1, p2, p3;

	vec3uc* tex_data;

	vec2ui tex_dim;

	vec3ui dim(vol->dx, vol->dy, vol->dz);
	if (dir == 0) {
		vec3f onplane = vol->pos(sliceID, 0,0);
		float x = onplane.x;
		p0 = vec3f(x, minV.y, minV.z);
		p1 = vec3f(x, minV.y, maxV.z);
		p2 = vec3f(x, maxV.y, minV.z);
		p3 = vec3f(x, maxV.y, maxV.z);

		if (min == 0 && max == 0) {
			max = -std::numeric_limits<T>::max();
			min = std::numeric_limits<T>::max();
			for (UInt i1 = 0; i1 < vol->dy; i1++) {
				for (UInt i2 = 0; i2 < vol->dz; i2++) {
					T val = vol->get(sliceID, i1, i2);
					max = std::max(max, val);
					min = std::min(min, val);
				}
			}
		}
		T mm = max-min;

		std::cerr << "dir = 2 min: " << min << "   max: " << max << std::endl;

		tex_data = new vec3uc[vol->dy*vol->dz];
		for (UInt i1 = 0; i1 < vol->dy; i1++) {
			for (UInt i2 = 0; i2 < vol->dz; i2++) {
				T val = vol->get(sliceID, i1, i2);

				//T mm2 = std::min<T>(fabs(min), fabs(max));

				vec3ui rgb2 = ColorRamp::getColor(val, max, min);
				tex_data[i1+i2*vol->dy] = rgb2;
			}
		}
		tex_dim = vec2ui(vol->dy, vol->dz);
	}
	if (dir == 1) {
		vec3f onplane = vol->pos(0, sliceID,0);
		float y = onplane.y;
		p0 = vec3f(minV.x, y, minV.z);
		p1 = vec3f(minV.x, y, maxV.z);
		p2 = vec3f(maxV.x, y, minV.z);
		p3 = vec3f(maxV.x, y, maxV.z);

		if (min == 0 && max == 0) {
			max = -std::numeric_limits<T>::max();
			min = std::numeric_limits<T>::max();
			for (UInt i1 = 0; i1 < vol->dx; i1++) {
				for (UInt i2 = 0; i2 < vol->dz; i2++) {
					T val = vol->get(i1, sliceID, i2);
					max = std::max(max, val);
					min = std::min(min, val);
				}
			}
		}
		T mm = max-min;

		std::cerr << "dir = 1 min: " << min << "   max: " << max << std::endl;

		tex_data = new vec3uc[vol->dx*vol->dz];
		for (UInt i1 = 0; i1 < vol->dx; i1++) {
			for (UInt i2 = 0; i2 < vol->dz; i2++) {
				T val = vol->get(i1, sliceID, i2);

				//T mm2 = std::min<T>(fabs(min), fabs(max));

				//std::cerr << "min: " << min << "   max: " << max << std::endl;

				vec3ui rgb2 = ColorRamp::getColor(val, max, min);
				tex_data[i1+i2*vol->dx] = rgb2;
			}
		}
		tex_dim = vec2ui(vol->dx, vol->dz);
	}
	if (dir == 2) {
		vec3f onplane = vol->pos(0,0,sliceID);
		float z = onplane.z;
		p0 = vec3f(minV.x, minV.y, z);
		p1 = vec3f(minV.x, maxV.y, z);
		p2 = vec3f(maxV.x, minV.y, z);
		p3 = vec3f(maxV.x, maxV.y, z);

		if (min == 0 && max == 0) {
			max = -std::numeric_limits<T>::max();
			min = std::numeric_limits<T>::max();
			for (UInt i1 = 0; i1 < vol->dx; i1++) {
				for (UInt i2 = 0; i2 < vol->dy; i2++) {
					T val = vol->get(i1, i2, sliceID);
					max = std::max(max, val);
					min = std::min(min, val);
				}
			}
		}
		//min = -15;
		T mm = max-min;

		if (img != NULL) img->resize(vol->dx, vol->dy);

		std::cerr << "dir = 2 min: " << min << "   max: " << max << std::endl;

		tex_data = new vec3uc[vol->dx*vol->dy];
		for (UInt i1 = 0; i1 < vol->dx; i1++) {
			for (UInt i2 = 0; i2 < vol->dy; i2++) {
				T val = vol->get(i1, i2, sliceID);

				vec3ui rgb2 = ColorRamp::getColor(val, max, min);

				tex_data[i1+i2*vol->dx] = rgb2;
				if (img != NULL) img->setPixel(i1,i2, vec3ui((UInt)rgb2.x, (UInt)rgb2.y, (UInt)rgb2.z));
			}
		}
		tex_dim = vec2ui(vol->dx, vol->dy);
	}


	SoLightModel* lm = new SoLightModel;
	lm->model = SoLightModel::BASE_COLOR;
	sep->addChild(lm);

	SoTexture2 *myTexture = new SoTexture2();
	myTexture->image.setValue(SbVec2s(tex_dim.x, tex_dim.y), 3, (const unsigned char*)tex_data);
	sep->addChild(myTexture);

	SoTextureCoordinate2* texcoord = new SoTextureCoordinate2;
	texcoord->point.set1Value(0, SbVec2f(0,0));
	texcoord->point.set1Value(1, SbVec2f(1,0));
	texcoord->point.set1Value(2, SbVec2f(0,1));
	texcoord->point.set1Value(3, SbVec2f(1,1));
	sep->addChild(texcoord);

    SoCoordinate3* coords = new SoCoordinate3;
	coords->point.set1Value(0, p0);
	coords->point.set1Value(2, p1);
	coords->point.set1Value(1, p2);
	coords->point.set1Value(3, p3);


	sep->addChild(coords);
    SoQuadMesh* mesh = new SoQuadMesh;
    mesh->verticesPerRow = 2;
    mesh->verticesPerColumn = 2;
	sep->addChild(mesh);

	return sep;
}


template <class T>
void writeSliceToImage(Volume<T>* vol, char* filename, int sliceID=0, float hsv_h_scaler = 0.66, bool invert_color_scale=false) {

	T max = -std::numeric_limits<T>::max();
	T min = std::numeric_limits<T>::max();

	// Get min and max entries.
	for (UInt i1 = 0; i1 < vol->dx; i1++) {
		for (UInt i2 = 0; i2 < vol->dy; i2++) {
			T val = vol->get(i1, i2, sliceID);
			max = std::max(max, val);
			min = std::min(min, val);
		}
	}
	T mm = max-min;
	RGBImage img(vol->dx, vol->dy);

	for (UInt i1 = 0; i1 < vol->dx; i1++) {
		for (UInt i2 = 0; i2 < vol->dy; i2++) {
			T val = vol->get(i1, i2, sliceID);
			T u = (val-min)/mm;
			u *= hsv_h_scaler;
			if (invert_color_scale) u = hsv_h_scaler-u;

			point3d<T> rgb = JBSlib::hsv2rgb<T>(u, 1, 1);
			rgb *= 255;
			vec3ui rgb2((UInt)rgb.x, (UInt)rgb.y, (UInt)rgb.z);
			img.setPixel(i1,i2, vec3ui((UInt)rgb.x, (UInt)rgb.y, (UInt)rgb.z));
		}
	}

	img.writeRAW(filename);
	std::cerr << "Wrote " << vol->dx << " * " << vol->dy << " image" << std::endl;

}

#endif
