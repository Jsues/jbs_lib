#ifndef SCAPE_REGISTER_H
#define SCAPE_REGISTER_H

#include "OFFReader.h"

#define NO_GSL
#include "SCAPE.h"
#include "VARAP_Register.h"
#include "levmar.h"
#include "PointCloud.h"
#include "operators/computeVertexNormals.h"



// ****************************************************************************************


class SCAPE_Register {

public:

struct LevMarPayload {

	LevMarPayload() : cnt(0), weightSCAPE(1.0), inOutScaler(1.0), poseFixed(false) {}

	int cnt;
	SCAPE_Model* sm;
	int numEV;
	std::vector<vec3d>* verts;
	std::vector<vec3d>* target_normals;
	std::vector<double> init_fit;
	std::vector<bool>* skin_verts;
	double weightSCAPE;
	double inOutScaler;
	bool poseFixed;

	//! end-start should be 'm'
	int start;
	int end;
};

// ****************************************************************************************

SCAPE_Register(SCAPE_Model& sm_, PointCloudNormals<vec3f>* target_pcn_, std::vector<vec3d>& restpose_verts_) : sm(sm_), target_pcn(target_pcn_), restpose_verts(restpose_verts_) {
	vm.points = restpose_verts;
	vm.tris = sm.default_mesh_triangles;

	VARAP_Pointcloud vpc;
	vpc.points = target_pcn->m_pts;
	vpc.normals = target_pcn->m_normals;

	vr = new VARAP_Register(vm, vpc);

	cnt = 0;
}

// ****************************************************************************************

static void evalGoodnessOfFit(double *p, double *x, int m, int n, void *dvoid)
{

	LevMarPayload* data = (LevMarPayload*)dvoid;
	SCAPE_Model* sm = data->sm;

	std::cerr << "Evaluating " << data->cnt++ << " ...";

	// Vector params is larger that the number of unknows as it must contain zeros for the small eigenvectors
	for (int i1 = 0; i1 < data->end-data->start; i1++) {
		data->init_fit[i1+data->start] = p[i1];
	}
	std::vector<vec3d> newverts = sm->reconstructFromSCAPEParams(data->init_fit);

	sm->rigidlyAlignModel(newverts, *(data->verts));

	double err = 0;
	for (UInt i1 = 0; i1 < ((LevMarPayload*)data)->verts->size(); i1++) {
		const vec3d& sourcev = newverts[i1];
		const vec3d& targetv = (*data->verts)[i1];
		const vec3d& targetn = (*data->target_normals)[i1];
		x[i1] = (targetn|(sourcev-targetv));
		if ((x[i1] > 0) || ((*data->skin_verts)[i1])) x[i1] *= data->inOutScaler;
		err += x[i1]*x[i1];
	}

	int offset = sm->numParts*3;
	int last = (data->poseFixed) ? m : m-offset;


	std::cerr << "data->weightSCAPE: " << data->weightSCAPE << std::endl;

	double errSCAPE = 0;
	for (int i1 = 0; i1 < last; i1++) {
		// divide component by standard deviation
		double val = data->init_fit[i1+offset] / sqrt(sm->pcabasis.EVals[i1]);
		errSCAPE += (val*data->weightSCAPE)*(val*data->weightSCAPE);
		x[((LevMarPayload*)data)->verts->size()+i1] = val*data->weightSCAPE;
	}
	std::cerr << "done\n err: " << err+errSCAPE << "\t err fit = " << err << "\t errScape: " << errSCAPE << std::endl;

	std::cerr << data->poseFixed << std::endl;
	for (int i1 = 0; i1 < 10; i1++) std::cerr << p[i1] << "  " << data->init_fit[i1+offset] << std::endl;

}

// ****************************************************************************************

void levMarFit(std::vector<vec3d>& target_verts, int numIter = 10, bool insideoutsidetest = false, bool poseFixed = false, std::vector<bool>* skin_verts = NULL) {


	int numEV = 10;
	int numEqns = target_verts.size()+numEV;

	std::vector<vec3d> target_normals;
	target_normals = computeVertexNormals(target_verts, vm.tris);

	LevMarPayload pl;
	pl.sm = &sm;
	pl.numEV = numEV;
	pl.verts = &target_verts;
	pl.target_normals = &target_normals;
	pl.weightSCAPE = 100.0;
	pl.weightSCAPE = 2000.0;
	pl.inOutScaler = (insideoutsidetest) ? 10.0 : 1.0;
	pl.poseFixed = poseFixed;
	pl.start = (poseFixed) ? sm.numParts*3 : 0;
	pl.end = sm.numParts*3+numEV;

	if (skin_verts == NULL) pl.skin_verts = new std::vector<bool>(target_verts.size());
	else pl.skin_verts = skin_verts;

	if (pl.init_fit.size() == 0) {
		std::vector<vec3d> proj = sm.projectToSCAPESpace(target_verts, &(pl.init_fit));
		sm.rigidlyAlignModel(proj, target_verts);
		writeOFFFile(proj, sm.default_mesh_triangles, "proj.off");
	}
	// Cancel out influence of small eigenvectors
	for (UInt i1 = pl.end; i1 < pl.init_fit.size(); i1++) pl.init_fit[i1] = 0;

	int numUnknown = (poseFixed) ? numEV : numEV+sm.numParts*3;
	std::vector<double> unknowns(numUnknown);

	for (int i1 = 0; i1 < pl.end-pl.start; i1++) unknowns[i1] = pl.init_fit[i1+pl.start];


	// Levenberg marquard
	double info[LM_INFO_SZ];
	int ret;

	// 3rd argument is NULL because we want the single errors to be 0 each
	ret=dlevmar_dif(evalGoodnessOfFit, &unknowns[0], NULL, numUnknown, numEqns, numIter, NULL, info, NULL, NULL, &pl);
	printf("Levenberg-Marquardt returned in %g iter, reason %g, sumsq %g [%g]\n", info[5], info[6], info[1], info[0]);

	//for (int i1 = 0; i1 < sm.pcabasis.numVecs; i1++) {
	//	// divide component by standard deviation
	//	double val = initguess[i1+sm.numParts*3] / sqrt(sm.pcabasis.EVals[i1]);
	//	std::cerr << initguess[i1+sm.numParts*3] << " \t " << val << std::endl;
	//}


	std::vector<vec3d> new_verts = sm.reconstructFromSCAPEParams(pl.init_fit);

	sm.rigidlyAlignModel(new_verts, target_verts);

	target_verts = new_verts;

	writeOFFFile(new_verts, sm.default_mesh_triangles, "lm.off");

}

// ****************************************************************************************

void doARAPFit(std::vector<vec3d>& target_verts, std::vector<int>& activeVertices, int numIter = 1) {
	
	vr->externallyUpdateMeshVertices(target_verts);

	VARAP_Param vp;
	vp.numIterInner = 10;
	vp.numIterOuter = numIter;
	vp.stiffness = 1.0;
	vr->doSurfaceARAPRegistration(vp, activeVertices, true, cnt == 0);

	cnt++;

	target_verts = vm.points;
	writeOFFFile(target_verts, vm.tris, "aaarap.off");
}

// ****************************************************************************************

private:

	SCAPE_Model& sm;
	PointCloudNormals<vec3f>* target_pcn;

	VARAP_Mesh vm;

	VARAP_Pointcloud vpc;

	VARAP_Register* vr;

	int cnt;

public:

	std::vector<vec3d>& restpose_verts;

};


#endif
