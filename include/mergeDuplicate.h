#ifndef MERGE_DUPLICATE_MESH_H
#define MERGE_DUPLICATE_MESH_H

#include <map>

#include "point3d.h"

struct sortableVec3d {

	// ****************************************************************************************

	inline bool operator<(const sortableVec3d& other) const {
		if (x < other.x) return true;
		if (x > other.x) return false;

		if (y < other.y) return true;
		if (y > other.y) return false;

		if (z < other.z) return true;
		if (z > other.z) return false;

		return false;
	}

	union {
		struct {
			double x,y,z;          // standard names for components
		};
		double array[3];     // array access
	};
};


template <class T>
void mergeDuplicate(const std::vector<T>& verts_in, const std::vector<vec3i>& tris_in, std::vector<T>& verts_out, std::vector<vec3i>& tris_out) {

	
	std::vector<sortableVec3d> verts;
	verts.resize(verts_in.size());
	for (unsigned int i1 = 0; i1 < verts_in.size(); i1++) {
		const T& v = verts_in[i1];
		verts[i1].x = v.x;
		verts[i1].y = v.y;
		verts[i1].z = v.z;
	}

	std::map<sortableVec3d, int> vertmap;
	std::vector<int> vertToIDX(verts.size());

	for (unsigned int i1 = 0; i1 < verts.size(); i1++) {
		const sortableVec3d& p = verts[i1];

		std::map<sortableVec3d, int>::const_iterator it = vertmap.find(p);
		if (it == vertmap.end()) {
			int idx = vertmap.size();
			vertmap.insert(std::make_pair(p, idx));
			vertToIDX[i1] = idx;
		} else {
			int idx = it->second;
			vertToIDX[i1] = idx;
		}
	}

	std::cerr << "Kept " << vertmap.size() << " of " << verts.size() << " vertices" << std::endl;
	verts_out.resize(vertmap.size());
	for (std::map<sortableVec3d, int>::const_iterator it = vertmap.begin(); it != vertmap.end(); ++it) {
		verts_out[it->second].x = (T::ValueType)(it->first.x);
		verts_out[it->second].y = (T::ValueType)(it->first.y);
		verts_out[it->second].z = (T::ValueType)(it->first.z);
	}

	for (unsigned int i1 = 0; i1 < tris_in.size(); i1++) {
		vec3i t = tris_in[i1];

		t.x = vertToIDX[t.x];
		t.y = vertToIDX[t.y];
		t.z = vertToIDX[t.z];

		if (t.x == t.y) continue;
		if (t.x == t.z) continue;
		if (t.y == t.z) continue;

		tris_out.push_back(t);
	}
}


template <class T>
void mergeDuplicate(const std::vector<T>& verts_in, const std::vector<vec2i>& lines_in, std::vector<T>& verts_out, std::vector<vec2i>& lines_out) {

	
	std::vector<sortableVec3d> verts;
	verts.resize(verts_in.size());
	for (unsigned int i1 = 0; i1 < verts_in.size(); i1++) {
		const T& v = verts_in[i1];
		verts[i1].x = v.x;
		verts[i1].y = v.y;
		verts[i1].z = v.z;
	}

	std::map<sortableVec3d, int> vertmap;
	std::vector<int> vertToIDX(verts.size());

	for (unsigned int i1 = 0; i1 < verts.size(); i1++) {
		const sortableVec3d& p = verts[i1];

		std::map<sortableVec3d, int>::const_iterator it = vertmap.find(p);
		if (it == vertmap.end()) {
			int idx = vertmap.size();
			vertmap.insert(std::make_pair(p, idx));
			vertToIDX[i1] = idx;
		} else {
			int idx = it->second;
			vertToIDX[i1] = idx;
		}
	}

	verts_out.resize(vertmap.size());
	for (std::map<sortableVec3d, int>::const_iterator it = vertmap.begin(); it != vertmap.end(); ++it) {
		verts_out[it->second].x = (T::ValueType)(it->first.x);
		verts_out[it->second].y = (T::ValueType)(it->first.y);
		verts_out[it->second].z = (T::ValueType)(it->first.z);
	}

	for (unsigned int i1 = 0; i1 < lines_in.size(); i1++) {
		vec2i l = lines_in[i1];

		l.x = vertToIDX[l.x];
		l.y = vertToIDX[l.y];

		if (l.x == l.y) continue;

		lines_out.push_back(l);
	}
}

#endif
