#ifndef BOOLEAN_TREE_H
#define BOOLEAN_TREE_H


#include <vector>
#include <string>


#include "point3d.h"
#include "point2d.h"


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class BooleanTreeNode {

public:

// ****************************************************************************************

	typedef T Point;
	typedef typename T::ValueType ValueType;
	const static UInt dim = Point::dim;

// ****************************************************************************************

	//! Destructor.
	virtual ~BooleanTreeNode() {
		std::cerr << "Destructor BooleanTreeNode" << std::endl;
	}

// ****************************************************************************************

	virtual inline ValueType eval(const Point& pos) = 0;

// ****************************************************************************************

	virtual inline Point getCenter() = 0;

// ****************************************************************************************

	virtual inline UInt getSize() = 0;

// ****************************************************************************************

	virtual inline Point getNormal() = 0;

// ****************************************************************************************

	virtual inline void print(UInt indent=0) = 0;
};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class BooleanTreeNode_Plane : public BooleanTreeNode<T> {
	
public:

// ****************************************************************************************

	typedef T Point;
	typedef typename T::ValueType ValueType;
	const static UInt dim = Point::dim;

	//! Default Constructor.
	BooleanTreeNode_Plane() {}

// ****************************************************************************************

	//! Copy Constructor.
	BooleanTreeNode_Plane(const BooleanTreeNode_Plane& other) {
		normal = other.normal;
		center = other.center;
		d = other.d;
	}

// ****************************************************************************************

	//! Constructs a plane at 'center_' with normal 'normal_'.
	BooleanTreeNode_Plane(const Point& center_, const Point& normal_) {
		center = center_;
		normal = normal_;
		normal.normalize();
		d = (center|normal);
	}

// ****************************************************************************************

	//! Destructor.
	virtual ~BooleanTreeNode_Plane() {
		std::cerr << "Destructor BooleanTreeNode_Plane" << std::endl;
	}

// ****************************************************************************************

	//! Computes the distance between 'pos' and the plane.
	virtual inline ValueType eval(const Point& pos) {
		return (normal|pos)-d;
	}

// ****************************************************************************************

	virtual inline Point getNormal() {
		return normal;
	};

// ****************************************************************************************

	virtual inline UInt getSize() {
		return 1;
	};

// ****************************************************************************************

	virtual inline Point getCenter() {
		return center;
	};

// ****************************************************************************************

	virtual inline void print(UInt indent=0) {
		for (UInt i1 = 0; i1 < indent; i1++) std::cerr << " ";
		std::cerr << "Plane c: (" << center << ") & n: (" << normal << ")" << std::endl;
	};

// ****************************************************************************************

private:

	Point normal;
	Point center;
	ValueType d;

};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class BooleanTreeNode_Group : public BooleanTreeNode<T> {

public:

// ****************************************************************************************

	typedef T Point;
	typedef typename T::ValueType ValueType;
	const static UInt dim = Point::dim;
	typedef typename BooleanTreeNode<Point> Node;

// ****************************************************************************************

	//! Constructor.
	BooleanTreeNode_Group(bool is_convex) {
		convex = is_convex;
	}

// ****************************************************************************************

	void addChild(Node* child) {
		children.push_back(child);
	}

// ****************************************************************************************

	//! Destructor.
	virtual ~BooleanTreeNode_Group() {
		std::cerr << "Destructor BooleanTreeNode_Group" << std::endl;
	}

// ****************************************************************************************

	//! Computes the distance between 'pos' and the plane.
	virtual inline ValueType eval(const Point& pos) {

		UInt numChildren = (UInt) children.size();

		if (numChildren == 0) return 0;

		ValueType ret = children[0]->eval(pos);

		for (UInt i1 = 1; i1 < numChildren; i1++) {
			ValueType d = children[i1]->eval(pos);
			if ((d > ret) == convex) ret = d;
		}

		return ret;
	}

// ****************************************************************************************

	virtual inline UInt getSize() {
		UInt local_size = 0;
		for (UInt i1 = 0; i1 < children.size(); i1++) local_size += children[i1]->getSize();
		return local_size;
	};

// ****************************************************************************************

	virtual inline Point getNormal() {
		Point n;
		for (UInt i1 = 0; i1 < children.size(); i1++) n += children[i1]->getNormal();

		n.normalize();

		return n;
	};

// ****************************************************************************************

	virtual inline Point getCenter() {
		Point center;

		for (UInt i1 = 0; i1 < children.size(); i1++) center += children[i1]->getCenter();

		center /= children.size();

		return center;
	};

// ****************************************************************************************

	virtual inline void print(UInt indent=0) {
		for (UInt i1 = 0; i1 < indent; i1++) std::cerr << " ";
		std::string type = (convex) ? "convex " : "concave ";
		std::cerr << type << "group with " << (UInt)children.size() << " children: " << std::endl;
		for (UInt i1 = 0; i1 < (UInt)children.size(); i1++) {
			children[i1]->print(indent+2);
		}
	};

// ****************************************************************************************

	void setConvex(bool c) {
		convex = c;
	}

// ****************************************************************************************

	std::vector<Node*> children;

private:

	bool convex;

};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************



// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

#endif

