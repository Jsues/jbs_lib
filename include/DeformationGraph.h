#ifndef DEFORMATION_GRAPH_H
#define DEFORMATION_GRAPH_H

#include <vector>
#include "point3d.h"
#include "MatrixnD.h"

static const UInt numNeighbors = 4;

struct nodeWeight {
	UInt nodeId[numNeighbors];
	float weight[numNeighbors];
};


typedef std::vector<UInt> idxList;
typedef SquareMatrixND<vec3f> affineTransformation;

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

//! A node of the deformation graph.
class DeformationGraphNode {

public:

	DeformationGraphNode() {
		trans.setToIdentity();
	};

	DeformationGraphNode(vec3f initial_position_) {
		initial_position = initial_position_;
		trans.setToIdentity();
	};

	//! The original 3d position of the node
	vec3f initial_position;

	//! The affine 'quasi-rigid' transformation.
	affineTransformation trans;

	//! The inverse transpose of the transformation trans.
	affineTransformation transInvTrans;

	//! The nodes translation.
	vec3f translation;

	//! List of edges connecting this node to neighboring nodes.
	idxList edges;

private:

};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

//! An edge of the deformation graph.
class DeformationGraphEdge {

public:

	DeformationGraphEdge() {};
	DeformationGraphEdge(UInt v0_, UInt v1_) { v0 = v0_; v1 = v1_; };

	inline bool isEdge(UInt v0_, UInt v1_) const {
		if ((v0_ == v0) && (v1_ == v1)) return true;
		return false;
	}

	//! End-Node 1.
	UInt v0;

	//! End-Node 2.
	UInt v1;

private:

};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

//! Deformation graph as introduced in the paper Bob Sumner et al. "Embedded Deformation for Shape Manipulation".
class DeformationGraph {

public:

	//! Constructor.
	DeformationGraph() {
		nw = NULL;
		global_trans.setToIdentity();
	}

	//! Destructor.
	~DeformationGraph() {
		if (nw != NULL) delete[] nw;
	}

	//! Inserts a node at position 'pos' into the graph. The initial transformation is set to identity.
	void insertNode(vec3f pos) {
		DeformationGraphNode node(pos);
		nodes.push_back(node);
	}

	//! Inserts an edge connection nodes 'v0' and 'v1'.
	void insertEdge(UInt v0, UInt v1) {

		if (v0 > v1) {
			UInt tmp = v0;
			v0 = v1;
			v1 = tmp;
		}
		DeformationGraphEdge edge(v0, v1);
		UInt edgeID = (UInt)edges.size();
		edges.push_back(edge);
		nodes[v0].edges.push_back(edgeID);
		nodes[v1].edges.push_back(edgeID);
	}

	//! Checks if the edge ('v0', 'v1') is contained in the deformation graph.
	inline bool hasEdge(UInt v0, UInt v1) const {

		if (v0 > v1) {
			UInt tmp = v0;
			v0 = v1;
			v1 = tmp;
		}

		for (UInt i1 = 0; i1 < nodes[v0].edges.size(); i1++) {
			UInt eID = nodes[v0].edges[i1];
			if (edges[eID].isEdge(v0, v1)) return true;
		}
		return false;
	}

	//! Returns the number of nodes in the graph.
	UInt getNumNodes() {
		return (UInt)nodes.size();
	}

	//! Returns the number of edges in the graph.
	UInt getNumEdges() {
		return (UInt)edges.size();
	}

	//! Returns a point cloud of the nodes positions.
	PointCloud<vec3f>* getGraphPC() {
		PointCloud<vec3f>* pc = new PointCloud<vec3f>;
		for (UInt i1 = 0; i1 < getNumNodes(); i1++) {
			pc->insertPoint(nodes[i1].initial_position);
		}
		return pc;
	}

	//! Returns the point cloud transformed by the current deformation graph.
	PointCloudNormals<vec3f>* getDeformedPC() {

		PointCloudNormals<vec3f>* finalPC = new PointCloudNormals<vec3f>;

		for (UInt i1 = 0; i1 < (UInt)dataPC->getNumPts(); i1++) {
			vec3f p = dataPC->getPoints()[i1];
			vec3f n = dataPC->getNormals()[i1];
			// compute local transformation:
			vec3f p_local;
			vec3f n_local;
			for (UInt i2 = 0; i2 < numNeighbors; i2++) {
				UInt nodeID = nw[i1].nodeId[i2];
				float weight = nw[i1].weight[i2];
				const DeformationGraphNode& current_node = nodes[nodeID];
				p_local += (current_node.trans.vecTrans(p-current_node.initial_position)+current_node.initial_position+current_node.translation)*weight;
				n_local += (current_node.transInvTrans.vecTrans(n))*weight;
			}
			p = global_trans.vecTrans(p_local-com)+com+global_translation;
			n = global_trans_inverse_transposed.vecTrans(n_local);
			n.normalize();
			finalPC->insertPoint(p,n);
		}

		return finalPC;
	}

	//! Computes the inverse transpose of the nodes transformations for all nodes.
	void computeInverseTransformations() {
		for (UInt i1 = 0; i1 < nodes.size(); i1++) {
			nodes[i1].transInvTrans = nodes[i1].trans.getInverted();
			nodes[i1].transInvTrans.transpose();
		}
		global_trans_inverse_transposed = global_trans.getInverted();
		global_trans_inverse_transposed.transpose();
	}

	//! vector of nodes.
	std::vector<DeformationGraphNode> nodes;

	//! vector of edges.
	std::vector<DeformationGraphEdge> edges;

	//! Unmodified input point cloud.
	PointCloudNormals<vec3f>* dataPC;

	//! One nodeWeigth for each point in dataPC, holding the IDs of the nodes which influence the point and the corresponding weights.
	nodeWeight* nw;

	//! initial center of mass
	vec3f com;

private:

	//! Global rigid/affine transformation.
	affineTransformation global_trans;
	//! Global rigid/affine transformation.
	affineTransformation global_trans_inverse_transposed;
	//! Global translation.
	vec3f global_translation;

};

#endif
