#ifndef PROJ_MESH_ONTO_IMPLICIT
#define PROJ_MESH_ONTO_IMPLICIT

#include "ScalarFunction.h"

template <class Point>
bool projPointOntoImplicit(Point& pt, ScalarFunction<Point>* func, typename Point::ValueType convergence_epsilon = (typename Point::ValueType)0.01) {


	typedef typename Point::ValueType ValueType;

	Point dir;
	ValueType val = func->evalValAndGrad(pt, dir);

	if (val >= 10000.0) {
		return false;
	}


	int num_it = 0;
	while (fabs(val) > convergence_epsilon) {
		dir.normalize();
		ValueType multiplier = (val*(ValueType)0.2);
		pt -= (dir*multiplier);
		val = func->evalValAndGrad(pt, dir);
		num_it++;
		if (num_it > 1000) {
			return false;
		}
	}


	return true;
}


template <class Point>
void projMeshOntoImplicit(SimpleMesh* sm, ScalarFunction<Point>* func) {

	int numV = sm->getNumV();
	for (int i1 = 0; i1 < numV; i1++) {
		Point pt = sm->vList[i1].c;
		bool converged = projPointOntoImplicit(pt, func);
		if (converged)
			sm->vList[i1].c = pt;
	}

}

#endif
