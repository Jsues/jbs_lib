#ifndef CURVATURE_MESH_H
#define CURVATURE_MESH_H

#include "SimpleMesh.h"
#include "LineSet.h"


class CurvatureMesh : public SimpleMesh {

public:
	CurvatureMesh() : SimpleMesh() {
		curvature = NULL;

		max_curv = 0;
		min_curv = 10e10;
	};

	~CurvatureMesh() {
		if (curvature != NULL)
			delete[] curvature;
	};

	//! Computes the curvature at the vertices and stores them im SimpleMesh->Vertex->param->x.
	void ComputeVertexCurvatures();

	double* curvature;


	LineSet ComputeCurvatureIsoLines(double iso);

	double max_curv;
	double min_curv;

private:

	//! returns the estimated curvature at vertex 'v'.
	double ComputeCurvatureAt(int v, vec3d normal);

};

#endif

