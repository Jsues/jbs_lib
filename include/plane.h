#ifndef JBS_PLANE
#define JBS_PLANE

#include "point3d.h"

namespace JBSlib {
//! A plane, internally handled in <b>Hesse Normalform</b>
template <class T>
class Plane {

public:
	typedef T Point;
	typedef typename T::ValueType ValueType;

	//! Construct a plane from scalars 'a_', 'b_', 'c_', and 'd_'.
	Plane(ValueType a_, ValueType b_, ValueType c_, ValueType d_) {
		n = Point(a_, b_, c_);
		d = d_;
	};

	//! Construct a plane with a normalvector 'n_' and distance 'd_' from origin.
	Plane(Point normal, ValueType d_) {
		normal.normalize();
		n = normal;
		d = d_;
	};

	//! Copy constructor.
	Plane(const Plane& other) {
		n = other.n;
		d = other.d;
	}

	//! Construct the plane given by three points 'p0', 'p1' and 'p2'.
	Plane(Point p0, Point p1, Point p2) {
		Point normal = (p0-p1)^(p0-p2);
		normal.normalize();
		d = (p0|n);
		n = normal;
	};

	//! Compute distance between point 'p' and plane.
	inline ValueType getDistance(const Point& p) const {
		return (n|p)-d;
	};

	//! returns the normal of the plane:
	inline Point getNormal() const {
		return n;
	}

	//! returns the distance to the origin of the plane:
	inline ValueType getd() const {
		return d;
	}
	
	//! sets normal of the plane:
	inline void setNormal(const Point& n_) {
		n = n_;
	}

	//! sets distance to the origin of the plane:
	inline void setd(const ValueType d_) {
		d = d_;
	}

	void print() const {
		std::cerr << "n: " << n << "   d: " << d << std::endl;
	}

	//! Computes two orthogonal vectors which lie in the plane
	void getBasis(Point& b0, Point& b1) const {
		if (n.x != 1) {
			vec3d t(1,0,0);
			b0 = n^t;
			b0.normalize();
			b1 = n^b0;
		} else {
			if (n.y != 1) {
				vec3d t(0,1,0);
				b0 = n^t;
				b0.normalize();
				b1 = n^b0;
			} else {
				vec3d t(0,0,1);
				b0 = n^t;
				b0.normalize();
				b1 = n^b0;
			}
		}
	}

private:

	//! Plane normal
	Point n;

	//! distance between plane and origin.
	ValueType d;

};
};


#endif

