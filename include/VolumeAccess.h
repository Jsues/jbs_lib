#ifndef VOLUME_ACCESS_H
#define VOLUME_ACCESS_H

#include "Volume.h"

#define TRILINEAR_VOLUME_ACCESS

template <class T>
T getDensityAt(Volume<T>* vol, vec3f pos) {

	int gridsize = vol->getDimX();

	vec3f indices = vol->worldPosToIndices(pos);

#ifdef TRILINEAR_VOLUME_ACCESS

	vec3i lower((int)indices.x, (int)indices.y, (int)indices.z);
	vec3f d = indices-vec3f((float)lower.x, (float)lower.y, (float)lower.z);

	vec3i p;
	T v[2][2][2];
	for (int x = 0; x <= 1; x++) {
		p.x = lower.x+x;
		for (int y = 0; y <= 1; y++) {
			p.y = lower.y+y;
			for (int z = 0; z <= 1; z++) {
				p.z = lower.z+z;

				if (p[0] < 0 || p[0] > gridsize-1 || p[1] < 0 || p[1] > gridsize-1 || p[2] < 0 || p[2] > gridsize-1)
					return -1;

				v[x][y][z] = vol->get(p);
			}
		}
	}

	T v0 = v[0][0][0]*(1-d.z) + v[0][0][1]*d.z;
	T v1 = v[0][1][0]*(1-d.z) + v[0][1][1]*d.z;
	T v2 = v[1][0][0]*(1-d.z) + v[1][0][1]*d.z;
	T v3 = v[1][1][0]*(1-d.z) + v[1][1][1]*d.z;

	T v4 = v0*(1-d.y) + v1*d.y;
	T v5 = v2*(1-d.y) + v3*d.y;

	T v6 = v4*(1-d.x) + v5*d.x;
	return v6;

#else
	vec3i ind((int)(indices.x+0.5f), (int)(indices.y+0.5f), (int)(indices.z+0.5f));
	if (ind[0] < 0 || ind[0] > gridsize-1 || ind[1] < 0 || ind[1] > gridsize-1 || ind[2] < 0 || ind[2] > gridsize-1)
		return -1;
	return vol->get(ind);
#endif

}

#endif
