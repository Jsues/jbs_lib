#ifndef ANIMATED_MESH_H
#define ANIMATED_MESH_H

#include <string>
#include <vector>
#include <iostream>
#include "Windows.h"
#include "FileIO/MeshReader.h"
#include "sortStringVector.h"

#include "SimpleMesh.h"


class AnimatedMesh {

public:

	//! Constructor.
	AnimatedMesh() {
		basemesh = NULL;
	}

	//! Denstructor.
	~AnimatedMesh() {
		if (basemesh != NULL) delete basemesh;
		for (UInt i1 = 0; i1 < frames.size(); i1++) delete[] frames[i1];
		for (UInt i1 = 0; i1 < colors.size(); i1++) delete[] colors[i1];
	}

	//! Creates a animated mesh from a set of mesh files on the hard disk.
	void readFromFiles(char* pattern) {

		if (basemesh != NULL) delete basemesh;
		for (UInt i1 = 0; i1 < frames.size(); i1++) delete[] frames[i1];
		frames.clear();

		std::vector<std::string> filenames;
		WIN32_FIND_DATA FindFileData;
		HANDLE hFind;



		hFind = FindFirstFile(pattern, &FindFileData);
		if (hFind == INVALID_HANDLE_VALUE) {
			std::cerr << "No file matching pattern " << pattern << " found" << std::endl;
			exit(EXIT_FAILURE);
		}
		filenames.push_back(std::string(FindFileData.cFileName));
		while (FindNextFile(hFind, &FindFileData) != 0) {
			filenames.push_back(std::string(FindFileData.cFileName));
		}

		MeshReader mr;

		sortStringVector(filenames);

		//for (UInt i1 = 0; i1 < filenames.size(); i1++) std::cerr << filenames[i1] << std::endl;

		for (UInt i1 = 0; i1 < filenames.size(); i1++) {

			SimpleMesh* sm = mr.readFileFromExtension(filenames[i1].c_str());

			vec3d* pts = new vec3d[sm->getNumV()];
			for (UInt i2 = 0; i2 < (UInt)sm->getNumV(); i2++) pts[i2] = sm->vList[i2].c;
			frames.push_back(pts);

			if (i1 == 0) {
				basemesh = sm;
			} else {
				std::cerr << "Deleting" << std::endl;
				delete sm;
				std::cerr << "Deleting done" << std::endl;
			}

			std::cerr << sizeof(double) << std::endl;
		}
		std::cerr << "Read " << (int)frames.size() << " frames with " << basemesh->getNumV() << " vertices each" << std::endl;
	}

	//! Creates a colored animated mesh from a set of mesh files on the hard disk.
	void readFromFilesColored(char* pattern) {

		if (basemesh != NULL) delete basemesh;
		for (UInt i1 = 0; i1 < frames.size(); i1++) delete[] frames[i1];
		frames.clear();

		std::vector<std::string> filenames;
		WIN32_FIND_DATA FindFileData;
		HANDLE hFind;



		hFind = FindFirstFile(pattern, &FindFileData);
		if (hFind == INVALID_HANDLE_VALUE) {
			std::cerr << "No file matching pattern " << pattern << " found" << std::endl;
			exit(EXIT_FAILURE);
		}
		filenames.push_back(std::string(FindFileData.cFileName));
		while (FindNextFile(hFind, &FindFileData) != 0) {
			filenames.push_back(std::string(FindFileData.cFileName));
		}

		MeshReader mr;

		sortStringVector(filenames);

		//for (UInt i1 = 0; i1 < filenames.size(); i1++) std::cerr << filenames[i1] << std::endl;

		for (UInt i1 = 0; i1 < filenames.size(); i1++) {

			SimpleMesh* sm = mr.readFileFromExtension(filenames[i1].c_str());

			vec3d* pts = new vec3d[sm->getNumV()];
			for (UInt i2 = 0; i2 < (UInt)sm->getNumV(); i2++) pts[i2] = sm->vList[i2].c;
			frames.push_back(pts);

			int* colors_ = new int[sm->getNumV()];
			for (UInt i2 = 0; i2 < (UInt)sm->getNumV(); i2++) {
				vec3f col = sm->vList[i2].color;
				if (col.x == 1) colors_[i2] = 0;
				if (col.y == 1) colors_[i2] = 1;
				if (col.z == 1) colors_[i2] = 2;
			}
			colors.push_back(colors_);


			if (i1 == 0) {
				basemesh = sm;
			} else {
				std::cerr << "Deleting" << std::endl;
				delete sm;
				std::cerr << "Deleting done" << std::endl;
			}

			std::cerr << sizeof(double) << std::endl;
		}
		std::cerr << "Read " << (int)frames.size() << " frames with " << basemesh->getNumV() << " vertices each" << std::endl;
	}


	//! Writes the animated mesh to a binary file (see details for file format specification)
	/*!
		The first 12 Bytes of the file are 3 unsigned int numbers.<br>
		<ul>
		<li>The first UInt (<b>num_frames</b>) tells the number of frames in the animation</li>
		<li>The second UInt (<b>num_tris</b>) holds the number of triangles in the mesh</li>
		<li>The third UInt (<b>num_verts</b>) holds the number of vertices per frame </li>
		</ul>

		Next come all vertices in the animation, each vertex is a triple of doubles (i.e. 24 bytes). There are <b>num_frames</b>*<b>num_verts</b> vertices.

		Then the triangles are writen, each triangle consists of 3 ints, we have <b>num_tris</b> triangles.
	*/
	void writeBinaryFile(const char* filename) {

		std::ofstream fout(filename, std::ofstream::binary);

		if (!fout.good()) {
			std::cerr << "Error writing file in 'AnimatedMesh::writeBinaryFile(char* filename)'" << std::endl;
			return;
		}

		//! Write header
		UInt numFrames = (UInt)frames.size();
		UInt numT = basemesh->getNumT();
		UInt numVperFrame = basemesh->getNumV();
		fout.write((char*)&numFrames, sizeof(UInt));
		fout.write((char*)&numT, sizeof(UInt));
		fout.write((char*)&numVperFrame, sizeof(UInt));

		//! Write points
		for (UInt i1 = 0; i1 < numFrames; i1++) {
			fout.write((char*)&frames[i1][0], sizeof(double)*3*numVperFrame);
		}

		//! Write triangles
		for (UInt i1 = 0; i1 < numT; i1++) {
			vec3i tri(basemesh->tList[i1]->v0(), basemesh->tList[i1]->v1(), basemesh->tList[i1]->v2());
			fout.write((char*)&tri, sizeof(int)*3);
		}

		//! Write colors
		if (colors.size() > 0) {
			for (UInt i1 = 0; i1 < numFrames; i1++) {
				fout.write((char*)&colors[i1][0], sizeof(int)*numVperFrame);
			}
		}
		fout.close();
	}

	//! Reads a binary file.
	void readBinaryFile(const char* filename) {
		if (basemesh != NULL) delete basemesh;
		for (UInt i1 = 0; i1 < frames.size(); i1++) delete[] frames[i1];
		frames.clear();

		std::ifstream fin(filename, std::ifstream::binary);

		if (!fin.good()) {
			std::cerr << "Error reading file in 'AnimatedMesh::readBinaryFile(char* filename)'" << std::endl;
			return;
		}

		// Get length of file
		fin.seekg (0, std::ios::end);
		long length = fin.tellg();
		fin.seekg (0, std::ios::beg);

		//! Read header
		UInt numFrames;
		UInt numT;
		UInt numVperFrame;
			
		fin.read((char*)&numFrames, sizeof(UInt));
		fin.read((char*)&numT, sizeof(UInt));
		fin.read((char*)&numVperFrame, sizeof(UInt));

		UInt filesize = 3*sizeof(UInt) + 3*sizeof(double)*numFrames*numVperFrame + 3*sizeof(int)*numT;
		//std::cerr << "Expected filesize: " << filesize << std::endl;

		//! Read vertices;
		for (UInt i1 = 0; i1 < numFrames; i1++) {
			vec3d* pts = new vec3d[numVperFrame];
			fin.read((char*)pts, sizeof(double)*3*numVperFrame);
			frames.push_back(pts);
		}

		//! Read triangles;
		vec3i* tris = new vec3i[numT];
		fin.read((char*)tris, sizeof(int)*3*numT);


		long current_pos = fin.tellg();
		//std::cerr << current_pos << " " << length << std::endl;
		if (current_pos != length) { // Read colors
			for (UInt i1 = 0; i1 < numFrames; i1++) {
				int* cols = new int[numVperFrame];
				fin.read((char*)cols, sizeof(int)*numVperFrame);
				colors.push_back(cols);
			}
		}

		basemesh = new SimpleMesh;

		//! Fill first mesh:
		for (UInt i1 = 0; i1 < numVperFrame; i1++) {
			vec3d c = frames[0][i1];
			basemesh->insertVertex(c);
		}
		for (UInt i1 = 0; i1 < numT; i1++) {
			basemesh->insertTriangle(tris[i1].x, tris[i1].y, tris[i1].z);
		}
	}

	//! Returns the number of frames in the sequence.
	virtual UInt getNumFrames() const {
		return (UInt) frames.size();
	}

	//! Sets the vertices of 'sm' to the positions of frame 'i'
	void setFrame(SimpleMesh* sm, UInt i) {
		for (int i1 = 0; i1 < sm->getNumV(); i1++) {
			sm->vList[i1].c = frames[i][i1];
		}
	}

	//! Computes 'min' and max of all vertices in the animation.
	virtual void getMinMax(vec3d& v_min, vec3d& v_max) const {
		v_min = frames[0][0];
		v_max = frames[0][0];
		for (UInt i1 = 0; i1 < frames.size(); i1++) {
			for (UInt i2 = 0; i2 < (UInt)basemesh->getNumV(); i2++) {
				const vec3d& pt = frames[i1][i2];
				if (pt.x < v_min.x) v_min.x = pt.x;
				if (pt.y < v_min.y) v_min.y = pt.y;
				if (pt.z < v_min.z) v_min.z = pt.z;
				if (pt.x > v_max.x) v_max.x = pt.x;
				if (pt.y > v_max.y) v_max.y = pt.y;
				if (pt.z > v_max.z) v_max.z = pt.z;
			}
		}
	}

	SimpleMesh* basemesh;
	std::vector<vec3d*> frames;

	std::vector<int*> colors;

protected:


};

#endif
