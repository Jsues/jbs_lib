#ifndef DEC_REL_ROT_H
#define DEC_REL_ROT_H

//#define DEBUG_BOUNDS_CHECK

#include "SimpleMesh.h"
#include "MatrixnD.h"
#include "encRelRot.h"
#include "Timer.h"
#include "RandAccSparseMatrix.h"

#ifdef USE_TAUCS
extern "C" {
#include <taucs.h>
}
#else
#include "umfpack.h"
#endif


struct ConstrainedTri {
	int index;
	SquareMatrixND<vec3d> rot;

	ConstrainedTri(int i, SquareMatrixND<vec3d> r) {
		index = i;
		rot = r;
	}
};


#include "taucs_tools.h"

std::vector<double> decodeRotInvariantMesh(const RotInvEncMesh& rim, SimpleMesh* sm_template, const std::vector<vec3i>& neigh) {

	typedef SquareMatrixND<vec3d> M3D;
	typedef RandAccessCompColMatrix<double> CCSMatrix;

	std::vector<double> features;
	int numFaces = sm_template->getNumT();
	features.resize(10*numFaces);

	int row = 0;

	CCSMatrix A(9*numFaces, 3*numFaces);

	JBS_Timer t;
	//for (int i1 = 0; i1 < numFaces; i1++) {
	//	Triangle* t = sm_template->tList[i1];
	//	t->intFlag = i1;
	//}

	t.log("compute A");
	for (int i1 = 0; i1 < numFaces; i1++) {
		Triangle* t = sm_template->tList[i1];
		//const int& tn0 = t->getTriangleNeighbor(0)->intFlag;
		//const int& tn1 = t->getTriangleNeighbor(1)->intFlag;
		//const int& tn2 = t->getTriangleNeighbor(2)->intFlag;
		const int& tn0 = neigh[i1][0];
		const int& tn1 = neigh[i1][1];
		const int& tn2 = neigh[i1][2];

		if (tn0 > i1) {
			int pos = i1*rim.entriesPerFace;
			vec3d axis(rim.m_data[pos], rim.m_data[pos+1], rim.m_data[pos+2]);

			M3D rot;
			rot.setToRotationMatrixNew(axis);

			// Subtract Identity
			for (int c = 0; c < 3; c++) {
				A.setBottomDown(row+c,tn0*3+c,-1);
			}
			for (int x = 0; x < 3; x++) {
				for (int y = 0; y < 3; y++) {
					A.setBottomDown(row+x, i1*3+y, rot(y,x));
				}
			}
			row += 3;
		}

		if (tn1 > i1) {
			int pos = i1*rim.entriesPerFace+3;
			vec3d axis(rim.m_data[pos], rim.m_data[pos+1], rim.m_data[pos+2]);

			M3D rot;
			rot.setToRotationMatrixNew(axis);

			// Subtract Identity
			for (int c = 0; c < 3; c++) {
				A.setBottomDown(row+c,tn1*3+c,-1);
			}
			for (int x = 0; x < 3; x++) {
				for (int y = 0; y < 3; y++) {
					A.setBottomDown(row+x, i1*3+y, rot(y,x));
				}
			}
			row += 3;
		}

		if (tn2 > i1) {
			int pos = i1*rim.entriesPerFace+6;
			vec3d axis(rim.m_data[pos], rim.m_data[pos+1], rim.m_data[pos+2]);

			M3D rot;
			rot.setToRotationMatrixNew(axis);

			// Subtract Identity
			for (int c = 0; c < 3; c++) {
				A.setBottomDown(row+c,tn2*3+c,-1);
			}
			for (int x = 0; x < 3; x++) {
				for (int y = 0; y < 3; y++) {
					A.setBottomDown(row+x, i1*3+y, rot(y,x));
				}
			}
			row += 3;
		}

		for(int i2 = 4; i2 <= 9; i2++)
			features[i1*10+i2] = rim.m_data[i1*RotInvEncMesh::entriesPerFace+i2+5];
	
	}

	// Add constrained triangles (must be at least 1)
	std::vector<ConstrainedTri> constraints;
	double weight = 1.0;
	M3D Id;
	Id.setToIdentity();
	constraints.push_back(ConstrainedTri(0, Id));
	for (int i1 = 0; i1 < (int)constraints.size(); i1++) {
		int idx = constraints[i1].index;
		M3D rot = constraints[i1].rot;
		for (int c = 0; c < 3; c++) {
			A.setBottomDown(row+c,idx*3+c,weight);
		}
		//for (int x = 0; x < 3; x++) {
		//	for (int y = 0; y < 3; y++) {
		//		A.setBottomDown(row+x, idx*3+y, rot(x,y)); rhs, not A
		//	}
		//}
		row += 3;
	}

	std::vector<double> entries;
	std::vector<int> row_index;
	std::vector<int> col_ptr;

	t.log("get ATA");

	std::vector<double> rhs;
	rhs.resize(9*numFaces);
	rhs[0] = 1;
	rhs[3*numFaces+1] = 1;
	rhs[6*numFaces+2] = 1;

	std::vector<double> xvec;
	xvec.resize(9*numFaces);

#ifdef USE_TAUCS
	A.getAtAForTAUCSPrecomputeNonZeroEntries(entries, row_index, col_ptr);
	//A.getAtAForTAUCS(entries, row_index, col_ptr);

	//taucsMatrixToStreamPlain(entries, row_index, col_ptr, std::cerr);
	//Image* img = taucsMatrixToImage(entries, row_index, col_ptr);
	//img->writeRAW("matrix.raw");

	taucs_ccs_matrix AtA;
	AtA.n = 3*numFaces;
	AtA.m = 3*numFaces;
	AtA.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
	AtA.colptr = &col_ptr[0];
	AtA.rowind = &row_index[0];
	AtA.values.d = &entries[0];


	void* F = NULL;
	char* options[] = {"taucs.factor.LLT=true", NULL};
	void* opt_arg[] = { NULL };

	t.log("prefactor AtA");

	// Pre-factor matrix
	int i = taucs_linsolve(&AtA,&F,0,NULL,NULL,options,NULL);


	t.log("solve AtA x = At b");
	// solve
	i = taucs_linsolve(&AtA,&F,3,&xvec[0],&rhs[0],options,NULL);

	//for (int i1 = 0; i1 < 3*numFaces; i1++) {
	//	std::cerr << xvec[i1] << " " << xvec[3*numFaces+i1] << " " << xvec[6*numFaces+i1] << std::endl;
	//}

	t.log("free TAUCS");

	//free the factorization
	i = taucs_linsolve(NULL,&F,0,NULL,NULL,NULL,NULL);

#else
	A.getAtAForUMFPACKPrecomputeNonZeroEntries(entries, row_index, col_ptr);

	void *Symbolic, *Numeric;
	int result1 = umfpack_di_symbolic(3*numFaces, 3*numFaces, &col_ptr[0], &row_index[0], &entries[0], &Symbolic, NULL, NULL);
	int result2 = umfpack_di_numeric(&col_ptr[0], &row_index[0], &entries[0], Symbolic, &Numeric, NULL, NULL);

	std::cerr << "UMFPACK result: " << result1 << " / " << result2 << std::endl;

	for (int c = 0; c < 3; c++) {
		double* b = &rhs[c*3*numFaces];
		double* x = &xvec[c*3*numFaces];
		int result3 = umfpack_di_solve(UMFPACK_A, &col_ptr[0], &row_index[0], &entries[0], x, b, Numeric, NULL, NULL);
		//std::cerr << "UMFPACK result: " << result2 << std::endl;
	}

#endif


	t.log("make the matrices rotation matrices again");
	// make the matrices rotation matrices again
#pragma omp parallel for
	for (int i1 = 0; i1 < numFaces; i1++) {
		M3D r;
		for (int c = 0; c < 3; c++) {
			r(c, 0) = xvec[3*i1+c];
			r(c, 1) = xvec[3*numFaces+3*i1+c];
			r(c, 2) = xvec[6*numFaces+3*i1+c];
		}
		
		M3D r2 = r.getBestOrtho();

		vec3d rotv = r2.getRotVec();
		features[i1*10+0] = -rotv.x;
		features[i1*10+1] = -rotv.y;
		features[i1*10+2] = -rotv.z;
	}
	t.log();
	t.print();

	return features;

}


SimpleMesh* decTrans(std::vector<double>& features, SimpleMesh* sm_template) {

	typedef SquareMatrixND<vec3d> M3D;
	typedef RandAccessCompColMatrix<double> CCSMatrix;


	double weight = 1;
	int fixvertex = 0;
	vec3d fixto(0,0,0);

	int numFaces = sm_template->getNumT();
	int numV = sm_template->getNumV();

	CCSMatrix A(numV+1, numV);


	std::vector<double> rhs_x;
	rhs_x.resize(numV+1);
	std::vector<double> rhs_y;
	rhs_y.resize(numV+1);
	std::vector<double> rhs_z;
	rhs_z.resize(numV+1);

	for (int i1 = 0; i1 < numFaces; i1++) {

		Triangle* t = sm_template->tList[i1];
		int v0 = t->m_v0;
		int v1 = t->m_v1;
		int v2 = t->m_v2;
		int at = i1*10;

		// reconstruct transformation matrix
		vec3d axis(features[at+0], features[at+1], features[at+2]);
		M3D R;
		R.setToRotationMatrixNew(axis);

		M3D S;
		S(0,0) = features[at+4];
		S(0,1) = features[at+5];
		S(0,2) = features[at+6];
		S(1,0) = features[at+5];
		S(1,1) = features[at+7];
		S(1,2) = features[at+8];
		S(2,0) = features[at+6];
		S(2,1) = features[at+8];
		S(2,2) = features[at+9];

		M3D T = R*S;

		const vec3d x0 = T.vecTrans(sm_template->vList[v0].c);
		const vec3d x1 = T.vecTrans(sm_template->vList[v1].c);
		const vec3d x2 = T.vecTrans(sm_template->vList[v2].c);

		vec3d p01 = x0-x1;
		vec3d p02 = x0-x2;
		vec3d p12 = x1-x2;

		p01.normalize();
		p02.normalize();
		p12.normalize();

		vec3d wts(p02|p12, -p01|p12, p01|p02);

		M3D wij3;
		wij3(0,1) = wts[0]/sqrt(1-wts[0]*wts[0]);
		wij3(0,2) = wts[1]/sqrt(1-wts[1]*wts[1]);
		wij3(1,2) = wts[2]/sqrt(1-wts[2]*wts[2]);

		wij3 += wij3.getTransposed();
		double sum0 = wij3(0,0)+wij3(0,1)+wij3(0,2);
		double sum1 = wij3(1,0)+wij3(1,1)+wij3(1,2);
		double sum2 = wij3(2,0)+wij3(2,1)+wij3(2,2);

		wij3(0,0) -= sum0;
		wij3(1,1) -= sum1;
		wij3(2,2) -= sum2;

		A.add(v0,v0, wij3(0,0));
		A.add(v0,v1, wij3(0,1));
		A.add(v0,v2, wij3(0,2));
		A.add(v1,v0, wij3(1,0));
		A.add(v1,v1, wij3(1,1));
		A.add(v1,v2, wij3(1,2));
		A.add(v2,v0, wij3(2,0));
		A.add(v2,v1, wij3(2,1));
		A.add(v2,v2, wij3(2,2));

		vec3d x0dash = wij3.vecTrans(vec3d(x0.x, x1.x, x2.x));
		vec3d x1dash = wij3.vecTrans(vec3d(x0.y, x1.y, x2.y));
		vec3d x2dash = wij3.vecTrans(vec3d(x0.z, x1.z, x2.z));

		rhs_x[v0] += x0dash.x;
		rhs_y[v0] += x1dash.x;
		rhs_z[v0] += x2dash.x;

		rhs_x[v1] += x0dash.y;
		rhs_y[v1] += x1dash.y;
		rhs_z[v1] += x2dash.y;

		rhs_x[v2] += x0dash.z;
		rhs_y[v2] += x1dash.z;
		rhs_z[v2] += x2dash.z;

	}

	A.add(numV, fixvertex, 1*weight);
	rhs_x[numV] = fixto.x*weight;
	rhs_y[numV] = fixto.y*weight;
	rhs_z[numV] = fixto.z*weight;

	//A.print();
	//for (int i1 = 0; i1 < (int)rhs_x.size(); i1++) {
	//	std::cerr << rhs_x[i1] << " " << rhs_y[i1] << " " << rhs_z[i1] << std::endl;
	//}

	std::vector<double> Atb_x = A.getAtb(rhs_x);
	std::vector<double> Atb_y = A.getAtb(rhs_y);
	std::vector<double> Atb_z = A.getAtb(rhs_z);
	std::vector<double> Atb;
	Atb.resize(3*numV);
	for (int i1 = 0; i1 < numV; i1++) {
		Atb[i1] = Atb_x[i1];
		Atb[numV+i1] = Atb_y[i1];
		Atb[2*numV+i1] = Atb_z[i1];
	}

	std::vector<double> entries;
	std::vector<int> row_index;
	std::vector<int> col_ptr;


	std::vector<double> res;
	res.resize(3*numV);

#ifdef USE_TAUCS
	A.getAtAForTAUCSPrecomputeNonZeroEntries(entries, row_index, col_ptr);
	//A.getAtAForTAUCS(entries, row_index, col_ptr);

	taucs_ccs_matrix AtA;
	AtA.n = numV;
	AtA.m = numV;
	AtA.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
	AtA.colptr = &col_ptr[0];
	AtA.rowind = &row_index[0];
	AtA.values.d = &entries[0];


	void* F = NULL;
	char* options[] = {"taucs.factor.LLT=true", NULL};
	void* opt_arg[] = { NULL };
	// Pre-factor matrix
	int i = taucs_linsolve(&AtA,&F,0,NULL,NULL,options,NULL);

	i = taucs_linsolve(&AtA,&F,3,&res[0],&Atb[0],options,NULL);

	//free the factorization
	i = taucs_linsolve(NULL,&F,0,NULL,NULL,NULL,NULL);
#else
	A.getAtAForUMFPACKPrecomputeNonZeroEntries(entries, row_index, col_ptr);

	void *Symbolic, *Numeric;
	int result1 = umfpack_di_symbolic(numV, numV, &col_ptr[0], &row_index[0], &entries[0], &Symbolic, NULL, NULL);
	int result2 = umfpack_di_numeric(&col_ptr[0], &row_index[0], &entries[0], Symbolic, &Numeric, NULL, NULL);

	std::cerr << "UMFPACK result: " << result1 << " / " << result2 << std::endl;

	for (int c = 0; c < 3; c++) {
		double* b = &Atb[c*numV];
		double* x = &res[c*numV];
		int result3 = umfpack_di_solve(UMFPACK_A, &col_ptr[0], &row_index[0], &entries[0], x, b, Numeric, NULL, NULL);
		//std::cerr << "UMFPACK result: " << result2 << std::endl;
	}
#endif

	SimpleMesh* sm(sm_template);
	for (int i1 = 0; i1 < numV; i1++) {
		vec3d p(res[i1], res[numV+i1], res[2*numV+i1]);
		sm->vList[i1].c = p;
	}

	return sm;
}


// works
SimpleMesh* getMeshFromDefGrads(std::vector<double>& features, const SimpleMesh* const sm_template) {

	typedef SquareMatrixND<vec3d> M3D;
	typedef RandAccessCompColMatrix<double> CCSMatrix;


	double weight = 1;
	int fixvertex = 0;
	vec3d fixto = sm_template->vList[0].c;

	int numFaces = sm_template->getNumT();
	int numV = sm_template->getNumV();

	CCSMatrix A(numV+1, numV);


	std::vector<double> rhs_x;
	rhs_x.resize(numV+1);
	std::vector<double> rhs_y;
	rhs_y.resize(numV+1);
	std::vector<double> rhs_z;
	rhs_z.resize(numV+1);

	for (int i1 = 0; i1 < numFaces; i1++) {

		Triangle* t = sm_template->tList[i1];
		int v0 = t->m_v0;
		int v1 = t->m_v1;
		int v2 = t->m_v2;
		int at = i1*9;

		// reconstruct transformation matrix
		vec3d axis(features[at+0], features[at+1], features[at+2]);
		M3D R;
		R.setToRotationMatrixNew(axis);

		M3D S;
		S(0,0) = features[at+3];
		S(0,1) = features[at+4];
		S(0,2) = features[at+5];
		S(1,0) = features[at+4];
		S(1,1) = features[at+6];
		S(1,2) = features[at+7];
		S(2,0) = features[at+5];
		S(2,1) = features[at+7];
		S(2,2) = features[at+8];

		M3D T = R*S;

		const vec3d x0 = T.vecTrans(sm_template->vList[v0].c);
		const vec3d x1 = T.vecTrans(sm_template->vList[v1].c);
		const vec3d x2 = T.vecTrans(sm_template->vList[v2].c);

		vec3d p01 = x0-x1;
		vec3d p02 = x0-x2;
		vec3d p12 = x1-x2;

		p01.normalize();
		p02.normalize();
		p12.normalize();

		vec3d wts(p02|p12, -p01|p12, p01|p02);

		M3D wij3;
		wij3(0,1) = wts[0]/sqrt(1-wts[0]*wts[0]);
		wij3(0,2) = wts[1]/sqrt(1-wts[1]*wts[1]);
		wij3(1,2) = wts[2]/sqrt(1-wts[2]*wts[2]);

		wij3 += wij3.getTransposed();
		double sum0 = wij3(0,0)+wij3(0,1)+wij3(0,2);
		double sum1 = wij3(1,0)+wij3(1,1)+wij3(1,2);
		double sum2 = wij3(2,0)+wij3(2,1)+wij3(2,2);

		wij3(0,0) -= sum0;
		wij3(1,1) -= sum1;
		wij3(2,2) -= sum2;

		A.add(v0,v0, wij3(0,0));
		A.add(v0,v1, wij3(0,1));
		A.add(v0,v2, wij3(0,2));
		A.add(v1,v0, wij3(1,0));
		A.add(v1,v1, wij3(1,1));
		A.add(v1,v2, wij3(1,2));
		A.add(v2,v0, wij3(2,0));
		A.add(v2,v1, wij3(2,1));
		A.add(v2,v2, wij3(2,2));

		vec3d x0dash = wij3.vecTrans(vec3d(x0.x, x1.x, x2.x));
		vec3d x1dash = wij3.vecTrans(vec3d(x0.y, x1.y, x2.y));
		vec3d x2dash = wij3.vecTrans(vec3d(x0.z, x1.z, x2.z));

		rhs_x[v0] += x0dash.x;
		rhs_y[v0] += x1dash.x;
		rhs_z[v0] += x2dash.x;

		rhs_x[v1] += x0dash.y;
		rhs_y[v1] += x1dash.y;
		rhs_z[v1] += x2dash.y;

		rhs_x[v2] += x0dash.z;
		rhs_y[v2] += x1dash.z;
		rhs_z[v2] += x2dash.z;

	}

	A.add(numV, fixvertex, 1*weight);
	rhs_x[numV] = fixto.x*weight;
	rhs_y[numV] = fixto.y*weight;
	rhs_z[numV] = fixto.z*weight;

	//A.print();
	//for (int i1 = 0; i1 < (int)rhs_x.size(); i1++) {
	//	std::cerr << rhs_x[i1] << " " << rhs_y[i1] << " " << rhs_z[i1] << std::endl;
	//}

	std::vector<double> Atb_x = A.getAtb(rhs_x);
	std::vector<double> Atb_y = A.getAtb(rhs_y);
	std::vector<double> Atb_z = A.getAtb(rhs_z);
	std::vector<double> Atb;
	Atb.resize(3*numV);
	for (int i1 = 0; i1 < numV; i1++) {
		Atb[i1] = Atb_x[i1];
		Atb[numV+i1] = Atb_y[i1];
		Atb[2*numV+i1] = Atb_z[i1];
	}

	std::vector<double> entries;
	std::vector<int> row_index;
	std::vector<int> col_ptr;
	
	std::vector<double> res;
	res.resize(3*numV);

#ifdef USE_TAUCS
	A.getAtAForTAUCSPrecomputeNonZeroEntries(entries, row_index, col_ptr);
	//A.getAtAForTAUCS(entries, row_index, col_ptr);


	taucs_ccs_matrix AtA;
	AtA.n = numV;
	AtA.m = numV;
	AtA.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
	AtA.colptr = &col_ptr[0];
	AtA.rowind = &row_index[0];
	AtA.values.d = &entries[0];


	void* F = NULL;
	char* options[] = {"taucs.factor.LLT=true", NULL};
	void* opt_arg[] = { NULL };
	// Pre-factor matrix
	int i = taucs_linsolve(&AtA,&F,0,NULL,NULL,options,NULL);

	i = taucs_linsolve(&AtA,&F,3,&res[0],&Atb[0],options,NULL);

	//free the factorization
	i = taucs_linsolve(NULL,&F,0,NULL,NULL,NULL,NULL);

#else
	A.getAtAForUMFPACKPrecomputeNonZeroEntries(entries, row_index, col_ptr);

	void *Symbolic, *Numeric;
	int result1 = umfpack_di_symbolic(numV, numV, &col_ptr[0], &row_index[0], &entries[0], &Symbolic, NULL, NULL);
	int result2 = umfpack_di_numeric(&col_ptr[0], &row_index[0], &entries[0], Symbolic, &Numeric, NULL, NULL);

	std::cerr << "UMFPACK result: " << result1 << " / " << result2 << std::endl;

	for (int c = 0; c < 3; c++) {
		double* b = &Atb[c*numV];
		double* x = &res[c*numV];
		int result3 = umfpack_di_solve(UMFPACK_A, &col_ptr[0], &row_index[0], &entries[0], x, b, Numeric, NULL, NULL);
		//std::cerr << "UMFPACK result: " << result2 << std::endl;
	}
#endif

	const SimpleMesh& xxxx = *sm_template;

	SimpleMesh* sm = new SimpleMesh(xxxx);
	for (int i1 = 0; i1 < numV; i1++) {
		vec3d p(res[i1], res[numV+i1], res[2*numV+i1]);
		sm->vList[i1].c = p;
	}

	return sm;
}




// works
SimpleMesh* getMeshFromDefGradsMatrix(std::vector<double>& data, const SimpleMesh* const sm_template, int fixvertex = -1, vec3d fixto = vec3d(0.0,0.0,0.0)) {

	typedef SquareMatrixND<vec3d> M3D;
	typedef RandAccessCompColMatrix<double> CCSMatrix;


	double weight = 1;

	if (fixvertex == -1) {
		fixvertex = 0;
		fixto = sm_template->vList[0].c;
	}

	int numFaces = sm_template->getNumT();
	int numV = sm_template->getNumV();

	CCSMatrix A(numV+1, numV);


	std::vector<double> rhs_x;
	rhs_x.resize(numV+1);
	std::vector<double> rhs_y;
	rhs_y.resize(numV+1);
	std::vector<double> rhs_z;
	rhs_z.resize(numV+1);

	for (int i1 = 0; i1 < numFaces; i1++) {

		Triangle* t = sm_template->tList[i1];
		int v0 = t->m_v0;
		int v1 = t->m_v1;
		int v2 = t->m_v2;
		int at = i1*9;


		M3D T;
		T(0,0) = data[at+0];
		T(0,1) = data[at+1];
		T(0,2) = data[at+2];
		T(1,0) = data[at+3];
		T(1,1) = data[at+4];
		T(1,2) = data[at+5];
		T(2,0) = data[at+6];
		T(2,1) = data[at+7];
		T(2,2) = data[at+8];
		const vec3d x0 = T.vecTrans(sm_template->vList[v0].c);
		const vec3d x1 = T.vecTrans(sm_template->vList[v1].c);
		const vec3d x2 = T.vecTrans(sm_template->vList[v2].c);

		vec3d p01 = x0-x1;
		vec3d p02 = x0-x2;
		vec3d p12 = x1-x2;

		p01.normalize();
		p02.normalize();
		p12.normalize();

		vec3d wts(p02|p12, -p01|p12, p01|p02);

		M3D wij3;
		wij3(0,1) = wts[0]/sqrt(1-wts[0]*wts[0]);
		wij3(0,2) = wts[1]/sqrt(1-wts[1]*wts[1]);
		wij3(1,2) = wts[2]/sqrt(1-wts[2]*wts[2]);

		wij3 += wij3.getTransposed();
		double sum0 = wij3(0,0)+wij3(0,1)+wij3(0,2);
		double sum1 = wij3(1,0)+wij3(1,1)+wij3(1,2);
		double sum2 = wij3(2,0)+wij3(2,1)+wij3(2,2);

		wij3(0,0) -= sum0;
		wij3(1,1) -= sum1;
		wij3(2,2) -= sum2;

		A.add(v0,v0, wij3(0,0));
		A.add(v0,v1, wij3(0,1));
		A.add(v0,v2, wij3(0,2));
		A.add(v1,v0, wij3(1,0));
		A.add(v1,v1, wij3(1,1));
		A.add(v1,v2, wij3(1,2));
		A.add(v2,v0, wij3(2,0));
		A.add(v2,v1, wij3(2,1));
		A.add(v2,v2, wij3(2,2));

		vec3d x0dash = wij3.vecTrans(vec3d(x0.x, x1.x, x2.x));
		vec3d x1dash = wij3.vecTrans(vec3d(x0.y, x1.y, x2.y));
		vec3d x2dash = wij3.vecTrans(vec3d(x0.z, x1.z, x2.z));

		rhs_x[v0] += x0dash.x;
		rhs_y[v0] += x1dash.x;
		rhs_z[v0] += x2dash.x;

		rhs_x[v1] += x0dash.y;
		rhs_y[v1] += x1dash.y;
		rhs_z[v1] += x2dash.y;

		rhs_x[v2] += x0dash.z;
		rhs_y[v2] += x1dash.z;
		rhs_z[v2] += x2dash.z;

	}

	A.add(numV, fixvertex, 1*weight);
	rhs_x[numV] = fixto.x*weight;
	rhs_y[numV] = fixto.y*weight;
	rhs_z[numV] = fixto.z*weight;

	//A.print();
	//for (int i1 = 0; i1 < (int)rhs_x.size(); i1++) {
	//	std::cerr << rhs_x[i1] << " " << rhs_y[i1] << " " << rhs_z[i1] << std::endl;
	//}

	std::vector<double> Atb_x = A.getAtb(rhs_x);
	std::vector<double> Atb_y = A.getAtb(rhs_y);
	std::vector<double> Atb_z = A.getAtb(rhs_z);
	std::vector<double> Atb;
	Atb.resize(3*numV);
	for (int i1 = 0; i1 < numV; i1++) {
		Atb[i1] = Atb_x[i1];
		Atb[numV+i1] = Atb_y[i1];
		Atb[2*numV+i1] = Atb_z[i1];
	}

	std::vector<double> entries;
	std::vector<int> row_index;
	std::vector<int> col_ptr;

	std::vector<double> res;
	res.resize(3*numV);

#ifdef USE_TAUCS
	A.getAtAForTAUCSPrecomputeNonZeroEntries(entries, row_index, col_ptr);
	//A.getAtAForTAUCS(entries, row_index, col_ptr);

	taucs_ccs_matrix AtA;
	AtA.n = numV;
	AtA.m = numV;
	AtA.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
	AtA.colptr = &col_ptr[0];
	AtA.rowind = &row_index[0];
	AtA.values.d = &entries[0];

	void* F = NULL;
	char* options[] = {"taucs.factor.LLT=true", NULL};
	void* opt_arg[] = { NULL };
	// Pre-factor matrix
	int i = taucs_linsolve(&AtA,&F,0,NULL,NULL,options,NULL);


	i = taucs_linsolve(&AtA,&F,3,&res[0],&Atb[0],options,NULL);

	//free the factorization
	i = taucs_linsolve(NULL,&F,0,NULL,NULL,NULL,NULL);
#else

	A.getAtAForUMFPACKPrecomputeNonZeroEntries(entries, row_index, col_ptr);

	void *Symbolic, *Numeric;
	int result1 = umfpack_di_symbolic(numV, numV, &col_ptr[0], &row_index[0], &entries[0], &Symbolic, NULL, NULL);
	int result2 = umfpack_di_numeric(&col_ptr[0], &row_index[0], &entries[0], Symbolic, &Numeric, NULL, NULL);

	std::cerr << "UMFPACK result: " << result1 << " / " << result2 << std::endl;

	for (int c = 0; c < 3; c++) {
		double* b = &Atb[c*numV];
		double* x = &res[c*numV];
		int result3 = umfpack_di_solve(UMFPACK_A, &col_ptr[0], &row_index[0], &entries[0], x, b, Numeric, NULL, NULL);
		//std::cerr << "UMFPACK result: " << result2 << std::endl;
	}

#endif

	const SimpleMesh& xxxx = *sm_template;

	SimpleMesh* sm = new SimpleMesh(xxxx);
	for (int i1 = 0; i1 < numV; i1++) {
		vec3d p(res[i1], res[numV+i1], res[2*numV+i1]);
		sm->vList[i1].c = p;
	}

	return sm;
}



// works
std::vector<vec3d> getMeshFromDefGradsMatrix(std::vector<double>& data, const std::vector<vec3d>& verts, const std::vector<vec3i>& tris, int fixvertex = -1, vec3d fixto = vec3d(0.0,0.0,0.0)) {

	typedef SquareMatrixND<vec3d> M3D;
	typedef RandAccessCompColMatrix<double> CCSMatrix;


	double weight = 1;

	if (fixvertex == -1) {
		fixvertex = 0;
		fixto = verts[0];
	}

	int numFaces = (int)tris.size();
	int numV = (int)verts.size();

	CCSMatrix A(numV+1, numV);


	std::vector<double> rhs_x;
	rhs_x.resize(numV+1);
	std::vector<double> rhs_y;
	rhs_y.resize(numV+1);
	std::vector<double> rhs_z;
	rhs_z.resize(numV+1);

	for (int i1 = 0; i1 < numFaces; i1++) {

		const vec3i& t = tris[i1];
		int v0 = t.x;
		int v1 = t.y;
		int v2 = t.z;


		M3D T;
		int at = i1*9;
		T(0,0) = data[at+0];
		T(0,1) = data[at+1];
		T(0,2) = data[at+2];
		T(1,0) = data[at+3];
		T(1,1) = data[at+4];
		T(1,2) = data[at+5];
		T(2,0) = data[at+6];
		T(2,1) = data[at+7];
		T(2,2) = data[at+8];
		const vec3d x0 = T.vecTrans(verts[v0]);
		const vec3d x1 = T.vecTrans(verts[v1]);
		const vec3d x2 = T.vecTrans(verts[v2]);

		vec3d p01 = x0-x1;
		vec3d p02 = x0-x2;
		vec3d p12 = x1-x2;

		p01.normalize();
		p02.normalize();
		p12.normalize();

		vec3d wts(p02|p12, -p01|p12, p01|p02);

		M3D wij3;
		wij3(0,1) = wts[0]/sqrt(1-wts[0]*wts[0]);
		wij3(0,2) = wts[1]/sqrt(1-wts[1]*wts[1]);
		wij3(1,2) = wts[2]/sqrt(1-wts[2]*wts[2]);

		wij3 += wij3.getTransposed();
		double sum0 = wij3(0,0)+wij3(0,1)+wij3(0,2);
		double sum1 = wij3(1,0)+wij3(1,1)+wij3(1,2);
		double sum2 = wij3(2,0)+wij3(2,1)+wij3(2,2);

		wij3(0,0) -= sum0;
		wij3(1,1) -= sum1;
		wij3(2,2) -= sum2;

		A.add(v0,v0, wij3(0,0));
		A.add(v0,v1, wij3(0,1));
		A.add(v0,v2, wij3(0,2));
		A.add(v1,v0, wij3(1,0));
		A.add(v1,v1, wij3(1,1));
		A.add(v1,v2, wij3(1,2));
		A.add(v2,v0, wij3(2,0));
		A.add(v2,v1, wij3(2,1));
		A.add(v2,v2, wij3(2,2));

		vec3d x0dash = wij3.vecTrans(vec3d(x0.x, x1.x, x2.x));
		vec3d x1dash = wij3.vecTrans(vec3d(x0.y, x1.y, x2.y));
		vec3d x2dash = wij3.vecTrans(vec3d(x0.z, x1.z, x2.z));

		rhs_x[v0] += x0dash.x;
		rhs_y[v0] += x1dash.x;
		rhs_z[v0] += x2dash.x;

		rhs_x[v1] += x0dash.y;
		rhs_y[v1] += x1dash.y;
		rhs_z[v1] += x2dash.y;

		rhs_x[v2] += x0dash.z;
		rhs_y[v2] += x1dash.z;
		rhs_z[v2] += x2dash.z;

	}

	A.add(numV, fixvertex, 1*weight);
	rhs_x[numV] = fixto.x*weight;
	rhs_y[numV] = fixto.y*weight;
	rhs_z[numV] = fixto.z*weight;

	std::vector<double> Atb_x = A.getAtb(rhs_x);
	std::vector<double> Atb_y = A.getAtb(rhs_y);
	std::vector<double> Atb_z = A.getAtb(rhs_z);
	std::vector<double> Atb;
	Atb.resize(3*numV);
	for (int i1 = 0; i1 < numV; i1++) {
		Atb[i1] = Atb_x[i1];
		Atb[numV+i1] = Atb_y[i1];
		Atb[2*numV+i1] = Atb_z[i1];
	}

	std::vector<double> entries;
	std::vector<int> row_index;
	std::vector<int> col_ptr;

	std::vector<double> res;
	res.resize(3*numV);

#ifdef USE_TAUCS
	A.getAtAForTAUCSPrecomputeNonZeroEntries(entries, row_index, col_ptr);
	//A.getAtAForTAUCS(entries, row_index, col_ptr);

	taucs_ccs_matrix AtA;
	AtA.n = numV;
	AtA.m = numV;
	AtA.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
	AtA.colptr = &col_ptr[0];
	AtA.rowind = &row_index[0];
	AtA.values.d = &entries[0];

	void* F = NULL;
	char* options[] = {"taucs.factor.LLT=true", NULL};
	void* opt_arg[] = { NULL };
	// Pre-factor matrix
	int i = taucs_linsolve(&AtA,&F,0,NULL,NULL,options,NULL);


	i = taucs_linsolve(&AtA,&F,3,&res[0],&Atb[0],options,NULL);

	//free the factorization
	i = taucs_linsolve(NULL,&F,0,NULL,NULL,NULL,NULL);
#else

	A.getAtAForUMFPACKPrecomputeNonZeroEntries(entries, row_index, col_ptr);

	Image* img = taucsMatrixToImage(entries, row_index, col_ptr, 500);
	img->writePPM("matrix2.ppm");

	void *Symbolic, *Numeric;
	int result1 = umfpack_di_symbolic(numV, numV, &col_ptr[0], &row_index[0], &entries[0], &Symbolic, NULL, NULL);
	int result2 = umfpack_di_numeric(&col_ptr[0], &row_index[0], &entries[0], Symbolic, &Numeric, NULL, NULL);

	//umfpack_di_save_symbolic(Symbolic, "scape_symbolic.bin");
	//umfpack_di_save_numeric(Numeric, "scape_numeric.bin");

	std::cerr << "UMFPACK result: " << result1 << " / " << result2 << std::endl;

	for (int c = 0; c < 3; c++) {
		double* b = &Atb[c*numV];
		double* x = &res[c*numV];
		int result3 = umfpack_di_solve(UMFPACK_A, &col_ptr[0], &row_index[0], &entries[0], x, b, Numeric, NULL, NULL);
		//std::cerr << "UMFPACK result: " << result2 << std::endl;
	}

	umfpack_di_free_numeric(&Numeric);
	umfpack_di_free_symbolic(&Symbolic);

#endif

	std::vector<vec3d> ret(numV);

	for (int i1 = 0; i1 < numV; i1++) {
		vec3d p(res[i1], res[numV+i1], res[2*numV+i1]);
		ret[i1] = p;
	}

	return ret;
}



#endif

