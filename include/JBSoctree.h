#ifndef JBS_OCTREE_H
#define JBS_OCTREE_H

//#pragma warning (disable:4311)

#include <vector>
#include <map>
#include "point3d.h"
#include "JBS_General.h"
#include "plane.h"

#ifndef NOMINMAX
#define NOMINMAX
#endif

#define STORE_CELLS_IN_TREE

namespace JBSlib {

// ****************************************************************************************

template <class T> class OcTree;

// ****************************************************************************************

////! A point 3d which may be inserted into an octree.
template <class T>
class OcPoint3 : public point3d<T> {

public:
	typedef point3d<T> Point;
	typedef T ValueType;

	OcPoint3() : Point() {}

	OcPoint3(ValueType x_, ValueType y_, ValueType z_) : Point(x_,y_,z_) {}

	OcPoint3(const Point& p_) : Point(p_) {}

	//! Returns if the point is contained in the cell from 'start' to 'end'.
	inline bool isContainedIn(const Point& start, const Point& end) const {
		if (start <= *this && end > *this)
			return true;

		return false;
	}

	//! Liefert nur dann true, wenn (this[0] == other[0]) && (this[1] == other[1]) && (this[2] == other[2])
	bool operator==(const OcPoint3& other) {
		//std::cerr << "compare" << std::endl;
		if ((this->array[0] == other.array[0]) && (this->array[1] == other.array[1]) && (this->array[2] == other[2]))
			return true;

		//std::cerr << "false" << std::endl;

		return false;
	}


    //! prints the point to stderr
	void print() {
		Point::print();
	}

};


typedef OcPoint3<double> OcPoint3d;
typedef OcPoint3<float> OcPoint3f;

// ****************************************************************************************


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

//! A triangle which can be added to an OcTree.
template <class T>
class OcTriangle {

public:
	typedef T Point;
	typedef typename T::ValueType ValueType;

// ****************************************************************************************

	//! (Void-)Constructor
	OcTriangle() : triplane(v0, v1, v2) {
	}

// ****************************************************************************************

	//! Constructor
	OcTriangle(Point v0_, Point v1_, Point v2_) : v0(v0_), v1(v1_), v2(v2_), triplane(v0_, v1_, v2_) {
	}

// ****************************************************************************************

	//! Returns if the triangle is contained in / intersects the cell from 'start' to 'end'.
	inline bool isContainedIn(const Point& start, const Point& end) const {

		// Trivial reject, triangle lies entirely outside one of the cells faces:
		if (v0.x < start.x && v1.x < start.x && v2.x < start.x) return false;
		if (v0.x > end.x && v1.x > end.x && v2.x > end.x) return false;
		if (v0.y < start.y && v1.y < start.y && v2.y < start.y) return false;
		if (v0.y > end.y && v1.y > end.y && v2.y > end.y) return false;
		if (v0.z < start.z && v1.z < start.z && v2.z < start.z) return false;
		if (v0.z > end.z && v1.z > end.z && v2.z > end.z) return false;

		// Trivial accept, one vertex of the triangle lies entirely in the cell.
		if ((start < v0 && end > v0) || (start < v1 && end > v1) || (start < v2 && end > v2)) return true;

		// Not so trivial accept, check if one of the cells edges intersects the triangle:
		if (lineIntersects(start, Point(end.x, start.y, start.z))) return true;
		if (lineIntersects(start, Point(start.x, end.y, start.z))) return true;
		if (lineIntersects(start, Point(start.x, start.y, end.z))) return true;
		if (lineIntersects(end, Point(start.x, end.y, end.z))) return true;
		if (lineIntersects(end, Point(end.x, start.y, end.z))) return true;
		if (lineIntersects(end, Point(end.x, end.y, start.z))) return true;
		if (lineIntersects(Point(end.x, end.y, start.x), Point(end.x, start.y, start.z))) return true;
		if (lineIntersects(Point(end.x, end.y, start.x), Point(start.x, end.y, start.z))) return true;
		if (lineIntersects(Point(start.x, end.y, end.z), Point(start.x, start.y, end.z))) return true;
		if (lineIntersects(Point(start.x, end.y, end.z), Point(start.x, end.y, start.z))) return true;
		if (lineIntersects(Point(start.x, start.y, end.z), Point(end.x, start.y, end.z))) return true;
		if (lineIntersects(Point(end.x, start.y, start.z), Point(end.x, start.y, end.z))) return true;

		return false;
	}

// ****************************************************************************************

	//! Check if line ('start' - 'end') intersects the triangle.
	inline bool lineIntersects(const Point& start, const Point& end) const {
		
		Point direction = end-start;

		ValueType d = - ((start-v0)|triplane.getNormal())/(direction|triplane.getNormal());

		if ((d < 0) || (d > 1)) return false;

		// Line between 'start' and 'end' intersects triangle plane, check if the
		// intersection point lies within the triangle.
		Point intersect_point = start + direction * d;

		return liesInsideTriangle(intersect_point);
	}

// ****************************************************************************************

	//! Computes the intersection point between the ray through 'start' and 'end' and the plane in which the triangle is embedded.
	inline Point getPlaneIntersection(const Point& start, const Point& end) const {
		
		Point direction = end-start;

		ValueType d = - ((start-v0)|triplane.getNormal())/(direction|triplane.getNormal());

		// Line between 'start' and 'end' intersects triangle plane, check if the
		// intersection point lies within the triangle.
		return start + direction * d;

	}

// ****************************************************************************************

	//! Computes the (real) distance between 'point' and the triangle.
	inline ValueType distance(const Point& point) {

		// If the ray ('point', triangles normal) intersects the triangle-plane within the triangle,
		// the distance between the triangles and the point is the distance between point and intersection.
		ValueType d = triplane.getDistance(point);
		Point intersection = this->start + this->direction * d;


		point2d<ValueType> v0proj, v1proj, v2proj, iproj;
		if (triplane.getNormal().x <= triplane.getNormal().y && triplane.getNormal().x <= triplane.getNormal().z) {
			v0proj = point2d<ValueType>(v0.y, v0.z);
			v1proj = point2d<ValueType>(v1.y, v1.z);
			v2proj = point2d<ValueType>(v2.y, v2.z);
			iproj = point2d<ValueType>(point.y, point.z);
		} else {
			v0proj = point2d<ValueType>(v0.x, v0.z);
			v1proj = point2d<ValueType>(v1.x, v1.z);
			v2proj = point2d<ValueType>(v2.x, v2.z);
			iproj = point2d<ValueType>(point.x, point.z);
		}
		// 2D barycentric test:
		ValueType a_tri = Matrix3D<ValueType>(v0proj.x, v1proj.x, v2proj.x, v0proj.y, v1proj.y, v2proj.y, 1, 1, 1).determinant();
		ValueType a_0 = Matrix3D<ValueType>(iproj.x, v1proj.x, v2proj.x, iproj.y, v1proj.y, v2proj.y, 1, 1, 1).determinant()/a_tri;
		ValueType a_1 = Matrix3D<ValueType>(v0proj.x, iproj.x, v2proj.x, v0proj.y, iproj.y, v2proj.y, 1, 1, 1).determinant()/a_tri;
		ValueType a_2 = Matrix3D<ValueType>(v0proj.x, v1proj.x, iproj.x, v0proj.y, v1proj.y, iproj.y, 1, 1, 1).determinant()/a_tri;

		if (a_0 >= 0 && a_1 >= 0 && a_2 >= 0) { // point lies within triangle
			return (intersection-point).length();
		}

		// Intersection does not lie within the triangle.
		if (a_0 < 0) { // closest point must lie on the line 'v1' - 'v2'
			if (a_1 < 0) { // closest point must lie on the line 'v0' - 'v2'
				// ergo, closest point is 'v2'
				return (v2-point).length();
			}
			if (a_2 < 0) { // closest point must lie on the line 'v0' - 'v1'
				// ergo, closest point is 'v1'
				return (v1-point).length();
			}
			// well, closest point is the distance between point and the line 'v1' - 'v2'.
			return (getClosestPointOnLine(v1, v2, point)-point).length();
		}
		if (a_1 < 0) { // closest point must lie on the line 'v0' - 'v2'
			if (a_2 < 0) { // closest point must lie on the line 'v0' - 'v1'
				// ergo, closest point is 'v0'
				return (v0-point).length();
			}
			// well, closest point is the distance between point and the line 'v0' - 'v2'.
			return (getClosestPointOnLine(v2, v0, point)-point).length();
		}
		if (a_2 < 0) { // a_1 and a_0
		}


	}

// ****************************************************************************************

	//! not implemented
	void print() {}

// ****************************************************************************************

	//! Vertex of the triangle.
	Point v0;
	//! Vertex of the triangle.
	Point v1;
	//! Vertex of the triangle.
	Point v2;

private:

// ****************************************************************************************

	inline Point getClosestPointOnLine(const Point& v0, const Point& v1, const Point& p) const {

		ValueType u = ((p.x-v0.x)*(v1.x-v0.x) + (p.y-v0.y)*(v1.y-v0.y) + (p.z-v0.z)*(v1.z-v0.z)) / (v0-v1).squaredLength();

		if (u <= 0) return v0;
		if (u >= 1) return v1;

        return (v0 + (v1-v0)*u);
	}

// ****************************************************************************************

	//! Checks if 'point' lies inside the triangle; it is assumed, that 'point' lies on the plane of the triangle.
	inline bool liesInsideTriangle(const Point& point) const {

		// Project triangle and point into 2D and do computation there:
		// Project triangle and point onto some plane
		point2d<ValueType> v0proj, v1proj, v2proj, iproj;
		if (triplane.getNormal().x <= triplane.getNormal().y && triplane.getNormal().x <= triplane.getNormal().z) {
			v0proj = point2d<ValueType>(v0.y, v0.z);
			v1proj = point2d<ValueType>(v1.y, v1.z);
			v2proj = point2d<ValueType>(v2.y, v2.z);
			iproj = point2d<ValueType>(point.y, point.z);
		} else {
			v0proj = point2d<ValueType>(v0.x, v0.z);
			v1proj = point2d<ValueType>(v1.x, v1.z);
			v2proj = point2d<ValueType>(v2.x, v2.z);
			iproj = point2d<ValueType>(point.x, point.z);
		}
		// 2D barycentric test:
		ValueType a_tri = Matrix3D<ValueType>(v0proj.x, v1proj.x, v2proj.x, v0proj.y, v1proj.y, v2proj.y, 1, 1, 1).determinant();
		ValueType a_0 = Matrix3D<ValueType>(iproj.x, v1proj.x, v2proj.x, iproj.y, v1proj.y, v2proj.y, 1, 1, 1).determinant()/a_tri;
		ValueType a_1 = Matrix3D<ValueType>(v0proj.x, iproj.x, v2proj.x, v0proj.y, iproj.y, v2proj.y, 1, 1, 1).determinant()/a_tri;
		ValueType a_2 = Matrix3D<ValueType>(v0proj.x, v1proj.x, iproj.x, v0proj.y, v1proj.y, iproj.y, 1, 1, 1).determinant()/a_tri;

		if (a_0 > 0 && a_1 > 0 && a_2 > 0)
			return true;

		return false;
	}

// ****************************************************************************************

	Plane<vec3d> triplane;
};

typedef OcTriangle<vec3f> OcTrianglef;
typedef OcTriangle<vec3d> OcTriangled;

//! write a OcTriangle to a stream
template <class T> inline std::ostream& operator<<(std::ostream& s, const OcTriangle<T>& t)
{ return (s << "Triangle: (" << t.v0 << ") / (" << t.v1 << ") / (" << t.v2 << ")" );}


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class OcNode {

public:

	typedef T InsertedType;
	typedef typename T::ValueType ValueType;
	typedef point3d<ValueType> Point;

// ****************************************************************************************

	OcNode(OcTree<InsertedType>* tree_, Point min_, Point max_, OcNode<InsertedType>* father_ = NULL): tree(tree_), min_ext(min_), max_ext(max_), m_isLeafNode(true), father(father_) {

		if (father == NULL) nodelevel = 0;
		else nodelevel = father->getNodeLevel()+1;

#ifdef _DEBUG
//		std::cerr << "build node: " << min_ext << " - " << max_ext << std::endl;
#endif

#ifdef STORE_CELLS_IN_TREE
		tree->cells.push_back(this);
#endif

	}

// ****************************************************************************************

	~OcNode() {
		if (!m_isLeafNode) { // we have kids, kill them!
			delete children[0][0][0];
			delete children[1][0][0];
			delete children[0][1][0];
			delete children[1][1][0];
			delete children[0][0][1];
			delete children[1][0][1];
			delete children[0][1][1];
			delete children[1][1][1];
		}
	}

// ****************************************************************************************

	//! Inserts an object into this node. If the object does not intersect / is not contained in this node, it won't be inserted.
	inline int insertObjectAvoidMultiple(InsertedType* const obj_) {

		int pos = 0;

		// Check if triangle is contained in this node:
		if (!obj_->isContainedIn(min_ext, max_ext)) return pos;

		if (m_isLeafNode) {

			// Check if the object is already contained in the current node
			for (UInt i1 = 0; i1 < content.size(); i1++) {
				if (*obj_ == *content[i1]) {
					int position = reinterpret_cast<int>(content[i1]);
					//std::cerr << "duplicate found: " << position << std::endl;
					return position;
				}
			}

			if (getNumObjects() >= tree->getMaxNumObjectsPerNode()) {

				// split node;
				children[0][0][0] = new OcNode<T>(tree, getInterpolatedPos(0,0,0), getInterpolatedPos(1,1,1), this);
				children[1][0][0] = new OcNode<T>(tree, getInterpolatedPos(1,0,0), getInterpolatedPos(2,1,1), this);
				children[0][1][0] = new OcNode<T>(tree, getInterpolatedPos(0,1,0), getInterpolatedPos(1,2,1), this);
				children[1][1][0] = new OcNode<T>(tree, getInterpolatedPos(1,1,0), getInterpolatedPos(2,2,1), this);
				children[0][0][1] = new OcNode<T>(tree, getInterpolatedPos(0,0,1), getInterpolatedPos(1,1,2), this);
				children[1][0][1] = new OcNode<T>(tree, getInterpolatedPos(1,0,1), getInterpolatedPos(2,1,2), this);
				children[0][1][1] = new OcNode<T>(tree, getInterpolatedPos(0,1,1), getInterpolatedPos(1,2,2), this);
				children[1][1][1] = new OcNode<T>(tree, getInterpolatedPos(1,1,1), getInterpolatedPos(2,2,2), this);

				// distribute old objects to kids:
				for (UInt i1 = 0; i1 < content.size(); i1++) {
					propagateToChildrenAvoidMultiple(content[i1]);
				}
				// Add newly added point to children.
				pos = propagateToChildrenAvoidMultiple(obj_);

				// remove own storage:
				content.clear();

				// We're not longer leaf-node:
				m_isLeafNode = false;

			} else {
				content.push_back(obj_);
				return -1;
			}
		} else {
			// propagate object to children.
			pos = propagateToChildrenAvoidMultiple(obj_);
		}
		return pos;
	}

// ****************************************************************************************

	//! Prints the content of the current node.
	inline void printContent() {
		std::cerr << "Node: " << std::endl;
		for (UInt i1 = 0; i1 < content.size(); i1++) {
			content[i1]->print();
		}
		std::cerr << "------------------" << std::endl;
	}

// ****************************************************************************************

	//! Inserts an object into this node. If the object does not intersect / is not contained in this node, it won't be inserted.
	inline void insertObject(InsertedType* const obj_) {

		// Check if triangle is contained in this node:
		if (!obj_->isContainedIn(min_ext, max_ext)) return;

		if (m_isLeafNode) {
			if (getNumObjects() >= tree->getMaxNumObjectsPerNode()) {
				// split node;
				children[0][0][0] = new OcNode<T>(tree, getInterpolatedPos(0,0,0), getInterpolatedPos(1,1,1), this);
				children[1][0][0] = new OcNode<T>(tree, getInterpolatedPos(1,0,0), getInterpolatedPos(2,1,1), this);
				children[0][1][0] = new OcNode<T>(tree, getInterpolatedPos(0,1,0), getInterpolatedPos(1,2,1), this);
				children[1][1][0] = new OcNode<T>(tree, getInterpolatedPos(1,1,0), getInterpolatedPos(2,2,1), this);
				children[0][0][1] = new OcNode<T>(tree, getInterpolatedPos(0,0,1), getInterpolatedPos(1,1,2), this);
				children[1][0][1] = new OcNode<T>(tree, getInterpolatedPos(1,0,1), getInterpolatedPos(2,1,2), this);
				children[0][1][1] = new OcNode<T>(tree, getInterpolatedPos(0,1,1), getInterpolatedPos(1,2,2), this);
				children[1][1][1] = new OcNode<T>(tree, getInterpolatedPos(1,1,1), getInterpolatedPos(2,2,2), this);

				// distribute old objects to kids:
				for (UInt i1 = 0; i1 < content.size(); i1++) {
					propagateToChildren(content[i1]);
				}
				// Add newly added point to children.
				propagateToChildren(obj_);

				// remove own storage:
				content.clear();

				// We're not longer leaf-node:
				m_isLeafNode = false;

			} else {
				// insert in this node
				content.push_back(obj_);
			}
		} else {
			// propagate object to children.
			propagateToChildren(obj_);
		}
	}

// ****************************************************************************************

	inline Point getInterpolatedPos(UInt x, UInt y, UInt z) const {

		//Point pos;
		//pos.x = (min_ext.x*(2-x) + max_ext.x*(x)) / ValueType(2.0);
		//pos.y = (min_ext.y*(2-y) + max_ext.y*(y)) / ValueType(2.0);
		//pos.z = (min_ext.z*(2-z) + max_ext.z*(z)) / ValueType(2.0);

		return Point(
			(min_ext.x*(2-x) + max_ext.x*(x)) / ValueType(2.0),
			(min_ext.y*(2-y) + max_ext.y*(y)) / ValueType(2.0),
			(min_ext.z*(2-z) + max_ext.z*(z)) / ValueType(2.0)
			);
	}

// ****************************************************************************************

	//! Returns true if the node does not contain any further kids.
	inline bool isLeafNode() const {
		return m_isLeafNode;
	}

// ****************************************************************************************

	//! Returns the number of triangles in this node.
	inline UInt getNumObjects() const {
		return (UInt)content.size();
	}

// ****************************************************************************************

	//! Returns the min corner of the cell.
	Point getMin() const {
		return min_ext;
	}

// ****************************************************************************************

	//! Returns the min corner of the cell.
	Point getMax() const {
		return max_ext;
	}

// ****************************************************************************************

	UInt getNodeLevel() const {
		return nodelevel;
	}

// ****************************************************************************************

protected:


// ****************************************************************************************

	inline int propagateToChildrenAvoidMultiple(InsertedType* obj_) {
		int pos = children[0][0][0]->insertObjectAvoidMultiple(obj_);
		pos += children[1][0][0]->insertObjectAvoidMultiple(obj_);
		pos += children[0][1][0]->insertObjectAvoidMultiple(obj_);
		pos += children[1][1][0]->insertObjectAvoidMultiple(obj_);
		pos += children[0][0][1]->insertObjectAvoidMultiple(obj_);
		pos += children[1][0][1]->insertObjectAvoidMultiple(obj_);
		pos += children[0][1][1]->insertObjectAvoidMultiple(obj_);
		pos += children[1][1][1]->insertObjectAvoidMultiple(obj_);

		return pos;
	}

// ****************************************************************************************

	inline void propagateToChildren(InsertedType* obj_) {
		children[0][0][0]->insertObject(obj_);
		children[1][0][0]->insertObject(obj_);
		children[0][1][0]->insertObject(obj_);
		children[1][1][0]->insertObject(obj_);
		children[0][0][1]->insertObject(obj_);
		children[1][0][1]->insertObject(obj_);
		children[0][1][1]->insertObject(obj_);
		children[1][1][1]->insertObject(obj_);
	}

// ****************************************************************************************

private:

	UInt nodelevel;

	Point min_ext;
	Point max_ext;

	OcTree<InsertedType>* tree;

	bool m_isLeafNode;

	OcNode<InsertedType>* children[2][2][2];
	OcNode<InsertedType>* father;

	std::vector<InsertedType*> content;

	ValueType isovalue[2][2][2];
};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

//! An OcTree space partition for Triangles.
template <class T>
class OcTree {

public:

	typedef T InsertedType;
	typedef typename T::ValueType ValueType;
	typedef point3d<ValueType> Point;
	typedef OcNode<InsertedType> NodeType;
	static const int SizeOfInsertedObject = sizeof(InsertedType);

// ****************************************************************************************

	//! Allocate a new octree ranging from 'min_' to 'max_'.
	OcTree(UInt num_alloc_, UInt maxNumObjectsPerNode_ = 10, Point min_ = Point(0,0,0), Point max_ = Point(1,1,1))
	: maxNumObjectsPerNode(maxNumObjectsPerNode_), min_ext(min_), max_ext(max_), num_alloc(num_alloc_)
	{
//		std::cerr << "Allocated " << num_alloc << " points" << std::endl;
		objects = new InsertedType[num_alloc];
		head = new OcNode<InsertedType>(this, min_ext, max_ext);
		count = 0;
		pointsDuplicate = 0;
		pointsAdded = 0;

	}

// ****************************************************************************************

	//! Destructor.
	~OcTree() {
		delete[] objects;
		delete head;
	}

// ****************************************************************************************

	//! Inserts an object into the octree. returns false if the point was already contained in the octree.
	inline bool insertObjectAvoidMultiple(UInt& diff, InsertedType obj_) {

#ifdef _DEBUG
//		std::cerr << "Insert " << obj_ << std::endl;
#endif

		//objects.push_back(obj_);
		//head->insertObject(&objects[objects.size()-1]);

		pointsAdded++;

#ifdef _DEBUG
		if (count+1 >= (int)num_alloc) {
			std::cerr << "ERROR: capacity for octree reached" << std::endl;
			std::cerr << "Capacity is " << num_alloc << std::endl;
			std::cerr << "count is " << count << std::endl;
			std::cerr << "pointsAdded is " << pointsAdded << std::endl;
			std::cerr << "pointsDuplicate is " << pointsDuplicate << std::endl;
			std::cerr << "Allocate more memory for octree." << std::endl;
			std::cerr << "EXIT" << std::endl;
			exit(EXIT_FAILURE);
		}
#endif

		objects[count] = obj_;

		objectToInsert = &objects[count];

		int pos = head->insertObjectAvoidMultiple(objectToInsert);

		if (pos < 0) { // Point is unique.
			diff = count;
			count++;
			return true;
		}

		diff = (UInt)((pos - (int)objects)/SizeOfInsertedObject);

		pointsDuplicate++;

		if (diff >= (UInt)count) {
			obj_.print();
		}

		return false;
	}

// ****************************************************************************************

	//! Inserts an object into the octree.
	inline void insertObject(InsertedType obj_) {

#ifdef _DEBUG
//		std::cerr << "Insert " << obj_ << std::endl;
#endif

		//objects.push_back(obj_);
		//head->insertObject(&objects[objects.size()-1]);

		objects[count++] = obj_;

		objectToInsert = &objects[count-1];

		head->insertObject(objectToInsert);
	}

// ****************************************************************************************

	//! Returns how many objects may be maximally stored in each leaf-node.
	inline UInt getMaxNumObjectsPerNode() const {
		return maxNumObjectsPerNode;
	}

// ****************************************************************************************

	//! Returns the 'min' corner of the octree.
	inline Point getMin() const {
		return min_ext;
	}

// ****************************************************************************************

	//! Returns the 'max' corner of the octree.
	inline Point getMax() const {
		return max_ext;
	}

// ****************************************************************************************

	void printStatistics() const {
		std::cerr << "----------------------------" << std::endl;
		std::cerr << "Octree:" << std::endl;
#ifdef STORE_CELLS_IN_TREE
		std::cerr << "Number of cells: " << (int)cells.size() << std::endl;

		int num_leafes = 0;
		std::map<int, int> levels;
		std::map<int, int> numEntries;

		for (UInt i1 = 0; i1 < cells.size(); i1++) {
			if (cells[i1]->isLeafNode()) {
				num_leafes++;
				numEntries[cells[i1]->getNumObjects()]++;
			}

			levels[cells[i1]->getNodeLevel()]++;
		}


		std::cerr << "Number of leaf-nodes: " << num_leafes << std::endl;
		std::cerr << std::endl << "Cell levels: " << std::endl;
		for (std::map<int, int>::iterator it = levels.begin(); it != levels.end(); it++)
			std::cerr << "Level: " << it->first << "   count: " << it->second << std::endl;

		std::cerr << std::endl << "Number of objects per node: " << std::endl;
		for (std::map<int, int>::iterator it = numEntries.begin(); it != numEntries.end(); it++)
			std::cerr << "Num Objects: " << it->first << "   frequency: " << it->second << std::endl;

#endif
		std::cerr << "----------------------------" << std::endl;
		std::cerr << std::endl;
	}

// ****************************************************************************************

#ifdef STORE_CELLS_IN_TREE
	std::vector< NodeType* > cells;
#endif


	InsertedType* objectToInsert;



//	std::vector<InsertedType> objects;
	InsertedType* objects;
	int count;

	UInt pointsAdded;
	UInt pointsDuplicate;

private:


	Point min_ext;
	Point max_ext;
	UInt maxNumObjectsPerNode;
	UInt num_alloc;

	NodeType* head;
};

}

#endif
