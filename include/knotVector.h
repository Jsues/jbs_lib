#ifndef KNOT_VECTOR_H
#define KNOT_VECTOR_H


#include <vector>
#include <fstream>
#include "point2d.h"


//! A B-Spline Knot-Vector
template<class T>
class KnotVector {

public:

	//! Value-Type of Templates components.
	typedef T ValueType;

// ****************************************************************************************

	//! Dummy Constructor.
	KnotVector(){};

// ****************************************************************************************

	//! Constructs an knot-vector for a degree 'deg' B-Spline from the given knots.
	KnotVector(UInt deg_, std::vector<ValueType> knot_) {
		deg = deg_;
		knot = knot_;
		n = (UInt)knot.size() - (deg+1);

		// Check if the knot vector is ment to be end-point-interpolating (first and last (deg+1) knots are equal).
		bool end_point_interpolating = true;
		for (UInt i1 = 0; i1 < deg; i1++)
			if (knot[i1] != knot[i1+1])
				end_point_interpolating = false;
		for (UInt i1 = (UInt)knot.size()-deg-1; i1 < knot.size()-2; i1++)
			if (knot[i1] != knot[i1+1])
				end_point_interpolating = false;

		if (end_point_interpolating) {
			std::cerr << "Knot-vector is end-point-interpolating" << std::endl;
			knot[0] = knot[1]-(ValueType)0.1;
			knot[knot.size()-1] = knot[knot.size()-2]+(ValueType)0.1;
		}
	}

// ****************************************************************************************

	//! Constructs an end-point-interpolating uniform knot-vector for a degree 'deg' B-Spline with 'n' control-points.
	KnotVector(UInt deg_, UInt n_) {

		deg = deg_;
		n = n_;

		if (deg+1 > n) {
			std::cerr << "KnotVector::KnotVector(....): To few control points for this degree" << std::endl;
			std::cerr << "with degree=" << deg << ", num_ctrl_pts=" << n << std::endl;
			exit(EXIT_FAILURE);
		}
		
		UInt knot_vector_size = (deg+1)*2 + (n-1-deg);
		knot.push_back((ValueType)-0.1);
		for (UInt i1 = 1; i1 <= deg; i1++)
			knot.push_back((ValueType)0);
		// Inner none-zero knots:
		int num_none_zero = knot_vector_size - (deg+1)*2;
		for (int i1 = 0; i1 < num_none_zero; i1++)
			knot.push_back((ValueType)(((double)i1+1)/((double)num_none_zero+1)));
		for (UInt i1 = 1; i1 <= deg; i1++)
			knot.push_back((ValueType)1);
		knot.push_back((ValueType)1.1);
	}

// ****************************************************************************************

	//! Prints the knot-vector.
	void print() const {
		std::cerr << "Knot vector: degree = " << deg << " ,  number of control-points = " << n << std::endl;
		for (UInt i1 = 0; i1 < knot.size(); i1++)
			std::cerr << knot[i1] << " ";
		std::cerr << std::endl;
	}

// ****************************************************************************************

	//! Evaluates the bspline-basis at 'u'
	ValueType evalBasis(UInt i, ValueType u) const {
		return Basis(i, deg, u);
	}

// ****************************************************************************************

	//! Returns the range of full support.
	point2d<ValueType> getRange() const {
		return point2d<ValueType>(knot[deg], knot[knot.size()-deg-1]);
	}

// ****************************************************************************************

	UInt getNumOfPoints() const {
		return n;
	}

// ****************************************************************************************

	size_t size() const {
		return knot.size();
	}

// ****************************************************************************************

	ValueType operator[](UInt i) const {
		return knot[i];
	}

// ****************************************************************************************

	UInt getDegree() const {
		return deg;
	}

// ****************************************************************************************

	int getInsertPosition(ValueType u) const {

		for (UInt i1 = 0; i1 < knot.size()-1; i1++) {
			if ((knot[i1] <= u) && (knot[i1+1] >= u)) {
				return i1;
			}
		}
		std::cerr << "Error, unable to insert knot at: " << u << std::endl;
		std::cerr << "In: KnotVector::insertKnot(ValueType u)" << std::endl;
		exit(EXIT_FAILURE);

		return -1;
	}

// ****************************************************************************************

	//! Inserts the knot 'u' at the appropriate index.
	int insertKnot(ValueType u) {
		int new_pos = getInsertPosition(u);
		knot.insert(knot.begin()+new_pos+1, u);

		n = n+1;

		return new_pos;
	}

// ****************************************************************************************

	//! Scales the values in the knot-vector such that the last element is 1;
	void normalize() {
		ValueType fac = knot[knot.size()-1];
		for (UInt i1 = 0; i1 < knot.size(); i1++)
			knot[i1] /= fac;
	}

// ****************************************************************************************

	std::vector<ValueType> knot;

private:

// ****************************************************************************************

	ValueType Basis(UInt i, UInt degree, ValueType t) const {

		if  (degree == 0) { // Recursion termination
			if ( ( t >= knot[i]) && ( t < knot[i+1]) ) {
				return 1;
			} else {
				return 0;
			}
		} else {
	      
			if ( (knot[i+degree+1] - knot[i]) == 0 ) {
				return 0;
			}
			if ( (knot[i+degree+1] - knot[i+1]) == 0 ) {
				ValueType const1 = ( t - knot[i] ) / ( knot[i+degree] - knot[i] );
				return (const1 * Basis(i, degree-1, t));
			}
			if ( (knot[i+degree] - knot[i]) == 0 ) {
				ValueType const2 = ( knot[i+degree+1] - t ) / ( knot[i+degree+1] - knot[i+1]);
				return (const2 * Basis(i+1, degree-1, t));
			}

			/* else */  
			ValueType const1 = ( t - knot[i] ) / ( knot[i+degree] - knot[i] );
			ValueType const2 = ( knot[i+degree+1] - t ) / ( knot[i+degree+1] - knot[i+1]);
			return (const1 * Basis(i, degree-1, t) + const2 * Basis(i+1, degree-1, t));
		}
	}

// ****************************************************************************************

	UInt deg;
	UInt n;
};

#endif
