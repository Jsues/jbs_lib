#ifndef JBS_ICP_H
#define JBS_ICP_H

#include <algorithm>
#include <ctime>
#include "HoppeImplicitFunc.h"
#include "PolyLine.h"
#include "rigid_transform.h"
#include "SVD_Matrix.h"


template <class Point>
typename Point::ValueType transformPCN(PointCloudNormals<Point>* pc, RigidTransform<Point> rtrans) {

	Point::ValueType d = 0;

	for (int i1 = 0; i1 < pc->getNumPts(); i1++) {
		vec3f p = pc->getPoint(i1);
		vec3f n = pc->getNormals()[i1];

		vec3f p2 = rtrans.vecTrans(p);
		n = rtrans.normTrans(n);
		pc->getPoints()[i1] = p2;
		pc->getNormals()[i1] = n;
		d += p2.dist(p);
	}
	return d;
}


template <class T>
struct Correspondence {
public:
	Correspondence(const T& p0, const T& p1) : pt0(p0), pt1(p1) {}

	bool operator<(const Correspondence<T>& other) {
		return ((pt0.squareDist(pt1)) < (other.pt0.squareDist(other.pt1)));
	}

	T pt0, pt1;
};


template <class T>
struct CorrespondenceWithNormal {
public:
	CorrespondenceWithNormal(const T& p0, const T& p1, const T& n) : pt0(p0), pt1(p1), n0(n) {}

	bool operator<(const CorrespondenceWithNormal<T>& other) {
		return ((pt0.squareDist(pt1)) < (other.pt0.squareDist(other.pt1)));
		//return fabs((pt0-pt1)|n0) < fabs((other.pt0-other.pt1)|other.n0);
	}

	T pt0, n0, pt1;
};


template <class T>
class ICP_aligner {
	
public:

	typedef T Point;
	typename typedef T::ValueType ValueType;

	ICP_aligner (PointCloudNormals<Point>* pc_fixed_) : pc_fixed(pc_fixed_) {

		hoppe = new HoppeImplicitFit<Point>(pc_fixed);

		isBoundary = new bool[pc_fixed->getNumPts()];
		for (int i1 = 0; i1 < pc_fixed->getNumPts(); i1++) isBoundary[i1] = false;

	}

	~ICP_aligner() {

		delete hoppe;
		delete[] isBoundary;

	}

	void computeBoundary(int numN = 20) {

		int* ids = new int[numN];
		double* dists = new double[numN];

		for (int i1 = 0; i1 < pc_fixed->getNumPts(); i1++) {

			const Point& pt = pc_fixed->getPoints()[i1];

			hoppe->getNearestNeighbors(pt, numN, ids, dists);

			Point avg;
			for (int i2 = 0; i2 < numN; i2++) {
				const Point& p = pc_fixed->getPoints()[ids[i2]];
				avg += p;
			}
			avg /= (ValueType)numN;

			ValueType avgD = 0;
			for (int i2 = 0; i2 < numN; i2++) {
				const Point& p = pc_fixed->getPoints()[ids[i2]];
				Point d = p-avg;
				avgD += d.length();
			}
			avgD /= numN;

			ValueType inPlaneDispl = avg.dist(pt);
			if (inPlaneDispl > avgD/2.5) isBoundary[i1] = true;
		}
	}

	void storeBoundary() {

		PointCloudNormals<Point> pc;
		for (int i1 = 0; i1 < pc_fixed->getNumPts(); i1++) {
			if (isBoundary[i1]) {
				pc.insertPoint(pc_fixed->getPoints()[i1], pc_fixed->getNormals()[i1]);
			}
		}
		PointCloudWriter<Point>::writePWNFileBinary(&pc, "boundary.pcb");
	}

	RigidTransform<Point> step(const PointCloudNormals<Point>* const pc_move, ValueType divider=4, int step=10, std::vector< std::vector<int> >* normalSpaceSampling = NULL, int numCorrs = 1000) {

		std::vector<Correspondence<Point> > corrs;

		srand((unsigned int)time(NULL));

		if (normalSpaceSampling == NULL) {
			for (UInt i1 = 0; i1 < (UInt)pc_move->getNumPts(); i1 += step) {
				const Point& pt = pc_move->getPoints()[i1];
				int id = hoppe->getNearestID(pt);

				Point near_pt = pc_fixed->getPoints()[id];
				Point near_n = pc_fixed->getNormals()[id];

				Point dir = pt-near_pt;
				ValueType dist = dir|near_n;

				Point bestFit = pt - (near_n*dist);
				//corrs.push_back(Correspondence<Point>(bestFit, pt));
				corrs.push_back(Correspondence<Point>(near_pt, pt));

				//pl.insertVertex(bestFit);
				//pl.insertVertex(pt);
				//pl.insertLine(pl.getNumV()-2, pl.getNumV()-1);
			}
			//pl.writeToFile("corr.pl");
		} else {
			int numBins = normalSpaceSampling->size();

			int drawsPerBin = numCorrs/numBins;

			for (int i1 = 0; i1 < numCorrs; i1++) {
				int bin = rand()%numBins;

				int numInBin = (*normalSpaceSampling)[bin].size();
				if (numInBin == 0) continue;


				if (rand()%drawsPerBin > numInBin) continue;

				int sample = rand()%numInBin;
				int pid = (*normalSpaceSampling)[bin][sample];

				const Point& pt = pc_move->getPoints()[pid];
				int id = hoppe->getNearestID(pt);

				Point near_pt = pc_fixed->getPoints()[id];
				Point near_n = pc_fixed->getNormals()[id];

				if ((near_n|pc_move->getNormals()[pid]) < 0.6) // reject bad normals
					continue;

				Point dir = pt-near_pt;
				ValueType dist = dir|near_n;

				Point bestFit = pt - (near_n*dist);
				//corrs.push_back(Correspondence<Point>(bestFit, pt));
				corrs.push_back(Correspondence<Point>(near_pt, pt));

			}
		}

		std::sort(corrs.begin(), corrs.end());
		UInt max = (UInt)(corrs.size() / divider);

		ValueType dist = 0;
		// Compute cog of both point clouds:
		Point cog_f_i(0,0,0);
		Point cog_f_i1(0,0,0);
		for (UInt i1 = 0; i1 < max; i1++) {
			cog_f_i  += corrs[i1].pt0;
			cog_f_i1 += corrs[i1].pt1;

			dist += corrs[i1].pt0.dist(corrs[i1].pt1);
		}
		cog_f_i /= (ValueType)max;
		cog_f_i1 /= (ValueType)max;
		//std::cerr << "Dist: " << dist << std::endl;

		// Build cross-covariance matrix:
		SquareMatrixND<Point> COV;
		for (UInt i1 = 0; i1 < max; i1++) {
			COV.addFromTensorProduct((corrs[i1].pt1-cog_f_i1),(corrs[i1].pt0-cog_f_i));
		}

		// Compute aligning rotation
		SquareMatrixND<Point> U, V;
		Point sigma;
		bool SVD_result = COV.SVD_decomp(U, sigma, V);
		V.transpose();
		SquareMatrixND<Point> rot_matrix = U*V;


		ValueType det = rot_matrix.getDeterminant();
		if (det < 0) {
			int sm_id = 0;
			double smallest = sigma[sm_id];
			for (UInt dd = 1; dd < 3; dd++) {
				if (sigma[dd] < smallest) {
					smallest = sigma[dd];
					sm_id = dd;
				}
			}
			// flip sign of entries in colums 'sm_id' in 'U'
			U.m[sm_id + 0] *= -1;
			U.m[sm_id + 3] *= -1;
			U.m[sm_id + 6] *= -1;
			rot_matrix = U*V;
		}
		rot_matrix.transpose();

		Point new_cog = rot_matrix.vecTrans(cog_f_i1);
		vec3f cog_offset = cog_f_i - new_cog;

		trans = RigidTransform<Point>(cog_offset, rot_matrix);

		return trans;
	}

	RigidTransform<Point> stepPointPlaneICP(const PointCloudNormals<Point>* const pc_move, ValueType divider=4, int step=10, std::vector< std::vector<int> >* normalSpaceSampling = NULL, int numCorrs = 1000) {

		std::vector<CorrespondenceWithNormal<Point> > corrs;


		srand((unsigned int)time(NULL));

		if (normalSpaceSampling == NULL) {
			for (UInt i1 = 0; i1 < (UInt)pc_move->getNumPts(); i1 += step) {
				const Point& pt = pc_move->getPoints()[i1];
				int id = hoppe->getNearestID(pt);

				if (isBoundary[id]) // closest point is a boundary point -> not a valid correspondence
					continue;

				Point near_pt = pc_fixed->getPoints()[id];
				Point near_n = pc_fixed->getNormals()[id];

				if (near_n.squaredLength() > 0) corrs.push_back(CorrespondenceWithNormal<Point>(near_pt, pt, near_n));
			}
		} else {
			int numBins = normalSpaceSampling->size();

			int drawsPerBin = numCorrs/numBins;

			//PolyLine<vec3f> pl;
			for (int i1 = 0; i1 < numCorrs; i1++) {
				int bin = rand()%numBins;

				int numInBin = (*normalSpaceSampling)[bin].size();

				if (numInBin == 0) continue;

				//if (rand()%drawsPerBin > numInBin) continue;

				int sample = rand()%numInBin;
				int pid = (*normalSpaceSampling)[bin][sample];

				const Point& pt = pc_move->getPoints()[pid];
				int id = hoppe->getNearestID(pt);

				const Point& near_pt = pc_fixed->getPoints()[id];
				const Point& near_n = pc_fixed->getNormals()[id];

				if ((near_n|pc_move->getNormals()[pid]) < 0.6) // reject bad normals
					continue;

				if (near_n.squaredLength() > 0) corrs.push_back(CorrespondenceWithNormal<Point>(near_pt, pt, near_n));
				//pl.insertVertex(near_pt);
				//pl.insertVertex(pt);
				//pl.insertLine(pl.getNumV()-2, pl.getNumV()-1);
			}
			//pl.writeToFile("corr.pl");
			//exit(1);
		}

		std::sort(corrs.begin(), corrs.end());
		UInt max = (UInt)(corrs.size() / divider);

		SVDMatrix<float> mat(max, 6);

		float* rhs = new float[max+1];

		for (UInt i1 = 0; i1 < max; i1++) {

			const CorrespondenceWithNormal<Point>& c = corrs[i1];

			vec3f tmp = c.n0^c.pt1;
			mat(i1, 0) = tmp.x;
			mat(i1, 1) = tmp.y;
			mat(i1, 2) = tmp.z;
			mat(i1, 3) = c.n0.x;
			mat(i1, 4) = c.n0.y;
			mat(i1, 5) = c.n0.z;

			rhs[i1+1] = (c.n0|c.pt0) - (c.n0|c.pt1);
		}

		mat.computeSVD();
		float x[7];
		mat.backSolveSVD(rhs, x);

		float alpha = x[1];
		float beta = x[2];
		float gamma = x[3];

		SquareMatrixND<Point> rot;
		rot(0,0) = cos(gamma)*cos(beta); rot(0,1) = -sin(gamma)*cos(alpha) + cos(gamma)*sin(beta)*sin(alpha); rot(0,2) = sin(gamma)*sin(alpha) + cos(gamma)*sin(beta)*cos(alpha);
		rot(1,0) = sin(gamma)*cos(beta); rot(1,1) = cos(gamma)*cos(alpha) + sin(gamma)*sin(beta)*sin(alpha);  rot(1,2) = -cos(gamma)*sin(alpha) + sin(gamma)*sin(beta)*cos(alpha);
		rot(2,0) = -sin(beta);           rot(2,1) = cos(beta)*sin(alpha);                                     rot(2,2) = cos(beta)*cos(alpha);

		rot.transpose();
		vec3f mov(x[4], x[5], x[6]);
		//mov = rot.vecTrans(mov);
		delete[] rhs;
		return RigidTransform<Point>(mov, rot);
	}

	RigidTransform<Point> trans;

private:

	bool* isBoundary;

	PointCloudNormals<Point>* pc_fixed;
	PointCloudNormals<Point>* pc_move;
	HoppeImplicitFit<Point>* hoppe;
};

#endif
