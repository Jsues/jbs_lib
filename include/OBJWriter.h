#ifndef OBJ_WRITER_OFF
#define OBJ_WRITER_OFF

#include <vector>
#include <fstream>
#include <string>
#include "point3d.h"
#include "point4d.h"

//****************************************************************************************************

//! Reads the vertices and the triangles of the OBJ file 'filename'. Returns 'true' on success.
template <class T, class U, class V>
bool readOBJFile(std::vector<T>& verts, std::vector<T>& normals, std::vector<U>& texturecoords, std::vector<V>& tris, const char* filename) {
    
    std::ifstream InFile(filename);
    // Fehler abfangen
    if (InFile.fail()) {
        std::cerr << "Fehler beim Oeffnen der Datei: " << filename << std::endl;
        return false;
    }
    
    char typ;
    float x,y,z;
    vec3i vertexid, textureid, normalid;
    char line[2000];
    char tester;
    
    bool bEnd = false;
    while(!InFile.eof() || bEnd) {
        InFile >> typ;
        //fprintf(stderr, "%c  ",typ);
        if (InFile.eof()) break;
        switch(typ) {
            case 'v':
                // This may be a vertex 'v' or a vertex normal 'vn'
                if (InFile.peek() == 'n') { // dude, it's a normal
                    InFile >> typ;
                    InFile >> x >> y >> z;
                    normals.push_back(T(x,y,z));
                } else if (InFile.peek() == 't') { // dude, it's a textur coordinate
                    //std::cerr << "Read textcoord" << std::endl;
                    InFile >> typ;
                    InFile >> x >> y;
                    texturecoords.push_back(U(x,y));
                } else {
                    InFile >> x >> y >> z;
                    verts.push_back(T(x,y,z));
                }
                if (InFile.eof()) {bEnd = true; break;};
                break;
            case 'f':
                
                InFile >> vertexid.x;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v0\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') { // eskoennte sein, dass er fehlt, dass sieht dann so aus: 1 // 7
                        InFile >> textureid.x;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t0\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester >> normalid.x;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n0\n"); break;}
                }
                
                InFile >> vertexid.y;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v1\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.y;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t1\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester >> normalid.y;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n1\n"); break;}
                }
                
                InFile >> vertexid.z;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v2\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.z;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t2\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester >> normalid.z;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n2\n"); break;}
                }
                
                tris.push_back(V(vertexid.x-1, vertexid.y-1, vertexid.z-1));
                
                break;
            default:
                InFile.getline(line, 2000);	//TODO this is ugly I know
                break;
        }
    }
    
    InFile.close();
    return true;
    
}

//! Reads the vertices and the triangles of the OBJ file 'filename' into the vectors 'verts' and 'tris', ignores tex-coords & normals. Returns 'true' on success.
template <class T>
bool readOBJFileNormals(std::vector< T >& verts, std::vector<T>& normals, std::vector< point3d<int> >& tris, const char* filename) {
    
    std::ifstream InFile(filename);
    // Fehler abfangen
    if (InFile.fail()) {
        std::cerr << "Fehler beim Oeffnen der Datei: " << filename << std::endl;
        return false;
    }
    
    char typ;
    float x,y,z;
    vec3i vertexid, textureid, normalid;
    char line[2000];
    char tester;
    
    std::vector<vec2d> texturecoords;
    
    bool bEnd = false;
    while(!InFile.eof() || bEnd) {
        InFile >> typ;
        //fprintf(stderr, "%c  ",typ);
        if (InFile.eof()) break;
        switch(typ) {
            case 'v':
                // This may be a vertex 'v' or a vertex normal 'vn'
                if (InFile.peek() == 'n') { // dude, it's a normal
                    InFile >> typ;
                    InFile >> x >> y >> z;
                    normals.push_back(vec3f(x,y,z));
                } else if (InFile.peek() == 't') { // dude, it's a textur coordinate
                    //std::cerr << "Read textcoord" << std::endl;
                    InFile >> typ;
                    InFile >> x >> y;
                    texturecoords.push_back(vec2d(x,y));
                } else {
                    InFile >> x >> y >> z;
                    verts.push_back(T(x,y,z));
                }
                if (InFile.eof()) {bEnd = true; break;};
                break;
            case 'f':
                
                InFile >> vertexid.x;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v0\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') { // eskoennte sein, dass er fehlt, dass sieht dann so aus: 1 // 7
                        InFile >> textureid.x;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t0\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester >> normalid.x;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n0\n"); break;}
                }
                
                InFile >> vertexid.y;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v1\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.y;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t1\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester >> normalid.y;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n1\n"); break;}
                }
                
                InFile >> vertexid.z;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v2\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.z;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t2\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester >> normalid.z;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n2\n"); break;}
                }
                
                tris.push_back(point3d<int>(vertexid.x-1, vertexid.y-1, vertexid.z-1));
                
                break;
            default:
                InFile.getline(line, 2000);	//TODO this is ugly I know
                break;
        }
    }
    
    InFile.close();
    return true;
    
}

//****************************************************************************************************


//! Reads the vertices and the triangles of the OBJ file 'filename' into the vectors 'verts' and 'tris', ignores tex-coords & normals. Returns 'true' on success.
template <class T>
bool readOBJFileNoTex(std::vector< T >& verts, std::vector< point3d<int> >& tris, const char* filename) {
    
    std::ifstream InFile(filename);
    // Fehler abfangen
    if (InFile.fail()) {
        std::cerr << "Fehler beim Oeffnen der Datei: " << filename << std::endl;
        return false;
    }
    
    char typ;
    float x,y,z;
    vec3i vertexid, textureid, normalid;
    char line[2000];
    char tester;
    
    std::vector<T> normals;
    std::vector<vec2d> texturecoords;
    
    bool bEnd = false;
    while(!InFile.eof() || bEnd) {
        InFile >> typ;
        //fprintf(stderr, "%c  ",typ);
        if (InFile.eof()) break;
        switch(typ) {
            case 'v':
                // This may be a vertex 'v' or a vertex normal 'vn'
                if (InFile.peek() == 'n') { // dude, it's a normal
                    InFile >> typ;
                    InFile >> x >> y >> z;
                    normals.push_back(vec3f(x,y,z));
                } else if (InFile.peek() == 't') { // dude, it's a textur coordinate
                    //std::cerr << "Read textcoord" << std::endl;
                    InFile >> typ;
                    InFile >> x >> y;
                    texturecoords.push_back(vec2d(x,y));
                } else {
                    InFile >> x >> y >> z;
                    verts.push_back(T(x,y,z));
                }
                if (InFile.eof()) {bEnd = true; break;};
                break;
            case 'f':
                
                InFile >> vertexid.x;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v0\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') { // eskoennte sein, dass er fehlt, dass sieht dann so aus: 1 // 7
                        InFile >> textureid.x;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t0\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester >> normalid.x;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n0\n"); break;}
                }
                
                InFile >> vertexid.y;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v1\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.y;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t1\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester >> normalid.y;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n1\n"); break;}
                }
                
                InFile >> vertexid.z;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v2\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.z;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t2\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester >> normalid.z;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n2\n"); break;}
                }
                
                tris.push_back(point3d<int>(vertexid.x-1, vertexid.y-1, vertexid.z-1));
                
                break;
            default:
                InFile.getline(line, 2000);	//TODO this is ugly I know
                break;
        }
    }
    
    InFile.close();
    return true;
    
}

template <class T, class V>
bool readOBJQuadFile(std::vector<T>& verts, std::vector<V>& quadVertexIndices, const char* filename)
{
    std::vector<T> normalDummy;
    std::vector<vec2i> textureDummy;
    std::vector<V> qniDummy, qtiDummy;
    return readOBJQuadFile(verts, normalDummy, textureDummy, quadVertexIndices, qniDummy, qtiDummy, filename);
}

//****************************************************************************************************

//! Reads the vertices and the triangles of the OBJ file 'filename'. Returns 'true' on success.
template <class T, class U, class V>
bool readOBJQuadFile(std::vector<T>& verts, std::vector<T>& normals, std::vector<U>& texturecoords, std::vector<V>& quadVertexIndices, std::vector<V>& quadNormalIndices, std::vector<V>& quadTextureIndices, const char* filename) {
    
    //std::cerr << "Reading obj quad file " << filename << std::endl;
    
    std::ifstream InFile(filename);
    // Fehler abfangen
    if (InFile.fail()) {
        std::cerr << "Fehler beim Oeffnen der Datei: " << filename << std::endl;
        return false;
    }
    
    char typ;
    float x,y,z;
    char line[2000];
    char tester;
    
    bool bEnd = false;
    while(!InFile.eof() || bEnd) {
        InFile >> typ;
        //fprintf(stderr, "%c  ",typ);
        if (InFile.eof()) break;
        switch(typ) {
            case 'v':
                // This may be a vertex 'v' or a vertex normal 'vn'
                if (InFile.peek() == 'n') { // dude, it's a normal
                    InFile >> typ;
                    InFile >> x >> y >> z;
                    normals.push_back(T(x,y,z));
                } else if (InFile.peek() == 't') { // dude, it's a textur coordinate
                    //std::cerr << "Read textcoord" << std::endl;
                    InFile >> typ;
                    InFile >> x >> y;
                    texturecoords.push_back(U(x,y));
                } else {
                    InFile >> x >> y >> z;
                    verts.push_back(T(x,y,z));
                }
                if (InFile.eof()) {bEnd = true; break;};
                break;
            case 'f':
            {
                vec4i vertexid(-1);
                vec4i normalid(-1);
                vec4i textureid(-1);
                
                InFile >> vertexid.x;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v0\n"); return false;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') { // eskoennte sein, dass er fehlt, dass sieht dann so aus: 1 // 7
                        InFile >> textureid.x;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t0\n"); return false;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester;
                    int peek = InFile.peek();
                    if (peek != ' ' && peek != '\r' && peek != '\n') {
                        InFile >> normalid.x;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n0\n"); return false;}
                    }
                }
                
                InFile >> vertexid.y;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v1\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.y;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t1\n"); return false;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester;
                    int peek = InFile.peek();
                    if (peek != ' ' && peek != '\r' && peek != '\n') {
                        InFile >> normalid.y;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n0\n"); return false;}
                    }
                }
                
                InFile >> vertexid.z;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v2\n"); return false;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.z;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t2\n"); return false;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester;
                    int peek = InFile.peek();
                    if (peek != ' ' && peek != '\r' && peek != '\n') {
                        InFile >> normalid.z;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n0\n"); return false;}
                    }
                }
                
                // This is most likely a triangle mesh
                int peek = InFile.peek();
                if (InFile.eof() || peek == '\r' || peek == '\n') {
                    bEnd = true;
                    fprintf(stderr, "Failed to read quad mesh: fourt vertex index is missing\n");
                    return false;
                }
                
                InFile >> vertexid.w;
                if (InFile.fail()) {
                    bEnd = true;
                    fprintf(stderr, "Failed to read quad mesh: fourt vertex index is missing\n");
                    return false;
                }

                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v3\n"); return false;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.w;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t3\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester;
                    int peek = InFile.peek();
                    if (peek != ' ' && peek != '\r' && peek != '\n') {
                        InFile >> normalid.w;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n0\n"); break;}
                    }
                }
                
                quadVertexIndices.push_back(V(vertexid.x-1, vertexid.y-1, vertexid.z-1, vertexid.w-1));
                if (normalid.x != -1) {
                    quadNormalIndices.push_back(V(normalid.x-1, normalid.y-1,normalid.z-1,normalid.w-1));
                } else {
                    quadNormalIndices.push_back(V(vertexid.x-1, vertexid.y-1, vertexid.z-1, vertexid.w-1));
                }
                if (textureid.x != -1) {
                    quadTextureIndices.push_back(V(textureid.x-1, textureid.y-1,textureid.z-1,textureid.w-1));
                } else {
                    quadTextureIndices.push_back(V(vertexid.x-1, vertexid.y-1, vertexid.z-1, vertexid.w-1));
                }
                
                break;
            }
            default:
                InFile.getline(line, 2000);	//TODO this is ugly I know
                break;
        }
    }
    
    //std::cerr << "Reading obj quad file done" << std::endl;
    InFile.close();
    return true;
    
}

//! Reads the vertices and the triangles of the OBJ file 'filename' into the vectors 'verts' and 'tris', ignores tex-coords & normals. Returns 'true' on success.
template <class T>
bool readOBJQuadFileNoTex(std::vector< T >& verts, std::vector< point4d<int> >& quads, const char* filename) {
    
    std::cerr << "Reading obj quad file " << filename << std::endl;
    
    std::ifstream InFile(filename);
    // Fehler abfangen
    if (InFile.fail()) {
        std::cerr << "Fehler beim Oeffnen der Datei: " << filename << std::endl;
        return false;
    }
    
    char typ;
    float x,y,z;
    vec4i vertexid, textureid, normalid;
    char line[2000];
    char tester;
    
    std::vector<T> normals;
    std::vector<vec2d> texturecoords;
    
    bool bEnd = false;
    while(!InFile.eof() || bEnd) {
        InFile >> typ;
        //fprintf(stderr, "%c  ",typ);
        if (InFile.eof()) break;
        switch(typ) {
            case 'v':
                // This may be a vertex 'v' or a vertex normal 'vn'
                if (InFile.peek() == 'n') { // dude, it's a normal
                    InFile >> typ;
                    InFile >> x >> y >> z;
                    normals.push_back(vec3f(x,y,z));
                } else if (InFile.peek() == 't') { // dude, it's a textur coordinate
                    //std::cerr << "Read textcoord" << std::endl;
                    InFile >> typ;
                    InFile >> x >> y;
                    texturecoords.push_back(vec2d(x,y));
                } else {
                    InFile >> x >> y >> z;
                    verts.push_back(T(x,y,z));
                }
                if (InFile.eof()) {bEnd = true; break;};
                break;
            case 'f':
                
                InFile >> vertexid.x;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v0\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') { // eskoennte sein, dass er fehlt, dass sieht dann so aus: 1 // 7
                        InFile >> textureid.x;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t0\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester >> normalid.x;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n0\n"); break;}
                }
                
                InFile >> vertexid.y;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v1\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.y;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t1\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester >> normalid.y;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n1\n"); break;}
                }
                
                InFile >> vertexid.z;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v2\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.z;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t2\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester >> normalid.z;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n2\n"); break;}
                }
                
                InFile >> vertexid.w;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v3\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.w;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t3\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester >> normalid.w;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n3\n"); break;}
                }
                
                quads.push_back(point4d<int>(vertexid.x-1, vertexid.y-1, vertexid.z-1, vertexid.w-1));
                
                break;
            default:
                InFile.getline(line, 2000);	//TODO this is ugly I know
                break;
        }
    }
    
    std::cerr << "Reading obj quad file done" << std::endl;
    InFile.close();
    return true;
    
}

//****************************************************************************************************

//! Reads the vertices and the triangles of the OBJ file 'filename' into the vectors 'verts' and 'tris', ignores tex-coords & normals. Returns 'true' on success.
template <class T>
bool readOBJFileFacesAsString(std::vector< T >& verts, std::vector< std::string >& faces, const char* filename) {
    
    std::cerr << "Reading obj quad file" << std::endl;
    
    std::ifstream InFile(filename);
    // Fehler abfangen
    if (InFile.fail()) {
        std::cerr << "Fehler beim Oeffnen der Datei: " << filename << std::endl;
        return false;
    }
    
    char typ;
    float x,y,z;
    vec4i vertexid, textureid, normalid;
    char line[2000];
    
    std::vector<T> normals;
    std::vector<vec2d> texturecoords;
    
    bool bEnd = false;
    while(!InFile.eof() || bEnd) {
        InFile >> typ;
        //fprintf(stderr, "%c  ",typ);
        if (InFile.eof()) break;
        switch(typ) {
            case 'v':
                // This may be a vertex 'v' or a vertex normal 'vn'
                if (InFile.peek() == 'n') { // dude, it's a normal
                    InFile >> typ;
                    InFile >> x >> y >> z;
                    normals.push_back(vec3f(x,y,z));
                } else if (InFile.peek() == 't') { // dude, it's a textur coordinate
                    //std::cerr << "Read textcoord" << std::endl;
                    InFile >> typ;
                    InFile >> x >> y;
                    texturecoords.push_back(vec2d(x,y));
                } else {
                    InFile >> x >> y >> z;
                    verts.push_back(T(x,y,z));
                }
                if (InFile.eof()) {bEnd = true; break;};
                break;
            case 'f':
                
                InFile.getline(line, 2000);
                faces.push_back(std::string(line));
                
                break;
            default:
                InFile.getline(line, 2000);	//TODO this is ugly I know
                break;
        }
    }
    
    std::cerr << "Reading obj quad file done" << std::endl;
    InFile.close();
    return true;
    
}

//****************************************************************************************************


//! Reads the vertices and the triangles of the OBJ file 'filename' into the vectors 'verts' and 'tris', ignores normals. Returns 'true' on success.
template <class T>
bool readOBJQuadWithTex(std::vector< T >& verts, std::vector< point4d<int> >& quads, std::vector< point2d<double> >& texturecoords, const char* filename) {
    
    std::ifstream InFile(filename);
    // Fehler abfangen
    if (InFile.fail()) {
        std::cerr << "Fehler beim Oeffnen der Datei: " << filename << std::endl;
        return false;
    }
    
    char typ;
    float x,y,z;
    vec4i vertexid, textureid, normalid;
    char line[2000];
    char tester;
    
    std::vector<T> normals;
    
    bool bEnd = false;
    while(!InFile.eof() || bEnd) {
        InFile >> typ;
        //fprintf(stderr, "%c  ",typ);
        if (InFile.eof()) break;
        switch(typ) {
            case 'v':
                // This may be a vertex 'v' or a vertex normal 'vn'
                if (InFile.peek() == 'n') { // dude, it's a normal
                    InFile >> typ;
                    InFile >> x >> y >> z;
                    normals.push_back(vec3f(x,y,z));
                } else if (InFile.peek() == 't') { // dude, it's a textur coordinate
                    //std::cerr << "Read textcoord" << std::endl;
                    InFile >> typ;
                    InFile >> x >> y;
                    texturecoords.push_back(vec2d(x,y));
                } else {
                    InFile >> x >> y >> z;
                    verts.push_back(T(x,y,z));
                }
                if (InFile.eof()) {bEnd = true; break;};
                break;
            case 'f':
                InFile >> vertexid.x;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v0\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') { // eskoennte sein, dass er fehlt, dass sieht dann so aus: 1 // 7
                        InFile >> textureid.x;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t0\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester;
                    if (InFile.peek() != ' ') InFile >> normalid.x;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n0\n"); break;}
                }
                
                InFile >> vertexid.y;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v1\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.y;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t1\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester;
                    if (InFile.peek() != ' ') InFile >> normalid.y;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n1\n"); break;}
                }
                
                InFile >> vertexid.z;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v2\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.z;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t2\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester;
                    if (InFile.peek() != ' ') InFile >> normalid.z;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n2\n"); break;}
                }
                
                InFile >> vertexid.w;
                if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at v3\n"); break;}
                if (InFile.peek() == '/') {
                    // read texture index
                    InFile >> tester;
                    if (InFile.peek() != '/') {
                        InFile >> textureid.w;
                        if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at t3\n"); break;}
                    }
                }
                if (InFile.peek() == '/') {
                    // read normal index
                    InFile >> tester;
                    if (InFile.peek() != ' ' && InFile.peek() != '\n') InFile >> normalid.w;
                    if (InFile.eof()) {bEnd = true; fprintf(stderr, "end at n3\n"); break;}
                }
                
                quads.push_back(point4d<int>(vertexid.x-1, vertexid.y-1, vertexid.z-1, vertexid.w-1));
                
                break;
            default:
                InFile.getline(line, 2000);	//TODO this is ugly I know
                break;
        }
    }
    
    InFile.close();
    return true;
    
}


//****************************************************************************************************


template <class T, class U>
bool writeOBJFile(const std::vector< T >& verts, const std::vector<U>& tris, const char* filename) {
    
    std::ofstream OutFile(filename);
    // Fehler abfangen
    if (OutFile.fail()) {
        std::cerr << "Fehler beim oeffnen der Datei!" << std::endl;
        std::cerr << "Could not write to [" << filename << "]" << std::endl;
        return false;
    }
    
    OutFile << "g mesh" << std::endl;
    
    int numV = (int)verts.size();
    int numT = (int)tris.size();
    
    
    // write points
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "v " << verts[i1].x <<  " " << verts[i1].y <<  " " <<  verts[i1].z <<  std::endl;
    }
    
    
    // write triangles
    for(int i1 = 0; i1 < numT; i1++) {
        // std::cerr << "Writing Triangle " << i1 << " of " << numT << std::endl;
        OutFile <<  "f " << tris[i1].x+1 << " " << tris[i1].y+1 << " " << tris[i1].z+1 << std::endl;
    }
    
    OutFile.close();
    return true;
}

template <class T, class U>
bool writeOBJFile2D(const std::vector< T >& verts, const std::vector<U>& tris, const char* filename) {
    
    std::ofstream OutFile(filename);
    // Fehler abfangen
    if (OutFile.fail()) {
        std::cerr << "Fehler beim oeffnen der Datei!" << std::endl;
        std::cerr << "Could not write to [" << filename << "]" << std::endl;
        return false;
    }
    
    OutFile << "g mesh" << std::endl;
    
    int numV = (int)verts.size();
    int numT = (int)tris.size();
    
    
    // write points
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "v " << verts[i1].x <<  " " << verts[i1].y <<  " " <<  0.0 <<  std::endl;
    }
    
    
    // write triangles
    for(int i1 = 0; i1 < numT; i1++) {
        // std::cerr << "Writing Triangle " << i1 << " of " << numT << std::endl;
        OutFile <<  "f " << tris[i1].x+1 << " " << tris[i1].y+1 << " " << tris[i1].z+1 << std::endl;
    }
    
    OutFile.close();
    return true;
}


//****************************************************************************************************


template <class T, class U, class V>
bool writeOBJFileWithTris(const std::vector< T >& verts, const std::vector< V >& tex, const std::vector<U>& tris, const char* filename) {
    
    std::ofstream OutFile(filename);
    // Fehler abfangen
    if (OutFile.fail()) {
        std::cerr << "Fehler beim oeffnen der Datei!" << std::endl;
        std::cerr << "Could not write to [" << filename << "]" << std::endl;
        return false;
    }
    
    OutFile << "g mesh" << std::endl;
    
    int numV = (int)verts.size();
    int numT = (int)tris.size();
    
    
    // write points
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "v " << verts[i1].x <<  " " << verts[i1].y <<  " " <<  verts[i1].z <<  std::endl;
    }
    
    // write tex
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "vt " << tex[i1].x <<  " " << tex[i1].y << std::endl;
    }
    
    // write triangles
    for(int i1 = 0; i1 < numT; i1++) {
        // std::cerr << "Writing Triangle " << i1 << " of " << numT << std::endl;
        OutFile <<  "f " << tris[i1].x+1 << "/" << tris[i1].x+1 << " " << tris[i1].y+1 << "/" << tris[i1].y+1 << " " << tris[i1].z+1 << "/" << tris[i1].z+1 << std::endl;
    }
    
    OutFile.close();
    return true;
}

template <class T, class U, class V>
bool writeOBJFileWithTrisTexNormals(const std::vector< T >& verts, const std::vector< V >& tex, const std::vector<T>& normals, const std::vector<U>& tris, const char* filename) {
    
    std::ofstream OutFile(filename);
    // Fehler abfangen
    if (OutFile.fail()) {
        std::cerr << "Fehler beim oeffnen der Datei!" << std::endl;
        std::cerr << "Could not write to [" << filename << "]" << std::endl;
        return false;
    }
    
    OutFile << "g mesh" << std::endl;
    
    int numV = (int)verts.size();
    int numT = (int)tris.size();
    
    
    // write points
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "v " << verts[i1].x <<  " " << verts[i1].y <<  " " <<  verts[i1].z <<  std::endl;
    }
    
    // write tex
    for(int i1 = 0; i1 < numV; i1++) {
        if (i1 < tex.size()) {
            OutFile << "vt " << tex[i1].x <<  " " << tex[i1].y << std::endl;
        }
    }
    
    // write normals
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "vn " << normals[i1].x <<  " " << normals[i1].y <<  " " <<  normals[i1].z <<  std::endl;
    }
    
    // write triangles
    for(int i1 = 0; i1 < numT; i1++) {
        // std::cerr << "Writing Triangle " << i1 << " of " << numT << std::endl;
        OutFile <<  "f " << tris[i1].x+1 << "/" << tris[i1].x+1 << "/" << tris[i1].x+1 << " " << tris[i1].y+1 << "/" << tris[i1].y+1 << "/" << tris[i1].y+1 << " " << tris[i1].z+1 << "/" << tris[i1].z+1 << "/" << tris[i1].z+1 << std::endl;
    }
    
    OutFile.close();
    return true;
}

//****************************************************************************************************


template <class T, class U, class V>
bool writeOBJQuadFile(const std::vector<T>& verts, const std::vector< V >& tex, const std::vector<T>& normals, const std::vector<U>& quadVertices, const std::vector<U>& quadNormals, const std::vector<U>& quadTexcoords, const char* filename)
{
    std::ofstream OutFile(filename);
    // Fehler abfangen
    if (OutFile.fail()) {
        std::cerr << "Fehler beim oeffnen der Datei!" << std::endl;
        std::cerr << "Could not write to [" << filename << "]" << std::endl;
        return false;
    }
    
    OutFile << "g mesh" << std::endl;
    
    int numV = (int)verts.size();
    int numT = (int)quadVertices.size();
    
    
    // write points
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "v " << verts[i1].x <<  " " << verts[i1].y <<  " " <<  verts[i1].z <<  std::endl;
    }
    
    // write tex
    if (tex.size() >= numV) {
        for(int i1 = 0; i1 < numV; i1++) {
            OutFile << "vt " << tex[i1].x <<  " " << tex[i1].y << std::endl;
        }
    }
    
    // write normals
    if (normals.size() >= numV) {
        for(int i1 = 0; i1 < numV; i1++) {
            OutFile << "vn " << normals[i1].x <<  " " << normals[i1].y << " " << normals[i1].z << std::endl;
        }
    }
    
    // write quads
    if (normals.size() >= numV && tex.size() >= numV) {
        for(int i1 = 0; i1 < numT; i1++) {
            OutFile <<  "f " << quadVertices[i1].x+1 << "/" << quadTexcoords[i1].x+1 << "/" << quadNormals[i1].x+1 <<  " " << quadVertices[i1].y+1 << "/" << quadTexcoords[i1].y+1 << "/" << quadNormals[i1].y+1 << " " << quadVertices[i1].z+1 << "/" << quadTexcoords[i1].z+1 << "/" << quadNormals[i1].z+1 << " " << quadVertices[i1].w+1 << "/" << quadTexcoords[i1].w+1 << "/" << quadNormals[i1].w+1 << std::endl;
        }
    } else if (normals.size() >= numV && !(tex.size() >= numV)) {
        for(int i1 = 0; i1 < numT; i1++) {
            OutFile <<  "f " << quadVertices[i1].x+1 << "/" << "/" << quadNormals[i1].x+1 <<  " " << quadVertices[i1].y+1 << "/" << "/" << quadNormals[i1].y+1 << " " << quadVertices[i1].z+1 << "/" << "/" << quadNormals[i1].z+1 << " " << quadVertices[i1].w+1 << "/" << "/" << quadNormals[i1].w+1 << std::endl;
        }
    } else if (!(normals.size() >= numV) && tex.size() >= numV) {
        for(int i1 = 0; i1 < numT; i1++) {
            OutFile <<  "f " << quadVertices[i1].x+1 << "/" << quadTexcoords[i1].x+1 <<  " " << quadVertices[i1].y+1 << "/" << quadTexcoords[i1].y+1 << " " << quadVertices[i1].z+1 << "/" << quadTexcoords[i1].z+1 << " " << quadVertices[i1].w+1 << "/" << quadTexcoords[i1].w+1 << std::endl;
        }
    }
    
    OutFile.close();
    return true;
}


template <class T, class U>
bool writeOBJQuadFileWithTex(const std::vector< T >& verts, const std::vector< point4d< U > >& quads, const std::vector< vec2d >& tex, const char* filename) {
    
    std::ofstream OutFile(filename);
    // Fehler abfangen
    if (OutFile.fail()) {
        std::cerr << "Fehler beim Öffnen der Datei!" << std::endl;
        std::cerr << "Could not write to [" << filename << "]" << std::endl;
        return false;
    }
    
    OutFile << "g mesh" << std::endl;
    
    int numV = (int)verts.size();
    int numT = (int)quads.size();
    
    
    // write points
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "v " << verts[i1].x <<  " " << verts[i1].y <<  " " <<  verts[i1].z <<  std::endl;
    }
    // write tex
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "vt " << tex[i1].x <<  " " << tex[i1].y << std::endl;
    }
    
    // write quads
    for(int i1 = 0; i1 < numT; i1++) {
        // std::cerr << "Writing Triangle " << i1 << " of " << numT << std::endl;
        OutFile <<  "f "	<< quads[i1].x+1 << "/" << quads[i1].x+1 << "/ "
        << quads[i1].y+1 << "/" << quads[i1].y+1 << "/ "
        << quads[i1].z+1 << "/" << quads[i1].z+1 << "/ "
        << quads[i1].w+1 << "/" << quads[i1].w+1 << "/" << std::endl;
    }
    
    OutFile.close();
    return true;
}

//****************************************************************************************************


template <class T, class U>
bool writeOBJQuadFileWithTexDummyNormals(const std::vector< T >& verts, const std::vector< point4d< U > >& quads, const std::vector< vec2d >& tex, const char* filename) {
    
    std::ofstream OutFile(filename);
    // Fehler abfangen
    if (OutFile.fail()) {
        std::cerr << "Fehler beim Öffnen der Datei!" << std::endl;
        std::cerr << "Could not write to [" << filename << "]" << std::endl;
        return false;
    }
    
    OutFile << "g mesh" << std::endl;
    
    int numV = (int)verts.size();
    int numT = (int)quads.size();
    
    
    // write points
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "v " << verts[i1].x <<  " " << verts[i1].y <<  " " <<  verts[i1].z <<  std::endl;
    }
    // write tex
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "vt " << tex[i1].x <<  " " << tex[i1].y << std::endl;
    }
    // write tex
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "vn 1 0 0" << std::endl;
    }
    
    // write quads
    for(int i1 = 0; i1 < numT; i1++) {
        OutFile <<  "f "	<< quads[i1].x+1 << "/" << quads[i1].x+1 << "/" << quads[i1].x+1 << " "
        << quads[i1].y+1 << "/" << quads[i1].y+1 << "/" << quads[i1].y+1 << " "
        << quads[i1].z+1 << "/" << quads[i1].z+1 << "/" << quads[i1].z+1 << " "
        << quads[i1].w+1 << "/" << quads[i1].w+1 << "/" << quads[i1].w+1 << std::endl;
    }
    
    OutFile.close();
    return true;
}



//****************************************************************************************************


template <class T, class U>
bool writeOBJQuadFileWithoutTex(const std::vector<T>& verts, const std::vector<U>& quads, const char* filename) {
    
    std::ofstream OutFile(filename);
    // Fehler abfangen
    if (OutFile.fail()) {
        std::cerr << "Fehler beim oeffnen der Datei!" << std::endl;
        std::cerr << "Could not write to [" << filename << "]" << std::endl;
        return false;
    }
    
    OutFile << "g mesh" << std::endl;
    
    int numV = (int)verts.size();
    int numT = (int)quads.size();
    
    
    // write points
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "v " << verts[i1].x <<  " " << verts[i1].y <<  " " <<  verts[i1].z <<  std::endl;
    }
    
    
    // write quads
    for(int i1 = 0; i1 < numT; i1++) {
        // std::cerr << "Writing Triangle " << i1 << " of " << numT << std::endl;
        OutFile <<  "f " << quads[i1].x+1 << " " << quads[i1].y+1 << " " << quads[i1].z+1 << " " << quads[i1].w+1 << std::endl;
    }
    
    OutFile.close();
    return true;
}


//****************************************************************************************************


template <class T>
bool writeOBJFacesAsStringWithoutTex(const std::vector< T >& verts, const std::vector< std::string >& faces, const char* filename) {
    
    std::ofstream OutFile(filename);
    // Fehler abfangen
    if (OutFile.fail()) {
        std::cerr << "Fehler beim oeffnen der Datei!" << std::endl;
        std::cerr << "Could not write to [" << filename << "]" << std::endl;
        return false;
    }
    
    OutFile << "g mesh" << std::endl;
    
    int numV = (int)verts.size();
    int numT = (int)faces.size();
    
    
    // write points
    for(int i1 = 0; i1 < numV; i1++) {
        OutFile << "v " << verts[i1].x <<  " " << verts[i1].y <<  " " <<  verts[i1].z <<  std::endl;
    }
    
    
    // write quads
    for(int i1 = 0; i1 < numT; i1++) {
        // std::cerr << "Writing Triangle " << i1 << " of " << numT << std::endl;
        OutFile <<  "f " << faces[i1] << std::endl;
    }
    
    OutFile.close();
    return true;
}


#endif
