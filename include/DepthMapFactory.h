#ifndef DEPTH_MAP_FACTORY_H
#define DEPTH_MAP_FACTORY_H

class RGBImageSDL;
class SoSeparator;

#include "Polyline.h"
#include "JBS_General.h"
#include "pointNd.h"
#include "MathMatrix.h"
#include "point3d.h"
#include "Line.h"



struct S_Sensordimensions {

	//! Guess what, it's a constuctor.
	S_Sensordimensions(float width, float height) {
		w = width;
		h = height;
		ratio = w/h;
	}

	//! width of the sensor in mm.
	float w;

	//! height of the sensor in mm.
	float h;

	//! ratio 'w/h' will be computed automatically.
	float ratio;
};

struct S_CameraSetup {

	//! Constructor.
	S_CameraSetup(vec3f second_camera_pos_, float focal_length_, float distance_to_fp_) {
		second_camera_pos = second_camera_pos_;
		focal_length = focal_length_;
		distance_to_fp = distance_to_fp_;
	}

	//! The position of the second camera (In meters, relative to the first camera).
	vec3f second_camera_pos;

	//! The focal length used for <b> both shots (in mm).
	float focal_length;

	//! Initial distance to the focal point for camera 1 (in meters).
	float distance_to_fp;
};


static S_Sensordimensions NikonD70(23.7f, 15.6f);
static S_Sensordimensions Canon400D(22.2f, 14.8f);
static S_Sensordimensions FullFormat(22.2f, 14.8f);

//! skips the first/last BOUNDARY_SKIP rows/columns since the computation of a feature vector doesn't make too much sense there. 
static const int BOUNDARY_SKIP = 4;

//! Search range for neighborhood comparison, -OFFSET - OFFSET
static const int OFFSET = 1;

//! Number of features in the initial feature Vector.
static const int FEATURE_VECTOR_SIZE = (2*OFFSET+1)*(2*OFFSET+1)*3;

//! Number of features used for neighbor search.
static const int REDUCED_FEATURE_VECTOR_SIZE = 7;

typedef pointNd<float,FEATURE_VECTOR_SIZE> featureVec;
typedef pointNd<float,REDUCED_FEATURE_VECTOR_SIZE> reducedFeatureVec;



class DepthMapFactory {

public:

	//! Constructor.
	DepthMapFactory(char* filename1, char* filename2);

	//! Destructor.
	~DepthMapFactory();

	void setCameraInformations(S_Sensordimensions* sensor_info, S_CameraSetup* cam_setup);

	//! Computes the Feature vectors for all pixels in 'img2', and reduces them by SVD, does then compute the features for all pixels in 'img2' as well.
	void init();

	void computeDepthForAllPixels();

	//! Returns an inventor node containing the current scene.
	SoSeparator* getIV_scene();

private:


	//! Computes the epipolar-line in 'img2' for the pixel 'pix_pos' in 'img1'.
	inline Line getProjectedLine(vec2ui pix_pos);

	//! Returns the position of the best fitting pixel in 'img2'.
	inline vec2ui getBestFitPixel(Line l, vec2ui pix_pos);

	//! Computes the depth for the pair of pixels.
	inline float getDepthForPixelPair(vec2ui pox_pos_img1, vec2ui pix_pos_img2);

	//! Creates a coin node for the frustum at 'pos'.
	PolyLine<vec3f> frustumToPolyine(vec3f pos, vec3f lookat);

	//! Computes the n-dimensional feature vector of the point (x,y).
	featureVec getFeatureVector(UInt x, UInt y);

	//! Source image (= Image for which we compute the depth map).
	RGBImageSDL* img1;

	//! Target image.
	RGBImageSDL* img2;

	//! Information about the camera that was used for shooting the images.
	S_Sensordimensions* m_sensor_info;
	
	//! Camera setup struct (position, focal length, distance to focal point).
	S_CameraSetup* m_cam_setup;

	//! Matrix used for dimensionality reduction.
	DoRo::Matrix<float>* dim_reduce;

};

#endif
