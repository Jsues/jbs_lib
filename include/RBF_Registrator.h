#ifndef RBF_REGISTRATOR_H
#define RBF_REGISTRATOR_H

#include "rigid_transform.h"
#include "ScalarFunction.h"
#include "PointCloud.h"
#include "point6d.h"

template <class T>
class RBF_Registrator {
public:
	typedef T Point;
	typedef typename Point::ValueType ValueType;

	RBF_Registrator(PointCloudNormals<Point>* pc, RigidTransform<Point> trans, ScalarFunctionWithConfidence<Point>* distFunc) : m_pc(pc), m_trans(trans), m_distFunc(distFunc) {
	}

	PointCloudNormals<Point>* getPointCloud() {
		PointCloudNormals<Point>* pc = new PointCloudNormals<Point>;
		for (UInt i1 = 0; i1 < (UInt)m_pc->getNumPts(); i1++) {
			pc->insertPoint(m_trans.vecTrans(m_pc->getPoints()[i1]), m_trans.normTrans(m_pc->getNormals()[i1]));
		}
		return pc;
	}

	void step() {

		point6d<ValueType> v;
		SquareMatrixND< point6d<ValueType> > A;
		point6d<ValueType> b;


		for (UInt i1 = 0; i1 < (UInt)m_pc->getNumPts(); i1++) {

			Point p = m_trans.vecTrans(m_pc->getPoints()[i1]);
			Point grad;
			ValueType dist = m_distFunc->evalValAndGrad(p, grad);

			Point n = grad; n.normalize();

			ValueType weight = 1;//m_distFunc->evalConfidence(p + grad*dist);

			vec3f p_x_n = p^n;
			v[0] = n[0]; v[1] = n[1]; v[2] = n[2]; 
			v[3] = p_x_n[0]; v[4] = p_x_n[1]; v[5] = p_x_n[2];

			SquareMatrixND< point6d<ValueType> > A_tmp(v);
			b += v*dist*weight;
			A_tmp *= weight;
			A += A_tmp;
		}
		// Solve system of linear equations
		SquareMatrixND< point6d<ValueType> > U_m, V_m;
		point6d<ValueType> sigma, x_result;
		A.SVD_decomp(U_m, sigma, V_m);
		A.SVD_bksolve(U_m, sigma, V_m, b, x_result);

		// assign solution to translation(c_dash) and rotation(c)
		Point c_dash(x_result[0], x_result[1], x_result[2]);
		Point c(x_result[3], x_result[4], x_result[5]);

		// rotation angle
		ValueType a = -c.length();
		if (c.length() > 0)	c.normalize();
		//c_dash = vec3f(0.0);

		// build alligning transformation
		SquareMatrixND<Point> rot;

		rot.setToRotationMatrix(c, a);

		RigidTransform<Point> aligning_transformation(-c_dash, rot);

		// set alligning transformation
		m_trans.appendRigidTransformation(aligning_transformation);

		std::cerr << "-----------------------------------------" << std::endl;
		std::cerr << "Rotation angle: " << a << std::endl;
		std::cerr << "Rotation axis:  " << c << std::endl;
		std::cerr << "Translation:    " << c_dash << std::endl;
		std::cerr << "-----------------------------------------" << std::endl;
	}


private:

	PointCloudNormals<Point>* m_pc;
	RigidTransform<Point> m_trans;
	ScalarFunctionWithConfidence<Point>* m_distFunc;

};

#endif
