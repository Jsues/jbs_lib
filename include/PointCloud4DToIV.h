#ifndef POINTCLOUD_4D_TO_IV_H
#define POINTCLOUD_4D_TO_IV_H

#include "PointCloud4D.h"
#include "PointCloud.h"
#include "PointCloudToIV.h"

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoIndexedLineSet.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoSelection.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoNormal.h>
#include <Inventor/nodes/SoTransparencyType.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/SbColor.h>

//****************************************************************************************************


template <class T>
SoSeparator* PointCloud4DToIV_step_showNormals(PointCloud4D<T>* pc, UInt step) {


	SoSeparator* iv_pc = new SoSeparator;

	if (step >= pc->getNumTimeSteps()) {
		std::cerr << "PointCloud4DToIV_step(...), timestep out of range" << std::endl;
		return iv_pc;
	}

	UInt startI, endI;
	pc->getNumRangeOfPointSteps(step, startI, endI);
	int numOfPoints = endI-startI;

	/*
	point4d<T>* pts = pc->getPoints();
	SbVec3f* points = new SbVec3f[numOfPoints];
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		int offset = i1+startI;
		points[i1][0] = (float)pts[offset].x;
		points[i1][1] = (float)pts[offset].y;
		points[i1][2] = (float)pts[offset].z;
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numOfPoints, points);
	iv_pc->addChild(coord3);

	SoNormal* normals = new SoNormal;
	point4d<T>* normals_p4 = pc->getNormals();
	normals->vector.setNum(numOfPoints);

	bool reverseNormals = false;
	if (!reverseNormals) {
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			int offset = i1+startI;
			normals->vector.set1Value(i1, SbVec3f(normals_p4[offset][0], normals_p4[offset][1], normals_p4[offset][2]));
		}
	} else {
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			int offset = i1+startI;
			normals->vector.set1Value(i1, SbVec3f(-normals_p4[offset][0], -normals_p4[offset][1], -normals_p4[offset][2]));
		}
	}
	iv_pc->addChild(normals);

	SoDrawStyle* ds = new SoDrawStyle;
	ds->pointSize = 5;
	iv_pc->addChild(ds);

	SoPointSet* pointset = new SoPointSet;
	iv_pc->addChild(pointset);
	delete[] points;

	*/

	PointCloudNormals<vec3f> pcn;
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		vec4f p4 = pc->getPoints()[i1+startI];
		vec4f n4 = pc->getNormals()[i1+startI];

		pcn.insertPoint(vec3f(p4.x, p4.y, p4.z), vec3f(n4.x, n4.y, n4.z));
	}

	iv_pc->addChild(pointCloudNormalsToIVdisplayNormals(&pcn));



	return iv_pc;

}

//****************************************************************************************************


template <class T>
SoSeparator* PointCloud4DToIV_step(PointCloud4D<T>* pc, UInt step, bool invertNormals = false) {

	SoSeparator* iv_pc = new SoSeparator;

	if (step >= pc->getNumTimeSteps()) {
		std::cerr << "PointCloud4DToIV_step(...), timestep out of range" << std::endl;
		return iv_pc;
	}

	UInt startI, endI;
	pc->getNumRangeOfPointSteps(step, startI, endI);
	int numOfPoints = endI-startI;

	point4d<T>* pts = pc->getPoints();
	SbVec3f* points = new SbVec3f[numOfPoints];
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		int offset = i1+startI;
		points[i1][0] = (float)pts[offset].x;
		points[i1][1] = (float)pts[offset].y;
		points[i1][2] = (float)pts[offset].z;
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numOfPoints, points);
	iv_pc->addChild(coord3);

	SoNormal* normals = new SoNormal;
	point4d<T>* normals_p4 = pc->getNormals();
	normals->vector.setNum(numOfPoints);

	bool reverseNormals = invertNormals;
	if (!reverseNormals) {
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			int offset = i1+startI;
			normals->vector.set1Value(i1, SbVec3f(normals_p4[offset][0], normals_p4[offset][1], normals_p4[offset][2]));
		}
	} else {
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			int offset = i1+startI;
			normals->vector.set1Value(i1, SbVec3f(-normals_p4[offset][0], -normals_p4[offset][1], -normals_p4[offset][2]));
		}
	}
	iv_pc->addChild(normals);

	SoDrawStyle* ds = new SoDrawStyle;
	ds->pointSize = 2;
	iv_pc->addChild(ds);

	SoPointSet* pointset = new SoPointSet;
	iv_pc->addChild(pointset);

	delete[] points;

	return iv_pc;

}


//****************************************************************************************************


template <class T>
SoSeparator* PointCloud4DToIV_allPts(PointCloud4D<T>* pc) {
	SoSeparator* iv_pc = new SoSeparator();
	int numOfPoints = pc->getNumPts();

	point4d<T>* pts = pc->getPoints();
	SbVec3f* points = new SbVec3f[numOfPoints];
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		points[i1][0] = (float)pts[i1].x;
		points[i1][1] = (float)pts[i1].y;
		points[i1][2] = (float)pts[i1].z;
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numOfPoints, points);
	iv_pc->addChild(coord3);

	SoNormal* normals = new SoNormal;
	point4d<T>* normals_p4 = pc->getNormals();
	normals->vector.setNum(numOfPoints);

	bool reverseNormals = false;
	if (!reverseNormals) {
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			normals->vector.set1Value(i1, SbVec3f(normals_p4[i1][0], normals_p4[i1][1], normals_p4[i1][2]));
		}
	} else {
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			normals->vector.set1Value(i1, SbVec3f(-normals_p4[i1][0], -normals_p4[i1][1], -normals_p4[i1][2]));
		}
	}
	iv_pc->addChild(normals);

	SoDrawStyle* ds = new SoDrawStyle;
	ds->pointSize = 5;
	iv_pc->addChild(ds);

	SoPointSet* pointset = new SoPointSet;
	iv_pc->addChild(pointset);

	delete[] points;

	return iv_pc;
}


//****************************************************************************************************


#endif
