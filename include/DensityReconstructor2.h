#ifndef DENSITY_RECONSTRUCTOR_2_H
#define DENSITY_RECONSTRUCTOR_2_H


#include "JBS_General.h"
#include "PointCloud.h"
#include "point3d.h"
#include "Matrix3D.h"
#include "Volume.h"
#include "TrivialMesh.h"
#include "classMarchingCubes.h"

// For kD-tree
#include "ANN/ANN.h"


//****************************************************************************************************************

class posLookUp {

public:
	posLookUp(vec3f min_, vec3f max_, int x_, int y_, int z_) {
		min = min_;
		max = max_;
		x = x_;
		y = y_;
		z = z_;
	}
	//! Returns the cartesian coordinates of node (i,j,k).
	inline vec3f pos(int i, int j, int k) const {
		vec3f coord(min[0] + (max[0]-min[0])*(float(i)/float(x-1)), (max[1]-min[1])*(float(j)/float(y-1)), (max[2]-min[2])*(float(k)/float(z-1)));
		return coord;
	};

private:
	vec3f min, max;
	int x,y,z;
};


struct returnHelper2 {
	float density;
	float value;
	vec3f gradient;
	vec3f e0;
	vec3f curvatures;
	vec3f h;

	returnHelper2(float d, float v, vec3f g, vec3f e, vec3f c, vec3f h_) {
		density = d;
		value = v;
		gradient = g;
		e0 = e;
		curvatures = c;
		h = h_;
	}
	returnHelper2() {
		//density = 0;
		//value = 0;
		//gradient = vec3f(0,0,0);
		//e0 = vec3f(0,0,0);
		//curvatures = vec3f(0,0,0);
		//h = vec3f(0,0,0);
	}

	void print() {
		std::cerr << "Density:    " << density << std::endl;
		std::cerr << "Value:      " << value << std::endl;
		std::cerr << "Gradient:   "; gradient.print();
		std::cerr << "ev0:        "; e0.print();
		std::cerr << "Curvatures: "; curvatures.print();

	}
};

//****************************************************************************************************************

inline bool extractRidgesInCell(Volume<float>* vol, Volume<float>* vol2, Volume<vec3f>* curveature0, Volume<float>* lambda_1, int x_, int y, int z, TrivialMesh<vec3f>* tm, posLookUp* posFinder, int MaxValleys = 6) {


	int x = x_%2;
	int x1 = (x+1)%2;

	// If 'f' is zero at one corner, ignore cell
	if (vol->get(x,y,z) == 0 || vol->get(x1,y,z) == 0 || vol->get(x1,y+1,z) == 0 || vol->get(x,y+1,z) == 0 || vol->get(x,y,z+1) == 0 || vol->get(x1,y,z+1) == 0 || vol->get(x1,y+1,z+1) == 0 || vol->get(x,y+1,z+1) == 0)
		return false;

	int numValley = 0;
	if (lambda_1->get(x1,y,z) > 0) numValley++;
	if (lambda_1->get(x,y+1,z) > 0) numValley++;
	if (lambda_1->get(x1,y+1,z) > 0) numValley++;
	if (lambda_1->get(x,y,z) > 0) numValley++;
	if (lambda_1->get(x1,y,z+1) > 0) numValley++;
	if (lambda_1->get(x,y+1,z+1) > 0) numValley++;
	if (lambda_1->get(x1,y+1,z+1) > 0) numValley++;
	if (lambda_1->get(x,y,z+1) > 0) numValley++;
	if (numValley > MaxValleys) return false;

	JBSlib::MarchingCubes<vec3f> mc;

	JBSlib::MC_Gridcell<vec3f>& cell = mc.grid;
	JBSlib::MC_Triangle* tris = mc.tris;
	mc.isolevel = 0;

	cell.p[0] = posFinder->pos(x_+1,y,z);
	cell.p[1] = posFinder->pos(x_,y,z);
	cell.p[2] = posFinder->pos(x_,y+1,z);
	cell.p[3] = posFinder->pos(x_+1,y+1,z);
	cell.p[4] = posFinder->pos(x_+1,y,z+1);
	cell.p[5] = posFinder->pos(x_,y,z+1);
	cell.p[6] = posFinder->pos(x_,y+1,z+1);
	cell.p[7] = posFinder->pos(x_+1,y+1,z+1);

	cell.val[0] = vol2->get(x,y,z);
	cell.val[1] = vol2->get(x1,y,z);
	cell.val[2] = vol2->get(x1,y+1,z);
	cell.val[3] = vol2->get(x,y+1,z);
	cell.val[4] = vol2->get(x,y,z+1);
	cell.val[5] = vol2->get(x1,y,z+1);
	cell.val[6] = vol2->get(x1,y+1,z+1);
	cell.val[7] = vol2->get(x,y+1,z+1);

	//*******************************************
	// Correct directional flips in Eigenvectors
	//*******************************************
	// correctly orient signs:
	if ((curveature0->get(x,y,z) | curveature0->get(x1,y,z)) < 0) {
		cell.val[1] = -cell.val[1];
	}
	if ((curveature0->get(x,y,z) | curveature0->get(x1,y+1,z)) < 0) {
		cell.val[2] = -cell.val[2];
	}
	if ((curveature0->get(x,y,z) | curveature0->get(x,y+1,z)) < 0) {
		cell.val[3] = -cell.val[3];
	}
	if ((curveature0->get(x,y,z) | curveature0->get(x,y,z+1)) < 0) {
		cell.val[4] = -cell.val[4];
	}
	if ((curveature0->get(x,y,z) | curveature0->get(x1,y,z+1)) < 0) {
		cell.val[5] = -cell.val[5];
	}
	if ((curveature0->get(x,y,z) | curveature0->get(x1,y+1,z+1)) < 0) {
		cell.val[6] = -cell.val[6];
	}
	if ((curveature0->get(x,y,z) | curveature0->get(x,y+1,z+1)) < 0) {
		cell.val[7] = -cell.val[7];
	}


	int numTris = 0;
	numTris = mc.Polygonise();

	if (numTris == 0)
		return false;

	//float variation = fabs(curveature0->get(x,y,z) | curveature0->get(x1,y,z))
	//	* fabs(curveature0->get(x,y,z) | curveature0->get(x,y+1,z))
	//	* fabs(curveature0->get(x1,y,z) | curveature0->get(x1,y+1,z))
	//	* fabs(curveature0->get(x,y+1,z) | curveature0->get(x1,y+1,z))
	//	* fabs(curveature0->get(x,y,z+1) | curveature0->get(x1,y,z+1))
	//	* fabs(curveature0->get(x,y,z+1) | curveature0->get(x,y+1,z+1))
	//	* fabs(curveature0->get(x1,y,z+1) | curveature0->get(x1,y+1,z+1))
	//	* fabs(curveature0->get(x,y+1,z+1) | curveature0->get(x1,y+1,z+1))
	//	* fabs(curveature0->get(x,y,z) | curveature0->get(x,y,z+1))
	//	* fabs(curveature0->get(x1,y,z) | curveature0->get(x1,y,z+1))
	//	* fabs(curveature0->get(x,y+1,z) | curveature0->get(x,y+1,z+1))
	//	* fabs(curveature0->get(x1,y+1,z) | curveature0->get(x1,y+1,z+1));

	//vec3f triColor = vec3f(1, variation, variation);

	//float dens = vol->get(x,y,z) + vol->get(x1,y,z) + vol->get(x1,y+1,z) + vol->get(x,y+1,z) + vol->get(x,y,z+1) + vol->get(x1,y,z+1) + vol->get(x1,y+1,z+1) + vol->get(x,y+1,z+1);

	for (int i1 = 0; i1 < numTris; i1++) {

		const vec3f& v0 = tris[i1].p[0];
		const vec3f& v1 = tris[i1].p[1];
		const vec3f& v2 = tris[i1].p[2];

		//if (	(v0.dist(v1) < 0.00000001)
		//	||	(v0.dist(v2) < 0.00000001)
		//	||	(v1.dist(v2) < 0.00000001)) {
		//		//v0.print();
		//		//v1.print();
		//		//v2.print();
		//		continue;
		//}

		tm->insertTriangle(v0, v1, v2);

		//sm->vList[ind_v0].color = triColor;
		//sm->vList[ind_v1].color = triColor;
		//sm->vList[ind_v2].color = triColor;

		//if (dens > largest_density) {
		//	largest_density = dens;
		//	largest = v_id0;
		//}
	}

	return (numTris > 0);
}


//****************************************************************************************************************

struct returnHelper {
	float density;
	float value;
	vec3f gradient;
	vec3f e0;
	vec3f curvatures;

	returnHelper(float d, float v, vec3f g, vec3f e, vec3f c) {
		density = d;
		value = v;
		gradient = g;
		e0 = e;
		curvatures = c;
	}
	returnHelper() {
		density = 0;
		value = 0;
		gradient = vec3f(0,0,0);
		e0 = vec3f(0,0,0);
		curvatures = vec3f(0,0,0);
	}

	void print() {
		std::cerr << "Density:    " << density << std::endl;
		std::cerr << "Value:      " << value << std::endl;
		std::cerr << "Gradient:   "; gradient.print();
		std::cerr << "ev0:        "; e0.print();
		std::cerr << "Curvatures: "; curvatures.print();

	}
};


class DensityReconstructor2
{
public:
	DensityReconstructor2(float sigma, float search_radius);
	~DensityReconstructor2(void);

	//! Sets the input Pointcloud and creates the kd-tree.
	void setPointCloud(PointCloud<vec3f>* pc);

	//! computes the 'numNeighbors' nearest neighbors for each point.
	/*!
		Does furthermore compute the covariance tensor of all neighborhoods.
	*/
	void computeNeighborTensorData(int numNeighbors = 100);

	//! ...
	returnHelper getTargetValueEllipticalAt(float x, float y, float z) const ;
	returnHelper2 getTargetValueEllipticalAtver2(float x, float y, float z);

	Matrix3D<float>* CVs;

	inline vec3f getPoint(int i) const {
		return vec3f((float)dataPts[i][0], (float)dataPts[i][1], (float)dataPts[i][2]);
	}

	inline int getNumPoints() const {
		return size_kDtree;
	}

	float inline getEXP(float x) const {
		return (float)exp(x);
	}

	float inline getEXPapprox(float x) const {
		float r = x/search_radius;
		std::cerr << r << std::endl;
		return 1-(r*r*r*r*(5-4*r));
	}

private:

	//! ANN kD-tree.
	ANNkd_tree* kdTree;

	//! Stores the vectors of the kD-tree.
	ANNpointArray dataPts;

	//! Pre-allocated data structure for nn-search.
	ANNidxArray nnIdx;
	//! Pre-allocated data structure for nn-search.
	ANNdistArray dists;

	//! Number of elements in the kD-tree.
	int size_kDtree;

	//! Point used to query Neighbors.
	ANNpoint queryPt;

	float search_radius;

	float scale;

	float scale_square;

	float search_radius_square;

	float sigma_square;
};


#endif
