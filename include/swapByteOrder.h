#ifndef SWAP_BYTE_ORDER
#define SWAP_BYTE_ORDER


template <class T>
T swapByteOrder(T in) {
	T ret;

	int numBytes = sizeof(T);

	//std::cerr << "numBytes: " << numBytes << std::endl;

	char* in_p = (char*)&in;
	char* out_p = (char*)&ret;

	for (int i1 = 0; i1 < numBytes; i1++) {
		out_p[i1] = in_p[numBytes-1-i1];
	}

	return ret;
}


#endif
