#ifndef HOPPE_IMPLICIT_FUNCTION_H
#define HOPPE_IMPLICIT_FUNCTION_H


#include "ScalarFunction.h"
#include "PointCloud.h"
#include "ANN/ANN.h"

#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif

template <class T>
class ICP_aligner;


template <class T>
class HoppeImplicitFit : public ScalarFunction<T> {

public:

	friend class ICP_aligner<T>;
	typedef T Point;
	static const UInt dim = Point::dim;

	HoppeImplicitFit(PointCloudNormals<Point>* pc) : m_pc(pc) {
		m_numPts = pc->getNumPts();
		m_queryPt = annAllocPt(dim);
		m_dataPts = annAllocPts(m_numPts, dim);
		for (UInt i1 = 0; i1 < m_numPts; i1++) {
			for (UInt x = 0; x < dim; x++) {
				m_dataPts[i1][x] = pc->getPoints()[i1][x];
			}
		}
		m_kdTree = new ANNkd_tree(m_dataPts, m_numPts, dim);

	}

	~HoppeImplicitFit() {
		delete m_kdTree;
		annDeallocPts(m_dataPts);
		annDeallocPt(m_queryPt);
		annClose();			// close ANN
	}

	UInt getID() const {
		return TYPE_HOPPE_IMPLICIT;
	}


	virtual ValueType eval(const Point& pt) const {

		Point g;
		return evalInterpolAndGrad(pt, g);

		int nearest = getNearestID(pt);
		const Point& other = m_pc->getPoints()[nearest];
		Point diff = other-pt;
		ValueType dist = diff|m_pc->getNormals()[nearest];
		return dist;
	}

	virtual ValueType evalValAndGrad(const Point& pt, Point& grad) const {
		int nearest = getNearestID(pt);
		const Point& other = m_pc->getPoints()[nearest];

		Point diff = other-pt;
		ValueType dist = diff|m_pc->getNormals()[nearest];
		grad = m_pc->getNormals()[nearest];
		grad.normalize();
		return dist;
	}

	virtual ValueType evalValAndGradAndDist(const Point& pt, Point& grad, ValueType& distClosest, ValueType& angle) const {
		int nearest = getNearestID(pt);
		const Point& other = m_pc->getPoints()[nearest];

		Point diff = other-pt;
		distClosest = diff.length();
		ValueType dist = diff|m_pc->getNormals()[nearest];
		grad = m_pc->getNormals()[nearest];
		grad.normalize();
		angle = fabs(diff.getNormalized()|grad);
		return dist;
	}

	int getNearestID(const Point& pt) const {

		int m_ids[1];
		double m_dists[1];

		for (UInt x = 0; x < dim; x++) {
			m_queryPt[x] = pt[x];
		}
		m_kdTree->annkSearch(m_queryPt, 1, m_ids, m_dists, 0);

		return m_ids[0];
	}

	UInt getNumInRadius(const Point& pt, ValueType dist) {
		for (UInt x = 0; x < dim; x++) {
			m_queryPt[x] = pt[x];
		}
		UInt numInRadius = m_kdTree->annkFRSearch(m_queryPt, dist, 0);
		return numInRadius;
	}

	ValueType getMedian6NNSpacing(UInt numOfProbes = 100) {

		std::vector<ValueType> med_dist_vec;

		int m_ids[6];
		double m_dists[6];

		for (UInt i1 = 0; i1 < m_numPts; i1 += m_numPts/numOfProbes) {
			for (UInt x = 0; x < dim; x++) {
				m_queryPt[x] = m_pc->getPoints()[i1][x];
			}
			m_kdTree->annkSearch(m_queryPt, 6, m_ids, m_dists, 0);
			for (UInt i2 = 1; i2 < 6; i2++) {
				med_dist_vec.push_back((ValueType)m_dists[i2]);
			}
		}
		std::sort(med_dist_vec.begin(), med_dist_vec.end()); 
		ValueType medDistSquare = (ValueType)med_dist_vec[med_dist_vec.size()/2];

		return medDistSquare;
	}


	void getNearestNeighbors(const Point& pt, int num, int* ids, double* dists) const {
		for (UInt x = 0; x < dim; x++) {
			m_queryPt[x] = pt[x];
		}
		m_kdTree->annkSearch(m_queryPt, num, ids, dists, 0);
	}

	Point getNearestPoint(const Point& pt) const {
		int nearest = getNearestID(pt);
		const Point& other = m_pc->getPoints()[nearest];
		return other;
	}

private:

	ValueType evalInterpolAndGrad(const Point& pt, Point& grad) const {

		int m_ids[6];
		double m_dists[6];

		for (UInt x = 0; x < dim; x++) {
			m_queryPt[x] = pt[x];
		}
		m_kdTree->annkSearch(m_queryPt, 6, m_ids, m_dists, 0);

		ValueType total_dist = 0;

		for (UInt i1 = 0; i1 < 6; i1++) {
			const int& nearest = m_ids[i1];
			const Point& other = m_pc->getPoints()[nearest];
			Point diff = other-pt;
			ValueType dist = diff|m_pc->getNormals()[nearest];
			total_dist += dist;
			diff.normalize();
			grad += diff;
		}
		grad.normalize();
		total_dist /= 6;
		return total_dist;
	}

	UInt m_numPts;
	ANNkd_tree* m_kdTree;
	ANNpointArray m_dataPts;
	ANNpoint m_queryPt;

public:
	PointCloudNormals<Point>* m_pc;

};

#endif
