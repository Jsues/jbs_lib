#ifndef JBS_PATCH_TO_MESH_H
#define JBS_PATCH_TO_MESH_H


#include "JBS_General.h"
#include "BSplinePatch.h"
#include "SimpleMesh.h"
#include "Matrix2D.h"


//******************************************************************************************


//! This function converts a BSplinePatch to a SimpleMesh by uniformly sampling the patch.
template<class T>
SimpleMesh* BSplinePatch2SimpleMesh(BSplinePatch<T>* patch, UInt num_u, UInt num_v) {
	SimpleMesh* mesh = new SimpleMesh();

	vec2f range_u = patch->getParamRangeU();
	vec2f range_v = patch->getParamRangeV();

	range_u.print();
	range_v.print();


	// Sample points
	for (UInt i1 = 0; i1 < num_u; i1++) {
		float u = range_u[0] + (float(i1) / float(num_u-1))*(range_u[1]-range_u[0]);
		for (UInt i2 = 0; i2 < num_v; i2++) {
			float v = range_v[0] + (float(i2) / float(num_v-1))*(range_v[1]-range_v[0]);
			//std::cerr << u << " " << v << std::endl;
			T point = patch->eval(u,v);
			mesh->insertVertex(point[0], point[1], point[2]);
		}
	}

	// Insert Triangles
	for (UInt i1 = 0; i1 < num_u-1; i1++) {
		for (UInt i2 = 0; i2 < num_v-1; i2++) {
			UInt v0 = (i1+0)*num_v+(i2+0);
			UInt v1 = (i1+1)*num_v+(i2+0);
			UInt v2 = (i1+0)*num_v+(i2+1);
			UInt v3 = (i1+1)*num_v+(i2+1);
			mesh->insertTriangle(v0, v1, v2);
			mesh->insertTriangle(v3, v2, v1);
		}
	}
	return mesh;
}


//******************************************************************************************


//! This function converts a BSplinePatch to a SimpleMesh by uniformly sampling the patch.
template<class T>
SimpleMesh* BSplinePatch2SimpleMeshColorByCurvature(BSplinePatch<T>* patch, UInt num_u, UInt num_v) {
	SimpleMesh* mesh = new SimpleMesh();

	vec2f range_u = patch->getParamRangeU();
	vec2f range_v = patch->getParamRangeV();

	BSplinePatch<T>* Fu = patch->getDerivative_u();
	BSplinePatch<T>* Fv = patch->getDerivative_v();
	BSplinePatch<T>* Fuu = Fu->getDerivative_u();
	BSplinePatch<T>* Fuv = Fu->getDerivative_v();
	BSplinePatch<T>* Fvv = Fv->getDerivative_v();

	patch->print();
	Fu->print();
	Fv->print();
	Fuu->print();
	Fvv->print();
	Fuv->print();


	// Sample points
	std::cerr << "Evaluating Surface...";
	for (UInt i1 = 0; i1 < num_u; i1++) {
		float u = range_u[0] + (float(i1) / float(num_u-1))*(range_u[1]-range_u[0]);
		for (UInt i2 = 0; i2 < num_v; i2++) {
			float v = range_v[0] + (float(i2) / float(num_v-1))*(range_v[1]-range_v[0]);

			T point = patch->eval(u,v);

			T x_u = Fu->eval(u,v);
			T x_v = Fv->eval(u,v);

			//x_u.print();
			//x_v.print();

			float E = x_u|x_u;
			float F = x_u|x_v;
			float G = x_v|x_v;

			T Normal = x_u^x_v;
			Normal.normalize();
			T x_uu = Fuu->eval(u,v);
			T x_uv = Fuv->eval(u,v);
			T x_vv = Fvv->eval(u,v);

			float det_I_ff = sqrt(E*G-F*F);

			float L = (Normal|x_uu)/det_I_ff;
			float M = (Normal|x_uv)/det_I_ff;
			float N = (Normal|x_vv)/det_I_ff;

			// E, F, G are the coefficients of the _first_ fundamental form
			// L, M, N are the coefficients of the _second_ fundamental form

			float mean  = (N*G + 2.0f*M*F + L*E)/((E*G-F*F)*2.0f);
			float gauss = (L*N-M*M)/(E*G-F*F);



			//std::cerr << "(" << i1 << ", " << i2 << ")" << std::endl;
			//point.print();

			//x_u.print();
			//x_v.print();
			//if (fabs(E*G-F*F) < 0.001) {
			//	std::cerr << "Metrik: " << (E*G-F*F) << std::endl;
			//	std::cerr << "(" << i1 << ", " << i2 << ")" << std::endl;
			//	x_u.print();
			//	x_v.print();
			//}

//			std::cerr << gauss << std::endl;

			mesh->insertVertex(point[0], point[1], point[2]);
			mesh->vList[mesh->vList.size()-1].param.x = gauss;
//			mesh->vList[mesh->vList.size()-1].param.x = mean;


			mesh->vList[mesh->vList.size()-1].color = vec3f(Normal.x, Normal.y, Normal.z);
		}
	}
	std::cerr << "done" << std::endl;


	// Insert Triangles
	for (UInt i1 = 0; i1 < num_u-1; i1++) {
		for (UInt i2 = 0; i2 < num_v-1; i2++) {
			UInt v0 = (i1+0)*num_v+(i2+0);
			UInt v1 = (i1+1)*num_v+(i2+0);
			UInt v2 = (i1+0)*num_v+(i2+1);
			UInt v3 = (i1+1)*num_v+(i2+1);
			mesh->insertTriangle(v0, v1, v2);
			mesh->insertTriangle(v3, v2, v1);
		}
	}
	return mesh;
}


//******************************************************************************************


#endif //JBS_PATCH_TO_MESH_H

