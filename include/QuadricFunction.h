#ifndef QUADRIC_FUNCTION_H
#define QUADRIC_FUNCTION_H


#include "point2d.h"
#include "point3d.h"
#include "point4d.h"
#include "GenericQuadric.h"

#include "MPU.h"

#define NUM_CLOSE_PTS_FOR_AUXILIARY 6

// ****************************************************************************************

template <class T> class MPUDFOcNode;
template <class T> class MPUDFOcTree;

// ****************************************************************************************


namespace JBSlib {


//! Number of points for generic quadrics/lowvariate quadrics (dimension-dependant).
const int nMIN[]={4 /* 1D */ ,7 /* 2D */ ,15 /* 3D */ ,25 /* 4D */ }; 


//! Base class for quadrics.
template <class T>
class QuadricFunction {

public:

// ****************************************************************************************

	typedef T Point;
	typedef typename T::ValueType ValueType;
	typedef MPUDFOcNode<Point> OcNode;
	typedef MPUDFOcTree<OcNode> OcTree;
	static const UInt dim = Point::dim;

// ****************************************************************************************

	//! Generates a new quadric that fits the points in the 'node_'
	QuadricFunction(OcNode* node_) : m_node(node_), m_tree(m_node->tree) {
		m_error = 0;
	}

// ****************************************************************************************

	inline ValueType getError() const {
		return m_error;
	}

// ****************************************************************************************

	virtual inline ValueType evaluate(const Point& pos) const = 0;

// ****************************************************************************************

	virtual inline Point evaluateGradient(const Point& pos) const = 0;

// ****************************************************************************************

	virtual inline SquareMatrixND<Point> evaluateHessian(const Point& pos) const = 0;

// ****************************************************************************************

	virtual inline void fitAndComputeError(UInt numPts, int* ptsIDs, ValueType* weights, ValueType w_min, const Point& normal) = 0;

// ****************************************************************************************

protected:

// ****************************************************************************************

	//! The node which's content is fitted.
	OcNode* m_node;

	//! The 'mother' octree of the cell.
	OcTree* m_tree;

	//! Error of quadric fit.
	ValueType m_error;

// ****************************************************************************************

};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

template <class T> class QuadricFactory;

template <class T>
class LowVariateQuadricFunction : public QuadricFunction<T> {

	friend class QuadricFactory<T>;

public:

// ****************************************************************************************

	LowVariateQuadricFunction(OcNode* node_) : QuadricFunction<T>(node_) {
		m_quad = NULL;
	}

// ****************************************************************************************

	~LowVariateQuadricFunction() {
		if (m_quad != NULL) delete m_quad;
	}

// ****************************************************************************************

	virtual inline ValueType evaluate(const Point& pos) const {
		return m_quad->eval(pos);
	}


// ****************************************************************************************

	virtual inline Point evaluateGradient(const Point& pos) const {
		return m_quad->evalGrad(pos);
	}

// ****************************************************************************************

	virtual inline SquareMatrixND<Point> evaluateHessian(const Point& pos) const {
		return m_quad->getHessian(pos);
	}

// ****************************************************************************************

	virtual inline void fitAndComputeError(UInt numPts, int* ptsIDs, ValueType* weights, ValueType w_min, const Point& normal) {

		std::vector<Point> pts;		pts.reserve(numPts);
		std::vector<ValueType> w;	w.reserve(numPts);
		for (UInt i1 = 0; i1 < numPts; i1++) {
			pts.push_back(m_tree->objects[ptsIDs[i1]]);
		}

		// Perform Covariance Analysis:
		// 1.) compute cog:
		Point cog;
		for (UInt i1 = 0; i1 < numPts; i1++) {
			cog += pts[i1];
		}
		cog /= (ValueType)numPts;

		SquareMatrixND<Point> m;
		for (UInt i1 = 0; i1 < numPts; i1++) {
			m.addFromTensorProduct_NO_SYMMETRIZE(pts[i1]-cog);
		}
		m.symmetrize();

		SquareMatrixND<Point> basis;
		Point eigenvalues = m.calcEValuesAndVectors(basis);
		basis.transpose(); // This basis may be used to tranform points into the local coordinate system.

		UInt max_entry = 0;
		//ValueType max_val = fabs(basis.getRowI(0)|normal);
		//for (UInt i1 = 1; i1 < dim; i1++) {
		//	ValueType cur_val = fabs(basis.getRowI(i1)|normal);
		//	if (cur_val > max_val) {
		//		max_entry = i1;
		//		max_val = cur_val;
		//	}
		//}
		//if (max_entry != dim-1) {
		//	basis.changeRows(max_entry, dim-1);
		//}

		ValueType sign_corrector = (basis.getRowI(dim-1)|normal);
		if (sign_corrector < 0) {
			basis.flipSignsInRowI(dim-1);
			//std::cerr << "FLIPPED" << std::endl;
		}


		//if ((basis.getRowI(dim-1)|normal) < 0.8) {
		//	std::cerr << "Warning, normal*tangent_normal = " << (basis.getRowI(dim-1)|normal) << std::endl;
		//	std::cerr << "maxentry was: " << max_entry << std::endl;
		//}

		m_quad = new QuadricLowVariate<Point>(pts, w, basis, cog, m_tree->memorySVD_lowvariate);

		// Compute error:
		for (UInt i1 = 0; i1 < numPts; i1++) {
			if (weights[i1] < w_min)
				break;
			m_error = std::max(m_error, m_quad->eval(pts[i1])/(m_quad->evalGrad(pts[i1])).length());
		}

	}

// ****************************************************************************************

//private:

	QuadricLowVariate<Point>* m_quad;
};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class GenericQuadricFunction : public QuadricFunction<T> {

public:

// ****************************************************************************************

	GenericQuadricFunction(OcNode* node_) : QuadricFunction<T>(node_) {
		m_quad = NULL;
	}

// ****************************************************************************************

	~GenericQuadricFunction() {
		if (m_quad != NULL) delete m_quad;
	}

// ****************************************************************************************

	virtual inline Point evaluateGradient(const Point& pos) const {
		return m_quad->evalGrad(pos);
	}

// ****************************************************************************************

	virtual inline SquareMatrixND<Point> evaluateHessian(const Point& pos) const {
		return m_quad->getHessian(pos);
	}

// ****************************************************************************************

	virtual inline ValueType evaluate(const Point& pos) const {
		return m_quad->eval(pos);
	}

// ****************************************************************************************

	virtual inline void fitAndComputeError(UInt numPts, int* ptsIDs, ValueType* weights, ValueType w_min, const Point& normal) {

		std::vector<Point> pts;		pts.reserve(numPts + m_node->numChildren);
		std::vector<ValueType> w;	w.reserve(numPts + m_node->numChildren);
		std::vector<ValueType> d;	d.reserve(numPts + m_node->numChildren);
		for (UInt i1 = 0; i1 < numPts; i1++) {
			pts.push_back(m_tree->objects[ptsIDs[i1]]);
			w.push_back(weights[i1]);
			d.push_back(0);
		}

		// Find auxiliary points
		bool is_a_good_guy; // All scalar products have the same sign.
		for (UInt i1 = 0; i1 < m_node->numChildren; i1++) {
			Point additionalPt = m_node->getCorner(i1);

			// get the six closest points in the pointset:
			m_tree->doNNSearch(additionalPt, NUM_CLOSE_PTS_FOR_AUXILIARY);

			is_a_good_guy = true;
			ValueType sp = (additionalPt-m_tree->objects[m_tree->nnIdx[0]])|m_tree->normals[m_tree->nnIdx[0]];
			bool signum = (sp > 0);
			for (UInt i2 = 1; i2 < NUM_CLOSE_PTS_FOR_AUXILIARY; i2++) {
				ValueType sp_i = (additionalPt-m_tree->objects[m_tree->nnIdx[i2]])|m_tree->normals[m_tree->nnIdx[i2]];
				bool signum_i = (sp_i > 0);

				if (signum != signum_i) {
					is_a_good_guy = false;
					break;
				}
				sp += sp_i;
			}

			if (is_a_good_guy) { // Add auxiliary point
				sp /= NUM_CLOSE_PTS_FOR_AUXILIARY;
				pts.push_back(additionalPt);
				d.push_back(-sp);
			}
		}

		UInt numAuxiliaryPts = (UInt)pts.size() - numPts;
		ValueType ww = (ValueType)1/(ValueType)numAuxiliaryPts;
		for (UInt i1 = 0; i1 < numAuxiliaryPts; i1++) {
			w.push_back(ww);
		}

		m_quad = new GenericQuadric<Point>(pts, d, w, m_tree->memorySVD_generic);


		// Compute error:
		for (UInt i1 = 0; i1 < numPts; i1++) {
			m_error = std::max(m_error, m_quad->eval(pts[i1])/(m_quad->evalGrad(pts[i1])).length());
		}

	}

// ****************************************************************************************

private:

	GenericQuadric<Point>* m_quad;
};



// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class EdgeQuadricFunction : public QuadricFunction<T> {

public:

// ****************************************************************************************

	EdgeQuadricFunction(OcNode* node_) : QuadricFunction<T>(node_) {
		m_quad1 = NULL;
		m_quad2 = NULL;
	}

// ****************************************************************************************

	~EdgeQuadricFunction() {
		if (m_quad1 != NULL) delete m_quad1;
		if (m_quad2 != NULL) delete m_quad2;
	}

// ****************************************************************************************

	virtual inline Point evaluateGradient(const Point& pos) const {
		return (m_quad1->evaluate(pos) < m_quad2->evaluate(pos)) ? m_quad1->evaluateGradient(pos) : m_quad2->evaluateGradient(pos);
	}

// ****************************************************************************************

	virtual inline SquareMatrixND<Point> evaluateHessian(const Point& pos) const {
		return (m_quad1->evaluate(pos) < m_quad2->evaluate(pos)) ? m_quad1->evaluateHessian(pos) : m_quad2->evaluateHessian(pos);
	}

// ****************************************************************************************

	virtual inline ValueType evaluate(const Point& pos) const {
		return std::min(m_quad1->evaluate(pos), m_quad2->evaluate(pos));
	}

// ****************************************************************************************

	virtual inline void fitAndComputeError(UInt numPts, int* ptsIDs, ValueType* weights, ValueType w_min, const Point& normal) {
		std::cerr << "Do not use this, EXIT" << std::endl;
		exit(EXIT_FAILURE);
	}

// ****************************************************************************************

	inline void fitAndComputeError(int* ptsIDs, ValueType* weights, ValueType w_min, const Point& normal1, const Point& normal2, int* idxCluster1, int* idxCluster2, UInt numPts1, UInt numPts2) {

		std::cerr << "XXXXXXXXX" << std::endl;

		m_quad1 = new LowVariateQuadricFunction<Point>(m_node);
		m_quad2 = new LowVariateQuadricFunction<Point>(m_node);

		// Build first quadric:
		ValueType* weights1 = new ValueType[numPts1];
		for (UInt i1 = 0; i1 < numPts1; i1++) {
			weights1[i1] = weights[idxCluster1[i1]];
            idxCluster1[i1] = ptsIDs[idxCluster1[i1]];
		}
		m_quad1->fitAndComputeError(numPts1, idxCluster1, weights1, w_min, normal1);
		delete[] weights1;

		// Build second quadric:
		ValueType* weights2 = new ValueType[numPts2];
		for (UInt i1 = 0; i1 < numPts2; i1++) {
			weights2[i1] = weights[idxCluster2[i1]];
            idxCluster2[i1] = ptsIDs[idxCluster2[i1]];
		}
		m_quad2->fitAndComputeError(numPts2, idxCluster2, weights2, w_min, normal2);
		delete[] weights2;

		// Compute error:
		for (UInt i1 = 0; i1 < numPts1+numPts2; i1++) {
			const Point& currentPt = m_tree->objects[ptsIDs[i1]];
			ValueType current_error = std::min(
				m_quad1->m_quad->eval(currentPt)/(m_quad1->m_quad->evalGrad(currentPt)).length(),
				m_quad2->m_quad->eval(currentPt)/(m_quad2->m_quad->evalGrad(currentPt)).length()
				);

			m_error = std::max(m_error, current_error);
		}
	}

// ****************************************************************************************

private:

	LowVariateQuadricFunction<Point>* m_quad1;
	LowVariateQuadricFunction<Point>* m_quad2;
};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class QuadricFactory {

public:

// ****************************************************************************************

	typedef T Point;
	typedef typename T::ValueType ValueType;
	typedef MPUDFOcNode<Point> OcNode;
	typedef MPUDFOcTree<OcNode> OcTree;
	const UInt numNmin;
	const UInt twiceNumNmin;

// ****************************************************************************************

	QuadricFactory(OcTree* tree_) : tree(tree_), numNmin(nMIN[Point::dim-1]), twiceNumNmin(nMIN[Point::dim-1]*2) {
		numQuadrics = 0;
	}

// ****************************************************************************************

	//! returns the number of clusters found.
	inline ValueType ClusterPointsEdge(int** clusters, UInt numPts, UInt& numPtsInCluster0, UInt& numPtsInCluster1, Point& v0, Point& v1) const {

		// Compute vectors the differ most:
		ValueType max_diff = 1;
		for (UInt i = 0; i < numPts; i++) {
			const Point& n_i = tree->normals[tree->nnIdx[i]];
			for (UInt j = i+1; j < numPts; j++) {
				const Point& n_j = tree->normals[tree->nnIdx[j]];
				
				if ((n_j|n_i) < max_diff) {
					max_diff = (n_j|n_i);
					v0 = n_i;
					v1 = n_j;
				}
			}
		}
		// Now we have found the two most distant vectors:
		if (max_diff > 0.9) // Use a generic quadric.
			return 1;

		ValueType error = 1000;

		int runs = 0;

		do {

			runs++;

			numPtsInCluster0 = 0;
			numPtsInCluster1 = 0;

			// Cluster and recompute average:
			Point new_v0;
			Point new_v1;
			for (UInt i1 = 0; i1 < numPts; i1++) {
				if ((tree->normals[tree->nnIdx[i1]]|v0) > (tree->normals[tree->nnIdx[i1]]|v1)) {
					clusters[0][numPtsInCluster0++] = i1;
					new_v0 += tree->normals[tree->nnIdx[i1]];
				} else {
					clusters[1][numPtsInCluster1++] = i1;
					new_v0 += tree->normals[tree->nnIdx[i1]];
				}
			}
			new_v0.normalize();
			new_v1.normalize();

			error = (1-(new_v0|v0)) + (1-(new_v1|v1));

			v0 = new_v0;
			v1 = new_v1;

		} while ((error > 0.2) && (runs < 10));

		return (v0|v1);

	}

// ****************************************************************************************

	inline QuadricFunction<Point>* getQuadricForCell(OcNode* node)  {
		QuadricFunction<Point>* qf;

		const Point& center = node->center;
		const ValueType& ball_radius = node->search_diag;

		// determine type of quadric:
		UInt numPoints_in_Cell_Ball = (UInt) tree->dokDRadiusSearch(center, ball_radius);

		node->num_points_in_ball = numPoints_in_Cell_Ball;

		ValueType newRadius = ball_radius;

		if (numPoints_in_Cell_Ball < numNmin) {
			// Blow up Ball for cell.
			newRadius = sqrt(tree->doNNSearch(center, numNmin));
			numPoints_in_Cell_Ball = numNmin;
		}

		ValueType* weights = new ValueType[numPoints_in_Cell_Ball];
		// **************************************************
		// Compute weighted average normal and weighted cog:
		// **************************************************
		Point weighted_normal;
		Point weighted_cog;
		ValueType sum_of_weights = 0;
		for (UInt i1 = 0; i1 < numPoints_in_Cell_Ball; i1++) {
			const ValueType t = (ValueType)1.5*(ValueType)sqrt(tree->dists[i1])/newRadius;
			weights[i1] = tree->getWeight(t);
			sum_of_weights += weights[i1];
			weighted_normal += tree->normals[tree->nnIdx[i1]]*weights[i1];
			weighted_cog += tree->objects[tree->nnIdx[i1]]*weights[i1];
		}
		weighted_normal.normalize();
		weighted_cog /= sum_of_weights;


//#ifdef DEEP_DEBUG
		//for (UInt i1 = 0; i1 < numPoints_in_Cell_Ball; i1++) {
		//	node->points_indices.push_back(tree->nnIdx[i1]);
		//}
//#endif


		if (numPoints_in_Cell_Ball > twiceNumNmin) { // We have more then 2*N_min Points in the Ball

			// Check if we should fit a generic Quadric (angle deviation > 90�) or a lowvariate Quadric
			bool UseGenericQuadric = false;
			//for (UInt i1 = 0; i1 < numPoints_in_Cell_Ball; i1++) {
			//	if (tree->normals[tree->nnIdx[i1]] | weighted_normal < 0) { // found deviation > 90�
			//		UseGenericQuadric = true;
			//		break;
			//	}
			//}
			//UseGenericQuadric = false;

			if (UseGenericQuadric) {
				qf = new GenericQuadricFunction<Point>(node);
				qf->fitAndComputeError(numPoints_in_Cell_Ball, tree->nnIdx, weights, 0, weighted_normal);
			} else { // Fit a lowvariate quadric
				qf = new LowVariateQuadricFunction<Point>(node);
				qf->fitAndComputeError(numPoints_in_Cell_Ball, tree->nnIdx, weights, 0, weighted_normal);
			}

		} else { // We have less then 2*N_min Points in the Ball

			Point normal_cluster0;
			Point normal_cluster1;
			int** clusters = new int*[2];
			clusters[0] = new int[numPoints_in_Cell_Ball];
			clusters[1] = new int[numPoints_in_Cell_Ball];
			UInt pts_in_cluster0, pts_in_cluster1;

			ValueType clusterDeriv = 1; //ClusterPointsEdge(clusters, numPoints_in_Cell_Ball, pts_in_cluster0, pts_in_cluster1, normal_cluster0, normal_cluster1);

			// The weight of the last point within the cells original radius.
			ValueType min_weight_for_cells_in_initial_radius = tree->getWeight((ValueType)1.5*node->search_diag/newRadius);

			if (clusterDeriv > 0.9) {	// fit a generic quadric.
				qf = new LowVariateQuadricFunction<Point>(node);
				qf->fitAndComputeError(numPoints_in_Cell_Ball, tree->nnIdx, weights, min_weight_for_cells_in_initial_radius, weighted_normal);
			} else {					// fit a quadric edge.

				//// Write files:
				//PointCloudNormals<Point> pc;
				//for (UInt i1 = 0; i1 < pts_in_cluster0; i1++) {
				//	pc.insertPoint(tree->objects[tree->nnIdx[clusters[0][i1]]], tree->normals[tree->nnIdx[clusters[0][i1]]]);
				//}
				//PointCloudWriter<Point>::writePWNFileDirect(&pc, "test.pwn");
				//PointCloudNormals<Point> pc1;
				//for (UInt i1 = 0; i1 < pts_in_cluster1; i1++) {
				//	pc1.insertPoint(tree->objects[tree->nnIdx[clusters[1][i1]]], tree->normals[tree->nnIdx[clusters[1][i1]]]);
				//}
				//PointCloudWriter<Point>::writePWNFileDirect(&pc1, "test1.pwn");
				////exit(1);

				EdgeQuadricFunction<Point>* eqf = new EdgeQuadricFunction<Point>(node);
				eqf->fitAndComputeError(tree->nnIdx, weights, min_weight_for_cells_in_initial_radius, normal_cluster0, normal_cluster1, clusters[0], clusters[1], pts_in_cluster0, pts_in_cluster1);
				qf = eqf;
			}

			// Cleanup:
			delete[] clusters[0];
			delete[] clusters[1];
			delete[] clusters;

		}

		delete[] weights;

		return qf;
	}

// ****************************************************************************************

	OcTree* tree;

	UInt numQuadrics;

};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


}


#endif
