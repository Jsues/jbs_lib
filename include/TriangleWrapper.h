#ifndef TRIANGLE_WRAPPER_H
#define TRIANGLE_WRAPPER_H

#include <iostream>
#include <fstream>

#include "PolyLine.h"
#include "operators/findPathInMesh.h"
#include "operators/getVertexFan.h"

template <class Point>
std::vector<std::pair<int, int>> computeConvexHullTriangle(const Point* const pts, UInt numPts) {

	std::ofstream fileout("pts.node");

	fileout << numPts << " 2 0 0" << std::endl;
	for (UInt i1 = 0; i1 < numPts; i1++) {
		fileout << i1 << " " << pts[i1].x << " " << pts[i1].y << std::endl;
	}
	fileout.close();


	int result = system("Triangle.exe -c pts.node");

	std::ifstream filein("pts.1.poly");
	int i1, i2, i3, i4;
	filein >> i1 >> i2 >> i3 >> i4;
	int numLines, numSegs;
	filein >> numLines >> numSegs;

	std::vector<std::pair<int, int>> lines;
	int id, v0, v1;
	for (int i1 = 0; i1 < numLines; i1++) {
		filein >> id >> v0 >> v1 >> i2;
		lines.push_back(std::pair<int, int>(v0, v1));
	}
	return lines;

}

void cleanup() {
	system("del pts.*");
	system("del shape.*");
	system("del shape2.*");
}

template <class Point>
SimpleMesh* constrainedDelaunay(PolyLine<Point>* pl, Point innerPoint, typename Point::ValueType minArea) {

	std::ofstream fileout("shape.poly");

	fileout << pl->getNumV() << " 2 0 0" << std::endl;

	for (UInt i1 = 0; i1 < pl->getNumV(); i1++) {
		fileout << i1 << "  " << pl->points[i1].c.x << " " << pl->points[i1].c.y <<  std::endl;
	}

	fileout << pl->getNumL() << " 0" << std::endl;

	int cnt = 0;
	for (std::list<LineSegment>::const_iterator it = pl->lines.begin(); it != pl->lines.end(); ++it) {
		fileout << cnt << "  " << it->v0 << " " << it->v1 <<  std::endl;
		cnt++;
	}
	// add final zero
	fileout << "1" << std::endl;
	fileout << "1 " << innerPoint.x << " " << innerPoint.y << std::endl;
	fileout.close();

	std::stringstream ss;
	ss << "Triangle.exe -pcq0LDga" << std::setprecision(10) << std::fixed << minArea << " shape";
	//ss << "Triangle.exe -pcq0LDga0.00001 shape";
	std::cerr << "Triangle call: " << ss.str() << std::endl;
	int result = system(ss.str().c_str());

	SimpleMesh* sm = MeshReader::readOFFFile("shape.1.off");

	for (UInt i1 = 0; i1 < (UInt)sm->getNumV(); i1++) sm->vList[i1].bool_flag = true;

	for (std::list<LineSegment>::const_iterator it = pl->lines.begin(); it != pl->lines.end(); ++it) {
		std::vector<unsigned int> path = Dijkstra(sm, it->v0, it->v1);
		for (UInt i1 = 1; i1 < path.size()-1; i1++) {
			int id = path[i1];

			std::vector<int> fan;
			getVertexFan(sm, id, fan);

			// Remove fan
			sm->vList[id].bool_flag = false;
			sm->removeVertex(id);
			for (UInt x = 1; x < fan.size()-2; x++) {
				sm->insertTriangle(fan[0], fan[x], fan[x+1]);
			}
		}
	}
	//sm->cleanValence0Vertices();
	MeshWriter::writeOFFFile("deleted_wrong.off", sm);
	return sm;


	//UInt before = pl->getNumV();
	//for (UInt i1 = before; i1 < (UInt)sm->getNumV(); i1++) {
	//	if (sm->vList[i1].bool_flag) {
	//		vec2d pt(sm->vList[i1].c.x, sm->vList[i1].c.y);
	//		pl->insertVertex(pt);
	//	}
	//}

	//std::ofstream fileout2("shape2.poly");

	//fileout2 << pl->getNumV() << " 2 0 0" << std::endl;

	//for (UInt i1 = 0; i1 < pl->getNumV(); i1++) {
	//	fileout2 << i1 << "  " << pl->points[i1].c.x << " " << pl->points[i1].c.y <<  std::endl;
	//}

	//fileout2 << pl->getNumL() << " 0" << std::endl;

	//cnt = 0;
	//for (std::list<LineSegment>::const_iterator it = pl->lines.begin(); it != pl->lines.end(); ++it) {
	//	fileout2 << cnt << "  " << it->v0 << " " << it->v1 <<  std::endl;
	//	cnt++;
	//}
	//// add final zero
	//fileout2 << "1" << std::endl;
	//fileout2 << "1 " << innerPoint.x << " " << innerPoint.y << std::endl;
	//fileout2.close();

	//result = system("Triangle.exe -pcg shape2");


	//SimpleMesh* sm2 = MeshReader::readOFFFile("shape.2.off");

	//delete sm;
	////cleanup();
	//return sm2;
}

#endif
