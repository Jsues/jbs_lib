#ifndef CURVE_TO_IV
#define CURVE_TO_IV

#pragma warning( disable : 4244 )

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include "JBS_General.h"

template <class T>
SoSeparator* curveToIV(T curve, UInt numSamplePoints=100) {

	SoSeparator* sep = new SoSeparator;

	// Build controlPoly
	SoSeparator* ctrlPoly = new SoSeparator;
	std::vector<T::ControlPoint> ctrlPts = curve.getCtrlPts();
	UInt numCtrlPts = (UInt)ctrlPts.size();

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.set1Value(0, 0, 0, 1);
	ctrlPoly->addChild(mat);

	std::vector<SbVec3f> linepoints;
	for (UInt i1 = 0; i1 < numCtrlPts; i1++) {
		linepoints.push_back(SbVec3f(ctrlPts[i1].x, ctrlPts[i1].y, (T::ControlPoint::dim > 2) ? ctrlPts[i1][2] : 0));
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, (int)linepoints.size(), &linepoints[0]);
	ctrlPoly->addChild(coord3);

	SoLineSet* ls = new SoLineSet;
	ls->numVertices.setNum(1);
	ls->numVertices.set1Value(0, numCtrlPts);
	ctrlPoly->addChild(ls);
	sep->addChild(ctrlPoly);

	// Sample curve
	SoSeparator* curvePoly = new SoSeparator;
	SoMaterial* mat2 = new SoMaterial;
	mat2->diffuseColor.set1Value(0, 1, 0, 0);
	curvePoly->addChild(mat2);

	std::vector<SbVec3f> curvepoints;
	vec2f range = curve.getRange();
	float diff = range[1]-range[0];

	for (UInt i1 = 0; i1 < numSamplePoints; i1++) {

		double u = ((double)i1/(double)(numSamplePoints-1))*diff + range[0];

		T::ControlPoint pt = curve.eval(u);

		curvepoints.push_back(SbVec3f(pt.x, pt.y, (T::ControlPoint::dim > 2) ? pt[2] : 0));
	}
	SoCoordinate3* coord3b = new SoCoordinate3;
	coord3b->point.setValues(0, (int)curvepoints.size(), &curvepoints[0]);
	curvePoly->addChild(coord3b);

	SoLineSet* ls2 = new SoLineSet;
	ls2->numVertices.setNum(1);
	ls2->numVertices.set1Value(0, numSamplePoints);
	curvePoly->addChild(ls2);
	sep->addChild(curvePoly);

	return sep;
}


#endif
