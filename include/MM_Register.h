#ifndef MM_Register_H
#define MM_Register_H

#define NO_GSL
#include "PCA_basis.h"
#include "levmar.h"


class MM_Register {


	struct edge {
		int v0, v1;
		double target_length;
	};

	struct LM_paramEdgeLength {
		LM_paramEdgeLength(const PCA_Basis& p) : pcabas(p) {}
		std::vector<double> params;
		std::vector<edge>* edgeStruct;
		std::vector<vec3d> avg_mesh;
		const PCA_Basis& pcabas;
		double weight_pca;
	};

	static void evalGoodnessOfFitEdgeLength(double *p, double *x, int m, int n, void *dvoid) {
	
		LM_paramEdgeLength* data = (LM_paramEdgeLength*)dvoid;

		std::vector<double> inpca(data->params);
		double w_pca = 1; //data->weight_pca;
		int numParam = (int)inpca.size();
		for (UInt i1 = 0; i1 < inpca.size(); i1++) {
			inpca[i1] = p[i1]*sqrt(data->pcabas.EVals[i1]);
		}
	
		// reconstruct model from current parameters
		std::vector<double> xx = data->pcabas.projectFromBasis(inpca);
		std::vector<vec3d> verts = data->avg_mesh;
		for (UInt i1 = 0; i1 < verts.size(); i1++) {
			verts[i1].x += xx[i1*3+0];
			verts[i1].y += xx[i1*3+1];
			verts[i1].z += xx[i1*3+2];
		}

		double sum = 0;
		for (int i1 = 0; i1 < (int)data->edgeStruct->size(); i1++) {
			const edge& e = (*data->edgeStruct)[i1];
			double d = verts[e.v0].dist(verts[e.v1]);
			x[i1] = d;
			double target = e.target_length;
			sum += (x[i1]-target)*(x[i1]-target);
		}

		double scapesum = 0;
		for (int i1 = 0; i1 < data->pcabas.numVecs; i1++) {
			int i = (int)data->edgeStruct->size()+i1;
			x[i] = p[i1]*data->weight_pca;
			scapesum += x[i]*x[i];
		}
		std::cerr << "Sum: " << sum+scapesum << " SCAPE: " << scapesum << " edge: " << sum << std::endl;
	}


	static void getEdgesStucts(std::vector<edge>& edges,std::vector<vec3d> verts,std::vector<vec3i> tris, const std::vector<double>& weights) {
		for (UInt i1 = 0; i1 < tris.size(); i1++) {
			const vec3i& t = tris[i1];

			if ((t.x < t.y) && (weights[t.x] > 0) && (weights[t.y] > 0)) {
				edge e;
				e.v0 = t.x;
				e.v1 = t.y;
				e.target_length = verts[t.x].dist(verts[t.y]);
				edges.push_back(e);
			}
			if ((t.y < t.z) && (weights[t.y] > 0) && (weights[t.z] > 0)) {
				edge e;
				e.v0 = t.y;
				e.v1 = t.z;
				e.target_length = verts[t.y].dist(verts[t.z]);
				edges.push_back(e);
			}
			if ((t.z > t.x) && (weights[t.z] > 0) && (weights[t.x] > 0)) {
				edge e;
				e.v0 = t.z;
				e.v1 = t.x;
				e.target_length = verts[t.z].dist(verts[t.x]);
				edges.push_back(e);
			}
		}
	}


public:

	static std::vector<vec3d> fitToData(const PCA_Basis& pcabas, const std::vector<vec3d>& verts, const std::vector<vec3i>& tris, const std::vector<vec3d> avg, const std::vector<double>& weights) {
	
		std::vector<edge> edges;
		getEdgesStucts(edges, verts, tris, weights);

		int numIter = 100;
		int numUnknown = pcabas.numVecs;
		LM_paramEdgeLength param(pcabas);
		param.weight_pca = 0.01;
		param.avg_mesh = avg;
		param.params.resize(numUnknown);
		param.edgeStruct = &edges;
		int numEqns = (int)edges.size()+pcabas.numVecs;
		std::vector<double> target(numEqns);
		for (int i1 = 0; i1 < numEqns; i1++) {
			target[i1] = edges[i1].target_length;
		}

		std::cerr << "Computing avatar...";
		double info[LM_INFO_SZ];
		int ret = dlevmar_dif(evalGoodnessOfFitEdgeLength, &param.params[0], &target[0], numUnknown, numEqns, numIter, NULL, info, NULL, NULL, &param);
		printf("Levenberg-Marquardt returned in %g iter, reason %g, sumsq %g [%g]\n", info[5], info[6], info[1], info[0]);
		std::cerr << "done" << std::endl;


		std::vector<double> inpca(param.params);
		int numParam = (int)inpca.size();
		for (UInt i1 = 0; i1 < inpca.size(); i1++) {
			inpca[i1] = param.params[i1]*sqrt(pcabas.EVals[i1]);
		}
	
		// reconstruct model from current parameters
		std::vector<double> xx = pcabas.projectFromBasis(inpca);
		std::vector<vec3d> verts_out = param.avg_mesh;
		for (UInt i1 = 0; i1 < verts.size(); i1++) {
			verts_out[i1].x += xx[i1*3+0];
			verts_out[i1].y += xx[i1*3+1];
			verts_out[i1].z += xx[i1*3+2];
		}
		return verts_out;

	}

};


#endif
