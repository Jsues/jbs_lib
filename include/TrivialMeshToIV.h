#ifndef TRIVIAL_MESH_TO_IV_H
#define TRIVIAL_MESH_TO_IV_H


#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoIndexedLineSet.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoSelection.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoNormal.h>
#include <Inventor/nodes/SoTransparencyType.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/SbColor.h>


#include <Inventor/manips/SoHandleBoxManip.h>

#include "TrivialMesh.h"
#include "TetraMesh4D.h"


SoSeparator* createTrivialMeshToIv(SoCoordinate3** coord3, SoFaceSet** fs, float col = 0.0f) {

	assert(coord3 != 0);
	assert(fs != 0);
	
	SoSeparator* ivmesh = new SoSeparator();
	SbColor color;
	if (col == 0.0f)
		color = SbVec3f(0.7f, 0.7f, 0.7f);
	else {
		color.setHSVValue(col, 1.0f, 1.0f);
	}
	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setValue(color);
	ivmesh->addChild(mat);

	SoShapeHints* sh = new SoShapeHints();
	// Flat shading
	sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	//sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	*coord3 = new SoCoordinate3;
	ivmesh->addChild(*coord3);
	*fs = new SoFaceSet;
	ivmesh->addChild(*fs);

	return ivmesh;

}

template<class T>
void updateTrivialMeshToIv(const TrivialMesh<T>* mesh, SoCoordinate3* coord3, SoFaceSet* fs) {

	assert(coord3 != 0);
	assert(fs != 0);
	
	// Create points:
	int numTri = (int)mesh->getNumTris();
	SbVec3f* Points = new SbVec3f[numTri*3];
	for (int i1 = 0; i1 < numTri; i1++) {
		T pt0 = mesh->tris[i1].v0;
		Points[i1*3+0][0] = (float)pt0[0]; Points[i1*3+0][1] = (float)pt0[1]; Points[i1*3+0][2] = (float)pt0[2];
		T pt1 = mesh->tris[i1].v1;
		Points[i1*3+1][0] = (float)pt1[0]; Points[i1*3+1][1] = (float)pt1[1]; Points[i1*3+1][2] = (float)pt1[2];
		T pt2 = mesh->tris[i1].v2;
		Points[i1*3+2][0] = (float)pt2[0]; Points[i1*3+2][1] = (float)pt2[1]; Points[i1*3+2][2] = (float)pt2[2];
	}
	coord3->point.deleteValues(0);
	coord3->point.setValues(0, numTri*3, Points);
	delete[] Points;
	
	fs->numVertices.deleteValues(0);
	for (int i1 = 0; i1 < numTri; i1++) {
		fs->numVertices.set1Value(i1, 3);
	}
}

template<class T>
SoSeparator* TrivialMeshToIv(const TrivialMesh<T>* mesh, float col = 0.0f) {

	SoCoordinate3* coord3;
	SoFaceSet* fs;
	SoSeparator *separator = createTrivialMeshToIv(&coord3, &fs, col);
	updateTrivialMeshToIv<T>(mesh, coord3, fs);
	return separator;
}

template<class T>
SoSeparator* Tet4DfToIv(Tet4D<T>* tet, float time_min=0.0f, float time_max=1.0f) {

	SoSeparator* ivmesh = new SoSeparator();

	SoShapeHints* sh = new SoShapeHints();
	// Flat shading
	sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	//sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	// Add 4 points projected to t = 0;
	SbVec3f Points[4];
	Points[0][0] = tet->v0.x; Points[0][1] = tet->v0.y; Points[0][2] = tet->v0.z;
	Points[1][0] = tet->v1.x; Points[1][1] = tet->v1.y; Points[1][2] = tet->v1.z;
	Points[2][0] = tet->v2.x; Points[2][1] = tet->v2.y; Points[2][2] = tet->v2.z;
	Points[3][0] = tet->v3.x; Points[3][1] = tet->v3.y; Points[3][2] = tet->v3.z;
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, 4, Points);
	ivmesh->addChild(coord3);

	SbColor color0;
	color0.setHSVValue(tet->v0.w, 1.0f, 1.0f);
	SbColor color1;
	color1.setHSVValue(tet->v1.w, 1.0f, 1.0f);
	SbColor color2;
	color2.setHSVValue(tet->v2.w, 1.0f, 1.0f);
	SbColor color3;
	color3.setHSVValue(tet->v3.w, 1.0f, 1.0f);
	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setNum(4);
	mat->diffuseColor.set1Value(0, color0);
	mat->diffuseColor.set1Value(1, color1);
	mat->diffuseColor.set1Value(2, color2);
	mat->diffuseColor.set1Value(3, color3);
	ivmesh->addChild(mat);

	SoMaterialBinding* matbind = new SoMaterialBinding;
	matbind->value = SoMaterialBinding::PER_VERTEX_INDEXED;
	ivmesh->addChild(matbind);

	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(4);
	int32_t indices1[] = {0, 2, 1, -1 };
	int32_t indices2[] = {0, 1, 3, -1 };
	int32_t indices3[] = {2, 3, 1, -1 };
	int32_t indices4[] = {0, 3, 2, -1 };
	ifs->coordIndex.setValues(0, 4, indices1);
	ifs->coordIndex.setValues(4, 4, indices2);
	ifs->coordIndex.setValues(8, 4, indices3);
	ifs->coordIndex.setValues(12, 4, indices4);
	//ifs->materialIndex.setNum(4);
	//ifs->materialIndex.set1Value(0,0);
	//ifs->materialIndex.set1Value(1,1);
	//ifs->materialIndex.set1Value(2,2);
	//ifs->materialIndex.set1Value(3,3);
	ivmesh->addChild(ifs);

	SoPointSet* ps = new SoPointSet;
	SoDrawStyle* ds = new SoDrawStyle;
	ds->pointSize = 6;
	ivmesh->addChild(ds);
	ivmesh->addChild(ps);

	return ivmesh;
}


SoSeparator* TetraMesh4DfIv(TetraMesh4D<float>* tetmesh, float time_min=0.0f, float time_max=1.0f) {

	SoSeparator* ivmesh = new SoSeparator();

	SoShapeHints* sh = new SoShapeHints();
	// Flat shading
	sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	//sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	UInt numTets = tetmesh->getNumTets();
	SbVec3f* Points = new SbVec3f[numTets*4];
	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setNum(numTets*4);

	for (UInt i1 = 0; i1 < numTets; i1++) {

		Tet4D<float>* tet = &(tetmesh->getTetrahedraPts())[i1];

		Points[i1*4+0][0] = tet->v0.x; Points[i1*4+0][1] = tet->v0.y; Points[i1*4+0][2] = tet->v0.z;
		Points[i1*4+1][0] = tet->v1.x; Points[i1*4+1][1] = tet->v1.y; Points[i1*4+1][2] = tet->v1.z;
		Points[i1*4+2][0] = tet->v2.x; Points[i1*4+2][1] = tet->v2.y; Points[i1*4+2][2] = tet->v2.z;
		Points[i1*4+3][0] = tet->v3.x; Points[i1*4+3][1] = tet->v3.y; Points[i1*4+3][2] = tet->v3.z;

		//std::cerr << tet->v0.w << " " << tet->v1.w << " " << tet->v2.w << " " << tet->v3.w << std::endl;

		SbColor color0;
		float c = ((tet->v0.w-time_min)/(time_max-time_min));
		color0.setHSVValue(c*0.75f, 1.0f, 1.0f);
		mat->diffuseColor.set1Value(i1*4+0, color0);
		c = ((tet->v1.w-time_min)/(time_max-time_min));
		color0.setHSVValue(c*0.75f, 1.0f, 1.0f);
		mat->diffuseColor.set1Value(i1*4+1, color0);
		c = ((tet->v2.w-time_min)/(time_max-time_min));
		color0.setHSVValue(c*0.75f, 1.0f, 1.0f);
		mat->diffuseColor.set1Value(i1*4+2, color0);
		c = ((tet->v3.w-time_min)/(time_max-time_min));
		color0.setHSVValue(c*0.75f, 1.0f, 1.0f);
		mat->diffuseColor.set1Value(i1*4+3, color0);
	}

	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numTets*4, Points);
	ivmesh->addChild(coord3);
	ivmesh->addChild(mat);


	SoMaterialBinding* matbind = new SoMaterialBinding;
	matbind->value = SoMaterialBinding::PER_VERTEX_INDEXED;
	ivmesh->addChild(matbind);

	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(16*numTets);

	for (UInt i1 = 0; i1 < numTets; i1++) {
		int32_t si = 4*i1;
		int32_t indices1[] = {si+0, si+2, si+1, -1 };
		int32_t indices2[] = {si+0, si+1, si+3, -1 };
		int32_t indices3[] = {si+2, si+3, si+1, -1 };
		int32_t indices4[] = {si+0, si+3, si+2, -1 };
		ifs->coordIndex.setValues(i1*16+0, 4, indices1);
		ifs->coordIndex.setValues(i1*16+4, 4, indices2);
		ifs->coordIndex.setValues(i1*16+8, 4, indices3);
		ifs->coordIndex.setValues(i1*16+12, 4, indices4);
	}
	ivmesh->addChild(ifs);

	return ivmesh;
}



#endif
