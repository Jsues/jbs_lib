#ifndef CONVEXCONCAVE_GRAPH_TO_IV_H
#define CONVEXCONCAVE_GRAPH_TO_IV_H

#include "PolyLineToIV.h"
#include "ConvexConcaveGraph.h"


#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoScale.h>


SoSeparator* ConvexConcaveGraphToIV(ConvexConcaveGraph* g, float sphere_scaler, float tube_scaler) {

	SoSeparator* sep = new SoSeparator;
	UInt numNodes = g->getNumNodes();

	PolyLine<vec3d> pl_concave;
	PolyLine<vec3d> pl_convex;
	for (UInt i1 = 0; i1 < numNodes; i1++) {
		pl_concave.insertVertex(g->nodes[i1].getNodeCenter());
		pl_convex.insertVertex(g->nodes[i1].getNodeCenter());
	}
	for (UInt i1 = 0; i1 < g->getNumEdges(); i1++) {
		ConvexConcaveEdge e = g->edges[i1];
		if (e.getEdgeType() == CONVEX) {
			pl_convex.insertLine(e.n0, e.n1);
		} else {
			pl_concave.insertLine(e.n0, e.n1);
		}
	}

	std::vector<vec3d> convexNodes;
	std::vector<vec3d> concaveNodes;
	std::vector<vec3d> mixedNodes;
	for (UInt i1 = 0; i1 < numNodes; i1++) {
		EdgeTypeEnum eType = g->nodes[i1].getNodeType();
		if		(eType == CONVEX)	convexNodes.push_back(g->nodes[i1].getNodeCenter());
		else if	(eType == CONCAVE)	concaveNodes.push_back(g->nodes[i1].getNodeCenter());
		else						mixedNodes.push_back(g->nodes[i1].getNodeCenter());
	}

	SoSphere* sp = new SoSphere;
	sp->radius = sphere_scaler;

	//std::cerr << "convexNodes: " << (UInt)convexNodes.size() << " concaveNodes: " << (UInt)concaveNodes.size() << " mixedNodes: " << (UInt)mixedNodes.size() << std::endl;

	//! Convex Nodes
	SoMaterial* mat_convex	= new SoMaterial;
 	mat_convex->ambientColor.setValue(0.1f, 0.1f, 0.7f);
 	mat_convex->diffuseColor.setValue(0.5f, 0.5f, 1.0f);
 	mat_convex->specularColor.setValue(0.7f, 0.9f, 0.7f);
 	mat_convex->shininess = 0.2f;
 	sep->addChild(mat_convex);
	for (UInt i1 = 0; i1 < convexNodes.size(); i1++) {
		SoSeparator* s1 = new SoSeparator;
		vec3d nC = convexNodes[i1];
		SoTranslation* trans = new SoTranslation;
		//SoScale scale;
		//scale.scaleFactor.setValue();
		trans->translation.setValue(SbVec3f((float)nC.x, (float)nC.y, (float)nC.z));
		s1->addChild(trans);
		s1->addChild(sp);
		sep->addChild(s1);
	}

	SoMaterial* mat_concave = new SoMaterial;
 	mat_concave->ambientColor.setValue(0.7f, 0.1f, 0.1f);
 	mat_concave->diffuseColor.setValue(1.0f, 0.5f, 0.5f);
 	mat_concave->specularColor.setValue(0.7f, 0.7f, 0.9f);
 	mat_concave->shininess = 0.2f;
 	sep->addChild(mat_concave);
	for (UInt i1 = 0; i1 < concaveNodes.size(); i1++) {
		SoSeparator* s1 = new SoSeparator;
		vec3d nC = concaveNodes[i1];
		SoTranslation* trans = new SoTranslation;
		trans->translation.setValue(SbVec3f((float)nC.x, (float)nC.y, (float)nC.z));
		s1->addChild(trans);
		s1->addChild(sp);
		sep->addChild(s1);
	}

	SoMaterial* mat_mixed   = new SoMaterial;
 	mat_mixed->ambientColor.setValue(0.1f, 0.1f, 0.1f);
 	mat_mixed->diffuseColor.setValue(0.5f, 0.5f, 0.5f);
 	mat_mixed->specularColor.setValue(0.7f, 0.7f, 0.9f);
 	mat_mixed->shininess = 0.2f;
 	sep->addChild(mat_mixed);
	for (UInt i1 = 0; i1 < mixedNodes.size(); i1++) {
		SoSeparator* s1 = new SoSeparator;
		vec3d nC = mixedNodes[i1];
		SoTranslation* trans = new SoTranslation;
		trans->translation.setValue(SbVec3f((float)nC.x, (float)nC.y, (float)nC.z));
		s1->addChild(trans);
		s1->addChild(sp);
		sep->addChild(s1);
	}

	sep->addChild(PolyLineToIV(&pl_convex, 0.66f));
	sep->addChild(PolyLineToIV(&pl_concave, 0.01f));


	return sep;
}



#endif
