#ifndef JBS_RBF_H
#define JBS_RBF_H

#include "MatrixnD.h"
#include "ScalarFunction.h"
#include "PointCloud.h"



template <class T>
class RBFFunctionSamplesBase {
public:

	//! Returns the 'i-th' sample point.
	virtual inline T getSamplePos(UInt i) const = 0;

	//! Returns the desired function value at the 'i-th' sample point.
	virtual inline typename T::ValueType getSampleVal(UInt i) const = 0;

	//! Returns the desired gradient value at the 'i-th' sample point.
	virtual inline T getSampleGrad(UInt i) const = 0;

	//! Returns the number of function samples
	virtual inline UInt getNumSamples() const = 0;

};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

template <class T>
class RBFFunctionSamplesRBF_TreeWrap : public RBFFunctionSamplesBase<T> {

public:

	//! Type of a Point, for example a 3D Vector of floats.
	typedef T Point;

	//! Value-Type of Templates components (i.e. float or double).
	typedef typename T::ValueType ValueType;

	RBFFunctionSamplesRBF_TreeWrap(UInt numSamples_, UInt* idxArray_, Point* points_, ValueType* values_, Point* gradients_=NULL ) {
		m_numSamples = numSamples_;
		m_idxArray = idxArray_;
		m_points = points_;
		m_values = values_;
		m_gradients = gradients_;
	}

	//! Returns the 'i-th' sample point.
	virtual Point getSamplePos(UInt i) const {
		if (i < m_numSamples) return m_points[m_idxArray[i]];
		else return addPts[i-m_numSamples];
	}

	//! Returns the desired function value at the 'i-th' sample point.
	virtual inline ValueType getSampleVal(UInt i) const {
		if (i < m_numSamples) return m_values[m_idxArray[i]];
		else return addVals[i-m_numSamples];
	}

	//! Returns the 'i-th' normal vector.
	virtual Point getSampleGrad(UInt i) const {
		return m_gradients[m_idxArray[i]];
	}

	//! Returns the number of function samples
	virtual inline UInt getNumSamples() const {
		return (UInt)(m_numSamples+addPts.size());
	}

private:
	UInt* m_idxArray;
	Point* m_points;
	ValueType* m_values;
	Point* m_gradients;
	UInt m_numSamples;

	std::vector<Point> addPts;
	std::vector<ValueType> addVals;
};

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************


//! The function samples which will be approximated by the RBFs.
template <class Point>
class RBFFunctionSamples : public RBFFunctionSamplesBase<Point> {

public:

	//! Value-Type of Templates components (i.e. float or double).
	typedef typename Point::ValueType ValueType;

	//! Returns the function value for sample 'i', or 0 if 'i' is an on-surface point.
	inline ValueType getFunctionValue(UInt i) const {
		return ((i >= numNonZeroSamples) ? 0 : values[i]);
	}

	//! Returns the 'i-th' sample point.
	virtual Point getSamplePos(UInt i) const {
		return positions[i];
	}

	//! Returns the 'i-th' normal vector.
	virtual Point getSampleGrad(UInt i) const {
		return directions[i];
	}

	//! Returns the desired function value at the 'i-th' sample point.
	virtual inline ValueType getSampleVal(UInt i) const {
		return getFunctionValue(i);
	}

	//! Returns the number of function samples
	virtual inline UInt getNumSamples() const {
		return (UInt) positions.size();
	}

	std::vector<Point> positions;
	std::vector<Point> directions;
	std::vector<ValueType> values;

	UInt numNonZeroSamples;
};


//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class ValueType, class Point=point3d<ValueType> >
class RadialBasisFunction {
	
public:
	typedef Point Point;

	RadialBasisFunction() {
		c_square = 1;
		radius = 1;
	}

	//! Evaluates the basis function at r.
	virtual inline ValueType eval(ValueType r) const = 0;
	//! Returns the factor when this function is derived for x, if it is derived for center, use the negative value!
	virtual inline ValueType evalGradFactor(ValueType r) const = 0;
	//! Computes the hessian Matrix
	virtual inline SquareMatrixND<Point> evalHessian(Point x_min_c) const = 0;

	virtual char* getName() const = 0;

	void print() const {
		std::cerr << "Radial basis function, radius = " << radius << " c_square = " << c_square << std::endl;
		std::cerr << "RBF Type: " << getName() << std::endl;
	}

	ValueType c_square;
	ValueType radius;
};

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class ValueType, class Point=point3d<ValueType> >
class thinplate2D : public RadialBasisFunction<ValueType,Point> {

public:
	typedef Point Point;

	inline ValueType eval(ValueType r) const {
		if (r == 0) return 0;
		return r*r*log(r);
	}
	inline ValueType evalGradFactor(ValueType r) const {
		if (r == 0) return 0;
		return (ValueType)1.0+log(r*r);
	}
	inline SquareMatrixND<Point> evalHessian(Point x_min_c) const {
		return SquareMatrixND<Point>();
	}
	char* getName() const {
		return "thinplate2D";
	}

};

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class ValueType, class Point=point3d<ValueType> >
class Gaussian : public RadialBasisFunction<ValueType,Point> {

public:
	typedef Point Point;

	inline ValueType eval(ValueType r) const {
		return exp(- r*r * c_square);
	}
	inline ValueType evalGradFactor(ValueType r) const {
		return -2*c_square * eval(r);
	}
	inline SquareMatrixND<Point> evalHessian(Point x_min_c) const {

		ValueType f = exp(- x_min_c.squaredLength() * c_square);

		SquareMatrixND<Point> ret(x_min_c);
		ret *= 4*c_square*c_square*f;

		ValueType id = 2*c_square*f;
		for (UInt i1 = 0; i1 < Point::dim; i1++) {
			ret(i1,i1) -= id;
		}
		return ret;
	}
	char* getName() const {
		return "Gaussian";
	}

};


//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

//! Hardy's multiquadric
template <class ValueType, class Point=point3d<ValueType> >
class multiquadric2 : public RadialBasisFunction<ValueType,Point> {

public:
	typedef Point Point;

	inline ValueType eval(ValueType r) const {
		return sqrt(r*r * c_square);
	}
	inline ValueType evalGradFactor(ValueType r) const {
		return c_square/sqrt(r*r * c_square);
	}
	inline SquareMatrixND<Point> evalHessian(Point x_min_c) const {

		ValueType denom = sqrt(x_min_c.squaredLength() * c_square);
		ValueType denom3 = denom*denom*denom;

		SquareMatrixND<Point> ret(x_min_c);
		ret *= (-c_square*c_square)/denom3;

		ValueType id = c_square/denom;
		for (UInt i1 = 0; i1 < Point::dim; i1++) {
			ret(i1,i1) += id;
		}

		return ret;
	}
	char* getName() const {
		return "MultiQuadric2, Hardy's multiquadric";
	}

};


//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class ValueType, class Point=point3d<ValueType> >
class multiquadric : public RadialBasisFunction<ValueType,Point> {

public:
	typedef Point Point;

	inline ValueType eval(ValueType r) const {
		return sqrt((ValueType)1.0 + r*r * c_square);
	}
	inline ValueType evalGradFactor(ValueType r) const {
		return c_square/sqrt((ValueType)1.0 + r*r * c_square);
	}
	inline SquareMatrixND<Point> evalHessian(Point x_min_c) const {

		ValueType denom = sqrt((ValueType)1.0 + x_min_c.squaredLength() * c_square);
		ValueType denom3 = denom*denom*denom;

		SquareMatrixND<Point> ret(x_min_c);
		ret *= (-c_square*c_square)/denom3;

		ValueType id = c_square/denom;
		for (UInt i1 = 0; i1 < Point::dim; i1++) {
			ret(i1,i1) += id;
		}

		return ret;
	}
	char* getName() const {
		return "MultiQuadric";
	}

};


//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************
// See e.g. STABLE COMPUTATIONS WITH GAUSSIAN RADIAL BASIS FUNCTIONS IN 2-D  -  BENGT FORNBERG, ELISABETH LARSSON†, AND NATASHA FLYER
template <class ValueType, class Point=point3d<ValueType> >
class InverseMultiquadric : public RadialBasisFunction<ValueType,Point> {

public:
	typedef Point Point;

	inline ValueType eval(ValueType r) const {
		return 1/sqrt((ValueType)1.0 + r*r * c_square);
	}
	inline ValueType evalGradFactor(ValueType r) const {
		return c_square/sqrt((ValueType)1.0 + r*r * c_square);
	}
	inline SquareMatrixND<Point> evalHessian(Point x_min_c) const {

		ValueType denom = sqrt((ValueType)1.0 + x_min_c.squaredLength() * c_square);
		ValueType denom3 = denom*denom*denom;

		SquareMatrixND<Point> ret(x_min_c);
		ret *= (-c_square*c_square)/denom3;

		ValueType id = c_square/denom;
		for (UInt i1 = 0; i1 < Point::dim; i1++) {
			ret(i1,i1) += id;
		}

		return ret;
	}
	char* getName() const {
		return "MultiQuadric";
	}

};

//****************************************************************************************************

template <class ValueType, class Point=point3d<ValueType> >
class InverseQuadratic : public RadialBasisFunction<ValueType,Point> {

public:
	typedef Point Point;

	inline ValueType eval(ValueType r) const {
		return ValueType(1.0)/(1+c_square*r*r);
	}
	inline ValueType evalGradFactor(ValueType r) const {
		return -(ValueType)2.0*c_square/((1+c_square*r*r)*(1+c_square*r*r));
	}
	inline SquareMatrixND<Point> evalHessian(Point x_min_c) const {

		ValueType length_sq = x_min_c.squaredLength();
		SquareMatrixND<Point> ret(x_min_c);

		ValueType denom = (1+length_sq*c_square);
		ret *= (8*c_square*c_square)/(denom*denom*denom);
		ValueType id = (2*c_square)/(denom*denom);
		for (UInt i1 = 0; i1 < Point::dim; i1++) {
			ret(i1,i1) -= id;
		}

		return ret;
	}
	
	char* getName() const {
		return "InverseQuadratic";
	}
};

//****************************************************************************************************

template <class ValueType, class Point=point3d<ValueType> >
class biharmonic3D : public RadialBasisFunction<ValueType,Point> {

public:
	typedef Point Point;

	inline ValueType eval(ValueType r) const {
		return r;
	}
	inline ValueType evalGradFactor(ValueType r) const {
		return (ValueType)-1.0/r;
	}
	inline SquareMatrixND<Point> evalHessian(Point x_min_c) const {
		return SquareMatrixND<Point>();
	}
	
	char* getName() const {
		return "biharmonic3D";
	}
};

//****************************************************************************************************

template <class ValueType, class Point=point3d<ValueType> >
class thinplate3D : public RadialBasisFunction<ValueType,Point> {

public:
	typedef Point Point;

	inline ValueType eval(ValueType r) const {
		return r*r*r;
	}
	inline ValueType evalGradFactor(ValueType r) const {
		return 3*r;
	}
	inline SquareMatrixND<Point> evalHessian(Point x_min_c) const {

		ValueType length = x_min_c.length();
		SquareMatrixND<Point> ret(x_min_c);

		ret /= length;
		for (UInt i1 = 0; i1 < Point::dim; i1++) {
			ret(i1,i1) += length;
		}
		ret *= 3;

		return ret;
	}
	
	char* getName() const {
		return "thinplate3D";
	}
};

//****************************************************************************************************

template <class ValueType, class Point=point3d<ValueType> >
class local : public RadialBasisFunction<ValueType,Point> {

public:
	typedef Point Point;

	inline ValueType eval(ValueType r) const {
		r /= radius;
		if (r > 1) return 0;
		return (1-r)*(1-r)*(1-r)*(1-r)*(1+4*r);
	}
	inline ValueType evalGradFactor(ValueType r) const {
		r /= radius;
		if (r > 1) return 0;
		return 20*(r-1)*(r-1)*(r-1);
	}
	inline SquareMatrixND<Point> evalHessian(Point x_min_c) const {
		return SquareMatrixND<Point>();
	}
	
	char* getName() const {
		return "local";
	}
};


//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

template <class RBF_Type = thinplate3D<double, vec3d> >
class RBF_sum : public ScalarFunction<typename RBF_Type::Point> {

public:

//****************************************************************************************************

	typedef RBF_Type RBF_Type;
	typedef typename RBF_Type::Point Point;
	typedef typename Point::ValueType ValueType;
	
//****************************************************************************************************

	RBF_sum(UInt numCenters, Point* centers, const ValueType* heights, ValueType radius = 1, ValueType c_square = 1) : m_numCenters(numCenters) {

		m_centers = new Point[numCenters];
		m_heights = new ValueType[m_numCenters];

		basisfunction.radius = radius;
		basisfunction.c_square = c_square;

		for (UInt i1 = 0; i1 < numCenters; i1++) {
			m_centers[i1] = centers[i1];
			m_heights[i1] = (heights == NULL) ? 0 : heights[i1];
		}
		linear_term = Point((ValueType)0);
		d = 0;
	}
	
//****************************************************************************************************

	RBF_sum(const RBF_sum& other) {

		m_numCenters = other.m_numCenters;
		basisfunction = other.basisfunction;

		m_centers = new Point[m_numCenters];
		m_heights = new ValueType[m_numCenters];

		for (UInt i1 = 0; i1 < m_numCenters; i1++) {
			m_centers[i1] = other.m_centers[i1];
			m_heights[i1] = other.m_heights[i1];
		}
		linear_term = other.linear_term;
		d = other.d;
	}

//****************************************************************************************************

	~RBF_sum() {
		delete[] m_centers;
		delete[] m_heights;
	}

//****************************************************************************************************

	inline UInt getNumCenters() const {
		return m_numCenters;
	}

//****************************************************************************************************

	inline ValueType evalValAndGrad(const Point& pt, Point& res_g) const {
		ValueType res = 0;
		res_g = Point((ValueType)0);
		Point dir;
		ValueType d_i;
		for (UInt i1 = 0; i1 < m_numCenters; i1++) {
			dir = pt-m_centers[i1];
			d_i = dir.length();
			res += basisfunction.eval(d_i)*m_heights[i1];
			res_g += dir * (m_heights[i1]*basisfunction.evalGradFactor(d_i));
		}

		res_g += linear_term;
		return res+(pt|linear_term) + d;
	}

//****************************************************************************************************

	virtual inline ValueType eval(const Point& pt) const {
		ValueType res = 0;
		for (UInt i1 = 0; i1 < m_numCenters; i1++) {
			res += evalBasis(pt, i1)*m_heights[i1];
		}
		return res+(pt|linear_term) + d;
	}
	
//****************************************************************************************************

	virtual inline Point evalGrad(const Point& pt) const {
		Point res((ValueType)0);
		for (UInt i1 = 0; i1 < m_numCenters; i1++) {
			const ValueType r = pt.dist(m_centers[i1]);
			res += (pt-m_centers[i1])*m_heights[i1] * basisfunction.evalGradFactor(r);
		}
		return (res + linear_term);
	}

//****************************************************************************************************

	inline ValueType evalNormalize(const Point& pt) const {
		ValueType res = 0;
		for (UInt i1 = 0; i1 < m_numCenters; i1++) {
			res += evalBasis(pt, i1)*m_heights[i1];
		}
		res += (pt|linear_term) + d;

		ValueType gl = evalGrad(pt).length();

		if (gl == 0) return res;
		return (res/gl);
	}

//****************************************************************************************************

	//! Sums up the distance of all points in 'pc' to the surface (uses the taubin distance)
	inline ValueType computeLeastSquaresError(PointCloudNormals<Point>* pc) {
		ValueType error = 0;
		Point* pts = pc->getPoints();
		ValueType e;
		for (UInt i1 = 0; i1 < (UInt)pc->getNumPts(); i1++) {
			e = evalNormalize(pts[i1]);
			error += e*e;
		}
		return error;
	}

//****************************************************************************************************

	//! Sums up the distance of all points in 'pc' to the surface (uses the taubin distance)
	inline ValueType computeError(PointCloudNormals<Point>* pc) {
		ValueType error = 0;
		Point* pts = pc->getPoints();
		for (UInt i1 = 0; i1 < (UInt)pc->getNumPts(); i1++) {
			error += fabs(evalNormalize(pts[i1]));
		}
		return error;
	}

//****************************************************************************************************

	inline const Point* getPoints() const {
		return m_centers;
	}

//****************************************************************************************************

	void print() const {
		for (UInt i1 = 0; i1 < m_numCenters; i1++) {
			std::cerr << m_centers[i1] << "\t " << m_heights[i1] << std::endl;
		}
		std::cerr << "Linear part: " << linear_term << std::endl;
		std::cerr << "Const offset: " << d << std::endl;
	}

//****************************************************************************************************

	inline ValueType evalBasis(const Point& pt, UInt RBF_id) const {
		ValueType r = pt.dist(m_centers[RBF_id]);
		return basisfunction.eval(r);
	}

// ****************************************************************************************

	virtual UInt getID() const {
		return ScalarFunction<typename RBF_Type::Point>::TYPE_RBF_SUM;
	}

//****************************************************************************************************

	ValueType* m_heights;

	Point linear_term;
    ValueType d;

private:

//****************************************************************************************************
public:

	RBF_Type basisfunction;
	Point* m_centers;
	UInt m_numCenters;
};

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************

//template <class T>
//class RBFStack {
//
//public:
//
////****************************************************************************************************
//
//	typedef T Point;
//	typedef typename T::ValueType ValueType;
//
////****************************************************************************************************
//
//	void addRBF(RBF_sumBase<Point>* rbf) {
//		rbfs.push_back(rbf);
//	}
//
////****************************************************************************************************
//
//	inline UInt getNumRBFs() const {
//		return (UInt) rbfs.size();
//	}
//
////****************************************************************************************************
//
//	inline ValueType eval(const Point& pt, Point& grad) const {
//		ValueType val = 0;
//		grad = Point((ValueType)0);
//
//		for (UInt i1 = 0; i1 < getNumRBFs(); i1++) {
//			val += rbfs[i1]->eval(pt);
//			grad += rbfs[i1]->evalGrad(pt);
//		}
//
//		//val /= grad.length();
//
//		return val;
//	}
//
////****************************************************************************************************
//
//	inline ValueType evalNormalize(const Point& pt, Point& grad) const {
//		ValueType val = eval(pt, grad);
//
//		return val/(grad.length());
//	}
//
////****************************************************************************************************
//
//private:
//public:
//
//	std::vector<RBF_sumBase<Point>*> rbfs;
//};

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************


#endif
