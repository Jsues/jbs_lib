#ifndef POLY_LINE_SAMPLER_H
#define POLY_LINE_SAMPLER_H

#include <algorithm>
#include "Polyline.h"
#include <glm/glm.hpp>

template <class T>
class LineSampler {

public:

	LineSampler(const std::vector<T>& points) {

		T last = points[0];
		typename T::ValueType sum_dists = 0;
		params.push_back(0);
		positions.push_back(last);

		for (UInt i1 = 1; i1 < points.size(); i1++) {
			T current = points[i1];
			typename T::ValueType dist = current.dist(last);
			sum_dists += dist;

			positions.push_back(current);
			params.push_back(sum_dists);
			last = current;
		}
		
		for (UInt i1 = 0; i1 < params.size(); i1++) {
			params[i1] /= sum_dists;
		}
	}

	T sampleAt(typename T::ValueType u_in) {

		typename T::ValueType u = u_in;
		u = std::max(u, (typename T::ValueType)0.0);
		u = std::min(u, (typename T::ValueType)1.0);

		typename std::vector<typename T::ValueType>::const_iterator beg = params.begin();
		typename std::vector<typename T::ValueType>::const_iterator it = std::lower_bound(params.begin(), params.end(), u);

		int index_of_pre = it-beg-1;
		//std::cerr << "u: " << u << " pos: " << index_of_pre << std::endl;

		if (index_of_pre < 0) index_of_pre = 0; // Catch u_in == 0 case.

		// linearly interpolate between vertices index_of_pre and index_of_pre+1
		const T& p_p = positions[index_of_pre];
		const T& p_n = positions[index_of_pre+1];
		typename T::ValueType u_p = params[index_of_pre];
		typename T::ValueType u_n = params[index_of_pre+1];

		typename T::ValueType on_line = u-u_p;
		typename T::ValueType w_n = (on_line)/(u_n-u_p);
		typename T::ValueType w_p = 1-w_n;

		//std::cerr << (p_p*w_p + p_n*w_n) << std::endl;
		return p_p*w_p + p_n*w_n;
	}

	T sampleAtCyclic(typename T::ValueType u_in) {

		// cyclically wraps u_in onto [0...1] interval
		typename T::ValueType u = u_in - std::floor(u_in);

		typename std::vector<typename T::ValueType>::const_iterator beg = params.begin();
		typename std::vector<typename T::ValueType>::const_iterator it = std::upper_bound(params.begin(), params.end(), u);

		int index_of_pre = it-beg;
		//std::cerr << "u: " << u << " pos: " << it-beg << std::endl;

		// linearly interpolate between vertices index_of_pre and index_of_pre+1
		const T& p_p = positions[index_of_pre];
		const T& p_n = positions[index_of_pre+1];
		typename T::ValueType u_p = params[index_of_pre];
		typename T::ValueType u_n = params[index_of_pre+1];

		typename T::ValueType on_line = u-u_p;
		typename T::ValueType w_n = (on_line)/(u_n-u_p);
		typename T::ValueType w_p = 1-w_n;

		return p_p*w_p + p_n*w_n;
	}

private:

	// the u-parameter of the vertices on the polyline (0 to 1) (0 for the first vertex)
	std::vector<typename T::ValueType> params;
	
	// The vertices of the polyline (first = last!!!)
	std::vector<T> positions;

};



template <class T>
class PolyLineSampler {

public:

	PolyLineSampler(const PolyLine<T>& pl) {

		std::vector<UInt> cycle = pl.getCycle();

		T last = pl.points[cycle[0]].c;
		typename T::ValueType sum_dists = 0;
		params.push_back(0);
		for (UInt i1 = 1; i1 < cycle.size(); i1++) {
			UInt pos = cycle[i1];
			T current = pl.points[pos].c;
			typename T::ValueType dist = current.dist(last);
			sum_dists += dist;

			positions.push_back(current);
			params.push_back(sum_dists);
			last = current;
		}
		//positions.push_back(positions[0]);
		//params.push_back(1);
		
		for (UInt i1 = 0; i1 < params.size(); i1++) {
			params[i1] /= sum_dists;
		}
	}

	T sampleAtNonCyclic(typename T::ValueType u_in) {

		typename T::ValueType u = u_in;
		u = std::max(u, (typename T::ValueType)0.0);
		u = std::min(u, (typename T::ValueType)1.0);

		typename std::vector<typename T::ValueType>::const_iterator beg = params.begin();
		typename std::vector<typename T::ValueType>::const_iterator it = std::upper_bound(params.begin(), params.end(), u);

		int index_of_pre = it-beg-1;
		//std::cerr << "u: " << u << " pos: " << it-beg << std::endl;

		// linearly interpolate between vertices index_of_pre and index_of_pre+1
		const T& p_p = positions[index_of_pre];
		const T& p_n = positions[index_of_pre+1];
		typename T::ValueType u_p = params[index_of_pre];
		typename T::ValueType u_n = params[index_of_pre+1];

		typename T::ValueType on_line = u-u_p;
		typename T::ValueType w_n = (on_line)/(u_n-u_p);
		typename T::ValueType w_p = 1-w_n;

		return p_p*w_p + p_n*w_n;
	}

	T sampleAtCyclic(typename T::ValueType u_in) {

		// cyclically wraps u_in onto [0...1] interval
		typename T::ValueType u = u_in - std::floor(u_in);

		typename std::vector<typename T::ValueType>::const_iterator beg = params.begin();
		typename std::vector<typename T::ValueType>::const_iterator it = std::upper_bound(params.begin(), params.end(), u);

		int index_of_pre = it-beg;
		//std::cerr << "u: " << u << " pos: " << it-beg << std::endl;

		// linearly interpolate between vertices index_of_pre and index_of_pre+1
		const T& p_p = positions[index_of_pre];
		const T& p_n = positions[index_of_pre+1];
		typename T::ValueType u_p = params[index_of_pre];
		typename T::ValueType u_n = params[index_of_pre+1];

		typename T::ValueType on_line = u-u_p;
		typename T::ValueType w_n = (on_line)/(u_n-u_p);
		typename T::ValueType w_p = 1-w_n;

		return p_p*w_p + p_n*w_n;
	}

private:

	// the u-parameter of the vertices on the polyline (0 to 1) (0 for the first vertex)
	std::vector<typename T::ValueType> params;
	
	// The vertices of the polyline (first = last!!!)
	std::vector<T> positions;

};



template <class GLM_VEC>
class LineSamplerGLM {

public:

	LineSamplerGLM(const std::vector<GLM_VEC>& points) {

		GLM_VEC last = points[0];
		typename GLM_VEC::value_type sum_dists = 0;
		params.push_back(0);
		positions.push_back(last);

		for (UInt i1 = 1; i1 < points.size(); i1++) {
			GLM_VEC current = points[i1];
			typename GLM_VEC::value_type dist = glm::distance(current, last);
			sum_dists += dist;

			positions.push_back(current);
			params.push_back(sum_dists);
			last = current;
		}

		for (UInt i1 = 0; i1 < params.size(); i1++) {
			params[i1] /= sum_dists;
		}
	}

	GLM_VEC sampleAt(typename GLM_VEC::value_type u_in) {

		typename GLM_VEC::value_type u = u_in;
		u = std::max(u, (typename GLM_VEC::value_type)0.0);
		u = std::min(u, (typename GLM_VEC::value_type)1.0);

		typename std::vector<typename GLM_VEC::value_type>::const_iterator beg = params.begin();
		typename std::vector<typename GLM_VEC::value_type>::const_iterator it = std::lower_bound(params.begin(), params.end(), u);

		int index_of_pre = it - beg - 1;
		//std::cerr << "u: " << u << " pos: " << index_of_pre << std::endl;

		if (index_of_pre < 0) index_of_pre = 0; // Catch u_in == 0 case.

		// linearly interpolate between vertices index_of_pre and index_of_pre+1
		const GLM_VEC& p_p = positions[index_of_pre];
		const GLM_VEC& p_n = positions[index_of_pre + 1];
		typename GLM_VEC::value_type u_p = params[index_of_pre];
		typename GLM_VEC::value_type u_n = params[index_of_pre + 1];

		typename GLM_VEC::value_type on_line = u - u_p;
        typename GLM_VEC::value_type w_n;
        typename GLM_VEC::value_type u_d = u_n - u_p;
        if (u_d == typename GLM_VEC::value_type(0.0)) {
            w_n = typename GLM_VEC::value_type(0.0);
        } else {
		    w_n = (on_line) / u_d;
        }
		typename GLM_VEC::value_type w_p = 1 - w_n;

		//std::cerr << (p_p*w_p + p_n*w_n) << std::endl;
		return p_p*w_p + p_n*w_n;
	}

	int indexAt(typename GLM_VEC::value_type u_in) {

		typename GLM_VEC::value_type u = u_in;
		u = std::max(u, (typename GLM_VEC::value_type)0.0);
		u = std::min(u, (typename GLM_VEC::value_type)1.0);

		typename std::vector<typename GLM_VEC::value_type>::const_iterator beg = params.begin();
		typename std::vector<typename GLM_VEC::value_type>::const_iterator it = std::upper_bound(params.begin(), params.end(), u);

		return it - beg;
	}


private:

	// the u-parameter of the vertices on the polyline (0 to 1) (0 for the first vertex)
	std::vector<typename GLM_VEC::value_type> params;

	// The vertices of the polyline (first = last!!!)
	std::vector<GLM_VEC> positions;

};


#endif



