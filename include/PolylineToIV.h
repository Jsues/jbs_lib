#ifndef POLYLINE_TO_IV_H
#define POLYLINE_TO_IV_H

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoIndexedLineSet.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoMatrixTransform.h>
#include <Inventor/nodes/SoComplexity.h>

#include "Polyline.h"
#include "hsv2rgb.h"


// ****************************************************************************************


//! Convert Polyline to Inventor node
/*!
	\param pl Polyline
	\param radius radius of the Polyline
	\param col Sorry dude, not used a.t.m.
	\param comp Complexity [0...1] set to 1 for a high detailed geometry, to 0.5 for medium detail and 0.1 for low detail.
*/
template <class T>
SoSeparator* PolyLineToIVTube(PolyLine<T>* pl, float radius = 1, float col = 0, float comp = 0.5f, UInt bar_length = 10, SoMaterial* mat1 = NULL, SoMaterial* mat2 = NULL) {

	SoSeparator* sep = new SoSeparator;
	SoComplexity* complexity = new SoComplexity;
	complexity->value = comp;
	sep->addChild(complexity);

	float sphere_radius = radius;
	SoSphere* sp = new SoSphere;
	sp->radius = sphere_radius;

	if (mat1 == NULL) {
		mat1 = new SoMaterial;
		SbColor color;
		if (col == 0.0f) {
			color = SbVec3f(0.7f, 0.7f, 0.7f);
		} else {
			color.setHSVValue(col, 1.0f, 1.0f);
		}
		mat1->diffuseColor.setValue(color);
	}
	if (mat2 == NULL) {
		mat2 = new SoMaterial;
		SbColor color;
		color = SbVec3f(0.7f,0.7f,0.7f);
		mat2->diffuseColor.setValue(color);
	}

	sep->addChild(mat1);

// 	SoMaterial* mat = new SoMaterial;
// 	SbColor color;
// 	if (col == 0.0f) {
// 		color = SbVec3f(0.7f, 0.7f, 0.7f);
// 	} else {
// 		color.setHSVValue(col, 1.0f, 1.0f);
// 	}
// 	mat->diffuseColor.setValue(color);
// 	sep->addChild(mat);

// 	mat->ambientColor.setValue(0.1f, 0.1f, 0.3f);
// 	mat->diffuseColor.setValue(0.5f, 0.5f, 0.6f);
// 	mat->specularColor.setValue(0.7f, 0.9f, 0.7f);
// 	mat->shininess = 0.2f;
// 	sep->addChild(mat);

	UInt count = 0;
	for (std::list<LineSegment>::iterator it = pl->lines.begin(); it != pl->lines.end(); it++) {
		point3d<typename T::ValueType> endpoint;
		for (UInt i1 = 0; i1 < 3; i1++) endpoint[i1] = pl->points[it->v1].c[i1];
		point3d<typename T::ValueType> startpoint;
		for (UInt i1 = 0; i1 < 3; i1++) startpoint[i1] = pl->points[it->v0].c[i1];
		point3d<typename T::ValueType> midpoint = (startpoint + endpoint)/2;

		point3d<typename T::ValueType> dir = (startpoint-endpoint);
		typename T::ValueType length = dir.length();

		dir.normalize();
		point3d<typename T::ValueType> second, third;
		second = point3d<typename T::ValueType>(1,0,0);
		third = second^dir;
		if (third.squaredLength() == 0) {
			second = point3d<typename T::ValueType>(0,1,0);
			third = dir^second;
		}
		third.normalize();
		second = third^dir;

		SoSFMatrix mat;
		mat.setValue(	(float)second.x,	(float)second.y,	(float)second.z, 0,
						(float)dir.x,		(float)dir.y,		(float)dir.z, 0,
						(float)third.x,		(float)third.y,		(float)third.z, 0,
						(float)midpoint.x,	(float)midpoint.y,	(float)midpoint.z, 1);
		SoMatrixTransform* transm = new SoMatrixTransform;
		transm->matrix = mat;

		if (count % bar_length == 0) {
			//std::cerr << "Add bar at " << count;
			if ((count / bar_length) % 2 == 0) {
				//std::cerr << ": Even bar" << std::endl;
				sep->addChild(mat1);
			} else {
				//std::cerr << ": Odd bar" << std::endl;
				sep->addChild(mat2);
			}
		}
		++count;

		SoSeparator* s2 = new SoSeparator;
		s2->addChild(complexity);
		SoCylinder* cyl = new SoCylinder;
		cyl->radius = radius;
		cyl->height = (float)length;
		s2->addChild(transm);
		s2->addChild(cyl);
		sep->addChild(s2);

		// Add startsphere if v0 has only one line
		if (pl->points[it->v0].lines.size() == 1) {
			SoSeparator* sep2 = new SoSeparator;
			SoTranslation* trans = new SoTranslation;
			trans->translation.setValue((float)startpoint.x, (float)startpoint.y, (float)startpoint.z);
			sep2->addChild(trans);
			sep2->addChild(sp);
			sep->addChild(sep2);
		}

		SoSeparator* sep2 = new SoSeparator;
		SoTranslation* trans = new SoTranslation;
		trans->translation.setValue((float)endpoint.x, (float)endpoint.y, (float)endpoint.z);
		sep2->addChild(trans);
		sep2->addChild(sp);
		sep->addChild(sep2);

	}

	return sep;
}


// ****************************************************************************************


template <class T>
SoSeparator* PolyLineToIV(PolyLine<T>* pl, float col = 0, int size = 3, float* col2 = NULL, float col3 = 1.0f, vec3d col_rgb = vec3d(0,0,0)) {

	SoSeparator* sep = new SoSeparator;

	if (T::dim <= 3) {
		SbColor color;
		if (col == 0.0f)
			color = SbVec3f(0.7f, 0.7f, 0.7f);
		else if (col == -1.0f)
			color = SbVec3f(0.0f, 0.0f, 0.0f);
		else if (col == 10.0f)
			color = SbVec3f(1.0f, 1.0f, 1.0f);
		else
			color.setHSVValue(col, 1.0f, 1.0f);

		if (col2 != NULL)
			color = SbVec3f(col2[0], col2[1], col2[2]);

		if (col3 != 1.0f) {
			color.setHSVValue(col, 1.0f, col3);
		}

		if (col_rgb.squaredLength() > 0)
			color = SbVec3f((float)col_rgb[0], (float)col_rgb[1], (float)col_rgb[2]);


		SoMaterial* mat = new SoMaterial;
		mat->diffuseColor.setValue(color);
		sep->addChild(mat);
	} else if (T::dim == 4) {
		T minV, maxV;
		pl->ComputeMinAndMaxExt(minV, maxV);
		typename T::ValueType minT = minV[3];
		typename T::ValueType maxT = maxV[3];
		SoMaterial* mat = new SoMaterial;
		UInt numV = (UInt)pl->points.size();
		for (UInt i1 = 0; i1 < numV; i1++) {
			typename T::ValueType w = pl->points[i1].c[3];
			w -= minT;
			w /= (maxT-minT);
			w *= (typename T::ValueType) 0.7;
			SbColor color; color.setHSVValue((float)w, 1, 1);
			mat->diffuseColor.set1Value(i1, color);
		}
		sep->addChild(mat);
		SoMaterialBinding* matbind = new SoMaterialBinding;
		matbind->value = SoMaterialBinding::PER_VERTEX_INDEXED;
		sep->addChild(matbind);
	}
	
	SbVec3f* line_pts = new SbVec3f[pl->getNumV()];

	if (T::dim >= 3) {
		for (UInt i1 = 0; i1 < pl->getNumV(); i1++) {
			line_pts[i1] = SbVec3f((float)pl->points[i1].c[0], (float)pl->points[i1].c[1], (float)pl->points[i1].c[2]);
		}
	} else {
		for (UInt i1 = 0; i1 < pl->getNumV(); i1++) {
			line_pts[i1] = SbVec3f((float)pl->points[i1].c[0], (float)pl->points[i1].c[1], 0);
		}
	}

	SoCoordinate3* coord = new SoCoordinate3;
	coord->point.setValues(0, (int)(pl->getNumV()), line_pts);
	sep->addChild(coord);
	delete[] line_pts;

	SoDrawStyle* ds = new SoDrawStyle;
	ds->lineWidth = (float)size;
	sep->addChild(ds);

	SoIndexedLineSet* ls = new SoIndexedLineSet;
	ls->coordIndex.setNum((int)(3*pl->lines.size()));

	int num = 0;
	for (std::list<LineSegment>::iterator it = pl->lines.begin(); it != pl->lines.end(); it++) {
		int32_t indices[] = {it->v0, it->v1, -1 };
		//std::cerr << indices[0] << " " << indices[1] << " " << indices[2] << std::endl;
		ls->coordIndex.setValues(num, 3, indices);
		num += 3;
	}
	sep->addChild(ls);


	return sep;

}


// ****************************************************************************************


template <class T>
SoSeparator* PolyLineToIVColorByMarker(PolyLine<T>* pl, int size = 3) {

	SoSeparator* sep = new SoSeparator;

	
	SbVec3f* line_pts = new SbVec3f[pl->getNumV()];

	if (T::dim >= 3) {
		for (UInt i1 = 0; i1 < pl->getNumV(); i1++) {
			line_pts[i1] = SbVec3f((float)pl->points[i1].c[0], (float)pl->points[i1].c[1], (float)pl->points[i1].c[2]);
		}
	} else {
		for (UInt i1 = 0; i1 < pl->getNumV(); i1++) {
			line_pts[i1] = SbVec3f((float)pl->points[i1].c[0], (float)pl->points[i1].c[1], 0);
		}
	}

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setNum(pl->getNumL());
	int i = 0;
	for (std::list<LineSegment>::const_iterator it = pl->lines.begin(); it != pl->lines.end(); ++it) {
		int col = it->marker;
		float x = (float)col/255.0f;
		x *= 0.66f;
		x = 0.66f-x;
		vec3f rgb = JBSlib::hsv2rgb(vec3f(x, 1,1));
		mat->diffuseColor.set1Value(i, SbColor(rgb.array));
		i++;
	}
	sep->addChild(mat);
	SoMaterialBinding* matbind = new SoMaterialBinding;
	matbind->value = SoMaterialBinding::PER_FACE;
	sep->addChild(matbind);

	SoCoordinate3* coord = new SoCoordinate3;
	coord->point.setValues(0, (int)(pl->getNumV()), line_pts);
	sep->addChild(coord);
	delete[] line_pts;

	SoDrawStyle* ds = new SoDrawStyle;
	ds->lineWidth = (float)size;
	sep->addChild(ds);

	SoIndexedLineSet* ls = new SoIndexedLineSet;
	ls->coordIndex.setNum((int)(3*pl->lines.size()));

	int num = 0;
	for (std::list<LineSegment>::iterator it = pl->lines.begin(); it != pl->lines.end(); it++) {
		int32_t indices[] = {it->v0, it->v1, -1 };
		//std::cerr << indices[0] << " " << indices[1] << " " << indices[2] << std::endl;
		ls->coordIndex.setValues(num, 3, indices);
		num += 3;
	}
	sep->addChild(ls);


	return sep;

}


// ****************************************************************************************


template <class T>
SoSeparator* TrivialLineToIV(TrivialLine<T>* tl, float col = 0, int lw = 3) {

	SoSeparator* sep = new SoSeparator;
	sep->setName("TrivialLineSep");

	SbColor color;
	if (col == 0.0f)
		color = SbVec3f(0.7f, 0.7f, 0.7f);
	else {
		color.setHSVValue(col, 1.0f, 1.0f);
	}
	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setValue(color);
	sep->addChild(mat);
	
	SbVec3f* line_pts = new SbVec3f[2 * tl->lines.size()];
	for (UInt i1 = 0; i1 < tl->lines.size(); i1++) {
		line_pts[i1*2+0] = SbVec3f((float)tl->lines[i1].v0.x, (float)tl->lines[i1].v0.y, (float)tl->lines[i1].v0.z);
		line_pts[i1*2+1] = SbVec3f((float)tl->lines[i1].v1.x, (float)tl->lines[i1].v1.y, (float)tl->lines[i1].v1.z);
	}
	SoCoordinate3* coord = new SoCoordinate3;
	coord->point.setValues(0, (int)(2*tl->lines.size()), line_pts);
	sep->addChild(coord);
	delete[] line_pts;

	SoDrawStyle* ds = new SoDrawStyle;
	ds->lineWidth = (float)lw;
	sep->addChild(ds);

	SoIndexedLineSet* ls = new SoIndexedLineSet;
	ls->coordIndex.setNum((int)(3*tl->lines.size()));
	for (UInt i1 = 0; i1 < tl->lines.size(); i1++) {
		int32_t indices[] = {i1*2, i1*2+1, -1 };
		ls->coordIndex.setValues(i1*3, 3, indices);
	}
	sep->addChild(ls);


	return sep;

}


// ****************************************************************************************

template <class T>
SoSeparator* BBToIVTube(T minV, T maxV, float radius = 1, float col = 0, float comp = 0.5f, UInt bar_length = 10, SoMaterial* mat1 = NULL, SoMaterial* mat2 = NULL) {
	PolyLine<T>* pl = new PolyLine<T>;
	pl->insertVertex(T(minV.x, minV.y, minV.z));
	pl->insertVertex(T(maxV.x, minV.y, minV.z));
	pl->insertVertex(T(maxV.x, maxV.y, minV.z));
	pl->insertVertex(T(minV.x, maxV.y, minV.z));
	pl->insertVertex(T(minV.x, minV.y, maxV.z));
	pl->insertVertex(T(maxV.x, minV.y, maxV.z));
	pl->insertVertex(T(maxV.x, maxV.y, maxV.z));
	pl->insertVertex(T(minV.x, maxV.y, maxV.z));
	pl->insertLine(0,1);
	pl->insertLine(1,2);
	pl->insertLine(2,3);
	pl->insertLine(3,0);
	pl->insertLine(4,5);
	pl->insertLine(5,6);
	pl->insertLine(6,7);
	pl->insertLine(7,4);
	pl->insertLine(0,4);
	pl->insertLine(1,5);
	pl->insertLine(2,6);
	pl->insertLine(3,7);
	return PolyLineToIVTube(pl, radius, col, comp, bar_length, mat1, mat2);
}

// ****************************************************************************************


#endif
