#ifndef PATTERN_DEFORMER_H
#define PATTERN_DEFORMER_H

#include <vector>
#include <set>
#include "point3d.h"
#include "point4d.h"
#include "MatrixnD.h"
#include "SVD_Matrix.h"
//#include "svd3.h"

#include <utility>

#if defined(USE_UMFPACK)
#include "umfpack.h"
#elif defined(USE_EIGEN)
#include <Eigen/Sparse>
#else
#error Please specify either USE_UMFPACK=1 or USE_EIGEN=1 as preprocesser define.
#endif

class PatternDeformer {

public:
	
	//! A constraint (An affine combination ('weights') of the 4 vertices in 'indices')
	struct Constraint {
		vec4i indices;
		vec4d weights;
		vec3d target_pos;
	};
	
private:

	struct Edge {
		Edge(int _n, double _w=0) : neighbor(_n), w(_w) {
		}
		int neighbor;
		double w;
	};

	struct VertexSnapConstraint {
		VertexSnapConstraint(int _v0, int _v1) : v0(_v0), v1(_v1) {};
		int v0, v1;
	};

public:

	PatternDeformer(std::vector<vec3d>& _m_verts, std::vector<vec4i>& _m_quads) {
		addMesh(_m_verts, _m_quads);
	}


	void addMesh(std::vector<vec3d>& _m_verts, std::vector<vec4i>& _m_quads) {
		m_verts.insert(m_verts.end(), _m_verts.begin(), _m_verts.end());
		m_quads.insert(m_quads.end(), _m_quads.begin(), _m_quads.end());
		// Build connectivity graph
		m_connectivity.resize(m_verts.size());
		m_connectivity_full.resize(m_verts.size());
		for (unsigned int i1 = 0; i1 < _m_quads.size(); i1++) {
			const vec4i q = _m_quads[i1];
			insertNeighbor(q.x, q.y);
			insertNeighbor(q.x, q.w);
			insertNeighborDiag(q.x, q.z);
			insertNeighbor(q.y, q.x);
			insertNeighbor(q.y, q.z);
			insertNeighborDiag(q.y, q.w);
			insertNeighbor(q.z, q.y);
			insertNeighbor(q.z, q.w);
			insertNeighborDiag(q.z, q.x);
			insertNeighbor(q.w, q.z);
			insertNeighbor(q.w, q.x);
			insertNeighborDiag(q.w, q.y);
		}
		m_verts_working = m_verts;
		is_constrained.clear();
		is_constrained.resize(m_verts.size(), false);
	}


	~PatternDeformer() {}


	void addQuadMidpointConstraint(int quadID, const vec3d& targetpos) {
		Constraint c;
		c.indices = m_quads[quadID];
		c.weights = vec4d(0.25);
		c.target_pos = targetpos;
		m_constraints.push_back(c);
		m_constrained_quads.push_back(quadID);

		m_needToUpdateMatrix = true;
	}


	void addConstraint(vec4i indices, vec4d weights, const vec3d& targetpos) {
		Constraint c;
		c.indices = indices;
		c.weights = weights;
		c.target_pos = targetpos;
		m_constraints.push_back(c);

		m_needToUpdateMatrix = true;
	}


	void addPositionConstraint(int vertexID, const vec3d& targetpos) {
		int quadIndex = -1;
		for (unsigned int i1 = 0; i1 < m_quads.size(); i1++) {
			const vec4i& q = m_quads[i1];
			if (q.x == vertexID || q.y == vertexID || q.z == vertexID || q.w == vertexID) {
				quadIndex = i1;
				break;
			}
		}

		Constraint c;
		c.indices = m_quads[quadIndex];
		c.weights = vec4d(0.0);
		for (int i1 = 0; i1 < 4; i1++) if (c.indices[i1] == vertexID) c.weights[i1] = 1;
		c.target_pos = targetpos;
		m_constraints.push_back(c);

		m_needToUpdateMatrix = true;
	}


	void setStiffness(double stiffness) {
		m_stiffness = stiffness;
		m_needToUpdateMatrix = true;
	}


	//! Deforms the pattern according to constraints and returns the new vertex positions.
	const std::vector<vec3d>& deformToConstraints(int numIter = 10) {
		if (m_needToUpdateMatrix) factorMatrix();

		m_verts_working = m_verts;
		for (int i1 = 0; i1 < numIter; i1++) {
			smoothStitch();
			ARAP_Iter();
		}

		return m_verts_working;
	}


	// Adds a constraint that links vertices v0 and v1.
	void AddVertexSnapConstraint(int v0, int v1, bool is_smooth=false) {
		m_vert_snap.push_back(VertexSnapConstraint(v0,v1));
		if (is_smooth) {
			m_smoothVertices.push_back(v0);
			m_smoothVertices.push_back(v1);
		}
	}


	void moveSnappedVertsToSamePosition() {
		for (unsigned int i1 = 0; i1 < m_vert_snap.size(); i1++) {
			int v0 = m_vert_snap[i1].v0;
			int v1 = m_vert_snap[i1].v1;
			vec3d newpos = (m_verts_working[v0]+m_verts_working[v1])/2;
			m_verts_working[v0] = newpos;
			m_verts_working[v1] = newpos;
		}
	}

	void updateConnectivityToContainStichesNeighbors() {

		is_constrained.clear();
		is_constrained.resize(m_verts.size(), false);

		for (unsigned int i1 = 0; i1 < m_vert_snap.size(); i1++) {
			const VertexSnapConstraint& v = m_vert_snap[i1];
			addNeighbors(v.v0, v.v1);
			addNeighbors(v.v1, v.v0);
			is_constrained[v.v0] = true;
			is_constrained[v.v1] = true;
		}
	}

private:

	// adds neighbors of 'source' to 'target' unless a corresponding snapped neighbor already exists in 'target'
	void addNeighbors(int source, int target) {
		for (unsigned int i1 = 0; i1 < m_connectivity[source].size(); i1++) {
			int n = m_connectivity[source][i1].neighbor;
			// Check if n or it's corresponding vertex is already contained:
			bool is_contained = false;
			for (unsigned int i2 = 0; i2 < m_connectivity[target].size(); i2++) {
				int nn = m_connectivity[target][i2].neighbor;
				if (nn == n || areSnapped(nn, n)) is_contained = true;
			}
			if (!is_contained) {
				insertNeighbor(target, n, -1);
			}
		}
	}
    /*
    void FastSvd(const SquareMatrixND<vec3d>& COV, SquareMatrixND<vec3d>& U, SquareMatrixND<vec3d>& V)
    {
        //https://github.com/ericjang/svd3.git
        
        float   u11, u12, u13,
        u21, u22, u23,
        u31, u32, u33;
        
        float   s11, s12, s13,
        s21, s22, s23,
        s31, s32, s33;
        
        float   v11, v12, v13,
        v21, v22, v23,
        v31, v32, v33;
        
        svd(COV(0,0), COV(0,1), COV(0,2), COV(1,0), COV(1,1), COV(1,2), COV(2,0), COV(2,1), COV(2,2), u11, u12, u13, u21, u22, u23, u31, u32, u33, s11, s12, s13, s21, s22, s23, s31, s32, s33, v11, v12, v13, v21, v22, v23, v31, v32, v33);
        
        U(0,0) = u11; U(0,1) = u12; U(0,2) = u13;
        U(1,0) = u21; U(1,1) = u22; U(1,2) = u23;
        U(2,0) = u31; U(2,1) = u32; U(2,2) = u33;
        
        V(0,0) = v11; V(0,1) = v12; V(0,2) = v13;
        V(1,0) = v21; V(1,1) = v22; V(1,2) = v23;
        V(2,0) = v31; V(2,1) = v32; V(2,2) = v33;
    }
     */

	void ARAP_Iter() {

		int numUnknown = (int)m_verts.size();
		//int numC = (int)m_constraints.size();

		std::vector< SquareMatrixND<vec3d> > rots_vec(numUnknown);
		SquareMatrixND<vec3d>* rots = &rots_vec[0];
		std::vector<double> final_rhs(3*numUnknown);
		std::copy(m_rhs.begin(), m_rhs.end(), final_rhs.begin());
		std::vector<double> xv(numUnknown*3);

        double** cachedCV = NULL;
        double** cachedV = NULL;
        
		// Compute optimal rotations:
		for (int i2 = 0; i2 < (int)numUnknown; i2++) {
			SquareMatrixND<vec3d> COV;
			int v0 = i2;
			const vec3d& p_i = m_verts[v0];
			const vec3d& p_dash_i = m_verts_working[v0];
			for (unsigned i3 = 0; i3 < m_connectivity[v0].size(); i3++) {
				if (m_connectivity[v0][i3].w == -1) {
					continue; // found over-seam neighbor, don't use for rotation computation.
				}
				int v1 = m_connectivity[v0][i3].neighbor;
				const vec3d& p_j = m_verts[v1];
				const vec3d& p_dash_j = m_verts_working[v1];
				COV.addFromTensorProduct((p_i-p_j), (p_dash_i-p_dash_j));
			}
			SquareMatrixND<vec3d> U, V;
			vec3d sigma;
			/*bool SVD_result = */COV.SVD_decomp(U, sigma, V, &cachedCV, &cachedV);
            // Is not actually faster than generic NR SVD.
            //FastSvd(COV, U, V);
			U.transpose();
			SquareMatrixND<vec3d> rot = V*U;
			rots[i2] = rot;
		}

		// Add rhs:
		for (int i1 = 0; i1 < numUnknown; i1++) {
			int numE = (int)m_connectivity[i1].size();
			const vec3d& p0 = m_verts[i1];
			for (int i2 = 0; i2 < numE; i2++) {
				int other = m_connectivity[i1][i2].neighbor;
				const vec3d& p1 = m_verts[other];
				SquareMatrixND<vec3d> rot = rots[i1]+rots[other];
				vec3d diff = rot.vecTrans(p0-p1)*(m_stiffness/2);
				final_rhs[i1] += diff.x;
				final_rhs[i1+numUnknown] += diff.y;
				final_rhs[i1+2*numUnknown] += diff.z;
			}
		}

#if defined(USE_UMFPACK)
		for (int c = 0; c < 3; c++) {
			double* b = &final_rhs[c*numUnknown];
			double* x = &xv[c*numUnknown];
			umfpack_di_solve(UMFPACK_A, &m_col_ptr[0], &m_row_index[0], &m_entries[0], x, b, m_Numeric, NULL, NULL);
			for (int i1 = 0; i1 < numUnknown; i1++) m_verts_working[i1][c] = x[i1];	
		}
#else
		Eigen::VectorXd rhs_eigen_x;
		rhs_eigen_x.resize(numUnknown);
		Eigen::VectorXd rhs_eigen_y;
		rhs_eigen_y.resize(numUnknown);
		Eigen::VectorXd rhs_eigen_z;
		rhs_eigen_z.resize(numUnknown);
		memcpy(&rhs_eigen_x[0], &final_rhs[0*numUnknown], numUnknown*sizeof(double));
		memcpy(&rhs_eigen_y[0], &final_rhs[1*numUnknown], numUnknown*sizeof(double));
		memcpy(&rhs_eigen_z[0], &final_rhs[2*numUnknown], numUnknown*sizeof(double));
		Eigen::VectorXd X_x = solverA.solve(rhs_eigen_x);
		Eigen::VectorXd X_y = solverA.solve(rhs_eigen_y);
		Eigen::VectorXd X_z = solverA.solve(rhs_eigen_z);
		for (int i1 = 0; i1 < numUnknown; i1++) m_verts_working[i1] = vec3d(X_x[i1], X_y[i1], X_z[i1]);
#endif

	}



	void factorMatrix() {
		
		double snapweight_sq = 100.0;

		int dim = m_verts.size();
		int numC = m_constraints.size();

		// Initialize constraint weights with 0
		std::vector<double> constraint_diag_weights(dim, 0.0);
		for (int i1 = 0; i1 < dim; i1++) {
			for (unsigned int i2 = 0; i2 < m_connectivity_full[i1].size(); i2++) {
				m_connectivity_full[i1][i2].w = 0;
			}
		}

		// Initialize vertex snap constraint array
		std::vector<int> vertexSnapConstraints(dim, -1);
		int numVCS = (int)m_vert_snap.size();
		for (int i1 = 0; i1 < numVCS; i1++) {
			VertexSnapConstraint& vsc = m_vert_snap[i1];
			vertexSnapConstraints[vsc.v0] = vsc.v1;
			vertexSnapConstraints[vsc.v1] = vsc.v0;
		}


		// Constraint part of the matrix (BtB)
		for (int i1 = 0; i1 < numC; i1++) {
	
			const Constraint& c = m_constraints[i1];

			// Set vertex weights:
			for (int i2 = 0; i2 < 4; i2++) {
				const int& v1 = c.indices[i2];
				const double& w1 = c.weights[i2];

				constraint_diag_weights[v1] += w1*w1;

				// Set edge weights
				for (int i3 = 0; i3 < 4; i3++) {
					if (i3 == i2) continue;
					const int& v2 = c.indices[i3];
					const double& w2 = c.weights[i3];
					Edge* e = getEdge_full(v1,v2);
					e->w += w1*w2;
				}
			}
		}



#if defined(USE_UMFPACK)
		m_row_index.clear();
		m_entries.clear();
		m_col_ptr.clear(); m_col_ptr.push_back(0);
#else
		std::vector<Eigen::Triplet<double>> entries;
		A.resize(dim, dim);
#endif

		for (int i1 = 0; i1 < dim; i1++) {

			std::set< std::pair<int, double> > neighbors;
			//double sum = 0;

			for (unsigned i2 = 0; i2 < m_connectivity_full[i1].size(); i2++) {
				int n = m_connectivity_full[i1][i2].neighbor;
				double w = m_connectivity_full[i1][i2].w;
				
				const Edge* e = getEdge(i1, n);
				if (e != NULL) { // Add volume ARAP regularizer part:
					w -= 1*m_stiffness;
				}

				if (w != 0) {
					neighbors.insert(std::make_pair(n, w));
				}
			}
			
			// Add volume ARAP regularizer part:
			if (is_constrained[i1]) std::cerr << m_connectivity[i1].size() << std::endl;
			double vertWeight = m_connectivity[i1].size()*m_stiffness;
			// Add constraint weight:
			vertWeight += constraint_diag_weights[i1];

			// Add snapping constraints
			int snapNeighbor = vertexSnapConstraints[i1];
			if (snapNeighbor != -1) {
				vertWeight += snapweight_sq;
				neighbors.insert(std::pair<int, double>(snapNeighbor, -snapweight_sq));
			}

			neighbors.insert(std::pair<int, double>(i1, vertWeight));

#if defined(USE_UMFPACK)
			for (std::set< std::pair<int, double> >::const_iterator it = neighbors.begin(); it != neighbors.end(); it++) {
				int nID = it->first;
				m_entries.push_back(it->second);
				m_row_index.push_back(nID);
			}
			m_col_ptr.push_back((int)m_entries.size());
#else
			for (std::set< std::pair<int, double> >::const_iterator it = neighbors.begin(); it != neighbors.end(); it++) {
				const int& nID = it->first;
				const double& weight = it->second;
				entries.push_back(Eigen::Triplet<double>(i1, nID, weight));
			}
#endif


		}

#if defined(USE_UMFPACK)
		// Pre-factor matrix
		int result1 = umfpack_di_symbolic(dim, dim, &m_col_ptr[0], &m_row_index[0], &m_entries[0], &m_Symbolic, NULL, NULL);
		int result2 = umfpack_di_numeric(&m_col_ptr[0], &m_row_index[0], &m_entries[0], m_Symbolic, &m_Numeric, NULL, NULL);
		std::cerr << "UMFPACK Result: " << result1 << " " << result2 << std::endl;
#else
		A.setFromTriplets(entries.begin(), entries.end());
		solverA.compute(A);
#endif

		// Compute rhs_addon here and not during iteration!!
		m_rhs.clear();
		m_rhs.resize(3*dim, 0);
		double* rhs_x = &m_rhs[0];
		double* rhs_y = &m_rhs[dim];
		double* rhs_z = &m_rhs[2*dim];
		for (int i1 = 0; i1 < numC; i1++) {
			const Constraint& c = m_constraints[i1];
			// Set vertex weights:
			for (int i2 = 0; i2 < 4; i2++) {
				const int& v1 = c.indices[i2];
				const double& w1 = c.weights[i2];
				rhs_x[v1] += c.target_pos.x*w1;
				rhs_y[v1] += c.target_pos.y*w1;
				rhs_z[v1] += c.target_pos.z*w1;
			}
		}
		m_needToUpdateMatrix = false;
	}


	void insertNeighbor(int vert, int neighbor, double weight=0.0) {
		// search neighbor
		bool exists = false;
		for (std::vector<Edge>::const_iterator it = m_connectivity[vert].begin(); it != m_connectivity[vert].end(); ++it) {
			if (it->neighbor == neighbor) {
				exists = true;
				break;
			}
		}
		if (!exists) {
			m_connectivity[vert].push_back(Edge(neighbor,weight));
			m_connectivity_full[vert].push_back(Edge(neighbor,weight));
		}
	}


	void insertNeighborDiag(int vert, int neighbor) {
		// search neighbor
		bool exists = false;
		for (std::vector<Edge>::const_iterator it = m_connectivity_full[vert].begin(); it != m_connectivity_full[vert].end(); ++it) {
			if (it->neighbor == neighbor) {
				exists = true;
				break;
			}
		}
		if (!exists) {
			m_connectivity_full[vert].push_back(Edge(neighbor,0));
		}
	}


	// returns the edge from v1 to v2.
	Edge* getEdge(int v1, int v2) {
		bool exists = false;
		for (std::vector<Edge>::iterator it = m_connectivity[v1].begin(); it != m_connectivity[v1].end(); ++it) {
			if (it->neighbor == v2) {
				return &(*it);
			}
		}
		if (!exists) {
			//std::cerr << "Edge does not exist" << std::endl;
		}
		return NULL;
	}


	// returns the edge from v1 to v2.
	Edge* getEdge_full(int v1, int v2) {
		bool exists = false;
		for (std::vector<Edge>::iterator it = m_connectivity_full[v1].begin(); it != m_connectivity_full[v1].end(); ++it) {
			if (it->neighbor == v2) {
				return &(*it);
			}
		}
		if (!exists) {
			//std::cerr << "Edge does not exist" << std::endl;
		}
		return NULL;
	}

	//! Returns whether there is a snapping constraint between v0 and v1:
	bool areSnapped(int v0, int v1) {
		for (unsigned int i1 = 0; i1 < m_vert_snap.size(); i1++) {
			const VertexSnapConstraint& v = m_vert_snap[i1];
			if ((v.v0 == v0 && v.v1 == v1) || (v.v0 == v1 && v.v1 == v0)) return true;
		}
		return false;
	}

	void smoothStitch() {

		std::vector<vec3d> norms = computeVertexNeighbors();

		for (unsigned int i1 = 0; i1 < m_smoothVertices.size(); i1++) {
			int idx = m_smoothVertices[i1];
			if (getSnapPartner(idx) >= 0) {
				vec3d newPos = getSmoothPosStitched(idx);
				vec3d disp = newPos - m_verts_working[idx];
				double d = disp|norms[idx];
				//m_verts_working[idx] += disp;
				m_verts_working[idx] += norms[idx]*d;
			}
		}
	}

	//! Returns whether there is a snapping constraint between v0 and v1:
	int getSnapPartner(int v0) {
		for (unsigned int i1 = 0; i1 < m_vert_snap.size(); i1++) {
			const VertexSnapConstraint& v = m_vert_snap[i1];
			if (v.v0 == v0) return v.v1;
			if (v.v1 == v0) return v.v0;
		}
		//std::cerr << "no snapping partner" << std::endl;
		return -1;
	}

	vec3d getSmoothPosStitched(int index) {
		//if (!is_constrained[index]) {
		//	std::cerr << "Error, smoothing non-stitched vertex" << std::endl;
		//}
		vec3d avg;
		int cnt = 0;
		for (unsigned int i1 = 0; i1 < m_connectivity[index].size(); i1++) {
			int n = m_connectivity[index][i1].neighbor;
			avg += m_verts_working[n];
			cnt++;
		}
		int nindex = getSnapPartner(index);
		for (unsigned int i1 = 0; i1 < m_connectivity[nindex].size(); i1++) {
			int n = m_connectivity[nindex][i1].neighbor;
			avg += m_verts_working[n];
			cnt++;
		}
		return avg/cnt;
	}

	std::vector<vec3d> computeVertexNeighbors() {
		std::vector<vec3d> normals;
		normals.resize(m_verts_working.size(), vec3d(0,0,0));

		for (unsigned int i1 = 0; i1 < m_quads.size(); i1++) {
			const vec4i& q = m_quads[i1];
			vec3d n = getQuadNormal(q);
			for (int c = 0; c < 4; c++) normals[q[c]] += n;
		}

		for (unsigned int i1 = 0; i1 < normals.size(); i1++) {
			normals[i1].normalize();
		}

		return normals;
	}

	vec3d getQuadNormal(const vec4i& q) {
		vec3d n;
		n += (m_verts_working[q.x]-m_verts_working[q.w])^(m_verts_working[q.x]-m_verts_working[q.y]);
		n += (m_verts_working[q.y]-m_verts_working[q.x])^(m_verts_working[q.y]-m_verts_working[q.z]);
		n += (m_verts_working[q.z]-m_verts_working[q.y])^(m_verts_working[q.z]-m_verts_working[q.w]);
		n += (m_verts_working[q.w]-m_verts_working[q.z])^(m_verts_working[q.w]-m_verts_working[q.x]);
		//q.print();n.print();
		n.normalize();
		return n;
	}

public:
	std::vector<Constraint> m_constraints;
	bool m_needToUpdateMatrix;
	double m_stiffness;
	std::vector<vec3d> m_verts;
	std::vector<vec4i> m_quads;
	std::vector< std::vector<Edge> > m_connectivity;
	std::vector< std::vector<Edge> > m_connectivity_full;
	std::vector<bool> is_constrained;
	
#if defined(USE_UMFPACK)
	// UMFPACK STUFF
	std::vector<double> m_entries;
	std::vector<int> m_row_index;
	std::vector<int> m_col_ptr;
	void *m_Symbolic, *m_Numeric;
#else
	// Eigen stuff
	Eigen::SparseMatrix<double> A;
	Eigen::SimplicialLDLT< Eigen::SparseMatrix<double> > solverA;
#endif


	std::vector<double> m_rhs;
	std::vector<int> m_constrained_quads;

	std::vector<VertexSnapConstraint> m_vert_snap;
	std::vector<int> m_smoothVertices;

public:
	std::vector<vec3d> m_verts_working;
};

#endif
