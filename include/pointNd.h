#ifndef JBS_POINTND_H
#define JBS_POINTND_H


#include <iostream>
#include <cmath>


//! A generic N-dimensional vector.
template <class T, int d>
class pointNd
{
public:

	//! Value-Type of Templates components.
	typedef T ValueType;
	static const int dim = d;

// ****************************************************************************************

	pointNd() {
		setToZero();
	}

// ****************************************************************************************

	pointNd(ValueType value) {
		for (int i1 = 0; i1 < dim; i1++) array[i1] = value;
	}

// ****************************************************************************************

	pointNd(const pointNd& other) {
		for (int i1 = 0; i1 < dim; i1++) array[i1] = other.array[i1];
	}

// ****************************************************************************************

	void setToZero() {
		for (int i1 = 0; i1 < dim; i1++) array[i1] = ValueType(0);
	}

// ****************************************************************************************

	pointNd<ValueType,dim> operator=(const pointNd& other) {

		if (dim != other.dim) {
			std::cerr << "Incompatible vector dimensions in pointNd::operator=" << std::endl;
			return *this;
		}

		for (int i1 = 0; i1 < dim; i1++) array[i1] = other.array[i1];
		return *this;
	}

// ****************************************************************************************

	inline pointNd<ValueType,dim> operator-() const {

		pointNd ret;
		for (int i1 = 0; i1 < dim; i1++) ret.array[i1] = -array[i1];
		return ret;
	}

// ****************************************************************************************

	inline pointNd<ValueType,dim> operator+(const pointNd& other) const {

		pointNd ret;
		for (int i1 = 0; i1 < dim; i1++) ret.array[i1] = array[i1]+other.array[i1];
		return ret;
	}

// ****************************************************************************************

	inline pointNd<ValueType,dim> operator-(const pointNd& other) const {

		pointNd ret;
		for (int i1 = 0; i1 < dim; i1++) ret.array[i1] = array[i1]-other.array[i1];
		return ret;
	}

// ****************************************************************************************

	inline void operator+=(const pointNd& other) {
		for (int i1 = 0; i1 < dim; i1++) array[i1] += other.array[i1];
	}

// ****************************************************************************************

	inline void operator-=(const pointNd& other) {
		for (int i1 = 0; i1 < dim; i1++) array[i1] -= other.array[i1];
	}

// ****************************************************************************************

	inline void operator*=(ValueType val) {
		for (int i1 = 0; i1 < dim; i1++) array[i1] *= val;
	}

// ****************************************************************************************

	inline void operator/=(ValueType val) {
		for (int i1 = 0; i1 < dim; i1++) array[i1] /= val;
	}

// ****************************************************************************************

	inline pointNd<ValueType,dim> operator*(ValueType val) const {

		pointNd ret;
		for (int i1 = 0; i1 < dim; i1++) ret.array[i1] = array[i1]*val;
		return ret;
	}

// ****************************************************************************************

	inline pointNd<ValueType,dim> operator/(ValueType val) const {

		pointNd ret;
		for (int i1 = 0; i1 < dim; i1++) ret.array[i1] = array[i1]/val;
		return ret;
	}

// ****************************************************************************************

	//! Skalarmultiplikation
	inline ValueType operator|(const pointNd& other) const {

		ValueType ret = 0;
		for (int i1 = 0; i1 < dim; i1++) ret += array[i1]*other.array[i1];
		return ret;
	}

// ****************************************************************************************

	//! Liefert nur dann true, wenn (this[0] == other[0]) && (this[1] == other[1]) && ...
	inline bool operator==(const pointNd& other) const {
		
		for (int i1 = 0; i1 < dim; i1++) 
			if (other.array[i1] != array[i1])
				return false;

		return true;
	}

// ****************************************************************************************

	inline ValueType norm() const { return squaredLength(); }

// ****************************************************************************************

	inline ValueType squaredLength() const {
		return (*this)|(*this);
	}

// ****************************************************************************************

	inline ValueType length() const {
		return sqrt(squaredLength());
	}

// ****************************************************************************************

	inline ValueType squareDist(const pointNd& other) const {

		pointNd tmp = other-(*this);

		return tmp.squaredLength();
	}

// ****************************************************************************************

	inline ValueType squaredDist(const pointNd& other) const {
		return squareDist(other);
	}

// ****************************************************************************************

	inline ValueType dist(const pointNd& other) const {
		pointNd<ValueType> dist = *this - other;
		return dist.length();
	}

// ****************************************************************************************

	~pointNd(void) {
	}

// ****************************************************************************************

	void print() const {
		std::cerr << "(";
		for (int i1 = 0; i1 < dim; i1++) {
			std::cerr << array[i1];
			if (i1 <= dim-1) std::cerr << " ";
		}
		std::cerr << ")" << std::endl;
	}

// ****************************************************************************************

	inline const ValueType& operator[](unsigned int i) const {
		if (i < dim)
			return array[i];
		else {
			std::cerr << "Out of bounds access to pointNd with dim=" << dim << std::endl;
			exit(EXIT_FAILURE);
		}
	}

// ****************************************************************************************

	inline ValueType& operator[](unsigned int i) {
		if (i < dim)
			return array[i];
		else {
			std::cerr << "Out of bounds access to pointNd with dim=" << dim << std::endl;
			exit(EXIT_FAILURE);
		}
	}

// ****************************************************************************************

	inline void normalize() {
		ValueType val = length();

		for (int i1 = 0; i1 < dim; i1++) array[i1] /= val;
	}

// ****************************************************************************************

	ValueType array[dim];

private:

};


//! write a pointNd to a stream
template <class ValueType, int dim> inline std::ostream& operator<<(std::ostream& s, const pointNd<ValueType,dim>& v)
{
	s << "(";
	for (int i1 = 0; i1 < dim; i1++) {
		s << v.array[i1];
		if (i1 <= dim-1) std::cerr << " ";
	}
	s << ")" << std::endl;

	return s;
}

//! read a pointNd from a stream
template <class ValueType, int dim> inline std::istream& operator>>(std::istream& s, pointNd<ValueType,dim>& v)
{
	for (int i1 = 0; i1 < dim; i1++) {
		s >> v.array[i1];
	}

	return s;
}


#endif
