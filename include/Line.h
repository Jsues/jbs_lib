#ifndef JBS_LINE_H
#define JBS_LINE_H

#include "point2d.h"


template <class T>
T sgn(T x){
	return (x > 0) ? T(1) : T(-1);
}

class Line {

public:

	Line(vec2i start, vec2i end) {
		m_start = start;
		m_end = end;
		init();
	};

	//! Sets 'm_current' to the start of the line and initializes the increments.
	void init() {
		vec2i diag = m_end-m_start;

		if (abs(diag.x) > abs(diag.y)) {
			increment.x = sgn((float)diag.x);
			numSteps = abs(diag.x);
			increment.y = (float)diag.y / (float)numSteps;
		} else {
			increment.y = sgn((float)diag.y);
			numSteps = abs(diag.y);
			increment.x = (float)diag.x / (float)numSteps;
		}

		end_line = false;
		counter = 0;
		m_current = m_start;
	}

	inline void operator++() {
		counter++;
		vec2f addon = increment*(float)counter;
		//vec2i addon_int = vec2i((int)(addon.x+0.5f), (int)(addon.y+0.5f));
		vec2i addon_int = vec2i((int)(addon.x), (int)(addon.y));
		m_current = m_start + addon_int;
		if (counter > numSteps) end_line = true;
	}

	inline bool is_end() {
		return end_line;
	}

	vec2i m_current;

private:

	UInt numSteps;
	UInt counter;
	bool end_line;
	vec2f increment;
	vec2i m_start;
	vec2i m_end;

};

#endif
