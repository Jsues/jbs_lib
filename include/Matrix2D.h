#ifndef MATRIX2D
#define MATRIX2D


#include <cmath>
#include <iostream>
#include <iomanip>

#include "point2d.h"
#include "point3d.h"

//! A simple 2D matrix
class Matrix2D {

public:

	//! Constructor: Creates a matrix from the values a_, b_, c_ and d_.
	Matrix2D(float a_, float b_, float c_, float d_) : a(a_), b(b_), c(c_), d(d_) {
		values_calculated = false;
		vectors_calculated = false;
	};

	//! Constructor: Creates a matrix with entries 0,0,0,0.
	Matrix2D() : a(0.0f), b(0.0f), c(0.0f), d(0.0f) {
		values_calculated = false;
		vectors_calculated = false;
	};

	//! Computes 'v'*M*'v'.
	float evalVector(vec2f v);

	//! Transposes the matrix.
	void transpose();

	//! Calculates the eigenvectors of the matrix.
	void calc_e_vectors();

	//! Matrixaddition.
	void add(Matrix2D &m2);

	void operator+=(const Matrix2D& other) {
		a += other.a;
		b += other.b;
		c += other.c;
		d += other.d;
	}

	void operator-=(const Matrix2D& other) {
		a -= other.a;
		b -= other.b;
		c -= other.c;
		d -= other.d;
	}

	//! Matrixmultiplication.
	void mult(Matrix2D &m2);

	//! Calculates the eigenvalues of the matrix.
	void calc_e_values();

	//! Returns the determinant of the matrix.
	float determinant();

	//! Inverts the matrix.
	void invert();

	//! Inverts the matrix, doesn't divide matrix by determinat.
	void invert_ignore_scale();

	//! Prints the elements of the matrix.
	void print() {
		//std::cerr.width(7);
		std::cerr << "/ "  << std::setprecision(5) << a << "   " << std::setprecision(5) << b << " \\" << std::endl; 
		std::cerr << "\\ " << std::setprecision(5) << c << "   " << std::setprecision(5) << d << " /" << std::endl; 
	};


	//! Returns the i-th (0 or 1) eigenvalue.
	float getEValue(int i) {
		if (!values_calculated)
			calc_e_values();

		if (i == 0)
			return lambda_0;
		else
			return lambda_1;
	};

	//! Returns the i-th (0 or 1) eigenvector.
	vec2f getEV(int i) {
		if (!vectors_calculated)
			calc_e_vectors();

		if (i == 0)
			return ev0;
		else
			return ev1;
	};

	//! Multiplies M*v.
	vec2f vecTrans(vec2f v) {
		return vec2f(a*v.x+b*v.y, c*v.x+d*v.y);
	};

	//! The entries of the matrix.
	float a, b, c, d;

	//! eigenvalues of the matrix,
	float lambda_0, lambda_1;

	//! eigenvectors of the matrix,
	vec2f ev0, ev1;

private:

	//! Set to true when the eigenvalues have been calculated.
	bool values_calculated;

	//! Set to true when the eigenvectors have been calculated.
	bool vectors_calculated;

};

#endif
