#ifndef TRIANGLE_INTERSECT_H
#define TRIANGLE_INTERSECT_H


#include "point3d.h"

//! Triangle class for triangle/triangle intersection
/*!
Implementation of the paper
<b>A Fast Triangle-Triangle Intersection Test</b>
by Tomas M�ller
*/
template <class T>
class TriangleIntersect {


public:

	TriangleIntersect(T v0_, T v1_, T v2_) : v0(v0_), v1(v1_), v2(v2_) {
		normal = ((v0-v1)^(v0-v2));
		normal.normalize();

		d = -(normal|v0);
	}

	T barycentric_with_proj(T& p) {
		// Project
		p = p-normal*(normal|(p-v0));

		T t2 = ((v0-v1)^(v0-p));
		double A2 = t2.length();
		T t1 = ((v0-p)^(v0-v2));
		double A1 = t1.length();
		T t0 = ((p-v1)^(p-v2));
		double A0 = t0.length();

		double sum = A0+A1+A2;

		if ((t2|normal) < 0) A2 *= -1;
		if ((t1|normal) < 0) A1 *= -1;
		if ((t0|normal) < 0) A0 *= -1;

		return T(A0, A1, A2)/sum;
	}

	//! Intersect the triangle with another triangle 'tri2'.
	/*!
		\return whether there was an intersection
		\param tri2 the other triangle
		\param p0 start-point of the intersection line
		\param p1 start-point of the intersection line
	*/
	bool intersects(TriangleIntersect<T> tri2, T& p0, T& p1) {

		// Compute distance of all points in 'tri2' to this plane:
		typename T::ValueType d_1_0 = (normal|tri2.v0)+d;
		typename T::ValueType d_1_1 = (normal|tri2.v1)+d;
		typename T::ValueType d_1_2 = (normal|tri2.v2)+d;
		// Compute distance of all points of this triangle to the plane of 'tri2':
		typename T::ValueType d_0_0 = (tri2.normal|v0)+tri2.d;
		typename T::ValueType d_0_1 = (tri2.normal|v1)+tri2.d;
		typename T::ValueType d_0_2 = (tri2.normal|v2)+tri2.d;


		if (d_0_0 == 0 && d_0_1 == 0 && d_0_2 == 0) {
			// Triangles on one plane!
			return false;
			std::cerr << "TODO Implement TriangleIntersect intersects for coplanar triangles!!" << std::endl;
			exit(EXIT_FAILURE);
		}


		if ( (d_0_0 > 0 && d_0_1 > 0 && d_0_2 > 0) || (d_0_0 < 0 && d_0_1 < 0 && d_0_2 < 0) )
			return false;
		if ( (d_1_0 > 0 && d_1_1 > 0 && d_1_2 > 0) || (d_1_0 < 0 && d_1_1 < 0 && d_1_2 < 0) )
			return false;


		// sort points in this triangles such that v0 lies on the other side of the plane then v1 and v2:
		if ((d_0_0 * d_0_1) > 0) { // v0 and v1 have the same sign, i.e. they lie on the same side of the plane! Ergo: v2 must lie on the other side
			// Swap v0 and v2:
			T temp = v2;
			v2 = v0;
			v0 = temp;
            typename T::ValueType temp2 = d_0_2;
			d_0_2 = d_0_0;
			d_0_0 = temp2;
		} else if ((d_0_0 * d_0_2) > 0) { // v0 and v2 have the same sign, i.e. they lie on the same side of the plane! Ergo: v1 must lie on the other side
			// Swap v0 and v1:
			T temp = v1;
			v1 = v0;
			v0 = temp;
            typename T::ValueType temp2 = d_0_1;
			d_0_1 = d_0_0;
			d_0_0 = temp2;
		}
		// sort points in the other triangles such that tri2.v0 lies on the other side of the plane then tri2.v1 and tri2.v2:
		if ((d_1_0 * d_1_1) > 0) { // tri2.v0 and tri2.v1 have the same sign, i.e. they lie on the same side of the plane! Ergo: tri2.v2 must lie on the other side
			// Swap v0 and v2:
			T temp = tri2.v2;
			tri2.v2 = tri2.v0;
			tri2.v0 = temp;
            typename T::ValueType temp2 = d_1_2;
			d_1_2 = d_1_0;
			d_1_0 = temp2;
		} else if ((d_1_0 * d_1_2) > 0) { // tri2.v0 and tri2.v2 have the same sign, i.e. they lie on the same side of the plane! Ergo: tri2.v1 must lie on the other side
			// Swap v0 and v1:
			T temp = tri2.v1;
			tri2.v1 = tri2.v0;
			tri2.v0 = temp;
            typename T::ValueType temp2 = d_1_1;
			d_1_1 = d_1_0;
			d_1_0 = temp2;
		}


		//std::cerr << "tri1: " << d_0_0 << " " << d_0_1 << " " << d_0_2 << std::endl;
		//std::cerr << "tri2: " << d_1_0 << " " << d_1_1 << " " << d_1_2 << std::endl;

		// the intersection line runs through point 'O':
		T O;
		O.z = 0;
		O.x = (normal.y*tri2.d - tri2.normal.y*d)/(tri2.normal.y*normal.x - normal.y*tri2.normal.x);
		O.y = (-d - normal.x*O.x)/normal.y;
		//O.print();



		// intersection line direction:
		T D = normal^tri2.normal;
		D.normalize();
		typename T::ValueType p_v_0_0 = D|(v0-O);
		typename T::ValueType p_v_0_1 = D|(v1-O);
		typename T::ValueType p_v_0_2 = D|(v2-O);
		typename T::ValueType p_v_1_0 = D|(tri2.v0-O);
		typename T::ValueType p_v_1_1 = D|(tri2.v1-O);
		typename T::ValueType p_v_1_2 = D|(tri2.v2-O);

		// interval of triangle1:
		typename T::ValueType t_0_1 = p_v_0_0 + (p_v_0_1 - p_v_0_0) * ((d_0_0)/(d_0_0-d_0_1));
		typename T::ValueType t_0_2 = p_v_0_0 + (p_v_0_2 - p_v_0_0) * ((d_0_0)/(d_0_0-d_0_2));
		//sort
		if (t_0_1 > t_0_2) {
			typename T::ValueType tmp = t_0_2;
			t_0_2 = t_0_1;
			t_0_1 = tmp;
		}
		//std::cerr << "interval triangle1: [" << t_0_1 << " : " << t_0_2 << "]" << std::endl;
		// interval of triangle2:
		typename T::ValueType t_1_1 = p_v_1_0 + (p_v_1_1 - p_v_1_0) * ((d_1_0)/(d_1_0-d_1_1));
		typename T::ValueType t_1_2 = p_v_1_0 + (p_v_1_2 - p_v_1_0) * ((d_1_0)/(d_1_0-d_1_2));
		//sort
		if (t_1_1 > t_1_2) {
			typename T::ValueType tmp = t_1_2;
			t_1_2 = t_1_1;
			t_1_1 = tmp;
		}
		//std::cerr << "interval triangle2: [" << t_1_1 << " : " << t_1_2 << "]" << std::endl;

		typename T::ValueType interval_start = (t_0_1 > t_1_1) ? t_0_1 : t_1_1;
		typename T::ValueType interval_end = (t_0_2 < t_1_2) ? t_0_2 : t_1_2;

		//std::cerr << "real interval: [" << interval_start << " : " << interval_end << "]" << std::endl;

		if ((t_1_2 < t_0_1))
			return false;

		if (interval_start > interval_end)
			return false;

		p0 = O+D*interval_start;
		p1 = O+D*interval_end;
		//p0.print();
		//p1.print();


		return true;
	}

	void print() {
		v0.print();
		v1.print();
		v2.print();
	}

	//! Normal in normal equation "'normal'|x + d";
	T normal;

	//! distance to origin in normal equation "'normal'|x + d";
	typename T::ValueType d;

	//! Vertex 1
	T v0;
	//! Vertex 2
	T v1;
	//! Vertex 3
	T v2;

private:


};

#endif
