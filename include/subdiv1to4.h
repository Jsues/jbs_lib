#ifndef JBS_SUBDIV_1_TO_4_H
#define JBS_SUBDIV_1_TO_4_H


#include "JBS_General.h"
#include "BSplinePatch.h"
#include "SimpleMesh.h"

#include <map>


int get_key(int v0, int v1, int numV) {

	int key;

	(v0 < v1) ? key = v0*numV + v1 : key = v1*numV + v0;

	return key;
}


//! Subdivides the mesh 'm_mesh' once and returns the new finer mesh.
SimpleMesh* Subdiv1to4(SimpleMesh* m_mesh) {
	SimpleMesh* ret_mesh = new SimpleMesh();


	int numV = m_mesh->getNumV();
	// Stores the index 'i' of the new vertex on the edge ('x', 'y') under the key 'k' = 'x'*numV+y;
	std::map<int, int> lookup;	

	// Firstly, insert all old vertices into the new mesh:
	for (int i1 = 0; i1 < numV; i1++) {
		ret_mesh->insertVertex(m_mesh->vList[i1].c[0], m_mesh->vList[i1].c[1], m_mesh->vList[i1].c[2]);
	}

	// Now insert new triangles, plus insert the new points where needed
	for (UInt i1 = 0; i1 < (UInt)m_mesh->getNumT(); i1++) {
		Triangle* t = m_mesh->tList[i1];

		// Old vertices
		int v0 = t->v0();
		int v1 = t->v1();
		int v2 = t->v2();

		int key_v01 = get_key(v0, v1, numV);
		int key_v12 = get_key(v1, v2, numV);
		int key_v20 = get_key(v2, v0, numV);

		if (lookup.find(key_v01) == lookup.end()) {
			vec3d pt = (m_mesh->vList[v0].c + m_mesh->vList[v1].c)/2.0;
			ret_mesh->insertVertex(pt[0], pt[1], pt[2]);
			lookup[key_v01] = ret_mesh->getNumV()-1;
		}
		if (lookup.find(key_v12) == lookup.end()) {
			vec3d pt = (m_mesh->vList[v1].c + m_mesh->vList[v2].c)/2.0;
			ret_mesh->insertVertex(pt[0], pt[1], pt[2]);
			lookup[key_v12] = ret_mesh->getNumV()-1;
		}
		if (lookup.find(key_v20) == lookup.end()) {
			vec3d pt = (m_mesh->vList[v2].c + m_mesh->vList[v0].c)/2.0;
			ret_mesh->insertVertex(pt[0], pt[1], pt[2]);
			lookup[key_v20] = ret_mesh->getNumV()-1;
		}

		int v01 = lookup[key_v01];
		int v12 = lookup[key_v12];
		int v20 = lookup[key_v20];

		//std::cerr << v01 << " " << v12 << " " << v20 << std::endl;
		//std::cerr << key_v01 << " " << key_v12 << " " << key_v20 << std::endl;

		// Insert the 4 triangles
		ret_mesh->insertTriangle(v0, v01, v20);
		ret_mesh->insertTriangle(v01, v12, v20);
		ret_mesh->insertTriangle(v01, v1, v12);
		ret_mesh->insertTriangle(v12, v2, v20);
	}

	//delete m_mesh;

	return ret_mesh;
}


#endif //JBS_SUBDIV_1_TO_4_H

