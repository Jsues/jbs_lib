#ifndef HSV_2_RGB
#define HSV_2_RGB

#include "point3d.h"

namespace JBSlib {

	template <class T>
	point3d<T> hsv2rgb(point3d<T> val) {
		return hsv2rgb(val.x, val.y, val.z);
	}

	template <class T>
	point3d<T> hsv2rgb(T h, T s, T v) {
		int idx = (int)(h*6);
		idx = idx%6;

		T f = (h*6)-idx;
		T p = v*(1-s);
		T q = v*(1-f*s);
		T t = v*(1-(1-f)*s);

		if (idx == 0) return point3d<T>(v,t,p);
		else if (idx == 1) return point3d<T>(q,v,p);
		else if (idx == 2) return point3d<T>(p,v,t);
		else if (idx == 3) return point3d<T>(p,q,v);
		else if (idx == 4) return point3d<T>(t,p,v);
		else  return point3d<T>(v,p,q);
	}

};

#endif

