#ifndef BUILD_DEFORMATION_GRAPH_H
#define BUILD_DEFORMATION_GRAPH_H

#include <map>

#include "DeformationGraph.h"
#include "PointCloud.h"
#include "RNG.h"
#include "ANN/ANN.h"

template <class T>
DeformationGraph* defGraphFromPC(PointCloudNormals<T>* pc, float distance) {
	DeformationGraph* dg = new DeformationGraph;

	RNG randnum;

	UInt numPts = pc->getNumPts();
	dg->dataPC = pc;
	dg->nw = new nodeWeight[numPts];


	// Build center of mass:
	vec3f com;
	for (UInt i1 = 0; i1 < numPts; i1++) {
		T pt = pc->getPoints()[i1];
		com += pt;
	}
	com /= (float)numPts;
	dg->com = com;

	int* flag = new int[numPts];

	std::map<UInt, UInt> list;

	ANNpointArray dataPts = annAllocPts(numPts, 3);
	for (UInt i1 = 0; i1 < numPts; i1++) {
		flag[i1] = 1; // initially set all points to be contained
		T pt = pc->getPoints()[i1];
		dataPts[i1][0] = pt[0]; dataPts[i1][1] = pt[1]; dataPts[i1][2] = pt[2]; 
		double tmp = randnum.uniform(0.0, 100000000.0);
		UInt tmp_i = (UInt)tmp;
		list.insert(std::pair<UInt, UInt>(tmp_i, i1));
	}
	ANNkd_tree* kdTree = new ANNkd_tree(dataPts, numPts, 3);

	ANNpoint queryPt = annAllocPt(3);
	// Allocate arrays for NN-Search:
	ANNidxArray nnIdx = new ANNidx[numPts];							// allocate near neigh indices
	ANNdistArray dists = new ANNdist[numPts];						// allocate near neighbor dists

	float rad_square = distance*distance;

	for (std::map<UInt, UInt>::const_iterator it = list.begin(); it != list.end(); it++) {
		UInt id = it->second;

		if (flag[id] == 0) // point is already canceled, skip
			continue;

		T pt = pc->getPoints()[id];
		queryPt[0] = pt[0]; queryPt[1] = pt[1]; queryPt[2] = pt[2];
		int num_pts = kdTree->annkFRSearch(queryPt, rad_square, 0);		// Get number of points within radius
		kdTree->annkFRSearch(queryPt, rad_square, num_pts, nnIdx, dists);

		for (UInt i2 = 1; i2 < (UInt)num_pts; i2++) {
			flag[nnIdx[i2]] = 0;
		}
	}

	for (UInt i1 = 0; i1 < numPts; i1++) {
		if (flag[i1] == 1) {
			dg->insertNode(pc->getPoints()[i1]);
		}
	}

	// Build kd_Tree of deformation graph nodes:
	UInt numDGNodes = dg->getNumNodes();
	ANNpointArray dataPtsDG = annAllocPts(numDGNodes, 3);
	for (UInt i1 = 0; i1 < numDGNodes; i1++) {
		T pt = dg->nodes[i1].initial_position;
		dataPtsDG[i1][0] = pt[0]; dataPtsDG[i1][1] = pt[1]; dataPtsDG[i1][2] = pt[2]; 
	}
	ANNkd_tree* kdTreeDG = new ANNkd_tree(dataPtsDG, numDGNodes, 3);


	ANNidxArray nnIdxDG = new ANNidx[numNeighbors+1];							// allocate near neigh indices
	ANNdistArray distsDG = new ANNdist[numNeighbors+1];						// allocate near neighbor dists

	for (UInt i1 = 0; i1 < numPts; i1++) {
		T pt = pc->getPoints()[i1];
		queryPt[0] = pt[0]; queryPt[1] = pt[1]; queryPt[2] = pt[2];
		
		kdTreeDG->annkSearch(queryPt, numNeighbors+1, nnIdxDG, distsDG, 0);

		float d_max = sqrt((float)distsDG[numNeighbors]);
		float sum_weights = 0.0f;

		for (UInt x = 0; x < numNeighbors; x++) {
			int v0 = nnIdxDG[x];

			dg->nw[i1].nodeId[x] = v0;
			float weight = 1 - sqrt((float)distsDG[x])/d_max;
			sum_weights += weight;
			dg->nw[i1].weight[x] = weight;


			for (UInt y = x; y < numNeighbors; y++) {
				int v1 = nnIdxDG[y];

				if (!(dg->hasEdge(v0, v1))) {
					dg->insertEdge(v0, v1);
				}
			}
		}

		// Normalize weights
		for (UInt x = 0; x < numNeighbors; x++) {
			dg->nw[i1].weight[x] /= sum_weights;
		}

	}


	return dg;
}

#endif
