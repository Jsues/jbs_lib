#ifndef JBS_SPARSE_MATRIX
#define JBS_SPARSE_MATRIX

#include <map>
#include <iostream>

#include "CompColSparseMatrix.h"
#include "CompRowSparseMatrix.h"


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

class PairentryRow {

public:
	PairentryRow(int x_, int y_) { x = x_; y = y_; }

	bool operator< (const PairentryRow& other) const {
		if (y == other.y) {
			return (x < other.x);
		}
		return (y < other.y);
	}

	int x;
	int y;
};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

class PairentryCol {

public:
	PairentryCol(int x_, int y_) { x = x_; y = y_; }

	bool operator< (const PairentryCol& other) const {
		if (x == other.x) {
			return (y < other.y);
		}
		return (x < other.x);
	}

	int x;
	int y;
};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


//! Abstract baseclass for converting between CompCol and CompRow matrices. Don't use this, use the appropriate derived class instead.
template <class T, class U>
class SparseMatrix {

public:
	//! Type of Templates components.
	typedef T SortType; 

	//! Floating point precision
	typedef U ValueType;

	//! Constructor.
	SparseMatrix(int dim_) { dim = dim_; };

	//! Destructor.
	virtual ~SparseMatrix() {};

	//! Inserts element 'val' at (x,y).
	void insert(int pos_x, int pos_y, ValueType val) {
		std::pair<SortType, ValueType> the_pair(SortType(pos_x, pos_y), val);
		entries.insert(the_pair);
	}

	//! Adds 'val' to entry (x,y).
	void add(int pos_x, int pos_y, ValueType val) {
		SortType key(pos_x, pos_y);
		//if (entries.find(key) != entries.end())
			entries[key] += val;
		//else
		//	std::cerr << "unsecure access to std::map" << std::endl;
	}

	void printAllEntries() {

		for (typename std::map<SortType, ValueType>::iterator it = this->entries.begin(); it != this->entries.end(); it++) {
			std::cerr << "(" << it->first.x << ", " << it->first.y << " -> " << it->second << ")" << std::endl;
		}

	}
    
	int dim;

	void freeMemory() {
		entries.clear();
		//entries = std::map<SortType, ValueType>;
	}


	//long getMemoryRequiredInMB() const {
	//	long mem_req = sizeof(int);
	//	mem_req += 2*(long)entries.size()*sizeof(int);
	//	mem_req += (long)entries.size()*sizeof(ValueType);

	//	mem_req /= (1024*1024);
	//	return mem_req;
	//}

protected:

	std::map<SortType, ValueType> entries;

};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


//! Use this matrix container to convert a matrix to a Compressed Column Sparse Matrix
template <class T>
class SparseMatrixCol : public SparseMatrix<PairentryCol, T> {

public:

	//! Floating point precision
	typedef T ValueType;

	typedef PairentryCol SortType;


	// Constructor, create a Compressed <b>Column</b> Sparse Matrix from a Compressed <b>Row</b> Sparse Matrix
	SparseMatrixCol(int dim_, CompRowSparseMatrix* crsm) : SparseMatrix<PairentryCol, ValueType>(dim_) {

		// Copy entries:
		int current_col_ptr_pos = 0;
		int current_line = 0;
		for (unsigned int i1 = 0; i1 < crsm->entries.size(); i1++) {
			ValueType val = (ValueType)crsm->entries[i1];
			int x = crsm->row_ind[i1];

			if ((int)i1 >= crsm->col_ptr[current_col_ptr_pos+1]) {
				current_col_ptr_pos++;
				current_line++;
			}
			int y = current_line;
			insert(x,y, val);
		}

	};

	// Constructor.
	SparseMatrixCol(int dim_) : SparseMatrix<PairentryCol, ValueType>(dim_) {};

	void getCompColSparseMatrix(CompColSparseMatrix& ccsm) {
		
		ccsm.dim = this->dim;
		ccsm.entries.reserve(this->entries.size());
		ccsm.col_ind.reserve(this->entries.size());

		int row_ptr = 0;
		int ind = 0;
		ccsm.row_ptr.push_back(row_ptr);

		for (typename std::map<SortType, ValueType>::iterator it = this->entries.begin(); it != this->entries.end(); it++) {
			
			ccsm.entries.push_back(it->second);
			ccsm.col_ind.push_back(it->first.y);

			if (it->first.x > row_ptr) {
				row_ptr++;
				ccsm.row_ptr.push_back(ind);
			}
			ind++;
		}

		ccsm.row_ptr.push_back(ind);

	};

	void getCompColSparseMatrixLowerTriangular(CompColSparseMatrix& ccsm) {
		
		ccsm.dim = this->dim;
		ccsm.entries.reserve(this->entries.size());
		ccsm.col_ind.reserve(this->entries.size());

		int row_ptr = 0;
		int ind = 0;
		ccsm.row_ptr.push_back(row_ptr);

		for (typename std::map<SortType, ValueType>::iterator it = this->entries.begin(); it != this->entries.end(); it++) {
			
			// Skip all entries in the upper half
			if(it->first.x > it->first.y) continue;

			ccsm.entries.push_back(it->second);
			ccsm.col_ind.push_back(it->first.y);

			if (it->first.x > row_ptr) {
				row_ptr++;
				ccsm.row_ptr.push_back(ind);
			}
			ind++;
		}

		ccsm.entries.resize(ind);
		ccsm.col_ind.resize(ind);

		ccsm.row_ptr.push_back(ind);

	};

};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


//! Use this matrix container to convert a matrix to a Compressed Row Sparse Matrix
template <class T>
class SparseMatrixRow : public SparseMatrix<PairentryRow, T> {

public:

	//! Floating point precision
	typedef T ValueType;

	typedef PairentryRow SortType;

	SparseMatrixRow(int dim_) : SparseMatrix<PairentryRow, ValueType>(dim_) {}

	void getCompRowSparseMatrix(CompRowSparseMatrix& crsm) {

		crsm.dim = this->dim;
		crsm.entries.reserve(this->entries.size());
		crsm.row_ind.reserve(this->entries.size());

		int col_ptr = 0;
		int ind = 0;
		crsm.col_ptr.push_back(col_ptr);

		for (typename std::map<SortType, ValueType>::iterator it = this->entries.begin(); it != this->entries.end(); it++) {
			
			crsm.entries.push_back(it->second);
			crsm.row_ind.push_back(it->first.x);

			if (it->first.y > col_ptr) {
				col_ptr++;
				crsm.col_ptr.push_back(ind);
			}
			ind++;
		}

		crsm.col_ptr.push_back(ind);
	}

};


#endif
