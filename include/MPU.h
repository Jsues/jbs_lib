#ifndef MPU_OCTREE_H
#define MPU_OCTREE_H

//#pragma warning (disable:4311)

#include <vector>
#include <list>
#include <limits>

#include "point2d.h"
#include "point3d.h"
#include "point4d.h"
#include "JBS_General.h"
#include "QuadricFunction.h"
#include "ScalarFunction.h"

#include "ANN/ANN.h"

#define SPLIT_NODES_WITH_MAAAAANY_POINTS
#define NODE_SPLIT_NUM 200

#define STORE_CELLS_IN_TREE


namespace JBSlib {

// ****************************************************************************************

template <class T> class MPUDFOcTree;

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


//! Cell of a n-D Octree.
template <class T>
class MPUDFOcNode {

public:

// ****************************************************************************************

	typedef T Point;
	typedef typename MPUDFOcNode<Point> OcNodeType;
	typedef typename T::ValueType ValueType;
	static const UInt dim = Point::dim;
	typedef QuadricFunction<Point> QuadricFunctionType;
	//! Number of children = 2_tothepowerof_dim (8 for 3D, 16 for 4D...).
	static const UInt numChildren = 1 << dim;
	//! Maximum octree depth
	static const UInt maxOctreeDepth = 15;

	//! Allow MPUDFOcTree to access our private members.
	friend class MPUDFOcTree<OcNodeType>;
	//! Allow QuadricFactory to access our private members.
	friend class QuadricFactory<Point>;
	//! Allow QuadricFunction to access our private members.
	friend class QuadricFunction<Point>;

// ****************************************************************************************

	MPUDFOcNode(MPUDFOcTree< OcNodeType >* tree_, Point min_, Point max_, ValueType search_diag_, OcNodeType* father_ = NULL): tree(tree_), min_ext(min_), max_ext(max_), search_diag(search_diag_), search_diag_square(search_diag_*search_diag_), m_isLeafNode(true), father(father_) {
		if (father == NULL) nodelevel = 0;
		else nodelevel = father->getNodeLevel()+1;


		//std::cerr << "(" << min_ << ")  (" << max_ << ")" << std::endl;

		qf = NULL;
		num_points_in_ball = 0;

		center = (min_ext+max_ext)/2;

#ifdef STORE_CELLS_IN_TREE
		tree->cells.push_back(this);
#endif

		tree->numCells++;
	}

// ****************************************************************************************

	~MPUDFOcNode() {
		if (!m_isLeafNode) { // we have kids, kill them!
			for (UInt i1 = 0; i1 < numChildren; i1++) {
				delete children[i1];
			}
			delete[] children;
		}

		if (qf != NULL) delete qf;
	}

// ****************************************************************************************

	inline UInt getNumOfPointsInInitialBall() const {
		return num_points_in_ball;
	}

// ****************************************************************************************

	inline ValueType getWeightForPoint(const Point& pos) const {
		ValueType t = (pos.dist(center)/search_diag) * (ValueType)1.5;

		if (t < 0.5)
			return (ValueType)0.75 - t*t;
		else
			return (ValueType)0.5*((ValueType)1.5-t)*((ValueType)1.5-t);
	}

// ****************************************************************************************

	void MPUapproxSecDeriv(const Point& pos) {
		// Cell does not contribute to value at 'pos'
		//if ((pos-center).squaredLength() > search_diag_square) { return; }
		if (pos.squareDist(center) > search_diag_square) { return; }

		// Early traversal
		if (!isLeafNode()) {
            for (UInt i1 = 0; i1 < numChildren; i1++)
				children[i1]->MPUapproxSecDeriv(pos);
			return;
		}

		// Build quadric
		if (qf == NULL) {
			if (content.size() < NODE_SPLIT_NUM) {
				qf = tree->quadricFactory->getQuadricForCell(this);
				error = qf->getError();
			} else {
				error = 10000000;
			}
		}

		//std::cerr << "nodelevel: " << nodelevel << " , error: " << error << " , max_error: " << tree->max_error << std::endl;

		if ((error > tree->max_error) && (nodelevel <= maxOctreeDepth)) {

			if (qf != NULL) {
				delete qf;
				qf = NULL;
			}

			splitAndPropagate();

            for (UInt i1 = 0; i1 < numChildren; i1++)
				children[i1]->MPUapproxSecDeriv(pos);

		} else {

			//std::cerr << "else" << std::endl;
			ValueType weight = getWeightForPoint(pos);
			tree->SwSecDeriv += qf->evaluateHessian(pos)*weight;
			tree->Sw  += weight;
		}
	}

// ****************************************************************************************

	void MPUapproxGrad(const Point& pos) {
		// Cell does not contribute to value at 'pos'
		//if ((pos-center).squaredLength() > search_diag_square) { return; }
		if (pos.squareDist(center) > search_diag_square) { return; }

		// Early traversal
		if (!isLeafNode()) {
            for (UInt i1 = 0; i1 < numChildren; i1++)
				children[i1]->MPUapproxGrad(pos);
			return;
		}

		// Build quadric
		if (qf == NULL) {
			if (content.size() < NODE_SPLIT_NUM) {
				qf = tree->quadricFactory->getQuadricForCell(this);
				error = qf->getError();
			} else {
				error = 1000;
			}
		}

		//std::cerr << "nodelevel: " << nodelevel << " , error: " << error << " , max_error: " << tree->max_error << std::endl;

		if ((error > tree->max_error) && (nodelevel <= maxOctreeDepth)) {

			if (qf != NULL) {
				delete qf;
				qf = NULL;
			}

			splitAndPropagate();

            for (UInt i1 = 0; i1 < numChildren; i1++)
				children[i1]->MPUapproxGrad(pos);

		} else {

			//std::cerr << "else" << std::endl;
			ValueType weight = getWeightForPoint(pos);
			tree->SwG += qf->evaluateGradient(pos)*weight;
			tree->Sw  += weight;
		}
	}

// ****************************************************************************************

	void MPUapprox(const Point& pos) {

		// Cell does not contribute to value at 'pos'
		//if ((pos-center).squaredLength() > search_diag_square) { return; }
		if (pos.squareDist(center) > search_diag_square) { return; }

		// Early traversal
		if (!isLeafNode()) {
            for (UInt i1 = 0; i1 < numChildren; i1++)
				children[i1]->MPUapprox(pos);
			return;
		}

		// Build quadric
		if (qf == NULL) {
			if (content.size() < NODE_SPLIT_NUM) {
				qf = tree->quadricFactory->getQuadricForCell(this);
				error = qf->getError();
			} else {
				error = 1000;
			}
		}

		//std::cerr << "nodelevel: " << nodelevel << " , error: " << error << " , max_error: " << tree->max_error << std::endl;

		if ((error > tree->max_error) && (nodelevel <= maxOctreeDepth)) {

			if (qf != NULL) {
				delete qf;
				qf = NULL;
			}

			splitAndPropagate();

            for (UInt i1 = 0; i1 < numChildren; i1++)
				children[i1]->MPUapprox(pos);

		} else {

			//std::cerr << "else" << std::endl;
			ValueType weight = getWeightForPoint(pos);
			tree->SwQ += weight * qf->evaluate(pos);
			tree->Sw  += weight;
		}

	}

// ****************************************************************************************

	//! Prints the content of the current node.
	inline void printContent() const {
		std::cerr << "Node: " << std::endl;
		for (UInt i1 = 0; i1 < content.size(); i1++) {
			content[i1]->print();
		}
		std::cerr << "------------------" << std::endl;
	}

// ****************************************************************************************

	//! Returns true if the node does not contain any further kids.
	inline bool isLeafNode() const {
		return m_isLeafNode;
	}
	
// ****************************************************************************************

	//! Returns the number of triangles in this node.
	inline UInt getNumObjects() const {
		return (UInt)content.size();
	}


// ****************************************************************************************

	//! Returns the min corner of the cell.
	Point getMin() const {
		return min_ext;
	}

// ****************************************************************************************

	//! Returns the min corner of the cell.
	Point getMax() const {
		return max_ext;
	}

// ****************************************************************************************

	UInt getNodeLevel() const {
		return nodelevel;
	}

// ****************************************************************************************

	inline bool PointisIn(const Point& pt) const {
		if (pt > min_ext && pt <= max_ext)
			return true;
		else
			return false;
	}

// ****************************************************************************************

	inline bool needSplit() const {
		//return (getNumObjects() >= tree->getMaxNumObjectsPerNode());
		return false;
	}

// ****************************************************************************************

	inline void splitAndPropagate() {
		// split node;
		splitNode();

		// distribute old objects to kids:
		for (UInt i1 = 0; i1 < content.size(); i1++) {
			propagateToChildren(content[i1]);
		}

		content.clear();
	}

// ****************************************************************************************

	inline const MPUDFOcNode<Point>* getCellContainingPoint(const Point& pt) const {
		if (m_isLeafNode)
			return this;
		else
			return children[getChildIndexContaining(pt)]->getCellContainingPoint(pt);
	}

// ****************************************************************************************

	//! Inserts an object into this node.
	inline void insertObject(UInt obj_pos) {
		content.push_back(obj_pos);
	}

// ****************************************************************************************

	//! Returns a corner of the cell.
	inline Point getCorner(UInt idx) const {
		Point v;
		for (UInt i = 0; i < dim; ++i) {
			v[i] = (idx & (1 << i)) ? max_ext[i] : min_ext[i];
		}
		return v;
	}

// ****************************************************************************************

	inline const QuadricFunctionType* getQuadricFunction() const {
		return qf;
	}

// ****************************************************************************************

	inline Point getCenter() const {
		return center;
	}

protected:

// ****************************************************************************************

	inline Point getInterpolatedPos(UInt x, UInt y) const {
		Point p;
		p[0] = (min_ext.x*(2-x) + max_ext.x*(x)) / ValueType(2.0);
		p[1] = (min_ext.y*(2-y) + max_ext.y*(y)) / ValueType(2.0);
		return p;
	}

// ****************************************************************************************

	inline Point getInterpolatedPos(UInt x, UInt y, UInt z) const {
		Point p;
		p[0] = (min_ext.x*(2-x) + max_ext.x*(x)) / ValueType(2.0);
		p[1] = (min_ext.y*(2-y) + max_ext.y*(y)) / ValueType(2.0);
		p[2] = (min_ext.z*(2-z) + max_ext.z*(z)) / ValueType(2.0);
		return p;
	}

// ****************************************************************************************

	inline Point getInterpolatedPos(UInt x, UInt y, UInt z, UInt w) const {
		Point p;
		p[0] = (min_ext.x*(2-x) + max_ext.x*(x)) / ValueType(2.0);
		p[1] = (min_ext.y*(2-y) + max_ext.y*(y)) / ValueType(2.0);
		p[2] = (min_ext.z*(2-z) + max_ext.z*(z)) / ValueType(2.0);
		p[3] = (min_ext.w*(2-w) + max_ext.w*(w)) / ValueType(2.0);
		return p;
	}

// ****************************************************************************************

	void propagateToChildren(UInt obj_pos) {
		const Point& obj = tree->objects[obj_pos];

		children[getChildIndexContaining(obj)]->insertObject(obj_pos);
	};
	
// ****************************************************************************************

	inline UInt getChildIndexContaining(const Point& obj) const;

// ****************************************************************************************

	inline UInt getChildIndexContaining2D(const Point& obj) const {
		int xx = (obj[0] > center[0]); int yy = (obj[1] > center[1]);
		return getPosInChildrenArray(xx,yy);
	}

// ****************************************************************************************

	inline UInt getChildIndexContaining3D(const Point& obj) const {
		int xx = (obj[0] > center[0]); int yy = (obj[1] > center[1]); int zz = (obj[2] > center[2]);
		return getPosInChildrenArray(xx,yy,zz);
	}

// ****************************************************************************************

	inline UInt getChildIndexContaining4D(const Point& obj) const {
		int xx = (obj[0] > center[0]); int yy = (obj[1] > center[1]); int zz = (obj[2] > center[2]); int ww = (obj[3] > center[3]);
		return getPosInChildrenArray(xx,yy,zz,ww);
	}

// ****************************************************************************************

	void splitNode();

// ****************************************************************************************

	void splitNode2D() {
		ValueType child_diag = search_diag / 2;
		children = new MPUDFOcNode<Point>*[numChildren];
		children[getPosInChildrenArray(0,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,0), getInterpolatedPos(1,1), child_diag, this);
		children[getPosInChildrenArray(1,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,0), getInterpolatedPos(2,1), child_diag, this);
		children[getPosInChildrenArray(0,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,1), getInterpolatedPos(1,2), child_diag, this);
		children[getPosInChildrenArray(1,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,1), getInterpolatedPos(2,2), child_diag, this);
		// We're not longer leaf-node:
		m_isLeafNode = false;
	}

// ****************************************************************************************

	void splitNode3D() {
		ValueType child_diag = search_diag / 2;
		children = new MPUDFOcNode<Point>*[numChildren];
		children[getPosInChildrenArray(0,0,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,0,0), getInterpolatedPos(1,1,1), child_diag, this);
		children[getPosInChildrenArray(1,0,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,0,0), getInterpolatedPos(2,1,1), child_diag, this);
		children[getPosInChildrenArray(0,1,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,1,0), getInterpolatedPos(1,2,1), child_diag, this);
		children[getPosInChildrenArray(1,1,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,1,0), getInterpolatedPos(2,2,1), child_diag, this);
		children[getPosInChildrenArray(0,0,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,0,1), getInterpolatedPos(1,1,2), child_diag, this);
		children[getPosInChildrenArray(1,0,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,0,1), getInterpolatedPos(2,1,2), child_diag, this);
		children[getPosInChildrenArray(0,1,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,1,1), getInterpolatedPos(1,2,2), child_diag, this);
		children[getPosInChildrenArray(1,1,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,1,1), getInterpolatedPos(2,2,2), child_diag, this);
		// We're not longer leaf-node:
		m_isLeafNode = false;
	}

// ****************************************************************************************

	void splitNode4D() {
		ValueType child_diag = search_diag / 2;
		children = new MPUDFOcNode<Point>*[numChildren];
		children[getPosInChildrenArray(0,0,0,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,0,0,0), getInterpolatedPos(1,1,1,1), child_diag, this);
		children[getPosInChildrenArray(1,0,0,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,0,0,0), getInterpolatedPos(2,1,1,1), child_diag, this);
		children[getPosInChildrenArray(0,1,0,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,1,0,0), getInterpolatedPos(1,2,1,1), child_diag, this);
		children[getPosInChildrenArray(1,1,0,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,1,0,0), getInterpolatedPos(2,2,1,1), child_diag, this);
		children[getPosInChildrenArray(0,0,1,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,0,1,0), getInterpolatedPos(1,1,2,1), child_diag, this);
		children[getPosInChildrenArray(1,0,1,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,0,1,0), getInterpolatedPos(2,1,2,1), child_diag, this);
		children[getPosInChildrenArray(0,1,1,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,1,1,0), getInterpolatedPos(1,2,2,1), child_diag, this);
		children[getPosInChildrenArray(1,1,1,0)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,1,1,0), getInterpolatedPos(2,2,2,1), child_diag, this);
		children[getPosInChildrenArray(0,0,0,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,0,0,1), getInterpolatedPos(1,1,1,2), child_diag, this);
		children[getPosInChildrenArray(1,0,0,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,0,0,1), getInterpolatedPos(2,1,1,2), child_diag, this);
		children[getPosInChildrenArray(0,1,0,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,1,0,1), getInterpolatedPos(1,2,1,2), child_diag, this);
		children[getPosInChildrenArray(1,1,0,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,1,0,1), getInterpolatedPos(2,2,1,2), child_diag, this);
		children[getPosInChildrenArray(0,0,1,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,0,1,1), getInterpolatedPos(1,1,2,2), child_diag, this);
		children[getPosInChildrenArray(1,0,1,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,0,1,1), getInterpolatedPos(2,1,2,2), child_diag, this);
		children[getPosInChildrenArray(0,1,1,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(0,1,1,1), getInterpolatedPos(1,2,2,2), child_diag, this);
		children[getPosInChildrenArray(1,1,1,1)] = new MPUDFOcNode<T>(tree, getInterpolatedPos(1,1,1,1), getInterpolatedPos(2,2,2,2), child_diag, this);
		// We're not longer leaf-node:
		m_isLeafNode = false;
	}

// ****************************************************************************************

	inline int getPosInChildrenArray(UInt x, UInt y) const					{ return x*2+y; }
	inline int getPosInChildrenArray(UInt x, UInt y, UInt z) const			{ return x*4+y*2+z; }
	inline int getPosInChildrenArray(UInt x, UInt y, UInt z, UInt w) const	{ return x*8+y*4+z*2+w; }

// ****************************************************************************************

	//! Depth of this node
	UChar nodelevel;

	//! Min. corner of cell.
	Point min_ext;
	//! Max. corner of cell.
	Point max_ext;
	//! Center of the cell = (max_ext+min_ext)/2;
	Point center;

	//! 'Father'-tree.
	MPUDFOcTree< OcNodeType >* tree;

	//! Flag for leaf-/inner-nodes.
	bool m_isLeafNode;

	//! Children nodes.
	OcNodeType** children;
	//! Father-node pointer.
	OcNodeType* father;

	//! The search range for fitting a quadric.
	ValueType search_diag;

	//! The square of the search range for fitting a quadric.
	ValueType search_diag_square;

	//! Error of current approximating quadric.
	ValueType error;

	//! Number of points in the initial ball of this cell.
	UInt num_points_in_ball;

#ifdef DEEP_DEBUG
public:
#endif

	//! Indices (in parent-tree array) of the cells content.
	std::vector<UInt> content;

	//! The quadric.
	QuadricFunctionType* qf;

};

// ****************************************************************************************
// ************************** TEMPLATE SPECIFIC FUNCTIONS *********************************
// ****************************************************************************************

template <> void MPUDFOcNode<vec2d>::splitNode() { splitNode2D(); }
template <> void MPUDFOcNode<vec2f>::splitNode() { splitNode2D(); }
template <> void MPUDFOcNode<vec3d>::splitNode() { splitNode3D(); }
template <> void MPUDFOcNode<vec3f>::splitNode() { splitNode3D(); }
template <> void MPUDFOcNode<vec4d>::splitNode() { splitNode4D(); }
template <> void MPUDFOcNode<vec4f>::splitNode() { splitNode4D(); }
template <> inline UInt MPUDFOcNode<vec2d>::getChildIndexContaining(const Point& obj) const { return getChildIndexContaining2D(obj); }
template <> inline UInt MPUDFOcNode<vec2f>::getChildIndexContaining(const Point& obj) const { return getChildIndexContaining2D(obj); }
template <> inline UInt MPUDFOcNode<vec3d>::getChildIndexContaining(const Point& obj) const { return getChildIndexContaining3D(obj); }
template <> inline UInt MPUDFOcNode<vec3f>::getChildIndexContaining(const Point& obj) const { return getChildIndexContaining3D(obj); }
template <> inline UInt MPUDFOcNode<vec4d>::getChildIndexContaining(const Point& obj) const { return getChildIndexContaining4D(obj); }
template <> inline UInt MPUDFOcNode<vec4f>::getChildIndexContaining(const Point& obj) const { return getChildIndexContaining4D(obj); }



typedef MPUDFOcNode<vec2f> MPUDFOcNode2f;
typedef MPUDFOcNode<vec2d> MPUDFOcNode2d;
typedef MPUDFOcNode<vec3f> MPUDFOcNode3f;
typedef MPUDFOcNode<vec3d> MPUDFOcNode3d;
typedef MPUDFOcNode<vec4f> MPUDFOcNode4f;
typedef MPUDFOcNode<vec4d> MPUDFOcNode4d;



// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************







//! An nD-Octree for detecting multiple points.
/*!
	use DFOcNode<point2d>, DFOcNode<point3d> or DFOcNode<point4d> as template parameter.
*/
template <class T>
class MPUDFOcTree {

public:

// ****************************************************************************************

	typedef T MPUOcNodeType;
	typedef typename T::Point Point;
	typedef typename Point::ValueType ValueType;
	typename typedef Point::lowerDegreePoint LowerDegreePoint;
	static const UInt dim = Point::dim;

	//! Allow DFOcNode to access our private members.
	friend class MPUDFOcNode<Point>;
	//! Allow QuadricFactory to access our private members.
	friend class QuadricFactory<Point>;

// ****************************************************************************************

	//! Allocate a new octree ranging from 'min_' to 'max_'.
	MPUDFOcTree()
	{
		head = NULL;
		kdTree = NULL;
		dataPts = NULL;
		queryPt = NULL;
		quadricFactory = NULL;
		numCells = 0;

		max_ext = Point(-std::numeric_limits<ValueType>::max());
		min_ext = Point(std::numeric_limits<ValueType>::max());

		memorySVD_generic = new MemoryPointersForSVD<ValueType>(dim);
		memorySVD_lowvariate = new MemoryPointersForSVD<ValueType>(dim-1);

#ifdef PER_SLICE_KD_TREES
		maxPointsInASlice = 0;
		current_w = -std::numeric_limits<ValueType>::max();
#endif
	}

// ****************************************************************************************

	//! Destructor.
	~MPUDFOcTree() {

		delete memorySVD_generic;
		delete memorySVD_lowvariate;

		if (head != NULL) delete head;
		if (kdTree != NULL) delete kdTree;
		if (dataPts != NULL) annDeallocPts(dataPts);
		if (queryPt != NULL) annDeallocPt(queryPt);
		if (nnIdx != NULL) delete[] nnIdx;
		if (dists != NULL) delete[] dists;
		if (quadricFactory != NULL) delete quadricFactory;

	}

// ****************************************************************************************

	inline void build(ValueType max_error_, UInt notUsed) {

		max_error_ /= (max_ext-min_ext).length();
		initMPU(max_error_);

	}

// ****************************************************************************************

	inline void initMPU(ValueType max_error_ = ValueType(0.01)) {

		Point diag = max_ext-min_ext;
		ValueType diaglength = diag.length();

		max_ext += diag * (ValueType)0.03;
		min_ext -= diag * (ValueType)0.03;

		//std::cerr << "octree: (" << min_ext << ") (" << max_ext << ")" << std::endl;

		// Compute 'min_ext_head_node' and 'max_ext_head_node'
		Point center = (max_ext+min_ext)/2;
		ValueType max_side_length = fabs(max_ext[0]-min_ext[0]);
		for (UInt i1 = 0; i1 < dim; i1++) {
			max_side_length = std::max(max_side_length, fabs(max_ext[i1]-min_ext[i1]));
		}
		Point diag_head_node_half(max_side_length);
        diag_head_node_half /= 2;
		Point min_ext_head_node = center-diag_head_node_half;
		Point max_ext_head_node = center+diag_head_node_half;

		//std::cerr << "h_node: (" << min_ext_head_node << ") (" << max_ext_head_node << ")" << std::endl;

		head = new MPUOcNodeType(this, min_ext_head_node, max_ext_head_node, (diaglength*(ValueType)(1.06*0.75)));

		max_error = diaglength*max_error_;

		std::cerr << "initialize MPU with " << (UInt)objects.size() << " points, max error: " << max_error << std::endl;
		std::cerr << "min: " << min_ext_head_node << std::endl;
		std::cerr << "max: " << max_ext_head_node << std::endl;

		//head->splitNode();
		// Insert all points:
		numPtsInOctree = (UInt)objects.size();

		for (UInt i1 = 0; i1 < numPtsInOctree; i1++)
			head->insertObject(i1);

		// Build ANN
		dataPts = annAllocPts(numPtsInOctree, dim);
		for (UInt i1 = 0; i1 < numPtsInOctree; i1++) {
			for (UInt i2 = 0; i2 < dim; i2++) {
				dataPts[i1][i2] = objects[i1][i2];
			}
		}
		kdTree = new ANNkd_tree(dataPts, numPtsInOctree, dim);

		quadricFactory = new QuadricFactory<Point>(this);

		// Allocate memory for query Point.
		queryPt = annAllocPt(dim);
		// Allocate arrays for NN-Search:
		nnIdx = new ANNidx[numPtsInOctree];							// allocate near neigh indices
		dists = new ANNdist[numPtsInOctree];						// allocate near neighbor dists


#ifdef PER_SLICE_KD_TREES
		std::cerr << "Building per slice kd-trees..." << std::endl;
		for (UInt i1 = 0; i1 < slice_dataPts.size()-1; i1++) {
			std::cerr << i1 << "/" << (UInt)slice_dataPts.size() << "         \r";
			UInt num_pts = timeSliceInfo[i1+1].second-timeSliceInfo[i1].second;
			ANNkd_tree* kdTree_tmp = new ANNkd_tree(slice_dataPts[i1], num_pts, dim-1);
			slice_Trees.push_back(kdTree_tmp);
		}
		std::cerr << "done";
		slice_nnIdx = new ANNidx[maxPointsInASlice];
		slice_dists = new ANNdist[maxPointsInASlice];
		slice_queryPt = annAllocPt(dim-1);
#endif

	}

// ****************************************************************************************

#ifdef PER_SLICE_KD_TREES
	inline void insertPointIntoSlice(const Point& obj_) {
		ValueType w = obj_[dim-1];
		if (w > current_w) {
			current_w = w;

			// SliceChange
			UInt numV = (UInt)tmp_pts.size();

			maxPointsInASlice = (maxPointsInASlice < numV) ? numV : maxPointsInASlice;

			//std::cerr << "maxPointsInASlice: " << maxPointsInASlice << std::endl;

			int current_num = 0;
			if (timeSliceInfo.size() > 0) {
				current_num = timeSliceInfo[timeSliceInfo.size()-1].second + numV;
			}

			timeSliceInfo.push_back(std::pair<ValueType, UInt>(current_w, current_num));

			if (numV > 0) {
				ANNpointArray dataPts_tmp = annAllocPts(numV, dim-1);
				for (UInt i1 = 0; i1 < numV; i1++) {
					for (UInt i2 = 0; i2 < dim-1; i2++) {
						dataPts_tmp[i1][i2] = tmp_pts[i1][i2];
					}
				}
				slice_dataPts.push_back(dataPts_tmp);
			}
			tmp_pts.clear();
		}

		if (obj_.x != std::numeric_limits<ValueType>::max()) { // that will be the finisher!!
			tmp_pts.push_back(obj_);
		}
	}

// ****************************************************************************************

	inline void printSliceInfos() const {
		for (UInt i1 = 0; i1 < timeSliceInfo.size(); i1++) {
			std::cerr << "Timeslice " << i1 << ": " << timeSliceInfo[i1].first << " starts at " << timeSliceInfo[i1].second << std::endl;
		}
	}

// ****************************************************************************************

#endif

// ****************************************************************************************

	inline void insertPoints(const Point* pnts, const Point* nrmls, UInt numPoints) {

		objects.reserve(numPoints);
		normals.reserve(numPoints);

		for (UInt i1 = 0; i1 < numPoints; i1++) {
			insertPoint(pnts[i1], nrmls[i1]);
		}
	}

// ****************************************************************************************

	inline void insertPoint(const Point& obj_, const Point& normal_) {
		objects.push_back(obj_);
		normals.push_back(normal_);

		for (UInt i1 = 0; i1 < dim; i1++) {
			if (min_ext[i1] > obj_[i1]) min_ext[i1] = obj_[i1];
			if (max_ext[i1] < obj_[i1]) max_ext[i1] = obj_[i1];
		}
	}

// ****************************************************************************************

	//! Returs whether there are any point in the sphere around 'pt' with radius 'search_radius'.
	bool PointsInSphere(const Point& pt, ValueType search_radius) {
		return (dokDRadiusSearch(pt, search_radius) > 0);
	}

// ****************************************************************************************

	//! Performs a Fixed radius search and returns the number of points found.
	int dokDRadiusSearch(const Point& pt, ValueType search_radius) {


#ifdef PER_SLICE_KD_TREES

		ValueType currenttime = pt[dim-1];

		ValueType sliceDist = timeSliceInfo[1].first-timeSliceInfo[0].first;
		ValueType radius = sliceDist*(ValueType)2.6;

		for (UInt i1 = 0; i1 < dim-1; i1++) slice_queryPt[i1] = pt[i1];

		int counter = 0;

		// run through all timeslices:
		for (UInt i1 = 0; i1 < slice_Trees.size(); i1++) {
			ValueType time = timeSliceInfo[i1].first;
			UInt offset = timeSliceInfo[i1].second;
			ValueType currentDist = fabs(currenttime-time);

			if (currentDist < radius) {
				currentDist /= radius;

				ValueType scaler = sqrt(1-(currentDist*currentDist));
				ValueType radius_for_slice = search_radius*scaler;
				int num_pts = slice_Trees[i1]->annkFRSearch(slice_queryPt, radius_for_slice, 0);
				slice_Trees[i1]->annkFRSearch(slice_queryPt, radius_for_slice, num_pts, slice_nnIdx, slice_dists);
				for (int i2 = 0; i2 < num_pts; i2++) {
					nnIdx[counter] = slice_nnIdx[i2]+offset;
					ValueType dist = objects[nnIdx[counter]].squareDist(pt);
					dists[counter] = dist;
					counter++;
				}
			}
		}

		return counter;
#else

		for (UInt i1 = 0; i1 < dim; i1++) queryPt[i1] = pt[i1];

		// Get all points in the radius 'search_radius'
		ANNdist sr_radius = search_radius*search_radius;
		int num_pts = kdTree->annkFRSearch(queryPt, sr_radius, 0);		// Get number of points within radius

		kdTree->annkFRSearch(queryPt, sr_radius, num_pts, nnIdx, dists);

		return num_pts;

#endif
	}

// ****************************************************************************************

	//! Performs a 'numN' nearest-neighbors search and returns the radius for the most distant point.
	ValueType doNNSearch(const Point& pt, UInt numN) {

		for (UInt i1 = 0; i1 < dim; i1++) queryPt[i1] = pt[i1];
		kdTree->annkSearch(queryPt, numN, nnIdx, dists);

		return (ValueType)dists[numN-1];
	}

// ****************************************************************************************

	inline UInt getNearestPointsInDistance(Point pt, ValueType distSquared) {
		for (UInt i1 = 0; i1 < dim; i1++) queryPt[i1] = pt[i1];
		UInt num = kdTree->annkFRSearch(queryPt, distSquared, 0);
		return num;
	}

// ****************************************************************************************

	static inline ValueType getWeight(const ValueType& t) {
		if (t < 0.5)
			return (ValueType)0.75 - t*t;
		else
			return (ValueType)0.5*((ValueType)1.5-t)*((ValueType)1.5-t);
	}

// ****************************************************************************************

	//! Returns how many objects may be maximally stored in each leaf-node.
	inline UInt getMaxNumObjectsPerNode() const {
		return maxNumObjectsPerNode;
	}

// ****************************************************************************************

	//! evaluates the gradient of MPU-Function at 'pos'.
	SquareMatrixND<Point> evaluateSecondDeriv(const Point& pos) {
		if (!head->PointisIn(pos))
			return SquareMatrixND<Point>();

		Sw = 0;
		SwSecDeriv = SquareMatrixND<Point>();

		head->MPUapproxSecDeriv(pos);

		if (Sw != 0) {
			return SwSecDeriv;
		} else {
			return SquareMatrixND<Point>();
		}
	}

// ****************************************************************************************

	Point evalGrad(const Point& pos) { return evaluateGradient(pos); }

// ****************************************************************************************

	//! evaluates the gradient of MPU-Function at 'pos'.
	Point evaluateGradient(const Point& pos) {
		if (!head->PointisIn(pos))
			return Point((ValueType)0);

		Sw = 0;
		SwG = Point((ValueType)0);

		head->MPUapproxGrad(pos);

		if (Sw != 0) {
			SwG.normalize();
			return SwG;
		} else {
			return Point((ValueType)0);
		}
	}

// ****************************************************************************************

	ValueType eval(const Point& pos) { return evaluate(pos); }

// ****************************************************************************************

	//! evaluates the MPU-Function at 'pos'.
	ValueType evaluate(const Point& pos) {

		if (!head->PointisIn(pos))
			return (ValueType)-10000;

		Sw = 0;
		SwQ = 0;

		head->MPUapprox(pos);

		//std::cerr << Sw << " / " << SwQ << std::endl;

		return (Sw != 0) ?  SwQ/Sw : 1;
	}

// ****************************************************************************************

	inline const MPUDFOcNode<Point>* getCellContainingPoint(const Point& pt) const {
		return head->getCellContainingPoint(pt);
	}

// ****************************************************************************************


	//! Returns the 'min' corner of the octree.
	inline Point getMin() const {
		return min_ext;
	}

// ****************************************************************************************

	//! Returns the 'max' corner of the octree.
	inline Point getMax() const {
		return max_ext;
	}

// ****************************************************************************************

	//! Storage for points in tree.
	std::vector<Point> objects;
	//! Storage for points in tree.
	std::vector<Point> normals;

	//! Pre-allocated data structure for nn-search.
	ANNidxArray nnIdx;

// ****************************************************************************************

	void printStatistics() const {
		std::cerr << "----------------------------" << std::endl;
		std::cerr << "Octree:" << std::endl;
#ifdef STORE_CELLS_IN_TREE
		std::cerr << "Number of cells: " << (int)cells.size() << std::endl;

		int num_leafes = 0;
		std::map<int, int> levels;
		std::map<int, int> numEntries;

		for (UInt i1 = 0; i1 < cells.size(); i1++) {
			if (cells[i1]->isLeafNode()) {
				num_leafes++;
				numEntries[cells[i1]->getNumObjects()]++;
			}

			levels[cells[i1]->getNodeLevel()]++;
		}


		std::cerr << "Number of leaf-nodes: " << num_leafes << std::endl;
		std::cerr << std::endl << "Cell levels: " << std::endl;
		for (std::map<int, int>::iterator it = levels.begin(); it != levels.end(); it++)
			std::cerr << "Level: " << it->first << "   count: " << it->second << std::endl;

		//std::cerr << std::endl << "Number of objects per node: " << std::endl;
		//for (std::map<int, int>::iterator it = numEntries.begin(); it != numEntries.end(); it++)
		//	std::cerr << "Num Objects: " << it->first << "   frequency: " << it->second << std::endl;

#endif
		std::cerr << "----------------------------" << std::endl;
		std::cerr << std::endl;
	}

// ****************************************************************************************
	void setMinExt(Point min_ext_) { min_ext = min_ext_; }
// ****************************************************************************************
	void setMaxExt(Point max_ext_) { max_ext = max_ext_; }
// ****************************************************************************************


	MemoryPointersForSVD<ValueType>* memorySVD_generic;
	MemoryPointersForSVD<ValueType>* memorySVD_lowvariate;

#ifdef STORE_CELLS_IN_TREE
	std::vector< MPUOcNodeType* > cells;
#endif

	UInt getNumCells() {
		return numCells;
	}

protected:

// ****************************************************************************************

	//! Number of cells in the octree:
	UInt numCells;

	//! Maximum error for MPU approximation.
	ValueType max_error;

	//! denominator.
	ValueType Sw;

	//! nominator (Value).
	ValueType SwQ;

	//! nominator (Gradient).
	Point SwG;

	//! nominator (second derivative).
	SquareMatrixND<Point> SwSecDeriv;

	//! Min. corner of head-cell.
	Point min_ext;
	//! Max. corner of head-cell.
	Point max_ext;

#ifdef PER_SLICE_KD_TREES
// ****************************************************************************************
//	STUFF FOR SINGLE POINTCLOUDS
// ****************************************************************************************
public:
	//! needed for 'insertPointIntoSlice'
	ValueType current_w;

	std::vector<ANNkd_tree*> slice_Trees;
	std::vector<Point> tmp_pts;
	//! Stores the vectors of the kD-tree.
	std::vector<ANNpointArray> slice_dataPts;
	//! timesliceInfo (first = time, second = first vertex);
	std::vector< std::pair<ValueType, UInt> > timeSliceInfo;
	//! the maximum number of points contained in a slice.
	UInt maxPointsInASlice;
	//! Point used to query Neighbors.
	ANNpoint slice_queryPt;
	//! Pre-allocated data structure for nn-search.
	ANNdistArray slice_dists;
	//! Pre-allocated data structure for nn-search.
	ANNidxArray slice_nnIdx;
private:
// ****************************************************************************************
#endif

	//! Number of points in octree.
	UInt numPtsInOctree;

	//! First cell.
	MPUOcNodeType* head;

	//! ANN kD-tree.
	ANNkd_tree* kdTree;

	//! Stores the vectors of the kD-tree.
	ANNpointArray dataPts;

	//! Point used to query Neighbors.
	ANNpoint queryPt;

	//! Pre-allocated data structure for nn-search.
	ANNdistArray dists;

	//! Factory for generating quadrics (the factory decides whether a generic quadric, a lowvariate quadric or a quadric-edge is fitted).
	QuadricFactory<Point>* quadricFactory;

};

typedef MPUDFOcTree< MPUDFOcNode2f > MPUOctree2f;
typedef MPUDFOcTree< MPUDFOcNode2d > MPUOctree2d;
typedef MPUDFOcTree< MPUDFOcNode3f > MPUOctree3f;
typedef MPUDFOcTree< MPUDFOcNode3d > MPUOctree3d;
typedef MPUDFOcTree< MPUDFOcNode4f > MPUOctree4f;
typedef MPUDFOcTree< MPUDFOcNode4d > MPUOctree4d;

}


#endif
