#ifndef TRIVIAL_MESH
#define TRIVIAL_MESH


#include "point3d.h"
#include "JBS_General.h"
#include <vector>


//******************************************************************************************


template<class T>
struct trivialTriangle {

	typedef T Point;

	trivialTriangle() {
	}

	Point v0, v1, v2;
	trivialTriangle(Point v0_, Point v1_, Point v2_) {
		v0 = v0_;
		v1 = v1_;
		v2 = v2_;
	};

	void print() {
		v0.print();
		v1.print();
		v2.print();
		std::cerr << std::endl;
	}

	Point getNormal() const {
		Point d0 = v0-v1;
		Point d1 = v0-v2;
		Point n = d0^d1;
		if (n.squaredLength() > 0) n.normalize();

		return n;
	}
};


//******************************************************************************************


template<class T>
class TrivialMesh1 {

public:

	typedef T Point;

	TrivialMesh1(UInt numAlloc=1000) {
		tris.reserve(numAlloc);
	};

	void insertTriangle(Point v0_, Point v1_, Point v2_) {
		tris.push_back(trivialTriangle<Point>(v0_, v1_, v2_));
	};

	inline void print() {
		printStats();
		for (UInt i1 = 0; i1 < tris.size(); i1++)
			tris[i1].print();
	};

	inline void printStats() {
		std::cerr << "Trivial mesh with " << (UInt)tris.size() << " triangles: " << std::endl;
	};

	inline UInt getNumTris() const {
		return tris.size();
	};

	std::vector<trivialTriangle<Point> > tris;
};


//******************************************************************************************


template<class T>
class TrivialMesh {

public:

	typedef T Point;
	typedef trivialTriangle<Point> MyTriangle;

	TrivialMesh(UInt numAlloc=1000) {
		//std::cerr << "Reserving " << numAlloc << " Triangles" << std::endl;
		tris.reserve(numAlloc);
		//counter = 0;
		//tris = new MyTriangle[numAlloc];
	};

	~TrivialMesh() {
		//delete[] tris;
	}

	void insertTriangle(const Point& v0_, const Point& v1_, const Point& v2_) {
		tris.push_back(MyTriangle(v0_, v1_, v2_));
		//counter++;
	};

	inline void print() {
		printStats();
		for (UInt i1 = 0; i1 < getNumTris(); i1++)
			tris[i1].print();
	};

	inline void printStats() {
		std::cerr << "Trivial mesh with " << getNumTris() << " triangles: " << std::endl;
	};

	inline UInt getNumTris() const {
		return (UInt)tris.size();
		//return counter;
	};

	inline void ComputeMinAndMaxExt(Point& min, Point& max) {
		min = tris[0].v0;
		max = tris[0].v0;

		for (UInt i1 = 0; i1 < getNumTris(); i1++) {
			for (UInt i2 = 0; i2 < 3; i2++) {
				Point p = tris[i1].v0;
				if (i2 == 1) p = tris[i1].v1;
				else if (i2 == 1) p = tris[i1].v2;

				if (min.x > p.x) min.x = p.x;
				if (max.x < p.x) max.x = p.x;
				if (min.y > p.y) min.y = p.y;
				if (max.y < p.y) max.y = p.y;
				if (min.z > p.z) min.z = p.z;
				if (max.z < p.z) max.z = p.z;
			}
		}
	};

	std::vector<MyTriangle> tris;
	//MyTriangle* tris;

private:
	//UInt counter;

};


#endif
