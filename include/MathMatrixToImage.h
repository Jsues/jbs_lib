#ifndef JBS_MATH_MATRIX_TO_IMAGE_H
#define JBS_MATH_MATRIX_TO_IMAGE_H

#include "MathMatrix.h"
#include "MathMatrix2.h"
#include "Image.h"

#ifdef max
#undef max
#endif

template <class Matrix>
void MathMatrixToImage(const Matrix& A, const char* filename) {

	Matrix::ValueType maxV = -std::numeric_limits<Matrix::ValueType>::max();
	Matrix::ValueType minV = std::numeric_limits<Matrix::ValueType>::max();
	for (int x = 0; x < A.nrows(); x++) {
		for (int y = 0; y < A.ncols(); y++) {
			Matrix::ValueType val = A(x,y);
			maxV = std::max(val, maxV);
			minV = std::min(val, minV);
		}
	}

	Matrix::ValueType range = maxV-minV;
	range /= (Matrix::ValueType)255;
	Image img(A.nrows(), A.ncols());
	for (int x = 0; x < A.nrows(); x++) {
		for (int y = 0; y < A.ncols(); y++) {
			Matrix::ValueType val = A(x,y);
			val -= minV;
			val /= range;
			img.setPixel(x,y, (unsigned char)val);
		}
	}
	std::cerr << "image dim: " << A.nrows() << " x " << A.ncols() << std::endl;
	img.writeRAW(filename);
}

#endif
