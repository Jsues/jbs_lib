#ifndef JBS_KD_POINT_CLOUD_H
#define JBS_KD_POINT_CLOUD_H



#include "PointCloud.h"
#include <ANN/ANN.h>



//! This class represents a pointcloud or unstructured pointset, which is stored in a kD-Tree.
template<class T>
class kdPointCloudNormals {
public:

//****************************************************************************************************

	//! Type of a Point, for example a 3D Vector of floats.
	typedef T Point;

	//! Value-Type of Templates components (i.e. float or double).
	typedef typename T::ValueType ValueType;

	//! Dimension of the pointcloud.
	static const UInt dim = T::dim;

//****************************************************************************************************

	//! Constructor
	kdPointCloudNormals(int numAlloc) {
		numANNalloc = numAlloc;
		kdTree = NULL;
		dataPts = annAllocPts(numANNalloc, dim);
		queryPt = annAllocPt(dim);
	}

//****************************************************************************************************

	//! Constructor
	kdPointCloudNormals(PointCloudNormals<Point>* pc) {

		numANNalloc = pc->getNumPts();
		kdTree = NULL;
		dataPts = annAllocPts(numANNalloc, dim);
		queryPt = annAllocPt(dim);

		// Fill data:
		for (unsigned int i1 = 0; i1 < numANNalloc; i1++) {
			const Point& p = pc->m_pts[i1];
			dataPts[i1][0] = p.x;
			dataPts[i1][1] = p.y;
			dataPts[i1][2] = p.z;
		}

		m_normals = pc->m_normals;
		m_points  = pc->m_pts;

		buildKdTree();
	}

//****************************************************************************************************

	//! Destructor
	~kdPointCloudNormals() {
		if (kdTree != NULL) delete kdTree;
		if (dataPts != NULL) annDeallocPts(dataPts);
		annDeallocPt(queryPt);
		annClose();			// close ANN
	}

//****************************************************************************************************

	//! Explicitly builds the kD tree. Call this function only if you inserted the points via 'insertPoint'. You do not have to call the function if you gave a point cloud to this class via the constructor!
	void explicitlyBuildkDTree() {
		buildKdTree();
	}

//****************************************************************************************************

	//! Inserts a point with normal into the pc. Caution, you must call explicitlyBuildkDTree() once all points have been added!
	void insertPoint(const Point& p, const Point& n) {
		int idx = (int)m_points.size();

		dataPts[idx][0] = p.x;
		dataPts[idx][1] = p.y;
		dataPts[idx][2] = p.z;

		m_points.push_back(p);
		m_normals.push_back(n);
	}

//****************************************************************************************************

	//! Computes min/max dimensions of the point cloud.
	void recomputeMinMax() {
		max = Point(-std::numeric_limits<ValueType>::max());
		min = Point(std::numeric_limits<ValueType>::max());

		for (int i1 = 0; i1 < (int)m_points.size(); i1++) {
			for (UInt i = 0; i < dim; ++i) {
				min[i] = std::min(min[i], m_points[i1][i]);
				max[i] = std::max(max[i], m_points[i1][i]);
			}
		}
	}

//****************************************************************************************************

	//! Get index of nearest point in kd-tree
	void getkNearestPoints(const T& p, int k, ANNidx* nnIdx, ANNdist* dist) {
		for (UInt i = 0; i < dim; ++i) {
			queryPt[i] = p[i];
		}
		kdTree->annkSearch(queryPt, k, nnIdx, dist, 0);
	}

//****************************************************************************************************

	//! Get index of nearest point in kd-tree
	int getNearestPoint(const T& p) {
		for (UInt i = 0; i < dim; ++i) {
			queryPt[i] = p[i];
		}
		kdTree->annkSearch(queryPt, 1, &nnIdx1, &dist1, 0);

		return nnIdx1;
	}

//****************************************************************************************************

	//! Get number of points inside sphere radius
	UInt getNearestPointsInDistance(const T& p, ValueType distSquared) {
		for (UInt i = 0; i < dim; ++i) {
			queryPt[i] = p[i];
		}
		UInt num = kdTree->annkFRSearch(queryPt, distSquared, 0);

		return num;
	}

//****************************************************************************************************

	//! The normal vectors of the points.
	std::vector<Point> m_normals;

//****************************************************************************************************

	//! The positions of the points.
	std::vector<Point> m_points;

	//! Max extension of the pc.
	Point max;
	//! Min extension of the pc.
	Point min;


private:

//****************************************************************************************************

	//! Builds the kD-Tree.
	void buildKdTree() {
		kdTree = new ANNkd_tree(dataPts, (int)numANNalloc, dim);
	};

//****************************************************************************************************

	//! Number of points allocated for the kD-Tree.
	unsigned int numANNalloc;

	//! The ANN kD-Tree.
	ANNkd_tree* kdTree;

	//! Points of the kd-tree.
	ANNpointArray dataPts;
	
	//! Point structure for kd-tree queries
	ANNpoint queryPt;
	
	//! Index value from kd-tree queries
	ANNidx nnIdx1;
	
	//! Distance value from kd-tree queries
	ANNdist dist1;
};

#endif

