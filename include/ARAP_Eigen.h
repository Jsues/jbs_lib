#ifndef ARAP_EIGEN_H
#define ARAP_EIGEN_H

#include <iostream>
#include <fstream>

#include "MatrixnD.h"
#include "SimpleMesh.h"
#include "timer.h"
#include "ARAP_common.h"


#include <Eigen/Sparse>


#include <set>

#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif

//******************************************************************************************

struct PrecomputedMatrixEigen {
	Eigen::SparseMatrix<double> A;
	Eigen::SimplicialLDLT< Eigen::SparseMatrix<double> > solverA;
	std::vector<int> vertexLookup;
	int unconstrained;
};

//******************************************************************************************

void ARAP_computeMatrixEigen(SimpleMesh* sm, SimpleMesh* urshape, PrecomputedMatrixEigen& pm);
void ARAP_computeMatrixEigenSimpleUniform(SimpleMesh* sm, PrecomputedMatrixEigen& pm);
void ARAP_Deform_with_given_factorizationEigen(SimpleMesh* sm, SimpleMesh* urshape, PrecomputedMatrixEigen& pm, UInt numInnerIter = 20);
void Laplace_Deform_with_given_factorizationEigen(SimpleMesh* sm, SimpleMesh* urshape, PrecomputedMatrixEigen& pm);
void Laplace_with_given_factorizationEigenUniform(SimpleMesh* sm, PrecomputedMatrixEigen& pm);
void ARAP_DeformEigen(SimpleMesh* sm, SimpleMesh* urshape, UInt numInnerIter = 20);

#endif
