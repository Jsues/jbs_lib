#ifndef CONVEX_HULL_4D_H
#define CONVEX_HULL_4D_H

// for sort
#include <algorithm>
#include "JBS_General.h"
#include "point4d.h"
#include <vector>
#include <algorithm>
#include <set>

#include "determinant.h"

//#define PRINT_DEBUG


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************
//! Pair of vertex and index, stores the original insertion order.
template<class T>
struct vertexAndIndex
{
	vertexAndIndex(const point4d<T>& v, UInt i) : vertex(v), index(i) {};
	UInt index;
	point4d<T> vertex;
};



// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************
//! Needed for sorting 'point4d<t>'s.
template<class T>
struct ltvec4
{
	bool operator()(const vertexAndIndex<T>& s1, const vertexAndIndex<T>& s2) const
	{
		if (s1.vertex.x == s2.vertex.x) {
			if (s1.vertex.y == s2.vertex.y) {
				if (s1.vertex.z == s2.vertex.z) {
					if (s1.vertex.w == s2.vertex.w) {
						return false;
					} else {
						return (s1.vertex.w < s2.vertex.w);
					}
				} else {
					return (s1.vertex.z < s2.vertex.z);
				}
			} else {
				return (s1.vertex.y < s2.vertex.y);
			}
		} else {
			return (s1.vertex.x < s2.vertex.x);
		}
	}
};



// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************



template <class T> class ConvexHull4D;
template <class T> class Simplex4D;






// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************



template <class T>
class Simplex1D {
public:
	typedef T ValueType;
	typedef point4d<T> Vertex;

	Simplex1D(UInt v0_, UInt v1_, ConvexHull4D<point4d<ValueType> >* parent_ = NULL) : parent(parent_) {
		v[0] = v0_;
		v[1] = v1_;
	}

	inline bool isContained(const Vertex& vert) const {
		Vertex v0 = parent->vertices[v[0]].vertex;
		Vertex v1 = parent->vertices[v[1]].vertex;

		ValueType segmentLength = v0.dist(v1);
		ValueType d0 = v0.dist(vert);
		ValueType d1 = v1.dist(vert);

		if (d0 > segmentLength || d1 > segmentLength)
			return false;
		else
			return true;
	}

	UInt v[2];     // array access
private:
	ConvexHull4D<Vertex>* parent;
};



// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************



template <class T>
class Simplex3D {
public:
	typedef T ValueType;
	typedef point4d<T> Vertex;

	Simplex3D(UInt v0_, UInt v1_, UInt v2_, UInt v3_, ConvexHull4D<point4d<ValueType> >* parent_ = NULL) : parent(parent_) {
		v[0] = v0_;
		v[1] = v1_;
		v[2] = v2_;
		v[3] = v3_;
	}


	UInt v[4];     // array access
private:
	ConvexHull4D<Vertex>* parent;
};



// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************



template <class T>
class Simplex2D {
public:
	typedef T ValueType;
	typedef point4d<T> Vertex;

	Simplex2D(UInt v0_, UInt v1_, UInt v2_, ConvexHull4D<point4d<ValueType> >* parent_ = NULL) : parent(parent_) {
		v[0] = v0_;
		v[1] = v1_;
		v[2] = v2_;
	}

	bool liesInPlane(const Vertex& vert) {
		Vertex vec1 = parent->vertices[v[1]].vertex - parent->vertices[v[0]].vertex;
		Vertex vec2 = parent->vertices[v[2]].vertex - parent->vertices[v[0]].vertex;
		Vertex vec3 = vert - parent->vertices[v[0]].vertex;

		vec1.normalize();
		vec2 -= vec1*(vec1|vec2);
		vec2.normalize();

		//std::cerr << "v0: " << vec1 << std::endl;
		//std::cerr << "v1: " << vec2 << std::endl;
		//std::cerr << "a:  " << parent->vertices[v[0]].vertex << std::endl;
		//std::cerr << "p:  " << vert << std::endl;
		Vertex p_dot = vec3;

		p_dot = vec3 - vec1*(vec1|vec3) - vec2*(vec2|vec3);

		// Check if 3 vectors are linear dependent:
		if (p_dot.squaredLength() < (ValueType)0.000000000001)
			return true;
		else
            return false;

	}

	UInt v[3];     // array access
private:
	ConvexHull4D<Vertex>* parent;
};



// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************



template <class T>
class Simplex4D {


// ****************************************************************************************

	friend class ConvexHull4D;

// ****************************************************************************************

	friend bool haveCorrespondingFace(const Simplex4D<T>& s1, const Simplex4D<T>& s2, int* faceIndex) {
	
		// Test all faces of s1 against all faces of s2:
		std::vector<UInt> face1;
		face1.reserve(4);
		std::vector<UInt> face2;
		face2.reserve(4);

		bool match = false;

		for (UInt i = 0; i < 5; i++) {
			for (UInt i1 = 0; i1 < 4; i1++) face1.push_back(s1.v[Simplex4D<T>::faces[i][i1]]);
			std::sort(face1.begin(), face1.end());

			for (UInt j = 0; j < 5; j++) {
				for (UInt i1 = 0; i1 < 4; i1++) face2.push_back(s2.v[Simplex4D<T>::faces[j][i1]]);
				std::sort(face2.begin(), face2.end());

				if (face1 == face2) {
					//std::cerr << "Match: ";
					//for (UInt i1 = 0; i1 < 4; i1++) std::cerr << "(" << face1[i1] << "/" << face2[i1] << ") ";
					//std::cerr << std::endl;
					match = true;
					//std::cerr << s1.neighbors[i] << " / " << s2.neighbors[j] << std::endl;
					faceIndex[0] = i;
					faceIndex[1] = j;
				}

				face2.clear();
			}
            face1.clear();
		}
		return match;
	};


// ****************************************************************************************

public:

// ****************************************************************************************

	static const UInt faces[5][4];

	typedef T ValueType;
	typedef point4d<T> Vertex;


// ****************************************************************************************

	Simplex4D(ConvexHull4D<point4d<ValueType> >* parent_ = NULL) : parent(parent_) {
		for (UInt i1 = 0; i1 < 5; i1++) neighbors[i1] = -1;
	}


// ****************************************************************************************

	Simplex4D(UInt v0_, UInt v1_, UInt v2_, UInt v3_, UInt v4_, ConvexHull4D<point4d<ValueType> >* parent_ = NULL) : parent(parent_), v0(v0_), v1(v1_), v2(v2_), v3(v3_), v4(v4_) {
		for (UInt i1 = 0; i1 < 5; i1++) neighbors[i1] = -1;
	}


// ****************************************************************************************

	ValueType getHyperArea() const {
		return (determinant(v0,v1,v2,v3,v4));
	}


// ****************************************************************************************

	void print() const {
		std::cerr << "Simplex: " << v0 << " " << v1 << " " << v2 << " " << v3 << " " << v4 << std::endl;
	}


// ****************************************************************************************

	void printWithVertices() const {
		print();
		for (UInt i1 = 0; i1 < 5; i1++)
			parent->vertices[v[i1]].vertex.print();
	}


// ****************************************************************************************

	inline bool isContained(UInt v, bool* outcodes) const {

#ifdef PRINT_DEBUG
		std::cerr << "Testing if vertex " << v << " is contained in Simplex " << v0 << " " << v1 << " " << v2 << " " << v3 << " " << v4 << std::endl;
#endif

		// Faces (which are 3d tetrahedra) are:
		// v0, v1, v2, v3		// face1
		// v1, v0, v2, v4		// face2
		// v0, v1, v3, v4		// face3
		// v2, v0, v3, v4		// face4
		// v1, v2, v3, v4		// face1

		ValueType detFather = determinant(v0,v1,v2,v3,v4);

		bool is_Inside = true;
		int sign = (detFather < 0) ? -1 : 1;

#ifdef PRINT_DEBUG
		ValueType ddd[5];
#endif

		for (UInt i1 = 0; i1 < 5; i1++) { // for all faces
			ValueType d = determinant(faces[i1], v);

#ifdef PRINT_DEBUG
		ddd[i1] = d;
#endif

			if ((d*sign) < 0) {
				is_Inside = false;
				outcodes[i1] = true;
			} else {
				outcodes[i1] = false;
			}
		}

#ifdef PRINT_DEBUG
		std::cerr << "Barycentrics: ";
		for (UInt i1 = 0; i1 < 5; i1++)
			std::cerr << ddd[i1] << " ";
		std::cerr << "det_father: " << detFather << std::endl;

		if (detFather == 0) {
			for (UInt i1 = 0; i1 < 5; i1++)
				parent->vertices[this->v[i1]].vertex.print();
		}
		std::cerr << "v5: " << parent->vertices[v].vertex << std::endl;
#endif

		//std::cerr << "Child determinant d1: " << d1 << std::endl;
		//std::cerr << "Child determinant d2: " << d2 << std::endl;
		//std::cerr << "Child determinant d3: " << d3 << std::endl;
		//std::cerr << "Child determinant d4: " << d4 << std::endl;
		//std::cerr << "Child determinant d5: " << d5 << std::endl;
		//ValueType sum = d1+d2+d3+d4+d5;
		//std::cerr << "Father: " << detFather << "; Sum: " << sum << std::endl;

		return is_Inside;
	}


// ****************************************************************************************

	inline ValueType determinant(const UInt face[4], UInt v4_) const {
		const UInt& v0_ = v[face[0]];
		const UInt& v1_ = v[face[1]];
		const UInt& v2_ = v[face[2]];
		const UInt& v3_ = v[face[3]];
		return determinant4x4(parent->vertices[v0_].vertex-parent->vertices[v1_].vertex, parent->vertices[v1_].vertex-parent->vertices[v2_].vertex, parent->vertices[v2_].vertex-parent->vertices[v3_].vertex, parent->vertices[v3_].vertex-parent->vertices[v4_].vertex);
	}


// ****************************************************************************************

	//! Computes the determinant of (v0, 1), (v1, 1), (v2, 1), (v3, 1), (v4, 1).
	inline ValueType determinant(UInt v0_, UInt v1_, UInt v2_, UInt v3_, UInt v4_) const {
		return determinant4x4(parent->vertices[v0_].vertex-parent->vertices[v1_].vertex, parent->vertices[v1_].vertex-parent->vertices[v2_].vertex, parent->vertices[v2_].vertex-parent->vertices[v3_].vertex, parent->vertices[v3_].vertex-parent->vertices[v4_].vertex);
	}


// ****************************************************************************************

	union {
		struct {
			UInt v0, v1, v2, v3, v4;
		};
		UInt v[5];     // array access
	};

	int neighbors[5]; // Neighbor-simplex index or -1.


// ****************************************************************************************

private:

	ConvexHull4D<Vertex>* parent;
};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************



template <class T>
const UInt Simplex4D<T>::faces[5][4] = {
	{0, 1, 2, 3},
	{1, 0, 2, 4},
	{0, 1, 3, 4},
	{2, 0, 3, 4},
	{1, 2, 3, 4}
};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


template <class T>
class ConvexHull4D {


// ****************************************************************************************

	friend class Simplex4D;


// ****************************************************************************************

public:


// ****************************************************************************************

	typedef T Vertex;
	typedef typename Vertex::ValueType ValueType;


// ****************************************************************************************

	ConvexHull4D() : has4Simplex(false) {
	}


// ****************************************************************************************

	inline void insertVertex(Vertex v, bool isIntersection_=true) {
		vertices.push_back(vertexAndIndex<ValueType>(v, (UInt)vertices.size()));
		isIntersection.push_back(isIntersection_);
	}


// ****************************************************************************************

	void buildHull() {
		// Assert that we have at least 5 vertices:
		if (vertices.size() < 5) {
			std::cerr << "ConvexHull4D::buildHull():" << std::endl;
			std::cerr << "need at least 5 vertices to build a convex hull in 4D. You gave me: " << (UInt)vertices.size() << std::endl;
			std::cerr << "EXIT" << std::endl;
			exit(EXIT_FAILURE);
		}

		std::sort(vertices.begin(), vertices.end(), ltvec4<ValueType>());
		std::cerr << "Sorted: " << std::endl;
		for (std::vector<vertexAndIndex<ValueType> >::iterator it2 = vertices.begin(); it2 != vertices.end(); it2++) it2->vertex.print();

		std::set<UInt> indices;
		for (UInt i1 = 0; i1 < vertices.size(); i1++)
			indices.insert(i1);

		// Build first simplex:
		std::vector< Simplex1D<ValueType> > oneSimplices;
		std::vector< Simplex2D<ValueType> > twoSimplices;

		Simplex1D<ValueType> fs = Simplex1D<ValueType>(0,1, this);
		oneSimplices.push_back(fs);
		indices.erase(fs.v[0]);
		indices.erase(fs.v[1]);

		// find the first 2 Simplex:
		std::set<UInt>::iterator it = indices.begin();
		while (it != indices.end()) {
			UInt currentIndex = *it;
			++it;

			Vertex currentVertex = vertices[currentIndex].vertex;
			// We have only 1-Simplices up to now
			// Check if the new point is also colinear with the existing Simplices:
			Vertex v0 = vertices[twoSimplices[0].v[0]].vertex - currentVertex;
			Vertex v1 = vertices[twoSimplices[0].v[1]].vertex - currentVertex;

			if (v0.isLinearDependent(v1)) {

				bool isContainedInExisting1Simplex = false;
				for (UInt i1 = 0; i1 < oneSimplices.size(); i1++) {
					if (oneSimplices[i1].isContained(currentVertex)) {
						isContainedInExisting1Simplex = true;
						UInt tmp = oneSimplices[i1].v[1];
						oneSimplices[i1].v[1] = currentIndex;
						oneSimplices.push_back(Simplex1D<ValueType>(oneSimplices[i1].v[0],currentIndex, this));
						break;
					}
				}
				if (!isContainedInExisting1Simplex) { // Find Index of closest point:
					ValueType closestSquareDist = 10e100;
					UInt closestID = 0;

					for (UInt i1 = 0; i1 < oneSimplices.size(); i1++) {
						Simplex1D<ValueType>& cs = oneSimplices[i1];
						for (UInt i2 = 0; i2 < 2; i2++) {
							if (ValueType dist = (vertices[cs.v[i2]].vertex).squaredDist(currentVertex) < closestSquareDist) {
								closestSquareDist = dist;
								closestID = cs.v[i2];
							}
						}
					}
					oneSimplices.push_back(Simplex1D<ValueType>(closestID,currentIndex, this));
				}
			} else {
				for (UInt i1 = 0; i1 < oneSimplices.size(); i1++) // Build 2-Simplices from all 1-Simplices:
					twoSimplices.push_back(Simplex2D<ValueType>(oneSimplices[i1].v[0], oneSimplices[i1].v[1], currentIndex, this));
				break;
			}
		}

		// find the first 3 Simplex = tetraedron
		while (it != indices.end()) {
			UInt currentIndex = *it;
			++it;

			Vertex currentVertex = vertices[currentIndex].vertex;
			// We have only 2 Simplices up to now
			// Check if the new point is also colinear with the existing Simplices:
			Vertex v0 = vertices[twoSimplices[0].v[0]].vertex - currentVertex;
			Vertex v1 = vertices[twoSimplices[0].v[1]].vertex - currentVertex;
			Vertex v2 = vertices[twoSimplices[0].v[2]].vertex - currentVertex;

			// Check if currentVertex lies in the plane of (v0, v1 and v2)
			if(twoSimplices[0].liesInPlane(currentVertex)) { // Lies in the same plane:

				// Check if it lies inside one existing triangle:
				if (false) {
					// Handle case
				} else {

				}

			} else {
				//// We have the first 3-Simplex
				//for (UInt i1 = 0; i1 < twoSimplices.size(); i1++) // Build 3-Simplices from all 2-Simplices:
				//	threeSimplices.push_back(Simplex3D<ValueType>(twoSimplices[i1].v[0], twoSimplices[i1].v[1], twoSimplices[i1].v[2], currentIndex, this));
				//break;
			}

		}


		// Add the remaining vertices to the hull:
		for (; it != indices.end(); it++) {
			insertPointIntoHull(*it);
		}
	}

// ****************************************************************************************

	void CheckForVertex(UInt i) {
		printStats();
		for (UInt i1 = 0; i1 < simplices.size(); i1++) {
			for (UInt i2 = 0; i2 < 5; i2++) {
				if (vertices[simplices[i1].v[i2]].index == i) {
					std::cerr << "Index of i = " << i << " is now " << simplices[i1].v[i2] << std::endl;
					std::cerr << "Original indices: ";
					for (UInt i2 = 0; i2 < 5; i2++)
						std::cerr << vertices[simplices[i1].v[i2]].index << " ";
					std::cerr << std::endl;
					std::cerr << "Resorted indices: ";
					for (UInt i2 = 0; i2 < 5; i2++)
						std::cerr << simplices[i1].v[i2] << " ";
					std::cerr << std::endl;
				}
			}
		}
	}

// ****************************************************************************************

	void printStats() const {
		std::cerr << "Convex Hull: " << std::endl;
		std::cerr << "number of points: " << (UInt)vertices.size() << std::endl;
		std::cerr << "number of simplices: " << (UInt)simplices.size() << std::endl;
	}

// ****************************************************************************************

	void printHull() const {
		for (UInt i2 = 0; i2 < simplices.size(); i2++) {		// For all 4-simplices
			for (UInt i3 = 0; i3 < 5; i3++) {					// For all faces = tetrahedra
				if (simplices[i2].neighbors[i3] == -1) {		// face is boundary face
					// print face:
					for (UInt i4 = 0; i4 < 4; i4++)
						std::cerr << simplices[i2].v[Simplex4D<float>::faces[i3][i4]] << " ";
					std::cerr << std::endl;					
				}
			}
		}
	}

// ****************************************************************************************

	std::vector<vertexAndIndex<ValueType> >		vertices;
	std::vector<bool>							isIntersection;
	std::vector<Simplex4D<ValueType> >			simplices;

// ****************************************************************************************

private:

	bool has4Simplex;

// ****************************************************************************************

	inline UInt countOpenFace() const {
		UInt open = 0;
		for (UInt i2 = 0; i2 < simplices.size(); i2++) {
			for (UInt i3 = 0; i3 < 5; i3++)
				if (simplices[i2].neighbors[i3] == -1)
					open++;
		}
		return  open;
	}

// ****************************************************************************************

	inline void insertPointIntoHull(UInt v) {

#ifdef PRINT_DEBUG
		std::cerr << "Insert point: " << v << std::endl;
#endif

		// Check if the point is allready contained in one simplex:
		UInt numSimplices = (UInt)simplices.size();
		for (UInt i1 = 0; i1 < numSimplices; i1++) {

			bool outcodes[5];

			if (simplices[i1].isContained(v, outcodes)) {

				for (UInt i2 = 0; i2 < 5; i2++)
					std::cerr << outcodes[i2] << " ";
				std::cerr << std::endl;

				// We don't have to do anything.
				std::cerr << "Is contained" << std::endl;
				return;

#ifdef PRINT_DEBUG
		std::cerr << "point is contained: " << v << std::endl;
		std::cerr << "outcode: ";
		for (unsigned int i2 = 0; i2 < 5; i2++)
			std::cerr << outcodes[i2] << " ";
		std::cerr << std::endl;
#endif
				//std::cerr << "is Contained" << std::endl;
				// remove this and add the 5 new.
				std::vector<Simplex4D<ValueType> > simpls;
				simpls.reserve(5);
				for (UInt i2 = 0; i2 < 5; i2++) {
					UInt v0 = simplices[i1].v[Simplex4D<T>::faces[i2][0]];
					UInt v1 = simplices[i1].v[Simplex4D<T>::faces[i2][1]];
					UInt v2 = simplices[i1].v[Simplex4D<T>::faces[i2][2]];
					UInt v3 = simplices[i1].v[Simplex4D<T>::faces[i2][3]];
					Simplex4D<ValueType> simplex(v0, v1, v2, v3, v, this);
					//std::cerr << "Insert inside: " << v0 << " " << v1 << " " << v2 << " " << v3 << " " << v << std::endl;
					// The neighbor of the newly added face (face[0]) is the old neighbor
					simplex.neighbors[0] = simplices[i1].neighbors[i2];
					simplex.neighbors[1] = 1000;
					simplex.neighbors[2] = 1000;
					simplex.neighbors[3] = 1000;
					simplex.neighbors[4] = 1000;
					simpls.push_back(simplex);
					//simpl[simpl.size()-1].print();
				}
				simplices[i1] = simpls[0];
				for (UInt i2 = 1; i2 < 5; i2++)
					simplices.push_back(simpls[i2]);

				return;

			} else {
				for (UInt i2 = 0; i2 < 5; i2++) {
					// If outcode is set and the face is a boundary face, join simplex.
					if (outcodes[i2] && (simplices[i1].neighbors[i2] == -1)) {
						UInt v0 = simplices[i1].v[Simplex4D<T>::faces[i2][0]];
						UInt v1 = simplices[i1].v[Simplex4D<T>::faces[i2][1]];
						UInt v2 = simplices[i1].v[Simplex4D<T>::faces[i2][2]];
						UInt v3 = simplices[i1].v[Simplex4D<T>::faces[i2][3]];
						simplices.push_back(Simplex4D<ValueType>(v0, v1, v2, v3, v, this));
						// Assign neighborhood information:
						simplices[i1].neighbors[i2] = (int)simplices.size()-1;
						simplices[simplices.size()-1].neighbors[0] = i1;
#ifdef PRINT_DEBUG
		std::cerr << "causes new simplex: " << v << std::endl;
#endif
					}

					//std::cerr << outcodes[i2] << " ";
				}
				//std::cerr << std::endl;
			}
		}
		UInt numSimplicesAfter = (UInt)simplices.size();
		UInt numInserted = numSimplicesAfter - numSimplices;
		//std::cerr << "Inserted " << numInserted << " 4-Simplices" << std::endl; 
		if (numInserted > 1) { // Correct neighborhoods:
			for (UInt i = numSimplices; i < numSimplicesAfter; i++) {
				for (UInt j = i+1; j < numSimplicesAfter; j++) {
					int faceindices[2];
					if (haveCorrespondingFace(simplices[i], simplices[j], faceindices)) {
						//simplices[i].print();
						//simplices[j].print();
						//std::cerr << "i=" << i << ", j=" << j << ":   match: (" << faceindices[0] << " / " << faceindices[1] << ")" << std::endl;
						simplices[i].neighbors[faceindices[0]] = faceindices[1];
						simplices[j].neighbors[faceindices[1]] = faceindices[0];
					} else {
						//std::cerr << "miss" << std::endl;
					}
				}
			}
		}

		//UInt numOpen = countOpenFace();
		//std::cerr << numOpen << " open faces" << std::endl;
	}

// ****************************************************************************************

};

#endif
