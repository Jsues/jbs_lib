#ifndef JBS_RANDOM_H
#define JBS_RANDOM_H

#include <cmath>

double getGaussian0to1() {

	double x1, x2, w, y1, y2;

	do {
		x1 = (double(rand()) / RAND_MAX);
		x2 = (double(rand()) / RAND_MAX);
		w = x1 * x1 + x2 * x2;
	} while ( w >= 1.0 );

	w = sqrt( (-2.0 * log( w ) ) / w );
	y1 = x1 * w;
	y2 = x2 * w;

	return y2;
}

#endif
