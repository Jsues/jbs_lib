#ifndef JBS_COMP_COL_SPARSE_MATRIX
#define JBS_COMP_COL_SPARSE_MATRIX

#include <vector>
#include <iostream>


#include "Image.h"

#ifndef NO_UMFPACK
#include "umfpack.h"
#endif

#ifdef USETAUCS
extern "C" {
#include <taucs.h>
}
#undef max
#undef min
#endif

//! A simple compressed column sparse matrix class for UMFPACK.
class CompColSparseMatrix {
public:

	CompColSparseMatrix() {
		null = NULL;
		Numeric = NULL;
		Symbolic = NULL;

#ifdef USETAUCS
		F = NULL;
#endif
	}

	~CompColSparseMatrix() {
#ifndef NO_UMFPACK
		if (Symbolic != NULL)
			umfpack_di_free_symbolic(&Symbolic);
		if (Numeric != NULL)
			umfpack_di_free_numeric(&Numeric);
#endif
	}

	//! Compute LU-factorization using UMFPACK.
	void computeLU_Umfpack() {
#ifdef _DEBUG
		std::cerr << "Computing LU-Factorization" << std::endl;
#endif
#ifndef NO_UMFPACK
		int result1 = umfpack_di_symbolic(dim, dim, &row_ptr[0], &col_ind[0], &entries[0], &Symbolic, null, null);
		int result2 = umfpack_di_numeric(&row_ptr[0], &col_ind[0], &entries[0], Symbolic, &Numeric, null, null);
#endif
#ifdef _DEBUG
		std::cerr << "Result LU Factorization: " << result1 << ", " << result2 << std::endl;
		if (result1 == UMFPACK_ERROR_out_of_memory || result2 == UMFPACK_ERROR_out_of_memory) {
			std::cerr << "Error, UMFPACK reports 'Out of memory'" << std::endl;
		}
#endif
	}

	//! Solve Ax=b using UMFPACK.
	void backSolve_Umfpack(double* b, double* x) {
		if (Symbolic == NULL)
			computeLU_Umfpack();

		// Backsolve
#ifndef NO_UMFPACK
		umfpack_di_solve(UMFPACK_A, &row_ptr[0], &col_ind[0], &entries[0], x, b, Numeric, null, null);
#endif

		//std::cerr << "Result: x = (";
		//for (int i1 = 0; i1 < dim; i1++) {
		//	std::cerr << x[i1] << " ";
		//}
		//std::cerr << ")" << std::endl;

	}

	
#ifdef USETAUCS
	void decompCholesky_Taucs() {
	}

	void backSolve_Taucs(double* b, double* x) {

		char* options[4];
		options[0] = "taucs.factor.LLT=true";
		options[1] = "taucs.factor.numeric=false";
		options[2] = "taucs.factor.ordering=genmmd";
		options[3] = NULL;
		void* opt_arg[] = { NULL };

		A.n = dim;
		A.m = dim;
		A.flags = TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER;
		A.colptr = &row_ptr[0];
		A.rowind = &col_ind[0];
		A.values.d = &entries[0];

		int retVal = taucs_linsolve(&A, NULL, 1, x, b, options, opt_arg);

		if (retVal != TAUCS_SUCCESS) {
			std::cerr << "Solution error." << std::endl;

			if (retVal==TAUCS_ERROR) std::cerr << "  Generic error." << std::endl;

			if (retVal==TAUCS_ERROR_NOMEM) std::cerr << "  NOMEM error." << std::endl;

			if (retVal==TAUCS_ERROR_BADARGS) std::cerr << "  BADARGS error." << std::endl;

			if (retVal==TAUCS_ERROR_MAXDEPTH) std::cerr << "  MAXDEPTH error." << std::endl;

			if (retVal==TAUCS_ERROR_INDEFINITE) std::cerr << "  NOT POSITIVE DEFINITE error." << std::endl;
		} else {
			std::cerr << "Taucs success" << std::endl;
		}

		//taucs_linsolve(&A,&F,1,x,b,NULL,NULL); //solve
	}

	void free_Taucs() {
		taucs_linsolve(NULL,&F,0,NULL,NULL,NULL,NULL); //free the factorization
	}
#endif


	void print() {
		std::cerr << "---- Compressed Column Matrix : ---" << std::endl;
		std::cerr << "---- Entries: ---" << std::endl;
		for (unsigned int i1 = 0; i1 < entries.size(); i1++) std::cerr << entries[i1] << " ";
		std::cerr << std::endl << "-----------------" << std::endl << "---- Col_ind: " << std::endl;
		for (unsigned int i1 = 0; i1 < col_ind.size(); i1++) std::cerr << col_ind[i1] << " ";
		std::cerr << std::endl << "-----------------" << std::endl << "---- Row_ptr: " << std::endl;
		//for (unsigned int i1 = 0; i1 < row_ptr.size(); i1++) std::cerr << row_ptr[i1] << " ";
		//std::cerr << std::endl << "-----------------" << std::endl;
		std::cerr << "---- numEntries: " << entries.size() << std::endl;
		std::cerr << "---- dim: " << dim << std::endl;
		std::cerr << std::endl;
	}

	void printToImage(char* filename) {

		Image* img = new Image(dim, dim);

		// Init with white:
		for (int x = 0; x < dim; x++)
			for (int y = 0; y < dim; y++)
				img->setPixel(x,y,255);

		// mark values as dots:
		int current_col_ptr_pos = 0;
		int current_row = 0;
		for (unsigned int i1 = 0; i1 < entries.size(); i1++) {
			int y = col_ind[i1];
			if ((int)i1 >= row_ptr[current_col_ptr_pos+1]) {
				current_col_ptr_pos++;
				current_row++;
			}
			int x = current_row;
			img->setPixel(x, y, 0);
		}

		img->writeRAW(filename);

		std::cerr << "Wrote " << dim << " x " << dim << " image: " << filename << std::endl;

		delete img;
	}

	//long getMemoryRequiredInMB() const {
	//	long mem_req = sizeof(std::vector<double>);
	//	mem_req = sizeof(std::vector<int>);
	//	mem_req = sizeof(std::vector<int>);
	//	mem_req += sizeof(double*) + 2*sizeof(void*);
	//	mem_req += (long)entries.size()*sizeof(double);
	//	mem_req += (long)col_ind.size()*sizeof(int);
	//	mem_req += (long)row_ptr.size()*sizeof(int);

	//	mem_req /= (1024*1024);
	//	return mem_req;
	//}

	std::vector<double> entries;
	std::vector<int> col_ind;
	std::vector<int> row_ptr;

	int dim;

private:

#ifdef USETAUCS
	taucs_ccs_matrix  A;
	void* F;
#endif

    double *null;
    void *Symbolic, *Numeric;

};

#endif
