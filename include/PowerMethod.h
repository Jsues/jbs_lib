#ifndef POWER_METHOD_H
#define POWER_METHOD_H

#include <vector>
#include <algorithm>
#include <limits>
#include <cmath>
#include <sstream>

#undef max

//! Performs 'numIter' iterations of the Power method to compute the largest Eigenvector of the matrix defined by (matrix^T matrix). Initial guess must be provided in 'seed'. Result will be returned in 'result'. Correctness verified using Matlab.
double getEVPower(const std::vector<std::vector<double>>& matrix, std::vector<double>& seed, std::vector<double>& result, int numIter) {

	std::vector<double>* eigenOld = &seed;
	std::vector<double>* eigenNew = &result;
	int numElements = (int)matrix[0].size();
	int numDataSets = (int)matrix.size();
	double EVal;
	double EValAlt = std::numeric_limits<double>::max();

	for (int i1 = 0; i1 < numIter; i1++) {

        std::vector<double>& eigenOldRef = *eigenOld;
        std::vector<double>& eigenNewRef = *eigenNew; 

        for(int i2 = 0; i2 < numDataSets; i2++) {

			double dot = 0;
			for (int i3 = 0; i3 < numElements; i3++) {
				dot += matrix[i2][i3]*eigenOldRef[i3];
			}

			for (int i3 = 0; i3 < numElements; i3++) {
				eigenNewRef[i3] += dot*matrix[i2][i3];
			}
        }

		EVal = 0;
		for (int i3 = 0; i3 < numElements; i3++) {
			eigenOldRef[i3] = 0;
			EVal += eigenNewRef[i3]*eigenNewRef[i3];
		}
		EVal = sqrt(EVal);

		if (fabs(EVal-EValAlt)/fabs(EVal) < 0.000000001) {
			//std::cerr << i1 << std::endl;
			i1 = numIter;
		}

		EValAlt = EVal;

		for (int i3 = 0; i3 < numElements; i3++) {
			eigenNewRef[i3] /= EVal;
		}

        std::vector<double>* tmp = eigenOld;
        eigenOld = eigenNew;
        eigenNew = tmp; 
	}

	result = (*eigenOld);
	return EVal;

}

//! Gets the 'numEVecs' most significant Eingenvectors&Eigenvalues of the matrix 'matrix'.
void getEVecs(std::vector<std::vector<double>>& matrix, int numEVecs, std::vector<std::vector<double>>& EVecs, std::vector<double>& EVals, int numIter=10, double sum=1.0) {

	int numElements = (int)matrix[0].size();
	int numDataSets = (int)matrix.size();

	if (numEVecs > numDataSets) numEVecs = numDataSets;


	// Compute total variance:
	double total_var = 0;
	for (int i1 = 0; i1 < numElements; i1++) {
		for (int i2 = 0; i2 < numDataSets; i2++)
			total_var += matrix[i2][i1]*matrix[i2][i1];
	}
	total_var /= sum;
	std::cerr << "Total variance: " << total_var << std::endl;


	//std::ofstream fout("evals.txt");

	double accu_var = 0;

	for (int i1 = 0; i1 < numEVecs; i1++) {

		//std::cerr << "Computing EV " << i1 << " of " << numEVecs << "\r";

		std::vector<double> seed;
		seed.resize(numElements);
		seed[1] = 1;

		std::vector<double> res;
		res.resize(numElements);

		double EVal = getEVPower(matrix, seed, res, numIter);
		EVal /= sum;
		//for (int i2 = 0; i2 < 20; i2++) std::cerr << res[i2] << std::endl;

#ifdef WRITE_FILES
		std::stringstream ss;
		ss << "evec_" << i1 << ".vec";
		std::ofstream fout(ss.str().c_str());
		//fout.write((char*)&EVal, sizeof(double));
		fout.write((char*)&numElements, sizeof(int));
		fout.write((char*)&res[0], sizeof(double)*numElements);


		std::stringstream ss2;
		ss2 << "eigenVector_" << i1 << ".vec";
		std::ofstream fout2(ss2.str().c_str(), std::ios::binary);
		//fout.write((char*)&EVal, sizeof(double));
		unsigned int lala = numElements;
		fout2.write((char*)&lala, sizeof(unsigned int));
		std::vector<float> xxxx;
		for (int i1 = 0; i1 < numElements; i1++) xxxx.push_back((float)res[i1]);
		fout2.write((char*)&xxxx[0], sizeof(float)*numElements);
#endif


		accu_var += EVal;
		double oftot = (accu_var / total_var)*100;

		std::cerr << "EigenValue " << EVal << ". sum is " << accu_var << " - " << oftot << "% of total variance" << std::endl;

		EVecs.push_back(res);
		EVals.push_back(EVal);

		for (int i2 = 0; i2 < numDataSets; i2++) {

			double dot = 0;
			for (int i3 = 0; i3 < numElements; i3++) {
				dot += res[i3]*matrix[i2][i3];
			}
			for (int i3 = 0; i3 < numElements; i3++) {
				matrix[i2][i3] -= dot*res[i3];
			}
		}

	}

}


#endif
