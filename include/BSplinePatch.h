#ifndef JBS_BSPLINEPATCH_H
#define JBS_BSPLINEPATCH_H


#include "JBS_General.h"
#include "knotVector.h"

//! A matrix of control-points.
template<class T>
class ControlGrid {
public:

// ****************************************************************************************

	//! Type of a Control-point, for example a 3D Vector of floats.
	typedef T ControlPoint;

	//! Value-Type of Templates components.
	typedef typename T::ValueType ValueType;

// ****************************************************************************************

	//! Copy constructor.
	ControlGrid(const ControlGrid<ControlPoint>& other) {

		dim_u = other.dim_u;
		dim_v = other.dim_v;

		UInt size = dim_u*dim_v;

		point1array = new ControlPoint[size];

		for (UInt i1 = 0; i1 < size; i1++) {
			point1array[i1] = other.point1array[i1];
		}
	}

// ****************************************************************************************

	ControlGrid(UInt dim_u_, UInt dim_v_, ControlPoint** pts) : dim_u(dim_u_), dim_v(dim_v_) {

		UInt size = dim_u*dim_v;

		point1array = new ControlPoint[size];

		for (UInt i1 = 0; i1 < dim_u; i1++) {
			for (UInt i2 = 0; i2 < dim_v; i2++) {
				point1array[i1*dim_v+i2] = pts[i1][i2];
			}
		}
	}

// ****************************************************************************************

	ControlGrid(UInt dim_u_, UInt dim_v_, std::vector<ControlPoint> pts) : dim_u(dim_u_), dim_v(dim_v_) {

		UInt size = dim_u*dim_v;

		point1array = new ControlPoint[size];

		for (UInt i1 = 0; i1 < size; i1++) {
			point1array[i1] = pts[i1];
		}
	}

// ****************************************************************************************

	~ControlGrid() {
		delete[] point1array;
	}

// ****************************************************************************************

	//! returns the derived control grid.
	ControlGrid<ControlPoint> getDerivedU(ValueType factor) {
		UInt new_dim_u = dim_u-1;
		UInt new_dim_v = dim_v;

		ControlPoint** new_points = new ControlPoint*[new_dim_u];
		for (UInt i1 = 0; i1 < new_dim_u; i1++)
			new_points[i1] = new ControlPoint[new_dim_v];

		for (UInt x = 0; x < new_dim_u; x++) {
			for (UInt y = 0; y < new_dim_v; y++) {
				new_points[x][y] = (get(x+1,y)-get(x,y))*factor;
			}
		}

		ControlGrid<ControlPoint> deriv(new_dim_u, new_dim_v, new_points);

		for (UInt i1 = 0; i1 < new_dim_u; i1++)
			delete[] new_points[i1];
		delete[] new_points;

		return deriv;
	}

// ****************************************************************************************

	//! returns the derived control grid.
	ControlGrid<ControlPoint> getDerivedV(ValueType factor) {
		UInt new_dim_u = dim_u;
		UInt new_dim_v = dim_v-1;

		ControlPoint** new_points = new ControlPoint*[new_dim_u];
		for (UInt i1 = 0; i1 < new_dim_u; i1++)
			new_points[i1] = new ControlPoint[new_dim_v];

		for (UInt x = 0; x < new_dim_u; x++) {
			for (UInt y = 0; y < new_dim_v; y++) {
				new_points[x][y] = (get(x,y+1)-get(x,y))*factor;
			}
		}

		ControlGrid<ControlPoint> deriv(new_dim_u, new_dim_v, new_points);

		for (UInt i1 = 0; i1 < new_dim_u; i1++)
			delete[] new_points[i1];
		delete[] new_points;

		return deriv;

	}

// ****************************************************************************************

	void print() {
		for (UInt x = 0; x < dim_u; x++) {
			for (UInt y = 0; y < dim_v; y++) {
				std::cerr << "(" << get(x,y) << ")   ";
			}
			std::cerr << std::endl;
		}
	}

// ****************************************************************************************

	//! returns the number of control points in u-direction.
	inline UInt getDimU() { return dim_u; };
	//! returns the number of control points in v-direction.
	inline UInt getDimV() { return dim_v; };
	//! returns control point (x,y).
	inline ControlPoint get(UInt x, UInt y) const {
		//assert((x < dim_u) && (y < dim_v));
		return point1array[y*dim_u+x];
	};
	//! returns control point (i).
	inline ControlPoint get(UInt i) const {
		return point1array[i];
	};
	//! control point (i) to vv;
	inline void set(UInt i, ControlPoint vv) {
		point1array[i] = vv;
	};
// ****************************************************************************************

	inline ControlPoint* getAllPts() {
		return point1array;
	}

private:

	UInt dim_u;
	UInt dim_v;

	ControlPoint* point1array;
};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


//! This class represents a single B-Spline surface patch.
template<class T>
class BSplinePatch {

public:

// ****************************************************************************************

	//! Type of a Control-point, for example a 3D Vector of floats.
	typedef T ControlPoint;

	//! Value-Type of Templates components.
	typedef typename T::ValueType ValueType;

// ****************************************************************************************

	//! empty Constructor, do not use.
	BSplinePatch();

// ****************************************************************************************

	//! Constructor
	/*!
	\param deg_u_ Degree in u direction.
	\param deg_v_ Degree in v direction.
	\param knot_u_ Knot-vector in u direction.
	\param knot_v_ Knot-vector in v direction.
	\param ctrl_grid_ Control-Grid
	*/
	BSplinePatch(UInt deg_u_, UInt deg_v_, std::vector<ValueType> knot_u_, std::vector<ValueType> knot_v_, ControlGrid<ControlPoint> ctrl_grid_) : ctrl_pts(ctrl_grid_) {

		n = ctrl_pts.getDimU();
		m = ctrl_pts.getDimV();
		deg_u = deg_u_;
		deg_v = deg_v_;
		knot_u = KnotVector<ValueType>(deg_u, knot_u_);
		knot_v = KnotVector<ValueType>(deg_v, knot_v_);
	}

// ****************************************************************************************

	//! Destructor, does some cleanup.
	~BSplinePatch() {

	}
// ****************************************************************************************

	ControlPoint eval(ValueType u, ValueType v) {
		ControlPoint ret;
		for (UInt i = 0; i < n; i++) {
			ValueType basis_i = knot_u.evalBasis(i, v);
			for (UInt j = 0; j < m; j++) {
				ValueType basis_j = knot_v.evalBasis(j, u);
				ret = ret + ctrl_pts.get(i,j) * basis_i * basis_j;
			}
		}
		return ret;
	}

// ****************************************************************************************

	//! Calculates the diagonal of the extend.
	ValueType getDiagonal() {
		ControlPoint min;
		ControlPoint max;

		min = ctrl_pts.get(0,0)[0];
		max = ctrl_pts.get(0,0)[1];

		for (UInt x = 0; x < n; x++) {
			for (UInt y = 0; y < m; y++) {
				ControlPoint current = ctrl_pts.get(x,y);
				if (current[0] < min[0])
					min[0] = current[0];
				if (current[1] < min[1])
					min[1] = current[1];
				if (current[2] < min[2])
					min[2] = current[2];
				if (current[0] > max[0])
					max[0] = current[0];
				if (current[1] > max[1])
					max[1] = current[1];
				if (current[2] > max[2])
					max[2] = current[2];
			}
		}

		ControlPoint diag = max-min;

		return diag.length();
	}

// ****************************************************************************************

	//! returns the number of controlpoints in u-direction.
	int getNumCtrlPtsU() {
		return m;
	};

// ****************************************************************************************

	//! returns the number of controlpoints in v-direction.
	int getNumCtrlPtsV() {
		return n;
	};

// ****************************************************************************************

	//! returns the control-grid array.
	ControlGrid<ControlPoint>* getCtrlGrid() {
		return &ctrl_pts;
	};

// ****************************************************************************************

	//! returns the control-grid array.
	ControlGrid<ControlPoint> getCtrlPts() {
		return ctrl_pts;
	};

// ****************************************************************************************

	//! Returns the ParameterRange in u-direction
	point2d<ValueType> getParamRangeU() {
		return knot_u.getRange();
	}

// ****************************************************************************************

	//! Returns the ParameterRange in v-direction
	point2d<ValueType> getParamRangeV() {
		return knot_v.getRange();
	}

// ****************************************************************************************

	//! Returns a pointer to the derivative in u-direction
	BSplinePatch<ControlPoint>* getDerivative_u() {

		ControlGrid<ControlPoint> new_ctrl_pts = ctrl_pts.getDerivedU((ValueType)deg_u);

		// Calculate new knot-vector for u-direction:
		std::vector<float> new_knot_m;
		for (unsigned int i1 = 1; i1 < knot_u.size()-1; i1++)
			new_knot_m.push_back(knot_u[i1]);

		BSplinePatch<T>* deriv_u = new BSplinePatch<T>(deg_u-1, deg_v, new_knot_m, knot_v.knot, new_ctrl_pts);
		return deriv_u;
	}

// ****************************************************************************************

	//! Returns a pointer to the derivative in v-direction
	BSplinePatch<ControlPoint>* getDerivative_v() {

		ControlGrid<ControlPoint> new_ctrl_pts = ctrl_pts.getDerivedV((ValueType)deg_v);

		// Calculate new knot-vector for v-direction:
		std::vector<float> new_knot_n;
		for (unsigned int i1 = 1; i1 < knot_v.size()-1; i1++)
			new_knot_n.push_back(knot_v[i1]);

		BSplinePatch<T>* deriv_v = new BSplinePatch<T>(deg_u, deg_v-1, knot_u.knot, new_knot_n, new_ctrl_pts);
		return deriv_v;
	}

// ****************************************************************************************

	//! Prints out the ctrl_pts
	void print() {
		std::cerr << "BSpline-Patch: degree_u=" << deg_u << " degree_v=" << deg_v << std::endl;

		ctrl_pts.print();
		std::cerr << "knot_u:";
		knot_u.print();
		std::cerr << "knot_v:";
		knot_v.print();
		std::cerr << "-------" << std::endl;
	};

// ****************************************************************************************

	//! Returns the degree in 'u'-direction
	inline UInt getDegreeU() const {
		return deg_u;
	}

// ****************************************************************************************

	//! Returns the degree in 'v'-direction
	inline UInt getDegreeV() const {
		return deg_v;
	}

// ****************************************************************************************

	inline KnotVector<ValueType>* getKnotU() {
		return &knot_u;
	}

// ****************************************************************************************

	inline KnotVector<ValueType>* getKnotV() {
		return &knot_v;
	}

// ****************************************************************************************

private:

	//! The Control-grid.
	ControlGrid<ControlPoint> ctrl_pts;

	//! Knot vector in u-direction.
	KnotVector<ValueType> knot_u;

	//! Knot vector in v-direction.
	KnotVector<ValueType> knot_v;

	//! Number of control-points in u-direction.
	UInt n;
	//! Number of control-points in v-direction.
	UInt m;

	//! Degree in u-direction.
	UInt deg_u;
	//! Degree in v-direction.
	UInt deg_v;

};









//*******************************************************************************************************************************
//************************************************ OLD STUFF DEPRECATED!!!!!!!! ************************************************
//*******************************************************************************************************************************


////! This class represents a single B-Spline surface patch. Beware, it's still highly buggy!!!
///*!
//  - Number of Control-points in u-direction = 'm_num_m'+1.
//  - Number of Control-points in v-direction = 'm_num_n'+1.
// */
//template<class T>
//class BSplinePatch {
//
//
//public:
//
//	////! Type of a Control-point, for example a 3D Vector of floats.
//	//typedef T ControlPoint;
//
//	//! Type of a Point, for example a 3D Vector of floats.
//	typedef T Point;
//
//	////! Type of a Vector, for example a 3D Vector of floats.
//	//typedef T Vector;
//
//	//! Value-Type of Templates components (i.e. float or double).
//	typedef typename T::ValueType ValueType;
//
//	//! empty Constructor, do not use.
//	BSplinePatch();
//
//	//! Constructor
//	/*!
//	\param n (Number of control-points)-1 in u direction.
//	\param m (Number of control-points)-1 in v direction.
//	\param deg_u Degree in u direction.
//	\param deg_v Degree in v direction.
//	\param knot_m Knot-vector in u direction.
//	\param knot_n Knot-vector in v direction.
//	\param pts Array of control-points; pts.size must be equal to (n+1)(m+1)!!
//	*/
//	BSplinePatch(UInt n, UInt m, UInt deg_u, UInt deg_v, std::vector<float> knot_m, std::vector<float> knot_n, std::vector<Point>& pts);
//
//	//! Evaluates the Patch at (u,v), TODO, fix boundary Problem (u|v=1)!.
//	T eval(float u, float v);
//
//	//! Assigns new ctrl-pts to the patch
//	void set(UInt deg_n, UInt deg_m, std::vector<T> n_pts) {
//		for (UInt i1 = 0; i1 < n_pts.size(); i1++)
//			m_pts[i1] = n_pts[i1];
//	};
//
//	//! Returns the derived Surface in u-direction
//	BSplinePatch<T>* getDerivative_u();
//
//	//! Returns the derived Surface in v-direction
//	BSplinePatch<T>* getDerivative_v();
//
//	//! Prints out the ctrl_pts
//	void print() {
//		std::cerr << "BSpline-Patch: degree_u=" << m_order_u << " degree_v=" << m_order_v << std::endl;
//		for (unsigned int i = 0; i <= m_num_n; i++) {
//			for (unsigned int j = 0; j <= m_num_m; j++) {
//				//	cerr << setprecision (2) << "(" << m_pts[i*(m_num_m+1)+j][0] << ", " <<  m_pts[i*(m_num_m+1)+j][1] << ", " << m_pts[i*(m_num_m+1)+j][2] << ")   ";
//				std::cerr << "(" << m_pts[i*(m_num_m+1)+j][0] << ", " <<  m_pts[i*(m_num_m+1)+j][1] << ", " << m_pts[i*(m_num_m+1)+j][2] << ")   ";
//			}
//			std::cerr << std::endl;
//		}
//		std::cerr << "knot_u:";
//		for (unsigned int i1 = 0; i1 < m_knot_m.size(); i1++)
//			std::cerr << " " << m_knot_m[i1];
//		std::cerr << std::endl;
//		std::cerr << "knot_v:";
//		for (unsigned int i1 = 0; i1 < m_knot_n.size(); i1++)
//			std::cerr << " " << m_knot_n[i1];
//		std::cerr << std::endl;
//		std::cerr << "-------" << std::endl;
//	};
//
//	//! TODO: Not yet implemented correctly.
//	void calculateRidges();
//
//	BSplineCurve<T> *curve_u, *curve_v;
//
//	//! returns the number of controlpoints in u-direction.
//	int getNumCtrlPtsU() {
//		return m_num_n+1;
//	};
//
//	//! returns the number of controlpoints in v-direction.
//	int getNumCtrlPtsV() {
//		return m_num_m+1;
//	};
//
//	//! returns a pointer to the controlpoint array.
//	Point* getCtrlPts() {
//		return &m_pts[0];
//	};
//
//	//! Calculates the diagonal of the extend.
//	ValueType getDiagonal() {
//		Point min;
//		Point max;
//
//		min = m_pts[0];
//		max = m_pts[0];
//
//		for (UInt i1 = 1; i1 < m_pts.size(); i1++) {
//			Point current = m_pts[i1];
//			if (current[0] < min[0])
//				min[0] = current[0];
//			if (current[1] < min[1])
//				min[1] = current[1];
//			if (current[2] < min[2])
//				min[2] = current[2];
//			if (current[0] > max[0])
//				max[0] = current[0];
//			if (current[1] > max[1])
//				max[1] = current[1];
//			if (current[2] > max[2])
//				max[2] = current[2];
//		}
//
//		Point diag = max-min;
//
//		return diag.length();
//	}
//
//	//! Returns the degree of the patch in u-direction
//	int getDegreeU() {
//		return m_order_u;
//	}
//
//	//! Returns the degree of the patch in v-direction
//	int getDegreeV() {
//		return m_order_v;
//	}
//
//	//! Returns the knot-vector in u-direction
//	std::vector<float> getKnotU() {
//		return m_knot_m;
//	}
//
//	//! Returns the knot-vector in v-direction
//	std::vector<float> getKnotV() {
//		return m_knot_n;
//	}
//
//	//! Returns the ParameterRange in u-direction
//	vec2f getParamRangeU() {
//		return vec2f(m_knot_m[m_order_u], m_knot_m[m_knot_m.size()-1-m_order_u]);
//	}
//
//	//! Returns the ParameterRange in v-direction
//	vec2f getParamRangeV() {
//		return vec2f(m_knot_n[m_order_v], m_knot_n[m_knot_n.size()-1-m_order_v]);
//	}
//
//private:
//
//
//	//! Knot-Vector in 'u'-direction
//	std::vector<float> m_knot_m;
//
//	//! Knot-Vector in 'v' direction
//	std::vector<float> m_knot_n;
//
//	//! Controlpoints
//	std::vector<Point> m_pts;
//
//	//! Polynomial Order in 'u'-direction
//	UInt m_order_u;
//	//! Polynomial Order in 'v'-direction
//	UInt m_order_v;
//
//	//! Number of control-points in u direction
//	UInt m_num_n;
//	//! Number of control-points in v direction
//	UInt m_num_m;
//
//};
//
//
//
//// ###########################################################################
//// ###########################   IMPLEMENTIERUNG   ###########################
//// ###########################################################################
//
//
//
//template<class T>
//BSplinePatch<T>::BSplinePatch(UInt m, UInt n, UInt deg_u, UInt deg_v, std::vector<float> knot_m, std::vector<float> knot_n, std::vector<Point>& pts) {
//	//std::cerr << "BSplinePatch Construktor called" << std::endl;
//	//std::cerr << "BSplinePatch( " << m << ", " << n << ", " << deg_u << ", " << deg_v << ", " << knot_m.size() << ", " << knot_n.size() << ", ..." << std::endl;
//
//	// Check if all restrictions are fullfilled.
//	int corr_knot_size_m = m+deg_u+2;
//	if (knot_m.size() != corr_knot_size_m) {
//		std::cerr << "Knot Vector in u direction is of wrong size (" << (int)knot_m.size() << ") should have been " << corr_knot_size_m << std::endl;
//		exit(EXIT_FAILURE);
//	}
//	int corr_knot_size_n = n+deg_v+2;
//	if (knot_n.size() != corr_knot_size_n) {
//		std::cerr << "Knot Vector in v direction is of wrong size (" << (int)knot_n.size() << ") should have been " << corr_knot_size_n << std::endl;
//		exit(EXIT_FAILURE);
//	}
//	if (pts.size() != (m+1)*(n+1)) {
//		std::cerr << "Number of control-points: " << (m+1)*(n+1) << " doesnt match pts.size()" <<  (int)pts.size() << std::endl;
//		exit(EXIT_FAILURE);
//	}
//	m_num_n = n;
//	m_num_m = m;
//	m_order_u = deg_u;
//	m_order_v = deg_v;
//	m_knot_m = knot_m;
//	m_knot_n = knot_n;
//	m_pts = pts;
//
//	// Check whether the patch is supposed to be end-point interpolating
//	bool is_end_point_interpolating_m;
//	for (UInt i1 = 1; i1 < deg_u-1; i1++)
//		if (knot_m[i1] != knot_m[i1+1])
//			is_end_point_interpolating_m = false;
//	for (UInt i1 = (UInt)m_knot_m.size()-1-deg_u; i1 < m_knot_m.size()-2; i1++)
//		if (knot_m[i1] != knot_m[i1+1])
//			is_end_point_interpolating_m = false;
//	if (is_end_point_interpolating_m) {
//		std::cerr << "Knot vector is endpoint interpolating in u-direction" << std::endl;
//		m_knot_m[0] = m_knot_m[0]-1;
//		m_knot_m[m_knot_m.size()-1] = m_knot_m[m_knot_m.size()-1]+1;
//	}
//
//	bool is_end_point_interpolating_n;
//	for (UInt i1 = 1; i1 < deg_v-1; i1++)
//		if (knot_n[i1] != knot_n[i1+1])
//			is_end_point_interpolating_n = false;
//	for (UInt i1 = (UInt)m_knot_n.size()-1-deg_v; i1 < m_knot_n.size()-2; i1++)
//		if (knot_n[i1] != knot_n[i1+1])
//			is_end_point_interpolating_n = false;
//	if (is_end_point_interpolating_n) {
//		std::cerr << "Knot vector is endpoint interpolating in v-direction" << std::endl;
//		m_knot_n[0] = m_knot_n[0]-1;
//		m_knot_n[m_knot_n.size()-1] = m_knot_n[m_knot_n.size()-1]+1;
//	}
//
//	//! Dummy curves for Basis evaluation
//	std::vector<T> dummy_m;
//	for (UInt i1 = 0; i1 <= m; i1++) {
//		dummy_m.push_back(pts[0]);
//	}
//	std::vector<T> dummy_n;
//	for (UInt i1 = 0; i1 <= n; i1++) {
//		dummy_n.push_back(pts[0]);
//	}
//
//	curve_u = new BSplineCurve<T>(deg_u, dummy_m);
//	curve_v = new BSplineCurve<T>(deg_v, dummy_n);
//
//
//    print();
//}
//
//
//template<class T>
//BSplinePatch<T>::BSplinePatch() {
//	std::cerr << "Leerer Konstruktor" << std::endl;
//}
//
//
//
//template<class T>
//BSplinePatch<T>* BSplinePatch<T>::getDerivative_v() {
//
//	int new_num_n = m_num_n - 1;
//	int new_order_v = m_order_v - 1;
//
//	std::vector<T> pnt;
//	// Construct Array of new Ctrl-Pts.
//	for (UInt j = 0; j < m_num_n; j++) {
//		for (UInt i = 0; i <= m_num_m; i++) {
//			pnt.push_back((m_pts[(j+1)*(m_num_m+1)+i] - m_pts[j*(m_num_m+1)+i]) * (float)new_order_v);
//		}
//	}
//
//	// Calculate new knot-vector for v-direction:
//	std::vector<float> new_knot_n;
//	for (unsigned int i1 = 1; i1 < m_knot_n.size()-1; i1++)
//		new_knot_n.push_back(m_knot_n[i1]);
//
//	BSplinePatch<T>* deriv_v = new BSplinePatch<T>(m_num_m, new_num_n, m_order_u, new_order_v, m_knot_m, new_knot_n, pnt);
//	return deriv_v;
//}
//
//
//
//template<class T>
//BSplinePatch<T>* BSplinePatch<T>::getDerivative_u() {
//
//	int new_num_m = m_num_m - 1;
//	int new_order_u = m_order_u - 1;
//
//	std::vector<T> pnt;
//	// Construct Array of new Ctrl-Pts.
//	for (UInt j = 0; j <= m_num_n; j++) {
//		for (UInt i = 0; i < m_num_m; i++) {
//			pnt.push_back((m_pts[j*(m_num_m+1)+i+1] - m_pts[j*(m_num_m+1)+i]) * (float)new_order_u);
//		}
//	}
//
//	// Calculate new knot-vector for u-direction:
//	std::vector<float> new_knot_m;
//	for (unsigned int i1 = 1; i1 < m_knot_m.size()-1; i1++)
//		new_knot_m.push_back(m_knot_m[i1]);
//
//	BSplinePatch<T>* deriv_u = new BSplinePatch<T>(new_num_m, m_num_n, new_order_u, m_order_v, new_knot_m, m_knot_n, pnt);
//	return deriv_u;
//}
//
//
//
//template<class T>
//T BSplinePatch<T>::eval(float u, float v) {
//
//	T ret(0,0,0);
//
//	for (UInt i = 0; i <= m_num_m; i++) {
//
//		float basis_i = curve_u->Basis(i, m_order_u, u);
//		// cerr << "Row " << i << ": N_i(" << i << ", " << m_order_u << ", " << u << ") = " << basis_i << endl;
//
//		for (UInt j = 0; j <= m_num_n; j++) {
//			float basis_j = curve_v->Basis(j, m_order_v, v);
//			ret = ret + m_pts[i*(m_num_m+1)+j] * basis_i * basis_j;
//			// cerr << "N_j(" << j << ", " << m_order_v << ", " << v << ") = " << basis_j << endl;
//		}
//	}
//
//	// cerr << "f(" << u << "," << v << ") -> " << ret << endl << endl;
//	// cerr << ret << endl;
//	return ret;
//}
//
//template<class T>
//void BSplinePatch<T>::calculateRidges() {
//
//	cerr << "Calculate ridges" << endl;
//	// First, we need the derivatives up to degree 2:
//	  
//	BSplinePatch<T>* Fu = this->getDerivative_u();
//	BSplinePatch<T>* Fv = this->getDerivative_v();
//	BSplinePatch<T>* Fuu = Fu->getDerivative_u();
//	BSplinePatch<T>* Fuv = Fu->getDerivative_v();
//	BSplinePatch<T>* Fvv = Fv->getDerivative_v();
//
//	float e,f,g;
//	/*  I_FF = ( e f 
//				f g ) */
//
//	// Werte aus:
//	float dx = 0.05;
//	float dy = 0.05;
//
//	int num_x = ((1.0/dx)+1);
//	int num_y = ((1.0/dy)+1);
//
//	T* iso_function = new T[num_x*num_y];
//	int u = 0, v = 0;
//	for (float x = 0; x <= 1; x += dx) {
//		v = 0;
//		for (float y = 0; y <= 1; y += dy) {
//
//		T fu = Fu->eval(x,y);
//		T fv = Fv->eval(x,y);
//		T fuu = Fuu->eval(x,y);
//		T fuv = Fuv->eval(x,y);
//		T fvv = Fvv->eval(x,y);
//
//		T norm = fu ^ fv;
//
//		// Compute the first fundamental form:
//		float e = fu * fu;
//		float f = fu * fv;
//		float g = fv * fv;
//
//		// And the second fundamental form:
//		float l = norm * fuu;
//		float m = norm * fuv;
//		float n = norm * fvv;
//
//		// Now the Weingarten-Map:
//		float A = g*l+f*m;
//		float B = g*m+f*n;
//		float C = f*l+e*m;
//		float D = f*m+e*n;
//	      
//		float iso = A*fu*fu + B*fv*fv + C*fu*fu + D*fu*fv;
//		cerr << iso << endl;
//		iso_function[u*(num_x)+v] = T(x,y,iso);
//		v++;
//		}
//		u++;
//	}
//
//}


#endif //JBS_BSPLINEPATCH_H

