#ifndef MESH_TO_IV_SPECIALS_H
#define MESH_TO_IV_SPECIALS_H


#include "CurvatureMesh.h"
#include "MeshToIV.h"


//****************************************************************************************************


SoSeparator* meshToIvColorByCurvature(CurvatureMesh* mesh, float crease = 10.0f) {
	SoSeparator* ivmesh = new SoSeparator();

	SoShapeHints* sh = new SoShapeHints();
	sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	sh->creaseAngle = crease;
	ivmesh->addChild(sh);


	if (mesh->curvature == NULL)
		mesh->ComputeVertexCurvatures();

	double min = mesh->min_curv; ///2.0;
	double max = mesh->max_curv; ///2.0;

	double max_c = (fabs(min) > fabs(max)) ? fabs(min) : fabs(max);

	std::cerr << "Curvature range: " << min << " to " << max << std::endl;

	double factor = 0.333;

	int numPts = mesh->getNumV();
	// Create points:
	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->getVertex(i1);
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);
	delete[] Points;

	SoMaterial* mat = new SoMaterial;
	for (int i1 = 0; i1 < numPts; i1++) {
		float range = fabs(mesh->curvature[i1]) / max_c;
		float col = float (range*factor);
		//SbColor color(0.7+(0.3*range), 0.7-(0.7*range), 0.7-(0.7*range));
		SbColor color; color.setHSVValue(range, 1, 1);
		mat->diffuseColor.set1Value(i1, color);
	}

	ivmesh->addChild(mat);
	SoMaterialBinding* matbind = new SoMaterialBinding;
	matbind->value = SoMaterialBinding::PER_VERTEX_INDEXED;
	ivmesh->addChild(matbind);

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(numTris*4);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = mesh->tList[i1]->v0();
		int v1 = mesh->tList[i1]->v1();
		int v2 = mesh->tList[i1]->v2();
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
	}
	ivmesh->addChild(ifs);

	return ivmesh;
}

//****************************************************************************************************


#endif