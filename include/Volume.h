#ifndef JBS_VOLUME_H
#define JBS_VOLUME_H


#include "JBS_General.h"
#include "point3d.h"


template<class T>
inline void swapEndian(T& val) {

	UInt size = sizeof(T);

	char *p = (char *)&val;
	char t;
	for (UInt i1 = 0; i1 < size/2; i1++) {
		t = p[size-i1-1];
		p[size-i1-1] = p[i1];
		p[i1] = t;
	}

}



//******************************************************************************************

template<class T> class Volume;

template<class T>
vec3i findMultiResMaximum(const Volume<T>* const vol) {

	vec3i maxPos;

	std::vector< const Volume<T>* > stack;
	stack.push_back(vol);
	while (true) {
		const Volume<T>* const current_vol = stack.back();
		if (current_vol->dx == 1 || current_vol->dy == 1 || current_vol->dz == 1) break;
		stack.push_back(current_vol->getDownsampled());
	}

	maxPos = stack.back()->getPosOfMaxEntry();
	//maxPos.print();
	for (int i1 = (int)stack.size()-2; i1 >= 0; i1--) {
		const Volume<T>* const current_vol = stack[i1];
		maxPos *= 2;
		vec3i oldMaxPos(maxPos);
		T maxVal = 0;
		for (int x = oldMaxPos.x; x <= oldMaxPos.x+1; x++) {
			for (int y = oldMaxPos.y; y <= oldMaxPos.y+1; y++) {
				for (int z = oldMaxPos.z; z <= oldMaxPos.z+1; z++) {
					T val = current_vol->get(x,y,z);
					if (val > maxVal) {
						maxVal = val;
						maxPos = vec3i(x,y,z);
					}
				}
			}
		}
		//maxPos.print();
	}
	for (int i1 = (int)stack.size()-1; i1 > 0; i1--) {
		delete stack[i1];
	}

	return maxPos;
}

//******************************************************************************************


//! A regular volume dataset
template<class T>
class Volume
{
public:

//******************************************************************************************

	//! Type of a volume element, for example 'float' or 'double'.
	typedef T ValueType;

//******************************************************************************************

	Volume() {
		vol = NULL;
	}

//******************************************************************************************

	Volume(vec3f min_, vec3f max_, UInt dx_ = 10, UInt dy_ = 10, UInt dz_ = 10, unsigned int dim = 1) {
		min = min_;
		max = max_;
		diag = max-min;
		dx = dx_;
		dy = dy_;
		dz = dz_;
		m_dim = dim;
		//maxValue = T(0);
		//minValue = T(0);
		diag = max-min;

		vol = NULL;
	}

//******************************************************************************************

	~Volume(void) {
		// Clean up
#ifdef DEBUG
		std::cerr << "Cleaning volume...";
#endif
		delete[] vol;
#ifdef DEBUG
		std::cerr << "done" << std::endl;
#endif
	};

//******************************************************************************************

	void initWithDims(vec3d min_, vec3d max_, UInt dx_ = 10, UInt dy_ = 10, UInt dz_ = 10, unsigned int dim = 1) {
		initWithDims(vec3f((float)min_.x, (float)min_.y, (float)min_.z), vec3f((float)max_.x, (float)max_.y, (float)max_.z), dx_, dy_, dz_, dim);
	}

//******************************************************************************************

	void initWithDims(vec3f min_, vec3f max_, UInt dx_ = 10, UInt dy_ = 10, UInt dz_ = 10, unsigned int dim = 1) {
		min = min_;
		max = max_;
		diag = max-min;
		dx = dx_;
		dy = dy_;
		dz = dz_;
		m_dim = dim;
		//maxValue = T(0);
		//minValue = T(0);

		init();
	}

//******************************************************************************************

	inline void computeMinMaxValues(ValueType& minVal, ValueType& maxVal) const {
		minVal = std::numeric_limits<ValueType>::max();
		maxVal = -minVal;
		for (UInt i1 = 0; i1 < dx*dy*dz; i1++) {
			if (minVal > vol[i1]) minVal = vol[i1];
			if (maxVal < vol[i1]) maxVal = vol[i1];
		}
	}

//******************************************************************************************

	//! Allocates space for the cells, i.e. calls new()s.
	void compute_ddx_dddx() {

		ddx = 1.0f / (dx-1);
		ddy = 1.0f / (dy-1);
		ddz = 1.0f / (dz-1);

		dddx = (max.x-min.x) / (dx-1);
		dddy = (max.y-min.y) / (dy-1);
		dddz = (max.z-min.z) / (dz-1);

		if (dz == 1) {
			ddz = 0;
			dddz = 0;
		}

		diag = max-min;

	};

//******************************************************************************************

	//! Zeros out the memory
	void zeroOutMemory() {
		for (UInt i1 = 0; i1 < dx*dy*dz; i1++)
			vol[i1] = ValueType(0);
	}

//******************************************************************************************

	//! Allocates space for the cells, i.e. calls new()s.
	void init() {
		//std::cerr << "init";
		vol = new ValueType[dx*dy*dz];
		//std::cerr << "done" << std::endl;

		ddx = 1.0f / (dx-1);
		ddy = 1.0f / (dy-1);
		ddz = 1.0f / (dz-1);

		dddx = (max.x-min.x) / (dx-1);
		dddy = (max.y-min.y) / (dy-1);
		dddz = (max.z-min.z) / (dz-1);
		diag = max-min;


		if (dz == 1) {
			ddz = 0;
			dddz = 0;
		}

	};

//******************************************************************************************

	//! Adds 'val' to the value at (i).
	inline void add(UInt i, ValueType val) {
		vol[i] += val;
	}

//******************************************************************************************

	//! Adds 'val' to the value at (x_, y_, z_).
	inline void add(UInt x_, UInt y_, UInt z_, ValueType val) {

#ifdef DEBUG
		if ((x_ < 0) || (x_ >= dx) || (y_ < 0) || (y_ >= dy) || (z_ < 0) || (z_ >= dz)) {
			std::cerr << "Volume::set(...) : out of range access!" << std::endl;
			exit(EXIT_FAILURE);
		}
#endif
		//if (val > maxValue)
		//	maxValue = val;
		//if (val < minValue)
		//	minValue = val;

		vol[getPosFromTuple(x_,y_,z_)] += val;
	};

//******************************************************************************************

	//! Set the value at i.
	inline void set(UInt i, ValueType val) {

		if (val > maxValue)
			maxValue = val;
		if (val < minValue)
			minValue = val;

		vol[i] = val;
	};

//******************************************************************************************

	//! Set the value at (x_, y_, z_).
	inline void set(UInt x_, UInt y_, UInt z_, ValueType val) {

#ifdef DEBUG
		if ((x_ < 0) || (x_ >= dx) || (y_ < 0) || (y_ >= dy) || (z_ < 0) || (z_ >= dz)) {
			std::cerr << "Volume::set(...) : out of range access!" << std::endl;
			std::cerr << "(" << x_ << ", " << y_ << ", " << z_ << ") of (" << dx << ", " << dy << ", " << dz << ") requested" << std::endl;
			exit(EXIT_FAILURE);
		}
#endif
		if (val > maxValue)
			maxValue = val;
		if (val < minValue)
			minValue = val;

		vol[getPosFromTuple(x_,y_,z_)] = val;
	};

//******************************************************************************************

	//! Get the value at (x_, y_, z_).
	inline ValueType get(UInt i) const {
		return vol[i];
	};

//******************************************************************************************

	//! Get the value at (x_, y_, z_).
	inline ValueType get(UInt x_, UInt y_, UInt z_) const {
#ifdef DEBUG
		if ((x_ < 0) || (x_ >= dx) || (y_ < 0) || (y_ >= dy) || (z_ < 0) || (z_ >= dz)) {
			std::cerr << "Volume::get(...) : out of range access!" << std::endl;
			std::cerr << "(" << x_ << ", " << y_ << ", " << z_ << ") of (" << dx << ", " << dy << ", " << dz << ") requested" << std::endl;
			exit(EXIT_FAILURE);
		}
#endif
		return vol[getPosFromTuple(x_,y_,z_)];
	};

//******************************************************************************************

	//! Get the value at (pos.x, pos.y, pos.z).
	inline ValueType get(const vec3i& pos_) const {
		return(get(pos_.x, pos_.y, pos_.z));
	};

//******************************************************************************************

	vec3i getPosOfMaxEntry() const {

		vec3i maxPos(0,0,0);
		ValueType maxVal = get(0,0,0);

		for (UInt x = 0; x < dx; x++) {
			for (UInt y = 0; y < dy; y++) {
				for (UInt z = 0; z < dz; z++) {
					ValueType val = get(x,y,z);
					if (val > maxVal) {
						maxVal = val;
						maxPos = vec3i(x,y,z);
					}
				}
			}
		}

		return maxPos;
	}

//******************************************************************************************

	inline vec3f worldPosToIndices(vec3f worldPos) {

		//min.print();
		//worldPos.print();
		//diag.print();

		worldPos -= min;

		worldPos.x /= diag.x;
		worldPos.y /= diag.y;
		worldPos.z /= diag.z;

		worldPos.x *= (dx-1);
		worldPos.y *= (dy-1);
		worldPos.z *= (dz-1);

		return worldPos;
	}

//******************************************************************************************

	//! Returns the cartesian x-coordinates of node (i,..).
	inline float posX(int i) const {
		return min[0] + diag[0]*(float(i)*ddx);;
	};

//******************************************************************************************

	//! Returns the cartesian y-coordinates of node (..,i,..).
	inline float posY(int i) const {
		return min[1] + diag[1]*(float(i)*ddy);;
	};

//******************************************************************************************

	//! Returns the cartesian z-coordinates of node (..,i).
	inline float posZ(int i) const {
		return min[2] + diag[2]*(float(i)*ddz);;
	};

//******************************************************************************************

	//! Returns the cartesian coordinates of node (i,j,k).
	inline vec3f posf(float i, float j, float k) const {
		vec3f coord(0,0,0);

		coord[0] = min[0] + (max[0]-min[0])*(float(i)*ddx);
		coord[1] = min[1] + (max[1]-min[1])*(float(j)*ddy);
		coord[2] = min[2] + (max[2]-min[2])*(float(k)*ddz);
		return coord;
	};

//******************************************************************************************

	//! Returns the cartesian coordinates of node (i,j,k).
	inline vec3f pos(int i, int j, int k) const {
		vec3f coord(0,0,0);

		coord[0] = min[0] + (max[0]-min[0])*(float(i)*ddx);
		coord[1] = min[1] + (max[1]-min[1])*(float(j)*ddy);
		coord[2] = min[2] + (max[2]-min[2])*(float(k)*ddz);
		return coord;
	};

//******************************************************************************************

	//! Computes a reduced (1/2x1/2x1/2) version of the volume.
	Volume<ValueType>* getDownsampled() const {

		// Check if all dimensions a multiples of 2:
		if ((dx%2!=0) || (dy%2!=0) || (dz%2!=0)) {
			std::cerr << "Error in Volume<T>::getDownsampled()" << std::endl;
			std::cerr << "one dimension is not a multiple of 2!" << std::endl;
			std::cerr << "exit" << std::endl;
			exit(EXIT_FAILURE);
		}

		Volume<ValueType>* redvol = new Volume<ValueType>(min, max, dx/2, dy/2, dz/2);
		redvol->init();

#ifdef DEBUG
		std::cerr << "Downsampling to " << (int)(dx/2) << " " << (int)(dy/2) << " " << (int)(dz/2);
#endif
		for (int x = 0; x < (int)dx/2; x++) {
#ifdef DEBUG
			std::cerr << ".";
#endif
			for (int y = 0; y < (int)dy/2; y++) {
				for (int z = 0; z < (int)dz/2; z++) {
					ValueType val = get(vec3i(x*2, y*2, z*2)) + get(vec3i(x*2, y*2, z*2+1))
								  + get(vec3i(x*2, y*2+1, z*2)) + get(vec3i(x*2, y*2+1, z*2+1))
								  + get(vec3i(x*2+1, y*2, z*2)) + get(vec3i(x*2+1, y*2, z*2+1))
								  + get(vec3i(x*2+1, y*2+1, z*2)) + get(vec3i(x*2+1, y*2+1, z*2+1));
					val = val / 8;
					redvol->set(x,y,z, val);
				}
			}
		}
#ifdef DEBUG
		std::cerr << "done" << std::endl;
#endif

		return redvol;
	}

//******************************************************************************************

	void swapByteOrder() {
		UInt size = sizeof(ValueType);


		std::cerr << "Swapping byte order...";
		for (UInt i1 = 0; i1 < dx; i1++) {
			for (UInt i2 = 0; i2 < dx; i2++) {
				for (UInt i3 = 0; i3 < dx; i3++) {
					swapEndian(vol[getPosFromTuple(i1,i2,i3)]);
				}
			}
		}
		std::cerr << "done" << std::endl;

	}

//******************************************************************************************

	//! Returns the Data.
	ValueType* getData() {
		return vol;
	};

//******************************************************************************************

	//! Sets all entries in the volume to '0'
	void clean() {
		for (UInt i1 = 0; i1 < dx*dy*dz; i1++) vol[i1] = ValueType(0.0);
	}

//******************************************************************************************

	//! Returns number of grids in x-dir.
	inline UInt getDimX() const { return dx; };
	//! Returns number of grids in y-dir.
	inline UInt getDimY() const { return dy; };
	//! Returns number of grids in z-dir.
	inline UInt getDimZ() const { return dz; };

//******************************************************************************************

	inline vec3f getMin() { return min; }
	inline vec3f getMax() { return max; }

//******************************************************************************************

	//! Sets minimum extension
	void SetMin(vec3f min_) {
		min = min_;
		diag = max-min;
	}

//******************************************************************************************

	//! Sets minimum extension
	void SetMax(vec3f max_) {
		max = max_;
		diag = max-min;
	}

//******************************************************************************************

	inline UInt getPosFromTuple(int x, int y, int z) const {
		return x*dy*dz + y*dz + z;
	}

//******************************************************************************************

	//! Starts a seed-fill with value 'val' at pos '(x,y,z)' (replaces all accessible voxels that have a value of 0 with 'val').
	/*
		\return returns the number of filled pixels
	*/
	int seedFill(int x, int y, int z, ValueType val) {
		if (get(x,y,z) != 0) return 0;

		int numPixelsFilled = 0;

		std::list<vec3i> queue;
		queue.push_back(vec3i(x,y,z));

		while (!queue.empty()) {
			vec3i c = queue.front();
			queue.pop_front();

			if (get(c.x, c.y, c.z) != 0) continue;

			set(c.x, c.y, c.z, val);
			numPixelsFilled++;

			// Push neighbors:
			if (c.x > 0) {
				queue.push_back(vec3i(c.x-1, c.y, c.z));
			}
			if (c.x < (int)dx-1) {
				queue.push_back(vec3i(c.x+1, c.y, c.z));
			}
			if (c.y > 0) {
				queue.push_back(vec3i(c.x, c.y-1, c.z));
			}
			if (c.y < (int)dy-1) {
				queue.push_back(vec3i(c.x, c.y+1, c.z));
			}
			if (c.z > 0) {
				queue.push_back(vec3i(c.x, c.y, c.z-1));
			}
			if (c.z < (int)dz-1) {
				queue.push_back(vec3i(c.x, c.y, c.z+1));
			}
		}
		return numPixelsFilled;
	}

//******************************************************************************************

	void dilate(Volume<ValueType>* other) {

		for (int i1 = 0; i1 < (int)dx; i1++) {
			for (int i2 = 0; i2 < (int)dy; i2++) {
				for (int i3 = 0; i3 < (int)dz; i3++) {

					ValueType val = get(i1, i2, i3);

					if (val == 0) continue;

					for (int x = std::max(i1-1, 0); x <= std::min((int)dx-1, i1+1); x++) {
						for (int y = std::max(i2-1, 0); y <= std::min((int)dy-1, i2+1); y++) {
							for (int z = std::max(i3-1, 0); z <= std::min((int)dz-1, i3+1); z++) {
								other->set(x,y,z, val);
							}
						}
					}
				}
			}
		}

	}

//******************************************************************************************

	//! Lower left and Upper right corner.
	vec3f min, max;
	
	//! max-min
	vec3f diag;

	float ddx, ddy, ddz;
	float dddx, dddy, dddz;

	//! Number of cells in x, y and z-direction.
	UInt dx, dy, dz;

	ValueType* vol;

	ValueType maxValue, minValue;

	unsigned int m_dim;

private:

//******************************************************************************************

	//! x,y,z access to vol*
	inline ValueType vol_access(int x, int y, int z) const {
		return vol[getPosFromTuple(x,y,z)];
	}

//******************************************************************************************

};


#endif
