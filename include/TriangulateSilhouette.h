#ifndef TRIANGULATE_SILHOUETTE_H
#define TRIANGULATE_SILHOUETTE_H

#include "rng.h"
#include "poly2tri.h"
#include "windingNumber.h"
#include "Polyline.h"
#include <random>

// returns whether the polyline is ordered clockwise
bool isClockwise(const PolyLine<vec2f>& pl);


class PoissonDiscGrid
{
public:
    PoissonDiscGrid(float width, float height, float radius);
    int LinearIndex(const vec2i& index) const;
    vec2i Index(const vec2f& sample) const;
    void Insert(const vec2f& sample, int sampleIndex);
    bool IsInside(const vec2f& sample) const;
    bool IsOccupied(const vec2i& gridIndex) const;
    int GetSampleIndex(const vec2i& gridIndex) const;
    bool IsValidIndex(const vec2i& gridIndex) const;
    float GetWidth() const;
    float GetHeight() const;
    float GetRadius() const;
    void Print() const;

public:
    float m_width;
    float m_height;
    float m_radius;
    float m_cellSize;
    int m_cellWidth;
    int m_cellHeight;
    std::vector<int> m_grid;
};


class PoissonDiscSampler
{
public:
    PoissonDiscSampler(const vec2f& min, const vec2f& max, float radius, unsigned int maxNumCandidates = 30);
    std::vector<vec2f> GetSamples();
    
private:
    vec2f GetCircleSampleBiased(vec2f center);
    bool IsCloseToNeighbor(const vec2f& sample, float squaredRadius, const vec2i& neighborIndex);
    bool IsCloseToNeighbors(const vec2f& sample, float squaredRadius);
    
    vec2f m_min;
    vec2f m_max;
    PoissonDiscGrid m_grid;
    std::vector<vec2f> m_samples;
    std::vector<int> m_activeList;
    unsigned int m_maxNumCandidates;
    
    //std::random_device m_rd;
    std::mt19937 m_mt;
    std::uniform_real_distribution<float> m_random_radius;
    std::uniform_real_distribution<float> m_random_angle;
};

void getInnerPoints2(const PolyLine<vec2f>& pl, float targetEdgeLength, std::vector<vec2f>& innerPoints);
void getInnerPoints(const PolyLine<vec2f>& pl, float targetEdgeLength, std::vector<vec2f>& innerPoints);

template <class T>
void smoothDataOnMeshIgnoreFirst(int ignoreFirstNVerts, std::vector<T>& verts, const std::vector<vec3i>& tris) {

	std::vector<T> newVerts;
	std::vector<int> counter;
	newVerts.resize(verts.size());
	counter.resize(verts.size());

	for(unsigned int i1 = 0; i1 < verts.size(); i1++) {
		newVerts[i1] = T(0.0);
		counter[i1] = 0;
	}

	for(unsigned int i1 = 0; i1 < tris.size(); i1++) {
		const vec3i& t = tris[i1];

		newVerts[t.x] += verts[t.y];
		newVerts[t.x] += verts[t.z];
		newVerts[t.y] += verts[t.x];
		newVerts[t.y] += verts[t.z];
		newVerts[t.z] += verts[t.x];
		newVerts[t.z] += verts[t.y];
		counter[t.x] += 2;
		counter[t.y] += 2;
		counter[t.z] += 2;
	}

	for(int i1 = 0; i1 < ignoreFirstNVerts; i1++) newVerts[i1] = verts[i1];
	for(unsigned  i1 = ignoreFirstNVerts; i1 < verts.size(); i1++) newVerts[i1] /= (float)counter[i1];

	verts = newVerts;
}

void triangulatePolyLine(const PolyLine<vec2f>& pl, std::vector<vec3f>& pts, std::vector<vec3i>& tris, float target_pt_dist, bool addSteinerPoints, bool useNewPoissonDiskSampling);

#endif
