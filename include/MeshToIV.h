#ifndef MESH_TO_IV_H
#define MESH_TO_IV_H

#include "BaseMesh.h"
#include "SimpleMesh.h"
#include "BSplinePatch.h"
#include "Polyline.h"
#include "PolylineToIV.h"
#include "LineSet.h"
#include "volumeToIv.h"

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoIndexedLineSet.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoSelection.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoNormal.h>
#include <Inventor/nodes/SoTransparencyType.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/SbColor.h>
#include <Inventor/nodes/SoLightModel.h>



//****************************************************************************************************

SoSeparator* loadBOBJToIV(const char* fileIn, SoMaterial* mat = NULL, vec3f move = vec3f(0,0,0)) {

	std::ifstream fin(fileIn, std::ifstream::binary);
	if (!fin.good()) {
		std::cerr << "Error reading file " << fileIn << std::endl;
		return NULL;
	}

	std::vector<vec3f> pts;
	std::vector<vec3i> tri;

	char type;
	vec3f pt;
	vec3i t;
	while(!fin.eof()) {
		fin.read((char*)&type, sizeof(char));
		if (type == 0) { // read vertex
			fin.read((char*)&pt, sizeof(vec3f));
			pts.push_back(pt);
		} else if (type == 1) { // read triangle
			fin.read((char*)&t, sizeof(vec3i));
			tri.push_back(t);
		}
	}
	tri.pop_back();

	size_t numV = pts.size();
	size_t numT = tri.size();

	SoSeparator* ivmesh = new SoSeparator();
	SoShapeHints* sh = new SoShapeHints();
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	ivmesh->addChild(sh);

	if (mat != NULL) ivmesh->addChild(mat);

	// Create points:
	SbVec3f* Points = new SbVec3f[numV];
	for (UInt i1 = 0; i1 < numV; i1++) {
		vec3f pt = pts[i1];
		pt -= move;
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, (int)numV, Points);
	ivmesh->addChild(coord3);
	delete[] Points;

	// Create triangles
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum((int)numT*4);
	for (UInt i1 = 0; i1 < numT; i1++) {
		int v0 = tri[i1].x-1;
		int v1 = tri[i1].y-1;
		int v2 = tri[i1].z-1;
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
	}
	ivmesh->addChild(ifs);

	return ivmesh;
}

//****************************************************************************************************

SoSeparator* meshToIvColorByMarker(SimpleMesh* mesh, float crease = 10.0f, SoMaterial* mat1 = NULL, SoMaterial* mat2 = NULL) {

	SoSeparator* ivmesh = new SoSeparator();

	SoShapeHints* sh = new SoShapeHints();
	// Flat shading
	//sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	//sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	//sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	//sh->creaseAngle = crease;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setNum(2);
	mat->diffuseColor.set1Value(0, mat1->diffuseColor[0]);
	mat->diffuseColor.set1Value(1, mat2->diffuseColor[0]);
	mat->specularColor.setNum(2);
	mat->specularColor.set1Value(0, mat1->specularColor[0]);
	mat->specularColor.set1Value(1, mat2->specularColor[0]);
	mat->ambientColor.setNum(2);
	mat->ambientColor.set1Value(0, mat1->ambientColor[0]);
	mat->ambientColor.set1Value(1, mat2->ambientColor[0]);
	mat->shininess.setNum(2);
	mat->shininess.set1Value(0, mat1->shininess[0]);
	mat->shininess.set1Value(1, mat2->shininess[0]);

	ivmesh->addChild(mat);
	SoMaterialBinding* matbind = new SoMaterialBinding;
	matbind->value = SoMaterialBinding::PER_VERTEX_INDEXED;
	ivmesh->addChild(matbind);

	// Create points:
	int numPts = mesh->getNumV();
	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->getVertex(i1);
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);
	delete[] Points;

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(numTris*4);
	ifs->materialIndex.setNum(numTris*5);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = mesh->tList[i1]->v0();
		int v1 = mesh->tList[i1]->v1();
		int v2 = mesh->tList[i1]->v2();
		int32_t indices[] = {v0, v1, v2, -1 };
		int m0 = 0; int m1 = 0; int m2 = 0;
		if (mesh->vList[v0].bool_flag) m0 = 1;
		if (mesh->vList[v1].bool_flag) m1 = 1;
		if (mesh->vList[v2].bool_flag) m2 = 1;
		int32_t mats[] = {m0, m1, m2, -1};
		ifs->coordIndex.setValues(i1*4, 4, indices);
		ifs->materialIndex.setValues(i1*4, 4, mats);
	}
	ivmesh->addChild(ifs);

	return ivmesh;
}

//****************************************************************************************************

//! Colors the mesh by the sm->vList[i].color.x component (assuming color.x to be in [0:1]).
template <class ColorRamp>
SoSeparator* meshToIvColorByVertexUseColorRamp(SimpleMesh* mesh, float crease = 10.0f, float saturation = 1.0f, float value = 1.0f, bool CULLFACE = false) {
	SoSeparator* ivmesh = new SoSeparator();

	SoShapeHints* sh = new SoShapeHints();
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	if (CULLFACE) sh->shapeType = SoShapeHintsElement::SOLID;
	sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	int numPts = mesh->getNumV();

	//// normalize weight:
	//float max = -std::numeric_limits<float>::max();
	//float min = std::numeric_limits<float>::max();

	//for (int i1 = 0; i1 < numPts; i1++) {
	//	float w = mesh->vList[i1].color.x;
	//	if (w > max) max = w;
	//	if (w < min) min = w;
	//}
	//std::cerr << "min: " << min << "  max: " << max << std::endl;

	//for (int i1 = 0; i1 < numPts; i1++) {
		//float w = mesh->vList[i1].color.x;
		//float w2 = (w-min)/(max-min);
		//w2 *= 0.7f;
		//mesh->vList[i1].color.x = w2;
	//}

	// Create points:

	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->getVertex(i1);
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);
	delete[] Points;

	SoMaterial* mat = new SoMaterial;
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3ui rgb2 = ColorRamp::getColor(mesh->vList[i1].color.x, 1, 0);
		vec3f c(float(rgb2.x)/255.0f, float(rgb2.y)/255.0f, float(rgb2.z)/255.0f);
		mat->diffuseColor.set1Value(i1, c);
	}

	ivmesh->addChild(mat);
	SoMaterialBinding* matbind = new SoMaterialBinding;
	matbind->value = SoMaterialBinding::PER_VERTEX_INDEXED;
	ivmesh->addChild(matbind);

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(numTris*4);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = mesh->tList[i1]->v0();
		int v1 = mesh->tList[i1]->v1();
		int v2 = mesh->tList[i1]->v2();
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
	}
	ivmesh->addChild(ifs);

	return ivmesh;
}

//****************************************************************************************************

SoSeparator* meshToIvColorByVertex(SimpleMesh* mesh, float crease = 10.0f, bool transp = false) {
	SoSeparator* ivmesh = new SoSeparator();

	//SoLightModel* lm = new SoLightModel();
	//lm->model = SoLightModel::BASE_COLOR;
	//ivmesh->addChild(lm);

	SoShapeHints* sh = new SoShapeHints();
	// Flat shading
	//sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	//sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	//sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	//sh->creaseAngle = crease;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	// Create points:
	int numPts = mesh->getNumV();
	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->getVertex(i1);
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);
	delete[] Points;

	if (transp) {
		SoTransparencyType* tt = new SoTransparencyType;
		tt->value = SoTransparencyType::SORTED_OBJECT_SORTED_TRIANGLE_BLEND;
		ivmesh->addChild(tt);
	}


	SoMaterial* mat = new SoMaterial;
	//mat->diffuseColor.setNum(numPts);
	mat->diffuseColor.setNum(numPts);
	//mat->specularColor.setNum(numPts);
	for (int i1 = 0; i1 < numPts; i1++) {
#ifndef USE_LIGHTWEIGHT_SIMPLE_MESH
		vec3f c = mesh->vList[i1].color;
		mat->diffuseColor.set1Value(i1, c.x, c.y, c.z);
		//mat->diffuseColor.set1Value(i1, 0.5f,0.5f,0.5f);
		//mat->specularColor.set1Value(i1, 0.5f,0.5f,0.5f);
		if (transp) {
			float t = mesh->vList[i1].color.y;
			mat->transparency.set1Value(i1, t*t*t*t);
		}
#endif
	}

	ivmesh->addChild(mat);
	SoMaterialBinding* matbind = new SoMaterialBinding;
	matbind->value = SoMaterialBinding::PER_VERTEX_INDEXED;
	ivmesh->addChild(matbind);

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(numTris*4);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = mesh->tList[i1]->v0();
		int v1 = mesh->tList[i1]->v1();
		int v2 = mesh->tList[i1]->v2();
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
	}
	ivmesh->addChild(ifs);

	return ivmesh;
}


//****************************************************************************************************


SoSeparator* meshToIvColorByVertexOnlyMarked(SimpleMesh* mesh, float crease = 10.0f) {
	SoSeparator* ivmesh = new SoSeparator();

	//float col = 0.1f;
	SbColor color = SbVec3f(0.9f, 0.9f, 1.0f);

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setValue(color);
	ivmesh->addChild(mat);

	SoShapeHints* sh = new SoShapeHints();
	//sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	//sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	//sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	// Create points:
	int numPts = mesh->getNumV();
	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->getVertex(i1);
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);
	delete[] Points;

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(numTris*4);
	int counter = 0;
	for (int i1 = 0; i1 < numTris; i1++) {
		if (!mesh->tList[i1]->marker)
			continue;
		int v0 = mesh->tList[i1]->v0();
		int v1 = mesh->tList[i1]->v1();
		int v2 = mesh->tList[i1]->v2();
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(counter*4, 4, indices);

		counter++;
	}
	ifs->coordIndex.setNum(counter*4);
	ivmesh->addChild(ifs);

	return ivmesh;
}


//****************************************************************************************************


SoSeparator* BaseMeshToIv(BaseMesh* mesh, float crease = 10.0f) {
	SoSeparator* ivmesh = new SoSeparator();

	SoShapeHints* sh = new SoShapeHints();
	//sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	//sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	// Create points:
	int numPts = mesh->getNumV();
	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->points[i1];
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);

	delete[] Points;

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(numTris*4);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = mesh->tris[i1].v0;
		int v1 = mesh->tris[i1].v1;
		int v2 = mesh->tris[i1].v2;
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
	}
	ivmesh->addChild(ifs);

	return ivmesh;
}

//****************************************************************************************************

#ifndef USE_LIGHTWEIGHT_SIMPLE_MESH

SoSeparator* meshToIvTriColorByColorX(SimpleMesh* mesh, SoMaterial* mat1, SoMaterial* mat2, SoMaterial* mat3, float crease = 0.1f) {
	SoSeparator* ivmesh = new SoSeparator();

	ivmesh->setName("meshToIv");

	SoShapeHints* sh = new SoShapeHints();
	//sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	//sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	SoMaterialBinding* mat_bind = new SoMaterialBinding;
	mat_bind->value = SoMaterialBinding::PER_VERTEX_INDEXED;
	ivmesh->addChild(mat_bind);

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setNum(3);
	mat->ambientColor.setNum(3);
	mat->specularColor.setNum(3);
	mat->diffuseColor.set1Value(0, mat1->diffuseColor.getValues(0)[0]);
	mat->ambientColor.set1Value(0, mat1->ambientColor.getValues(0)[0]);
	mat->specularColor.set1Value(0, mat1->specularColor.getValues(0)[0]);
	mat->diffuseColor.set1Value(1, mat2->diffuseColor.getValues(0)[0]);
	mat->ambientColor.set1Value(1, mat2->ambientColor.getValues(0)[0]);
	mat->specularColor.set1Value(1, mat2->specularColor.getValues(0)[0]);
	mat->diffuseColor.set1Value(2, mat3->diffuseColor.getValues(0)[0]);
	mat->ambientColor.set1Value(2, mat3->ambientColor.getValues(0)[0]);
	mat->specularColor.set1Value(2, mat3->specularColor.getValues(0)[0]);
	mat->shininess = 0.2f; //mat1->shininess;
	ivmesh->addChild(mat);


	// Create points:
	int numPts = mesh->getNumV();
	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->getVertex(i1);
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);

	delete[] Points;

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(numTris*4);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = mesh->tList[i1]->v0();
		int v1 = mesh->tList[i1]->v1();
		int v2 = mesh->tList[i1]->v2();
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
	}
	ifs->materialIndex.setNum(numTris*4);
	for (int i1 = 0; i1 < numTris; i1++) {
		Triangle* t = mesh->tList[i1];
		if (mesh->vList[t->v0()].color.x == 1) ifs->materialIndex.set1Value(i1*4+0, 1);
		else if (mesh->vList[t->v0()].color.x == 2) ifs->materialIndex.set1Value(i1*4+0, 2);
		else ifs->materialIndex.set1Value(i1*4+0, 0);

		if (mesh->vList[t->v1()].color.x == 1) ifs->materialIndex.set1Value(i1*4+1, 1);
		else if (mesh->vList[t->v1()].color.x == 2) ifs->materialIndex.set1Value(i1*4+1, 2);
		else ifs->materialIndex.set1Value(i1*4+1, 0);

		if (mesh->vList[t->v2()].color.x == 1) ifs->materialIndex.set1Value(i1*4+2, 1);
		else if (mesh->vList[t->v2()].color.x == 2) ifs->materialIndex.set1Value(i1*4+2, 2);
		else ifs->materialIndex.set1Value(i1*4+2, 0);

		ifs->materialIndex.set1Value(i1*4+3, -1);
	}

	ivmesh->addChild(ifs);

	return ivmesh;
}

#endif

//****************************************************************************************************


SoSeparator* meshToIvBiColorByBoundaryMarker(SimpleMesh* mesh, SoMaterial* mat1, SoMaterial* mat2, float crease = 0.1f) {
	SoSeparator* ivmesh = new SoSeparator();

	ivmesh->setName("meshToIv");

	SoShapeHints* sh = new SoShapeHints();
	//sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	//sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	SoMaterialBinding* mat_bind = new SoMaterialBinding;
	mat_bind->value = SoMaterialBinding::PER_VERTEX_INDEXED;
	ivmesh->addChild(mat_bind);

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.set1Value(0, mat1->diffuseColor.getValues(0)[0]);
	mat->ambientColor.set1Value(0, mat1->ambientColor.getValues(0)[0]);
	mat->specularColor.set1Value(0, mat1->specularColor.getValues(0)[0]);
	mat->diffuseColor.set1Value(1, mat2->diffuseColor.getValues(0)[0]);
	mat->ambientColor.set1Value(1, mat2->ambientColor.getValues(0)[0]);
	mat->specularColor.set1Value(1, mat2->specularColor.getValues(0)[0]);
	mat->shininess = 0.3f;
	ivmesh->addChild(mat);


	// Create points:
	int numPts = mesh->getNumV();
	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->getVertex(i1);
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);

	delete[] Points;

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(numTris*4);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = mesh->tList[i1]->v0();
		int v1 = mesh->tList[i1]->v1();
		int v2 = mesh->tList[i1]->v2();
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
	}
	ifs->materialIndex.setNum(numTris*4);
	for (int i1 = 0; i1 < numTris; i1++) {
		Triangle* t = mesh->tList[i1];
		if (mesh->vList[t->v0()].is_boundary_point) ifs->materialIndex.set1Value(i1*4+0, 0);
		else ifs->materialIndex.set1Value(i1*4+0, 1);
		if (mesh->vList[t->v1()].is_boundary_point) ifs->materialIndex.set1Value(i1*4+1, 0);
		else ifs->materialIndex.set1Value(i1*4+1, 1);
		if (mesh->vList[t->v2()].is_boundary_point) ifs->materialIndex.set1Value(i1*4+2, 0);
		else ifs->materialIndex.set1Value(i1*4+2, 1);
		ifs->materialIndex.set1Value(i1*4+3, -1);
	}

	ivmesh->addChild(ifs);

	return ivmesh;
}


//****************************************************************************************************


SoSeparator* meshToIvTexture(SimpleMesh* mesh, const char* texturefilename, float texturescale = 1, float crease = 10.0f) {

	SoSeparator* ivmesh = new SoSeparator();

	ivmesh->setName("meshToIv");

	SoShapeHints* sh = new SoShapeHints();
	//sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	//sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	int numPts = mesh->getNumV();

	SbVec2f* texCoords = new SbVec2f[numPts];
	// set texture coordinates:
	for (int i1 = 0; i1 < numPts; i1++) {
		texCoords[i1] = SbVec2f((float)mesh->vList[i1].param.x, (float)mesh->vList[i1].param.y);
		texCoords[i1] *= texturescale;
	}
	SoTexture2* texture = new SoTexture2();
	texture->filename.setValue(texturefilename);
	texture->model.setValue(SoTexture2::MODULATE);
	ivmesh->addChild(texture);

	SoTextureCoordinate2* texcoord = new SoTextureCoordinate2();
	texcoord->point.setValues(0, numPts, texCoords);
	ivmesh->addChild(texcoord);

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setValue(0.85f, 0.85f, 0.85f); mat->ambientColor.setValue(0.0f, 0.0f, 0.0f); mat->specularColor.setValue(0.9f, 0.9f, 1.0f); mat->shininess = 0.5f;
	ivmesh->addChild(mat);

	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->getVertex(i1);
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);

	delete[] Points;

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(numTris*4);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = mesh->tList[i1]->v0();
		int v1 = mesh->tList[i1]->v1();
		int v2 = mesh->tList[i1]->v2();
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
	}
	ivmesh->addChild(ifs);

	return ivmesh;
}

//****************************************************************************************************


SoSeparator* meshToIv(SimpleMesh* mesh, float crease = 10.0f, SoMaterial* mat = NULL, float transparency = 1.0f) {
	SoSeparator* ivmesh = new SoSeparator();

	ivmesh->setName("meshToIv");

	SoShapeHints* sh = new SoShapeHints();
	//sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	//sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	if (transparency < 1.0f) {
		SoTransparencyType* tt = new SoTransparencyType;
		tt->value = SoTransparencyType::SORTED_OBJECT_SORTED_TRIANGLE_BLEND;
		ivmesh->addChild(tt);
		mat->transparency.setValue(transparency);
	}

	if (mat != NULL) ivmesh->addChild(mat);

	// Create points:
	int numPts = mesh->getNumV();
	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->getVertex(i1);
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);

	delete[] Points;

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(numTris*4);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = mesh->tList[i1]->v0();
		int v1 = mesh->tList[i1]->v1();
		int v2 = mesh->tList[i1]->v2();
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
	}
	ivmesh->addChild(ifs);

	return ivmesh;
}

//****************************************************************************************************


SoSeparator* meshToIvHighlightBadEdges(SimpleMesh* mesh, float crease = 10.0f, SoMaterial* mat = NULL) {
	SoSeparator* ivmesh = new SoSeparator();

	ivmesh->addChild(meshToIv(mesh, crease, mat));

	TrivialLine<vec3d> openEdges;
	TrivialLine<vec3d> multiEdges;
	TrivialLine<vec3d> doubleTriEdges;
	for (UInt i1 = 0; i1 < mesh->eList.size(); i1++) {
		Edge* e = mesh->eList[i1];
		if (e->tList.size() < 2) {
			vec3d v0 = mesh->vList[e->v0].c;
			vec3d v1 = mesh->vList[e->v1].c;
			openEdges.insertSegment(v0,v1);
		} else if (e->tList.size() > 2) {
			vec3d v0 = mesh->vList[e->v0].c;
			vec3d v1 = mesh->vList[e->v1].c;
			multiEdges.insertSegment(v0,v1);
		} else { // e->tList.size() == 2
			int other1 = e->tList[0]->getOther(e->v0, e->v1);
			int other2 = e->tList[1]->getOther(e->v0, e->v1);
			if (other1 == other2) {
			vec3d v0 = mesh->vList[e->v0].c;
			vec3d v1 = mesh->vList[e->v1].c;
				doubleTriEdges.insertSegment(v0,v1);
			}
		}
	}

	ivmesh->addChild(TrivialLineToIV(&openEdges, 0.001f, 5));
	ivmesh->addChild(TrivialLineToIV(&multiEdges, 0.33f, 5));
	ivmesh->addChild(TrivialLineToIV(&doubleTriEdges, 0.66f, 5));

	std::cerr << "OPENEDGES: " << openEdges.lines.size() << std::endl;

	return ivmesh;
}

//****************************************************************************************************


SoSeparator* meshToIvColorByTriangleIntFlag(SimpleMesh* mesh, int numColors) {

	SoSeparator* ivmesh = new SoSeparator();

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setNum(numColors);
	//mat->diffuseColor.set1Value(0, 0.0f, 0.0f, 0.0f);
	mat->diffuseColor.set1Value(0, 0.7f, 0.7f, 0.7f);
	for (int i1 = 1; i1 < numColors; i1++) {
		float fcol = (i1-0.5f)/numColors;
		vec3f c = JBSlib::hsv2rgb(fcol, 0.8f, 0.9f);
		//std::cerr << i1 << " -> " << c << std::endl;
		mat->diffuseColor.set1Value(i1, c.x, c.y, c.z);
		//mat->diffuseColor.set1Value(i1, 0.8f, 0.1f, 0.1f);
	}
	ivmesh->addChild(mat);

	SoMaterialBinding* matBind = new SoMaterialBinding;
	matBind->value = SoMaterialBindingElement::PER_FACE_INDEXED;
	ivmesh->addChild(matBind);

	SoShapeHints* sh = new SoShapeHints();
	sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	sh->creaseAngle = 0.1f;
	ivmesh->addChild(sh);

	// Create points:
	int numPts = mesh->getNumV();
	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->getVertex(i1);
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->setName("MeshVertices");
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);

	delete[] Points;

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->setName("MeshObjectIFS");
	ifs->coordIndex.setNum(numTris*4);
	ifs->materialIndex.setNum(numTris);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = mesh->tList[i1]->v0();
		int v1 = mesh->tList[i1]->v1();
		int v2 = mesh->tList[i1]->v2();
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
		int color_id;
#ifndef USE_LIGHTWEIGHT_SIMPLE_MESH
		color_id = mesh->tList[i1]->intFlag;
#endif
		//if (color_id == 3 || color_id == 9) color_id = 0;
		ifs->materialIndex.set1Value(i1, color_id);
	}
	ivmesh->addChild(ifs);

	return ivmesh;
}


//****************************************************************************************************


SoSeparator* meshToIvColorByTriangleMarker(SimpleMesh* mesh) {

	SbColor color1(0.7f, 0.75f, 0.75f);
	SbColor color2(1.0f, 0.0f, 0.0f);

	SoSeparator* ivmesh = new SoSeparator();

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setNum(2);
	mat->diffuseColor.set1Value(0, 0.75f, 0.75f, 0.75f);
	mat->diffuseColor.set1Value(1, 1.0f, 0.0f, 0.0f);
	ivmesh->addChild(mat);

	SoMaterialBinding* matBind = new SoMaterialBinding;
	matBind->value = SoMaterialBindingElement::PER_FACE_INDEXED;
	ivmesh->addChild(matBind);

	SoShapeHints* sh = new SoShapeHints();
	sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	sh->creaseAngle = 10.0f;
	ivmesh->addChild(sh);

	// Create points:
	int numPts = mesh->getNumV();
	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->getVertex(i1);
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->setName("MeshVertices");
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);

	delete[] Points;

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->setName("MeshObjectIFS");
	ifs->coordIndex.setNum(numTris*4);
	ifs->materialIndex.setNum(numTris);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = mesh->tList[i1]->v0();
		int v1 = mesh->tList[i1]->v1();
		int v2 = mesh->tList[i1]->v2();
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
		int color_id = (mesh->tList[i1]->marker) ? 1 : 0;
		ifs->materialIndex.set1Value(i1, color_id);
	}
	ivmesh->addChild(ifs);

	return ivmesh;
}


//****************************************************************************************************


SoSeparator* meshToIvColor(SimpleMesh* mesh, float col, float crease = 10.0f) {

	SbColor color;

	if (col == 0.0f)
		color = SbVec3f(0.7f, 0.75f, 0.75f);
	else {
		color.setHSVValue(col, 1.0f, 1.0f);
	}

	SoSeparator* ivmesh = new SoSeparator();

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setValue(color);
	ivmesh->addChild(mat);

	ivmesh->setName("meshToIv");

	SoShapeHints* sh = new SoShapeHints();
	//sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	//sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	// Create points:
	int numPts = mesh->getNumV();
	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->getVertex(i1);
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);

	delete[] Points;

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(numTris*4);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = mesh->tList[i1]->v0();
		int v1 = mesh->tList[i1]->v1();
		int v2 = mesh->tList[i1]->v2();
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
	}
	ivmesh->addChild(ifs);

	return ivmesh;
}


//****************************************************************************************************


SoSeparator* meshToFaceSetColor(SimpleMesh* mesh, float col) {

	SbColor color;

	if (col == 0.0f)
		color = SbVec3f(0.7f, 0.7f, 0.7f);
	else {
		color.setHSVValue(col, 1.0f, 1.0f);
	}

	SoSeparator* ivmesh = new SoSeparator();

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setValue(color);
	ivmesh->addChild(mat);

	// Create points:
	SoVertexProperty* vp = new SoVertexProperty();

	int numUsedV = mesh->getNumT()*3;
	SoFaceSet* fs = new SoFaceSet();

	fs->numVertices = numUsedV;
	vp->normal.setNum(numUsedV);
	vp->vertex.setNum(numUsedV);

	for (int i1 = 0; i1 < mesh->getNumT(); i1++) {
		vec3d pt = mesh->getVertex(mesh->tList[i1]->v0());
		vp->vertex.set1Value(i1*3+0, (float)pt.x, (float)pt.y, (float)pt.z);
		pt = mesh->getVertex(mesh->tList[i1]->v1());
		vp->vertex.set1Value(i1*3+1, (float)pt.x, (float)pt.y, (float)pt.z);
		pt = mesh->getVertex(mesh->tList[i1]->v2());
		vp->vertex.set1Value(i1*3+2, (float)pt.x, (float)pt.y, (float)pt.z);

		vec3d normal = mesh->tList[i1]->getNormal();
		vp->normal.set1Value(i1*3+0, (float)normal.x, (float)normal.y, (float)normal.z);
		vp->normal.set1Value(i1*3+1, (float)normal.x, (float)normal.y, (float)normal.z);
		vp->normal.set1Value(i1*3+2, (float)normal.x, (float)normal.y, (float)normal.z);
	}
	fs->vertexProperty = vp;
	ivmesh->addChild(fs);

	return ivmesh;
}


//****************************************************************************************************


SoSeparator* patchToIv(BSplinePatch<vec3f>* patch, int num_u_lines = 10, int num_v_lines = 10, int num_samples = 100) {
	SoSeparator* ivmesh = new SoSeparator();

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setValue(0.1f, 0.1f, 0.1f);
	ivmesh->addChild(mat);

	SoIndexedLineSet* niveau_lines = new SoIndexedLineSet();
	niveau_lines->coordIndex.setNum(3 * (num_u_lines * num_samples + num_v_lines * num_samples));

	int line_num = 0;

	// Render u-lines:
	SbVec3f* u_v_pts = new SbVec3f[(num_u_lines+1)*2*(num_samples+1)];
	for (int i1 = 0; i1 <= num_u_lines; i1++) {
		for (int i2 = 0; i2 <= num_samples; i2++) {
			float u = float(i1) / float(num_u_lines);
			float v = float(i2) / float(num_samples);
			if (u >= 1.0f)
				u = 0.999f;
			if (v >= 1.0f)
				v = 0.999f;
			vec3f pt = patch->eval(u,v);
			u_v_pts[i1*(num_samples+1)+i2] = SbVec3f(pt[0], pt[1], pt[2]);

			if (i2 < num_samples) {
				int32_t indices[] = {i1*(num_samples+1)+i2, i1*(num_samples+1)+i2+1, -1 };
				niveau_lines->coordIndex.setValues(line_num, 3, indices);
				line_num += 3;
			}
		}
	}

	// Render v-lines:
	for (int i1 = 0; i1 <= num_v_lines; i1++) {
		for (int i2 = 0; i2 <= num_samples; i2++) {
			float u = float(i2) / float(num_samples);
			float v = float(i1) / float(num_u_lines);
			if (u >= 1.0f)
				u = 0.999f;
			if (v >= 1.0f)
				v = 0.999f;
			vec3f pt = patch->eval(u,v);
			u_v_pts[((num_u_lines+1)*(num_samples+1))+(i1*(num_samples+1)+i2)] = SbVec3f(pt[0], pt[1], pt[2]);

			if (i2 < num_samples) {
				int32_t indices[] = {((num_u_lines+1)*(num_samples+1))+(i1*(num_samples+1)+i2), ((num_u_lines+1)*(num_samples+1))+(i1*(num_samples+1)+i2)+1, -1 };
				niveau_lines->coordIndex.setValues(line_num, 3, indices);
				line_num += 3;
			}
		}
	}
	SoCoordinate3* u_v_coord = new SoCoordinate3;
	u_v_coord->point.setValues(0, (num_u_lines+1)*2*(num_samples+1), u_v_pts);
	ivmesh->addChild(u_v_coord);
	ivmesh->addChild(niveau_lines);

	delete[] u_v_pts;

	// visualisiere Kontroll-Punkte:
	ControlGrid<vec3f> ctrlpts = patch->getCtrlPts();
	int num_m = patch->getNumCtrlPtsU();
	int num_n = patch->getNumCtrlPtsV();

	SoSeparator* cptsGroup = new SoSeparator();
	cptsGroup->setName("Controlpts");
	SoDrawStyle* drawstyle = new SoDrawStyle();
	drawstyle->style = SoDrawStyle::FILLED;
	cptsGroup->addChild(drawstyle);
	SoMaterial* mat3 = new SoMaterial;
	mat3->diffuseColor.setValue(0.4f,0.4f,0.8f);
	cptsGroup->addChild(mat3);
	SoSphere* sphere = new SoSphere();
	// Scale spheres adequately
	float diag = patch->getDiagonal();
	sphere->radius = diag / (16*(num_m+num_n));
	for (int i1 = 0; i1 < num_m*num_n; i1++) {
		SoSelection* sel = new SoSelection();
		SoTransform* trans = new SoTransform();

		UInt x = i1/num_m;
		UInt y = i1%num_m;

		trans->translation.setValue(ctrlpts.get(x,y)[0], ctrlpts.get(x,y)[1], ctrlpts.get(x,y)[2]);
		sel->addChild(trans);
		sel->addChild(sphere);
		cptsGroup->addChild(sel);
	}
	ivmesh->addChild(cptsGroup);

	return ivmesh;
}


//****************************************************************************************************


template <class T>
SoSeparator* BoundingBoxToIV(OBB<T>* obb) {
	SoSeparator* root = new SoSeparator;

	SoTransparencyType* trans = new SoTransparencyType;
	trans->value = SoGLRenderAction::SORTED_OBJECT_BLEND;
	root->addChild(trans);
	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.set1Value(0, 1, 0, 0);
	mat->transparency = 0.7f;
	root->addChild(mat);

	SoTransform* transf = new SoTransform;
	SbMatrix matr((float)obb->basis[0], (float)obb->basis[1], (float)obb->basis[2], 0, (float)obb->basis[3], (float)obb->basis[4], (float)obb->basis[5], 0, (float)obb->basis[6], (float)obb->basis[7], (float)obb->basis[8], 0, (float)obb->center[0],  (float)obb->center[1],  (float)obb->center[2], 1);
	transf->setMatrix(matr);
	root->addChild(transf);

	SoCube* cube = new SoCube;
	cube->width = 2*(float)obb->dx;
	cube->height = 2*(float)obb->dy;
	cube->depth = 2*(float)obb->dz;
	root->addChild(cube);

	return root;
}

//****************************************************************************************************


SoSeparator* LineSetToIV(LineSet ls, float col = 0, int lineWidth = 1) {

	SbColor color;

	if (col == 0.0f)
		color = SbVec3f(0.7f, 0.7f, 0.7f);
	else {
		color.setHSVValue(col, 1.0f, 1.0f);
	}

	SoSeparator* root = new SoSeparator;

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setValue(color);
	root->addChild(mat);


	// Create points:
	int numPts = (int)ls.size();
	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = ls[i1];
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numPts, Points);
	root->addChild(coord3);

	SoDrawStyle* ds = new SoDrawStyle;
	ds->lineWidth = (float)lineWidth;
	root->addChild(ds);

	SoIndexedLineSet* ils = new SoIndexedLineSet;
	ils->coordIndex.setNum((int)((ls.size()/2)*3));
	for (unsigned int i2 = 0; i2 < ls.size()/2; i2++) {
		ils->coordIndex.set1Value(i2*3+0, i2*2);
		ils->coordIndex.set1Value(i2*3+1, i2*2+1);
		ils->coordIndex.set1Value(i2*3+2, -1);
	}
	root->addChild(ils);

	return root;
}


//****************************************************************************************************


template <class T>
SoSeparator* BBtoIVWireframe(point3d<T> bbmin, point3d<T> bbmax) {

	point3d<T> center = (bbmin+bbmax)/(T)2;
	point3d<T> diag = bbmax-bbmin;

	SoSeparator* root = new SoSeparator;
	SoTransform* trans = new SoTransform;
	trans->translation.setValue(SbVec3f(center.x, center.y, center.z));
	root->addChild(trans);
	SoLightModel* lm = new SoLightModel;
	lm->model = SoLazyElement::BASE_COLOR;
	root->addChild(lm);
	SoDrawStyle* ds = new SoDrawStyle;
	ds->style = SoDrawStyleElement::LINES;
	root->addChild(ds);
	SoCube* cube = new SoCube;
	cube->width = diag.x;
	cube->height = diag.y;
	cube->depth = diag.z;
	root->addChild(cube);

	return root;
}



//****************************************************************************************************

#ifndef USE_LIGHTWEIGHT_SIMPLE_MESH

SoSeparator* meshToIvShowNormalsFromFields(SimpleMesh* mesh, float crease = 40.0f, float normal_scaler = 1.0f) {
	SoSeparator* ivmesh = new SoSeparator();

	ivmesh->setName("meshToIvShowNormalsFromFiels");

	SoShapeHints* sh = new SoShapeHints();
	//sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	//sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	// Create points:
	int numPts = mesh->getNumV();
	SbVec3f* Points = new SbVec3f[2*numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = mesh->getVertex(i1);
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
		vec3f normal = mesh->vList[i1].color;
		normal *= normal_scaler;
		vec3f normal_tip((float)pt.x+normal.x, (float)pt.y+normal.y, (float)pt.z+normal.z);
		Points[numPts+i1][0] = normal_tip[0]; Points[numPts+i1][1] = normal_tip[1]; Points[numPts+i1][2] = normal_tip[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, 2*numPts, Points);
	ivmesh->addChild(coord3);
	delete[] Points;

	// Create triangles
	int numTris = mesh->getNumT();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(numTris*4);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = mesh->tList[i1]->v0();
		int v1 = mesh->tList[i1]->v1();
		int v2 = mesh->tList[i1]->v2();
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
	}
	ivmesh->addChild(ifs);

	// Create vectors
	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setValue(SbColor(1,0,0));
	ivmesh->addChild(mat);
	SoDrawStyle* ds = new SoDrawStyle;
	ds->lineWidth = 3;
	ivmesh->addChild(ds);

	SoIndexedLineSet* ils = new SoIndexedLineSet;
	ils->coordIndex.setNum(3*numPts);
	for (int i1 = 0; i1 < numPts; i1++) {
		ils->coordIndex.set1Value(i1*3+0, i1);
		ils->coordIndex.set1Value(i1*3+1, i1+numPts);
		ils->coordIndex.set1Value(i1*3+2, -1);
	}
	ivmesh->addChild(ils);

	return ivmesh;
}
#endif

//****************************************************************************************************

#endif
