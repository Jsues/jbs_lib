#ifndef RGB_IMAGE_SDL_JBS_H
#define RGB_IMAGE_SDL_JBS_H


#include "RGB_Image.h"

namespace SDL_namespace {
#include "SDL_image.h"
};



//! An RGB-Image, which can read and write PNG files (needs libpng)
class RGBImageSDL : public RGBImage {

public:

	//! Loads an image from a file using the SDL Image lib.
	bool readImage(const char* filename) {

		SDL_namespace::SDL_Surface* img = SDL_namespace::IMG_Load(filename);
		if (img == NULL) {
			std::cerr << "RGBImageSDL::readImage(...): Error reading file " << filename << std::endl;
			return false;
		}

		std::cerr << "Width: " << img->w << " / Height: " << img->h << std::endl;

		char bpp = img->format->BytesPerPixel;
		// std::cerr << "Bytes per pixel: " << (int)bpp << std::endl;

		if (bpp != 3) {
			std::cerr << "RGBImageSDL::readImage(...): Unsupported file type " << filename << std::endl;
			return false;
		}

		m_dim_x = img->w;
		m_dim_y = img->h;
		m_dim = m_dim_x*m_dim_y;
		m_data = new rgb_triple[m_dim];

		char* ip = (char*)img->pixels;

		for (UInt i1 = 0; i1 < m_dim; i1++) {
			m_data[i1] = rgb_triple(ip[i1*3+0], ip[i1*3+1], ip[i1*3+2]);
		}

		return true;
	}

private:



};

#endif
