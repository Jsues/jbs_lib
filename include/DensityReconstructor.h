#ifndef JBS_DENSITY_RECONSTRUCTOR_H
#define JBS_DENSITY_RECONSTRUCTOR_H

#include "JBS_General.h"
#include "PointCloud.h"
#include "point3d.h"
#include "Matrix3D.h"

// For kD-tree
#include "ANN/ANN.h"


class DensityReconstructor
{
public:
	DensityReconstructor(float sigma, float search_radius);
	~DensityReconstructor(void);

	//! Sets the input Pointcloud and creates the kd-tree.
	/*!
		Scales the Pointcloud such that it is in the range of [0,0,0] to [1,1,1].<br>
		The original range is stores in 'org_min' and 'org_max'.
	*/
	void setPointCloud(PointCloud<vec3f>* pc_);

	//! computes the 'numNeighbors' nearest neighbors for each point.
	/*!
		Does furthermore compute the covariance tensor of all neighborhoods.
	*/
	void computeNeighborTensorData(int numNeighbors = 100);

	//! Returns a pair of the target value (.first) and the density (.second) at (x,y,z).
	std::pair<double, double> getTargetValueAt(float x, float y, float z);

	//! Returns a pair of the target value (.first) and the density (.second) at (x,y,z) using elliptical kernels.
	std::pair<double, double> getTargetValueEllipticalAt(float x, float y, float z);

	//! Test-Function (x,y,z).
	std::pair<double, double> getTargetValueTestAt(float x, float y, float z);

	//! Looks up exp(exponent).
	double my_exp(double exponent);

	//! Returns the point with the index 'i'.
	inline vec3f getPoint(unsigned int i) {
		return vec3f((float)dataPts[i][0], (float)dataPts[i][1], (float)dataPts[i][2]);
	}

	//! Computes 'g' as stated in Kurzsammlung '3D_line_ridges'
	double computeRidgeLine3DIndicator(float x, float y, float z);

	////! Returns the (gaussian interpolated) density at (x,y,z).
	//double getDensityAt(float x, float y, float z, float search_radius);
	////! Returns the gradient at (x,y,z).
	//vec3f getGradientAt(float x, float y, float z, float search_radius);

	//! 'org_min'-'org_max' is the original range of the pointcloud.
	vec3f org_min;
	//! 'org_min'-'org_max' is the original range of the pointcloud.
	vec3f org_max;


	vec3f current_ev0;
	vec3f current_ev1;
	vec3f current_ev2;
	vec3f gradient;
	vec3f curvatures;
	vec3f third_derivs;
	vec3f gradgrad;

	double* exp_lookup;

	Matrix3D<float>* CVs;

	//! (Noisy) input Pointcloud.
	PointCloud<vec3f>* pc;

private:

	//! ANN kD-tree.
	ANNkd_tree* kdTree;

	//! Stores the vectors of the kD-tree.
	ANNpointArray dataPts;

	//! Pre-allocated data structure for nn-search.
	ANNidxArray nnIdx;
	//! Pre-allocated data structure for nn-search.
	ANNdistArray dists;

	//! Number of elements in the kD-tree.
	int size_kDtree;

	//! Point used to query Neighbors.
	ANNpoint queryPt;

	float search_radius;

	double scale;

};

#endif
