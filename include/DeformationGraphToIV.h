#ifndef DEFORMATION_GRAPH_TO_IV_H
#define DEFORMATION_GRAPH_TO_IV_H

#include "PointCloud.h"
#include "PointCloudToIV.h"
#include "PolyLine.h"
#include "DeformationGraph.h"
#include "PolyLineToIV.h"
#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/nodes/SoComplexity.h>

SoSeparator* DeformationGraphToIV(DeformationGraph* dg) {
	PolyLine<vec3f> pl;
	PointCloud<vec3f> pc;

	UInt numPts = dg->getNumNodes();

	SoSeparator* sep = new SoSeparator;

	for (UInt i1 = 0; i1 < numPts; i1++) {
		vec3f pt = dg->nodes[i1].initial_position + dg->nodes[i1].translation;
		pc.insertPoint(pt);
		pl.insertVertex(pt);
	}

	SoSeparator* spheres = new SoSeparator;
	sep->addChild(JBSmaterials::matBlue());

	float radius = pc.getMax().dist(pc.getMin())/100;

	radius /=3;
	//radius /=2;

	SoSphere* sp = new SoSphere();
	sp->radius = radius;

	for (UInt i1 = 0; i1 < numPts; i1++) {
		vec3f pt = pc.getPoint(i1);
		SoSeparator* ss = new SoSeparator;
		spheres->addChild(ss);
		SoTranslation* t = new SoTranslation;
		t->translation.setValue(pt.x, pt.y, pt.z);
		ss->addChild(t);
		ss->addChild(sp);
	}

	for (UInt i1 = 0; i1 < dg->getNumEdges(); i1++) {
		UInt v0 = dg->edges[i1].v0;
		UInt v1 = dg->edges[i1].v1;
		//std::cerr << "v0: " << v0 << "  v1: " << v1 << std::endl;
		pl.insertLine(v0, v1);
	}

//	sep->addChild(pointcloudToIV(&pc, vec3f(0,0,0), 4));
	float col[3];
	col[0] = 0.0f; col[1] = 0.0f; col[2] = 0.5f;
	//SoAnnotation* ann = new SoAnnotation;
	SoSeparator* ann = new SoSeparator;
	//ann->addChild(PolyLineToIV(&pl, 0, 1, col));
	ann->addChild(PolyLineToIVTube(&pl, radius/4, 0, 0, 10, JBSmaterials::matBlue(), JBSmaterials::matBlue()));
	
	ann->addChild(spheres);
	sep->addChild(ann);

    return sep;
}

SoSeparator* DeformationGraphConstraintsToIV(std::vector<vec3f> pos, std::vector<float> rads, SoMaterial* mat = JBSmaterials::matYellow()) {

	SoSeparator* sep = new SoSeparator;
	UInt numPts = (UInt)pos.size();

	SoComplexity* comp = new SoComplexity;
	comp->value = 1;
	sep->addChild(comp);

	for (UInt i1 = 0; i1 < numPts; i1++) {
		vec3f pt = pos[i1];
		SoSeparator* ss = new SoSeparator;
		sep->addChild(ss);

		SoTransparencyType* tt = new SoTransparencyType;
		tt->value = SoTransparencyType::SORTED_OBJECT_SORTED_TRIANGLE_BLEND;
		ss->addChild(tt);
		mat->transparency.setValue(0.3f);
		ss->addChild(mat);

		SoTranslation* t = new SoTranslation;
		t->translation.setValue(pt.x, pt.y, pt.z);
		SoSphere* sp = new SoSphere();
		sp->radius = rads[i1];
		ss->addChild(t);
		ss->addChild(sp);
	}

	return sep;
}

#endif
