#ifndef OCTREE_TO_IV
#define OCTREE_TO_IV

#pragma warning( disable : 4244 )

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoLineSet.h>

#include "JBSoctree.h"

template <class T>
SoSeparator* ocTreeToIV(JBSlib::OcTree<T>* tree) {

	SoSeparator* root = new SoSeparator;

	std::vector<SbVec3f> lineendpoints;
	// Start with root node:

	JBSlib::OcTree<T>::Point min = tree->getMin();
	JBSlib::OcTree<T>::Point max = tree->getMax();

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.set1Value(0, 0, 1, 0);
	root->addChild(mat);

	// Outer cell
	lineendpoints.push_back(SbVec3f(min.x, min.y, min.z));
	lineendpoints.push_back(SbVec3f(max.x, min.y, min.z));
	lineendpoints.push_back(SbVec3f(min.x, min.y, min.z));
	lineendpoints.push_back(SbVec3f(min.x, max.y, min.z));
	lineendpoints.push_back(SbVec3f(min.x, min.y, min.z));
	lineendpoints.push_back(SbVec3f(min.x, min.y, max.z));
	lineendpoints.push_back(SbVec3f(max.x, max.y, max.z));
	lineendpoints.push_back(SbVec3f(max.x, max.y, min.z));
	lineendpoints.push_back(SbVec3f(max.x, max.y, max.z));
	lineendpoints.push_back(SbVec3f(max.x, min.y, max.z));
	lineendpoints.push_back(SbVec3f(max.x, max.y, max.z));
	lineendpoints.push_back(SbVec3f(min.x, max.y, max.z));
	lineendpoints.push_back(SbVec3f(min.x, max.y, min.z));
	lineendpoints.push_back(SbVec3f(max.x, max.y, min.z));
	lineendpoints.push_back(SbVec3f(min.x, max.y, min.z));
	lineendpoints.push_back(SbVec3f(min.x, max.y, max.z));
	lineendpoints.push_back(SbVec3f(min.x, min.y, max.z));
	lineendpoints.push_back(SbVec3f(min.x, max.y, max.z));
	lineendpoints.push_back(SbVec3f(min.x, min.y, max.z));
	lineendpoints.push_back(SbVec3f(max.x, min.y, max.z));
	lineendpoints.push_back(SbVec3f(max.x, min.y, min.z));
	lineendpoints.push_back(SbVec3f(max.x, max.y, min.z));
	lineendpoints.push_back(SbVec3f(max.x, min.y, min.z));
	lineendpoints.push_back(SbVec3f(max.x, min.y, max.z));
	
	// Visualize all inner cells:
	for (UInt i1 = 0; i1 < tree->cells.size(); i1++) {
		JBSlib::OcTree<T>::NodeType* cell = tree->cells[i1];
		if (!cell->isLeafNode()) {

			JBSlib::OcTree<T>::Point min_ = cell->getMin();
			JBSlib::OcTree<T>::Point max_ = cell->getMax();

			lineendpoints.push_back(SbVec3f(min_.x, 0.5*(min_.y+max_.y), 0.5*(min_.z+max_.z)));
			lineendpoints.push_back(SbVec3f(max_.x, 0.5*(min_.y+max_.y), 0.5*(min_.z+max_.z)));
			lineendpoints.push_back(SbVec3f(0.5*(min_.x+max_.x), min_.y, 0.5*(min_.z+max_.z)));
			lineendpoints.push_back(SbVec3f(0.5*(min_.x+max_.x), max_.y, 0.5*(min_.z+max_.z)));
			lineendpoints.push_back(SbVec3f(0.5*(min_.x+max_.x), 0.5*(min_.y+max_.y), min_.z));
			lineendpoints.push_back(SbVec3f(0.5*(min_.x+max_.x), 0.5*(min_.y+max_.y), max_.z));
			lineendpoints.push_back(SbVec3f(0.5*(min_.x+max_.x), min_.y, min_.z));
			lineendpoints.push_back(SbVec3f(0.5*(min_.x+max_.x), max_.y, min_.z));
			lineendpoints.push_back(SbVec3f(0.5*(min_.x+max_.x), min_.y, max_.z));
			lineendpoints.push_back(SbVec3f(0.5*(min_.x+max_.x), max_.y, max_.z));
			lineendpoints.push_back(SbVec3f(0.5*(min_.x+max_.x), min_.y, min_.z));
			lineendpoints.push_back(SbVec3f(0.5*(min_.x+max_.x), min_.y, max_.z));
			lineendpoints.push_back(SbVec3f(0.5*(min_.x+max_.x), max_.y, min_.z));
			lineendpoints.push_back(SbVec3f(0.5*(min_.x+max_.x), max_.y, max_.z));
			lineendpoints.push_back(SbVec3f(min_.x, 0.5*(min_.y+max_.y), min_.z));
			lineendpoints.push_back(SbVec3f(min_.x, 0.5*(min_.y+max_.y), max_.z));
			lineendpoints.push_back(SbVec3f(max_.x, 0.5*(min_.y+max_.y), min_.z));
			lineendpoints.push_back(SbVec3f(max_.x, 0.5*(min_.y+max_.y), max_.z));
			lineendpoints.push_back(SbVec3f(min_.x, 0.5*(min_.y+max_.y), min_.z));
			lineendpoints.push_back(SbVec3f(max_.x, 0.5*(min_.y+max_.y), min_.z));
			lineendpoints.push_back(SbVec3f(min_.x, 0.5*(min_.y+max_.y), max_.z));
			lineendpoints.push_back(SbVec3f(max_.x, 0.5*(min_.y+max_.y), max_.z));
			lineendpoints.push_back(SbVec3f(min_.x, min_.y, 0.5*(min_.z+max_.z)));
			lineendpoints.push_back(SbVec3f(min_.x, max_.y, 0.5*(min_.z+max_.z)));
			lineendpoints.push_back(SbVec3f(max_.x, min_.y, 0.5*(min_.z+max_.z)));
			lineendpoints.push_back(SbVec3f(max_.x, max_.y, 0.5*(min_.z+max_.z)));
			lineendpoints.push_back(SbVec3f(max_.x, min_.y, 0.5*(min_.z+max_.z)));
			lineendpoints.push_back(SbVec3f(min_.x, min_.y, 0.5*(min_.z+max_.z)));
			lineendpoints.push_back(SbVec3f(max_.x, max_.y, 0.5*(min_.z+max_.z)));
			lineendpoints.push_back(SbVec3f(min_.x, max_.y, 0.5*(min_.z+max_.z)));
		}
	}

	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, (int)lineendpoints.size(), &lineendpoints[0]);
	root->addChild(coord3);

	SoLineSet* ls = new SoLineSet;
	ls->numVertices.setNum((int)lineendpoints.size()/2);

	for (UInt i1 = 0; i1 < lineendpoints.size(); i1+=2) {
		ls->numVertices.set1Value(i1/2, 2);
	}
	root->addChild(ls);

	return root;
}


#endif
