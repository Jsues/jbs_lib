#ifndef SARAP_REG_HELPER_H
#define SARAP_REG_HELPER_H


#include "volumeARAP_ICP.h"

// ****************************************************************************************

//! Finds for each point in 'm_template_mesh' the closest point in 'm_target_pc' and, if the two points have compatible normals, adds a new correspondence for these points into 'm_correspondences'.
void findCorrespondencesSARAP(std::vector<vec3d>& verts, std::vector<vec3i> tris, std::vector<vec3d>& targetNormal, std::vector<double>& targetDistances, kdPointCloudNormals<vec3f>* target_pc) {

	std::vector<vec3d> mesh_normals = computeVertexNormals(verts, tris);

	for (UInt i1 = 0; i1 < verts.size(); i1++) {
		const vec3d& p = verts[i1];
		const vec3d& n = mesh_normals[i1];

		int idx = target_pc->getNearestPoint(p);
		vec3d cp = target_pc->m_points[idx];
		vec3d cn = target_pc->m_normals[idx];

		if ((cn|n) > 0.8) { // Is a valid correspondence

			//if (((cp-p).getNormalized()|n) < SLIDING_THRESHOLD) {
				targetNormal[i1] = cn;
				targetDistances[i1] = cp|cn;
			//} else {
			//	targetNormal[i1] = vec3d(0,0,0);
			//	targetDistances[i1] = 0;
			//}
		} else {
			targetNormal[i1] = vec3d(0,0,0);
			targetDistances[i1] = 0;
		}
	}
}
	
	
// ****************************************************************************************


void registerForCorrespondences(SimpleMesh* urshape, std::vector<vec3d>& working_vertices, std::vector<vec3d> targetNormals, std::vector<double>& targetDistances, const VARAP_Param& config) {

	int numUnknown = urshape->getNumV();

	// Precompute system matrix:
	std::vector<double> entries;
	std::vector<int> row_index;				// row indices, 0-based
	std::vector<int> col_ptr;				// pointers to where columns begin in rowind and values 0-based, length is (n+1)
	col_ptr.push_back(0);

	UInt current_row = 0;

	std::vector< SquareMatrixND<vec3d> > rots(numUnknown);

	for (int i = 0; i < numUnknown; i++) {

		const vec3d& p0 = urshape->vList[i].c;
		UInt numN = (UInt)urshape->vList[i].getNumN();
		vec3d n_i = targetNormals[i];

		SquareMatrixND<vec3d> tmp;
		tmp.setToIdentity();
		tmp *= (2*urshape->vList[i].param.x * config.stiffness);
		SquareMatrixND<vec3d> tmp2(n_i);
		//if (targetWeights.size() > 0) tmp2 *= targetWeights[i];
		tmp += tmp2;

		std::map<int, SquareMatrixND<vec3d> > row;
		row.insert(std::make_pair(i, tmp));

		// derive for p'_i.x and p'_i.y
		for (UInt i3 = 0; i3 < numN; i3++) {
			UInt neighbor = urshape->vList[i].getNeighbor(i3, i);
			const vec3d& p1 = urshape->vList[neighbor].c;

			const double& w_ij = urshape->vList[i].eList[i3]->cotangent_weight;
			SquareMatrixND<vec3d> tmp;
			tmp.setToIdentity();
			tmp *= (-w_ij*2 * config.stiffness);
			row.insert(std::make_pair(neighbor, tmp));
		}

		// Fill in matrix
		for (UInt line = 0; line < 3; line++) {
			for (std::map<int, SquareMatrixND<vec3d> >::const_iterator it = row.begin(); it != row.end(); it++) {
				UInt first_pos = it->first * 3;
#ifdef USE_UMFPACK
				entries.push_back(it->second(0, line));
				row_index.push_back(first_pos+0);
				entries.push_back(it->second(1, line));
				row_index.push_back(first_pos+1);
				entries.push_back(it->second(2, line));
				row_index.push_back(first_pos+2);
#else
				if (first_pos <= current_row) {
					entries.push_back(it->second(0, line));
					row_index.push_back(first_pos+0);
				}
				if ((first_pos+1) <= current_row) {
					entries.push_back(it->second(1, line));
					row_index.push_back(first_pos+1);
				}
				if ((first_pos+2) <= current_row) {
					entries.push_back(it->second(2, line));
					row_index.push_back(first_pos+2);
				}
#endif
			}
			col_ptr.push_back((UInt)entries.size());
			current_row++;
		}
	}


	std::vector<double> xv(numUnknown*3);
	std::vector<double> b(numUnknown*3);


	// Pre-factor matrix
#ifdef USE_UMFPACK
	void *Symbolic, *Numeric;
	int result1 = umfpack_di_symbolic(numUnknown*3, numUnknown*3, &col_ptr[0], &row_index[0], &entries[0], &Symbolic, NULL, NULL);
	int result2 = umfpack_di_numeric(&col_ptr[0], &row_index[0], &entries[0], Symbolic, &Numeric, NULL, NULL);

	std::cerr << "UMFPACK result: " << result1 << " / " << result2 << std::endl;
#else

	std::cerr << "ERROR SARAP_Reg_helper.h: registerForCorrespondences not tested with TAUCS" << std::endl;
	exit(1);

	// create TAUCS matrix
	taucs_ccs_matrix A;
	A.n = numUnknown*3;
	A.m = numUnknown*3;
	A.flags = (TAUCS_DOUBLE | TAUCS_SYMMETRIC | TAUCS_LOWER);
	A.colptr = &col_ptr[0];
	A.rowind = &row_index[0];
	A.values.d = &entries[0];

	taucs_double* xvec = &xv[0]; // the unknown vector to solve Ax=b

	void* F = NULL;
	char* options[] = {"taucs.factor.LLT=true", NULL};

	int* permutation;
	int* inversePermutation;
	void* factorization;
	taucs_ccs_matrix* permutedMatrix;
	taucs_ccs_matrix* matrixTemplatePermuted;

	// Compute permutation
	taucs_ccs_order(&A, &permutation, &inversePermutation, "metis"); // there was "amd" here, but that doesn't work with x64, use 'metis' for 64 bit
	// Compute permuted matrix template
	matrixTemplatePermuted = taucs_ccs_permute_symmetrically(&A, permutation, inversePermutation);
	// Compute symbolic factorization of the matrix template
	factorization = taucs_ccs_factor_llt_symbolic(matrixTemplatePermuted);
	// Compute permutation of the matrix
	permutedMatrix = taucs_ccs_permute_symmetrically(&A, permutation, inversePermutation);
	// Compute numeric factorization of the matrix
	taucs_ccs_factor_llt_numeric(permutedMatrix, factorization);

	std::vector<double> permutedRightHandSide_vec(numUnknown);
	std::vector<double> permutedVariables_vec(numUnknown);
	double* permutedRightHandSide	= &permutedRightHandSide_vec[0];
	double* permutedVariables		= &permutedVariables_vec[0];
#endif



	for (int innerIter = 0; innerIter < config.numIterInner; innerIter++) {

		//**********************************************************
		// Find ideal rotations
		//**********************************************************
		for (int id = 0; id < numUnknown; id++) {
			SquareMatrixND<vec3d> COV;
			UInt v0 = id;
			for (UInt i3 = 0; i3 < (UInt)urshape->vList[v0].getNumN(); i3++) {
				UInt v1 = urshape->vList[v0].getNeighbor(i3, v0);
				SquareMatrixND<vec3d> tmp;
				tmp.addFromTensorProduct(urshape->vList[v0].c-urshape->vList[v1].c, working_vertices[v0]-working_vertices[v1]);
				const double& w_ij = urshape->vList[v0].eList[i3]->cotangent_weight;
				tmp *= w_ij;
				COV += tmp;
			}
			SquareMatrixND<vec3d> U, V;
			vec3d sigma;
			bool SVD_result = COV.SVD_decomp(U, sigma, V);
			if (SVD_result) {
				V.transpose();
				SquareMatrixND<vec3d> rot = U*V;
				double det = rot.getDeterminant();
				if (det < 0) {
					int sm_id = 0;
					double smallest = sigma[sm_id];
					for (UInt dd = 1; dd < 3; dd++) {
						if (sigma[dd] < smallest) {
							smallest = sigma[dd];
							sm_id = dd;
						}
					}
					// flip sign of entries in colums 'sm_id' in 'U'
					U.m[sm_id + 0] *= -1;
					U.m[sm_id + 3] *= -1;
					U.m[sm_id + 6] *= -1;
					rot = U*V;
				}
				rot.transpose();
				rots[id] = rot;
			} else { // SVD failed!
				//std::cerr << "SVD failed" << std::endl;
				rots[id].setToIdentity();
			}
		}


		//**********************************************************
		// Find vertex positions which match rotations best
		//**********************************************************
		// init b with zero:
		for (int i = 0; i < 3*numUnknown; i++) b[i] = 0;

		for (int i = 0; i < numUnknown; i++) {

			const vec3d& p0 = urshape->vList[i].c;
			UInt numN = (UInt)urshape->vList[i].getNumN();


			// Fetch constraints
			vec3d n_i = targetNormals[i];
			double c_i = targetDistances[i];

			// rhs:
			b[3*i+0] = b[3*i+1] = b[3*i+2] = 0;
			vec3d rhs = n_i * c_i;
			//if (targetWeights.size() > 0) rhs *= targetWeights[i];
			b[3*i+0] += rhs.x;
			b[3*i+1] += rhs.y;
			b[3*i+2] += rhs.z;

			// derive for p'_i.x and p'_i.y
			for (UInt i3 = 0; i3 < numN; i3++) {
				UInt neighbor = urshape->vList[i].getNeighbor(i3, i);
				const vec3d& p1 = urshape->vList[neighbor].c;

				// right_hand_side:
				const double& w_ij = urshape->vList[i].eList[i3]->cotangent_weight;
				SquareMatrixND<vec3d> rot_avg = rots[i]+rots[neighbor];
				vec3d rhs = (rot_avg.vecTrans(p0-p1)) * w_ij * config.stiffness;
				b[3*i+0] += rhs.x;
				b[3*i+1] += rhs.y;
				b[3*i+2] += rhs.z;
			}
		}


	// Pre-factor matrix
#ifdef USE_UMFPACK
		umfpack_di_solve(UMFPACK_A, &col_ptr[0], &row_index[0], &entries[0], &xv[0], &b[0], Numeric, NULL, NULL);
#else

		// create TAUCS right-hand size
		taucs_double* b_taucs = b; // right hand side vector to solve Ax=b
		//taucs_logfile("stdout");
		// Compute permutation of the right hand side
		taucs_vec_permute(numOfPoints*3, TAUCS_DOUBLE, b_taucs, permutedRightHandSide, permutation);
		// Solve for the unknowns
		taucs_supernodal_solve_llt(factorization, permutedVariables, permutedRightHandSide);
		// Compute inverse permutation of the unknowns
		taucs_vec_permute(numOfPoints*3, TAUCS_DOUBLE, permutedVariables, x, inversePermutation);

#endif

		for (int i2 = 0; i2 < numUnknown; i2++) {
			working_vertices[i2] = vec3d(xv[3*i2+0],xv[3*i2+1],xv[3*i2+2]);
		}
	}

#ifdef USE_UMFPACK
	umfpack_di_free_symbolic(&Symbolic);
	umfpack_di_free_numeric(&Numeric);
#else
    // Free allocated memory
    if(factorization != NULL) taucs_supernodal_factor_free(factorization);
    //if(factorization != NULL) taucs_supernodal_factor_free_numeric(factorization);
    if(permutation != NULL) delete [] permutation;
    if(inversePermutation != NULL) delete [] inversePermutation;
    if(matrixTemplatePermuted != NULL) taucs_ccs_free(matrixTemplatePermuted);
	if(permutedMatrix != NULL) taucs_ccs_free(permutedMatrix);
#endif

}

#endif


