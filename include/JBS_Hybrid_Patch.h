#ifndef JBS_HYBRID_PATCH_H
#define JBS_HYBRID_PATCH_H


#include "BSplinePatch.h"
#include "point3d.h"
#include "JBS_General.h"
#include "ANN/ANN.h"
#include "nr_templates.h"


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************

//! Paar von zwei unsigned int.
struct pos_pair {
	UInt u, v;
};

enum EditingSteps {
	NOTHING,
	PICKED_PT_SELECTED,
	SPHERE_SELECTED,
	STARTED_MOVING
};

// ****************************************************************************************

//! Helfer-Struktur, speichert die ben�tigten Werte f�rs Mesh-Editing
struct EditingInfo {

	//! Der gepickte Punkt auf der Oberfl�che (muss kein Eckpunkt des Netzes sein!)
	vec3f pickedPoint;

	//! Gr��e der Auswahlkugel
	float sphereRadius;
	  
	//! Die betroffenen Punkte
	std::vector<UInt> affectedPoints;
	  
	//! Abst�nde zu 'pickedPoint' der betroffenen Punkte
	std::vector<float> affectedDistances;

	//! 'movedWidth[i1]' gibt an, wie weit der Punkt 'affectedPoints[i1]' entlang der Normalen 'currentNormal' verschoben wurde.
	std::vector<float> movedWidth;

	//! Letzter ausgef�hrter Schritt
	int step;

	//! "Weite" der Verschiebung.
	float d;

	//! bereits bearbeitete "Weite" der Verschiebung.
	float d_processed;
	  
	//! (Inventor) ID des Draggers der Selektion.
	int current_Dragger_id;
	  
	//! Richtung, entlang der verschoben wird.
	vec3f currentNormal;
	  
	//! Maus-Position beim Greifen des Draggers (wird ben�tigt, um die Verschiebung zu ermitteln.)
	vec3f mousePickPoint;
	  
	//! Brauch ich den??
	vec3f startTranslation;
	  
	//! Basis der Nachbarschaft von 'pickedPoint'
	std::vector<vec3f> base;

	//! Stellt alle struct-Werte auf ihren Anfangszustand zur�ck.
	void resetData() {
		sphereRadius = 0.0f;
		pickedPoint = vec3f(0, 0, 0);
		currentNormal = vec3f(0, 0, 0);
		mousePickPoint = vec3f(0, 0, 0);
		startTranslation = vec3f(0, 0, 0);
		affectedPoints.clear();
		affectedDistances.clear();
		base.clear();
		movedWidth.clear();
		d = 0.0f;
		d_processed = 0.0f;
		step = NOTHING;
		current_Dragger_id = -1;
	}

};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


class JBSHybridPatch {

public:

// ****************************************************************************************

	JBSHybridPatch(BSplinePatch<vec3f>* patch) {
		m_patch = patch;
		m_controlPoints = m_patch->getCtrlGrid();

		m_degree_m = m_patch->getDegreeU();
		m_degree_n = m_patch->getDegreeV();

		m_numCPts = m_controlPoints->getDimU()*m_controlPoints->getDimV();

		dataPts_set = false;
		ctrlPtsMoveWidth = new float[m_numCPts];
		resetEInfo();

		map = NULL;
		m_samplePoints = NULL;
		m_lu_decomposition = NULL;
		kdTree = NULL;
	}

// ****************************************************************************************

	~JBSHybridPatch() {
		delete[] ctrlPtsMoveWidth;
		if (m_samplePoints != NULL) {
			for (int i1 = 0; i1 < m_samples_u; i1++)
				delete[] m_samplePoints[i1];
			delete[] m_samplePoints;
		}
		if (map != NULL)
			delete[] map;

		if (kdTree != NULL)
			delete kdTree;

	}
// ****************************************************************************************

	//! Liefert die Matrix der Abstastpunkte zur�ck.
	const vec3f** getPoints() {
		return (const vec3f**) m_samplePoints;
	}

// ****************************************************************************************

	//! Bestimmt die Partielle Ableitung des Fehlers von Punkt (u/v), abgeleitet nach m[s][t]
	/*!
		\param u Index des Punkts in m Richtung.
		\param v Index des Punkts in n Richtung.
		\param ret_array Ein Array der Gr��e (degree_m+1)*(degree_n+1)+1, in dem die Einzelnen "Vorfaktoren" stehen.
	*/
	void derivErrorOfPoint(int u, int v, int s, int t, float* ret_array) {

		if (s > m_controlPoints->getDimU() || t > m_controlPoints->getDimV()) {
			std::cerr << "derivErrorOfPoint" << std::endl;
		}

		float BxBx2 = 2*(float)(m_B_u[s][u]*m_B_v[t][v]);
		for (int i = 0; i <= m_degree_m; i++) {
			for (int j = 0; j <= m_degree_n; j++) {
				ret_array[i*(m_degree_n+1)+j] = BxBx2*(float)(m_B_u[i][u]*m_B_v[j][v]);
			}
		}
		ret_array[m_numCPts] = BxBx2;
	}
// ****************************************************************************************

	//! Gleich wie 'derivErrorOfPoint', diesmal aber NUR f�r die Rechte Seite (L�sungsvektor)
	void derivErrorOfPointRightSideOnly(int u, int v, int s, int t, float* ret_array) {

		if (s > m_controlPoints->getDimU() || t > m_controlPoints->getDimV()) {
			std::cerr << "derivErrorOfPoint" << std::endl;
		}

		float BxBx2 = (float)(2*m_B_u[s][u]*m_B_v[t][v]);
		ret_array[m_numCPts] = BxBx2;
	}

// ****************************************************************************************

	//! Berechnet die BB-Basis der 12 n�chsten Nachbarn des Punkts (x, y, z)
	std::vector<vec3f> GetNormalAt(float x, float y,float z) {
		ANNpoint queryPt;
		queryPt = annAllocPt(3);

		queryPt[0] = x; queryPt[1] = y; queryPt[2] = z;

		// Hole 12 n�chste Nachbarn:
		int numK = 12;
		ANNidxArray nnIdx = new ANNidx[numK];						// allocate near neigh indices
		ANNdistArray dists = new ANNdist[numK];						// allocate near neighbor dists

		kdTree->annkSearch(queryPt, numK, nnIdx, dists, 0.0001);

	  
		// KovarianzMatrix:
		float** CV = new float*[4];
		for (int i1 = 0; i1 < 4; i1++) {
			CV[i1] = new float[4];
		}
	  
		// Eigenwerte:
		float lambda[4];
		  
		// Hier werden die Eigenvektoren gespeichert (einer pro Spalte). Eigenvektor
		// zum Eigenwert lambda[1] in den Feldern v[i][1], ...
		float **v = new float *[4];
		  
		for(int i1 = 0; i1 < 4; i1++)
			v[i1] = new float[4];
	  
		// Berechnen der Kovarianzmatrix.
		for(int i2 = 1; i2 <= 3; i2++)
			for(int i3 = 1; i3 <= 3; i3++)
				CV[i2][i3] = 0.0;
		  
		for(int i4 = 0; i4 < 12; i4++) {
			float y_o[4];
		    
			vec3f pt(dataPts[nnIdx[i4]][0], dataPts[nnIdx[i4]][1], dataPts[nnIdx[i4]][2]);
		    
			y_o[1] = pt[0] - x;
			y_o[2] = pt[1] - y;
			y_o[3] = pt[2] - z;

			for(int i2 = 1; i2 <= 3; i2++) {
				for(int i3 = i2; i3 <= 3; i3++) {
					CV[i2][i3] += y_o[i2] * y_o[i3];
					CV[i3][i2] = CV[i2][i3];
				}
			}
		}

		delete [] nnIdx;
		delete [] dists;
	//  delete kdTree;
	//	cerr << "kdTree gel�scht" << endl;

		// Berechnen der EWs und normalisierten EVs der Kovarianzmatrix.
		int muell;
		jacobi(CV, 3, lambda, v, &muell);
		  
		vec3f EVx, EVy, EVz;
		/* 
			Sortiere die Eigenvektoren immer so, dass die gr��te Ausdehnung in x-Richtung liegt
			(d.h. gr��ter Eigenwert gibt den Vektor an, der in x-Richtung zeigen soll),
			die mittlere Ausdehnung in y-Richtung und die kleinste in z-Richtung.
		*/
	  
		if (lambda[1] < lambda[2] && lambda[1] < lambda[3]) {
			EVz[0] = v[1][1]; EVz[1] = v[2][1]; EVz[2] = v[3][1]; 
			if (lambda[2] < lambda[3]) {
				EVy[0] = v[1][2]; EVy[1] = v[2][2]; EVy[2] = v[3][2]; 
				EVx[0] = v[1][3]; EVx[1] = v[2][3]; EVx[2] = v[3][3]; 
			} else {
				EVx[0] = v[1][2]; EVx[1] = v[2][2]; EVx[2] = v[3][2]; 
				EVy[0] = v[1][3]; EVy[1] = v[2][3]; EVy[2] = v[3][3]; 
			}
		} else if (lambda[2] < lambda[1] && lambda[2] < lambda[3]) {
			EVz[0] = v[1][2]; EVz[1] = v[2][2]; EVz[2] = v[3][2]; 
			if (lambda[1] < lambda[3]) {
				EVy[0] = v[1][1]; EVy[1] = v[2][1]; EVy[2] = v[3][1]; 
				EVx[0] = v[1][3]; EVx[1] = v[2][3]; EVx[2] = v[3][3]; 
			} else {
				EVx[0] = v[1][1]; EVx[1] = v[2][1]; EVx[2] = v[3][1]; 
				EVy[0] = v[1][3]; EVy[1] = v[2][3]; EVy[2] = v[3][3]; 
			}
		} else { // lambda[3] smallest!
			EVz[0] = v[1][3]; EVz[1] = v[2][3]; EVz[2] = v[3][3]; 
			if (lambda[1] < lambda[2]) {
				EVy[0] = v[1][1]; EVy[1] = v[2][1]; EVy[2] = v[3][1]; 
				EVx[0] = v[1][2]; EVx[1] = v[2][2]; EVx[2] = v[3][2]; 
			} else {
				EVx[0] = v[1][1]; EVx[1] = v[2][1]; EVx[2] = v[3][1]; 
				EVy[0] = v[1][2]; EVy[1] = v[2][2]; EVy[2] = v[3][2]; 
			}
		}
	  
		std::vector<vec3f> ret;
		ret.push_back(EVx);
		ret.push_back(EVy);
		ret.push_back(EVz);
	  
		for (int i1 = 0; i1 <= 3; i1++) {
			delete[] CV[i1];
			delete[] v[i1];
		}
		delete[] CV;
		delete[] v;

		return ret;
	}

// ****************************************************************************************

	//! Diese Funktion muss aufgerufen werden, wenn der Radius der gew�hlten Selektion bekannt ist.
	/*!
		Die Funktion berechnet dann alle von der Selektion beeinflussten Punkte.
	*/
	void setSelectionRadius(float r) {
		eInfo.sphereRadius = r;
		eInfo.step = SPHERE_SELECTED;

		// Suche beieinflusste Nachbarn:
		findNearest(eInfo.pickedPoint[0], eInfo.pickedPoint[1], eInfo.pickedPoint[2], r);
	}

// ****************************************************************************************

	//! Diese Funktion muss aufgerufen werden, wenn der "PickedPoint" bekannt ist.
	void setPickedPoint(float x, float y, float z) {
		std::cerr << "setPickedPoint called" << std::endl;
		  
		eInfo.pickedPoint = vec3f(x, y, z);
		std::cerr << "pickedPoint set" << std::endl;
		eInfo.step = PICKED_PT_SELECTED;
		std::cerr << "Step set" << std::endl;
		  
		// Nun berechne aus den 12 n�chsten Punkten zu 'eInfo.pickedPoint' die Normale, entlang der Verschoben wird:
		std::cerr << "GetNormalAt ...";
		buildKDTree();
		eInfo.base = GetNormalAt(x, y, z);
		std::cerr << " done" << std::endl;
		std::cerr << "Access Normal ...";
		eInfo.currentNormal = eInfo.base[0];
		std::cerr << " done" << std::endl;
	}

// ****************************************************************************************

	inline pos_pair getParameters(int pos) const {
		UInt v = pos % getNumPoints_u();
		UInt u = (pos-v) / getNumPoints_u();
		pos_pair ret;
		ret.u = u;
		ret.v = v;
		return ret;
	}

// ****************************************************************************************

	//! Wird aufgerufen, nachdem das Netz verzerrt wurde...
	/*!
		'reconstructBezier' versucht, die Kontrollpunkte des unterliegenden BezierPatches
		so auszurichten, dass die verformte Oberfl�che bestm�glichst rekonstruiert wird.
	*/
	void reconstructBezier() {
		/*
			Die betroffenen = verschobenen Punkte liegen _unsortiert_
			in eInfo.affectedPoints;

			Baue einen Hashtable auf, Key = index des Punktes, Value = Verschiebung.
		*/

		// cerr << "reconstructBezier() called" << endl;

		for (int i1 = 0; i1 < m_samples_u*m_samples_v; i1++)
			map[i1] = 0.0;

		for (UInt i1 = 0; i1 < eInfo.affectedPoints.size(); i1++) {
			map[eInfo.affectedPoints[i1]] = eInfo.movedWidth[i1];
		}

		//  for (int i1 = 0; i1 < m_samples_u*m_samples_v; i1++)
		//  cerr << i1 << " -> " << map[i1] << endl;

		//  cerr << "Dragger wurde um " << eInfo.d << " verschoben" << endl;

		int size = m_numCPts;
		// "right-side"-Vektor
		float* b = new float[size+1];
		for (int i1 = 0; i1 <= size; i1++)
			b[i1] = 0.0;


		// Leite den Fehlerterm nach allen 'size' Unbekannten ab:
		float* line = new float[size+1];
		for (int i1 = 0; i1 < size; i1++) {

			// Es wird abgeleitet nach:
			//    m[s,t];
			int s = i1 % (m_controlPoints->getDimU());
			int t = (i1-s) / (m_controlPoints->getDimU());

			//std::cerr << "s: " << s << " t: " << t << "  dim_u: " << m_controlPoints->getDimU() << "  dim_v: " << m_controlPoints->getDimV() << std::endl;

			// F�r alle Punkte:
			for (int u = 0; u < m_samples_u; u++) {
				for (int v = 0; v < m_samples_v; v++) {
					derivErrorOfPointRightSideOnly(v,u,s,t,line);
					float right_side = line[size] * map[u*m_samples_v+v];
					b[i1+1] += right_side;
				}
			}
		}
		delete[] line;

		lubksb(m_lu_decomposition, size, indx, b);

		//  //  cerr << "L�sung:";
		//  for (int i1 = 1; i1 <= size; i1++) {
		//    // Verschiebe Kontroll-Punkte entsprechend:
		//    ctrlPtsMoveWidth[i1-1] = b[i1];
		//  }
		for (int i1 = 0; i1 <= m_degree_m; i1++) {
			for (int i2 = 0; i2 <= m_degree_n; i2++) {
				ctrlPtsMoveWidth[i2+(m_degree_n+1)*i1] = b[i1+(m_degree_m+1)*i2+1];
				std::cerr << b[i1+(m_degree_m+1)*i2+1] << std::endl;
			}
		}

		// Aufr�umen
		delete[] b;
	}

// ****************************************************************************************

	//! Tastet den Patch an 'u'*'v' Punkten ab.
	void samplePatch(int u, int v) {

		m_samples_u = u;
		m_samples_v = v;

		// Reserviere Speicher f�r die 'u'*'v' Punkte:
		m_samplePoints = new vec3f*[u];
		for (int i1 = 0; i1 < u; i1++)
			m_samplePoints[i1] = new vec3f[v];
			
		// Reserviere Speicher im ANN-Punktespeicher
		dataPts = annAllocPts(u*v, 3);					// allocate query point

		float d_u = 1.0f/(float)(u-1);
		float d_v = 1.0f/(float)(v-1);
		float temp_u = 0.0, temp_v = 0.0;

		m_param_u = new float[u];
		m_param_v = new float[v];

		for (int u1 = 0; u1 < u; u1++) {
			for (int v1 = 0; v1 < v; v1++) {
				vec3f pnt = m_patch->eval(temp_u, temp_v);
				// cerr << "\t > " << temp_u << " / " << temp_v << " -> " << pnt << endl;
				m_samplePoints[u1][v1] = pnt;

				// F�ge Punkt im ANN Punktespeicher ein:
				int pos = u1*v+v1;
				dataPts[pos][0] = pnt[0]; dataPts[pos][1] = pnt[1]; dataPts[pos][2] = pnt[2]; 

				if (u1 == 0) {
					m_param_v[v1] = temp_v;
				}

				temp_v += d_v;
			}

			m_param_u[u1] = temp_u;
			temp_u += d_u;
			temp_v = 0;
		}

		// Berechne Bernsteinpolynome vor:
		std::cerr << "Berechne Bernsteinpolynome vor:" << std::endl;

		m_B_u = new float*[m_controlPoints->getDimU()];
		m_B_v = new float*[m_controlPoints->getDimV()];

		std::cerr << "Precomputed basis in u direction: m_B_u[" << m_controlPoints->getDimU() << "][" << u << "]" << std::endl;
		std::cerr << "Precomputed basis in u direction: m_B_v[" << m_controlPoints->getDimV() << "][" << v << "]" << std::endl;

		for (UInt i1 = 0; i1 < m_controlPoints->getDimU(); i1++) {
			m_B_u[i1] = new float[u];
			for (int i2 = 0; i2 < u; i2++) {
				m_B_u[i1][i2] = (m_patch->getKnotU())->evalBasis(i1, m_param_u[i2]);
				//std::cerr << " u: " << m_param_u[i2] << " " << m_B_u[i1][i2] << std::endl;
			}
		}

		for (UInt i1 = 0; i1 < m_controlPoints->getDimV(); i1++) {
			m_B_v[i1] = new float[v];
			for (int i2 = 0; i2 < v; i2++) {
				m_B_v[i1][i2] = (m_patch->getKnotV())->evalBasis(i1, m_param_v[i2]);
				//std::cerr << " v: " << m_param_v[i2] << " " << m_B_v[i1][i2] << std::endl;
			}
		}

		std::cerr << "Bernsteinpolynome berechnet" << std::endl;

		// ********************************************


		if (m_lu_decomposition != NULL) {
			std::cerr << "OOPS, 'JBSHybridPatch::samplePatch()', da muss der Jochen noch was machen!!!" << std::endl;
			exit(EXIT_FAILURE);
		} else {
			// Baue Matrix auf und berechne die LU-Dekomposition:
			int size = m_numCPts;
			m_lu_decomposition = new float*[size+1];
			for (int i1 = 0; i1 <= size; i1++) {
				m_lu_decomposition[i1] = new float[size+1];
				for (int i2 = 0; i2 <= size; i2++)
					m_lu_decomposition[i1][i2] = 0.0;
			}

			// Leite den Fehlerterm nach allen 'size' Unbekannten ab:
			float* line = new float[size+1];
			for (int i1 = 0; i1 < size; i1++) {

				// Es wird abgeleitet nach:
				//    m[s,t];
				int s = i1 % (m_controlPoints->getDimU());
				int t = (i1-s) / (m_controlPoints->getDimU());
		
				// F�r alle Punkte:
				for (int u = 0; u < m_samples_u; u++) {
					for (int v = 0; v < m_samples_v; v++) {
		
						derivErrorOfPoint(u,v,s,t,line);
						
						// �bertrage die Zeile in Matrix und "right-side" 'b':
						for (int i2 = 0; i2 < size; i2++) {
							m_lu_decomposition[i1+1][i2+1] += line[i2];
						}
					}
				}
			}
			delete[] line;

			float d;
			indx = new int[size+1];
			// Berechne LU-Decomposition:
			std::cerr << "Calculating LU-Decomposition...";
			ludcmp(m_lu_decomposition, size, indx, &d);
			std::cerr << "done" << std::endl;
		}

		map = new float[m_samples_u*m_samples_v];

	}

// ****************************************************************************************

	void buildKDTree() {

		std::cerr << "baue kD-Tree...";

		int numpts = getNumPoints_u() * getNumPoints_v();
		kdTree = new ANNkd_tree(dataPts, numpts, 3);
		std::cerr << "done" << std::endl;

	}

// ****************************************************************************************

	//! Grad in 'u'-Richtung.
	inline int getDegreeM() const {
		return m_degree_m;
	};

// ****************************************************************************************

	//! Grad in 'v'-Richtung.
	inline int getDegreeN() const {
		return m_degree_n;
	};

// ****************************************************************************************

	//! Anzahl der Punkte in u-Richtung.
	inline int getNumPoints_u() const {
		return m_samples_u;
	};

// ****************************************************************************************

	//! Anzahl der Punkte in v-Richtung.
	inline int getNumPoints_v() const {
		return m_samples_v;
	};

// ****************************************************************************************

	vec3f* getCtrlPts() {
		return m_controlPoints->getAllPts();
	}

// ****************************************************************************************

	int getNumCtrlPts() {
		return m_controlPoints->getDimU()*m_controlPoints->getDimV();
	}

// ****************************************************************************************

	//! Speichert die neue Position der Kontroll-Punkte.
	void saveCtrlPtsMovement() {

		vec3f currentNormalInWorldKoord = eInfo.base[2];
		for (int i1 = 0; i1 < getNumCtrlPts(); i1++) {
			vec3f o_pt = m_controlPoints->get(i1);
			vec3f n_pt = o_pt + currentNormalInWorldKoord * ctrlPtsMoveWidth[i1];
			m_controlPoints->set(i1,n_pt);
			std::cerr << n_pt << std::endl;
		}
	}

// ****************************************************************************************

	//! Hier drin sind die Werte f�r das Mesh-Editing gespeichert.
	EditingInfo eInfo;

	//! ANN kD-tree.
	ANNkd_tree* kdTree;

	//! Punktespeicher f�r den kD-Tree.
	ANNpointArray dataPts;

	//! in m_B_u[i][j] steht der Wert des i-ten B-Spline-Basis-Polynoms vom Grad 'd_degree_m' an der Stelle m_param_u[j]=u;
	float** m_B_u;
	//! in m_B_v[i][j] steht der Wert des i-ten B-Spline-Basis-Polynoms vom Grad 'd_degree_n' an der Stelle m_param_v[j]=u;
	float** m_B_v;

	//! Gibt an, wie weit die entsprechenden Kontroll-Punkte verschoben werden m�ssen...
	/*!
		Die Kontroll-Punkte werden erst nach dem Rechts-Klick wirklich angepasst!!
	*/
	float* ctrlPtsMoveWidth;

private:

// ****************************************************************************************

	//! Sucht alle Punkte die n�her als radius bei (x,y,z) liegen und speichert diese in eInfo.
	void findNearest(float x, float y, float z, float radius_) {
		std::cerr << "Searching close points" << std::endl;

		ANNpoint queryPt;
		queryPt = annAllocPt(3);

		queryPt[0] = x; queryPt[1] = y; queryPt[2] = z;

		ANNdist sr_radius = radius_*radius_;
		int num_pts = kdTree->annkFRSearch(queryPt, sr_radius, 0);		// Finde raus wieviele Punkte im Bereich liegen
		ANNidxArray nnIdx = new ANNidx[num_pts];						// allocate near neigh indices
		ANNdistArray dists = new ANNdist[num_pts];						// allocate near neighbor dists
		kdTree->annkFRSearch(queryPt, sr_radius, num_pts, nnIdx, dists);

		std::cerr << num_pts << " Points lie within the radius" << std::endl;

		for (int k = 0; k < num_pts; k++) {
			eInfo.affectedPoints.push_back(nnIdx[k]);
			eInfo.affectedDistances.push_back((float)(sqrt(dists[k])/radius_));
		}
		std::cerr << "Selektion berechnet" << std::endl;

		delete [] nnIdx;
		delete [] dists;
	}

// ****************************************************************************************

	void resetEInfo() {
		eInfo.step = NOTHING;
		eInfo.current_Dragger_id = -1;
		eInfo.startTranslation[0] = -1;
		eInfo.startTranslation[1] = -1;
		eInfo.startTranslation[2] = -1;
	}

// ****************************************************************************************

	ControlGrid<vec3f>* m_controlPoints;
	BSplinePatch<vec3f>* m_patch;
	bool dataPts_set;
	float* m_param_u;
	float* m_param_v;

	//! Flaechenpunkte
	vec3f** m_samplePoints;

	//! Die LU-Dekomposition der L�sungsmatrix (Muss nur einmal berechnet werden!)
	float** m_lu_decomposition;
	//! Hilfsarray f�r LU.
	int* indx;

	//! Grad in 'u'-Richtung.
	int m_degree_m;

	//! Grad in 'v'-Richtung.
	int m_degree_n;

	//! Anzahl der Punkte in u-Richtung.
	int m_samples_u;

	//! Anzahl der Punkte in v-Richtung.
	int m_samples_v;

	//! Wird nur in reconstructBezier verwendet; ausgelagert um weniger alloc/frees zu generieren.
	float* map;

	//! Anzahl der Kontrollpunkte
	int m_numCPts;

};


#endif
