#ifndef MESHTOIV_NEW_H
#define MESHTOIV_NEW_H

#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>

#include "point3d.h"

SoSeparator* MeshToIv(std::vector<vec3d> verts, std::vector<vec3i> tris, float crease = 10.0f) {
	SoSeparator* ivmesh = new SoSeparator();

	SoShapeHints* sh = new SoShapeHints();
	//sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	//sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::COUNTERCLOCKWISE;
	sh->creaseAngle = crease;
	ivmesh->addChild(sh);

	// Create points:
	int numPts = (int)verts.size();
	SbVec3f* Points = new SbVec3f[numPts];
	for (int i1 = 0; i1 < numPts; i1++) {
		vec3d pt = verts[i1];
		Points[i1][0] = (float)pt[0]; Points[i1][1] = (float)pt[1]; Points[i1][2] = (float)pt[2];
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numPts, Points);
	ivmesh->addChild(coord3);

	delete[] Points;

	// Create triangles
	int numTris = (int)tris.size();
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(numTris*4);
	for (int i1 = 0; i1 < numTris; i1++) {
		int v0 = tris[i1].x;
		int v1 = tris[i1].y;
		int v2 = tris[i1].z;
		int32_t indices[] = {v0, v1, v2, -1 };
		ifs->coordIndex.setValues(i1*4, 4, indices);
	}
	ivmesh->addChild(ifs);

	return ivmesh;
}


#endif
