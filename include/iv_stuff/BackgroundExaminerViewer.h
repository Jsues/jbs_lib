#ifndef BACKGROUND_EXAMINER_VIEWER_H
#define BACKGROUND_EXAMINER_VIEWER_H

#include <Inventor/Win/SoWin.h>
#include <Inventor/Win/viewers/SoWinExaminerViewer.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoRotationXYZ.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoTranslation.h>
#include <GL/gl.h>

// *************************************************************************
  
class BackgroundExaminerViewer : public SoWinExaminerViewer {

public:
	BackgroundExaminerViewer(HWND parent, const char * filename);
	~BackgroundExaminerViewer();

	void doForcedRedraw() {
		actualRedraw();
	}
	void doForcedRender() {
		render();
	}

protected:
	virtual void actualRedraw(void);

private:
	SoSeparator * bckgroundroot;
	SoSeparator * foregroundroot;
};
  
BackgroundExaminerViewer::BackgroundExaminerViewer(HWND parent, const char * filename) : SoWinExaminerViewer(parent) {
    // Coin should not clear the pixel-buffer, so the background image
    // is not removed.
    this->setClearBeforeRender(FALSE, TRUE);
    
    // Set up background scenegraph with image in it.
  
    this->bckgroundroot = new SoSeparator;
    this->bckgroundroot->ref();
  
    SoOrthographicCamera * cam = new SoOrthographicCamera;
    cam->position = SbVec3f(0, 0, 1);
    cam->height = 1;
    // SoImage will be at z==0.0.
    cam->nearDistance = 0.5;
    cam->farDistance = 1.5;
  
    SoImage * img = new SoImage;
    img->vertAlignment = SoImage::HALF;
    img->horAlignment = SoImage::CENTER;
    img->filename = filename;
  
    this->bckgroundroot->addChild(cam);
    this->bckgroundroot->addChild(img);
  
    // Set up foreground, overlayed scenegraph.
    this->foregroundroot = new SoSeparator;
    this->foregroundroot->ref();
  
    SoLightModel * lm = new SoLightModel;
    lm->model = SoLightModel::BASE_COLOR;
  
    SoBaseColor * bc = new SoBaseColor;
    bc->rgb = SbColor(1, 1, 0);
  
    cam = new SoOrthographicCamera;
    cam->position = SbVec3f(0, 0, 5);
    cam->height = 10;
    cam->nearDistance = 0;
    cam->farDistance = 10;
  

  
    this->foregroundroot->addChild(cam);
    this->foregroundroot->addChild(lm);
    this->foregroundroot->addChild(bc);
}
  
BackgroundExaminerViewer::~BackgroundExaminerViewer() {
	this->bckgroundroot->unref();
    this->foregroundroot->unref();
}
  
void BackgroundExaminerViewer::actualRedraw(void) {
    // Must set up the OpenGL viewport manually, as upon resize
    // operations, Coin won't set it up until the SoGLRenderAction is
    // applied again. And since we need to do glClear() before applying
    // the action..
    const SbViewportRegion vp = this->getViewportRegion();
    SbVec2s origin = vp.getViewportOriginPixels();
    SbVec2s size = vp.getViewportSizePixels();
    glViewport(origin[0], origin[1], size[0], size[1]);
  
    const SbColor col = this->getBackgroundColor();
    glClearColor(col[0], col[1], col[2], 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
    // Render our scenegraph with the image.
    SoGLRenderAction * glra = this->getGLRenderAction();
    glra->apply(this->bckgroundroot);
  
    // Render normal scenegraph.
    SoWinExaminerViewer::actualRedraw();
  
     // Render overlay front scenegraph.
    glClear(GL_DEPTH_BUFFER_BIT);
    glra->apply(this->foregroundroot);
}

#endif
