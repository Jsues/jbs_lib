#ifndef SOFTBOX_H
#define SOFTBOX_H

#include "point3d.h"
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoDirectionalLight.h>

template <class T>
SoSeparator* getSoftBox(point3d<T> minV=point3d<T>(0,0,0), point3d<T> maxV=point3d<T>(1,1,1), T angle = 0) {
	
	SoSeparator* box = new SoSeparator;
	T diag = maxV.dist(minV);
	point3d<T> scale(diag*1);
	maxV += scale;
	minV -= scale;
	maxV.y += scale.z*(T)0.6;
	minV.y += scale.z*(T)0.6;

	SbColor color = SbVec3f(0.95f, 0.95f, 0.95f);
	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setValue(color);
	//mat->emissiveColor.setValue(SbVec3f((T)0.5, (T)0.5, (T)0.5));
	mat->ambientColor.setValue(SbVec3f(1,1,1));
	box->addChild(mat);


	SbVec3f* points = new SbVec3f[8];
	points[0] = SbVec3f(minV.x, minV.y, minV.z);
	points[1] = SbVec3f(maxV.x, minV.y, minV.z);
	points[2] = SbVec3f(maxV.x, maxV.y, minV.z);
	points[3] = SbVec3f(minV.x, maxV.y, minV.z);
	points[4] = SbVec3f(minV.x, minV.y, maxV.z);
	points[5] = SbVec3f(maxV.x, minV.y, maxV.z);
	points[6] = SbVec3f(maxV.x, maxV.y, maxV.z);
	points[7] = SbVec3f(minV.x, maxV.y, maxV.z);

	// Convert angle from degree to radians
	angle *= (T)0.0174532925;

	SoTransform* trans = new SoTransform;
	trans->rotation.setValue(SbRotation(SbVec3f(0,1,0), angle));
	box->addChild(trans);

	SbVec3f lightdir(1,1,1);
	SoDirectionalLight* light = new SoDirectionalLight;
	light->direction.setValue(lightdir);


	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, 8, points);
	box->addChild(coord3);


	SoShapeHints* sh = new SoShapeHints;
	sh->vertexOrdering = SoShapeHints::COUNTERCLOCKWISE;
	sh->shapeType = SoShapeHints::SOLID;
	box->addChild(sh);

	
	SoIndexedFaceSet* ifs = new SoIndexedFaceSet();
	ifs->coordIndex.setNum(30);
	int32_t indices[] = {0, 1, 2, 3, -1,
						7, 6, 5, 4, -1,
						4, 5, 1, 0, -1,
						5, 6, 2, 1, -1,
						6, 7, 3, 2, -1,
						7, 4, 0, 3, -1};
	ifs->coordIndex.setValues(0, 30, indices);
	box->addChild(ifs);

	return box;
}

#endif