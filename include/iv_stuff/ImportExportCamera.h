#ifndef IMPORTEXPORTCAMERA
#define IMPORTEXPORTCAMERA

#include <Inventor/nodes/SoPerspectiveCamera.h>
#ifdef WIN32
#include <Inventor/Win/viewers/SoWinExaminerViewer.h>
#define EXAMINERVIEWER SoWinExaminerViewer
#else
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#define EXAMINERVIEWER SoQtExaminerViewer
#endif
#include <fstream>

#include "point4d.h"

// ****************************************************************************************

void ExportCamera(EXAMINERVIEWER* viewer, const char* filename) {

	std::ofstream outFile(filename);

	SoPerspectiveCamera* cam = (SoPerspectiveCamera*)viewer->getCamera();
	SoSFVec3f pos = cam->position;
	SoSFRotation orientation = cam->orientation;
	SoSFFloat aspectRatio = cam->aspectRatio;
	SoSFFloat near_d = cam->nearDistance;
	SoSFFloat far_d = cam->farDistance;
	SoSFFloat focal = cam->focalDistance;

	outFile << pos.getValue()[0] << " " << pos.getValue()[1] << " " << pos.getValue()[2] << std::endl;
	float angle; SbVec3f axis;
	orientation.getValue(axis, angle);
	outFile << axis[0] << " " << axis[1] << " " << axis[2] << " " << angle << std::endl;
	outFile << aspectRatio.getValue() << " " << near_d.getValue() << " " << far_d.getValue() << " " << focal.getValue() << std::endl;

	outFile.close();
}

// ****************************************************************************************

struct camStruct {
	vec4f params;
	vec3f posi;
	vec4f orient;

	void readFromFile(const char* filename) {
		std::ifstream inFile(filename);
		if (!inFile.is_open()) {
			std::cerr << "Error importing camera, skip!" << std::endl;
			return;
		}
		inFile >> posi;
		inFile >> orient;
		inFile >> params;
	}
};

// ****************************************************************************************

void ImportCamera(EXAMINERVIEWER* viewer, const char* filename) {

	std::ifstream inFile(filename);

	if (!inFile.is_open()) {
		std::cerr << "Error importing camera, skip!" << std::endl;
		return;
	}

	SoPerspectiveCamera* cam = (SoPerspectiveCamera*)viewer->getCamera();
	SoSFVec3f pos;
	SoSFRotation orientation;
	SoSFFloat aspectRatio;
	SoSFFloat near_d;
	SoSFFloat far_d;
	SoSFFloat focal;

	float a, n, f, foc; vec3f posi; vec4f orient;
	inFile >> posi;
	inFile >> orient;
	inFile >> a >> n >> f >> foc;

	//std::cerr << posi << std::endl;
	//std::cerr << orient << std::endl;
	//std::cerr << a << n << f << foc << std::endl;

	inFile.close();

	cam->position.setValue(posi.x, posi.y, posi.z);
	SbVec3f axis(orient.x, orient.y, orient.z);
	cam->orientation.setValue(axis, orient.w);
	cam->aspectRatio.setValue(a);
	cam->nearDistance.setValue(n);
	//std::cerr << "Near dist: " << n << std::endl;
	cam->farDistance.setValue(f);
	cam->focalDistance.setValue(foc);
}

#endif
