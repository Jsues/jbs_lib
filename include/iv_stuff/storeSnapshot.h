#ifndef STORE_SNAPSHOT_H
#define STORE_SNAPSHOT_H

#include <Inventor/SoDB.h>
#include <Inventor/SoOffscreenRenderer.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCamera.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/Win/viewers/SoWinExaminerViewer.h>
//#include <Inventor/actions/SoGetBoundingBoxAction.h>
#include "JBS_General.h"
#include "RGB_Image.h"


void storeSnapshot(SoWinExaminerViewer* viewer, SoNode* node, const char* filename, UInt size_x, UInt size_y = 0, vec3f bgcolor = vec3f(1,1,1), float nearDist = 0.1) {

	SbVec2s viewer_size = viewer->getSize();
	float ratio = (float)viewer_size[0]/(float)viewer_size[1];

	if (size_y == 0) size_y = (UInt)(size_x/ratio);

	SoSeparator* root = new SoSeparator;

	//SoDirectionalLight* light = new SoDirectionalLight;//viewer->getHeadlight();
	SoDirectionalLight* light = viewer->getHeadlight();
	root->addChild(light);
	SoCamera* cam = viewer->getCamera();

	//if (cam->nearDistance.getValue() > nearDist)
	//	cam->nearDistance.setValue(nearDist);
	//float newFarDist = cam->farDistance.getValue();
	////newFarDist *= 2;
	//cam->farDistance = newFarDist;

	root->addChild(cam);
	root->addChild(node);

	SbViewportRegion vpr;
	vpr.setWindowSize(size_x, size_y);

	//std::cerr << "nearDist: " << cam->nearDistance.getValue() << std::endl;

	//SoGetBoundingBoxAction ba(vpr);
	//node->getBoundingBoxS(&ba, node);
	//SbBox3f bbox = ba.getBoundingBox();
	//cam->viewBoundingBox(bbox, ratio, 1);


	SoOffscreenRenderer osr(vpr);
	osr.setBackgroundColor(SbColor(bgcolor.x,bgcolor.y,bgcolor.z));
	SbBool ok = osr.render(root);
	if (!ok) { 
		std::cerr << "Error in storeSnapshot(SoSeparator* root, UInt size_x, UInt size_y)" << std::endl;
		exit(1);
	}

	//const unsigned char* tmpbuff = osr.getBuffer();
	//Sleep(1000);
	//for (int i1 = 0; i1 < (int)(3*size_x*size_y); i1+=3) {
	//	if (tmpbuff[i1] < 255 || tmpbuff[i1+1] < 255 || tmpbuff[i1+2] < 255) {
	//		img->m_data[i1/3] = rgb_triple(tmpbuff[i1], tmpbuff[i1+1], tmpbuff[i1+2]);
	//	}
	//}


	ok = osr.writeToRGB(filename);
	if (!ok) {
		std::cerr << "Error in storeSnapshot(SoSeparator* root, UInt size_x, UInt size_y)" << std::endl;
		exit(1);
	}


}


void storeSnapshotOverlay(SoWinExaminerViewer* viewer, SoNode* node, RGBImage* img, UInt size_x, UInt size_y = 0, vec3f bgcolor = vec3f(1,1,1), float nearDist = 0.1) {

	SbVec2s viewer_size = viewer->getSize();
	float ratio = (float)viewer_size[0]/(float)viewer_size[1];

	if (size_y == 0) size_y = (UInt)(size_x/ratio);

	SoSeparator* root = new SoSeparator;

	//SoDirectionalLight* light = new SoDirectionalLight;//viewer->getHeadlight();
	SoDirectionalLight* light = viewer->getHeadlight();
	root->addChild(light);
	SoCamera* cam = viewer->getCamera();

	//if (cam->nearDistance.getValue() > nearDist)
	//	cam->nearDistance.setValue(nearDist);
	//float newFarDist = cam->farDistance.getValue();
	////newFarDist *= 2;
	//cam->farDistance = newFarDist;

	root->addChild(cam);
	root->addChild(node);

	SbViewportRegion vpr;
	vpr.setWindowSize(size_x, size_y);

	//std::cerr << "nearDist: " << cam->nearDistance.getValue() << std::endl;

	//SoGetBoundingBoxAction ba(vpr);
	//node->getBoundingBoxS(&ba, node);
	//SbBox3f bbox = ba.getBoundingBox();
	//cam->viewBoundingBox(bbox, ratio, 1);


	SoOffscreenRenderer osr(vpr);
	osr.setBackgroundColor(SbColor(bgcolor.x,bgcolor.y,bgcolor.z));
	SbBool ok = osr.render(root);
	if (!ok) { 
		std::cerr << "Error in storeSnapshot(SoSeparator* root, UInt size_x, UInt size_y)" << std::endl;
		exit(1);
	}

	const unsigned char* tmpbuff = osr.getBuffer();
	//Sleep(1000);
	for (int i1 = 0; i1 < (int)(3*size_x*size_y); i1+=3) {
		if (tmpbuff[i1] < 255 || tmpbuff[i1+1] < 255 || tmpbuff[i1+2] < 255) {
			img->m_data[i1/3] = rgb_triple(tmpbuff[i1], tmpbuff[i1+1], tmpbuff[i1+2]);
		}
	}



}

#endif

