#ifndef VEC_FIELD_TO_IV_H
#define VEC_FIELD_TO_IV_H

#include <vector>
#include "point3d.h"

#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoCone.h>



template <class T>
SoSeparator* PointCloudDiscVis(std::vector<T> pos, std::vector<T> dir, float thickness, float col) {

	SoSeparator* theArrows = new SoSeparator;

	SbColor color;
	if (col == 0.0f)
		color = SbVec3f(0.7f, 0.7f, 0.7f);
	else {
		color.setHSVValue(col, 1.0f, 1.0f);
	}

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.set1Value(0, color);

	for (UInt i1 = 0; i1 < pos.size(); i1++) {

		T dir1 = (dir[i1]);
		float length = (float)dir1.length();

		if (length == 0) continue;

		dir1.normalize();
		T second, third;
		second = T(1,0,0);
		third = second^dir1;
		if (third.squaredLength() == 0) {
			second = T(0,1,0);
			third = second^dir1;
		}
		third.normalize();
		second = third^dir1;
		SbMatrix matrix(
			(float)second.x, (float)second.y, (float)second.z, 0,
			(float)dir1.x, (float)dir1.y, (float)dir1.z, 0,
			(float)third.x, (float)third.y, (float)third.z, 0,
			(float)pos[i1].x, (float)pos[i1].y, (float)pos[i1].z, 1);

		SoSeparator* oneTranslated = new SoSeparator;

		SoTransform* trans = new SoTransform;
		trans->setMatrix(matrix);
		trans->scaleFactor.setValue(1, thickness, 1);
		trans->scaleOrientation.setValue(1, 0, 0, 0);


		SoSeparator* oneArrow = new SoSeparator;
	
		oneArrow->addChild(mat);
		
		SoCylinder* cyl = new SoCylinder;
		cyl->height = 0.01f*thickness;
		cyl->radius = 0.3f*thickness;
		oneArrow->addChild(cyl);
		
		SoTranslation* translation = new SoTranslation;
		translation->translation.setValue(0, cyl->height.getValue()*0.5f, 0);
		oneArrow->addChild(translation);


		oneTranslated->addChild(trans);
		oneTranslated->addChild(oneArrow);
		theArrows->addChild(oneTranslated);
	}

	return theArrows;
}


template <class T>
SoSeparator* vectorFieldToIV(std::vector<T> pos, std::vector<T> dir, float thickness, float col, float height_scaler = 1.0f) {

	SoSeparator* theArrows = new SoSeparator;

	SbColor color;
	if (col == 0.0f)
		color = SbVec3f(0.7f, 0.7f, 0.7f);
	else {
		color.setHSVValue(col, 1.0f, 1.0f);
	}

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.set1Value(0, color);
	SoCone* cone = new SoCone;
	cone->bottomRadius = 0.8f*thickness;
	cone->height = 0.8f;//*thickness;

	for (UInt i1 = 0; i1 < pos.size(); i1++) {

		T dir1 = (dir[i1]);
		float length = (float)dir1.length();

		if (length == 0) continue;

		dir1.normalize();
		T second, third;
		second = T(1,0,0);
		third = second^dir1;
		if (third.squaredLength() == 0) {
			second = T(0,1,0);
			third = second^dir1;
		}
		third.normalize();
		second = third^dir1;
		SbMatrix matrix(
			(float)second.x, (float)second.y, (float)second.z, 0,
			(float)dir1.x, (float)dir1.y, (float)dir1.z, 0,
			(float)third.x, (float)third.y, (float)third.z, 0,
			(float)pos[i1].x, (float)pos[i1].y, (float)pos[i1].z, 1);

		SoSeparator* oneTranslated = new SoSeparator;

		SoTransform* trans = new SoTransform;
		trans->setMatrix(matrix);
		trans->scaleFactor.setValue(1, thickness, 1);
		trans->scaleOrientation.setValue(1, 0, 0, 0);


		SoSeparator* oneArrow = new SoSeparator;
	
		oneArrow->addChild(mat);
		
		SoCylinder* cyl = new SoCylinder;
		cyl->height = length*height_scaler;
		cyl->radius = 0.3f*thickness;
		oneArrow->addChild(cyl);
		
		SoTranslation* translation = new SoTranslation;
		translation->translation.setValue(0, cyl->height.getValue()*0.5f, 0);
		oneArrow->addChild(translation);
		oneArrow->addChild(cone);


		oneTranslated->addChild(trans);
		oneTranslated->addChild(oneArrow);
		theArrows->addChild(oneTranslated);
	}

	return theArrows;
}

#endif
