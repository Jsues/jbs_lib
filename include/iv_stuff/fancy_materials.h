#ifndef FANCY_MATERIALS_H
#define FANCY_MATERIALS_H


#include <Inventor/nodes/SoMaterial.h>

namespace JBSmaterials {

	SoMaterial* matSilver() {
		SoMaterial* mat_silver = new SoMaterial;
		mat_silver->diffuseColor.setValue(0.6f, 0.6f, 0.6f); mat_silver->ambientColor.setValue(0.3f, 0.3f, 0.3f); mat_silver->specularColor.setValue(0.7f, 0.7f, 0.8f); mat_silver->shininess = 0.5f;
		return mat_silver;
	}

	SoMaterial* matMediumBlue() {
		SoMaterial* mat_blue = new SoMaterial;
		mat_blue->diffuseColor.setValue(0.5f, 0.5f, 0.7f); mat_blue->ambientColor.setValue(0.2f, 0.2f, 0.3f); mat_blue->specularColor.setValue(0.7f, 0.9f, 0.7f); mat_blue->shininess = 0.5f;
		return mat_blue;
	}

	SoMaterial* matBlue() {
		SoMaterial* mat_blue = new SoMaterial;
		mat_blue->diffuseColor.setValue(0.3f, 0.3f, 0.7f); mat_blue->ambientColor.setValue(0.1f, 0.1f, 0.3f); mat_blue->specularColor.setValue(0.7f, 0.9f, 0.7f); mat_blue->shininess = 0.2f;
		return mat_blue;
	}

	SoMaterial* matYellow() {
		SoMaterial* mat_blue = new SoMaterial;
		mat_blue->diffuseColor.setValue(1.0f, 1.0f, 0.0f);
		return mat_blue;
	}

	SoMaterial* matBlue2() {
		SoMaterial* mat_blue = new SoMaterial;
		mat_blue->diffuseColor.setValue(0.3f, 0.3f, 0.7f); mat_blue->ambientColor.setValue(0.1f, 0.1f, 0.3f); mat_blue->specularColor.setValue(0.5f, 0.7f, 0.5f); mat_blue->shininess = 0.5f;
		return mat_blue;
	}

	SoMaterial* matGray() {
		SoMaterial* mat_blue = new SoMaterial;
		mat_blue->diffuseColor.setValue(0.8f, 0.8f, 0.85f); mat_blue->ambientColor.setValue(0.15f, 0.15f, 0.15f); mat_blue->specularColor.setValue(0.05f, 0.05f, 0.05f); mat_blue->shininess = 2.0f;
		return mat_blue;
	}

	SoMaterial* matLightBlue() {
		SoMaterial* mat_blue = new SoMaterial;
		mat_blue->diffuseColor.setValue(0.8f, 0.8f, 0.85f); mat_blue->ambientColor.setValue(0.1f, 0.1f, 0.3f); mat_blue->specularColor.setValue(0.05f, 0.1f, 0.05f); mat_blue->shininess = 2.0f;
		return mat_blue;
	}

	SoMaterial* matOneMoreBlue() {
		SoMaterial* mat_blue = new SoMaterial;
		mat_blue->diffuseColor.setValue(0.18f, 0.57f, 0.894f); mat_blue->ambientColor.setValue(0.05f, 0.25f, 0.5f); mat_blue->specularColor.setValue(0.05f, 0.25f, 0.5f); mat_blue->shininess = 2.0f;
		return mat_blue;
	}

	SoMaterial* matOrange() {
		SoMaterial* mat_blue = new SoMaterial;
		mat_blue->diffuseColor.setValue(0.9f, 0.45f, 0.2f); mat_blue->ambientColor.setValue(0.5f, 0.25f, 0.05f); mat_blue->specularColor.setValue(0.5f, 0.25f, 0.05f); mat_blue->shininess = 2.0f;
		return mat_blue;
	}

	SoMaterial* matCoper() {
		SoMaterial* mat_coper = new SoMaterial;
		mat_coper->diffuseColor.setValue(0.7f, 0.5f, 0.5f); mat_coper->ambientColor.setValue(0.3f, 0.1f, 0.1f); mat_coper->specularColor.setValue(0.9f, 0.7f, 0.7f); mat_coper->shininess = 0.5f;
		return mat_coper;
	}

	SoMaterial* matRedVessels() {
		SoMaterial* mat_red = new SoMaterial;
		mat_red->diffuseColor.setValue(1.0f, 0.0f, 0.1f); mat_red->ambientColor.setValue(0.5f, 0.0f, 0.0f); mat_red->specularColor.setValue(1.0f, 0.4f, 0.4f); mat_red->shininess = 0.3f;
		return mat_red;
	}

	SoMaterial* matBlueVessels() {
		SoMaterial* mat_blue = new SoMaterial;
		mat_blue->diffuseColor.setValue(0.1f, 0.0f, 1.0f); mat_blue->ambientColor.setValue(0.0f, 0.0f, 0.5f); mat_blue->specularColor.setValue(0.2f, 0.2f, 1.0f); mat_blue->shininess = 0.3f;
		return mat_blue;
	}

	SoMaterial* matYellowNerves() {
		SoMaterial* mat_yellow = new SoMaterial;
		mat_yellow->diffuseColor.setValue(1.0f, 1.0f, 0.1f); mat_yellow->ambientColor.setValue(1.0f, 1.0f, 0.5f); mat_yellow->specularColor.setValue(1.0f, 1.0f, 0.6f); mat_yellow->shininess = 0.3f;
		return mat_yellow;
	}

}


#endif
