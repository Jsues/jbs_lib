#ifndef SAVE_AS_IV
#define SAVE_AS_IV

#include <Inventor/SoDB.h>
#include <Inventor/actions/SoWriteAction.h>
#include <Inventor/nodes/SoSeparator.h>

#include <fstream>

static char * buffer;
static size_t buffer_size = 0;

static void* buffer_realloc(void * bufptr, size_t size) {
	buffer = (char *)realloc(bufptr, size);
	buffer_size = size;
	return buffer;
}

static SbString buffer_writeaction(SoNode * root) {
	SoOutput out;
	buffer = (char *)malloc(1024);
	buffer_size = 1024;
	out.setBuffer(buffer, buffer_size, buffer_realloc);

	SoWriteAction wa(&out);
	wa.apply(root);

	SbString s(buffer);
	free(buffer);
	return s;
}

void saveAsIV(SoSeparator* sep, const char* filename) {
	SoDB::init();

    SbString s = buffer_writeaction(sep);

	std::ofstream outfile(filename);
	outfile << s.getString() << std::endl;

}


#endif
