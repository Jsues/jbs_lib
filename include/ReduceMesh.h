#ifndef REDUCE_MESH_H
#define REDUCE_MESH_H

#include "JBS_General.h"
#include "SimpleMesh.h"


//! Mesh class used for mesh-simplification.
class ReduceMesh : public SimpleMesh {
public:


	//! Constructor
	ReduceMesh() {
		new_pos = NULL;
		old_pos = NULL;
	};

	//! Destructor.
	~ReduceMesh() {
		if (new_pos != NULL)
			delete[] new_pos;
		if (old_pos != NULL)
			delete[] old_pos;
	};

	//! Inits the temp vertex array. Must be called prior to smoothing!
	void init() {
		new_pos = new vec3d[vList.size()];
		old_pos = new vec3d[vList.size()];
	};

	//! cleans up temporary arrays.
	void cleanUp() {
		if (new_pos != NULL) {
			std::cerr << "delete";
			delete[] new_pos;
		}
		if (old_pos != NULL) {
			std::cerr << "delete";
			delete[] old_pos;
		}
	};

	//! Reduces the mesh by some ..magic.. TODO: document this class!
	void reduceTriangle();

	//! Applies 'UmbrellaSmooth' to each Vertex of the Mesh.
	/*!
		e.g. Applies laplacian smoothing to the mesh.
		\param if allatonce == false, the operator is applied iteratively. i.e. It may happen that some neighbors of a point have already been moved and others not.
	*/
	float Umbrella(double lambda=1.0, bool allatonce = true, bool print = true);

	//! Applies the scale dependent umbrella operator to each Vertex of the Mesh.
	/*!
		e.g. Applies scale dependent laplacian smoothing to the mesh.
		\param if allatonce == false, the operator is applied iteratively. i.e. It may happen that some neighbors of a point have already been moved and others not.
	*/
	float ScaleDependentUmbrella(double lambda=1.0, bool allatonce = true, bool print = true);

	//! Applies the Curvature flow from "Desbrun 'Implicit Fairing of Irregular Meshes using Diffusion and Curvature Flow'" to each vertex of the mesh.
	/*!
		e.g. Applies curvature flow smoothing to the mesh.
		\param if allatonce == false, the operator is applied iteratively. i.e. It may happen that some neighbors of a point have already been moved and others not.
	*/
	float Curvature(double lambda=1.0, bool allatonce = true, bool print = true);

	//! Taubin Mesh-smoothing.
	/*!
		\param if allatonce == false, the operator is applied iteratively. i.e. It may happen that some neighbors of a point have already been moved and others not.
	*/
	float Taubin(double kpb=0.01, double lambda=0.6307, bool allatonce = true);

	//! Applies 'InverseUmbrellaSmoth' to each Vertex of the Mesh.
	/*!
		\param allatonce if 'allatonce == false', the operator is applied iteratively. i.e. It may happen that some neighbors of a point have already been moved and others not.
		\param startwith vertices with index < 'startwith' won't be moved. This is needed for interpolatory subdivision.
	*/
	float InverseUmbrella(int startwith = 0, double lambda=1.0, bool allatonce = true);

	//! Calculates the volume enclosed by the triangular mesh.
	/*!
		Caution: The mesh must be <b>closed</b> and all triangles must be <b>correctly orientated</b>.
	*/
	double CalculateVolume();

	//! Calculates the surface area of the mesh.
	double CalculateSurfaceArea();

	//! Balances the valence of the mesh
	void Balance();

	//! Prints the valence-distribution of the mesh.
	void printValenceStatistics();

	//! Splits the triangle 't' into 3 triangles.
	void subdivideTriangle(Triangle* t);

	//! TODO: description
	void subdivide();

private:

	//! Applies the Curvature flow from "Desbrun 'Implicit Fairing of Irregular Meshes using Diffusion and Curvature Flow'" to the vertex 'v'.
	/*!
		\return the new position of vertex 'v'.
	*/
	vec3d CurvatureSmooth(int v, double lambda=1.0);

	//! Applies the Umbrella Operator to the vertex 'v'.
	/*!
		\return the new position of vertex 'v'.
	*/
	vec3d UmbrellaSmoth(int v, double lambda=1.0);

	//! Applies the scale dependent umbrella operator, decribed in "Desbrun 'Implicit Fairing of Irregular Meshes using Diffusion and Curvature Flow'" to the vertex 'v'.
	/*!
		\return the new position of vertex 'v'.
	*/
	vec3d ScaleDependentUmbrellaSmoth(int v, double lambda=1.0);

	//! WRONG FORMULA Applies the <b>inverse</b> Umbrella Operator to the vertex 'v'.
	/*!
		\return the new position of vertex 'v'.
	*/
	vec3d InverseUmbrellaSmothFALSE(int v);

	//! Applies the <b>inverse</b> Umbrella Operator to the vertex 'v'.
	/*!
		\return the new position of vertex 'v'.
	*/
	vec3d InverseUmbrellaSmoth(int v);

	//! Checks wether the valence of the mesh is normalized when the edge is flipped (and flips the edge if is so).
	/*!
		\return true if the edge has been flipped.
	*/
	bool ValenceBalance(int e);

	//! Flips the egde e=(v0, v1).
	void FlipEdge(Edge* e);

	//! Returns the valence of vertex 'v'.
	UInt getValence(int v);

	//! Returns the error that would result from removing the triangle 'i1'.
	float getTriangleReduceError(int i1);

	//! Returns the umbrella-error of the triangle-fan at vertex 'v'.
	float getUmbrellaError(int v);

	//! Temp array for vertex positions
	vec3d* new_pos;

	//! Another temp array for vertex positions
	vec3d* old_pos;


};

#endif