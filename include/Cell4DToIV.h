#ifndef CELL4D_TO_IV_H
#define CELL4D_TO_IV_H

#define NOMINMAX

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoIndexedLineSet.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoSelection.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoNormal.h>
#include <Inventor/nodes/SoTransparencyType.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/SbColor.h>
#include <Inventor/nodes/SoTransparencyType.h>
#include <Inventor/nodes/SoMaterial.h>


#include "JBS_General.h"
#include "Cell4D.h"
#include "TrivialMeshToIV.h"
#include "meshToIV.h"
#include "PolyLine.h"
#include "PolyLineToIV.h"
#include "TriangleIntersect.h"
#include "operators/MeshSelfIntersection.h"

#include <vector>

template <class T>
point3d<T> proj(point4d<T> vec) {
	point3d<T> p(vec.x*2-1, vec.y*2-1, vec.z*2-1);
	T w = vec.w + 1;

	p /= w;

	return p;
}

SoSeparator* ProjTetTo4D() {
	return new SoSeparator;
}


template <class T, class U>
SoSeparator* Cell4DToIVIntersectTriangles(Cell4D<T,U>* cell) {

	SoSeparator* sep = new SoSeparator;

	U::ValueType iso_value = (U::ValueType)0.75;


	std::vector< point3d<U::ValueType> > projPts;
	PolyLine< point3d<U::ValueType> > pl;
	for (UInt i1 = 0; i1 < 16; i1++) {
		U pt4 = cell->gridpoints.array[i1];
		point3d<U::ValueType> pt3 = proj<T>(pt4);
		projPts.push_back(pt3);
		pl.insertVertex(pt3);
	}
	{
		int32_t indices[] = {
			0, 1, -1, 1, 3, -1, 3, 2, -1, 2, 0, -1,
			2, 6, -1, 3, 7, -1, 1, 5, -1, 0, 4, -1,
			6, 7, -1, 7, 5, -1, 5, 4, -1, 4, 6, -1,
			8, 9, -1, 9, 11, -1, 11, 10, -1, 10, 8, -1,
			14, 12, -1, 12, 13, -1, 13, 15, -1, 15, 14, -1,
			8, 12, -1, 9, 13, -1, 11, 15, -1, 10, 14, -1,
			2, 10, -1, 3, 11, -1, 1, 9, -1, 0, 8, -1,
			6, 14, -1, 7, 15, -1, 5, 13, -1, 4, 12, -1
		};
		for (UInt i1 = 0; i1 < 32; i1++) {
			pl.insertLine(indices[3*i1], indices[3*i1+1]);
		}
	}

	sep->addChild(PolyLineToIVTube(&pl, 0.04f));

	// Add spheres indicating if the isovalue of a corner is larger or smaller.
	for (UInt i1 = 0; i1 < 16; i1++) {
		SoSeparator* sphere = new SoSeparator();
		SoMaterial* mat3 = new SoMaterial;

			mat3->diffuseColor.setValue(1.0f,1.0f-cell->isovalues.array[i1], 1.0f-cell->isovalues.array[i1]);

		sphere->addChild(mat3);
		SoTransform* trans = new SoTransform;
		trans->translation.setValue(projPts[i1].x, projPts[i1].y, projPts[i1].z);
		sphere->addChild(trans);
		SoSphere* sp = new SoSphere;
		sp->radius = 0.16f;
		sphere->addChild(sp);

		sep->addChild(sphere);
	}


	std::vector<U> intersections;
	std::vector<short> indices;
	cell->GetIntersections(iso_value, intersections, indices);

	std::vector<SbVec3f> tetrapts;
	// Visualize intersections:
	for (UInt i1 = 0; i1 < intersections.size(); i1++) {
		SoSeparator* sphere = new SoSeparator();
		SoMaterial* mat3 = new SoMaterial;
		mat3->diffuseColor.setValue(0.9f,0.2f,0.2f);
		sphere->addChild(mat3);
		SoTransform* trans = new SoTransform;

		point3d<U::ValueType> projint = proj<T>(intersections[i1]);
		tetrapts.push_back(SbVec3f(projint.x, projint.y, projint.z));

		trans->translation.setValue(projint.x, projint.y, projint.z);
		sphere->addChild(trans);
		SoSphere* sp = new SoSphere;
		sp->radius = 0.08f;
		sphere->addChild(sp);

		sep->addChild(sphere);
	}

	SoSeparator* tets = new SoSeparator;
	SoTransparencyType* trans = new SoTransparencyType;
	trans->value = SoTransparencyType::SORTED_OBJECT_SORTED_TRIANGLE_BLEND;
	tets->addChild(trans);
	SoShapeHints* sh = new SoShapeHints();
	// Flat shading
	sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	tets->addChild(sh);
	SoMaterial* mat3 = new SoMaterial;
	mat3->transparency = 0.5;
	for (UInt i1 = 0; i1 < intersections.size(); i1++) {
		SbColor col;
		col.setHSVValue(intersections[i1].w, 1, 1);
		mat3->diffuseColor.set1Value(i1, col);
	}
	tets->addChild(mat3);
	SoMaterialBinding* matbind = new SoMaterialBinding;
	matbind->value = SoMaterialBinding::PER_VERTEX_INDEXED;
	tets->addChild(matbind);

	IndexedTetraMesh<U::ValueType>* mesh = new IndexedTetraMesh<U::ValueType>;

	U min = cell->gridpoints.array[0];
	U max = cell->gridpoints.array[15];
	U offset = (max-min)/10;
	min -= offset;
	max += offset;
	min.print();
	max.print();

	JBSlib::DFOcTree< JBSlib::DFOcNode< point4d<U::ValueType> > >* octree = new JBSlib::DFOcTree< JBSlib::DFOcNode< point4d<U::ValueType> > >;
	cell->GetIsoTetrahedra(iso_value, mesh, octree);

	SimpleMesh* sm = new SimpleMesh;
	mesh->boundaryToSimpleMesh(sm, &proj);

	SimpleMesh* sm2 = splitIntersectingTriangles(sm);

	exit(1);

	//tets->addChild(TrivialLineToIV(&trivialLine, (float)0.001));
	tets->addChild(meshToIv(sm));


	sep->addChild(tets);

	return sep;
}


template <class T, class U>
SoSeparator* Cell4DToIV(Cell4D<T,U>* cell) {

	SoSeparator* sep = new SoSeparator;


	std::vector< point3d<U::ValueType> > projPts;
	PolyLine< point3d<U::ValueType> > pl;
	for (UInt i1 = 0; i1 < 16; i1++) {
		U pt4 = cell->gridpoints.array[i1];
		point3d<U::ValueType> pt3 = proj<T>(pt4);
		projPts.push_back(pt3);
		pl.insertVertex(pt3);
	}
	{
		int32_t indices[] = {
			0, 1, -1, 1, 3, -1, 3, 2, -1, 2, 0, -1,
			2, 6, -1, 3, 7, -1, 1, 5, -1, 0, 4, -1,
			6, 7, -1, 7, 5, -1, 5, 4, -1, 4, 6, -1,
			8, 9, -1, 9, 11, -1, 11, 10, -1, 10, 8, -1,
			14, 12, -1, 12, 13, -1, 13, 15, -1, 15, 14, -1,
			8, 12, -1, 9, 13, -1, 11, 15, -1, 10, 14, -1,
			2, 10, -1, 3, 11, -1, 1, 9, -1, 0, 8, -1,
			6, 14, -1, 7, 15, -1, 5, 13, -1, 4, 12, -1
		};
		for (UInt i1 = 0; i1 < 32; i1++) {
			pl.insertLine(indices[3*i1], indices[3*i1+1]);
		}
	}

	sep->addChild(PolyLineToIVTube(&pl, 0.04f));

	// Add spheres indicating if the isovalue of a corner is larger or smaller.
	for (UInt i1 = 0; i1 < 16; i1++) {
		SoSeparator* sphere = new SoSeparator();
		SoMaterial* mat3 = new SoMaterial;

			mat3->diffuseColor.setValue(1.0f,1.0f-cell->isovalues.array[i1], 1.0f-cell->isovalues.array[i1]);

		sphere->addChild(mat3);
		SoTransform* trans = new SoTransform;
		trans->translation.setValue(projPts[i1].x, projPts[i1].y, projPts[i1].z);
		sphere->addChild(trans);
		SoSphere* sp = new SoSphere;
		sp->radius = 0.16f;
		sphere->addChild(sp);

		sep->addChild(sphere);
	}


	std::vector<U> intersections;
	std::vector<short> indices;
	cell->GetIntersections(0.75, intersections, indices);

	std::vector<SbVec3f> tetrapts;
	// Visualize intersections:
	for (UInt i1 = 0; i1 < intersections.size(); i1++) {
		SoSeparator* sphere = new SoSeparator();
		SoMaterial* mat3 = new SoMaterial;
		mat3->diffuseColor.setValue(0.9f,0.2f,0.2f);
		sphere->addChild(mat3);
		SoTransform* trans = new SoTransform;

		point3d<U::ValueType> projint = proj<T>(intersections[i1]);
		tetrapts.push_back(SbVec3f(projint.x, projint.y, projint.z));

		trans->translation.setValue(projint.x, projint.y, projint.z);
		sphere->addChild(trans);
		SoSphere* sp = new SoSphere;
		sp->radius = 0.08f;
		sphere->addChild(sp);

		sep->addChild(sphere);
	}

	SoSeparator* tets = new SoSeparator;
	SoTransparencyType* trans = new SoTransparencyType;
	trans->value = SoTransparencyType::SORTED_OBJECT_SORTED_TRIANGLE_BLEND;
	tets->addChild(trans);
	SoShapeHints* sh = new SoShapeHints();
	// Flat shading
	sh->faceType = SoShapeHintsElement::UNKNOWN_FACE_TYPE;
	sh->shapeType = SoShapeHintsElement::UNKNOWN_SHAPE_TYPE;
	sh->vertexOrdering = SoShapeHintsElement::CLOCKWISE;
	tets->addChild(sh);
	SoMaterial* mat3 = new SoMaterial;
	mat3->transparency = 0.5;
	for (UInt i1 = 0; i1 < intersections.size(); i1++) {
		SbColor col;
		col.setHSVValue(intersections[i1].w, 1, 1);
		mat3->diffuseColor.set1Value(i1, col);
	}
	tets->addChild(mat3);
	SoMaterialBinding* matbind = new SoMaterialBinding;
	matbind->value = SoMaterialBinding::PER_VERTEX_INDEXED;
	tets->addChild(matbind);
	SoCoordinate3* tetpts = new SoCoordinate3;
	tetpts->point.setValues(0, (int)tetrapts.size(), &tetrapts[0]);
	tets->addChild(tetpts);
	SoIndexedFaceSet* tetsfs = new SoIndexedFaceSet;

	tetsfs->coordIndex.setNum((int)indices.size()*4*4);
	for (UInt i1 = 0; i1 < (UInt)indices.size(); i1+=4) {

		std::cerr << indices[i1+0] << " " << indices[i1+1] << " " << indices[i1+2] << " " << indices[i1+3] << std::endl;

		int32_t v0 = indices[i1+0];
		int32_t v1 = indices[i1+1];
		int32_t v2 = indices[i1+2];
		int32_t v3 = indices[i1+3];

		int32_t indices[] = {
			v0, v1, v2, -1,
			v1, v0, v3, -1,
			v2, v1, v3, -1,
			v0, v3, v2, -1,
		};
		for (int aaaa = 0; aaaa < 16; aaaa++) {
			std::cerr << indices[aaaa] << " ";
		}
		std::cerr << std::endl;
		tetsfs->coordIndex.setValues(i1*4, 16, indices);
	}
	tets->addChild(tetsfs);

	sep->addChild(tets);

	return sep;
}

template <class T, class U>
SoSeparator* Cell4DToIVCellOnly(Cell4D<T,U>* cell, float t) {
	SoSeparator* sep = new SoSeparator;

	PolyLine< point3d<U::ValueType> > pl;
	for (UInt i1 = 0; i1 < 16; i1++) {
		U pt4 = cell->gridpoints.array[i1];
		pl.insertVertex(point3d<U::ValueType>(pt4.x, pt4.y, pt4.z));
	}
	int32_t indices[] = {
		0, 1, -1, 1, 3, -1, 3, 2, -1, 2, 0, -1,
		2, 6, -1, 3, 7, -1, 1, 5, -1, 0, 4, -1,
		6, 7, -1, 7, 5, -1, 5, 4, -1, 4, 6, -1,
	};
	for (UInt i1 = 0; i1 < 12; i1++) {
		pl.insertLine(indices[3*i1], indices[3*i1+1]);
	}
	sep->addChild(PolyLineToIVTube(&pl, 0.02));


	// Add spheres indicating if the isovalue of a corner is larger or smaller.
	for (UInt i1 = 0; i1 < 8; i1++) {
		SoSeparator* sphere = new SoSeparator();
		SoMaterial* mat3 = new SoMaterial;

		T isovalue = (1.0f-t)*cell->isovalues.array[i1]+(t)*cell->isovalues.array[i1+8];

		mat3->diffuseColor.setValue(1.0f,1.0f-isovalue, 1.0f-isovalue);

		sphere->addChild(mat3);
		SoTransform* trans = new SoTransform;
		U pt4 = cell->gridpoints.array[i1];
		trans->translation.setValue(pt4.x, pt4.y, pt4.z);
		sphere->addChild(trans);
		SoSphere* sp = new SoSphere;
		sp->radius = 0.08f;
		sphere->addChild(sp);

		sep->addChild(sphere);
	}

	return sep;
}


template <class T, class U>
SoSeparator* Cell4DToIVTimeFancy(Cell4D<T,U>* cell, float t) {

	SoSeparator* sep = new SoSeparator;

	sep->addChild(Cell4DToIVCellOnly(cell, t));


	// Add IsoSurface:
	TetraMesh4D<T> tetmesh;
	cell->GetIsoTetrahedra(0.5, &tetmesh);

	TrivialMesh<point3d<T> > tm;
	tetmesh.intersectWithTime(t, &tm);
	SoSeparator* isoSurface = new SoSeparator;
	SoTransparencyType* ttype = new SoTransparencyType;
	ttype->value = SoTransparencyType::SORTED_OBJECT_SORTED_TRIANGLE_BLEND;
	SoMaterial* mat = new SoMaterial;
	mat->transparency = 0.5;

	float col = 0.0f;
	SbColor color;
	if (col == 0.0f)
		color = SbVec3f(0.7f, 0.7f, 0.9f);
	else {
		color.setHSVValue(col, 1.0f, 1.0f);
	}
	mat->diffuseColor.setValue(color);

	isoSurface->addChild(ttype);
	isoSurface->addChild(mat);

	TrivialMeshToIv(&tm, isoSurface);
	sep->addChild(isoSurface);

	return sep;
}


template <class T, class U>
SoSeparator* Cell4DToIVTime(Cell4D<T,U>* cell, float t) {

	SoSeparator* sep = new SoSeparator;

	std::vector<SbVec3f> lineendpoints;
	lineendpoints.reserve(8);

	for (UInt i1 = 0; i1 < 16; i1++) {
		U pt4 = cell->gridpoints.array[i1];
		lineendpoints.push_back(SbVec3f(pt4.x, pt4.y, pt4.z));
	}
	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, 8, &lineendpoints[0]);
	sep->addChild(coord3);

	SoIndexedLineSet* ils = new SoIndexedLineSet;
	ils->coordIndex.setNum(96);
	{
		int32_t indices[] = {
			0, 1, -1, 1, 3, -1, 3, 2, -1, 2, 0, -1,
			2, 6, -1, 3, 7, -1, 1, 5, -1, 0, 4, -1,
			6, 7, -1, 7, 5, -1, 5, 4, -1, 4, 6, -1,
		};
		ils->coordIndex.setValues(0, 36, indices);
	}
	sep->addChild(ils);

	// Add spheres indicating if the isovalue of a corner is larger or smaller.
	for (UInt i1 = 0; i1 < 8; i1++) {
		SoSeparator* sphere = new SoSeparator();
		SoMaterial* mat3 = new SoMaterial;

		T isovalue = (1.0f-t)*cell->isovalues.array[i1]+(t)*cell->isovalues.array[i1+8];

		mat3->diffuseColor.setValue(1.0f,1.0f-isovalue, 1.0f-isovalue);

		sphere->addChild(mat3);
		SoTransform* trans = new SoTransform;
		U pt4 = cell->gridpoints.array[i1];
		trans->translation.setValue(pt4.x, pt4.y, pt4.z);
		sphere->addChild(trans);
		SoSphere* sp = new SoSphere;
		sp->radius = 0.08f;
		sphere->addChild(sp);

		sep->addChild(sphere);
	}

	// Add IsoSurface:
	TetraMesh4D<T> tetmesh;
	cell->GetIsoTetrahedra(0.5, &tetmesh);

	TrivialMesh<point3d<T> > tm;
	tetmesh.intersectWithTime(t, &tm);
	SoSeparator* isoSurface = TrivialMeshToIv(&tm);
	sep->addChild(isoSurface);

	return sep;
}


#endif

