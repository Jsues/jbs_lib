#include "point3d.h"
#include "point2d.h"

namespace JBSlib {

	//! TODO: Implement! Approximate the given pointset 'points' (with parametrization 'params') with a bi-quadratic taylor-polynomial.
	/*!
		\param num_pts Number of points to be approximated
		\param points Points to be approximated
		\param params Parametrization of points to be approximated
		\param f The approximating Taylor polynomial, f[0] is F_u, f[1] is F_v, f[2] is F_uu, <b>f[3] is F_uv</b> and F[4] is F_vv.
	*/
	void approximate(int num_pts, vec3f* points, vec2f* params, vec3d f[5]) {

		//for (int i1 = 0; i1 < num_pts; i1++) {
		//	points[i1].print();
		//	params[i1].print();
		//}


	};

};