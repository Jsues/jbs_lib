#ifndef MATRIX3D_H
#define MATRIX3D_H


#include <iomanip>


#include "point3d.h"
#include "nr_templates.h" // for eigenvectors/eigenvalues (jacobi)

//******************************************************************************************
//******************************************************************************************

//! A quaternion, i.e. a 4D Vector
template<class T>
class Quaternion {
public:

	//! Value-Type of Templates components (i.e. float or double).
	typedef T ValueType;

//******************************************************************************************

	Quaternion(ValueType q0_, ValueType q1_, ValueType q2_, ValueType q3_) {
		a[0] = q0_;
		a[1] = q1_;
		a[2] = q2_;
		a[3] = q3_;
	}

//******************************************************************************************

	void print() {
		std::cerr << "Quaternion: (" << q0 << ", " << q1 << ", " << q2 << ", " << q3 << ")" << std::endl;
	}

//******************************************************************************************

	union {
		ValueType a[4];
		struct {
			ValueType q0, q1, q2, q3;          // standard names for components
		};
	};

private:
};

//******************************************************************************************
//******************************************************************************************

//! A simple 3D matrix
/*!
	Matrix looks like this:<br>
	0 1 2<br>
	3 4 5<br>
	6 7 8<br>
*/
template<class T>
class Matrix3D {

public:

	typedef unsigned int UInt;

	//! Value-Type of Templates components (i.e. float or double).
	typedef T ValueType;

	//! Value-Type of Templates components (i.e. float or double).
	typedef point3d<T> Vector;

//******************************************************************************************

	//! Constructor: Creates a matrix from the values a_, b_, c_, d_, e_, f_, g_, h_, i_.
	Matrix3D(ValueType a_, ValueType b_, ValueType c_,
			 ValueType d_, ValueType e_, ValueType f_,
			 ValueType g_, ValueType h_, ValueType i_) {

		a[0] = a_;
		a[1] = b_;
		a[2] = c_;
		a[3] = d_;
		a[4] = e_;
		a[5] = f_;
		a[6] = g_;
		a[7] = h_;
		a[8] = i_;
		values_calculated = false;
		vectors_calculated = false;

		//check if matrix is orthonormal
		//point3d<T> v0(a[0], a[1], a[2]);
		//point3d<T> v1(a[3], a[4], a[5]);
		//point3d<T> v2(a[6], a[7], a[8]);
		//std::cerr << v0.length() << std::endl;
		//std::cerr << v1.length() << std::endl;
		//std::cerr << v2.length() << std::endl;
		//std::cerr << (v0|v1) << std::endl;
		//std::cerr << (v0|v2) << std::endl;
		//std::cerr << (v1|v2) << std::endl;

	};

//******************************************************************************************

	//! Constructor: Creates a matrix from the values a_[0]...a_[8].
	Matrix3D(ValueType* a_) {
		for (UInt i1 = 0; i1 < 9; i1++)
            a[i1] = a_[i1];
		values_calculated = false;
		vectors_calculated = false;
	};

//******************************************************************************************

	//! Constructor: Creates a matrix from the values a_[0]...a_[8], where the array a is of a different type.
	template <class OTHER>
	Matrix3D(OTHER* a_) {
		for (UInt i1 = 0; i1 < 9; i1++)
            a[i1] = (ValueType)a_[i1];
		values_calculated = false;
		vectors_calculated = false;
	};

//******************************************************************************************

	//! Copy Constructor
	Matrix3D(const Matrix3D& other) {
		for (UInt i1 = 0; i1 < 9; i1++)
            a[i1] = other.a[i1];
		values_calculated = false;
		vectors_calculated = false;
	};

//******************************************************************************************

	//! Constructor: Creates a matrix with only zeros as entries.
	Matrix3D() {
		for (UInt i1 = 0; i1 < 9; i1++)
            a[i1] = 0;
		values_calculated = false;
		vectors_calculated = false;
	};

//******************************************************************************************

	//! Constructor: Creates a matrix from three vectors v0, v1, v2.
	/*!
		the resulting matrix will then be
		v0.x	v1.x	v2.x
		v0.y	v1.y	v2.y
		v0.z	v1.z	v2.z
	*/
	Matrix3D(point3d<ValueType> v0, point3d<ValueType> v1, point3d<ValueType> v2) {
		a[0] = v0.x; a[1] = v1.x; a[2] = v2.x;
		a[3] = v0.y; a[4] = v1.y; a[5] = v2.y;
		a[6] = v0.z; a[7] = v1.z; a[8] = v2.z;
		values_calculated = false;
		vectors_calculated = false;
	};

//******************************************************************************************

	//! sets the matrix to contain the identity
	void setID() {
		for (UInt i1 = 0; i1 < 9; i1++)
            a[i1] = 0;
		a[0] = 1;
		a[4] = 1;
		a[8] = 1;
		values_calculated = false;
		vectors_calculated = false;
	};

//******************************************************************************************

	//! Constructor: Creates a matrix from the tensor product of vector 'v0' and 'v1'.
	Matrix3D(point3d<ValueType> v0, point3d<ValueType> v1) {
		a[0] = v0.x*v1.x; a[1] = v0.x*v1.y; a[2] = v0.x*v1.z;
		a[3] = v0.y*v1.x; a[4] = v0.y*v1.y; a[5] = v0.y*v1.z;
		a[6] = v0.z*v1.x; a[7] = v0.z*v1.y; a[8] = v0.z*v1.z;
		values_calculated = false;
		vectors_calculated = false;
	};

//******************************************************************************************

	//! Constructor: Creates a matrix from the Quaternion 'q'.
	Matrix3D(Quaternion<ValueType> q) {
		a[0] = 1 - 2*q.q1*q.q1 - 2*q.q2*q.q2;
		a[1] = (q.q0*q.q1 - q.q2*q.q3)*2;
		a[2] = (q.q0*q.q2 + q.q1*q.q3)*2;

		a[3] = (q.q0*q.q1 + q.q2*q.q3)*2;
		a[4] = 1 - 2*q.q0*q.q0 - 2*q.q2*q.q2;
		a[5] = (q.q1*q.q2 - q.q0*q.q3)*2;

		a[6] = (q.q0*q.q2 - q.q1*q.q3)*2;
		a[7] = (q.q1*q.q2 + q.q0*q.q3)*2;
		a[8] = 1 - 2*q.q0*q.q0 - 2*q.q1*q.q1;

		//a[0] = q.q0*q.q0+q.q1*q.q1 - q.q2*q.q2-q.q3*q.q3;
		//a[1] = (q.q1*q.q2 - q.q0*q.q3)*2;
		//a[2] = (q.q1*q.q3 + q.q0*q.q3)*2;

		//a[3] = (q.q1*q.q2 + q.q0*q.q3)*2;
		//a[4] = q.q0*q.q0+q.q2*q.q2 - q.q1*q.q1-q.q3*q.q3;
		//a[5] = (q.q2*q.q3 - q.q0*q.q1)*2;

		//a[6] = (q.q1*q.q3 - q.q0*q.q2)*2;
		//a[7] = (q.q2*q.q3 + q.q0*q.q1)*2;
		//a[8] = q.q0*q.q0+q.q3*q.q3 - q.q1*q.q1-q.q2*q.q2;

		values_calculated = false;
		vectors_calculated = false;
	};
//******************************************************************************************

	//! Constructor: Creates a matrix from the Quaternion 'q'.
	void setFromQuaterinion(Quaternion<ValueType> q) {
		a[0] = q.q3*q.q3 + q.q0*q.q0 - q.q1*q.q1 - q.q2*q.q2;
		a[1] = (q.q0*q.q1 + q.q2*q.q3)*2;
		a[2] = (q.q0*q.q2 - q.q1*q.q3)*2;

		a[3] = (q.q0*q.q1 - q.q2*q.q3)*2;
		a[4] = q.q3*q.q3 - q.q0*q.q0 + q.q1*q.q1 - q.q2*q.q2;
		a[5] = (q.q1*q.q2 + q.q0*q.q3)*2;

		a[6] = (q.q0*q.q2 + q.q1*q.q3)*2;
		a[7] = (q.q1*q.q2 - q.q0*q.q3)*2;
		a[8] = q.q3*q.q3 - q.q0*q.q0 - q.q1*q.q1 + q.q2*q.q2;

		//a[0] = q.q0*q.q0+q.q1*q.q1 - q.q2*q.q2-q.q3*q.q3;
		//a[1] = (q.q1*q.q2 - q.q0*q.q3)*2;
		//a[2] = (q.q1*q.q3 + q.q0*q.q3)*2;

		//a[3] = (q.q1*q.q2 + q.q0*q.q3)*2;
		//a[4] = q.q0*q.q0+q.q1*q.q1 - q.q2*q.q2-q.q3*q.q3;
		//a[5] = (q.q2*q.q3 - q.q0*q.q1)*2;

		//a[6] = (q.q1*q.q3 - q.q0*q.q2)*2;
		//a[7] = (q.q2*q.q3 + q.q0*q.q1)*2;
		//a[8] = q.q0*q.q0+q.q1*q.q1 - q.q2*q.q2-q.q3*q.q3;

		values_calculated = false;
		vectors_calculated = false;
	};

//******************************************************************************************

	inline void setCol(int i, const Vector& v) {
		a[i] = v.x;
		a[i+3] = v.y;
		a[i+6] = v.z;
	}

//******************************************************************************************

	inline void setRow(int i, const Vector& v) {
		a[3*i] = v.x;
		a[3*i+1] = v.y;
		a[3*i+2] = v.z;
	}

//******************************************************************************************

	inline Vector getRow(int i) const {
		return Vector(a[3*i], a[3*i+1], a[3*i+2]);
	}


//******************************************************************************************

	inline Vector getCol(int i) const {
		return Vector(a[i], a[i+3], a[i+6]);
	}

//******************************************************************************************

	//! Matrixaddition.
	void add(const Matrix3D &m2) {
		for (UInt i1 = 0; i1 < 9; i1++)
            a[i1] += m2.a[i1];		
	};

//******************************************************************************************

	//! Matrixaddition.
	inline Matrix3D operator+(const Matrix3D &m2) const {
		return Matrix3D<ValueType>(a[0]+m2.a[0], a[1]+m2.a[1], a[2]+m2.a[2], a[3]+m2.a[3], a[4]+m2.a[4], a[5]+m2.a[5], a[6]+m2.a[6], a[7]+m2.a[7], a[8]+m2.a[8]);
	};

//******************************************************************************************

	//! Matrixsubtraktion.
	inline Matrix3D operator-(const Matrix3D &m2) const {
		return Matrix3D<ValueType>(a[0]-m2.a[0], a[1]-m2.a[1], a[2]-m2.a[2], a[3]-m2.a[3], a[4]-m2.a[4], a[5]-m2.a[5], a[6]-m2.a[6], a[7]-m2.a[7], a[8]-m2.a[8]);
	};

//******************************************************************************************

	//! Copy operator.
	//Matrix3D operator=(const Matrix3D &m2) const {
	//	return Matrix3D<ValueType>(m2.a[0], m2.a[1], m2.a[2], m2.a[3], m2.a[4], m2.a[5], m2.a[6], m2.a[7], m2.a[8]);
	//};


//******************************************************************************************

	//! Divide matrix by factor.
	inline Matrix3D operator/(const ValueType factor) const {
		return Matrix3D<ValueType>(a[0]/factor, a[1]/factor, a[2]/factor, a[3]/factor, a[4]/factor, a[5]/factor, a[6]/factor, a[7]/factor, a[8]/factor);
	};

//******************************************************************************************

	//! Multiply matrix by factor.
	inline Matrix3D operator*(const ValueType factor) const {
		return Matrix3D<ValueType>(a[0]*factor, a[1]*factor, a[2]*factor, a[3]*factor, a[4]*factor, a[5]*factor, a[6]*factor, a[7]*factor, a[8]*factor);
	};

//******************************************************************************************

	//! Multiply matrix by vector.
	inline point3d<T> operator*(point3d<ValueType> p) const {
		vec3d entries_vecx(a[0], a[1], a[2]);
		vec3d entries_vecy(a[3], a[4], a[5]);
		vec3d entries_vecz(a[6], a[7], a[8]);
		return entries_vecx*p.x + entries_vecy*p.y + entries_vecz*p.z;
	}

//******************************************************************************************

	//! Multiply matrix and matrix.
	inline Matrix3D operator*(const Matrix3D &m2) const {
		ValueType b[9];

		b[0] = a[0]*m2.a[0] + a[1]*m2.a[3] + a[2]*m2.a[6];
		b[1] = a[0]*m2.a[1] + a[1]*m2.a[4] + a[2]*m2.a[7];
		b[2] = a[0]*m2.a[2] + a[1]*m2.a[5] + a[2]*m2.a[8];
		b[3] = a[3]*m2.a[0] + a[4]*m2.a[3] + a[5]*m2.a[6];
		b[4] = a[3]*m2.a[1] + a[4]*m2.a[4] + a[5]*m2.a[7];
		b[5] = a[3]*m2.a[2] + a[4]*m2.a[5] + a[5]*m2.a[8];
		b[6] = a[6]*m2.a[0] + a[7]*m2.a[3] + a[8]*m2.a[6];
		b[7] = a[6]*m2.a[1] + a[7]*m2.a[4] + a[8]*m2.a[7];
		b[8] = a[6]*m2.a[2] + a[7]*m2.a[5] + a[8]*m2.a[8];


		return Matrix3D<ValueType>(b);
	};

//******************************************************************************************

	inline Vector operator[](unsigned int i) const {
		if (i < 3)
			return vec3d(a[i*3+0], a[i*3+1], a[i*3+2]);
		else {
			std::cerr << "Out of bounds access to Matrix3D" << std::endl;
			exit(EXIT_FAILURE);
		}
	}

//******************************************************************************************

	//! Divides all entries by 'factor'.
	void divide(ValueType factor) {
		for (UInt i1 = 0; i1 < 9; i1++)
            a[i1] /= factor;		
	};

//******************************************************************************************

	//! Transposes the matrix
	Matrix3D getTransposed() const {
		ValueType b[9];
		b[0] = a[0];
		b[1] = a[3];
		b[2] = a[6];
		b[3] = a[1];
		b[4] = a[4];
		b[5] = a[7];
		b[6] = a[2];
		b[7] = a[5];
		b[8] = a[8];

		return Matrix3D(b);
	}

//******************************************************************************************

	//! Transposes the matrix
	void transpose() {
		ValueType b[9];
		for (int i1 = 0; i1 < 9; i1++)
			b[i1] = a[i1];

		a[1] = b[3];
		a[3] = b[1];
		a[2] = b[6];
		a[6] = b[2];
		a[5] = b[7];
		a[7] = b[5];
	}

//******************************************************************************************

	//! Matrix-Vector Multiplication
	inline point3d<T> vecTransTransposedMatrix(const point3d<T>& p) const {
		point3d<T> ret;

		//ret[0] = a[0]*p.x + a[1]*p.y + a[2]*p.z;
		//ret[1] = a[3]*p.x + a[4]*p.y + a[5]*p.z;
		//ret[2] = a[6]*p.x + a[7]*p.y + a[8]*p.z;

		ret.x = _a0*p.x + _a3*p.y + _a6*p.z;
		ret.y = _a1*p.x + _a4*p.y + _a7*p.z;
		ret.z = _a2*p.x + _a5*p.y + _a8*p.z;

		return ret;
	}

//******************************************************************************************

	//! Matrix-Vector Multiplication
	inline point3d<T> vecTrans(const point3d<T>& p) const {
		point3d<T> ret;

		//ret[0] = a[0]*p.x + a[1]*p.y + a[2]*p.z;
		//ret[1] = a[3]*p.x + a[4]*p.y + a[5]*p.z;
		//ret[2] = a[6]*p.x + a[7]*p.y + a[8]*p.z;

		ret.x = _a0*p.x + _a1*p.y + _a2*p.z;
		ret.y = _a3*p.x + _a4*p.y + _a5*p.z;
		ret.z = _a6*p.x + _a7*p.y + _a8*p.z;

		return ret;
	}

//******************************************************************************************

	//! Multiplies all entries by a common factor.
	void mult(ValueType factor) {
		for (UInt i1 = 0; i1 < 9; i1++)
            a[i1] *= factor;		
	}

//******************************************************************************************

	//! Matrixmultiplication. Computes 'this'*'m2' and stores it in 'this'.
	void mult(Matrix3D &m2) {
		ValueType b[9];
		for (int i1 = 0; i1 < 9; i1++)
			b[i1] = a[i1];

		a[0] = b[0]*m2.a[0] + b[1]*m2.a[3] + b[2]*m2.a[6];
		a[1] = b[0]*m2.a[1] + b[1]*m2.a[4] + b[2]*m2.a[7];
		a[2] = b[0]*m2.a[2] + b[1]*m2.a[5] + b[2]*m2.a[8];
		a[3] = b[3]*m2.a[0] + b[4]*m2.a[3] + b[5]*m2.a[6];
		a[4] = b[3]*m2.a[1] + b[4]*m2.a[4] + b[5]*m2.a[7];
		a[5] = b[3]*m2.a[2] + b[4]*m2.a[5] + b[5]*m2.a[8];
		a[6] = b[6]*m2.a[0] + b[7]*m2.a[3] + b[8]*m2.a[6];
		a[7] = b[6]*m2.a[1] + b[7]*m2.a[4] + b[8]*m2.a[7];
		a[8] = b[6]*m2.a[2] + b[7]*m2.a[5] + b[8]*m2.a[8];
	}

//******************************************************************************************

	//! Calculates the eigenvalues and eigenvectors of the matrix. Note that the eigensystem is not necessarily right handed!
	bool calcEValuesAndVectors() {

		// Use jacobi's method:
		// Build 3x3 matrix NR-style:
		float** CV = new float*[4];
		for(int i1 = 0; i1 < 4; i1++)
			CV[i1] = new float[4];
		float lambda[4];
		float** v = new float*[4];
		for(int i1 = 0; i1 < 4; i1++)
			v[i1] = new float[4];


		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				CV[i+1][j+1] = (float)a[i+3*j];

		int num_of_required_jabobi_rotations;

		if (!jacobi(CV, 3, lambda, v, &num_of_required_jabobi_rotations)) {
			print();
			return false;
		}

		point3d<ValueType> vec1(v[1][1], v[2][1], v[3][1]);
		point3d<ValueType> vec2(v[1][2], v[2][2], v[3][2]);
		point3d<ValueType> vec3(v[1][3], v[2][3], v[3][3]);

		// Sort eigenvectors such the ev[0] is the largest (absolute)...
		if (fabs(lambda[1]) < fabs(lambda[2]) && fabs(lambda[1]) < fabs(lambda[3])) {
			ev2 = vec1;
			lambda_2 = lambda[1];
			if (fabs(lambda[2]) < fabs(lambda[3])) {
				ev1 = vec2;
				ev0 = vec3;
				lambda_1 = lambda[2];
				lambda_0 = lambda[3];
			} else {
				ev0 = vec2;
				ev1 = vec3;
				lambda_1 = lambda[3];
				lambda_0 = lambda[2];
			}
		} else if (fabs(lambda[2]) < fabs(lambda[1]) && fabs(lambda[2]) < fabs(lambda[3])) {
			ev2 = vec2;
			lambda_2 = lambda[2];
			if (fabs(lambda[1]) < fabs(lambda[3])) {
				ev1 = vec1;
				ev0 = vec3;
				lambda_1 = lambda[1];
				lambda_0 = lambda[3];
			} else {
				ev0 = vec1;
				ev1 = vec3;
				lambda_1 = lambda[3];
				lambda_0 = lambda[1];
			}
		} else { // lambda[3] smallest!
			ev2 = vec3;
			lambda_2 = lambda[3];
			if (fabs(lambda[1]) < fabs(lambda[2])) {
				ev1 = vec1;
				ev0 = vec2;
				lambda_1 = lambda[1];
				lambda_0 = lambda[2];
			} else {
				ev0 = vec1;
				ev1 = vec2;
				lambda_1 = lambda[2];
				lambda_0 = lambda[1];
			}
		}

		values_calculated = true;
		vectors_calculated = true;

		for(int i1 = 0; i1 < 4; i1++) {
			delete[] v[i1];
			delete[] CV[i1];
		}
		delete[] v;
		delete[] CV;

		return true;

	};

//******************************************************************************************

	//! Calculates the eigenvalues and eigenvectors of the matrix. Note that the eigensystem is not necessarily right handed!
	bool calcEValuesAndVectorsSortByValue() {

		// Use jacobi's method:
		// Build 3x3 matrix NR-style:
		float** CV = new float*[4];
		for(int i1 = 0; i1 < 4; i1++)
			CV[i1] = new float[4];
		float lambda[4];
		float** v = new float*[4];
		for(int i1 = 0; i1 < 4; i1++)
			v[i1] = new float[4];


		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				CV[i+1][j+1] = (float)a[i+3*j];


		int num_of_required_jabobi_rotations;

		if (!jacobi(CV, 3, lambda, v, &num_of_required_jabobi_rotations)) {
			print();
			return false;
		}

		point3d<ValueType> vec1(v[1][1], v[2][1], v[3][1]);
		point3d<ValueType> vec2(v[1][2], v[2][2], v[3][2]);
		point3d<ValueType> vec3(v[1][3], v[2][3], v[3][3]);

		// Sort eigenvectors such the ev[0] is the smallest...
		if (lambda[1] < lambda[2] && lambda[1] < lambda[3]) {
			ev2 = vec1;
			lambda_2 = lambda[1];
			if (lambda[2] < lambda[3]) {
				ev1 = vec2;
				ev0 = vec3;
				lambda_1 = lambda[2];
				lambda_0 = lambda[3];
			} else {
				ev0 = vec2;
				ev1 = vec3;
				lambda_1 = lambda[3];
				lambda_0 = lambda[2];
			}
		} else if (lambda[2] < lambda[1] && lambda[2] < lambda[3]) {
			ev2 = vec2;
			lambda_2 = lambda[2];
			if (lambda[1] < lambda[3]) {
				ev1 = vec1;
				ev0 = vec3;
				lambda_1 = lambda[1];
				lambda_0 = lambda[3];
			} else {
				ev0 = vec1;
				ev1 = vec3;
				lambda_1 = lambda[3];
				lambda_0 = lambda[1];
			}
		} else { // lambda[3] smallest!
			ev2 = vec3;
			lambda_2 = lambda[3];
			if (lambda[1] < lambda[2]) {
				ev1 = vec1;
				ev0 = vec2;
				lambda_1 = lambda[1];
				lambda_0 = lambda[2];
			} else {
				ev0 = vec1;
				ev1 = vec2;
				lambda_1 = lambda[2];
				lambda_0 = lambda[1];
			}
		}

		values_calculated = true;
		vectors_calculated = true;

		for(int i1 = 0; i1 < 4; i1++) {
			delete[] v[i1];
			delete[] CV[i1];
		}
		delete[] v;
		delete[] CV;

		return true;

	};

//******************************************************************************************
//******************************************************************************************

	//! Calculates the eigenvalues and eigenvectors of the matrix. Note that the eigensystem is not necessarily right handed!
	bool calcEValuesAndVectorsSortByValueReverse() {

		// Use jacobi's method:
		// Build 3x3 matrix NR-style:
		float** CV = new float*[4];
		for(int i1 = 0; i1 < 4; i1++)
			CV[i1] = new float[4];
		float lambda[4];
		float** v = new float*[4];
		for(int i1 = 0; i1 < 4; i1++)
			v[i1] = new float[4];


		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				CV[i+1][j+1] = (float)a[i+3*j];


		int num_of_required_jabobi_rotations;

		if (!jacobi(CV, 3, lambda, v, &num_of_required_jabobi_rotations)) {
			print();
			return false;
		}

		point3d<ValueType> vec1(v[1][1], v[2][1], v[3][1]);
		point3d<ValueType> vec2(v[1][2], v[2][2], v[3][2]);
		point3d<ValueType> vec3(v[1][3], v[2][3], v[3][3]);

		// Sort eigenvectors such the ev[0] is the smallest...
		if (lambda[1] > lambda[2] && lambda[1] > lambda[3]) {
			ev2 = vec1;
			lambda_2 = lambda[1];
			if (lambda[2] > lambda[3]) {
				ev1 = vec2;
				ev0 = vec3;
				lambda_1 = lambda[2];
				lambda_0 = lambda[3];
			} else {
				ev0 = vec2;
				ev1 = vec3;
				lambda_1 = lambda[3];
				lambda_0 = lambda[2];
			}
		} else if (lambda[2] > lambda[1] && lambda[2] > lambda[3]) {
			ev2 = vec2;
			lambda_2 = lambda[2];
			if (lambda[1] > lambda[3]) {
				ev1 = vec1;
				ev0 = vec3;
				lambda_1 = lambda[1];
				lambda_0 = lambda[3];
			} else {
				ev0 = vec1;
				ev1 = vec3;
				lambda_1 = lambda[3];
				lambda_0 = lambda[1];
			}
		} else { // lambda[3] largest!
			ev2 = vec3;
			lambda_2 = lambda[3];
			if (lambda[1] > lambda[2]) {
				ev1 = vec1;
				ev0 = vec2;
				lambda_1 = lambda[1];
				lambda_0 = lambda[2];
			} else {
				ev0 = vec1;
				ev1 = vec2;
				lambda_1 = lambda[2];
				lambda_0 = lambda[1];
			}
		}

		values_calculated = true;
		vectors_calculated = true;

		for(int i1 = 0; i1 < 4; i1++) {
			delete[] v[i1];
			delete[] CV[i1];
		}
		delete[] v;
		delete[] CV;

		return true;

	};

//******************************************************************************************

	//! Checks if matrix is orthonormal.
	bool isOrthonormal() {
		point3d<T> v0(a[0], a[1], a[2]);
		point3d<T> v1(a[3], a[4], a[5]);
		point3d<T> v2(a[6], a[7], a[8]);
		std::cerr << v0.length() << std::endl;
		std::cerr << v1.length() << std::endl;
		std::cerr << v2.length() << std::endl;
		std::cerr << (v0|v1) << std::endl;
		std::cerr << (v0|v2) << std::endl;
		std::cerr << (v1|v2) << std::endl;

		return true;
	}

//******************************************************************************************

	//! Returns the determinant of the matrix.
	inline ValueType determinant() const {
		ValueType det = a[0]*a[4]*a[8] + a[1]*a[5]*a[6] + a[2]*a[3]*a[7] - a[0]*a[5]*a[7] - a[1]*a[3]*a[8] - a[2]*a[4]*a[6];
		return det;
	};

//******************************************************************************************

	//! Inverts the matrix.
	void invert() {

		ValueType det = determinant();

		if (det == 0)
			std::cerr << "Determinant is zero while inverting matrix!!!!" << std::endl;

//		std::cerr << "det: " << det << std::endl;
		invert_ignore_scale();
		divide(det);
	}
    
//******************************************************************************************

	//! Inverts the matrix, doesn't divide matrix by determinat.
	void invert_ignore_scale() {

//		std::cerr << "Invert" << std::endl;
		ValueType b[9];

		b[0] = a[4]*a[8] - a[5]*a[7];
		b[1] = -a[1]*a[8] + a[2]*a[7];
		b[2] = a[1]*a[5] - a[2]*a[4];

		b[3] = -a[3]*a[8] + a[5]*a[6];
		b[4] = a[0]*a[8] - a[2]*a[6];
		b[5] = -a[0]*a[5] + a[2]*a[3];

		b[6] = a[3]*a[7] - a[4]*a[6];
		b[7] = -a[0]*a[7] + a[1]*a[6];
		b[8] = a[0]*a[4] - a[1]*a[3];

		for (int i1 = 0; i1 < 9; i1++)
			a[i1] = b[i1];
	}

//******************************************************************************************

	//! Prints the elements of the matrix.
	void print() const {
		std::cerr << "/ "  << std::fixed << std::setprecision(5) << a[0] << "   " << std::setprecision(5) << a[1] << std::setprecision(5) << "   " << a[2] << " \\" << std::endl; 
		std::cerr << "| "  << std::fixed << std::setprecision(5) << a[3] << "   " << std::setprecision(5) << a[4] << std::setprecision(5) << "   " << a[5] << " |" << std::endl; 
		std::cerr << "\\ " << std::fixed << std::setprecision(5) << a[6] << "   " << std::setprecision(5) << a[7] << std::setprecision(5) << "   " << a[8] << " /" << std::endl; 
	};

//******************************************************************************************

	//! Returns the i-th (0th or 1th or 2th) eigenvector. Eigenvectors are normalized. Note that the eigensystem is not necessarily right handed!
	point3d<ValueType> getEV(int i) {
#ifndef DONOTCHECKIFEVSARECALCULATED
		if (!vectors_calculated)
			calcEValuesAndVectors();
#endif
		if (i == 0)
			return ev0;
		else if (i == 1)
			return ev1;
		else
			return ev2;
	};

//******************************************************************************************

	//! Returns the i-th (0th or 1th or 2th) eigenvalue.
	ValueType getEValue(int i) {
#ifndef DONOTCHECKIFEVSARECALCULATED
		if (!vectors_calculated)
			calcEValuesAndVectors();
#endif
		if (i == 0)
			return lambda_0;
		else if (i == 1)
			return lambda_1;
		else
			return lambda_2;
	};

//******************************************************************************************

	//! Alibi function, does always return false!!!
	bool operator<(const Matrix3D& other) const {
		return false;
	}

//******************************************************************************************

	//! Alibi function, does always return false!!!
	bool operator>(const Matrix3D& other) const {
		return false;
	}

// ****************************************************************************************

	//! Alibi function, does always return false!!!
	bool operator==(const Matrix3D& other) const {
		return false;
	}

//******************************************************************************************

	union {
		//struct {
		//	Vector entries_vec[3];		// Entries_stored as three Vectors
		//};
		ValueType a[9];					// Entries stored as array
		struct {
			ValueType _a0,_a1,_a2,_a3,_a4,_a5,_a6,_a7,_a8;          // standard names for components
		};
	};

//******************************************************************************************

private:

	//! Set to true when the eigenvalues have been calculated.
	bool values_calculated;

	//! Set to true when the eigenvectors have been calculated.
	bool vectors_calculated;

	//! eigenvalues of the matrix,
	ValueType lambda_0, lambda_1, lambda_2;

	//! eigenvectors of the matrix,
	point3d<ValueType> ev0, ev1, ev2;

};

typedef Matrix3D<double> mat3d;
typedef Matrix3D<float> mat3f;


//! write a Matrix3D to a stream
template <class T> inline std::ostream& operator<<(std::ostream& s, const Matrix3D<T>& m)
{ return (s << m.a[0] << " " << m.a[1] << " " << m.a[2] << " " << m.a[3] << " " << m.a[4] << " " << m.a[5] << " " << m.a[6] << " " << m.a[7] << " " << m.a[8]);}




#endif
