#ifndef SCAPE_REGISTER_2
#define SCAPE_REGISTER_2

#define POINT_TO_PLANE

#define NO_GSL
#include "SCAPE.h"
#include "levmar.h"
#include "operators/computeVertexNormals.h"


// ****************************************************************************************


class SCAPE_Register2 {


// ****************************************************************************************


	struct SCAPE_Register2_Params {
		
		SCAPE_Register2_Params (const std::vector<vec3d>& target, const std::vector<vec3d>& targetNormals_, const SCAPE_Model& sm_) : targetVerts(target), sm(sm_), targetNormals(targetNormals_), weightScape(0.5), weightPose(0.1) {}


		const std::vector<vec3d>& targetVerts;
		const SCAPE_Model& sm;
		const std::vector<vec3d>& targetNormals;
		std::vector<double> weights;
		double weightScape;
		double weightPose;
		int numScapeParams;
		std::vector<double> pose_params;
	};
	

// ****************************************************************************************


	static void evalGoodnessOfFitPose(double *p, double *x, int m, int n, void *dvoid) {

		SCAPE_Register2_Params data = *(SCAPE_Register2_Params*)dvoid;
		int numPoints = data.targetVerts.size();

		// Convert pca coeffs from stddeviation normalized space to real values
		std::vector<double> coeffs(data.sm.pcabasis.numVecs + data.sm.numParts*3);
		for (int i1 = 0; i1 < m; i1++) {
			coeffs[i1] = p[i1];
		}

		std::vector<vec3d> newverts = data.sm.reconstructFromSCAPEParams(coeffs);
		data.sm.rigidlyAlignModel(newverts, data.targetVerts, &data.weights);

		double error = 0;
		for (int i1 = 0; i1 < numPoints; i1++) {
			const vec3d& pos = newverts[i1];
			const vec3d& target_pos = data.targetVerts[i1];
			const vec3d& target_n = data.targetNormals[i1];
			double dist = (pos-target_pos)|target_n;
			//if ((dist > 0) || data.skinverts[i1]) dist *= 10;
			dist *= data.weights[i1];
			x[i1] = dist;
			error += x[i1]*x[i1];
		}


		// Pose error:
		SCAPE_Model::PartRotations pr;
		for (int i1 = 0; i1 < data.sm.numParts; i1++) {
			vec3d rot(p[i1*3+0], p[i1*3+1], p[i1*3+2]);
			pr.R[i1].setToRotationMatrixNew(rot);
		}
		SCAPE_Model::JointRotations jr = data.sm.getJointRotationsFromPartRotations(pr);
		std::vector<double> params;
		for (int i1 = 0; i1 < data.sm.numJoints; i1++) {
			const vec3d& j = jr.rv[i1];
			params.push_back(j.x);
			params.push_back(j.y);
			params.push_back(j.z);
		}
		for (UInt i1 = 0; i1 < params.size(); i1++) params[i1] -= data.sm.avg_jointangles[i1];
		std::vector<double> joints_in_pca = data.sm.pose_pcabasis.projectIntoBasis(params);

		double err_pose = 0;
		for (UInt i1 = 0; i1 < joints_in_pca.size(); i1++) {
			double d = joints_in_pca[i1];
			d /= sqrt(data.sm.pose_pcabasis.EVals[i1]);
			d *= data.weightPose;
			x[numPoints+i1] = d;
			err_pose += d*d;
		}


		std::cerr << "Error: " << error+err_pose << " | " << error  << " | " << err_pose << std::endl;

	}


// ****************************************************************************************


	static void evalGoodnessOfFitShape(double *p, double *x, int m, int n, void *dvoid) {

		SCAPE_Register2_Params data = *(SCAPE_Register2_Params*)dvoid;
		int numPoints = data.targetVerts.size();

		// Convert pca coeffs from stddeviation normalized space to real values
		for (int i1 = 0; i1 < m; i1++) {
			data.pose_params[data.sm.numParts*3 + i1] = p[i1] * sqrt(data.sm.pcabasis.EVals[i1]);
		}

		std::vector<vec3d> newverts = data.sm.reconstructFromSCAPEParams(data.pose_params);
		data.sm.rigidlyAlignModel(newverts, data.targetVerts, &data.weights);

		double error = 0;
		for (int i1 = 0; i1 < numPoints; i1++) {
			const vec3d& pos = newverts[i1];
			const vec3d& target_pos = data.targetVerts[i1];
			const vec3d& target_n = data.targetNormals[i1];
			double dist = (pos-target_pos)|target_n;
			dist *= data.weights[i1];
			x[i1] = dist;
			error += x[i1]*x[i1];
		}


		// SCAPE error (in stddeviations)
		double errorScape = 0;
		for (int i1 = 0; i1 < m; i1++) {
			x[i1] = p[i1]*data.weightScape;
			errorScape += x[i1]*x[i1];
		}

		std::cerr << "Error: " << error+errorScape << " | " << error << " | " << errorScape << std::endl;

	}

	
// ****************************************************************************************

public:

// ****************************************************************************************

	static std::vector<vec3d> fitScapePose(const SCAPE_Model& sm, const std::vector<vec3d>& target, std::vector<double> weights, std::vector<double>& init_fit, int numIter = 100) {


		int numUnknown = sm.numParts*3;
		if (init_fit.size() == 0) init_fit.resize(numUnknown);


		std::vector<vec3d> normals = computeVertexNormals(target, sm.default_mesh_triangles);
		SCAPE_Register2_Params param(target, normals, sm);
		param.weights = weights;
		param.weightPose = 0.1;

		std::cerr << "Initial fit: ";
		for (UInt i1 = 0; i1 < init_fit.size(); i1++) std::cerr << init_fit[i1] << " ";
		std::cerr << std::endl;


		int numEqns = (int)target.size()+numUnknown;

		std::cerr << "numUnknown: " << numUnknown << std::endl;

		double info[LM_INFO_SZ];
		int ret = dlevmar_dif(evalGoodnessOfFitPose, &init_fit[0], NULL, numUnknown, numEqns, numIter, NULL, info, NULL, NULL, &param);
		printf("Levenberg-Marquardt returned in %g iter, reason %g, sumsq %g [%g]\n", info[5], info[6], info[1], info[0]);

		std::cerr << "Final fit: ";
		for (UInt i1 = 0; i1 < init_fit.size(); i1++) std::cerr << init_fit[i1] << " ";
		std::cerr << std::endl;


		init_fit.resize(init_fit.size()+sm.pcabasis.numVecs);
		std::vector<vec3d> newverts = sm.reconstructFromSCAPEParams(init_fit);
		sm.rigidlyAlignModel(newverts, target, &weights);

		return newverts;
	}

// ****************************************************************************************

	
	static std::vector<vec3d> fitScapeShape(const SCAPE_Model& sm, const std::vector<vec3d>& target, std::vector<double> weights, std::vector<double>& init_fit, std::vector<double> pose_params, int numShapeParams = 5, int numIter = 100) {


		int numUnknown = numShapeParams;
		if (init_fit.size() == 0) init_fit.resize(numUnknown);

		std::vector<vec3d> normals = computeVertexNormals(target, sm.default_mesh_triangles);
		SCAPE_Register2_Params param(target, normals, sm);
		param.weights = weights;
		param.weightScape = 0.1;
		param.weightPose = 0.1;
		param.numScapeParams = numShapeParams;
		param.pose_params = pose_params;

		std::cerr << "Initial fit: ";
		for (UInt i1 = 0; i1 < init_fit.size(); i1++) std::cerr << init_fit[i1] << " ";
		std::cerr << std::endl;

		int numEqns = (int)target.size()+numShapeParams;

		std::cerr << "numUnknown: " << numUnknown << std::endl;

		double info[LM_INFO_SZ];
		int ret = dlevmar_dif(evalGoodnessOfFitShape, &init_fit[0], NULL, numUnknown, numEqns, numIter, NULL, info, NULL, NULL, &param);
		printf("Levenberg-Marquardt returned in %g iter, reason %g, sumsq %g [%g]\n", info[5], info[6], info[1], info[0]);

		std::cerr << "Final fit: ";
		for (UInt i1 = 0; i1 < init_fit.size(); i1++) std::cerr << init_fit[i1] << " ";
		std::cerr << std::endl;

		for (int i1 = 0; i1 < numUnknown; i1++) pose_params[sm.numParts*3 + i1] = init_fit[i1]*sqrt(sm.pcabasis.EVals[i1]);
		std::vector<vec3d> newverts = sm.reconstructFromSCAPEParams(pose_params);
		sm.rigidlyAlignModel(newverts, target, &weights);

		return newverts;
	}


// ****************************************************************************************

};

#endif