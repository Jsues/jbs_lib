#ifndef FIT_RBF_H
#define FIT_RBF_H

#include "rbf.h"
#include "pointNd.h"
#include "MathMatrix2.h"
#include "MathMatrixToImage.h"
#include "Timer.h"
#include "rng.h"

#ifdef USE_GSL
#include <cmath>
#include <gsl/gsl_math.h>
#include <gsl/gsl_ieee_utils.h>
#include <gsl/gsl_permute_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_linalg.h>
#endif

//#define USE_LINEAR_TERM

#ifdef USE_GSL
template <class ValueType>
bool solve_SVD(const ValueType* f_A, const ValueType* f_b, ValueType* f_x, const unsigned int n, const ValueType eps, const ValueType tikhonov_alpha) {
	gsl_matrix* mA       = gsl_matrix_alloc(n, n);		// matrix A 
	gsl_matrix* mV       = gsl_matrix_alloc(n, n);		// matrix V from svd A = USV^T
	gsl_vector* vs       = gsl_vector_alloc(n);			// singular values 
	gsl_matrix* mSinv    = gsl_matrix_alloc(n, n);		// pseudo inverse matrix of matrix S from svd
	gsl_vector* svd_work = gsl_vector_alloc(n);			// work vector required by gsl svd
	gsl_matrix* mC       = gsl_matrix_alloc(n, n);	    // temporary matrix will hold V * inv(S);
	gsl_vector* vb       = gsl_vector_alloc(n);			// right hand side
	gsl_vector* vx       = gsl_vector_alloc(n);			// vector we want to solve for

	// convert f_A and f_b to gsl data types
	unsigned int ofs = 0;
	for(unsigned int row = 0; row < n; row++) {
		for(unsigned int col = 0; col < n; col++) {
			gsl_matrix_set(mA, row, col, (double)f_A[ofs++]);
		}
		double d = gsl_matrix_get(mA, row, row);
		d += (double)tikhonov_alpha;
		gsl_matrix_set(mA, row, row, d);
		gsl_vector_set(vb, row, (double)f_b[row]);
	}

	// perform svd of mA
	int svd_res = gsl_linalg_SV_decomp(mA, mV, vs, svd_work);

	//fprintf(stderr, "Condition number: %f / %f = %f\n", vs->data[0], vs->data[n-1], vs->data[0]/vs->data[n-1]);

	// create pseudoinverse of mA and place it in mV
	{

		// create pseudo inverse of S
		gsl_matrix_set_zero(mSinv);
		for(unsigned int i = 0; i < n; i++) {
			double s = gsl_vector_get(vs, i);
			//std::cerr << "Singular value " << i << " " << s << "\n";
			s = (s <= eps) ? 0.0 : 1.0/s;	
			
			//s = s / (s*s + tikhonov_alpha * tikhonov_alpha);
			gsl_matrix_set(mSinv, i, i, s);
		}

		// C = V * inv(S) 
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, mV, mSinv, 0.0, mC);

		// V = C * U^T
		gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0,   mC, mA, 0.0, mV);

	}	
	// solve the system

	// x = V * b;
	gsl_blas_dgemv(CblasNoTrans, 1.0f, mV, vb, 0.0f, vx);

	// convert x to ValueType


	for(unsigned int row = 0; row < n; row++) {
		f_x[row] = (ValueType)gsl_vector_get(vx, row);
	}


	gsl_matrix_free(mA);
	gsl_matrix_free(mV);     
	gsl_matrix_free(mC); 
	gsl_vector_free(vs); 
	gsl_matrix_free(mSinv);
	gsl_vector_free(svd_work);
	gsl_vector_free(vb); 
	gsl_vector_free(vx); 


	return svd_res == GSL_SUCCESS;

}
#endif


template <class RBF_Type>
RBF_sum<RBF_Type>* fitRBFFunctions(typename RBF_Type::Point* points, typename RBF_Type::Point::ValueType* dists, UInt numCenters, typename RBF_Type::Point minV, typename RBF_Type::Point maxV, bool useConstraintsAsCenter = false, typename RBF_Type::Point::ValueType c_sq = 1, double tikhonovFactor = 0) {

	typedef RBF_Type::Point Point;
	typedef RBF_Type::Point::ValueType ValueType;
	UInt dim = Point::dim;

	RBF_sum<RBF_Type>* rbf;

	Point* centers = new Point[numCenters];
	if (useConstraintsAsCenter) {
		for (UInt i1 = 0; i1 < numCenters; i1++) {
			centers[i1] = points[i1];
		}
	} else {
		RNG randnum;
		for (UInt i1 = 0; i1 < numCenters; i1++) {
			Point pt;
			for (UInt d = 0; d < Point::dim; d++) {
				float xx = (float)randnum.uniform();
				pt[d] = minV[d]*xx + maxV[d]*(1-xx);
			}
			centers[i1] = pt;
		}
	}

	rbf = new RBF_sum<RBF_Type>(numCenters, &centers[0], NULL, 1, c_sq);


	UInt numUnknown = numCenters;
#ifdef USE_LINEAR_TERM
	numUnknown += dim+1;
#endif


	Point::ValueType* b = new Point::ValueType[numUnknown];
	MathMatrix2<RBF_Type::Point::ValueType> A(numUnknown);

	for (UInt x = 0; x < numCenters; x++) {
		Point p = points[x];
		for (UInt y = 0; y < numCenters; y++) {
			Point::ValueType psi = rbf->evalBasis(p, y);
			A(x,y) = psi;
		}
		b[x] = dists[x];
	}

#ifdef USE_LINEAR_TERM
	for (UInt x = 0; x < numCenters; x++) {
		Point p = points[x];

		A(numCenters, x) = A(x, numCenters) = 1;
		for (UInt y = 0; y < dim; y++) {
			A(numCenters+y+1, x) = A(x, numCenters+y+1) = p[y];
		}
	}

	for (UInt y = 0; y < dim+1; y++) {
		b[numCenters+y] = 0;
	}
#endif


	A.printMaple(5);


	ValueType maxEntry = 0;
	for (UInt i1 = 0; i1 < (UInt)numCenters; i1++) if (fabs(A(i1,i1)) > maxEntry) maxEntry = fabs(A(i1,i1));
	Point::ValueType* res = new Point::ValueType[numUnknown];

#ifndef USE_GSL
	for (UInt i1 = 0; i1 < (UInt)numCenters; i1++) A(i1,i1) += 8*numCenters*M_PI*tikhonovFactor;
	std::vector<int> indx;
	MathMatrix2<Point::ValueType> U(numUnknown);
	RBF_Type::Point::ValueType* sigma = new Point::ValueType[numUnknown];
	MathMatrix2<Point::ValueType> V(numUnknown);
	A.SVD_decomp(U, sigma, V, 300);
	A.SVD_bksolve(U, sigma, V, b, res);
#endif

#ifdef USE_GSL
	bool regular = solve_SVD<ValueType>(A.m, b, res, numUnknown, (ValueType)1.e-5 * maxEntry, maxEntry * 0);	
	if(!regular) {
		fprintf(stderr, "matrix is not regular!\n");		
	}
#endif

	for (UInt i1 = 0; i1 < numCenters; i1++) {
		rbf->m_heights[i1] = res[i1];
	}


#ifdef USE_LINEAR_TERM
	rbf->d = res[numCenters];
	for (UInt i1 = 0; i1 < dim; i1++) rbf->linear_term[i1] = res[numCenters+i1+1];
#endif

	delete[] b;
	delete[] res;
	return rbf;
}


template <class RBF_Type>
RBF_sum<RBF_Type>* aproximateWithGradientNOLINEAR(typename RBF_Type::Point* centers, UInt numCenters, RBFFunctionSamplesBase<typename RBF_Type::Point>* fs, typename RBF_Type::Point::ValueType radius = 1, typename RBF_Type::Point::ValueType c_square = 1, typename RBF_Type::Point::ValueType theta = 0.99) {

	typedef RBF_Type::Point Point;
	typedef Point::ValueType ValueType;

	RBF_sum<RBF_Type>* rbf = new RBF_sum<RBF_Type>(numCenters, &centers[0], NULL, radius, c_square);
	UInt numPoints = fs->getNumSamples();
	UInt dim = Point::dim;
	int numUnknown = numCenters;

	std::cerr << "numPoints: " << numPoints << std::endl;

	RBF_Type* basisfunction = &(rbf->basisfunction);
	//basisfunction->print();

	// Setup Matrix 'A1' and vector 'b1'
	MathMatrix2<Point::ValueType> A(numUnknown);
	Point::ValueType* b = new Point::ValueType[numUnknown];
	Point::ValueType psi_1, rhs_1, psi_2, rhs_2;
	for (UInt i = 0; i < numCenters; i++) {
		std::cerr << i << " / " << numCenters << "                \r";
		for (UInt j = i; j < numCenters; j++) {
			psi_1 = psi_2 = 0;
			for (UInt i3 = 0; i3 < numPoints; i3++) {
				const Point& pt = fs->getSamplePos(i3);
				Point dir_i = pt-centers[i];
				Point dir_j = pt-centers[j];
				ValueType d_i = dir_i.length();
				ValueType d_j = dir_j.length();
				psi_1 += basisfunction->eval(d_i)*basisfunction->eval(d_j);
				psi_2 += (dir_i*basisfunction->evalGradFactor(d_i))|(dir_j*basisfunction->evalGradFactor(d_j));
			}
			A(i,j) = A(j,i) = psi_1*theta + psi_2*(1-theta);
		}
		// The right hand side
		rhs_1 = rhs_2 = 0;
		for (UInt i3 = 0; i3 < numPoints; i3++) {
			const Point& pt = fs->getSamplePos(i3);
			Point dir_i = pt-centers[i];
			ValueType d_i = dir_i.length();
			rhs_1 += basisfunction->eval(d_i) * fs->getSampleVal(i3);
			rhs_2 += (dir_i*basisfunction->evalGradFactor(d_i))|(fs->getSampleGrad(i3)); // Residuum
		}
		b[i] = rhs_1*theta + rhs_2*(1-theta);
	}

	MathMatrix2<Point::ValueType> Chol(numUnknown);
	Point::ValueType* p = new Point::ValueType[numUnknown];
	Point::ValueType* res = new Point::ValueType[numUnknown];

	A.CHOLESKY_decomp(Chol, p);
	Chol.CHOLESKY_bksolve(p, b, res);

	//MathMatrix2<Point::ValueType> SVD2(numUnknown);
	//A.SVD_decomp(Chol, p, SVD2, 300);
	//std::cerr << "Condition: " << p[0]/p[numUnknown-1] << std::endl;
	//A.SVD_bksolve(Chol, p, SVD2, b, res);

	for (UInt i1 = 0; i1 < numCenters; i1++) {
		rbf->m_heights[i1] = res[i1];
	}

	delete[] b;
	delete[] p;
	delete[] res;

	return rbf;
}



template <class RBF_Type>
RBF_sum<RBF_Type>* aproximateWithGradient(typename RBF_Type::Point* centers, const UInt numCenters, RBFFunctionSamplesBase<typename RBF_Type::Point>* fs, typename RBF_Type::Point::ValueType radius = 1, const typename RBF_Type::Point::ValueType c_square = 1, const typename RBF_Type::Point::ValueType theta = 0.99, const typename RBF_Type::Point::ValueType tikhonovFactor = 0.01) {

	typedef RBF_Type::Point Point;
	typedef Point::ValueType ValueType;

	RBF_sum<RBF_Type>* rbf = new RBF_sum<RBF_Type>(numCenters, &centers[0], NULL, radius, c_square);
	UInt numPoints = fs->getNumSamples();
	const UInt dim = Point::dim;
	int numUnknown = numCenters + dim + 1;

	const ValueType theta2 = (1-theta);

	RBF_Type* basisfunction = &(rbf->basisfunction);


	//JBS_Timer tt;
	//tt.log("compute System Matrix");

	// Setup Matrix 'A1' and vector 'b1'
	MathMatrix2<Point::ValueType> A(numUnknown);
	Point::ValueType* b = new Point::ValueType[numUnknown];
	for (int i1 = 0; i1 < numUnknown; i1++) b[i1] = 0;

	for (UInt i1 = 0; i1 < numPoints; i1++) {
		const Point& pt		 = fs->getSamplePos(i1);
		const ValueType val  = fs->getSampleVal(i1);
		const Point& grad	 = fs->getSampleGrad(i1);

		for (UInt i = 0; i < numCenters; i++) {
			Point dir_i = pt-centers[i];
			ValueType d_i = dir_i.length();
			const ValueType phi_i = basisfunction->eval(d_i);
			const Point grad_phi_i = dir_i*basisfunction->evalGradFactor(d_i);
			// upper left numCenter(x)numCenter matrix
			for (UInt j = i; j < numCenters; j++) {
				const Point dir_j = pt-centers[j];
				const ValueType d_j = dir_j.length();
				const ValueType phi_j = basisfunction->eval(d_j);
				const Point grad_phi_j = dir_j*basisfunction->evalGradFactor(d_j);
				A(i,j) += (phi_i*phi_j)*theta + (grad_phi_i|grad_phi_j)*(1-theta);
			}
			// upper right matrix
			A(i, numCenters) += phi_i*theta;
			for (UInt i4 = 0; i4 < dim; i4++) {
				A(i, numCenters+1+i4) += (phi_i*pt[i4])*theta + ((grad_phi_i)[i4])*theta2;
			}
			// right hand side
			b[i] += (phi_i*val)*theta + ((grad_phi_i)|(grad))*theta2;
		}

		for (UInt i4 = 0; i4 < dim; i4++) {
			A(numCenters, numCenters+1+i4) += pt[i4]*theta;
		}
		for (UInt i4 = 0; i4 < dim; i4++) {
			for (UInt i5 = i4; i5 < dim; i5++) {
				ValueType tmp = 0;
				if (i4 == i5) tmp = (ValueType)1;
				A(numCenters+1+i4, numCenters+1+i5) += (pt[i4]*pt[i5])*theta + tmp*theta2;
			}
		}
		// Last (dim+1) entries of the right hand side
		b[numCenters] += val*theta;
		for (UInt i1 = 0; i1 < dim; i1++) {
			b[numCenters+1+i1] += (pt[i1]*val)*theta + (grad[i1])*theta2;
		}
	}
	A(numCenters, numCenters) = numPoints*theta;
	A.symmetrize();

	//A.printQuirin(5, "matrix32.txt");


	//A.printMaple(5, "mat_new.txt");
	//std::ofstream vec("vec1.txt");
	//vec << numUnknown << std::endl;
	//for (UInt i1 = 0; i1 < (UInt)numUnknown; i1++) vec << b[i1] << std::endl;

	Point::ValueType* res = new Point::ValueType[numUnknown];
	Point::ValueType* p = new Point::ValueType[numUnknown];
	
	ValueType maxEntry = 0;
	for (UInt i1 = 0; i1 < (UInt)numCenters; i1++) if (fabs(A(i1,i1)) > maxEntry) maxEntry = fabs(A(i1,i1));


	//tt.log("solve");

	for (UInt i1 = 0; i1 < (UInt)numCenters; i1++) A(i1,i1) += maxEntry*tikhonovFactor;
	MathMatrix2<Point::ValueType> Chol(numUnknown);
	bool chol_res = A.CHOLESKY_decomp(Chol, p);
	while (!chol_res) {
		std::cerr << "Cholesky failed in initial fit" << std::endl;
		for (UInt i1 = 0; i1 < (UInt)numCenters; i1++) A(i1,i1) += maxEntry*tikhonovFactor;
		chol_res = A.CHOLESKY_decomp(Chol, p);
	}
	Chol.CHOLESKY_bksolve(p, b, res);
	
	//tt.log();
	//tt.print();

	//bool regular = solve_SVD<ValueType>(A.m, b, res, numUnknown, (ValueType)1.e-5 * maxEntry, maxEntry * 0);	
	//if(!regular) {
	//	fprintf(stderr, "matrix is not regular!\n");		
	//}
	

#ifdef DEBUG_SHOW_CONDITION
	MathMatrix2<Point::ValueType> SVD2(numUnknown);
	A.SVD_decomp(Chol, p, SVD2, 300);
	std::cerr << "Condition: " << p[0]/p[numUnknown-1] << std::endl;
#endif

	for (UInt i1 = 0; i1 < numCenters; i1++) {
		rbf->m_heights[i1] = res[i1];
	}

	rbf->d = res[numCenters];
	for (UInt i1 = 0; i1 < dim; i1++) rbf->linear_term[i1] = res[numCenters+i1+1];


	delete[] b;
	delete[] p;
	delete[] res;

	return rbf;
}


template <class RBF_Type>
RBF_sum<RBF_Type>* aproximateWithGradientRunOverCenters(typename RBF_Type::Point* centers, const UInt numCenters, RBFFunctionSamplesBase<typename RBF_Type::Point>* fs, typename RBF_Type::Point::ValueType radius = 1, typename RBF_Type::Point::ValueType c_square = 1, typename RBF_Type::Point::ValueType theta = 0.99) {

	typedef RBF_Type::Point Point;
	typedef Point::ValueType ValueType;

	RBF_sum<RBF_Type>* rbf = new RBF_sum<RBF_Type>(numCenters, &centers[0], NULL, radius, c_square);
	UInt numPoints = fs->getNumSamples();
	const UInt dim = Point::dim;
	int numUnknown = numCenters;

#ifdef USE_LINEAR_TERM
	numUnknown += dim+1;
#endif

	//std::cerr << "numPoints: " << numPoints << std::endl;

	RBF_Type* basisfunction = &(rbf->basisfunction);
	//basisfunction->print();

	// Setup Matrix 'A1' and vector 'b1'
	MathMatrix2<Point::ValueType> A(numUnknown);
	Point::ValueType* b = new Point::ValueType[numUnknown];
	Point::ValueType psi_1, rhs_1, psi_2, rhs_2;
	for (UInt i = 0; i < numCenters; i++) {
		//std::cerr << i << " / " << numCenters << "                \r";
		for (UInt j = i; j < numCenters; j++) {
			psi_1 = psi_2 = 0;
			for (UInt i3 = 0; i3 < numPoints; i3++) {
				const Point& pt = fs->getSamplePos(i3);
				Point dir_i = pt-centers[i];
				Point dir_j = pt-centers[j];
				ValueType d_i = dir_i.length();
				ValueType d_j = dir_j.length();
				psi_1 += basisfunction->eval(d_i)*basisfunction->eval(d_j);
				psi_2 += (dir_i*basisfunction->evalGradFactor(d_i))|(dir_j*basisfunction->evalGradFactor(d_j));
			}
			A(i,j) = A(j,i) = psi_1*theta + psi_2*(1-theta);
		}

#ifdef USE_LINEAR_TERM
		// 1 row (constant term)
		psi_1 = 0; psi_2 = 0;
		for (UInt i3 = 0; i3 < numPoints; i3++) {
			psi_1 += basisfunction->eval(centers[i].dist(fs->getSamplePos(i3)));
		}
		A(i, numCenters) = A(numCenters, i) = psi_1*theta;
		// Linear term
		ValueType psis_1[dim], psis_2[dim];
		for (UInt i4 = 0; i4 < dim; i4++) { psis_1[i4] = 0; psis_2[i4] = 0; }
		for (UInt i3 = 0; i3 < numPoints; i3++) {
			for (UInt i4 = 0; i4 < dim; i4++) {
				const Point& pt = fs->getSamplePos(i3);
				Point dir_i = pt-centers[i];
				ValueType d_i = dir_i.length();
				psis_1[i4] += basisfunction->eval(d_i)*pt[i4];
				psis_2[i4] += (dir_i*basisfunction->evalGradFactor(d_i))[i4];
			}
		}
		for (UInt i4 = 0; i4 < dim; i4++) {
			A(i, numCenters+1+i4) = A(numCenters+1+i4, i) = psis_1[i4]*theta + psis_2[i4]*(1-theta);
		}
#endif

		// The right hand side
		rhs_1 = rhs_2 = 0;
		for (UInt i3 = 0; i3 < numPoints; i3++) {
			const Point& pt = fs->getSamplePos(i3);
			Point dir_i = pt-centers[i];
			ValueType d_i = dir_i.length();
			rhs_1 += basisfunction->eval(d_i) * fs->getSampleVal(i3);
			rhs_2 += (dir_i*basisfunction->evalGradFactor(d_i))|(fs->getSampleGrad(i3)); // Residuum
		}
		b[i] = rhs_1*theta + rhs_2*(1-theta);
	}

// Lower (dim+1)x(dim+1) Matrix:
#ifdef USE_LINEAR_TERM
	A(numCenters, numCenters) = numPoints*theta;
	for (UInt i4 = 0; i4 < dim; i4++) {
		psi_1 = 0;
		for (UInt i3 = 0; i3 < numPoints; i3++) {
			psi_1 += fs->getSamplePos(i3)[i4];
		}
		A(numCenters, numCenters+1+i4) = A(numCenters+1+i4, numCenters) = psi_1*theta;
	}
	for (UInt i4 = 0; i4 < dim; i4++) {
		for (UInt i5 = i4; i5 < dim; i5++) {
			psi_1 = 0; psi_2 = 0;
			for (UInt i3 = 0; i3 < numPoints; i3++) {
				psi_1 += fs->getSamplePos(i3)[i4]*fs->getSamplePos(i3)[i5];
			}
			if (i4 == i5) psi_2 = (ValueType)numPoints;
			A(numCenters+1+i4, numCenters+1+i5) = A(numCenters+1+i5, numCenters+1+i4) = psi_1*theta + psi_2*(1-theta);
		}
	}

	// Last (dim+1) entries of the right hand side
	psi_1 = 0; psi_2 = 0;
	for (UInt i1 = 0; i1 < fs->getNumSamples(); i1++) psi_1 += fs->getSampleVal(i1);
	b[numCenters] = psi_1*theta;
	for (UInt i1 = 0; i1 < dim; i1++) {
		psi_1 = 0; psi_2 = 0;
		for (UInt i3 = 0; i3 < numPoints; i3++) {
			psi_1 += fs->getSamplePos(i3)[i1] * fs->getSampleVal(i3);
			psi_2 += fs->getSampleGrad(i3)[i1];
		}
		b[numCenters+1+i1] = psi_1*theta + psi_2*(1-theta);
	}
#endif

	//A.printMaple(5, "mat_old.txt");
	//A.print();
	//for (UInt i1 = 0; i1 < (UInt)numUnknown; i1++) std::cerr << b[i1] << std::endl;
	//exit(1);

	Point::ValueType* res = new Point::ValueType[numUnknown];
	Point::ValueType* p = new Point::ValueType[numUnknown];
	
	//ValueType maxEntry = 0;
	//for (UInt i1 = 0; i1 < (UInt)numCenters; i1++) if (fabs(A(i1,i1)) > maxEntry) maxEntry = fabs(A(i1,i1));
	//for (UInt i1 = 0; i1 < (UInt)numCenters; i1++) A(i1,i1) += maxEntry*(REAL)0.001;
	//MathMatrix2<Point::ValueType> Chol(numUnknown);
	//bool chol_res = A.CHOLESKY_decomp(Chol, p);
	//while (!chol_res) {
	//	std::cerr << "Cholesky failed in initial fit" << std::endl;
	//	for (UInt i1 = 0; i1 < (UInt)numUnknown; i1++) A(i1,i1) += maxEntry*(REAL)0.1;
	//	chol_res = A.CHOLESKY_decomp(Chol, p);
	//}
	//Chol.CHOLESKY_bksolve(p, b, res);
	
	ValueType m = 0;
	for(int i = 0; i < numUnknown; i++) {
		m = std::max(fabs(A(i,i)), m);
	}
	bool regular = solve_SVD(A.m, b, res, numUnknown, 1.e-10f, m * 0);	
	if(!regular) {
		fprintf(stderr, "matrix is not regular!\n");		
	}
	

#ifdef DEBUG_SHOW_CONDITION
	MathMatrix2<Point::ValueType> SVD2(numUnknown);
	A.SVD_decomp(Chol, p, SVD2, 300);
	std::cerr << "Condition: " << p[0]/p[numUnknown-1] << std::endl;
#endif

	for (UInt i1 = 0; i1 < numCenters; i1++) {
		rbf->m_heights[i1] = res[i1];
	}

#ifdef USE_LINEAR_TERM
	rbf->d = res[numCenters];
	for (UInt i1 = 0; i1 < dim; i1++) rbf->linear_term[i1] = res[numCenters+i1+1];
#endif

	delete[] b;
	delete[] p;
	delete[] res;

	return rbf;
}




template <class RBF_Type>
RBF_sum<RBF_Type>* fitRBFFunctionsAproximate(typename RBF_Type::Point* centers, UInt numCenters, RBFFunctionSamplesBase<typename RBF_Type::Point>* fs, typename RBF_Type::Point::ValueType radius = 1, typename RBF_Type::Point::ValueType c_square = 1) {

	typedef RBF_Type::Point Point;
	typedef Point::ValueType ValueType;


	RBF_sum<RBF_Type>* rbf = new RBF_sum<RBF_Type>(numCenters, &centers[0], NULL, radius, c_square);
	RBF_Type* basisfunction = &(rbf->basisfunction);

	UInt numPoints = fs->getNumSamples();
	const UInt dim = Point::dim;

	int numUnknown = numCenters;
#ifdef USE_LINEAR_TERM
	numUnknown += dim+1;
#endif

	// Setup Matrix 'A' and vector 'b'
	MathMatrix2<Point::ValueType> A(numUnknown);
	Point::ValueType* b = new Point::ValueType[numUnknown];

	Point::ValueType psi, rhs;
	Point::ValueType psis[dim];

	for (UInt i = 0; i < numCenters; i++) {
		std::cerr << i << " / " << numCenters << "                \r";
		for (UInt j = i; j < numCenters; j++) {
			psi = 0;
			for (UInt i3 = 0; i3 < numPoints; i3++) {
				const Point& pt = fs->getSamplePos(i3);
				ValueType d_i = pt.dist(centers[i]);
				ValueType d_j = pt.dist(centers[j]);
				psi += basisfunction->eval(d_i)*basisfunction->eval(d_j);
			}
			A(i,j) = A(j,i) = psi;
		}

#ifdef USE_LINEAR_TERM
		// 1 row (constant term)
		psi = 0;
		for (UInt i3 = 0; i3 < numPoints; i3++) {
			psi += basisfunction->eval(fs->getSamplePos(i3).dist(centers[i]));
		}
		A(i, numCenters) = A(numCenters, i) = psi;
		// Linear term
		for (UInt i4 = 0; i4 < dim; i4++) psis[i4] = 0;
		for (UInt i3 = 0; i3 < numPoints; i3++) {
			for (UInt i4 = 0; i4 < dim; i4++) {
				psis[i4] += basisfunction->eval(fs->getSamplePos(i3).dist(centers[i]))*fs->getSamplePos(i3)[i4];
			}
		}
		for (UInt i4 = 0; i4 < dim; i4++) {
			A(i, numCenters+1+i4) = A(numCenters+1+i4, i) = psis[i4];
		}
#endif

		// The right hand side
		rhs = 0;
		for (UInt i3 = 0; i3 < numPoints; i3++) {
			const Point& pt = fs->getSamplePos(i3);
			ValueType d_i = pt.dist(centers[i]);
			rhs +=  basisfunction->eval(d_i) * fs->getSampleVal(i3);
		}

		b[i] = rhs;
	}

// Lower (dim+1)x(dim+1) Matrix:
#ifdef USE_LINEAR_TERM
	A(numCenters, numCenters) = numPoints;
	for (UInt i4 = 0; i4 < dim; i4++) {
		psi = 0;
		for (UInt i3 = 0; i3 < numPoints; i3++) {
			psi += fs->getSamplePos(i3)[i4];
		}
		A(numCenters, numCenters+1+i4) = A(numCenters+1+i4, numCenters) = psi;
	}
	for (UInt i4 = 0; i4 < dim; i4++) {
		for (UInt i5 = i4; i5 < dim; i5++) {
			psi = 0;
			for (UInt i3 = 0; i3 < numPoints; i3++) {
				psi += fs->getSamplePos(i3)[i4]*fs->getSamplePos(i3)[i5];
			}
			A(numCenters+1+i4, numCenters+1+i5) = A(numCenters+1+i5, numCenters+1+i4) = psi;
		}
	}

	// Last (dim+1) entries of the matrix:
	psi = 0;
	for (UInt i1 = 0; i1 < fs->getNumSamples(); i1++) psi += fs->getSampleVal(i1);
	b[numCenters] = psi;
	for (UInt i1 = 0; i1 < dim; i1++) {
		psi = 0;
		for (UInt i3 = 0; i3 < numPoints; i3++) {
			psi += fs->getSamplePos(i3)[i1] * fs->getSampleVal(i3);
		}
		b[numCenters+1+i1] = psi;
	}
#endif


	//MathMatrix2<Point::ValueType> U(numUnknown);
	//Point::ValueType* sigma = new Point::ValueType[numUnknown];
	//MathMatrix2<Point::ValueType> V(numUnknown);
	//A.SVD_decomp(U, sigma, V, 200);
	//Point::ValueType cond_num = sigma[0]/sigma[numUnknown-1];
	//std::cerr << "Condition Number: " << cond_num << "  - s0: " << sigma[0] << " - s1: " << sigma[numUnknown-1] << std::endl;


	MathMatrix2<Point::ValueType> Chol(numUnknown);
	Point::ValueType* p = new Point::ValueType[numUnknown];
	Point::ValueType* res = new Point::ValueType[numUnknown];
	A.CHOLESKY_decomp(Chol, p);
	Chol.CHOLESKY_bksolve(p, b, res);


	for (UInt i1 = 0; i1 < numCenters; i1++) rbf->m_heights[i1] = res[i1];

#ifdef USE_LINEAR_TERM
	rbf->d = res[numCenters];
	for (UInt i1 = 0; i1 < dim; i1++) rbf->linear_term[i1] = res[numCenters+i1+1];
#endif

	delete[] b;
	delete[] res;

	return rbf;
}


//template <class Point>
//RBF_sum<Point>* fitRBFFunctionsUseLinearFactor(Point* points, typename Point::ValueType* dists, UInt numCenters, vec3f minV, vec3f maxV, bool useConstraintsAsCenter = false) {
//
//	RBF_sum<Point>* rbf;
//
//	Point* centers = new Point[numCenters];
//	if (useConstraintsAsCenter) {
//		for (UInt i1 = 0; i1 < numCenters; i1++) {
//			centers[i1] = points[i1];
//		}
//	} else {
//		RNG randnum;
//		for (UInt i1 = 0; i1 < numCenters; i1++) {
//			Point pt;
//			float xx = (float)randnum.uniform();
//			float yy = (float)randnum.uniform();
//			float zz = (float)randnum.uniform();
//			pt.x = minV.x*xx + maxV.x*(1-xx);
//			pt.y = minV.y*yy + maxV.y*(1-yy);
//			pt.z = minV.z*zz + maxV.z*(1-zz);
//			centers[i1] = pt;
//		}
//	}
//
//	rbf = new RBF_sum<Point>(numCenters, &centers[0], NULL);
//
//	Point::ValueType* b = new Point::ValueType[numCenters+4];
//	MathMatrix2<Point::ValueType> A(numCenters+4);
//	for (UInt x = 0; x < numCenters; x++) {
//		Point p = points[x];
//		for (UInt y = 0; y < numCenters; y++) {
//			Point::ValueType psi = rbf->evalBasis(p, y);
//			A(x,y) = psi;
//		}
//		b[x] = dists[x];
//	}
//	// Linear part:
//	for (UInt x = 0; x < numCenters; x++) {
//		A(x, numCenters) = 1;
//		A(x, numCenters+1) = centers[x].x;
//		A(x, numCenters+2) = centers[x].y;
//		A(x, numCenters+3) = centers[x].z;
//		A(numCenters, x) = 1;
//		A(numCenters+1, x) = centers[x].x;
//		A(numCenters+2, x) = centers[x].y;
//		A(numCenters+3, x) = centers[x].z;
//	}
//	for (UInt x = numCenters; x < numCenters+4; x++) b[x] = 0;
//
//	std::vector<int> indx;
//	MathMatrix2<Point::ValueType> U(numCenters+4);
//	Point::ValueType* sigma = new Point::ValueType[numCenters+4];
//	MathMatrix2<Point::ValueType> V(numCenters+4);
//	A.SVD_decomp(U, sigma, V, 200);
//
//	// Compute condition number:
//	Point::ValueType s_min = std::numeric_limits<Point::ValueType>::max();
//	Point::ValueType s_max = -std::numeric_limits<Point::ValueType>::max();
//	for (UInt i = 0; i < numCenters+4; ++i) {
//		s_min = std::min(s_min, sigma[i]);
//		s_max = std::max(s_max, sigma[i]);
//	}
//	Point::ValueType condNum = s_max/s_min;
//	std::cerr << "Condition number: " << s_max << "/" << s_min << " = " << condNum << std::endl;
//
//
//	Point::ValueType* res = new Point::ValueType[numCenters+4];
//	A.SVD_bksolve(U, sigma, V, b, res);
//
//	for (UInt i1 = 0; i1 < numCenters; i1++) {
//		rbf->m_heights[i1] = res[i1];
//	}
//	rbf->linear_term = Point(res[numCenters+1], res[numCenters+2], res[numCenters+3]);
//	rbf->d = res[numCenters];
//
//	delete[] b;
//	delete[] res;
//	return rbf;
//}
//
//

#endif
