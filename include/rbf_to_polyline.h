#ifndef RBF_TO_POLYLINE_H
#define RBF_TO_POLYLINE_H

#include "Polyline.h"
#include "rbf.h"
#include "Volume.h"
#include "marchingcubes.h"
#include "FileIO/VolumeIO.h"
//#include "RBFTree2.h"

/*
template <class Point>
void WriteVolume(oldRBFTree::RBFTree<Point>* rbf_tree, UInt depth, vec2d minV, vec2d maxV) {
	vec2d diag = maxV-minV;
	minV -= diag*0.1;
	maxV += diag*0.1;
    
	int stepsMax = 120;
	double brick_size = std::max(maxV[0] - minV[0], maxV[1] - minV[1]) / (double)stepsMax;
	//std::cerr << "brick_size: " << brick_size << std::endl;
	const int brickX = 2 + (unsigned int)((maxV[0] - minV[0]) / brick_size);
	const int brickY = 2 + (unsigned int)((maxV[1] - minV[1]) / brick_size);
	Volume<double>* vol = new Volume<double>;
	vol->initWithDims(vec3d(minV.x, minV.y, 0), vec3d(maxV.x, maxV.y, 0), brickX, brickY, 1);

	double d;
	vec2d grad;
	for (int i = 0; i < brickX; ++i) {
		for (int j = 0; j < brickY; ++j) {
			vec3d pos = vol->pos(i,j,0);
			vec2d pos2d(pos.x, pos.y);
			d = rbf_tree->eval(pos2d, depth);
			vol->set(i,j,0, d);
		}
	}



	VolumeWriter<double> vw(vol);
	vw.writeRAWFile("bunny.raw");
	std::cerr << "brickX: " << brickX << "   brickY: " << brickY << std::endl;

}
*/

template <class Node>
PolyLine<vec3d> RBFTree_Cell_to_PolyLine(Node* node2) {

	PolyLine<vec3d> pl_base;
	Node::Point a = node2->getMin();
	Node::Point b = node2->getMax();
	pl_base.insertVertex(vec3d(a.x, a.y, 0));
	pl_base.insertVertex(vec3d(a.x, b.y, 0));
	pl_base.insertVertex(vec3d(b.x, b.y, 0));
	pl_base.insertVertex(vec3d(b.x, a.y, 0));
	pl_base.insertLine(0,1);
	pl_base.insertLine(2,1);
	pl_base.insertLine(2,3);
	pl_base.insertLine(0,3);

	return pl_base;
}





/*
PolyLine<vec3d> Function2D_to_PolyLineNoBlend(JBSlib::RBFTree<vec2d>* func, vec2d minV, vec2d maxV, double f = 0, UInt stepsMax=200) {

	vec2d diag = maxV-minV;
	minV -= diag*0.1;
	maxV += diag*0.1;

	double brick_size = std::max(maxV[0] - minV[0], maxV[1] - minV[1]) / (double)stepsMax;
	//std::cerr << "brick_size: " << brick_size << std::endl;
	const int brickX = 2 + (unsigned int)((maxV[0] - minV[0]) / brick_size);
	const int brickY = 2 + (unsigned int)((maxV[1] - minV[1]) / brick_size);
	Volume<double>* vol = new Volume<double>;
	vol->initWithDims(vec3d(minV.x, minV.y, 0), vec3d(maxV.x, maxV.y, 0), brickX, brickY, 1);

	double d;
	vec2d grad;
	for (int i = 0; i < brickX; ++i) {
		for (int j = 0; j < brickY; ++j) {
			vec3d pos = vol->pos(i,j,0);
			vec2d pos2d(pos.x, pos.y);
			d = func->evalNoBlend(pos2d);
			vol->set(i,j,0, d);
		}
	}


	PolyLine<vec3d> pl;
	getIsolineFromSlice<double>(vol, 0, f, &pl);
	for (UInt i1 = 0; i1 < pl.getNumV(); i1++) pl.points[i1].c.z = 0;

	return pl;

}
*/

template <class Point>
PolyLine<vec3d> Function2D_to_PolyLine(ScalarFunction<Point>* func, vec2d minV, vec2d maxV, typename Point::ValueType f = 0, UInt stepsMax=200, double diagScaler = 0.1) {

	vec2d diag = maxV-minV;
	minV -= diag*diagScaler;
	maxV += diag*diagScaler;

	double brick_size = std::max(maxV[0] - minV[0], maxV[1] - minV[1]) / (double)stepsMax;
	//std::cerr << "brick_size: " << brick_size << std::endl;
	const int brickX = 2 + (unsigned int)((maxV[0] - minV[0]) / brick_size);
	const int brickY = 2 + (unsigned int)((maxV[1] - minV[1]) / brick_size);
	Volume<double>* vol = new Volume<double>;
	vol->initWithDims(vec3d(minV.x, minV.y, 0), vec3d(maxV.x, maxV.y, 0), brickX, brickY, 1);

	double d;
	vec2d grad;
	for (int i = 0; i < brickX; ++i) {
		for (int j = 0; j < brickY; ++j) {
			vec3d pos = vol->pos(i,j,0);
			vec2d pos2d(pos.x, pos.y);
			d = func->eval(pos2d);
			vol->set(i,j,0, d);
		}
	}


	PolyLine<vec3d> pl;
	getIsolineFromSlice<double>(vol, 0, f, &pl);
	for (UInt i1 = 0; i1 < pl.getNumV(); i1++) pl.points[i1].c.z = 0;

	return pl;

}

/*
template <class Point>
PolyLine<vec3d> RBFTree_to_PolyLine(oldRBFTree::RBFTree<Point>* rbf_tree, UInt depth, vec2d minV, vec2d maxV, typename Point::ValueType f = 0, bool show_incr = false) {
	vec2d diag = maxV-minV;
	minV -= diag*0.1;
	maxV += diag*0.1;
    
	int stepsMax = 120;
	double brick_size = std::max(maxV[0] - minV[0], maxV[1] - minV[1]) / (double)stepsMax;
	//std::cerr << "brick_size: " << brick_size << std::endl;
	const int brickX = 2 + (unsigned int)((maxV[0] - minV[0]) / brick_size);
	const int brickY = 2 + (unsigned int)((maxV[1] - minV[1]) / brick_size);
	Volume<double>* vol = new Volume<double>;
	vol->initWithDims(vec3d(minV.x, minV.y, 0), vec3d(maxV.x, maxV.y, 0), brickX, brickY, 1);

	double d;
	vec2d grad;
	for (int i = 0; i < brickX; ++i) {
		for (int j = 0; j < brickY; ++j) {
			vec3d pos = vol->pos(i,j,0);
			vec2d pos2d(pos.x, pos.y);
			if (show_incr)
				d = rbf_tree->evalIncrementOnly(pos2d, depth);
			else
				d = rbf_tree->eval(pos2d, depth);
			vol->set(i,j,0, d);
		}
	}


	PolyLine<vec3d> pl_base;
	getIsolineFromSlice<double>(vol, 0, f, &pl_base);
	for (UInt i1 = 0; i1 < pl_base.getNumV(); i1++) pl_base.points[i1].c.z = 0;

	return pl_base;

}
*/

template <class RBF_Type>
PolyLine<vec3d> RBF_to_PolyLine(RBF_sum<RBF_Type>* rbf, vec2d minV, vec2d maxV, typename RBF_Type::ValueType iso = (RBF_Type::ValueType)0) {
	vec2d diag = maxV-minV;
	minV -= diag*0.1;
	maxV += diag*0.1;
    
	int stepsMax = 120;
	double brick_size = std::max(maxV[0] - minV[0], maxV[1] - minV[1]) / (double)stepsMax;
	//std::cerr << "brick_size: " << brick_size << std::endl;
	const int brickX = 2 + (unsigned int)((maxV[0] - minV[0]) / brick_size);
	const int brickY = 2 + (unsigned int)((maxV[1] - minV[1]) / brick_size);
	Volume<double>* vol = new Volume<double>;
	vol->initWithDims(vec3d(minV.x, minV.y, 0), vec3d(maxV.x, maxV.y, 0), brickX, brickY, 1);

	double d;
	vec2d grad;
	for (int i = 0; i < brickX; ++i) {
		for (int j = 0; j < brickY; ++j) {
			vec3d pos = vol->pos(i,j,0);
			vec2d pos2d(pos.x, pos.y);
			d = rbf->evalNormalize(pos2d);
			vol->set(i,j,0, d);
		}
	}


	PolyLine<vec3d> pl_base;
	getIsolineFromSlice<double>(vol, 0, iso, &pl_base);
	for (UInt i1 = 0; i1 < pl_base.getNumV(); i1++) pl_base.points[i1].c.z = 0;

	return pl_base;

}

#endif
