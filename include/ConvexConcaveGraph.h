#ifndef CONVEXCONCAVE_GRAPH_H
#define CONVEXCONCAVE_GRAPH_H


#include <vector>
#include <queue>
#include <list>
#include <map>
#include "SimpleMesh.h"
#include "booleanTree.h"

typedef std::vector<UInt> UIntList;

int iabs(int i) {
	if (i > 0) return i;
	else return -i;
}

class ConvexConcaveGraph;

enum NodeFlag {
	NONE = 0,
	IS_IN_GROUP,
	IS_VISITED
};

enum EdgeTypeEnum {
	CONVEX,
	CONCAVE,
	MIXED		// Only nodes may be mixed, edges are always either 'CONVEX' or 'CONCAVE'!
};

// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


class ConvexConcaveEdge {

public:

	//! Constructor.
	ConvexConcaveEdge(ConvexConcaveGraph* p, EdgeTypeEnum edgeType_, UInt n0_, UInt n1_)
		: parentGraph(p), n0(n0_), n1(n1_), edgeType(edgeType_) {}


	//! Returns the type of the edge, i.e. CONVEX or CONCAVE.
	inline EdgeTypeEnum getEdgeType() {
		return edgeType;
	}

	//! Returns the other node, i.e. if i == n0 then n1 else n0.
	UInt getOther(UInt i) {
		if (i == n0) return n1;
		return n0;
	}

	inline bool operator==(const ConvexConcaveEdge& other) const {
		if ((n0 == other.n0 && n1 == other.n1) || (n1 == other.n0 && n0 == other.n1))
			return true;

		return false;
	}

	void print() {
		std::cerr << n0 << " - " << n1 << std::endl;
	}

	//! The nodes that are connected by this edge.
	UInt n0;
	//! The nodes that are connected by this edge.
	UInt n1;

private:

	//! Type of the edge, i.e. CONVEX or CONCAVE
	EdgeTypeEnum edgeType;
	//! Parent graph
	ConvexConcaveGraph* parentGraph;

};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


class ConvexConcaveNode {

public:

	friend class ConvexConcaveGraph;

	//! Constructor.
	ConvexConcaveNode(ConvexConcaveGraph* p, BooleanTreeNode<vec3d>* func_) : parentGraph(p) {
		func = func_;
		flag = NONE;
		size = 1;
	}

	//! Destructor.
	~ConvexConcaveNode() {
	}

	//! Adds the edge 'eID' to the node's edgeList.
	void addEdge(UInt eID) {
		edges.push_back(eID);
	}

	//! Returns the center of the boolean function.
	vec3d getNodeCenter() {
		return func->getCenter();
	}

	//! Returns the center of the boolean function.
	vec3d getNodeNormal() {
		return func->getNormal();
	}

	//! Returns wether the node contains the edge 'e'.
	bool edgeExists(ConvexConcaveEdge e);

	//! Returns whether the node is completely 'CONVEX' or 'CONCAVE' or 'MIXED' (depending on adjacent edges);
	EdgeTypeEnum getNodeType();

	//! Flag which may be used as required. Initiated with 'NONE = 0'
	NodeFlag flag;

	//! number of nodes grouped in this node.
	UInt size;

	//! The boolean function associated to this node, may be a plane or a group;
	BooleanTreeNode<vec3d>* func;

private:

	//! Parent graph
	ConvexConcaveGraph* parentGraph;

	//! Edges of this node
	UIntList edges;

};


// ****************************************************************************************
// ****************************************************************************************
// ****************************************************************************************


class ConvexConcaveGraph {
	
public:

	//! Constructor.
	ConvexConcaveGraph() {}

	//! Performs one collaps step and returns the new graph.
	ConvexConcaveGraph* firstOrderCollapse() {
		ConvexConcaveGraph* newGraph = new ConvexConcaveGraph;

		UInt numNodes = getNumNodes();

		std::map<UInt, UInt> newNodeIDs;
		// collaps purely convex/concave nodes:
		for (UInt i1 = 0; i1 < numNodes; i1++) {

			EdgeTypeEnum nodeType = nodes[i1].getNodeType();
			if (nodeType == MIXED) continue;

			if (nodes[i1].flag != IS_IN_GROUP) { // Node is not yet contained in a group
				std::list<UInt> group = getPureNodesConnected(i1, nodeType);
				for (std::list<UInt>::iterator it = group.begin(); it != group.end(); ++it) {
					UInt neighbor = *it;
					newNodeIDs.insert(std::pair<UInt, UInt>(neighbor, i1));
					nodes[neighbor].flag = IS_IN_GROUP;
				}
			}
		}

		compactify(newGraph, newNodeIDs);

		return newGraph;
	}

	//! Performs one collaps step and returns the new graph.
	ConvexConcaveGraph* secondOrderCollapse() {
		ConvexConcaveGraph* newGraph = new ConvexConcaveGraph;

		UInt numNodes = getNumNodes();

		std::map<UInt, UInt> newNodeIDs;
		for (UInt i1 = 0; i1 < numNodes; i1++) {
			if (nodes[i1].getNodeType() != MIXED) {
				// Collapse neighbors:
				if (nodes[i1].edges.size() >= 2) {
					UInt firstNeighbor = edges[nodes[i1].edges[0]].getOther(i1);
					newNodeIDs.insert(std::pair<UInt, UInt>(firstNeighbor, firstNeighbor));
					for (UInt i2 = 1; i2 < nodes[i1].edges.size(); i2++) {
						UInt neighbor = edges[nodes[i1].edges[i2]].getOther(i1);
						newNodeIDs.insert(std::pair<UInt, UInt>(neighbor, firstNeighbor));
					}
				}
			}
		}

		compactify(newGraph, newNodeIDs);

		return newGraph;
	}


	//! ....
	ConvexConcaveGraph* makePure() {
		ConvexConcaveGraph* newGraph = new ConvexConcaveGraph;

		//std::cerr << "Make pure" << std::endl;

		UInt numNodes = getNumNodes();

		std::map<UInt, UInt> newNodeIDs;
		for (UInt i1 = 0; i1 < numNodes; i1++) {

			// Check if the node has already been collapsed:
			if (newNodeIDs.find(i1) != newNodeIDs.end()) {
				//std::cerr << "skip: " << i1 << std::endl;
				continue;
			}

			ConvexConcaveNode n = nodes[i1];
			UInt n_valance = (UInt)n.edges.size();
			int counter = 0;
			// Check if node can be made pure.
			for (UInt i2 = 0; i2 < n_valance; i2++) {
				if (edges[n.edges[i2]].getEdgeType() == CONVEX) counter++;
				else counter--;
			}

			if (iabs(counter) == n_valance-2) { // Node contains only one different color.

				EdgeTypeEnum typeToCollapse;
				if (counter > 0) typeToCollapse = CONCAVE;
				else typeToCollapse = CONVEX;

				// Find the outsider ;)
				UInt outsiderIndex;
				for (outsiderIndex = 0; outsiderIndex < n_valance; outsiderIndex++) if (edges[n.edges[outsiderIndex]].getEdgeType() == typeToCollapse) break;

				UInt otherNode = edges[n.edges[outsiderIndex]].getOther(i1);
				ConvexConcaveNode n2 = nodes[otherNode];
				UInt n2_valance = (UInt)n2.edges.size();
				// Check if 'otherNode' does also contain only one different color.
				int counter2 = 0;
				for (UInt i2 = 0; i2 < n2_valance; i2++) {
					if (edges[n2.edges[i2]].getEdgeType() == CONVEX) counter2++;
					else counter2--;
				}
				if (iabs(counter2) == n2_valance-2) { // Node contains only one different color.

					EdgeTypeEnum typeToCollapse2;
					if (counter2 > 0) typeToCollapse2 = CONCAVE;
					else typeToCollapse2 = CONVEX;

					if (typeToCollapse == typeToCollapse2) { // Collapse.
						UInt larger = (i1 > otherNode) ? i1 : otherNode;
						UInt smaller = (i1 < otherNode) ? i1 : otherNode;
						newNodeIDs.insert(std::pair<UInt, UInt>(smaller, smaller));
						newNodeIDs.insert(std::pair<UInt, UInt>(larger, smaller));
						//std::cerr << "Collapse " << i1 << " " << otherNode << std::endl;
					}
				}

			}

		}

		//std::cerr << "Compactify" << std::endl;

		compactify(newGraph, newNodeIDs);

		return newGraph;
	}

	//! Builds the graph from the SimpleMesh 'sm'.
	void buildGraphFromSimpleMesh(SimpleMesh* sm) {
		deleteGraph();

		double offset = (sm->max-sm->min).length() / 200;
		
		// build Triangle Lookup table and insert nodes:
		std::map<Triangle*, UInt> triangleLookup;
		nodes.reserve(sm->tList.size());
		for (UInt i1 = 0; i1 < sm->tList.size(); i1++) {
			Triangle* t = sm->tList[i1];
			triangleLookup.insert(std::pair<Triangle*, UInt>(t, i1));
			vec3d center = t->getCenter();
			vec3d normal = t->getNormal();
			center += normal*offset;
			ConvexConcaveNode newNode(this, new BooleanTreeNode_Plane<vec3d>(center, normal));
			nodes.push_back(newNode);
		}

		for (UInt i1 = 0; i1 < sm->eList.size(); i1++) {
			EdgeTypeEnum eType = (sm->eList[i1]->isConvex()) ? CONVEX : CONCAVE;
			UInt v0 = triangleLookup[sm->eList[i1]->tList[0]];
			UInt v1 = triangleLookup[sm->eList[i1]->tList[1]];

			ConvexConcaveEdge e(this, eType, v0, v1);
			insertEdge(e);
		}
	}

	//! Deletes the graph.
	void deleteGraph() {
		nodes.clear();
		edges.clear();
	}

	//! Returns the number of nodes in the graph.
	inline UInt getNumNodes() {
		return (UInt)nodes.size();
	}

	//! Returns the number of edges in the graph.
	inline UInt getNumEdges() {
		return (UInt)edges.size();
	}

	//! Prints the number of nodes and edges.
	inline void printStats() {
		std::cerr << "ConvexConcaveGraph with: " << std::endl << (UInt)nodes.size() << " nodes and " << (UInt)edges.size() << " edges" << std::endl;
	}

	inline void insertEdge(const ConvexConcaveEdge& e) {
		// check if edge exists:
		if (nodes[e.n0].edgeExists(e)) {
			//std::cerr << "Edge exists" << std::endl;
		} else {
			UInt edgeID = (UInt)edges.size();
			edges.push_back(e);
			nodes[e.n0].addEdge(edgeID);
			nodes[e.n1].addEdge(edgeID);
		}
	}

	//! Returns the index of the edge 'n0'-'n1'
	int getEdge(UInt n0, UInt n1) {
		for (UInt i1 = 0; i1 < nodes[n0].edges.size(); i1++) {
			ConvexConcaveEdge e = edges[nodes[n0].edges[i1]];
			if ((e.n0 == n0 && e.n1 == n1) || (e.n1 == n0 && e.n0 == n1))
				return i1;
		}
		//std::cerr << "ConvexConcaveGraph::getEdge(...): Error, Edge not found, EXIT" << std::endl;
		//std::cerr << "You requested edge: " << n0 << " - " << n1 << std::endl;
		//std::cerr << "I have edges: " << std::endl;
		//for (UInt i1 = 0; i1 < edges.size(); i1++) {
		//	edges[i1].print();
		//}
		//exit(EXIT_FAILURE);
		
		return -1;
	}

	std::vector<ConvexConcaveNode> nodes;
	std::vector<ConvexConcaveEdge> edges;

private:

	//! Collects all purely convex/concave nodes connected to 'nodeID'.
	std::list<UInt> getPureNodesConnected(UInt nodeID, EdgeTypeEnum edgeType) {

		std::list<UInt> connectedNodes;

		std::queue<UInt> toProcess;
		toProcess.push(nodeID);

		while (!toProcess.empty()) {
			UInt currentNode = toProcess.front(); toProcess.pop();
			connectedNodes.push_back(currentNode);

			for (UInt i1 = 0; i1 < nodes[currentNode].edges.size(); i1++) {
				UInt nextNode = edges[nodes[currentNode].edges[i1]].getOther(currentNode);
				if (nodes[nextNode].getNodeType() == edgeType && nodes[nextNode].flag != IS_VISITED) {
					nodes[nextNode].flag = IS_VISITED;
					toProcess.push(nextNode);
				}
			}

		}

		// remove first element:
		//connectedNodes.pop_front();

		return connectedNodes;

	}

	void compactify(ConvexConcaveGraph* newGraph, std::map<UInt, UInt> newNodeIDs) {

		for (std::map<UInt, UInt>::iterator it = newNodeIDs.begin(); it != newNodeIDs.end(); ++it) {
			if (it->second > it->first)	std::cerr << it->first << " -> " << it->second << std::endl;
		}

		UInt numNodes = getNumNodes();

		std::map<UInt, UInt> newNodeIDsAfterCollapse;
		// add to remaining nodes new graph:
		UInt numNodesLeft = 0;
		for (UInt i1 = 0; i1 < numNodes; i1++) {
			if (newNodeIDs.find(i1) == newNodeIDs.end()) {
				newNodeIDsAfterCollapse.insert(std::pair<UInt, UInt>(i1, numNodesLeft));
				numNodesLeft++;
				newGraph->nodes.push_back(ConvexConcaveNode(newGraph, nodes[i1].func));
			} else {
				UInt i1_collapsed = newNodeIDs[i1];
				if (i1_collapsed == i1) {
					newNodeIDsAfterCollapse.insert(std::pair<UInt, UInt>(i1, numNodesLeft));
					numNodesLeft++;
					bool is_convex = (nodes[i1].getNodeType() == CONVEX);
					newGraph->nodes.push_back(ConvexConcaveNode(newGraph, new BooleanTreeNode_Group<vec3d>(is_convex)));
					((BooleanTreeNode_Group<vec3d>*)newGraph->nodes[numNodesLeft-1].func)->addChild(nodes[i1].func);
				} else {
					// increase size:
					if (newNodeIDsAfterCollapse.find(i1_collapsed) == newNodeIDsAfterCollapse.end()) {
						std::cerr << "Warning! " << i1 << " -> " << i1_collapsed << std::endl;
						std::cerr << newNodeIDs[i1_collapsed] << std::endl;
					}
					UInt i1_new = newNodeIDsAfterCollapse[i1_collapsed];
					// Add child
					BooleanTreeNode_Group<vec3d>* tmp_func = (BooleanTreeNode_Group<vec3d>*)newGraph->nodes[i1_new].func;
					tmp_func->addChild(nodes[i1].func);
					// set convex:
					int collapsedEdge = getEdge(i1, newNodeIDs[i1]);
					if (collapsedEdge != -1) {
						bool is_convex = (edges[collapsedEdge].getEdgeType() == CONVEX);
						tmp_func->setConvex(is_convex);
					}
				}
			}
		}

		// add remaining edges to new graph:
		for (UInt i1 = 0; i1 < edges.size(); i1++) {
			UInt v0 = edges[i1].n0;
			UInt v1 = edges[i1].n1;

			if (newNodeIDs.find(v0) == newNodeIDs.end() && newNodeIDs.find(v1) == newNodeIDs.end()) {
				// uncritical, both nodes of this edge have not been changed.
				v0 = newNodeIDsAfterCollapse[v0];
				v1 = newNodeIDsAfterCollapse[v1];
				newGraph->insertEdge(ConvexConcaveEdge(newGraph, edges[i1].getEdgeType(), v0, v1));
			} else {
				UInt v0_ = v0;
				UInt v1_ = v1;
				if (newNodeIDs.find(v0_) != newNodeIDs.end()) v0_ = newNodeIDs[v0_];
				if (newNodeIDs.find(v1_) != newNodeIDs.end()) v1_ = newNodeIDs[v1_];

				//if (v0_ == 0 || v1_ == 0) {
				//	std::cerr << "<" << v0 << " " << v1 << ">" << std::endl;
				//	std::cerr << "|" << v0_ << " " << v1_ << "|" << std::endl;
				//}

				if (v0_ == v1_) { // edge was collapsed, do nothing
					//std::cerr << "Collapsed edge" << std::endl;
				}
				else { // one is new, the other is old.
					//std::cerr << "-------------" << std::endl;
					//std::cerr << "|" << v0 << " " << v1 << "|  ";

					if (newNodeIDsAfterCollapse.find(v0_) == newNodeIDsAfterCollapse.end() ||
						newNodeIDsAfterCollapse.find(v1_) == newNodeIDsAfterCollapse.end()) {
							std::cerr << "Error, not found!" << std::endl;
						}

					v0_ = newNodeIDsAfterCollapse[v0_];
					v1_ = newNodeIDsAfterCollapse[v1_];

					//if (v0_ == 0 || v1_ == 0) {
					//	std::cerr << "<" << v0 << " " << v1 << ">  <" << v0_ << " " << v1_ << ">" << std::endl;
					//}

					newGraph->insertEdge(ConvexConcaveEdge(newGraph, edges[i1].getEdgeType(), v0_, v1_));
				}
			}
		}

		// Print all edges:
		//for (UInt i1 = 0; i1 < newGraph->edges.size(); i1++) {
		//	std::cerr << newGraph->edges[i1].n0 << " " << newGraph->edges[i1].n1 << "\t";
		//}

	}

};


EdgeTypeEnum ConvexConcaveNode::getNodeType() {
	if (edges.size() < 1) return MIXED;
	EdgeTypeEnum type = parentGraph->edges[edges[0]].getEdgeType();

	for (UInt i1 = 0; i1 < (UInt)edges.size(); i1++) {
		if (type != parentGraph->edges[edges[i1]].getEdgeType()) return MIXED;
	}

    return type;
}


//! Returns wether the node contains the edge 'e'.
bool ConvexConcaveNode::edgeExists(ConvexConcaveEdge e) {
	for (UInt i1 = 0; i1 < edges.size(); i1++) {
		if (e == parentGraph->edges[edges[i1]]) return true;
	}
	return false;
}

#endif
