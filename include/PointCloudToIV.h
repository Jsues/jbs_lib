#ifndef POINTCLOUD_TO_IV_H
#define POINTCLOUD_TO_IV_H

#include "PointCloud.h"
//#include "kd_TreePointCloud.h"
#include "hsv2rgb.h"

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoIndexedLineSet.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoSelection.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoNormal.h>
#include <Inventor/nodes/SoTransparencyType.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/SbColor.h>



//****************************************************************************************************

template <class T>
SoSeparator* pointcloudToIV(PointCloud<T>* pc, vec3f color = vec3f(1,1,1), int pointSize = 2) {
	SoSeparator* iv_pc = new SoSeparator();
	int numOfPoints = pc->getNumPts();

	if (numOfPoints == 0) return iv_pc;

	const T* pts = pc->getPoints();
	SbVec3f* points = new SbVec3f[numOfPoints];
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		for (UInt i2 = 0; i2 < T::dim && i2 < 3; i2++) points[i1][i2] = (float)pts[i1][i2];
		for (UInt i2 = T::dim; i2 < 3; i2++) points[i1][i2] = 0;
	}


	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.set1Value(0, color.x, color.y, color.z);
	iv_pc->addChild(mat);


	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numOfPoints, points);
	iv_pc->addChild(coord3);
	SoDrawStyle* ds = new SoDrawStyle;
	ds->pointSize = (float)pointSize;
	iv_pc->addChild(ds);
	SoPointSet* pointset = new SoPointSet;
	iv_pc->addChild(pointset);

	delete[] points;

	return iv_pc;
}

//****************************************************************************************************

template <class T>
SoSeparator* pointcloudToIV_2D(PointCloud<T>* pc, vec3f color = vec3f(0.7f, 0.7f, 0.7f)) {

	SoSeparator* iv_pc = new SoSeparator();
	int numOfPoints = pc->getNumPts();

	T* pts = pc->getPoints();
	SbVec3f* points = new SbVec3f[numOfPoints];
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		points[i1][0] = (float)pts[i1][0];
		points[i1][1] = (float)pts[i1][1];
		points[i1][2] = 0;
	}

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.set1Value(0, color.x, color.y, color.z);
	iv_pc->addChild(mat);

	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numOfPoints, points);
	iv_pc->addChild(coord3);
	SoDrawStyle* ds = new SoDrawStyle;
	ds->pointSize = 2;
	iv_pc->addChild(ds);
	SoPointSet* pointset = new SoPointSet;
	iv_pc->addChild(pointset);

	delete[] points;
	return iv_pc;
}

//****************************************************************************************************

template <class T>
SoSeparator* pointcloudToIVcolor_2D(PointCloud<T>* pc, float hsv_color_h) {

	vec3f rgb_col = JBSlib::hsv2rgb(hsv_color_h, 1.0f, 1.0f);

	return pointcloudToIV_2D(pc, rgb_col);
}

//****************************************************************************************************

SoSeparator* pointcloudToIV(PointCloud<vec3f>* pc, vec3f color, int pointSize = 2) {
	SoSeparator* iv_pc = new SoSeparator();
	int numOfPoints = pc->getNumPts();

	if (numOfPoints == 0) return iv_pc;

	const vec3f* pts = pc->getPoints();
	SbVec3f* points = new SbVec3f[numOfPoints];
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		points[i1] = pts[i1].array;
	}


	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.set1Value(0, color.x, color.y, color.z);
	iv_pc->addChild(mat);


	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numOfPoints, points);
	iv_pc->addChild(coord3);
	SoDrawStyle* ds = new SoDrawStyle;
	ds->pointSize = (float)pointSize;
	iv_pc->addChild(ds);
	SoPointSet* pointset = new SoPointSet;
	iv_pc->addChild(pointset);

	delete[] points;

	return iv_pc;
}


//****************************************************************************************************
SoSeparator* pointcloudToIV(PointCloud<vec3d>* pc) {
	return pointcloudToIV(pc, vec3f(0,0,0));
}
//****************************************************************************************************
SoSeparator* pointcloudToIV(PointCloud<vec3f>* pc) {
	return pointcloudToIV(pc, vec3f(0,0,0));
}

//****************************************************************************************************

SoSeparator* pointcloudNormalsToIVuseNormalsAsColor(PointCloudNormals<vec3f>* pc, float pointsize = 3) {

	SoSeparator* iv_pc = new SoSeparator();
	int numOfPoints = pc->getNumPts();

	vec3f* pts = pc->getPoints();
	SbVec3f* points = new SbVec3f[numOfPoints];
	for (int i1 = 0; i1 < numOfPoints; i1++)
		points[i1] = pts[i1].array;

	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numOfPoints, points);
	iv_pc->addChild(coord3);


	SoMaterial* mat = new SoMaterial;
	for (int i1 = 0; i1 < numOfPoints; i1++)
		mat->diffuseColor.set1Value(i1, pc->m_normals[i1].x, pc->m_normals[i1].y, pc->m_normals[i1].z);
	iv_pc->addChild(mat);

	SoMaterialBinding* mat_bind = new SoMaterialBinding;
	mat_bind->value = SoMaterialBinding::PER_PART;
	iv_pc->addChild(mat_bind);

	SoDrawStyle* ds = new SoDrawStyle;
	ds->pointSize = pointsize;
	iv_pc->addChild(ds);

	SoPointSet* pointset = new SoPointSet;
	iv_pc->addChild(pointset);

	delete[] points;

	return iv_pc;
}

//****************************************************************************************************


//****************************************************************************************************
/*
SoSeparator* pointcloudWithCurvatureToIV(kdTreePointCloud<vec3f>* pc) {
	SoSeparator* iv_pc = new SoSeparator();
	int numOfPoints = pc->getNumPts();

	vec3f* pts = pc->getPoints();
	SbVec3f* points = new SbVec3f[numOfPoints];
	for (int i1 = 0; i1 < numOfPoints; i1++)
		points[i1] = pts[i1].array;

	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numOfPoints, points);
	iv_pc->addChild(coord3);


	SoMaterial* mat = new SoMaterial;
	for (int i1 = 0; i1 < numOfPoints; i1++)
		mat->diffuseColor.set1Value(i1, pc->color[i1], 1-pc->color[i1], 1-pc->color[i1]);
	iv_pc->addChild(mat);

	SoMaterialBinding* mat_bind = new SoMaterialBinding;
	mat_bind->value = SoMaterialBinding::PER_PART;
	iv_pc->addChild(mat_bind);

	SoPointSet* pointset = new SoPointSet;
	iv_pc->addChild(pointset);

	delete[] points;

	return iv_pc;
}
*/
//****************************************************************************************************

template <class T>
SoSeparator* pointCloudNormalsToIV_color(PointCloudNormals<T>* pc, bool reverseNormals = false, int pointSize = 2, SoCoordinate3* points_ptr = NULL, vec3f color = vec3f(0.7f, 0.7f, 0.7f), SoMaterial* mat_ext = NULL) {
	SoSeparator* iv_pc = new SoSeparator();
	int numOfPoints = pc->getNumPts();

	SoMaterial* mat;
	if (mat_ext == NULL) {
		mat = new SoMaterial;
		mat->diffuseColor.set1Value(0, color.x, color.y, color.z);
	} else {
		//std::cerr << "Using extern material" << std::endl;
		mat = mat_ext;
	}
	iv_pc->addChild(mat);

	const T* pts = pc->getPoints();
	SbVec3f* points = new SbVec3f[numOfPoints];
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		for (UInt xx = 0; xx < 3; xx++)
			points[i1][xx] = (float)pts[i1][xx];
	}

	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numOfPoints, points);
	iv_pc->addChild(coord3);

	points_ptr = coord3;

	SoNormal* normals = new SoNormal;
	normals->vector.setNum(numOfPoints);
	if (!reverseNormals)
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			normals->vector.set1Value(i1, SbVec3f((float)pc->m_normals[i1][0], (float)pc->m_normals[i1][1], (float)pc->m_normals[i1][2]));
		}
	else
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			normals->vector.set1Value(i1, SbVec3f((float)-pc->m_normals[i1][0], (float)-pc->m_normals[i1][1], (float)-pc->m_normals[i1][2]));
		}
	iv_pc->addChild(normals);

	SoDrawStyle* ds = new SoDrawStyle;
	ds->pointSize = (float)pointSize;
	iv_pc->addChild(ds);

	SoShapeHints* sh = new SoShapeHints;
	sh->shapeType = SoShapeHints::SOLID;
	sh->vertexOrdering = SoShapeHints::CLOCKWISE;
	iv_pc->addChild(sh);

	SoPointSet* pointset = new SoPointSet;
	iv_pc->addChild(pointset);

	delete[] points;

	return iv_pc;
}

//****************************************************************************************************


SoSeparator* pointCloudNormalsToIV(PointCloudNormals<vec3f>* pc, bool reverseNormals = false, int pointSize = 2, SoCoordinate3* points_ptr = NULL) {
	return pointCloudNormalsToIV_color(pc, reverseNormals, pointSize, points_ptr, vec3f(0.7f, 0.7f, 0.7f));
}


//****************************************************************************************************


template <class T>
SoSeparator* pointCloudNormalsToIVdisplayNormals(PointCloudNormals<T>* pc, bool reverseNormals = false, float col = 0.0f, float scale2 = 1.0f) {

	SbColor color;

	if (col == 0.0f)
		color = SbVec3f(0.7f, 0.75f, 0.75f);
	else {
		color.setHSVValue(col, 1.0f, 1.0f);
	}

	SoSeparator* iv_pc = new SoSeparator();

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.setValue(color);
	iv_pc->addChild(mat);

	int numOfPoints = pc->getNumPts();

	T* pts = pc->getPoints();
	SbVec3f* points = new SbVec3f[2*numOfPoints];
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		for (UInt xx = 0; xx < T::dim; xx++)
			points[i1][xx] = (float)pts[i1][xx];
	}

	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numOfPoints, points);
	iv_pc->addChild(coord3);

	//SoNormal* normals = new SoNormal;
	//normals->vector.setNum(numOfPoints);
	//if (!reverseNormals)
	//	for (int i1 = 0; i1 < numOfPoints; i1++) {
	//		normals->vector.set1Value(i1, SbVec3f(pc->m_normals[i1][0], pc->m_normals[i1][1], pc->m_normals[i1][2]));
	//	}
	//else
	//	for (int i1 = 0; i1 < numOfPoints; i1++) {
	//		normals->vector.set1Value(i1, SbVec3f(-pc->m_normals[i1][0], -pc->m_normals[i1][1], -pc->m_normals[i1][2]));
	//	}
	//iv_pc->addChild(normals);

	SoDrawStyle* ds = new SoDrawStyle;
	ds->pointSize = 3;
	iv_pc->addChild(ds);

	SoShapeHints* sh = new SoShapeHints;
	sh->shapeType = SoShapeHints::SOLID;
	//sh->faceType = SoShapeHints::UNKNOWN_FACE_TYPE;
	sh->vertexOrdering = SoShapeHints::CLOCKWISE;
	iv_pc->addChild(sh);

	SoPointSet* pointset = new SoPointSet;
	iv_pc->addChild(pointset);

	float scale = (float)(pc->getMax()-pc->getMin()).length() / 100.0f;
	scale *= scale2;
	// Show normals;
	SoSeparator* norms = new SoSeparator;
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		T normal = pc->m_normals[i1];
		//normal.normalize();
		normal *= scale;
		if (reverseNormals) normal *= -1.0f;
		normal += pts[i1];
		for (UInt xx = 0; xx < T::dim; xx++) points[numOfPoints+i1][xx] = (float)normal[xx];
	}
	SoCoordinate3* coord4 = new SoCoordinate3;
	coord4->point.setValues(0, 2*numOfPoints, points);
	norms->addChild(coord4);
	SoIndexedLineSet* ls = new SoIndexedLineSet;
	ls->coordIndex.setNum(3*numOfPoints);
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		int32_t indices[] = {i1, i1+numOfPoints, -1 };
		ls->coordIndex.setValues(i1*3, 3, indices);
	}
	norms->addChild(ls);

	iv_pc->addChild(norms);

	delete[] points;

	return iv_pc;
}

//****************************************************************************************************

/*
SoSeparator* pointcloudWithNormalsToIV_color(kdTreePointCloud<vec3f>* pc, bool reverseNormals = false, vec3f color = vec3f(0.7f, 0.7f, 0.7f)) {
	SoSeparator* iv_pc = new SoSeparator();
	int numOfPoints = pc->getNumPts();

	SoMaterial* mat = new SoMaterial;
	mat->diffuseColor.set1Value(0, color.x, color.y, color.z);
	iv_pc->addChild(mat);

	vec3f* pts = pc->getPoints();
	SbVec3f* points = new SbVec3f[numOfPoints];
	for (int i1 = 0; i1 < numOfPoints; i1++) {
		points[i1] = pts[i1].array;
	}

	SoCoordinate3* coord3 = new SoCoordinate3;
	coord3->point.setValues(0, numOfPoints, points);
	iv_pc->addChild(coord3);

	SoNormal* normals = new SoNormal;
	normals->vector.setNum(numOfPoints);
	if (!reverseNormals)
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			normals->vector.set1Value(i1, SbVec3f(pc->normals[i1][0], pc->normals[i1][1], pc->normals[i1][2]));
		}
	else
		for (int i1 = 0; i1 < numOfPoints; i1++) {
			normals->vector.set1Value(i1, SbVec3f(-pc->normals[i1][0], -pc->normals[i1][1], -pc->normals[i1][2]));
		}
	iv_pc->addChild(normals);

	SoDrawStyle* ds = new SoDrawStyle;
	ds->pointSize = 3;
	iv_pc->addChild(ds);

	SoShapeHints* sh = new SoShapeHints;
	sh->shapeType = SoShapeHints::SOLID;
	//sh->faceType = SoShapeHints::UNKNOWN_FACE_TYPE;
	sh->vertexOrdering = SoShapeHints::CLOCKWISE;
	iv_pc->addChild(sh);

	SoPointSet* pointset = new SoPointSet;
	iv_pc->addChild(pointset);

	delete[] points;

	return iv_pc;
}
*/

//****************************************************************************************************

/*
SoSeparator* pointcloudWithNormalsToIV(kdTreePointCloud<vec3f>* pc, bool reverseNormals = false) {
	return pointcloudWithNormalsToIV_color(pc, reverseNormals, vec3f(0.7f, 0.7f, 0.7f));
}
*/

//****************************************************************************************************


#endif
