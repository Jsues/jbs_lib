#include "Matrix2D.h"


//******************************************************************************************


void Matrix2D::transpose() {
	values_calculated = false;
	vectors_calculated = false;

	float b_ = b;
	float c_ = c;

	b = c_;
	c = b_;
}


//******************************************************************************************


float Matrix2D::evalVector(vec2f v) {
	vec2f trans(a*v[0]+b*v[1], c*v[0]+d*v[1]);
	return trans|v;
}


//******************************************************************************************


void Matrix2D::add(Matrix2D &m2) {
  a += m2.a;
  b += m2.b;
  c += m2.c;
  d += m2.d;
}


//******************************************************************************************


float Matrix2D::determinant() {
  return a*d-b*c;
}


//******************************************************************************************


void Matrix2D::calc_e_vectors() {
  if (!values_calculated)
    calc_e_values();

  ev0[0] = -b;
  ev0[1] = a-lambda_0;
  ev0.normalize();
  //ev1.print();

  ev1[0] = -b;
  ev1[1] = a-lambda_1;
  ev1.normalize();

  vectors_calculated = true;
}


//******************************************************************************************


void Matrix2D::calc_e_values() {
  float l1 = 0.5f*(+a+d+(float)sqrt((a+d)*(a+d)-4*(a*d-c*b)));
  float l2 = 0.5f*(+a+d-(float)sqrt((a+d)*(a+d)-4*(a*d-c*b)));

  //  std::cerr << "Lambda_1: " << l1 << "  Lambda_2: " << l2 << std::endl;

  if (fabs(l1) > fabs(l2)) {
    lambda_0 = l1;
    lambda_1 = l2;
  } else {
    lambda_0 = l2;
    lambda_1 = l1;
  }
  values_calculated = true;
}


//******************************************************************************************


void Matrix2D::invert_ignore_scale() {
  float new_a = d;
  float new_b = -b;
  float new_c = -c;
  float new_d = a;

  a = new_a;
  b = new_b;
  c = new_c;
  d = new_d;
}


//******************************************************************************************


void Matrix2D::invert() {

  float det = determinant();

  float new_a = d/det;
  float new_b = -b/det;
  float new_c = -c/det;
  float new_d = a/det;

  a = new_a;
  b = new_b;
  c = new_c;
  d = new_d;
}


//******************************************************************************************


void Matrix2D::mult(Matrix2D &m2) {
  float new_a, new_b, new_c, new_d;
  
  new_a = a * m2.a + b * m2.c;
  new_b = a * m2.b + b * m2.d;
  new_c = c * m2.a + d * m2.c;
  new_d = c * m2.b + d * m2.d;
    
  a = new_a;
  b = new_b;
  c = new_c;
  d = new_d;
}


//******************************************************************************************