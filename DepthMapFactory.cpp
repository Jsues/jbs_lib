
#include "DepthMapFactory.h"
#include "RGB_ImageSDL.h"
#include "MatrixnD.h"
#include "PolylineToIV.h"

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoQuadMesh.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoTexture2.h>

// ****************************************************************************************

//! 'viewAt' and sensor position 'pos' are assumed to be in meters.
SoSeparator* buildIVFrustum(vec3f pos, S_Sensordimensions* sensor, vec3f viewAt, RGBImageSDL* img1) {

	SoSeparator* sep = new SoSeparator;

	vec3f view_dir = viewAt-pos;
	float distance = view_dir.length();
	// All computations in mm.
	distance *= 1000.0f;
	viewAt *= 1000.0f;
	pos *= 1000.0f;

	view_dir.normalize();
	vec3f up_vector(0,1,0);
	vec3f left = view_dir^up_vector;
	left.normalize();

	PolyLine<vec3f> frustum1;
	frustum1.insertVertex(pos);

	//float d = sqrt(sensor->w*sensor->w+sensor->h*sensor->h);
	//float alpha_w = atan(d/(2*distance));
	//std::cerr << "d: " << d << std::endl;
	//std::cerr << "Blickwinkel: " << (alpha_w*180)/3.14 << std::endl;
	float sw = sensor->w*500;
	float sh = sensor->h*500;

	vec3f sensor_00 = viewAt + left*-sw + up_vector*-sh;
	vec3f sensor_01 = viewAt + left*-sw + up_vector*sh;
	vec3f sensor_10 = viewAt + left*sw + up_vector*-sh;
	vec3f sensor_11 = viewAt + left*sw + up_vector*sh;

	frustum1.insertVertex(sensor_00);
	frustum1.insertVertex(sensor_01);
	frustum1.insertVertex(sensor_11);
	frustum1.insertVertex(sensor_10);

	frustum1.insertLine(0,1); frustum1.insertLine(0,2); frustum1.insertLine(0,3); frustum1.insertLine(0,4);
	frustum1.insertLine(1,2); frustum1.insertLine(2,3); frustum1.insertLine(3,4); frustum1.insertLine(4,1);

	sep->addChild(PolyLineToIV(&frustum1, 0.2f, 2));

	vec3f iplane_center = pos+view_dir*distance/10.0f;

	vec3f iplane_00 = iplane_center + left*-sw/10.0f + up_vector*-sh/10.0f;
	vec3f iplane_01 = iplane_center + left*-sw/10.0f + up_vector*sh/10.0f;
	vec3f iplane_10 = iplane_center + left*sw/10.0f + up_vector*-sh/10.0f;
	vec3f iplane_11 = iplane_center + left*sw/10.0f + up_vector*sh/10.0f;

	SoTexture2 *myTexture = new SoTexture2();
	myTexture->image.setValue(SbVec2s(img1->getDimX(), img1->getDimY()), 3, (const unsigned char*)img1->m_data);
	sep->addChild(myTexture);

    SoCoordinate3* coords = new SoCoordinate3;
	coords->point.set1Value(0, iplane_00);
	coords->point.set1Value(1, iplane_01);
	coords->point.set1Value(2, iplane_10);
	coords->point.set1Value(3, iplane_11);
	sep->addChild(coords);
    SoQuadMesh* mesh = new SoQuadMesh;
    mesh->verticesPerRow = 2;
    mesh->verticesPerColumn = 2;
	sep->addChild(mesh);


	return sep;


}

// ****************************************************************************************

//void DrawLine(RGBImageSDL* img1, vec2i start, vec2i end) {
//	Line l(start, end);
//	std::cerr << "Line: " << start << " -> " << end << std::endl;
//	for (; !l.is_end(); l++) {
//		img1->setPixel(l.m_current.x, l.m_current.y, rgb_triple(255,0,0));
//
//		l.m_current.print();
//	}
//}

// ****************************************************************************************

DepthMapFactory::DepthMapFactory(char* filename1, char* filename2) {
	img1 = new RGBImageSDL;
	img2 = new RGBImageSDL;

	dim_reduce = NULL;
	m_sensor_info = NULL;
	m_cam_setup = NULL;

	img1->readImage(filename1);
	img2->readImage(filename2);

	//exit(1);
}

// ****************************************************************************************

DepthMapFactory::~DepthMapFactory() {
	delete img1;
	delete img2;

	if (dim_reduce != NULL) delete dim_reduce;
}

// ****************************************************************************************

void DepthMapFactory::setCameraInformations(S_Sensordimensions* sensor_info, S_CameraSetup* cam_setup) {
	m_sensor_info = sensor_info;
	m_cam_setup = cam_setup;
}

// ****************************************************************************************

void DepthMapFactory::init() {

	UInt dx = img2->getDimX();
	UInt dy = img2->getDimY();

	// The covariance Matrix
	SquareMatrixND<featureVec> CV;

	// Determine average feature vector
	featureVec avg(0);
	for (UInt x = BOUNDARY_SKIP; x < (dx-BOUNDARY_SKIP); x++) {
		for (UInt y = BOUNDARY_SKIP; y < (dy-BOUNDARY_SKIP); y++) {
			featureVec tmp = getFeatureVector(x,y);
			avg += tmp;
		}
	}

	UInt numPixels = (dx-2*BOUNDARY_SKIP)*(dy-2*BOUNDARY_SKIP);
	avg /= (float)numPixels;

	// Build Covariance Matrix
	for (UInt x = BOUNDARY_SKIP; x < (dx-BOUNDARY_SKIP); x++) {
		for (UInt y = BOUNDARY_SKIP; y < (dy-BOUNDARY_SKIP); y++) {
			featureVec tmp = getFeatureVector(x,y);
			tmp -= avg;
			CV.addFromTensorProduct_NO_SYMMETRIZE(tmp);
		}
	}

	CV.symmetrize();
	SquareMatrixND<featureVec> ESystem;
	CV.calcEValuesAndVectorsCORRECT(ESystem);

	// prepare dimensionality reduction matrix
	dim_reduce = new DoRo::Matrix<float>(FEATURE_VECTOR_SIZE, REDUCED_FEATURE_VECTOR_SIZE);
	for (UInt i1 = 0; i1 < REDUCED_FEATURE_VECTOR_SIZE; i1++) {
		for (UInt i2 = 0; i2 < FEATURE_VECTOR_SIZE; i2++) {
			(*dim_reduce)(i2,i1) = ESystem.getColI(i1)[i2];
		}
	}

	//std::cerr << "Reduction matrix:" << std::endl;
	//dim_reduce->print();

	// Build kd-Tree:
	for (UInt x = BOUNDARY_SKIP; x < (dx-BOUNDARY_SKIP); x++) {
		for (UInt y = BOUNDARY_SKIP; y < (dy-BOUNDARY_SKIP); y++) {
			featureVec tmp = getFeatureVector(x,y);
			reducedFeatureVec tmp2;
			dim_reduce->multiply(tmp.array, tmp2.array);
		}
	}


}

// ****************************************************************************************

void DepthMapFactory::computeDepthForAllPixels() {

	for (UInt x = 0; x < img1->getDimX(); x++) {
		for (UInt y = 0; y < img1->getDimY(); y++) {
			vec2ui pix_pos(x,y);
			Line projectedLine = getProjectedLine(pix_pos);
			vec2ui bestFitPixel = getBestFitPixel(projectedLine, pix_pos);
			float depth = getDepthForPixelPair(pix_pos, bestFitPixel);
		}
	}

}

// ****************************************************************************************

Line DepthMapFactory::getProjectedLine(vec2ui pix_pos) {
	Line l(vec2i(0), vec2i(0));

	return l;
}

// ****************************************************************************************

vec2ui DepthMapFactory::getBestFitPixel(Line l, vec2ui pix_pos) {

	vec2ui ret;

	return ret;
}

// ****************************************************************************************

float DepthMapFactory::getDepthForPixelPair(vec2ui pox_pos_img1, vec2ui pix_pos_img2) {
	float depth = 0;

	return depth;
}

// ****************************************************************************************

featureVec DepthMapFactory::getFeatureVector(UInt x, UInt y) {

	featureVec ret;

	int cnt = 0;
	for (UInt xx = x-OFFSET; xx <= x+OFFSET; xx++) {
		for (UInt yy = y-OFFSET; yy <= y+OFFSET; yy++) {
			rgb_triple col = img2->getPixel(xx,yy);
			ret[cnt*3+0] = (float)col.x/255.0f;
			ret[cnt*3+1] = (float)col.y/255.0f;
			ret[cnt*3+2] = (float)col.z/255.0f;
			cnt++;
		}
	}

	return ret;
}

// ****************************************************************************************

SoSeparator* DepthMapFactory::getIV_scene() {

	SoSeparator* sep = new SoSeparator;

	vec3f second_cam_pos = m_cam_setup->second_camera_pos;
	vec3f viewAt = vec3f(second_cam_pos.x/2.0f,second_cam_pos.y/2.0f,m_cam_setup->distance_to_fp);
	sep->addChild(buildIVFrustum(vec3f(0,0,0), m_sensor_info, viewAt, img1));
	sep->addChild(buildIVFrustum(second_cam_pos, m_sensor_info, viewAt, img2));


	return sep;
}

// ****************************************************************************************
