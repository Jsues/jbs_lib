#include "CLineHull.h"


#include <fstream>
#include <iostream>


std::vector<Quadruple> buildConvexHullCL(std::vector<vec4d> pts) {

	char* filenamein =  "in.pts";
	char* filenameout = "out.quad";
	char* systemcall = "hull <in.pts >out.quad";

	std::ofstream outfile(filenamein);
	// Write pts to a file
	for (UInt i1 = 0; i1 < pts.size(); i1++) {
		outfile << pts[i1].x << " " << pts[i1].y << " " << pts[i1].z << " " << pts[i1].w << std::endl;
	}
	outfile.close();

	int Errcode = system(systemcall);

	if (Errcode == 1) {
		std::cerr << systemcall << std::endl;
		std::cerr << "errcode: " << Errcode << std::endl;
		std::cerr << "EXIT" << std::endl;
		exit(EXIT_FAILURE);
	}

	std::vector<Quadruple> qv;

	std::ifstream infile(filenameout);
	while (!infile.eof()) {
		char c = infile.peek();
		if (c == '%') {
			//std::cerr << "Comment found" << std::endl;
			char trash[1000];
			infile.getline(trash, 999);
		} else {
			Quadruple q;
			infile >> q.v[0] >> q.v[1] >> q.v[2] >> q.v[3];
			qv.push_back(q);
		}
	}
	infile.close();

	return qv;
}